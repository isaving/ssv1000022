////Version: v0.0.1
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv1000022/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-12
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.String(constant.TopicPrefix + "ssv1000022"),
		&controllers.Ssv1000022Controller{}, "Ssv1000022")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-12
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv1000022",
		beego.NSNamespace("/ssv1000022",
			beego.NSInclude(
				&controllers.Ssv1000022Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}

func InitHttpRouters() error {
	beego.Router("/v1/ssv1000022", &controllers.Ssv1000022Controller{}, "post:Bfs")
	return nil
}