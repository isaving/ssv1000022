//Version: v0.0.1
package services

import (
	"encoding/base64"
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000022/constant"
	"git.forms.io/isaving/sv/ssv1000022/util"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	commRsa "git.forms.io/universe/common/crypto/rsa"
	"git.forms.io/universe/solapp-sdk/log"
	"strconv"
)
type Ssv1000022Impl struct {
    services.CommonService
	SSV1000021O         *models.SSV1000021O
	SSV1000021I         *models.SSV1000021I
	SSV1000023O         *models.SSV1000023O
	SSV1000023I         *models.SSV1000023I

	SSV1000022O         *models.SSV1000022O
	SSV1000022I         *models.SSV1000022I

	PriKey				string
}
// @Desc Ssv1000022 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000022Impl) Ssv1000022(ssv1000022I *models.SSV1000022I) (ssv1000022O *models.SSV1000022O, err error) {

	impl.SSV1000022I = ssv1000022I

	// get sv pri_key
	if err = impl.SV100021(); nil != err {
		return nil, err
	}
	impl.PriKey = HandResult(impl.SSV1000021O, impl.SSV1000022I.KeyVersion)
	if err := VerifyPassword(impl.PriKey, impl.SSV1000022I.BeVerifiedPsw, impl.SSV1000022I.AgrPsw); nil != err {
		return nil, err
	}

	ssv1000022O = &models.SSV1000022O{
		State:"OK",
	}

	return ssv1000022O, nil
}

func HandResult (para *models.SSV1000021O, KeyVersion string) string{
	var PriKey = ""
	for _, para :=range para.Result {
		if KeyVersion == strconv.Itoa(para.KeyVersion) {
			PriKey = para.KeyValue
		}
	}
	return PriKey
}

//get sv key
func (impl *Ssv1000022Impl) SV100021() error {
	SSV1000021I := models.SSV1000021I{
		KeyType: constants.RSA256PRI,
	}
	rspBody, err := SSV1000021I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100021, rspBody);
	if err != nil{
		log.Errorf("SV100021, err:%v",err)
		return err
	}
	Model := &models.SSV1000021O{}
	if err := Model.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV1000021O = Model
	return nil
}

func VerifyPassword(Pri string, BeVerifiedPsw, AgrPsw string) error {
	//Pri_Key base64 Decode
	PriKey, _ := base64.StdEncoding.DecodeString(Pri)
	base64Psw, _ := base64.StdEncoding.DecodeString(BeVerifiedPsw)
	//NewRSA--Decrypt
	rsaInstance := commRsa.NewRSA(PriKey, nil)
	RsaPsw, err := rsaInstance.Decrypt(base64Psw)
	if nil != err {
		log.Errorf("VerifyPassword Decrypt fail，err：%v", err)
		return errors.New(err, constants.ERRCODE9)
	}
	//+salt
	byteRsq := util.GenerateKey(string(RsaPsw))
	PriKey1 := base64.StdEncoding.EncodeToString(byteRsq)

	if AgrPsw != PriKey1 {
		log.Error("VerifyPassword Fail")
		return errors.New(err, constants.ERRCODE9)
	}
	log.Info("VerifyPassword Success")
	return nil
}
