package encryption

import (
	"encoding/base64"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/crypto/aes"
	"git.forms.io/universe/common/crypto/rsa"
	"git.forms.io/universe/common/json"
	"git.forms.io/universe/common/util"
	"git.forms.io/universe/solapp-sdk/log"
)

var GlobalPublicKey []byte

type Handler interface {
	EncryptMessage(interface{}) ([]byte, error)
	DecryptMessage([]byte, interface{}) (err error)
	getAESIns() *aes.AES
}

type EncryptMessageBody struct {
	Param1 []byte `json:"param1"`
	Param2 []byte `json:"param2"`
}

type PaotangAPIHandle struct {
	aesIns *aes.AES
}

func (o *PaotangAPIHandle) getAESIns() *aes.AES {
	return o.aesIns
}

func NewPaotangAPIHandler() Handler {

	return &PaotangAPIHandle{}

}

func (o *PaotangAPIHandle) EncryptMessage(msg interface{}) (requestBody []byte, err error) {

	aesKey := []byte(util.RandomString(16))
	o.aesIns = aes.NewAES("PKCS5", "ECB", aesKey)
	rsaIns := rsa.NewRSA(nil, GlobalPublicKey)

	msgBody, err := json.Marshal(msg)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	log.Debugf("raw message: %s", msgBody)

	param1, err := o.aesIns.Encrypt(msgBody)
	if err != nil {
		return nil, errors.Errorf(constant.REQPACKERR, "AES encrypt request message failed, [%++v]", err)
	}

	param2, err := rsaIns.Encrypt(aesKey)

	if err != nil {
		return nil, errors.Errorf(constant.REQPACKERR, "RSA Encrypt AES key failed, [%++v]", err)
	}

	encryptMessageBody := EncryptMessageBody{
		Param1: param1,
		Param2: param2,
	}

	requestBody, err = json.Marshal(encryptMessageBody)
	if err != nil {
		return nil, errors.Errorf(constant.REQPACKERR, "Build encrypt message failed, [%++v]", err)
	}

	return requestBody, nil

}

func (o *PaotangAPIHandle) DecryptMessage(body []byte, msg interface{}) (err error) {

	rawMsg, err := base64.StdEncoding.DecodeString(string(body))
	if err != nil {
		return errors.Errorf(constant.RSPUNPACKERR, "Raw message base64 decode failed, %v", err)
	}

	log.Debugf("Encrypt base64: %s", body)

	rawBody, err := o.aesIns.Decrypt(rawMsg)

	if err != nil {
		return errors.Errorf(constant.RSPUNPACKERR, "Decrypt message failed, %v", err)
	}

	log.Debugf("Decrypt success raw message: %s", rawBody)

	err = json.Unmarshal(rawBody, msg)
	if err != nil {
		return errors.Errorf(constant.RSPUNPACKERR, "Unmarshal message failed, %v", err)
	}

	return nil
}
