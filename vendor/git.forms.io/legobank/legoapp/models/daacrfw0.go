package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRFW0I struct {
	HostTranSerialNo	string `validate:"required,max=42"`
	HostTranSeq			int64
}

type DAACRFW0O struct {
	AbstractCode              string `json:"AbstractCode"`
	AccessChannel             string `json:"AccessChannel"`
	AcctBal                   int    `json:"AcctBal"`
	AcctManagerNo             string `json:"AcctManagerNo"`
	AcctNo                    string `json:"AcctNo"`
	AcctgAcctNo               string `json:"AcctgAcctNo"`
	AcctingCode1              string `json:"AcctingCode1"`
	AcctingCode2              string `json:"AcctingCode2"`
	AcctingCode3              string `json:"AcctingCode3"`
	AcctingCode4              string `json:"AcctingCode4"`
	AcctingOrgID              string `json:"AcctingOrgId"`
	AcctingTime               string `json:"AcctingTime"`
	AgentOrgID                string `json:"AgentOrgId"`
	Amt                       int    `json:"Amt"`
	AmtType                   string `json:"AmtType"`
	AntiTradingMark           string `json:"AntiTradingMark"`
	ApprovalNo                string `json:"ApprovalNo"`
	AuthTeller                string `json:"AuthTeller"`
	BalanceType               string `json:"BalanceType"`
	BanknoteFlag              string `json:"BanknoteFlag"`
	BookedWorkday             string `json:"BookedWorkday"`
	BussAcctNo                string `json:"BussAcctNo"`
	BussCategories            string `json:"BussCategories"`
	BussDate                  string `json:"BussDate"`
	BussSys                   string `json:"BussSys"`
	BussTime                  string `json:"BussTime"`
	BussType                  string `json:"BussType"`
	CashTranFlag              string `json:"CashTranFlag"`
	ChannelCorrectControlFlag string `json:"ChannelCorrectControlFlag"`
	ClearingBussType          string `json:"ClearingBussType"`
	CorrectTradingFlag        string `json:"CorrectTradingFlag"`
	Country                   string `json:"Country"`
	Currency                  string `json:"Currency"`
	CustDiff                  string `json:"CustDiff"`
	CustEvent1                string `json:"CustEvent1"`
	CustEvent2                string `json:"CustEvent2"`
	CustID                    string `json:"CustId"`
	DaysOfDep                 int    `json:"DaysOfDep"`
	DebitFlag                 string `json:"DebitFlag"`
	DurationOfDep             int    `json:"DurationOfDep"`
	EventBreakdown            string `json:"EventBreakdown"`
	EventIndication           string `json:"EventIndication"`
	ExchRestType              string `json:"ExchRestType"`
	FunctionCode              string `json:"FunctionCode"`
	HostTranSeq               int    `json:"HostTranSeq"`
	HostTranSerialNo          string `json:"HostTranSerialNo"`
	IsMakeUpForPayables       string `json:"IsMakeUpForPayables"`
	IsSendClearingFlag        string `json:"IsSendClearingFlag"`
	LastMaintBrno             string `json:"LastMaintBrno"`
	LastMaintDate             string `json:"LastMaintDate"`
	LastMaintTell             string `json:"LastMaintTell"`
	LastMaintTime             string `json:"LastMaintTime"`
	LiquidationDate           string `json:"LiquidationDate"`
	LiquidationTime           string `json:"LiquidationTime"`
	LocalAcctDiff             string `json:"LocalAcctDiff"`
	LocalBankFlag             string `json:"LocalBankFlag"`
	LocalMediaPrefix          string `json:"LocalMediaPrefix"`
	LocalTranDetailType       string `json:"LocalTranDetailType"`
	MediaNo                   string `json:"MediaNo"`
	MediaType                 string `json:"MediaType"`
	MessageCode               string `json:"MessageCode"`
	OriginalHostTranSerialNo  int    `json:"OriginalHostTranSerialNo"`
	OriginalTradeTellerNo     string `json:"OriginalTradeTellerNo"`
	OriginalTradingDay        string `json:"OriginalTradingDay"`
	OtherBankFlag             string `json:"OtherBankFlag"`
	PeripheralSysWorkday      string `json:"PeripheralSysWorkday"`
	PeripheralSysWorktime     string `json:"PeripheralSysWorktime"`
	PeripheralTranSeq         int    `json:"PeripheralTranSeq"`
	PeripheralTranSerialNo    string `json:"PeripheralTranSerialNo"`
	ProdCode                  string `json:"ProdCode"`
	ProdSeq                   int    `json:"ProdSeq"`
	ReviewTeller              string `json:"ReviewTeller"`
	SetNo                     string `json:"SetNo"`
	SubjectBreakdown          string `json:"SubjectBreakdown"`
	SubjectNo                 string `json:"SubjectNo"`
	TccState                  int    `json:"TccState"`
	TerminalNo                string `json:"TerminalNo"`
	TheMerchantNo             string `json:"TheMerchantNo"`
	TranChannel               string `json:"TranChannel"`
	TranOrgID                 string `json:"TranOrgId"`
	TranTeller                string `json:"TranTeller"`
	TrasactionCode            string `json:"TrasactionCode"`
	UseCode                   string `json:"UseCode"`
	ValueDate                 string `json:"ValueDate"`
	WhetherToAllowPunching    string `json:"WhetherToAllowPunching"`
}

type DAACRFW0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRFW0I
}

type DAACRFW0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRFW0O
}

type DAACRFW0RequestForm struct {
	Form []DAACRFW0IDataForm
}

type DAACRFW0ResponseForm struct {
	Form []DAACRFW0ODataForm
}

// @Desc Build request message
func (o *DAACRFW0RequestForm) PackRequest(DAACRFW0I DAACRFW0I) (responseBody []byte, err error) {

	requestForm := DAACRFW0RequestForm{
		Form: []DAACRFW0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRFW0I",
				},
				FormData: DAACRFW0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRFW0RequestForm) UnPackRequest(request []byte) (DAACRFW0I, error) {
	DAACRFW0I := DAACRFW0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRFW0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRFW0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRFW0ResponseForm) PackResponse(DAACRFW0O DAACRFW0O) (responseBody []byte, err error) {
	responseForm := DAACRFW0ResponseForm{
		Form: []DAACRFW0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRFW0O",
				},
				FormData: DAACRFW0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRFW0ResponseForm) UnPackResponse(request []byte) (DAACRFW0O, error) {

	DAACRFW0O := DAACRFW0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRFW0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRFW0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRFW0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
