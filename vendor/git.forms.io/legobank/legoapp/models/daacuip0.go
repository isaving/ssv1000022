package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUIP0I struct {
	IntPlanNo           string ` validate:"required,max=20"`
	BankNo              string
	Currency            string
	IntFlag             string
	IntCalcOption       string
	MinIntAmt           float64
	LyrdFlag            string
	AmtLvlsFlag         string
	TermLvlsFlag        string
	TermAmtPrtyFlag     string
	IntRateUseFlag      string
	IntRateNo           string
	FixdIntRate         float64
	MinIntRate          float64
	MonIntUnit          string
	YrIntUnit           string
	FloatOption         string
	FloatFlag           string
	FloatCeil           float64
	FloatFloor          float64
	DefFloatRate        float64
	CornerFlag          string
	CritocalPointFlag   string
	LvlsIntRateAmtType  string
	TolLvls             int64
	LyrdTermUnit        string
	LoanIntRateAdjType  string
	LoanIntRateAdjCycle string
	LoanIntRateAdjFreq  string
	IntPlanDesc         string
	Flag1               string
	Flag2               string
	Back1               float64
	Back2               float64
	Back3               string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
	TccState            int
}

type DAACUIP0O struct {

}

type DAACUIP0IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAACUIP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUIP0O
}

type DAACUIP0RequestForm struct {
	Form []DAACUIP0IDataForm
}

type DAACUIP0ResponseForm struct {
	Form []DAACUIP0ODataForm
}

// @Desc Build request message
func (o *DAACUIP0RequestForm) PackRequest(DAACUIP0I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAACUIP0RequestForm{
		Form: []DAACUIP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUIP0I",
				},
				FormData: DAACUIP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUIP0RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAACUIP0I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUIP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUIP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUIP0ResponseForm) PackResponse(DAACUIP0O DAACUIP0O) (responseBody []byte, err error) {
	responseForm := DAACUIP0ResponseForm{
		Form: []DAACUIP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUIP0O",
				},
				FormData: DAACUIP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUIP0ResponseForm) UnPackResponse(request []byte) (DAACUIP0O, error) {

	DAACUIP0O := DAACUIP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUIP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUIP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUIP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
