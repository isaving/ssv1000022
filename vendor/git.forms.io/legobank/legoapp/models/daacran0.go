package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRAN0I struct {
	AcctgAcctNo		string	`validate:"max=20,required"` // 贷款核算账号
}

type DAACRAN0O struct {
	AccruedIntAmt         int         `json:"AccruedIntAmt"`
	AcctOpenDate          string      `json:"AcctOpenDate"`
	AcctStatus            string      `json:"AcctStatus"`
	AcctType              string      `json:"AcctType"`
	AcctgAcctNo           string      `json:"AcctgAcctNo"`
	BanknoteFlag          string      `json:"BanknoteFlag"`
	ContID                string      `json:"ContId"`
	Currency              string      `json:"Currency"`
	CurrencyMarket        string      `json:"CurrencyMarket"`
	CurrentAcctBal        int         `json:"CurrentAcctBal"`
	CurrentAcctBalTemp    int         `json:"CurrentAcctBalTemp"`
	CustID                string      `json:"CustId"`
	CustLastEventDate     string      `json:"CustLastEventDate"`
	CustType              string      `json:"CustType"`
	DayCashInAmt          int         `json:"DayCashInAmt"`
	DayCashOutAmt         int         `json:"DayCashOutAmt"`
	DayCreditAmt          int         `json:"DayCreditAmt"`
	DayCreditCount        int         `json:"DayCreditCount"`
	DayDebitAmt           int         `json:"DayDebitAmt"`
	DayDebitCount         int         `json:"DayDebitCount"`
	DayTranCnt            int         `json:"DayTranCnt"`
	DayTranInAmt          int         `json:"DayTranInAmt"`
	DayTranOutAmt         int         `json:"DayTranOutAmt"`
	DebitFlag             string      `json:"DebitFlag"`
	FirstDepDate          string      `json:"FirstDepDate"`
	FrozenAmt             int         `json:"FrozenAmt"`
	IntPaidAmt            int         `json:"IntPaidAmt"`
	IsAllowOverdraft      string      `json:"IsAllowOverdraft"`
	IsCalInt              string      `json:"IsCalInt"`
	LastAccruedDate       string      `json:"LastAccruedDate"`
	LastAcctBal           int         `json:"LastAcctBal"`
	LastEventDate         string      `json:"LastEventDate"`
	LastMaintBrno         string      `json:"LastMaintBrno"`
	LastMaintDate         string      `json:"LastMaintDate"`
	LastMaintTell         string      `json:"LastMaintTell"`
	LastMaintTime         string      `json:"LastMaintTime"`
	LastRestAmt           int         `json:"LastRestAmt"`
	LastRestBeginDate     string      `json:"LastRestBeginDate"`
	LastRestDate          string      `json:"LastRestDate"`
	LastSubmitDate        string      `json:"LastSubmitDate"`
	LastTotDays           string      `json:"LastTotDays"`
	NextAccrualDate       string      `json:"NextAccrualDate"`
	NextRestDate          string      `json:"NextRestDate"`
	OrgID                 string      `json:"OrgId"`
	ProdCode              string      `json:"ProdCode"`
	ReservedAmt           int         `json:"ReservedAmt"`
	RestPeriodAccruedDays int         `json:"RestPeriodAccruedDays"`
	SerialNumber          interface{} `json:"SerialNumber"`
	SumCurrentBal         int         `json:"SumCurrentBal"`
	SumPeriodicBal        int         `json:"SumPeriodicBal"`
	TccState              int         `json:"TccState"`
}

type DAACRAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRAN0I
}

type DAACRAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRAN0O
}

type DAACRAN0RequestForm struct {
	Form []DAACRAN0IDataForm
}

type DAACRAN0ResponseForm struct {
	Form []DAACRAN0ODataForm
}

// @Desc Build request message
func (o *DAACRAN0RequestForm) PackRequest(DAACRAN0I DAACRAN0I) (responseBody []byte, err error) {

	requestForm := DAACRAN0RequestForm{
		Form: []DAACRAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRAN0I",
				},
				FormData: DAACRAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRAN0RequestForm) UnPackRequest(request []byte) (DAACRAN0I, error) {
	DAACRAN0I := DAACRAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRAN0ResponseForm) PackResponse(DAACRAN0O DAACRAN0O) (responseBody []byte, err error) {
	responseForm := DAACRAN0ResponseForm{
		Form: []DAACRAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRAN0O",
				},
				FormData: DAACRAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRAN0ResponseForm) UnPackResponse(request []byte) (DAACRAN0O, error) {

	DAACRAN0O := DAACRAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
