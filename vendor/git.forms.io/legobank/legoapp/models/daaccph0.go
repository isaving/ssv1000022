package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCPH0I struct {
	IntPlanNo      string ` validate:"required,max=20"`
	Lvls           int64
	BankNo         string
	DepCycle       string
	Dep            int64
	LvlsAmt        float64
	EffectDate     string ` validate:"required,max=10"`
	ChangeDate     string ` validate:"required,max=10"`
	IntRateUseFlag string ` validate:"required,max=1"`
	IntRateNo      string
	IntRateBankNo  string
	IntRate        float64
	FloatOption    string
	FloatFlag      string
	FloatCeil      float64
	FloatFloor     float64
	DefFloatRate   float64
	MinIntRate     float64
	ChangeTell     string
	AuthTeller     string
	Flag1          string
	Flag2          string
	Back1          float64
	Back2          float64
	Back3          string
	Back4          string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}

type DAACCPH0O struct {
	IntPlanNo      string
	Lvls           int64
	BankNo         string
	DepCycle       string
	Dep            int64
	LvlsAmt        float64
	EffectDate     string
	ChangeDate     string
	IntRateUseFlag string
	IntRateNo      string
	IntRateBankNo  string
	IntRate        float64
	FloatOption    string
	FloatFlag      string
	FloatCeil      float64
	FloatFloor     float64
	DefFloatRate   float64
	MinIntRate     float64
	ChangeTell     string
	AuthTeller     string
	Flag1          string
	Flag2          string
	Back1          float64
	Back2          float64
	Back3          string
	Back4          string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
}

type DAACCPH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCPH0I
}

type DAACCPH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCPH0O
}

type DAACCPH0RequestForm struct {
	Form []DAACCPH0IDataForm
}

type DAACCPH0ResponseForm struct {
	Form []DAACCPH0ODataForm
}

// @Desc Build request message
func (o *DAACCPH0RequestForm) PackRequest(DAACCPH0I DAACCPH0I) (responseBody []byte, err error) {

	requestForm := DAACCPH0RequestForm{
		Form: []DAACCPH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCPH0I",
				},
				FormData: DAACCPH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCPH0RequestForm) UnPackRequest(request []byte) (DAACCPH0I, error) {
	DAACCPH0I := DAACCPH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCPH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCPH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCPH0ResponseForm) PackResponse(DAACCPH0O DAACCPH0O) (responseBody []byte, err error) {
	responseForm := DAACCPH0ResponseForm{
		Form: []DAACCPH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCPH0O",
				},
				FormData: DAACCPH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCPH0ResponseForm) UnPackResponse(request []byte) (DAACCPH0O, error) {

	DAACCPH0O := DAACCPH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCPH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCPH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCPH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
