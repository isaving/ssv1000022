package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACU005I struct {

}

type DAACU005O struct {

}

type DAACU005IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAACU005ODataForm struct {
	FormHead CommonFormHead
	FormData DAACU005O
}

type DAACU005RequestForm struct {
	Form []DAACU005IDataForm
}

type DAACU005ResponseForm struct {
	Form []DAACU005ODataForm
}

// @Desc Build request message
func (o *DAACU005RequestForm) PackRequest(DAACU005I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAACU005RequestForm{
		Form: []DAACU005IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACU005I",
				},
				FormData: DAACU005I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACU005RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAACU005I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAACU005I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACU005I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACU005ResponseForm) PackResponse(DAACU005O DAACU005O) (responseBody []byte, err error) {
	responseForm := DAACU005ResponseForm{
		Form: []DAACU005ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACU005O",
				},
				FormData: DAACU005O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACU005ResponseForm) UnPackResponse(request []byte) (DAACU005O, error) {

	DAACU005O := DAACU005O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACU005O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACU005O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACU005I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
