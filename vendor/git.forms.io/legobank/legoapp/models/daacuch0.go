package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUCH0I struct {
	//输入是个map
}

type DAACUCH0O struct {

}

type DAACUCH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUCH0I
}

type DAACUCH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUCH0O
}

type DAACUCH0RequestForm struct {
	Form []DAACUCH0IDataForm
}

type DAACUCH0ResponseForm struct {
	Form []DAACUCH0ODataForm
}

// @Desc Build request message
func (o *DAACUCH0RequestForm) PackRequest(DAACUCH0I DAACUCH0I) (responseBody []byte, err error) {

	requestForm := DAACUCH0RequestForm{
		Form: []DAACUCH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUCH0I",
				},
				FormData: DAACUCH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUCH0RequestForm) UnPackRequest(request []byte) (DAACUCH0I, error) {
	DAACUCH0I := DAACUCH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUCH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUCH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUCH0ResponseForm) PackResponse(DAACUCH0O DAACUCH0O) (responseBody []byte, err error) {
	responseForm := DAACUCH0ResponseForm{
		Form: []DAACUCH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUCH0O",
				},
				FormData: DAACUCH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUCH0ResponseForm) UnPackResponse(request []byte) (DAACUCH0O, error) {

	DAACUCH0O := DAACUCH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUCH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUCH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUCH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
