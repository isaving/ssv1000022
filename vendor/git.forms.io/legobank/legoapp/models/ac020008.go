package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020008I struct {
	AcctgAcctNo       string  `validate:"max=32,required"` // 贷款核算账
	PeriodNum         int     `validate:"required"`        //期数
	TradeDate         string  `validate:"max=10"`          // 交易日期
	CurrPeriodRepayDt string  `validate:"max=10,required"` //当期还款日期 curr_period_repay_dt
	PlanRepayBal      float64 `validate:"required"`        //计划还款本金 plan_repay_bal
}

type AC020008O struct {
	AcctgAcctNo              string  `valid:"Required;MaxSize(20)"` //贷款核算账
	PeriodNum                int     `valid:"Required;MaxSize(8)"`  //期数
	AcctgDate                string  `valid:"Required;MaxSize(10)"` //动账日期
	CurrentStatus            string  `valid:"Required;MaxSize(1)"`  //当前记录状态
	CurrPeriodIntacrBgnDt    string  `valid:"Required;MaxSize(10)"` //当期计息开始日期
	CurrPeriodIntacrEndDt    string  `valid:"Required;MaxSize(10)"` //当期计息结束日期
	ValueDate                string  `valid:"Required;MaxSize(10)"` //起息日期
	BurningSum               float64 `valid:"Required"`             //核销金额
	PlanRepayBal             float64 `valid:"Required"`             //计划还款本金
	CurrPeriodRepayDt        string  `valid:"Required;MaxSize(10)"` //当期还款日
	RepayStatus              string  `valid:"Required;MaxSize(2)"`  //本金状态
	CurrentPeriodUnStillPrin float64 `valid:"Required"`             //到期未还本金
	ActualRepayDate          string  `valid:"Required;MaxSize(10)"` //实际还款日期
	ActualRepayBal           float64 `valid:"Required"`             //实际还款本金
}

type AC020008IDataForm struct {
	FormHead CommonFormHead
	FormData AC020008I
}

type AC020008ODataForm struct {
	FormHead CommonFormHead
	FormData AC020008O
}

type AC020008RequestForm struct {
	Form []AC020008IDataForm
}

type AC020008ResponseForm struct {
	Form []AC020008ODataForm
}

// @Desc Build request message
func (o *AC020008RequestForm) PackRequest(AC020008I AC020008I) (responseBody []byte, err error) {

	requestForm := AC020008RequestForm{
		Form: []AC020008IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020008I",
				},
				FormData: AC020008I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC020008RequestForm) UnPackRequest(request []byte) (AC020008I, error) {
	AC020008I := AC020008I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC020008I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020008I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC020008ResponseForm) PackResponse(AC020008O AC020008O) (responseBody []byte, err error) {
	responseForm := AC020008ResponseForm{
		Form: []AC020008ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020008O",
				},
				FormData: AC020008O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020008ResponseForm) UnPackResponse(request []byte) (AC020008O, error) {

	AC020008O := AC020008O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC020008O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020008O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC020008I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
