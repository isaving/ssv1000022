package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type Ac020001RequestForm struct {
	Form []Ac020001IDataForm `json:"Form"`
}

type Ac020001IDataForm struct {
	FormHead CommonFormHead		`json:"FormHead"`
	FormData Ac020001I		`json:"FormData"`
}
type Ac020001I []struct {
	AcctgAcctNo string `validate:"required"`
}

type Ac020001ResponseForm struct {
	Form []Ac020001ODataForm `json:"Form"`
}

type Ac020001ODataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData Ac020001O      `json:"FormData"`
}
type Ac020001O struct {
	Records		[]map[string]interface{}
}

type Ac020001ORecords struct {
	AcctgAcctNo     string 								  //贷款核算账号
	Currency        string                                //币种
	Status          string                                //余额状态
	AcctCreateDate  string                                //开户日
	Balance         float64                                //余额
	BalanceYestoday float64                                //昨日余额
	LastTranDate    string                                //最后动账日期
	AcctStatus      string                                //账户状态
	MgmtOrgId       string                                //管理行所号
	AcctingOrgId    string                                //核算行所号
	LastMaintDate   string                                //最后更新日期
	LastMaintTime   string                                //最后更新时间
}
// @Desc Parsing request message
func (w *Ac020001RequestForm) UnPackRequest(req []byte) (Ac020001I, error) {
	Ac020001I := Ac020001I{}
	if err := json.Unmarshal(req, w); nil != err {
		return Ac020001I, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return Ac020001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

// @Desc Build response message
func (w *Ac020001ResponseForm) PackResponse(Ac020001OData Ac020001O) (res []byte, err error) {
	resForm := Ac020001ResponseForm{
		Form: []Ac020001ODataForm{
			Ac020001ODataForm{
				FormHead: CommonFormHead{
					FormId: "Ac020001O001",
				},
				FormData: Ac020001OData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Build request message
func (w *Ac020001RequestForm) PackRequest(Ac020001IData Ac020001I) (res []byte, err error) {
	requestForm := Ac020001RequestForm{
		Form: []Ac020001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "Ac020001I001",
				},
				FormData: Ac020001IData,
			},
		},
	}

	rspBody, err := json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Parsing response message
func (w *Ac020001ResponseForm) UnPackResponse(req []byte) (Ac020001O, error) {
	Ac020001O := Ac020001O{}

	if err := json.Unmarshal(req, w); nil != err {
		return Ac020001O, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return Ac020001O, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

func (w *Ac020001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}