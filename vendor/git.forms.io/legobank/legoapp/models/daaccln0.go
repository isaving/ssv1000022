package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLN0I struct {
	AcctgAcctNo      string ` validate:"required,max=32"`
	CurrencyMarket   string
	Currency         string
	Status           string
	AcctType         string
	CustID           string
	ContID           string
	ProdCode         string
	OrgCreate        string
	IsCalInt         string
	AcctCreateDate   string
	BalanceType      string
	BtBalTemp        string
	Balance          string
	BalanceYestoday  string
	InterestRate     string
	Interest         string
	InterestYestoday string
	LastCalcIntdate  string
	LastTranIntdate  string
	TotIntReceived   float64
	LastTranDate     string
	MgmtOrgId        string
	AcctingOrgId     string
	LastMaintBrno    string
	LastMaintTell    string
}

type DAACCLN0O struct {
	AcctgAcctNo      string ` validate:"required,max=32"`
	CurrencyMarket   string
	Currency         string
	Status           string
	AcctType         string
	CustID           string
	ContID           string
	ProdCode         string
	OrgCreate        string
	IsCalInt         string
	AcctCreateDate   string
	BalanceType      string
	BtBalTemp        string
	Balance          string
	BalanceYestoday  string
	InterestRate     string
	Interest         string
	InterestYestoday string
	LastCalcIntdate  string
	LastTranIntdate  string
	TotIntReceived   float64
	LastTranDate     string
	MgmtOrgId        string
	AcctingOrgId     string
	LastMaintBrno    string
	LastMaintTell    string
}

type DAACCLN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLN0I
}

type DAACCLN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLN0O
}

type DAACCLN0RequestForm struct {
	Form []DAACCLN0IDataForm
}

type DAACCLN0ResponseForm struct {
	Form []DAACCLN0ODataForm
}

// @Desc Build request message
func (o *DAACCLN0RequestForm) PackRequest(DAACCLN0I DAACCLN0I) (responseBody []byte, err error) {

	requestForm := DAACCLN0RequestForm{
		Form: []DAACCLN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLN0I",
				},
				FormData: DAACCLN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLN0RequestForm) UnPackRequest(request []byte) (DAACCLN0I, error) {
	DAACCLN0I := DAACCLN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLN0ResponseForm) PackResponse(DAACCLN0O DAACCLN0O) (responseBody []byte, err error) {
	responseForm := DAACCLN0ResponseForm{
		Form: []DAACCLN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLN0O",
				},
				FormData: DAACCLN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLN0ResponseForm) UnPackResponse(request []byte) (DAACCLN0O, error) {

	DAACCLN0O := DAACCLN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
