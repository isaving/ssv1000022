package controllers

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/model"
	"git.forms.io/legobankda/inteloan/services"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	jsoniter "github.com/json-iterator/go"
)

type T_sms_template_tempController struct {
	controllers.DTSBaseController
}

func (c *T_sms_template_tempController) InsertT_sms_template_temp() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.InsertT_sms_template_temp)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start InsertT_sms_template_temp, request: %++v", c.Req)
	request := model.T_sms_template_tempEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	//formId := beego.AppConfig.String("form_id::InsertT_sms_template_temp")
	service := &services.T_sms_template_tempServiceImpl{}
	maps := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&maps)
	c.Rsp = util.CallDts(constant.InsertT_sms_template_temp+constant.Fomif2, service, services.InsertT_sms_template_tempCompensable, maps, c.DTSCtx)
}

func (c *T_sms_template_tempController) QueryT_sms_template_temp() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryT_sms_template_temp)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryT_sms_template_temp, request: %++v", c.Req)
	request := model.T_sms_template_tempEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	service := &services.T_sms_template_tempServiceImpl{}
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::QueryT_sms_template_temp")
	if ret, err := service.QueryT_sms_template_temp(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		if len(ret) != 0 {
			sum := len(ret)
			ret = util.QueryPage(ret, request.Form[0].FormData.PageNo, request.Form[0].FormData.PageRecCount)
			pageNo := request.Form[0].FormData.PageNo
			if pageNo == 0 {
				pageNo = 1
			}
			c.Rsp = util.BuildPageResponse("", sum, pageNo, ret)
		} else {
			c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		}
	}
}

func (c *T_sms_template_tempController) UpdateT_sms_template_temp() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.UpdateT_sms_template_temp)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start UpdateT_sms_template_temp, request: %++v", c.Req)
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::UpdateT_sms_template_temp")
	service := &services.T_sms_template_tempServiceImpl{}
	c.Rsp = util.CallDts(constant.UpdateT_sms_template_temp+constant.Fomif2, service, services.UpdateT_sms_template_tempCompensable, cols, c.DTSCtx)
}
