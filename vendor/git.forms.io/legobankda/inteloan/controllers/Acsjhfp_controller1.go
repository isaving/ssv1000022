package controllers

import (
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	"math"
	"sync"
)

// CMS API
type Acsjhfp1Controller struct {
	beego.Controller
	controllers.DTSBaseController
}

func (c *Acsjhfp1Controller) QueryAcsjhfp1() {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			//err := errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()



	maps := make(map[string]interface{})
	c.Rsp = util.BuildSucResponse("",  maps)
}

func count()  {
	x :=0
	for i :=0;i<math.MaxUint32;i++{
		x+=i
	}
	println(x)
}

func test(n int){
	for i :=0;i<n;i++{
		count()
	}
}

func test2(n int)  {
	var wg sync.WaitGroup
	wg.Add(n)
	for i :=0;i<n;i++{
		go func() {
			count()
			wg.Done()
		}()
	}
	wg.Wait()
}