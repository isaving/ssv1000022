package controllers

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/model"
	"git.forms.io/legobankda/inteloan/services"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	jsoniter "github.com/json-iterator/go"
)

type T_loan_repayment_billController struct {
	controllers.DTSBaseController
}

func (c *T_loan_repayment_billController) InsertT_loan_repayment_bill() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.InsertT_loan_repayment_bill)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start InsertT_loan_repayment_bill, request: %++v", c.Req)
	request := model.T_loan_repayment_billEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	//formId := beego.AppConfig.String("form_id::InsertT_loan_repayment_bill")
	service := &services.T_loan_repayment_billServiceImpl{}
	maps := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&maps)
	c.Rsp = util.CallDts(constant.InsertT_loan_repayment_bill+constant.Fomif2, service, services.InsertT_loan_repayment_billCompensable, maps, c.DTSCtx)
}

func (c *T_loan_repayment_billController) QueryT_loan_repayment_bill() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryT_loan_repayment_bill)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryT_loan_repayment_bill, request: %++v", c.Req)
	request := model.T_loan_repayment_billEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	service := &services.T_loan_repayment_billServiceImpl{}
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	flag := cols["Flag"]
	//formId := beego.AppConfig.String("form_id::QueryT_loan_repayment_bill")
	if ret, err := service.QueryT_loan_repayment_bill(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		if len(ret) != 0 {
			if flag == "8"{
				c.Rsp = util.BuildArrResponse("",ret)
				return
			}
			sum := len(ret)
			ret = util.QueryPage(ret, request.Form[0].FormData.PageNo, request.Form[0].FormData.PageRecCount)
			pageNo := request.Form[0].FormData.PageNo
			if pageNo == 0 {
				pageNo = 1
			}
			c.Rsp = util.BuildPageResponse("", sum, pageNo, ret)
		} else {
			c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		}
	}
}

func (c *T_loan_repayment_billController) UpdateT_loan_repayment_bill() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.UpdateT_loan_repayment_bill)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start UpdateT_loan_repayment_bill, request: %++v", c.Req)
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::UpdateT_loan_repayment_bill")
	service := &services.T_loan_repayment_billServiceImpl{}
	c.Rsp = util.CallDts(constant.UpdateT_loan_repayment_bill+constant.Fomif2, service, services.UpdateT_loan_repayment_billCompensable, cols, c.DTSCtx)
}
