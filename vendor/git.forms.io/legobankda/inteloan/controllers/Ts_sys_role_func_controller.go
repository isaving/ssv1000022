package controllers

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/model"
	"git.forms.io/legobankda/inteloan/services"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	jsoniter "github.com/json-iterator/go"
)

type Ts_sys_role_funcController struct {
	controllers.DTSBaseController
}

func (c *Ts_sys_role_funcController) InsertTs_sys_role_func() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.InsertTs_sys_role_func)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start InsertTs_sys_role_func, request: %++v", c.Req)
	request := model.Ts_sys_role_funcEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	//formId := beego.AppConfig.String("form_id::InsertTs_sys_role_func")
	service := &services.Ts_sys_role_funcServiceImpl{}
	maps := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&maps)
	c.Rsp = util.CallDts(constant.InsertTs_sys_role_func+constant.Fomif2, service, services.InsertTs_sys_role_funcCompensable, maps, c.DTSCtx)
}

func (c *Ts_sys_role_funcController) QueryTs_sys_role_func() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryTs_sys_role_func)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryTs_sys_role_func, request: %++v", c.Req)
	request := model.Ts_sys_role_funcEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	service := &services.Ts_sys_role_funcServiceImpl{}
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	flag := cols["Flag"]
	//formId := beego.AppConfig.String("form_id::QueryTs_sys_role_func")
	if ret, err := service.QueryTs_sys_role_func(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		if flag=="8"{
			c.Rsp = util.BuildArrResponse("", ret)
			return
		}
		if len(ret) != 0 {
			sum := len(ret)
			ret = util.QueryPage(ret, request.Form[0].FormData.PageNo, request.Form[0].FormData.PageRecCount)
			pageNo := request.Form[0].FormData.PageNo
			if pageNo == 0 {
				pageNo = 1
			}
			c.Rsp = util.BuildPageResponse("", sum, pageNo, ret)
		} else {
			c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		}
	}
}

func (c *Ts_sys_role_funcController) UpdateTs_sys_role_func() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.UpdateTs_sys_role_func)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start UpdateTs_sys_role_func, request: %++v", c.Req)
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::UpdateTs_sys_role_func")
	service := &services.Ts_sys_role_funcServiceImpl{}
	c.Rsp = util.CallDts(constant.UpdateTs_sys_role_func+constant.Fomif2, service, services.UpdateTs_sys_role_funcCompensable, cols, c.DTSCtx)
}

func (c *Ts_sys_role_funcController) DeleteTs_sys_role_func() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.DeleteTs_sys_role_func)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start DeleteTs_sys_role_func, request: %++v", c.Req)
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::DeleteTs_sys_role_func")
	service := &services.Ts_sys_role_funcServiceImpl{}
	if err := service.ConfirmDeleteTs_sys_role_func(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.DeleteTs_sys_role_func, err.Error())
	} else {
		ResponseMap := make(map[string]interface{})
		ResponseMap["status"]="ok"
		c.Rsp = util.BuildSucResponse("", ResponseMap)
	}
	//c.Rsp = util.CallDts(constant.DeleteTs_sys_role_func+constant.Fomif2, service, services.DeleteTs_sys_role_funcCompensable, cols, c.DTSCtx)
}