package controllers

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/services"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	jsoniter "github.com/json-iterator/go"
)

type T_acct_loan_acctg_periodController struct {
	controllers.DTSBaseController
}

func (c *T_acct_loan_acctg_periodController) QueryT_acct_loan_acctg_period() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryT_acct_loan_acctg_period)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryT_acct_loan_acctg_period, request: %++v", c.Req)
	//request := model.T_acct_loan_acctg_periodEvent{}
	//if err := util.CheckRequest(c.Req.Body, &request); err != nil {
	//	log.Errorf("request message decode failed,err=%v", err)
	//	c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
	//	return
	//}
	service := &services.T_acct_loan_acctg_periodServiceImpl{}
	cols := make([]map[string]interface{},0)
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::QueryT_acct_loan_acctg_period")
	if ret, err := service.QueryT_acct_loan_acctg_period(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		c.Rsp = util.BuildArrResponse("",  ret)
		//if len(ret) != 0 {
		//} else {
		//	c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		//}
	}
}
