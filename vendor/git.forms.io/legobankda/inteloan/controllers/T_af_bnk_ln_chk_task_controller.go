package controllers

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/model"
	"git.forms.io/legobankda/inteloan/services"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	jsoniter "github.com/json-iterator/go"
)

type T_af_bnk_ln_chk_taskController struct {
	controllers.DTSBaseController
}

func (c *T_af_bnk_ln_chk_taskController) InsertT_af_bnk_ln_chk_task() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.InsertT_af_bnk_ln_chk_task)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start InsertT_af_bnk_ln_chk_task, request: %++v", c.Req)
	request := model.T_af_bnk_ln_chk_taskEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	//formId := beego.AppConfig.String("form_id::InsertT_af_bnk_ln_chk_task")
	service := &services.T_af_bnk_ln_chk_taskServiceImpl{}
	maps := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&maps)
	c.Rsp = util.CallDts(constant.InsertT_af_bnk_ln_chk_task+constant.Fomif2, service, services.InsertT_af_bnk_ln_chk_taskCompensable, maps, c.DTSCtx)
}

func (c *T_af_bnk_ln_chk_taskController) QueryT_af_bnk_ln_chk_task() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryT_af_bnk_ln_chk_task)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryT_af_bnk_ln_chk_task, request: %++v", c.Req)
	request := model.T_af_bnk_ln_chk_taskEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	service := &services.T_af_bnk_ln_chk_taskServiceImpl{}
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::QueryT_af_bnk_ln_chk_task")
	if ret, err := service.QueryT_af_bnk_ln_chk_task(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		if len(ret) != 0 {
			sum := len(ret)
			ret = util.QueryPage(ret, request.Form[0].FormData.PageNo, request.Form[0].FormData.PageRecCount)
			pageNo := request.Form[0].FormData.PageNo
			if pageNo == 0 {
				pageNo = 1
			}
			c.Rsp = util.BuildPageResponse("", sum, pageNo, ret)
		} else {
			c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		}
	}
}

func (c *T_af_bnk_ln_chk_taskController) UpdateT_af_bnk_ln_chk_task() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.UpdateT_af_bnk_ln_chk_task)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start UpdateT_af_bnk_ln_chk_task, request: %++v", c.Req)
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::UpdateT_af_bnk_ln_chk_task")
	service := &services.T_af_bnk_ln_chk_taskServiceImpl{}
	c.Rsp = util.CallDts(constant.UpdateT_af_bnk_ln_chk_task+constant.Fomif2, service, services.UpdateT_af_bnk_ln_chk_taskCompensable, cols, c.DTSCtx)
}

//SELECT COUNT(*),opror_empnbr ,GROUP_CONCAT(af_bnk_ln_chk_task_stus_cd) FROM `t_af_bnk_ln_chk_task` GROUP BY opror_empnbr;
//统计百分比
func (c *T_af_bnk_ln_chk_taskController) QueryT_af_bnk_ln_chk_taskPercentage() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryT_af_bnk_ln_chk_task)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryT_af_bnk_ln_chk_task, request: %++v", c.Req)
	request := model.T_af_bnk_ln_chk_taskEvent1{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	service := &services.T_af_bnk_ln_chk_taskServiceImpl{}
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::QueryT_af_bnk_ln_chk_task")
	if ret, err := service.QueryT_af_bnk_ln_chk_taskPercentage(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		if len(ret) != 0 {
			c.Rsp = util.BuildArrResponse("", ret)
		} else {
			c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		}
	}
}
