package controllers

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/legobankda/inteloan/model"
	"git.forms.io/legobankda/inteloan/services"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	jsoniter "github.com/json-iterator/go"
)

type T_loan_batch_contract_trans_meterController struct {
	controllers.DTSBaseController
}

func (c *T_loan_batch_contract_trans_meterController) InsertT_loan_batch_contract_trans_meter() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.InsertT_loan_batch_contract_trans_meter)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start InsertT_loan_batch_contract_trans_meter, request: %++v", c.Req)
	request := model.T_loan_batch_contract_trans_meterEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	//formId := beego.AppConfig.String("form_id::InsertT_loan_batch_contract_trans_meter")
	service := &services.T_loan_batch_contract_trans_meterServiceImpl{}
	maps := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&maps)
	c.Rsp = util.CallDts(constant.InsertT_loan_batch_contract_trans_meter+constant.Fomif2, service, services.InsertT_loan_batch_contract_trans_meterCompensable, maps, c.DTSCtx)
}

func (c *T_loan_batch_contract_trans_meterController) QueryT_loan_batch_contract_trans_meter() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.QueryT_loan_batch_contract_trans_meter)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start QueryT_loan_batch_contract_trans_meter, request: %++v", c.Req)
	request := model.T_loan_batch_contract_trans_meterEvent{}
	if err := util.CheckRequest(c.Req.Body, &request); err != nil {
		log.Errorf("request message decode failed,err=%v", err)
		c.Rsp = util.BuildErrResponse(constant.DECODE_ERR, err.Error())
		return
	}
	service := &services.T_loan_batch_contract_trans_meterServiceImpl{}
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::QueryT_loan_batch_contract_trans_meter")
	if ret, err := service.QueryT_loan_batch_contract_trans_meter(cols); err != nil {
		c.Rsp = util.BuildErrResponse(constant.QUERY_FAILED, err.Error())
	} else {
		if len(ret) != 0 {
			sum := len(ret)
			ret = util.QueryPage(ret, request.Form[0].FormData.PageNo, request.Form[0].FormData.PageRecCount)
			pageNo := request.Form[0].FormData.PageNo
			if pageNo == 0 {
				pageNo = 1
			}
			c.Rsp = util.BuildPageResponse("", sum, pageNo, ret)
		} else {
			c.Rsp = util.BuildPageResponse("", 0, 1, ret)
		}
	}
}

func (c *T_loan_batch_contract_trans_meterController) UpdateT_loan_batch_contract_trans_meter() {
	retCode := "0"
	defer func() {
		util.HandleSysHeader(c.Req, c.Rsp, constant.UpdateT_loan_batch_contract_trans_meter)
		if c.Rsp.AppProps[constant.RET_STATUS] == constant.FAILED_STATUS {
			retCode = "1"
		}
		log.Infof_V4("End response: %++v", c.Rsp)
	}()
	log.Infof_V4("Start UpdateT_loan_batch_contract_trans_meter, request: %++v", c.Req)
	cols := make(map[string]interface{})
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	json.Get(c.Req.Body, "Form", 0).Get("FormData").ToVal(&cols)
	//formId := beego.AppConfig.String("form_id::UpdateT_loan_batch_contract_trans_meter")
	flag := cols["Flag"]
	delete(cols,"Flag")
	if flag == "9"{
		response := &client.UserMessage{}
		if err :=dao.UpdateT_loan_batch_contract_trans_meter(cols);err != nil {
			response = util.BuildErrResponse(constant.UPDATE_FAILED, err.Error())
		}
		response = util.BuildSucResponse("", map[string]interface{}{"status": "ok"})
		c.Rsp=response
		return
	}
	service := &services.T_loan_batch_contract_trans_meterServiceImpl{}
	c.Rsp = util.CallDts(constant.UpdateT_loan_batch_contract_trans_meter+constant.Fomif2, service, services.UpdateT_loan_batch_contract_trans_meterCompensable, cols, c.DTSCtx)
}
