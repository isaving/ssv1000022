package main

import (
	"encoding/json"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/legobankda/inteloan/event_routers"
	_ "git.forms.io/legobankda/inteloan/routers"
	"git.forms.io/universe/solapp-sdk/config"

	//"git.forms.io/legobankil/legoapp/config"
	"git.forms.io/universe/common/event_handler"
	"git.forms.io/universe/common/event_handler/base"
	"git.forms.io/universe/solapp-sdk/heartbeat"
	_ "git.forms.io/universe/solapp-sdk/heartbeat"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	"github.com/urfave/cli"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var (
	APP_NAME = "inteloan"
	VERSION  = "v0.0.1-dev"
)

func Init() error {
	// init log
	if err := log.InitLog(); err != nil {
		log.Errorf("InitLog failed err=%v", err)
		return err
	}

	//init Solapp
	if err := config.InitSolappConfig(); err != nil {
		log.Errorf("Init solapp config failed, err=%v", err)
		return err
	}

	////init service parameter
	//if err := config.InitServiceConfig(); err != nil {
	//	log.Errorf("init service config failed err: %v.", err)
	//	return err
	//}

	// init Database
	if err := dao.InitDatabase(); err != nil {
		log.Errorf("InitDatabase failed err=%v", err)
		return err
	}

	// enable DTS support
	//if err := imports.EnableDTSSupports(); err != nil {
	//	log.Errorf("Enable DTS supports failed err=%v", err)
	//	return err
	//}

	// init EventRouter
	if err := event_routers.InitEventRouters(); err != nil {
		log.Errorf("Deposit event_Routers failed err=%v", err)
		return err
	}

	// start model receiver
	if err := event_handler.InitEventReceiver(); err != nil {
		log.Errorf("init event receiver failed err: %v.", err)
		return err
	}

	// start heartbeat
	heartbeat.StartHeartbeat()

	return nil
}

func apiNotFound(rw http.ResponseWriter, r *http.Request) {
	output := base.BuildErrorResponse("API not found!")
	res, _ := json.Marshal(output)
	rw.Write(res)
}

func runBeego(c *cli.Context) error {
	// init
	if err := Init(); err != nil {
		log.Errorf("main init failed err=%v", err)
		return err
	}
	beego.ErrorHandler("404", apiNotFound)
	beego.Run()
	return nil
}

func newApp(appName string, version string) *cli.App {
	app := cli.NewApp()
	app.Version = version
	app.Usage = appName
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "conf",
			Usage:  "Basic application configuration",
			EnvVar: "APP_CONF",
			Value:  "conf/app.conf",
		},
	}

	app.Action = runBeego
	app.Commands = []cli.Command{}

	return app
}

func main() {
	app := newApp(APP_NAME, VERSION)

	/*	if beego.BConfig.RunMode == "dev" {
			beego.BConfig.WebConfig.DirectoryIndex = true
			beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
		}
	*/

	beego.BConfig.WebConfig.DirectoryIndex = true
	beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"

	// run beego
	go func() {
		if err := app.Run(os.Args); err != nil {
			log.Errorf("Run App error %s", err.Error())
		}
	}()

	// heartbeat
	//heartbeat.NewHeartbeatServiceAndRun()

	signals := make(chan (os.Signal), 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	signal := <-signals

	log.Infof("Received signal %s and exist", signal.String())

	return
}
