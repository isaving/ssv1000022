package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intrt_plan struct {
	IntacrRuleNo           string  `orm:"column(intacr_rule_no);pk" description:"计息规则编号  计划号组合规则利息计划类型+利息计划种类+序号1,利息计划类型'TA-存款活期,TM-存款定期,LA-贷款'2,利息计划种类001-正常利率,002-逾期利率,003-提前支取利率,004-复利利率,005-活期分层利率,006-剩余金额利率,007-贷款正常本金利率008=贷款逾期本金利率009-贷款复利利率计划号组合规则计息规则类型+计息规则种类+序号1,计息规则类型'TA-存款活期,TM-存款定期,LA-贷款'2,计息规则种类001-正常利率,002-逾期利率,003-提前支取利率,004-复利利率,005-活期分层利率,006-剩余金额利率,007-贷款正常本金利率008=贷款逾期本金利率009-贷款复利利率"`
	OrgNo                  string  `orm:"column(org_no);size(4);null" description:"机构号"`
	CurCd                  string  `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	IntacrPrtyTypCd        string  `orm:"column(intacr_prty_typ_cd);size(2);null" description:"计息优先类型代码  A:固定,C:利率变化优先,B:结息日定期机动,D:年初调整,E:贷放日重定价,F:自定义周期调整"`
	IntacrAgrthmTypCd      string  `orm:"column(intacr_agrthm_typ_cd);size(2);null" description:"计息算法类型代码  D-实际天数算法Y-对年对月对日算法Z-对年对日算法"`
	LowestIntStlAmt        float64 `orm:"column(lowest_int_stl_amt);null;digits(18);decimals(2)" description:"最低结息金额"`
	LyrdFlg                string  `orm:"column(lyrd_flg);size(1);null" description:"分层标志  0否1是"`
	AmtHrchTypCd           string  `orm:"column(amt_hrch_typ_cd);size(2);null" description:"金额层级类型代码  L,–分量层级A,–总量层级"`
	DeadlHrchTypCd         string  `orm:"column(deadl_hrch_typ_cd);size(2);null" description:"期限层级类型代码  L,–分量层级A,–总量层级"`
	DeadlAmtPrtyTypCd      string  `orm:"column(deadl_amt_prty_typ_cd);size(2);null" description:"期限金额优先类型代码  0-期限优先,1-金额优先,-期限优先:当层级上同时使用金额与期限时,优先考虑金额,金额优先:当层级上同时使用金额与期限时,优先考虑期限.只有存款使用"`
	UseIntrtSorcCd         string  `orm:"column(use_intrt_sorc_cd);size(2);null" description:"使用利率来源代码  1-编号利率,'利率平台发布的利率'2-固定利率,'协议利率'3-参考其他计息规则"`
	IntrtNo                string  `orm:"column(intrt_no);size(40);null" description:"利率编号  利息使用标识为1-编号利率时,必须输入,如有分层,则优先使用分层利率,"`
	FixdIntrt              float64 `orm:"column(fixd_intrt);null;digits(9);decimals(6)" description:"固定利率  利息使用标识为2-固定利率时,必须输入,如有分层,则优先使用分层利率,"`
	LowestIntrt            float64 `orm:"column(lowest_intrt);null;digits(9);decimals(6)" description:"最低利率  表示该产品的最低执行利率"`
	MonIntacrDaysCd        string  `orm:"column(mon_intacr_days_cd);size(2);null" description:"月计息天数代码  A-30天/月,C-月实际天数,-"`
	YrIntacrDaysCd         string  `orm:"column(yr_intacr_days_cd);size(2);null" description:"年计息天数代码  1-360,2-365,3-年实际天数,-"`
	IntrtFlotManrCd        string  `orm:"column(intrt_flot_manr_cd);size(2);null" description:"利率浮动方式代码  P-浮动百分比,N-浮动点数记录利率浮动方式,如:按上限浮动,按下限浮动,按浮动比例浮动,等,"`
	FlotDrctCd             string  `orm:"column(flot_drct_cd);size(2);null" description:"浮动方向代码  D-下浮,U-上浮,-"`
	IntrtFlotCeilVal       float64 `orm:"column(intrt_flot_ceil_val);null;digits(9);decimals(6)" description:"利率浮动上限值  选输,用于限定浮动的上限值,包括对层级浮动率的限制,参数值为“0”则表示不使用,"`
	IntrtFlotFloorVal      float64 `orm:"column(intrt_flot_floor_val);null;digits(9);decimals(6)" description:"利率浮动下限值  选输,用于限定浮动的下限值,包括对层级浮动率的限制,参数值为“0”则表示不使用,"`
	DfltFlotVal            float64 `orm:"column(dflt_flot_val);null;digits(12);decimals(8)" description:"默认浮动值  此参数用于设定该利息计划的利率是否可以浮动,一共有两个同名的参数,一个在利息计划中,一个在层级信息中,在不适用层级时,使用利息计划中的浮动选项,而存在层级信息时,以层级信息中的浮动选项为准,如果不需要在基准利率上浮动时,此参数设置为空,浮动选项都是基于固定利率或者利率编号的利率浮动,"`
	CntRoleBitPtpIntacrFlg string  `orm:"column(cnt_role_bit_ptp_intacr_flg);size(1);null" description:"分角位参与计息标志  0否1是"`
	CrtclDotDistClsfCd     string  `orm:"column(crtcl_dot_dist_clsf_cd);size(2);null" description:"临界点区间分类代码  0-包含,1-不包含,2-期限包含金额不包含,3-金额包含期限不包含,此参数界定其临界点应该属于哪一个区间,包含:则临界点属于上一层区间,不包含:则临界点属于下一层区间,,"`
	HrchIntrtAmtTypCd      string  `orm:"column(hrch_intrt_amt_typ_cd);size(2);null" description:"层级利率金额类型代码  0-发生额,1-日均余额,2-账户余额此参数标识按金额分层时是按哪种金额进行分层,层级标识为Y-分层,金额层级标识不为N,需要填写层级金额的类型,"`
	HrchTotlQty            int     `orm:"column(hrch_totl_qty);null" description:"层级总数量  层级标识为Y-分层,确定总层级数,"`
	LyrdDeadlUnitCd        string  `orm:"column(lyrd_deadl_unit_cd);size(2);null" description:"分层期限单位代码  I-分钟,O-小时,D-日,W-周,M-月,S-季,H-半年,Y-年,-"`
	IntrtCycCd             string  `orm:"column(intrt_cyc_cd);size(2);null" description:"利率周期代码  参考标准代码CD0075"`
	LoanIntrtAdjCycCd      string  `orm:"column(loan_intrt_adj_cyc_cd);size(2);null" description:"贷款利率调整周期代码  参考标准代码:CD0075D-日利率,W-周利率,M-月利率,S-季利率,H-半年利率,Y-年利率,"`
	LoanIntrtAdjCycQty     int     `orm:"column(loan_intrt_adj_cyc_qty);null" description:"贷款利率调整周期数量  大于0的整数"`
	IntacrRuleDescr        string  `orm:"column(intacr_rule_descr);size(200);null" description:"计息规则描述"`
	Flag1                  string  `orm:"column(flag1);size(2);null" description:"标志1"`
	Flag2                  string  `orm:"column(flag2);size(2);null" description:"标志2"`
	Bak1                   float64 `orm:"column(bak1);null;digits(18);decimals(6)" description:"备用1"`
	Bak2                   float64 `orm:"column(bak2);null;digits(18);decimals(6)" description:"备用2"`
	Bak3                   string  `orm:"column(bak3);size(10);null" description:"备用3"`
	NoIntrtEfftDt          string  `orm:"column(no_intrt_efft_dt);type(date);null" description:"编号利率生效日期"`
	FinlModfyDt            string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccKeprcdVisFlg        string  `orm:"column(tcc_keprcd_vis_flg);size(1);null" description:"tcc记录可见标志  0否1是"`
	TccState               int     `orm:"column(tcc_state);null"`
}

func (t *T_intrt_plan) TableName() string {
	return "t_intrt_plan"
}

func InsertT_intrt_plan(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intrt_plan).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intrt_planTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intrt_plan)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intrt_planTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intrt_plan where intacr_rule_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IntacrRuleNo"]).Exec()
	return err
}

func QueryT_intrt_plan(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intrt_plan panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intrt_plan))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intrt_planById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intrt_plan)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intrt_plan(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intrt_plan panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intrt_plan)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
