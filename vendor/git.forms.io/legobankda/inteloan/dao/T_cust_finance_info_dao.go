package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_cust_finance_info struct {
	CustId             string    `orm:"column(cust_id);pk" description:"客户号"`
	Education          string    `orm:"column(education);size(2);null" description:"学历"`
	JobStatus          string    `orm:"column(job_status);size(2);null" description:"职业状况"`
	Job                string    `orm:"column(job);size(50);null" description:"职业"`
	JobTitle           string    `orm:"column(job_title);size(50);null" description:"职称"`
	CompanyName        string    `orm:"column(company_name);size(320);null" description:"单位名称"`
	Department         string    `orm:"column(department);size(320);null" description:"部门"`
	CompanySize        string    `orm:"column(company_size);size(50);null" description:"公司规模"`
	JobClass           string    `orm:"column(job_class);size(50);null" description:"职务分类"`
	IndustryNature     string    `orm:"column(industry_nature);size(320);null" description:"行业性质"`
	Position           string    `orm:"column(position);size(50);null" description:"职务"`
	IndustryCode       string    `orm:"column(industry_code);size(10);null" description:"行业类别代码"`
	WorkYears          int       `orm:"column(work_years);null" description:"工龄"`
	Employer           string    `orm:"column(employer);size(100);null" description:"雇主"`
	EmployerAddr       string    `orm:"column(employer_addr);size(100);null" description:"雇主地址"`
	EmployerBussType   string    `orm:"column(employer_buss_type);size(50);null" description:"雇主业务类型"`
	WorkStartDate      string    `orm:"column(work_start_date);size(8);null" description:"工作开始日"`
	PayCurrency        string    `orm:"column(pay_currency);size(3);null" description:"薪资币种"`
	Salary             float64   `orm:"column(salary);null;digits(19);decimals(2)" description:"薪资"`
	AnnualIncome       float64   `orm:"column(annual_income);null;digits(19);decimals(2)" description:"年收入"`
	PayrollFreq        string    `orm:"column(payroll_freq);size(1);null" description:"薪资发放频率"`
	MonthlyIncome      float64   `orm:"column(monthly_income);null;digits(19);decimals(2)" description:"每月净收益"`
	MonthlyExpenditure float64   `orm:"column(monthly_expenditure);null;digits(19);decimals(2)" description:"每月净支出"`
	Remarks            string    `orm:"column(remarks);size(100);null" description:"备注"`
	HousingStatus      string    `orm:"column(housing_status);size(2);null" description:"住房状态"`
	HousingType        string    `orm:"column(housing_type);size(2);null" description:"住房类型"`
	StayOnDate         string    `orm:"column(stay_on_date);size(8);null" description:"入住DATE"`
	HousingValuation   float64   `orm:"column(housing_valuation);null;digits(19);decimals(2)" description:"住房估价"`
	HomeMortgageAmt    float64   `orm:"column(home_mortgage_amt);null;digits(19);decimals(2)" description:"住房抵押贷款金额"`
	LastMaintDate      string    `orm:"column(last_maint_date);type(date)" description:"最后更新日期"`
	LastMaintTime      string    `orm:"column(last_maint_time);type(datetime)" description:"最后更新时间"`
	LastMaintBrno      string    `orm:"column(last_maint_brno);size(20)" description:"最后更新机构"`
	LastMaintTell      string    `orm:"column(last_maint_tell);size(30)" description:"最后更新柜员"`
	TccState            int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_cust_finance_info) TableName() string {
	return "t_cust_finance_info"
}

func InsertT_cust_finance_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cust_finance_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cust_finance_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cust_finance_info)).Filter("cust_id", maps["CustId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cust_finance_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cust_finance_info where cust_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustId"]).Exec()
	return err
}

func QueryT_cust_finance_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cust_finance_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_cust_finance_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cust_finance_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cust_finance_info)).Filter("cust_id", maps["CustId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cust_finance_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cust_finance_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cust_finance_info)).Filter("cust_id", maps["CustId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
