package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_cust_repmt_day_info struct {
	KeprcdNo        string    `orm:"column(keprcd_no);pk" description:"记录编号   "`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	KeprcdStusCd    string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	MgmtOrgNo       string    `orm:"column(mgmt_org_no);size(4);null" description:"管理机构号  记录存款账户管理归属机构编号,引用自机构主题的机构编号,"`
	RepayDtChgFlg   string    `orm:"column(repay_dt_chg_flg);size(1);null" description:"还款日期变更标志  0否1是"`
	CrtDt           string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CtrtNo          string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	LoanDubilNo     string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	SpCycCd         string    `orm:"column(sp_cyc_cd);size(3);null" description:"结本周期代码  描述结本日周期单位:日,周,月,年,结算本金日期,T日,"`
	SpCycQty        int       `orm:"column(sp_cyc_qty);null" description:"结本周期数量  描述结算本金周期的次数,比如,按月付息,按年还本,那么,每个月结息一次,一年结本一次,"`
	IntStlDtCyc     string    `orm:"column(int_stl_dt_cyc);size(6);null" description:"结息日期周期"`
	IntStlCycQty    int       `orm:"column(int_stl_cyc_qty);null" description:"结息周期数量  描述结息日周期的次数,比如,按月付息,按年还本,那么,每个月结息一次,一年结本一次,"`
	IntStlDay       string    `orm:"column(int_stl_day);size(2);null" description:"结息日  '2位数字'"`
	SpDay           string    `orm:"column(sp_day);size(2);null" description:"结本日  '2位数字'"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int       `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_cust_repmt_day_info) TableName() string {
	return "t_cust_repmt_day_info"
}

func InsertT_cust_repmt_day_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cust_repmt_day_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cust_repmt_day_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cust_repmt_day_info)).Filter("keprcd_no", maps["KeprcdNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cust_repmt_day_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cust_repmt_day_info where keprcd_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_cust_repmt_day_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cust_repmt_day_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params,"Flag")
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_cust_repmt_day_info))
	if flag == "8"{			//条件是或的
		cond := orm.NewCondition()
		for k, v := range params {
			if k == "CustNo" && v !=nil  {
				cond=cond .Or("cust_no",v)
			}
			if k == "LoanProdtNo" && v !=nil  {
				cond=cond .Or("loan_prodt_no",v)
			}
			if k == "CtrtNo" && v !=nil  {
				cond=cond .Or("ctrt_no",v)
			}
		}
		cond = cond.And("tcc_state", state)
		if _, err = qs.SetCond(cond).Limit(-1).OrderBy("-crt_dt","-finl_modfy_dt","-finl_modfy_tm").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag != nil && flag =="9" {
		if _, err = qs.Filter("tcc_state", state).Limit(1).OrderBy("-crt_dt").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cust_repmt_day_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cust_repmt_day_info)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cust_repmt_day_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cust_repmt_day_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cust_repmt_day_info)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
