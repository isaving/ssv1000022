package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_cust_intr_info struct {
	AplyBizNo              string    `orm:"column(aply_biz_no);pk" description:"申请业务编号"`
	CustNo                 string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	IndvCrtfTypCd          string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码"`
	IndvCrtfNo             string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	LoanProdtNo            string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo        string    `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	ExecIntrt              float64   `orm:"column(exec_intrt);size(40);null" description:"执行利率"`
	SpcyBnchmkIntrt        float64   `orm:"column(spcy_bnchmk_intrt);null;digits(9);decimals(6)" description:"指定基准利率"`
	BaseIntrtTypCd         string    `orm:"column(base_intrt_typ_cd);size(10);null" description:"基础利率类型代码  LPR-贷款市场报价利率CNYL-人行贷款基准利率CNYHFL-住房公积金贷款利率SHIBOR-上海银行间同业拆借利率"`
	IntrtTypCd             string    `orm:"column(intrt_typ_cd);size(1);null" description:"利率类型代码"`
	IntrtNo                string    `orm:"column(intrt_no);size(40);null" description:"利率编号"`
	IntrtFlotDrctCd        string    `orm:"column(intrt_flot_drct_cd);size(1);null" description:"利率浮动方向代码"`
	BpFlotVal              float64   `orm:"column(bp_flot_val);null;digits(9);decimals(2)" description:"bp浮动值"`
	IntrtFlotRatio         float64   `orm:"column(intrt_flot_ratio);null;digits(9);decimals(6)" description:"利率浮动比例"`
	KeprcdStusCd           string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效,09-无效"`
	FilesTrmCd             string    `orm:"column(files_trm_cd);size(2);null" description:"档期代码  1Y,,3Y,,,5Y,,1M,,3M,,6M,,12M,,18M,,24M,,36M,,,48M,,60M,等待补充码值"`
	FixdIntrtPricingManrCd string    `orm:"column(fixd_intrt_pricing_manr_cd);size(2);null" description:"固定利率定价方式代码  1-指定利率基准值2-基于基准利率类型匹配期限,3-基于指定基准利率"`
	IntrtFlotManrCd        string    `orm:"column(intrt_flot_manr_cd);size(2);null" description:"利率浮动方式代码  P-浮动百分比,N-浮动点数记录利率浮动方式,如:按上限浮动,按下限浮动,按浮动比例浮动,等,"`
	FinlModfyDt            string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_cust_intr_info) TableName() string {
	return "t_cust_intr_info"
}

func InsertT_cust_intr_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cust_intr_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cust_intr_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cust_intr_info)).Filter("aply_biz_no", maps["AplyBizNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cust_intr_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cust_intr_info where aply_biz_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AplyBizNo"]).Exec()
	return err
}

func QueryT_cust_intr_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cust_intr_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_cust_intr_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9"{
		if _, err = qs.Filter("tcc_state", state).OrderBy("-aply_biz_no").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cust_intr_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cust_intr_info)).Filter("aply_biz_no", maps["AplyBizNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cust_intr_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cust_intr_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cust_intr_info)).Filter("aply_biz_no", maps["AplyBizNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
