package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_contract_auth struct {
	MobileNo            string    `orm:"column(mobile_no);size(11);pk" description:"手机号码"`
	SeqNo               int       `orm:"column(seq_no)" description:"序号"`
	CustNo              string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName            string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	SgnOrgNo            string    `orm:"column(sgn_org_no);size(4);null" description:"签署机构号"`
	FileSgnNo           string    `orm:"column(file_sgn_no);size(20);null" description:"文件签署编号"`
	TmplNo              string    `orm:"column(tmpl_no);size(32);null" description:"模板编号"`
	AuthTypCd           string    `orm:"column(auth_typ_cd);size(2);null" description:"授权类型代码  01-授信额度合同02-个人征信授权书03-个人信息查询及使用授权书04-公积金授权书05-涉税信息授权委托书06-其他"`
	AuthQuryStusCd      string    `orm:"column(auth_qury_stus_cd);size(2);null" description:"授权查询状态代码  1-签署成功2-签署失败"`
	AuthDt              string    `orm:"column(auth_dt);type(date);null" description:"授权日期"`
	AuthMatrDt          string    `orm:"column(auth_matr_dt);type(date);null" description:"授权到期日期"`
	DocMgmtMainTablPkNo int       `orm:"column(doc_mgmt_main_tabl_pk_no);null" description:"文档管理主表主键编号"`
	FinlModfyDt         string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState            int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_contract_auth) TableName() string {
	return "t_contract_auth"
}

func InsertT_contract_auth(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_contract_auth where mobile_no=?"
		o.Raw(sql, maps["MobileNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_contract_auth).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_contract_authTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_contract_auth where mobile_no=?"
		o.Raw(sql, maps["MobileNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_contract_auth)).Filter("mobile_no", maps["MobileNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_contract_authTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_contract_auth where mobile_no=?"
		o.Raw(sql, maps["MobileNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_contract_auth where mobile_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["MobileNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_contract_auth(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_contract_auth panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_contract_auth))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_contract_authById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_contract_auth)).Filter("mobile_no", maps["MobileNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_contract_auth(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_contract_auth panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_contract_auth)).Filter("mobile_no", maps["MobileNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
