package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
)

type T_risk_cls_adj_rec struct {
	LoanDubilNo                      string  `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	AdjDt                            string  `orm:"column(adj_dt);type(date)" description:"调整日期  PK"`
	SeqNo                            int     `orm:"column(seq_no)" description:"序号"`
	LoanRiskClsfAdjPrin              float64 `orm:"column(loan_risk_clsf_adj_prin);null;digits(18);decimals(2)" description:"贷款风险分类调整本金"`
	CustNo                           string  `orm:"column(cust_no);size(14);null" description:"客户编号"`
	AdjManrCd                        string  `orm:"column(adj_manr_cd);size(1);null" description:"调整方式代码  0-系统自动调整1-人工调整"`
	AdjPersonEmpnbr                  string  `orm:"column(adj_person_empnbr);size(6);null" description:"调整人员工号  人工调整时"`
	AdjPersonOrgNo                   string  `orm:"column(adj_person_org_no);size(4);null" description:"调整人机构号  人工调整时"`
	AdjBefLoanRiskClsfCd             string  `orm:"column(adj_bef_loan_risk_clsf_cd);size(2);null" description:"调整前贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	AdjAftLoanRiskClsfCd             string  `orm:"column(adj_aft_loan_risk_clsf_cd);size(2);null" description:"调整后贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	CurtprdCompEtimLnRiskClsfCd      string  `orm:"column(curtprd_comp_etim_ln_risk_clsf_cd);size(2);null" description:"本期机评贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	InendIdtfyLoanRiskClsfDt         string  `orm:"column(inend_idtfy_loan_risk_clsf_dt);type(date);null" description:"最终认定贷款风险分类日期  "`
	InendIdtfyLoanRiskClsCd          string  `orm:"column(inend_idtfy_loan_risk_clsf_cd);size(2);null" description:"最终认定贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	CurtprdCompEtimLnRiskClsfDt      string  `orm:"column(curtprd_comp_etim_ln_risk_clsf_dt);type(date);null" description:"本期机器贷款风险分类日期"`
	CurtprdArtgclIdtfyLoanRiskClsfCd string  `orm:"column(curtprd_artgcl_idtfy_loan_risk_clsf_cd);size(2);null" description:"本期人工认定贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	CurtprdArtgclIdtfyLoanRiskClsfDt string  `orm:"column(curtprd_artgcl_idtfy_loan_risk_clsf_dt);type(date);null" description:"本期人工认定贷款风险分类日期"`
	LoanRiskClsfStusCd   			 string  `orm:"column(loan_risk_clsf_stus_cd);size(1);null" description:"风险分类申请状态 1 审批中 2 审批成功 3 审批失败"`
	SugstnDescr		       	  		 string  `orm:"column(sugstn_descr);size(200);null" description:"人工调整意见描述"`
	FinlModfyDt                      string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                      string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo                   string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo                  string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                         int     `orm:"column(tcc_state);null"`
}

func (t *T_risk_cls_adj_rec) TableName() string {
	return "t_risk_cls_adj_rec"
}

func InsertT_risk_cls_adj_rec(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_risk_cls_adj_rec where loan_dubil_no=? and adj_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_risk_cls_adj_rec).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_risk_cls_adj_recTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_risk_cls_adj_rec where loan_dubil_no=? and adj_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_risk_cls_adj_rec)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("adj_dt", maps["AdjDt"]).Filter("seq_no", maps["SeqNo"]).Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_risk_cls_adj_recTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_risk_cls_adj_rec where loan_dubil_no=? and adj_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_risk_cls_adj_rec where loan_dubil_no=? and adj_dt=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_risk_cls_adj_rec(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_risk_cls_adj_rec panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_risk_cls_adj_rec))
	flag := params["Flag"]
	delete(params, "Flag")
	if flag == "8" {
		maps, err = QueryT_risk_cls_adj_rec1(params, 0)
		return
	} else if flag == "9" {
		if _, err = qs.Filter("tcc_state", state).Filter("loan_dubil_no", params["LoanDubilNo"]).OrderBy("-adj_dt").Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag == "7" { //lxf--，这张表需要传个借据号，取最近的调整日期，序号最大」
		if _, err = qs.Filter("tcc_state", state).Filter("loan_dubil_no", params["LoanDubilNo"]).OrderBy("-adj_dt", "-seq_no").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}

	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_risk_cls_adj_recById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_risk_cls_adj_rec)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("adj_dt", maps["AdjDt"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_risk_cls_adj_rec(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_risk_cls_adj_rec panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_risk_cls_adj_rec)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("adj_dt", maps["AdjDt"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_risk_cls_adj_rec1(params orm.Params, state int8) (maps []orm.Params, err error) {
	sql := strings.Builder{}
	sql.WriteString("SELECT t1.`adj_person_org_no` OrgNo,t1.`adj_person_empnbr` Empnbr,t1.`inend_idtfy_loan_risk_clsf_cd` InendIdtfyLoanRiskClsCd,t2.`loan_prodt_no` LoanProdtNo,SUM(t2.`loan_amt`),COUNT(0) ")
	sql.WriteString("FROM `t_risk_cls_adj_rec` t1 LEFT JOIN t_loan_loan_cont t2 ON t1.`loan_dubil_no`=t2.`loan_dubil_no` ")
	sql.WriteString("WHERE t1.`adj_person_org_no` IS NOT NULL AND t1.`adj_person_empnbr` IS NOT NULL AND t1.`inend_idtfy_loan_risk_clsf_cd` IN (1,2,3,4,5) AND t1.tcc_state=0 AND t2.tcc_state=0 ")
	OrgNo := params["OrgNo"]
	Empnbr := params["Empnbr"]
	LoanProdtNo := params["LoanProdtNo"]
	CountStartDate := params["CountStartDate"]
	CountEndDate := params["CountEndDate"]
	if OrgNo != nil {
		sql.WriteString(" AND t1.`adj_person_org_no`=\"" + OrgNo.(string) + "\"")
	}
	if Empnbr != nil {
		sql.WriteString(" AND t1.`adj_person_empnbr`=\"" + Empnbr.(string) + "\"")
	}
	if LoanProdtNo != nil {
		sql.WriteString(" AND t2.`loan_prodt_no`>=\"" + LoanProdtNo.(string) + "\"")
	}
	if CountStartDate != nil {
		sql.WriteString(" AND t1.adj_dt>=\"" + CountStartDate.(string) + "\"")
	}
	if CountEndDate != nil {
		sql.WriteString(" AND t1.adj_dt<=\"" + CountEndDate.(string) + "\"")
	}
	sql.WriteString(" GROUP BY t1.inend_idtfy_loan_risk_clsf_cd,t1.`adj_person_org_no`,t1.`adj_person_empnbr`,t1.`inend_idtfy_loan_risk_clsf_cd`")
	o := orm.NewOrm()
	_, err = o.Raw(sql.String()).Values(&maps)

	if err != nil {
		return nil, err
	}
	return
}
