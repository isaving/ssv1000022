package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_daycut struct {
	SoftProdtCd     string `orm:"column(soft_prodt_cd);pk" description:"软件产品编码  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'"`
	CurrAcctnDt     string `orm:"column(curr_acctn_dt);type(date);null" description:"当前会计日期  "`
	LstoneAcctnDt   string `orm:"column(lstone_acctn_dt);type(date);null" description:"上一会计日期"`
	NxtoneAcctnDt   string `orm:"column(nxtone_acctn_dt);type(date);null" description:"下一会计日期"`
	AcctnDayFlgCd   string `orm:"column(acctn_day_flg_cd);size(1);null" description:"会计日标志代码  Y-已开始营业,N-已結束营业,,"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int    `orm:"column(tcc_state);null"`
}

func (t *T_ae_daycut) TableName() string {
	return "t_ae_daycut"
}

func InsertT_ae_daycut(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_daycut).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_daycutTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_daycut)).Filter("soft_prodt_cd", maps["SoftProdtCd"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_daycutTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_daycut where soft_prodt_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["SoftProdtCd"]).Exec()
	return err
}

func QueryT_ae_daycut(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_daycut panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_daycut))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_daycutById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_daycut)).Filter("soft_prodt_cd", maps["SoftProdtCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_daycut(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_daycut panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_daycut)).Filter("soft_prodt_cd", maps["SoftProdtCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
