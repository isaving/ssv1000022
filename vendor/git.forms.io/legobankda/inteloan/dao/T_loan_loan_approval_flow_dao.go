package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_loan_approval_flow struct {
	MakelnAplySn            string `orm:"column(makeln_aply_sn);size(32);pk" description:"放款申请流水号"`
	SeqNo                   int    `orm:"column(seq_no)" description:"序号  10 pk"`
	CustNo                  string `orm:"column(cust_no);size(14);null" description:"客户编号"`
	NodeNm                  string `orm:"column(node_nm);size(120);null" description:"节点名称  20 "`
	TxSn                    string `orm:"column(tx_sn);size(32);null" description:"交易流水号 "`
	ApprvNodeTypCd          string `orm:"column(apprv_node_typ_cd);size(1);null" description:"审批节点类型代码  智能决策人工审批"`
	OperPersonEmpnbr        string `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号  公共服务提供类型及长度"`
	OperOrgNo               string `orm:"column(oper_org_no);size(4);null" description:"操作机构号  公共服务提供类型及长度"`
	OperTm                  string `orm:"column(oper_tm);type(time);null" description:"操作时间 "`
	MakelnApprvOperResultCd string `orm:"column(makeln_apprv_oper_result_cd);size(1);null" description:"放款审批操作结果代码  agree-同意 reject-不同意 reback-退档 20"`
	AfltInfo                string `orm:"column(aflt_info);size(200);null" description:"附属信息 "`
	FinlModfyDt             string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期 "`
	FinlModfyTm             string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间 "`
	FinlModfyOrgNo          string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号 "`
	FinlModfyTelrNo         string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号 "`
	TccState                int    `orm:"column(tcc_state);null"`
}

func (t *T_loan_loan_approval_flow) TableName() string {
	return "t_loan_loan_approval_flow"
}

func InsertT_loan_loan_approval_flow(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_loan_approval_flow where makeln_aply_sn=?"
		o.Raw(sql, maps["MakelnAplySn"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_loan_approval_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_loan_approval_flowTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_loan_approval_flow where makeln_aply_sn=?"
		o.Raw(sql, maps["MakelnAplySn"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_loan_approval_flow)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_loan_approval_flowTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_loan_approval_flow where makeln_aply_sn=?"
		o.Raw(sql, maps["MakelnAplySn"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_loan_approval_flow where makeln_aply_sn=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["MakelnAplySn"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_loan_approval_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_loan_approval_flow panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params, "Flag")
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_loan_approval_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9" { //查找序号最大的一条
		if _, err = qs.Filter("makeln_aply_sn", params["MakelnAplySn"]).Filter("cust_no", params["CustNo"]).
			Filter("node_nm", params["NodeNm"]).Filter("tcc_state", state).OrderBy("-seq_no").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_loan_approval_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_loan_approval_flow)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_loan_approval_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_loan_approval_flow panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_loan_approval_flow)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

