package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_link_acct struct {
	RelaAcctId                string    `orm:"column(rela_acct_id);pk" description:"关联账户id  这是一个编号,技术字段,为了唯一标识"`
	LoanProdtNo               string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	CtrtNo                    string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	RelaAcctBlngtoRwCategCd   string    `orm:"column(rela_acct_blngto_rw_categ_cd);size(6);null" description:"关联账户所属行类别代码  1-本法人机构账户2-联社非本法人机构账户3-他行账户"`
	CustNo                    string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	RelaAcctNoTypCd           string    `orm:"column(rela_acct_no_typ_cd);size(2);null" description:"关联账号类型代码  01-借记卡02-存折"`
	DfltRepayAcctFlg          string    `orm:"column(dflt_repay_acct_flg);size(1);null" description:"默认还款账户标志  0-否,1-是"`
	LoanDubilNo               string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	RelaAcctNo                string    `orm:"column(rela_acct_no);size(32);null" description:"关联账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	IndvCrtfTypCd             string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo                string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	RelaDt                    string    `orm:"column(rela_dt);type(date);null" description:"关联日期"`
	KeprcdStusCd              string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效,09-无效"`
	RelaAcctOpnAcctBnkBnkNo   string    `orm:"column(rela_acct_opn_acct_bnk_bnk_no);size(14);null" description:"关联账户开户行行号"`
	RelaAcctOpnAcctBnkBnkName string    `orm:"column(rela_acct_opn_acct_bnk_bnk_name);size(70);null" description:"关联账户开户行行名"`
	FinlModfyDt               string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm               string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo            string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo           string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState            	  int       `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_link_acct) TableName() string {
	return "t_loan_link_acct"
}

func InsertT_loan_link_acct(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_link_acct).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_link_acctTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_link_acct)).Filter("rela_acct_id", maps["RelaAcctId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_link_acctTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_link_acct where rela_acct_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["RelaAcctId"]).Exec()
	return err
}

func QueryT_loan_link_acct(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_link_acct panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_link_acct))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_link_acctById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_link_acct)).Filter("rela_acct_id", maps["RelaAcctId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_link_acct(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_link_acct panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_link_acct)).Filter("rela_acct_id", maps["RelaAcctId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
