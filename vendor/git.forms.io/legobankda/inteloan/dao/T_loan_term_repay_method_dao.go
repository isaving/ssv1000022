package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_term_repay_method struct {
	KeprcdNo        string    `orm:"column(keprcd_no);pk;size(32)" description:"授信申请流水号"`
	CustNo            string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	LoanProdtNo       string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号"`
	BrwmnyDeadl       string    `orm:"column(brwmny_deadl);size(6);null" description:"借款期限"`
	BrwmnyDeadlUnitCd string    `orm:"column(brwmny_deadl_unit_cd);size(3);null" description:"借款期限单位代码"`
	RepayManrCd       string    `orm:"column(repay_manr_cd);size(3);null" description:"还款方式代码  参考标准代码:CD0062标准基础上新增:51-按月付息按季还本52-按月付息按半年还本53-按月付息按年还本"`
	KeprcdStusCd      string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_term_repay_method) TableName() string {
	return "t_loan_term_repay_method"
}

func InsertT_loan_term_repay_method(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_term_repay_method).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_term_repay_methodTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_term_repay_method)).Filter("keprcd_no", maps["KeprcdNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_term_repay_methodTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_term_repay_method where keprcd_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_loan_term_repay_method(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_term_repay_method panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_term_repay_method))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_term_repay_methodById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_term_repay_method)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_term_repay_method(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_term_repay_method panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_term_repay_method)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
