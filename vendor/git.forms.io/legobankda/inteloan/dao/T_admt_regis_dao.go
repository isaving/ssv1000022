package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_admt_regis struct {
	LoanDubilNo     string    `orm:"column(loan_dubil_no);pk;size(32)" description:"贷款借据号"`
	SeqNo           int       `orm:"column(seq_no)" description:"序号"`
	DvalBizAplyNo   string    `orm:"column(dval_biz_aply_no);size(20);null" description:"提前到期业务申请编号"`
	DvalDlwthStusCd string    `orm:"column(dval_dlwth_stus_cd);size(2);null" description:"提前到期状态代码 1-待审批 2-审批通过 3-审批拒绝"`
	CtrtNo          string    `orm:"column(ctrt_no);size(32);null" description:"合同编号"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName        string    `orm:"column(cust_name);size(60);null" description:"客户名称"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号"`
	LoanProdtVersNo string    `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号"`
	LoanProdtNm     string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	GrantDt         string    `orm:"column(grant_dt);type(date);null" description:"发放日期"`
	MatrDt          string    `orm:"column(matr_dt);type(date);null" description:"到期日期"`
	VersNo          int       `orm:"column(vers_no);null" description:"还款计划版本号"`
	AdvMatrDt       string    `orm:"column(adv_matr_dt);type(date);null" description:"提前到期日期"`
	AdvMatrReason   string    `orm:"column(adv_matr_reason);size(200);null" description:"提前到期原因"`
	AuthOrgNo       string    `orm:"column(auth_org_no);size(6);null" description:"审批机构编号"`
	AuthEmpnbr      string    `orm:"column(auth_empnbr);size(6);null" description:"审批员工编号"`
	AplyOrgNo       string    `orm:"column(aply_org_no);size(4);null" description:"经办机构编号"`
	AplyEmpnbr      string    `orm:"column(aply_empnbr);size(6);null" description:"经办员工编号"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string    `orm:"column(Finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int       `orm:"column(tcc_state);null"`
}

func (t *T_admt_regis) TableName() string {
	return "t_admt_regis"
}

func InsertT_admt_regis(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo,MAX(vers_no)+1 VersNo from t_admt_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		Ver := mapOne[0]["VersNo"]
		var SeqNo , VersNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo

		if Ver == nil {
			VersNo = 1
		} else {
			VersNo, _ = strconv.Atoi(Ver.(string))
		}
		maps["VersNo"] = VersNo
	}
	sql := util.BuildSql(maps, new(T_admt_regis).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_admt_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_admt_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_admt_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_admt_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_admt_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_admt_regis where loan_dubil_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_admt_regis(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_admt_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_admt_regis))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9"{	//xml
		if _, err = qs.Filter("tcc_state", state).OrderBy("-seq_no").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag == "8" { //lzh客户在贷款合约主表最高的那边贷款记录
		maps, err = QueryT_admt_regisFlag8(params)
		return
	}
	if flag == "7"{	//tc
		if _, err = qs.Filter("tcc_state", state).OrderBy("-seq_no").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_admt_regisById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_admt_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_admt_regis(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_admt_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_admt_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_admt_regisFlag8(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  "+
		"t1.dval_biz_aply_no     WrtoffBizAplyNo, "+
		"t1.loan_dubil_no  LoanDubilNo, "+
		"t1.dval_dlwth_stus_cd   StusCd,"+
		"t1.cust_no   CustNo, "+
		"t1.adv_matr_dt    AdvMatrDt, "+
		"t2.cust_name    CustNm, "+
		"t2.indv_crtf_typ_cd    IndvCrtfTypCd, "+
		"t2.indv_crtf_no    IndvCrtfNo "+
		"FROM (t_admt_regis t1, t_loan_cust_base_info t2) "+
		"WHERE  t1.cust_no=t2.cust_no AND t1.cust_name=t2.cust_name "
	if maps["LoanDubilNo"] != nil {
		sql = sql+" and t1.loan_dubil_no='"+maps["LoanDubilNo"].(string)+"'"
	}
	if maps["CustNo"] != nil {
		sql = sql+" and t1.cust_no='"+maps["CustNo"].(string)+"'"
	}
	if maps["CustNm"] != nil {
		sql = sql+" and t1.cust_name='"+maps["CustNm"].(string)+"'"
	}
	if maps["IndvCrtfNo"] != nil {
		sql = sql+" and t2.indv_crtf_no='"+maps["IndvCrtfNo"].(string)+"'"
	}
	if maps["IndvCrtfTypCd"] != nil {
		sql = sql+" and t2.indv_crtf_typ_cd='"+maps["IndvCrtfTypCd"].(string)+"'"
	}
	if maps["DvalDlwthStusCd"] != nil {
		sql = sql+" and t1.dval_dlwth_stus_cd='"+maps["DvalDlwthStusCd"].(string)+"'"
	}
	sql = sql +"  group by t1.dval_biz_aply_no"
	_, err = o.Raw(sql).Values(&mapList)
	return
}