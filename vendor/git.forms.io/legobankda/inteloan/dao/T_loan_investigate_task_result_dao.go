package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_investigate_task_result struct {
	ArtgclInvstgTaskNo        string    `orm:"column(artgcl_invstg_task_no);size(60);pk" description:"人工调查任务编号  PK"`
	SeqNo                     int       `orm:"column(seq_no)" description:"序号"`
	CustNo                    string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	ReacCustTypCd             string    `orm:"column(reac_cust_typ_cd);size(1);null" description:"触达客户类型代码  1-现场收集2-远程收集"`
	ReacCustTms               int       `orm:"column(reac_cust_tms);null" description:"触达客户次数"`
	KeprcdStusCd              string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	PutawyGatherResultCd      string    `orm:"column(putawy_gather_result_cd);size(1);null" description:"收集结果代码  1-已收集2-未收集"`
	CotactTelNo               string    `orm:"column(cotact_tel_no);size(20);null" description:"联系电话号码"`
	CurrAddr                  string    `orm:"column(curr_addr);size(200);null" description:"当前地址"`
	Lgtd                      float64   `orm:"column(lgtd);null;digits(11);decimals(8)" description:"经度"`
	LgtdDrctCd                string    `orm:"column(lgtd_drct_cd);size(1);null" description:"经度方向代码  E-东W-西"`
	Lttd                      float64   `orm:"column(lttd);null;digits(11);decimals(8)" description:"纬度"`
	LttdDrctCd                string    `orm:"column(lttd_drct_cd);size(1);null" description:"纬度方向代码  N-北S-南"`
	RsdnceDtlAddr             string    `orm:"column(rsdnce_dtl_addr);size(200);null" description:"居住地详细地址"`
	RsdnceAddr                string    `orm:"column(rsdnce_addr);size(200);null" description:"居住地地址"`
	CmunicDtlAddr             string    `orm:"column(cmunic_dtl_addr);size(200);null" description:"通讯详细地址"`
	CmunicAddr                string    `orm:"column(cmunic_addr);size(200);null" description:"通讯地址"`
	WechatNo                  string    `orm:"column(wechat_no);size(120);null" description:"微信号码"`
	QqNo                      string    `orm:"column(qq_no);size(20);null" description:"qq号码"`
	IndusTypCd                string    `orm:"column(indus_typ_cd);size(5);null" description:"行业类型代码"`
	CareerTypCd               string    `orm:"column(career_typ_cd);size(5);null" description:"职业类型代码"`
	WorkUnitNm                string    `orm:"column(work_unit_nm);size(120);null" description:"工作单位名称"`
	DutyCd                    string    `orm:"column(duty_cd);size(5);null" description:"职务代码"`
	CmtdInfo                  string    `orm:"column(cmtd_info);size(200);null" description:"承诺信息"`
	SponsorCustMgrEmpnbr      string    `orm:"column(sponsor_cust_mgr_empnbr);size(6);null" description:"主办客户经理员工号"`
	SponsorOrgNo              string    `orm:"column(sponsor_org_no);size(4);null" description:"主办机构号"`
	CorgCustMgrEmpnbr         string    `orm:"column(corg_cust_mgr_empnbr);size(6);null" description:"协办客户经理员工号"`
	CorgOrgNo                 string    `orm:"column(corg_org_no);size(4);null" description:"协办机构号"`
	DlwthDt                   string    `orm:"column(dlwth_dt);type(date);null" description:"处理日期"`
	InfoVrfyTypCd             string    `orm:"column(info_vrfy_typ_cd);size(1);null" description:"信息核实类型代码  等待补充码值"`
	IndvAcctBal               float64   `orm:"column(indv_acct_bal);null;digits(18);decimals(2)" description:"个人账户余额"`
	SavedCrdnlnbr             float64   `orm:"column(saved_crdnlnbr);null;digits(18);decimals(2)" description:"缴存基数"`
	PaytYrmon                 string    `orm:"column(payt_yrmon);size(6);null" description:"缴至年月"`
	IndvAcctStusCd            string    `orm:"column(indv_acct_stus_cd);size(5);null" description:"个人账户状态代码  等待补充码值"`
	RsdnceProvScAdcmCd        string    `orm:"column(rsdnce_prov_sc_adcm_cd);size(12);null" description:"居住地省四川省行政区划代码  记录四川省行政区划代码中省份的完整区划代码,"`
	RsdnceCityScAdcmCd        string    `orm:"column(rsdnce_city_sc_adcm_cd);size(12);null" description:"居住地市四川省行政区划代码  记录四川省行政区划代码中市的完整区划代码,"`
	RsdnceCntyScAdcmCd        string    `orm:"column(rsdnce_cnty_sc_adcm_cd);size(12);null" description:"居住地县四川省行政区划代码  记录四川省行政区划代码中县/区的完整区划代码,"`
	RsdnceTwnScAdcmCd         string    `orm:"column(rsdnce_twn_sc_adcm_cd);size(12);null" description:"居住地镇四川省行政区划代码  记录四川省行政区划代码中乡/镇的完整区划代码,"`
	RsdnceVillgrpScAdcmCd     string    `orm:"column(rsdnce_villgrp_sc_adcm_cd);size(12);null" description:"居住地村组四川省行政区划代码"`
	CmunicAddrProvScAdcmCd    string    `orm:"column(cmunic_addr_prov_sc_adcm_cd);size(12);null" description:"通讯地址省四川省行政区划代码  记录四川省行政区划代码中省份的完整区划代码,"`
	CmunicAddrCityScAdcmCd    string    `orm:"column(cmunic_addr_city_sc_adcm_cd);size(12);null" description:"通讯地址市四川省行政区划代码  记录四川省行政区划代码中市的完整区划代码,"`
	CmunicAddrCntyScAdcmCd    string    `orm:"column(cmunic_addr_cnty_sc_adcm_cd);size(12);null" description:"通讯地址县四川省行政区划代码  记录四川省行政区划代码中县/区的完整区划代码,"`
	CmunicAddrTwnScAdcmCd     string    `orm:"column(cmunic_addr_twn_sc_adcm_cd);size(12);null" description:"通讯地址镇四川省行政区划代码  记录四川省行政区划代码中乡/镇的完整区划代码,"`
	CmunicAddrVillgrpScAdcmCd string    `orm:"column(cmunic_addr_villgrp_sc_adcm_cd);size(12);null" description:"通讯地址村组四川省行政区划代码"`
	FinlModfyDt               string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm               string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo            string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo           string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState              int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_investigate_task_result) TableName() string {
	return "t_loan_investigate_task_result"
}

func InsertT_loan_investigate_task_result(o orm.Ormer, maps map[string]interface{}) error {
	sql := util.BuildSql(maps, new(T_loan_investigate_task_result).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_investigate_task_resultTccState(o orm.Ormer, maps map[string]interface{}) error {
	_, err := o.QueryTable(new(T_loan_investigate_task_result)).Filter("artgcl_invstg_task_no", maps["ArtgclInvstgTaskNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_investigate_task_resultTccState(o orm.Ormer, maps map[string]interface{}) error {
	sql := "delete from t_loan_investigate_task_result where artgcl_invstg_task_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ArtgclInvstgTaskNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_investigate_task_result(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_investigate_task_result panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_investigate_task_result))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_investigate_task_resultById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	ArtgclInvstgTaskNo := params["ArtgclInvstgTaskNo"]
	SeqNo := params["SeqNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_investigate_task_result)).Filter("artgcl_invstg_task_no", ArtgclInvstgTaskNo).Filter("seq_no", SeqNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_investigate_task_result(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_investigate_task_result panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	ArtgclInvstgTaskNo := params["ArtgclInvstgTaskNo"]
	SeqNo := params["SeqNo"]
	_, err = o.QueryTable(new(T_loan_investigate_task_result)).Filter("artgcl_invstg_task_no", ArtgclInvstgTaskNo).Filter("seq_no", SeqNo).Filter("tcc_state", 0).Update(params)
	return err
}
