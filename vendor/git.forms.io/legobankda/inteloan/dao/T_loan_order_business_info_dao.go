package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_order_business_info struct {
	OrginlBizSn      string    `orm:"column(orginl_biz_sn);size(40);pk" description:"原业务流水号"`
	IntdTypCd        string    `orm:"column(intd_typ_cd);size(3)" description:"订单类型代码  1-放款受理,2-放款记账,,3-还款受理,4-还款记账,,5-授信申请,6-提额申请"`
	BizFolnNo        string    `orm:"column(biz_foln_no);size(32);null" description:"业务跟踪号"`
	SysFolnNo        string    `orm:"column(sys_foln_no);size(32);null" description:"系统跟踪号"`
	CustNo           string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	ExecStepNo       int       `orm:"column(exec_step_no);null" description:"执行步骤编号"`
	TxTelrNo         string    `orm:"column(tx_telr_no);size(6);null" description:"交易柜员号"`
	TxOrgNo          string    `orm:"column(tx_org_no);size(4);null" description:"交易机构号"`
	TxDt             string    `orm:"column(tx_dt);type(date);null" description:"交易日期"`
	TxTm             string    `orm:"column(tx_tm);type(time);null" description:"交易时间"`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModifrEmpnbr string    `orm:"column(finl_modifr_empnbr);size(6);null" description:"最后修改人员工号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_order_business_info) TableName() string {
	return "t_loan_order_business_info"
}

func InsertT_loan_order_business_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_order_business_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_order_business_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_order_business_info)).Filter("orginl_biz_sn", maps["OrginlBizSn"]).
		Filter("intd_typ_cd", maps["IntdTypCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_order_business_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_order_business_info where orginl_biz_sn=? and intd_typ_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OrginlBizSn"], maps["IntdTypCd"]).Exec()
	return err
}

func QueryT_loan_order_business_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_order_business_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_order_business_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_order_business_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_order_business_info)).Filter("orginl_biz_sn", maps["OrginlBizSn"]).Filter("intd_typ_cd", maps["IntdTypCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_order_business_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_order_business_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_order_business_info)).Filter("orginl_biz_sn", maps["OrginlBizSn"]).Filter("intd_typ_cd", maps["IntdTypCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
