package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strings"
)

type T_common_user struct {
	UserId             string    `orm:"column(user_id);pk" description:"用户ID"`
	Status             string    `orm:"column(status);size(1);null" description:"状态"`
	Name               string    `orm:"column(name);size(20);null" description:"名称"`
	OrgId              string    `orm:"column(org_id);size(8);null" description:"机构号"`
	EmployeeId         string    `orm:"column(employee_id);size(8);null" description:"员工编号"`
	RoleId             string    `orm:"column(role_id);size(3);null" description:"角色代码"`
	UserCategory       string    `orm:"column(user_category);size(1);null" description:"用户类别"`
	UserLvl            string    `orm:"column(user_lvl);size(1);null" description:"用户级别"`
	SuperiorUser       string    `orm:"column(superior_user);size(8);null" description:"上级用户"`
	BillStatus         string    `orm:"column(bill_status);size(1);null" description:"扎帐状态"`
	UserPassword       string    `orm:"column(user_password);size(16);null" description:"用户密码"`
	PasswordErrorCount int       `orm:"column(password_error_count);null" description:"密码错误次数"`
	PasswordUpdateDate string    `orm:"column(password_update_date);type(date);null" description:"密码更新日期"`
	CreateDate         string    `orm:"column(create_date);type(date);null" description:"创建日期"`
	CanceDate          string    `orm:"column(cance_date);type(date);null" description:"注销日期"`
	CreateTeller       string    `orm:"column(create_teller);size(8);null" description:"创建柜员"`
	CanceTeller        string    `orm:"column(cance_teller);size(8);null" description:"注销柜员"`
	LastMaintDate      string    `orm:"column(last_maint_date);type(date);null" description:"最后更新日期"`
	LastMaintTime      string    `orm:"column(last_maint_time);type(datetime);null" description:"最后更新时间"`
	LastMaintBrno      string    `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	LastMaintTell      string    `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	Tccstatus          string    `orm:"column(tccstatus);size(1);null" description:"tcc的状态"`
}

func (t *T_common_user) TableName() string {
	return "t_common_user"
}

func InsertT_common_user(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_common_user).TableName())
	sql = strings.Replace(sql,"tcc_state","tccstatus",1)
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_common_userTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_common_user)).Filter("user_id", maps["UserId"]).
		Update(orm.Params{"tccstatus": 0})
	return err
}

func DeleteT_common_userTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_common_user where user_id=? and tccstatus = 1"
	_, err := o.Raw(sql, maps["UserId"]).Exec()
	return err
}

func QueryT_common_user(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_common_user panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_common_user))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tccstatus", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_common_userById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_common_user)).Filter("user_id", maps["UserId"]).
		Filter("tccstatus", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_common_user(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_common_user panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_common_user)).Filter("user_id", maps["UserId"]).
		Filter("tccstatus", 0).Update(maps)
	return err
}
