package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_fin_txn_flow struct {
	TxAcctnDt         string    `orm:"column(tx_acctn_dt);type(date)" description:"交易会计日期  交易DATE,取系统DATE控制表DATE交易DATE,取系统DATE控制表的当前DATE"`
	TxTm              string    `orm:"column(tx_tm);type(time);null" description:"交易时间  记录交易发生的时间,"`
	BizFolnNo         string    `orm:"column(biz_foln_no);size(32)" description:"业务跟踪号  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	SysFolnNo         string    `orm:"column(sys_foln_no);size(32)" description:"系统跟踪号  标识一笔渠道接入交易在四川农信内部各系统之间交互的唯一识别流水号,32位组成为:客户端接入软件产品编码3位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号15位"`
	SeqNo             int       `orm:"column(seq_no)" description:"序号"`
	RcrdacctAcctnDt   string    `orm:"column(rcrdacct_acctn_dt);type(date);null" description:"入账会计日期  交易DATE,取系统DATE控制表的当前DATE交易DATE,取系统DATE控制表的当前DATE"`
	RcrdacctAcctnTm   string    `orm:"column(rcrdacct_acctn_tm);type(time);null" description:"入账会计时间  取当前系统TIME"`
	TxOrgNo           string    `orm:"column(tx_org_no);size(4);null" description:"交易机构号  记录交易发生机构的编号,填写机构代码,"`
	TxTelrNo          string    `orm:"column(tx_telr_no);size(6);null" description:"交易柜员号  记录交易涉及的柜员编号,柜员一般包括有交易柜员,复核柜员,授权柜员等不同角色"`
	AuthTelrNo        string    `orm:"column(auth_telr_no);size(6);null" description:"授权柜员号  记录贷款涉及的柜员编号,柜员一般包括有交易柜员,复核柜员,授权柜员等不同角色"`
	TxLunchChnlCd     string    `orm:"column(tx_lunch_chnl_cd);size(4);null" description:"交易发起渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表取报文头的发起渠道'TRM'"`
	AccessChnlCd      string    `orm:"column(access_chnl_cd);size(4);null" description:"接入渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表取报文头的接入渠道号'TRM'"`
	BizSysNo          string    `orm:"column(biz_sys_no);size(3);null" description:"业务系统编号  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'取报文头的服务消费者系统编号"`
	DebitCrdtFlgCd    string    `orm:"column(debit_crdt_flg_cd);size(1);null" description:"借贷标志代码  参考标准代码:CD0167借'D'"`
	CustNo            string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度如客户号发生变化是否要同步流水表中的客户号"`
	MerchtNo          string    `orm:"column(mercht_no);size(32);null" description:"商户编号"`
	CardNoOrAcctNo    string    `orm:"column(card_no_or_acct_no);size(32);null" description:"卡号或账号"`
	AcctiAcctNo       string    `orm:"column(accti_acct_no);size(32);null" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	FinTxAmtTypCd     string    `orm:"column(fin_tx_amt_typ_cd);size(1);null" description:"金融交易金额类型代码  001,,活期本金002,,定期本金003,,贷款本金004,,应付利息005,,应收利息006,,费用007,,利息税001,,活期本金"`
	TxCurCd           string    `orm:"column(tx_cur_cd);size(3);null" description:"交易币种代码  参考标准代码:CD0040交易币种"`
	Amount            float64   `orm:"column(amount);null;digits(18);decimals(2)" description:"发生额  资金系统长度为:20,4转账DECIMAL"`
	PostvReblnTxFlgCd string    `orm:"column(postv_rebln_tx_flg_cd);size(1);null" description:"正反交易标志代码  N-正交易,R-反交易,C-不入账交易反交易标志用于日终送总账的系统:当为C-不入账交易时表示此交易有对应的反交易,不需送总账,在反交易发生时需把对应的正交易置为不入账交易,不入账交易不允许再做反交易N-正交易"`
	RvrsTxFlgCd       string    `orm:"column(rvrs_tx_flg_cd);size(1);null" description:"冲正交易标志代码  N-正常交易,C-冲销交易,B-已冲正交易正常交易被冲正后需把此标志位修改为B-已冲正交易,已冲正交易不允许再次冲正N-正常交易"`
	OrginlHostTxSn    string    `orm:"column(orginl_host_tx_sn);size(32);null" description:"原主机交易流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,随机整数8位编号规则:1',随机数生成:(int)(Math.random()*100000000)"`
	OrginlTxAcctnDt   string    `orm:"column(orginl_tx_acctn_dt);type(date);null" description:"原交易会计日期  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	OrginlTxTelrNo    string    `orm:"column(orginl_tx_telr_no);size(6);null" description:"原交易柜员号"`
	CustMgrTelrNo     string    `orm:"column(cust_mgr_telr_no);size(6);null" description:"客户经理柜员号  查询获得活期账号的客户经理号"`
	MsgId             string    `orm:"column(msg_id);size(20);null" description:"消息id"`
	AbstCd            string    `orm:"column(abst_cd);size(3);null" description:"摘要代码  记录交易的摘要编码,101-转账存入102-转账支取554-留存转入'部提存入'555-利息支出560-结清支取561-部提支取562-留存转出563-结息转入564-利息补提565-利息冲回566-手工付息567-利息收入834-利息扣回F05-收利息税如,GC1-转账"`
	Abst              string    `orm:"column(abst);size(200);null" description:"摘要"`
	PmitRvrsFlg       string    `orm:"column(pmit_rvrs_flg);size(1);null" description:"允许冲正标志  0否1是"`
	FinChgacctFlg     string    `orm:"column(fin_chgacct_flg);size(2);null" description:"金融动账标志  0-是,1-否,"`
	OperTypCd         string    `orm:"column(oper_typ_cd);size(4);null" description:"操作类型代码  0-金额结转,1-转逾期,2-转减值,3-转核销,4-转正常"`
	Pridnum           int       `orm:"column(pridnum);null" description:"期数"`
	WrtoffAmt         float64   `orm:"column(wrtoff_amt);null;digits(18);decimals(2)" description:"核销金额"`
	TrsctnId          string    `orm:"column(trsctn_id);size(50);null" description:"事务id"`
	TccStusCd         string    `orm:"column(tcc_stus_cd);size(1);null" description:"tcc状态代码  0-try,1-confirm,2-cancel"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_fin_txn_flow) TableName() string {
	return "t_fin_txn_flow"
}

func InsertT_fin_txn_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_fin_txn_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_fin_txn_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_fin_txn_flow)).Filter("tx_acctn_dt", maps["TxAcctnDt"]).
		Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_fin_txn_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_fin_txn_flow where tx_acctn_dt=? and biz_foln_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxAcctnDt"], maps["BizFolnNo"]).Exec()
	return err
}

func QueryT_fin_txn_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_fin_txn_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_fin_txn_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_fin_txn_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_fin_txn_flow)).Filter("tx_acctn_dt", maps["TxAcctnDt"]).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("sys_foln_no", maps["SysFolnNo"]).Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_fin_txn_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_fin_txn_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_fin_txn_flow)).Filter("tx_acctn_dt", maps["TxAcctnDt"]).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("sys_foln_no", maps["SysFolnNo"]).Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
