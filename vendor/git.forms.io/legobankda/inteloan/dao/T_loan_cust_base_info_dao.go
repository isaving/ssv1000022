package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_cust_base_info struct {
	CrdtAplySn         string    `orm:"column(crdt_aply_sn);size(32);pk" description:"授信申请流水号  n "`
	CustNo             string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CustName           string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd      string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo         string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	TelNo              string    `orm:"column(tel_no);size(20);null" description:"电话号码  记录个人客户的电话类型所对应的电话号码信息,"`
	MobileNo           string    `orm:"column(mobile_no);size(11);null" description:"手机号码  记录个人客户的国内手机号码信息,"`
	GenderCd           string    `orm:"column(gender_cd);size(3);null" description:"性别代码  参考标准代码:CD0006"`
	EMail              string    `orm:"column(e_mail);size(40);null" description:"电子邮箱  记录个人客户的电子邮箱信息,"`
	StateAndRgnCd      string    `orm:"column(state_and_rgn_cd);size(4);null" description:"国家和地区代码  参考标准代码:CD0001"`
	HousdRgstScAdcmCd  string    `orm:"column(housd_rgst_sc_adcm_cd);size(12);null" description:"户籍四川省行政区划代码  参考标准代码:CD0002"`
	HousdRgstDtlAddr   string    `orm:"column(housd_rgst_dtl_addr);size(200);null" description:"户籍详细地址"`
	HousdRgstAddr      string    `orm:"column(housd_rgst_addr);size(200);null" description:"户籍地址"`
	CmunicAddrScAdcmCd string    `orm:"column(cmunic_addr_sc_adcm_cd);size(12);null" description:"通讯地址四川省行政区划代码  参考标准代码:CD0002"`
	CotactDtlAddr      string    `orm:"column(cotact_dtl_addr);size(200);null" description:"联系详细地址"`
	CotactAddr         string    `orm:"column(cotact_addr);size(200);null" description:"联系地址"`
	CotactAddrPostCd   string    `orm:"column(cotact_addr_post_cd);size(6);null" description:"联系地址邮政编码"`
	RsdnceScAdcmCd     string    `orm:"column(rsdnce_sc_adcm_cd);size(12);null" description:"居住地四川省行政区划代码  参考标准代码:CD0002"`
	RsdnceDtlAddr      string    `orm:"column(rsdnce_dtl_addr);size(200);null" description:"居住地详细地址"`
	RsdnceAddr         string    `orm:"column(rsdnce_addr);size(200);null" description:"居住地地址"`
	RsdncePostCd       string    `orm:"column(rsdnce_post_cd);size(6);null" description:"居住地邮政编码"`
	EdctbkgCd          string    `orm:"column(edctbkg_cd);size(2);null" description:"学历代码  参考标准代码:CD0008"`
	CareerTypCd        string    `orm:"column(career_typ_cd);size(5);null" description:"职业类型代码  参考标准代码:CD0013"`
	IndusTypCd         string    `orm:"column(indus_typ_cd);size(5);null" description:"行业类型代码  参考标准代码:CD0018"`
	WorkUnitNm         string    `orm:"column(work_unit_nm);size(120);null" description:"工作单位名称  记录个人客户的工作单位信息,此处应该填写工作单位的全称,"`
	WorkUnitAddr       string    `orm:"column(work_unit_addr);size(200);null" description:"工作单位地址"`
	WorkUnitCharcCd    string    `orm:"column(work_unit_charc_cd);size(3);null" description:"工作单位性质代码  参考标准代码:CD0087"`
	DutyCd             string    `orm:"column(duty_cd);size(5);null" description:"职务代码  参考标准代码:CD0012"`
	DutyNm             string    `orm:"column(duty_nm);size(60);null" description:"职务名称"`
	UnitTelNo          string    `orm:"column(unit_tel_no);size(20);null" description:"单位电话号码"`
	MarrgSituationCd   string    `orm:"column(marrg_situation_cd);size(3);null" description:"婚姻状况代码  参考标准代码:CD0004"`
	HavNotChildFlg     string    `orm:"column(hav_not_child_flg);size(1);null" description:"有无子女标志  0否1是"`
	BirthDt            string    `orm:"column(birth_dt);type(date);null" description:"出生日期  记录个人客户的出生日期信息,"`
	FinlModfyDt        string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  N"`
	FinlModfyTm        string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  N"`
	FinlModfyOrgNo     string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号  N"`
	FinlModfyTelrNo    string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号  N"`
	TccState           int    `orm:"column(tcc_state);null"`
}

func (t *T_loan_cust_base_info) TableName() string {
	return "t_loan_cust_base_info"
}

func InsertT_loan_cust_base_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_cust_base_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_cust_base_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_cust_base_info)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_cust_base_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_cust_base_info where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryT_loan_cust_base_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_cust_base_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_cust_base_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "CustName" {
				qs = qs.Filter("cust_name__contains", v)
			}else{
				qs = qs.Filter(k, v)
			}

		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_cust_base_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_cust_base_info)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_cust_base_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_cust_base_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_cust_base_info)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
