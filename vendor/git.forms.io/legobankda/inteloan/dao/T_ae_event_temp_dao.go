package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_event_temp struct {
	AcctnDt         string    `orm:"column(acctn_dt);type(date);pk";description:"会计日期  PK"`
	SeqNo           int       `orm:"column(seq_no)" description:"序号"`
	TxTm            string    `orm:"column(tx_tm);type(time);null" description:"交易时间"`
	BizFolnNo       string    `orm:"column(biz_foln_no);size(32);null" description:"业务跟踪号  全局流水号"`
	BizEventCd      string    `orm:"column(biz_event_cd);size(6);null" description:"业务事件编码  根据每种产品的业务特性,将产品每类业务拆分为最小的业务服务单元,称为业务事件,每种业务服务可由一个业务事件构成,也可以由多个业务事件构成"`
	SysFolnNo       string    `orm:"column(sys_foln_no);size(32);null" description:"系统跟踪号  标识一笔渠道接入交易在四川农信内部各系统之间交互的唯一识别流水号,由32位组成:客户端接入软件产品编码3位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号15位,"`
	FundFlgCd       string    `orm:"column(fund_flg_cd);size(4);null" description:"资金标志编码"`
	TxOrgNo         string    `orm:"column(tx_org_no);size(4);null" description:"交易机构号  记录交易发生机构的编号,填写机构代码,"`
	AccessChnlCd    string    `orm:"column(access_chnl_cd);size(4);null" description:"接入渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表取报文头的接入渠道号'TRM'"`
	SoftProdtCd     string    `orm:"column(soft_prodt_cd);size(3);null" description:"软件产品编码  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'"`
	ActngOrgNo      string    `orm:"column(actng_org_no);size(4);null" description:"账务机构号  交易归属"`
	AmtDrctCd       string    `orm:"column(amt_drct_cd);size(1);null" description:"金额方向代码  A-加,S-减,"`
	ProdtNo         string    `orm:"column(prodt_no);size(20);null" description:"产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	EventUdfInfo    string    `orm:"column(event_udf_info);size(200);null" description:"事件自定义信息  补记流水时备注"`
	CurCd           string    `orm:"column(cur_cd);size(4);null" description:"币种代码  参考标准代码:CD0040"`
	Amount          float64   `orm:"column(amount);null;digits(18);decimals(2)" description:"发生额"`
	MansbjTypCd     string    `orm:"column(mansbj_typ_cd);size(3);null" description:"主体类型代码  0-对公,,1-农户,,2非农个人"`
	WastStusCd      string    `orm:"column(wast_stus_cd);size(1);null" description:"流水状态代码"`
	AbstCd          string    `orm:"column(abst_cd);size(3);null" description:"摘要代码  记录交易的摘要编码,"`
	WrtffFlg        string    `orm:"column(wrtff_flg);size(1);null" description:"冲销标志  0-正常,1-冲销"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TelrNo          string    `orm:"column(telr_no);size(6);null" description:"柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_event_temp) TableName() string {
	return "t_ae_event_temp"
}

func InsertT_ae_event_temp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_event_temp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_event_tempTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_event_temp)).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_event_tempTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_event_temp where acctn_dt=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctnDt"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_ae_event_temp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_event_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_event_temp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_event_tempById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_event_temp)).Filter("acctn_dt", maps["AcctnDt"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_event_temp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_event_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_event_temp)).Filter("acctn_dt", maps["AcctnDt"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
