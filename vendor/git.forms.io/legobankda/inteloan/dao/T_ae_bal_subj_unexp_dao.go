package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_bal_subj_unexp struct {
	AcctnDt         string    `orm:"column(acctn_dt);type(date);pk" description:"会计日期"`
	SubjNo          string    `orm:"column(subj_no);size(8)" description:"科目编号"`
	AcctBookCategCd string    `orm:"column(acct_book_categ_cd);size(5);null" description:"账套类别代码"`
	SubjNm          string    `orm:"column(subj_nm);size(60);null" description:"科目名称"`
	SubjStusCd      string    `orm:"column(subj_stus_cd);size(3);null" description:"科目状态代码"`
	SubjBal         float64   `orm:"column(subj_bal);null;digits(18);decimals(2)" description:"科目余额"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_bal_subj_unexp) TableName() string {
	return "t_ae_bal_subj_unexp"
}

func InsertT_ae_bal_subj_unexp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_bal_subj_unexp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_bal_subj_unexpTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_bal_subj_unexp)).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("subj_no", maps["SubjNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_bal_subj_unexpTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_bal_subj_unexp where acctn_dt=? and subj_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctnDt"], maps["SubjNo"]).Exec()
	return err
}

func QueryT_ae_bal_subj_unexp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_bal_subj_unexp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_bal_subj_unexp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_bal_subj_unexpById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_bal_subj_unexp)).Filter("acctn_dt", maps["AcctnDt"]).Filter("subj_no", maps["SubjNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_bal_subj_unexp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_bal_subj_unexp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_bal_subj_unexp)).Filter("acctn_dt", maps["AcctnDt"]).Filter("subj_no", maps["SubjNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
