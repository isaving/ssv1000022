package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_contract_suppt_info_form struct {
	LoanDubilNo      				 string    `orm:"column(loan_dubil_no);pk" description:"贷款借据号"`
	CustNo                           string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	LoanTotlPridnum                  int       `orm:"column(loan_totl_pridnum);null" description:"贷款总期数"`
	CurrExecPridnum                  int       `orm:"column(curr_exec_pridnum);null" description:"当前执行期数"`
	LoanRemainPridnum                int       `orm:"column(loan_remain_pridnum);null" description:"贷款剩余期数"`
	PrinDueBgnDt                     string    `orm:"column(prin_due_bgn_dt);type(date);null" description:"本金拖欠开始日期"`
	IntrDueBgnDt                     string    `orm:"column(intr_due_bgn_dt);type(date);null" description:"利息拖欠开始日期"`
	AccmDuePridnum                   int       `orm:"column(accm_due_pridnum);null" description:"累计拖欠期数"`
	AccmWrtoffAmt                    float64   `orm:"column(accm_wrtoff_amt);null;digits(18);decimals(2)" description:"累计核销金额"`
	LsttrmCompEtimLnRiskClsfDt       string    `orm:"column(lsttrm_comp_etim_ln_risk_clsf_dt);type(date);null" description:"上期机评贷款风险分类日期"`
	LsttrmCompEtimLnRiskClsfCd       string    `orm:"column(lsttrm_comp_etim_ln_risk_clsf_cd);size(2);null" description:"上期机评贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级记录机器评测的贷款风险分类结果,主要有正常,关注,次级,可疑,损失等,"`
	CurtprdCompEtimLnRiskClsfDt      string    `orm:"column(curtprd_comp_etim_ln_risk_clsf_dt);type(date);null" description:"本期机评贷款风险分类日期"`
	CurtprdCompEtimLnRiskClsfCd      string    `orm:"column(curtprd_comp_etim_ln_risk_clsf_cd);size(2);null" description:"本期机评贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	LsttrmArtgclIdtfyLoanRiskClsfDt  string    `orm:"column(lsttrm_artgcl_idtfy_loan_risk_clsf_dt);type(date);null" description:"上期人工认定贷款风险分类日期"`
	LsttrmArtgclIdtfyLoanRiskClsfCd  string    `orm:"column(lsttrm_artgcl_idtfy_loan_risk_clsf_cd);size(2);null" description:"上期人工认定贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	CurtprdArtgclIdtfyLoanRiskClsfDt string    `orm:"column(curtprd_artgcl_idtfy_loan_risk_clsf_dt);type(date);null" description:"本期人工认定贷款风险分类日期"`
	CurtprdArtgclIdtfyLoanRiskClsfCd string    `orm:"column(curtprd_artgcl_idtfy_loan_risk_clsf_cd);size(2);null" description:"本期人工认定贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	FinlModfyDt                      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo                   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo                  string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState          int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_contract_suppt_info_form) TableName() string {
	return "t_loan_contract_suppt_info_form"
}

func InsertT_loan_contract_suppt_info_form(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_contract_suppt_info_form).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_contract_suppt_info_formTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_contract_suppt_info_form)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_contract_suppt_info_formTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_contract_suppt_info_form where loan_dubil_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"]).Exec()
	return err
}

func QueryT_loan_contract_suppt_info_form(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_contract_suppt_info_form panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_contract_suppt_info_form))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_contract_suppt_info_formById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_contract_suppt_info_form)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_contract_suppt_info_form(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_contract_suppt_info_form panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_contract_suppt_info_form)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
