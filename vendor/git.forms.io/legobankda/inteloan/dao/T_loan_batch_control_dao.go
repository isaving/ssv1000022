package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_batch_control struct {
	JobNo              string    `orm:"column(job_no);pk" description:"作业编号"`
	JobNm              string    `orm:"column(job_nm);size(120);null" description:"作业名称"`
	JobBgnExecDt       string    `orm:"column(job_bgn_exec_dt);type(date);null" description:"作业开始执行日期"`
	JobBgnExecTm       string    `orm:"column(job_bgn_exec_tm);type(time);null" description:"作业开始执行时间"`
	JobEndExecDt       string    `orm:"column(job_end_exec_dt);type(date);null" description:"作业结束执行日期"`
	JobEndExecTm       string    `orm:"column(job_end_exec_tm);type(time);null" description:"作业结束执行时间"`
	CurtyJobStusCd     string    `orm:"column(curty_job_stus_cd);size(1);null" description:"目前作业状态代码  1-未处理2-已处理"`
	JobCanRepetExecFlg string    `orm:"column(job_can_repet_exec_flg);size(1);null" description:"作业可重复执行标志"`
	PgmCdnmAssmblg     string    `orm:"column(pgm_cdnm_assmblg);size(512);null" description:"程序代号集合"`
	BizDt              string    `orm:"column(biz_dt);type(date);null" description:"业务日期"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_batch_control) TableName() string {
	return "t_loan_batch_control"
}

func InsertT_loan_batch_control(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_batch_control).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_batch_controlTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_batch_control)).Filter("job_no", maps["JobNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_batch_controlTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_batch_control where job_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["JobNo"]).Exec()
	return err
}

func QueryT_loan_batch_control(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_batch_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_batch_control))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_batch_controlById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_batch_control)).Filter("job_no", maps["JobNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_batch_control(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_batch_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_batch_control)).Filter("job_no", maps["JobNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
