package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_manager_customer struct {
	CustMgrEmpnbr     string `orm:"column(cust_mgr_empnbr);size(6);pk" description:"客户经理员工号"`
	CorgCustMgrEmpnbr string `orm:"column(corg_cust_mgr_empnbr);size(6);null" description:"协办客户经理员工号"`
	CorgCustMgrName   string `orm:"column(corg_cust_mgr_name);size(60);null" description:"协办客户经理姓名"`
	CustMgrName       string `orm:"column(cust_mgr_name);size(60);null" description:"客户经理姓名"`
	LprOrgNo          string `orm:"column(lpr_org_no);size(4);null" description:"法人机构号"`
	KeprcdStusCd      string `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CustNo            string `orm:"column(cust_no);size(14)" description:"客户编号"`
	ExcutvPrvlgTypCd  string `orm:"column(excutv_prvlg_typ_cd);size(5)" description:"管户权限类型代码  1-管户权2-白名单维护权3-贷后管理权及合作方管理权"`
	BlngtoOrgNo       string `orm:"column(blngto_org_no);size(4);null" description:"所属机构号"`
	RelBlngTypCd      string `orm:"column(rel_blng_typ_cd);size(1);null" description:"关系归属类型代码  1-系统分配,2-主动认领,3-客户移交,4-机构撤并转入"`
	CustName          string `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd     string `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo        string `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CustCotactTelNo   string `orm:"column(cust_cotact_tel_no);size(20);null" description:"客户联系电话号码"`
	CrtDt             string `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	FinlModfyDt       string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState          int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_manager_customer) TableName() string {
	return "t_loan_manager_customer"
}

func InsertT_loan_manager_customer(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_manager_customer).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_manager_customerTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_manager_customer)).Filter("cust_mgr_empnbr", maps["CustMgrEmpnbr"]).
		Filter("cust_no", maps["CustNo"]).Filter("excutv_prvlg_typ_cd", maps["ExcutvPrvlgTypCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_manager_customerTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_manager_customer where cust_mgr_empnbr=? and cust_no=? and excutv_prvlg_typ_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustMgrEmpnbr"], maps["CustNo"], maps["ExcutvPrvlgTypCd"]).Exec()
	return err
}

func QueryT_loan_manager_customer(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_manager_customer panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_loan_manager_customer))
	if flag == "9"{
		var list orm.ParamsList
		if _, err = qs.Filter("tcc_state", state).Limit(-1).GroupBy("CustNo").ValuesFlat(&list,"CustNo"); err != nil {
			return nil, err
		}
		mapOne := make(map[string]interface{})
		mapOne["ListCustNo"] = list
		maps =append(maps,mapOne)
		//fmt.Printf("All User Names: %s", strings.Join(list, ", ")
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_manager_customerById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_manager_customer)).Filter("cust_mgr_empnbr", maps["CustMgrEmpnbr"]).Filter("cust_no", maps["CustNo"]).
		Filter("excutv_prvlg_typ_cd", maps["ExcutvPrvlgTypCd"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_manager_customer(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_manager_customer panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_manager_customer)).Filter("cust_mgr_empnbr", maps["CustMgrEmpnbr"]).Filter("cust_no", maps["CustNo"]).
		Filter("excutv_prvlg_typ_cd", maps["ExcutvPrvlgTypCd"]).Filter("tcc_state", 0).Update(maps)
	return err
}
