package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Ts_sys_constant struct {
	SysPmn          string    `orm:"column(sys_pmn);size(30)" description:"参数代码"`
	OrgNo           string    `orm:"column(org_no);size(4);pk" description:"机构号"`
	SysPmt          string    `orm:"column(sys_pmt);size(1);null" description:"参数类型 1-数据 2-字符"`
	SysPml          int       `orm:"column(sys_pml);null" description:"参数长度"`
	SysPmd          string    `orm:"column(sys_pmd);size(2);null" description:"参数小数长度"`
	SysPmv          string    `orm:"column(sys_pmv);size(32);null" description:"参数值"`
	SysCd6          string    `orm:"column(sys_cd6);size(60);null" description:"参数说明"`
	SysRds          string    `orm:"column(sys_rds);size(1);null" description:"记录状态 0-否 1-是"`
	SysSmk          string    `orm:"column(sys_smk);size(10);null" description:"备用1"`
	SysSmk1         string    `orm:"column(sys_smk1);size(10);null" description:"备用2"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *Ts_sys_constant) TableName() string {
	return "ts_sys_constant"
}

func InsertTs_sys_constant(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Ts_sys_constant).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateTs_sys_constantTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Ts_sys_constant)).Filter("org_no", maps["OrgNo"]).
		Filter("sys_pmn", maps["SysPmn"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteTs_sys_constantTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from ts_sys_constant where org_no=? and sys_pmn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OrgNo"], maps["SysPmn"]).Exec()
	return err
}

func QueryTs_sys_constant(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Ts_sys_constant))
	if params["Flag"] != nil && params["Flag"]=="9"{
		sql :="SELECT MAX(sys_pmv)+1 SysPmv FROM ts_sys_constant WHERE sys_pmn=?"
		_, err = o.Raw(sql, params["SysPmn"]).Values(&maps)
		//SysPmv := maps[0]["SysPmv"]
		SysPmn := params["SysPmn"]
		o.Begin()
		if _, err =  qs.Filter("tcc_state", state).Filter("sys_pmn",SysPmn).Update(maps[0]);err != nil {
			o.Rollback()
		}
		o.Commit()
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryTs_sys_constantById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Ts_sys_constant)).Filter("org_no", maps["OrgNo"]).Filter("sys_pmn", maps["SysPmn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateTs_sys_constant(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Ts_sys_constant)).Filter("org_no", maps["OrgNo"]).Filter("sys_pmn", maps["SysPmn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
