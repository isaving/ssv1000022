package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intr_cal_his struct {
	IntacrRuleNo      string    `orm:"column(intacr_rule_no);size(12);pk" description:"计息规则编号  序号"`
	EfftDt            string    `orm:"column(efft_dt);type(date)" description:"生效日期  记录贷款合同的生效日期,"`
	InvalidDt         string    `orm:"column(invalid_dt);type(date)" description:"失效日期  PK"`
	IntacrAgrthmTypCd string    `orm:"column(intacr_agrthm_typ_cd);size(1);null" description:"计息算法类型代码  D-实际天数算法Y-对年对月对日算法Z-对年对日算法"`
	Flag              string    `orm:"column(flag);size(2);null" description:"标志"`
	Bak               string    `orm:"column(bak);size(10);null" description:"备用"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccKeprcdVisFlg   string    `orm:"column(tcc_keprcd_vis_flg);size(1);null" description:"tcc记录可见标志  0否1是"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_intr_cal_his) TableName() string {
	return "t_intr_cal_his"
}

func InsertT_intr_cal_his(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intr_cal_his).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intr_cal_hisTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intr_cal_his)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).
		Filter("efft_dt", maps["EfftDt"]).Filter("invalid_dt", maps["InvalidDt"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intr_cal_hisTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intr_cal_his where intacr_rule_no=? and efft_dt=? and invalid_dt=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IntacrRuleNo"], maps["EfftDt"], maps["InvalidDt"]).Exec()
	return err
}

func QueryT_intr_cal_his(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intr_cal_his panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intr_cal_his))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intr_cal_hisById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intr_cal_his)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).Filter("efft_dt", maps["EfftDt"]).
		Filter("invalid_dt", maps["InvalidDt"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intr_cal_his(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intr_cal_his panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intr_cal_his)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).Filter("efft_dt", maps["EfftDt"]).
		Filter("invalid_dt", maps["InvalidDt"]).Filter("tcc_state", 0).Update(maps)
	return err
}
