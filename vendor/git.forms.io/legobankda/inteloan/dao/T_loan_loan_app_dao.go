package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_loan_app struct {
	MakelnAplySn              string  `orm:"column(makeln_aply_sn);size(34);pk" description:"放款申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	CrdtAplySn                string  `orm:"column(crdt_aply_sn);size(34);null" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	LoanDubilNo               string  `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	CtrtNo                    string  `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	CustNo                    string  `orm:"column(cust_no);size(20);null" description:"客户编号  客户编号,ECIF生成"`
	CustName                  string  `orm:"column(cust_name);size(320);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd             string  `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo                string  `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	OthConsmTypCd             string  `orm:"column(oth_consm_typ_cd);size(4);null" description:"其他消费类型代码      1-个人日常消费,2-装修,3-旅游,4-教育,5-医疗,6-其他消费,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"`
	UsageRplshComnt           string  `orm:"column(usage_rplsh_comnt);size(200);null" description:"用途补充说明"`
	DpstAcctNo                string  `orm:"column(dpst_acct_no);size(32);null" description:"存款账号"`
	LoanProdtNo               string  `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo           string  `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	LoanAmt                   float64 `orm:"column(loan_amt);null;digits(18);decimals(2)" description:"贷款金额  如果多次贷放,或者信用卡模式,此金额如何处理?"`
	LoanCurCd                 string  `orm:"column(loan_cur_cd);size(3);null" description:"贷款币种代码  参考标准代码:CD0040"`
	Fee                       float64 `orm:"column(fee);null;digits(18);decimals(2)" description:"费用"`
	RprincPayIntCycCd         string  `orm:"column(rprinc_pay_int_cyc_cd);size(3);null" description:"还本付息周期代码  参考标准代码:CD0075"`
	LoanDeadl                 int     `orm:"column(loan_deadl);null" description:"贷款期限"`
	LoanDeadlUnitCd           string  `orm:"column(loan_deadl_unit_cd);size(3);null" description:"贷款期限单位代码  CD0075,年,季,月,双周,周,日"`
	RepayManrCd               string  `orm:"column(repay_manr_cd);size(2);null" description:"还款方式代码  参考标准代码:CD0062标准基础上新增:51-按月付息按季还本52-按月付息按半年还本53-按月付息按年还本"`
	MakelnAplyStusCd          string  `orm:"column(makeln_aply_stus_cd);size(2);null" description:"放款申请状态代码  01-放款申请已受理02-放款审批通过03-放款审批拒绝04-签订合同成功06-放款成功09-放款失败10-closed"`
	AplyMakelnDt              string  `orm:"column(aply_makeln_dt);type(date);null" description:"申请放款日期"`
	ActlMakelnTm              string  `orm:"column(actl_makeln_tm);type(time);null" description:"实际放款时间"`
	ExecIntrt                 float64 `orm:"column(exec_intrt);null;digits(9);decimals(6)" description:"执行利率"`
	RgtsintTypCd              string  `orm:"column(rgtsint_typ_cd);size(10);null" description:"权益类型代码"`
	RgtsintNo                 string  `orm:"column(rgtsint_no);size(12);null" description:"权益编号"`
	LoanUsageCmtd             string  `orm:"column(loan_usage_cmtd);size(2000);null" description:"贷款用途承诺"`
	BaseIntrtTypCd            string  `orm:"column(base_intrt_typ_cd);size(10);null" description:"基础利率类型代码  1-人行基准利率,2-LPR利率"`
	IntrtNo                   string  `orm:"column(intrt_no);size(40);null" description:"利率编号"`
	BnchmkIntrt               float64 `orm:"column(bnchmk_intrt);null;digits(9);decimals(6)" description:"基准利率"`
	IntrtFlotDrctCd           string  `orm:"column(intrt_flot_drct_cd);size(1);null" description:"利率浮动方向代码      D-下调,U-上调"`
	BpFlotVal                 float64 `orm:"column(bp_flot_val);null;digits(6);decimals(4)" description:"bp浮动值"`
	IntrtFlotRatio            float64 `orm:"column(intrt_flot_ratio);null;digits(9);decimals(6)" description:"利率浮动比例"`
	FixdIntrtPricingManrCd    string  `orm:"column(fixd_intrt_pricing_manr_cd);size(2);null" description:"固定利率定价方式代码  1-指定利率基准值2-基于基准利率类型匹配期限,3-基于指定基准利率"`
	IntrtFlotManrCd           string  `orm:"column(intrt_flot_manr_cd);size(1)" description:"P-浮动百分比;N-浮动点数"`
	RelaAcctOpnAcctBnkBnkNo   string  `orm:"column(rela_acct_opn_acct_bnk_bnk_no);size(14);null" description:"关联账户开户行行号"`
	RelaAcctOpnAcctBnkBnkName string  `orm:"column(rela_acct_opn_acct_bnk_bnk_name);size(120);null" description:"关联账户开户行行名"`
	RelaAcctBlngtoRwCategCd   string  `orm:"column(rela_acct_blngto_rw_categ_cd);size(6);null" description:"关联账户所属行类别代码"`
	RelaAcctNoTypCd           string  `orm:"column(rela_acct_no_typ_cd);size(2);null" description:"关联账号类型代码  记录关联账号的类型信息,如:客户账号,产品账号,核算账号等,"`
	RsdnceProvScAdcmCd        string  `orm:"column(rsdnce_prov_sc_adcm_cd);size(12);null" description:"居住地省行政区划代码"`
	RsdnceCityScAdcmCd        string  `orm:"column(rsdnce_city_sc_adcm_cd);size(12);null" description:"居住地市行政区划代码"`
	RsdnceCntyScAdcmCd        string  `orm:"column(rsdnce_cnty_sc_adcm_cd);size(12);null" description:"居住地县行政区划代码"`
	RsdnceTwnScAdcmCd         string  `orm:"column(rsdnce_twn_sc_adcm_cd);size(12);null" description:"居住地镇行政区划代码"`
	RsdnceVillgrpScAdcmCd     string  `orm:"column(rsdnce_villgrp_sc_adcm_cd);size(12);null" description:"居住地村组行政区划代码"`
	RsdnceDtlAddr             string  `orm:"column(rsdnce_dtl_addr);size(200);null" description:"居住地详细地址"`
	MakelnManrCd              string  `orm:"column(makeln_manr_cd);size(1);null" description:"放款方式代码  0-自主支付,,1-受托支付,2-混合支付记录贷款合同放款的支付方式,如自主支付,受托支付等,"`
	QtaChgSn                  string  `orm:"column(qta_chg_sn);size(34);null" description:"额度变更流水号"`
	LoanIntrtAdjCycCd         string  `orm:"column(loan_intrt_adj_cyc_cd);size(3);null" description:"贷款利率调整周期代码  参考标准代码:CD0075"`
	LoanIntrtAdjCycQty        int     `orm:"column(loan_intrt_adj_cyc_qty);null" description:"贷款利率调整周期数量"`
	LoanIntrtAdjManrCd        string  `orm:"column(loan_intrt_adj_manr_cd);size(1);null" description:"贷款利率调整方式代码  1-对日,2-周期开始日,3-固定"`
	CareerTypCd               string  `orm:"column(career_typ_cd);size(5);null" description:"职业类型代码"`
	EMail                     string  `orm:"column(e_mail);size(40);null" description:"电子邮箱"`
	FinlModfyDt               string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  N"`
	FinlModfyTm               string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  N"`
	FinlModfyTelrNo           string  `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号  N"`
	FinlModfyOrgNo            string  `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	TccState                  int8    `orm:"column(tcc_state);null"`
	LoanGuarManrCd            string  `orm:"column(loan_guar_manr_cd);size(2);null" description:"担保方式 01-No Guarantee 02-Guarantor"`
	GuarName                  string  `orm:"column(guar_name);size(320);null" description:"担保人姓名 如果担保方式为不需担保，此字段为空，否则，为必输项"`
	GuarIdType                string  `orm:"column(guar_id_type);size(4);null" description:"担保人证件类型 如果担保方式为不需担保，此字段为空，否则，为必输项 1- passport 2- citizen ID"`
	GuarIdNo                  string  `orm:"column(guar_id_no);size(20);null" description:"担保人证件号码 如果担保方式为不需担保，此字段为空，否则，为必输项"`
	GuarPassCountry           string  `orm:"column(guar_pass_country);size(4);null" description:"担保护照国家 如果担保方式为不需担保，此字段为空，否则，为必输项"`
	LoanProdtNm               string  `orm:"column(loan_prodt_nm);size(60);null" description:"产品名称"`
	LoanPur                   string  `orm:"column(loan_pur);size(30);null" description:"贷款目的"`
	MatrDt                    string  `orm:"column(matr_dt);type(date);null" description:"到期日期"`
	RepayDay                  string  `orm:"column(repay_day);size(2);null" description:"还款日"`
	LoanStatus                string  `orm:"column(loan_status);size(2);null" description:"放款状态 1.total 2.Partial"`
	LoanChannel               string  `orm:"column(loan_channel);size(2);null" description:"放款渠道  1-Bank"`
	RepayCyc                  string  `orm:"column(repay_cyc);size(2);null" description:"还款周期 - Daily - Fortnightly -Monthly * (The system default monthly.) -Quarterly -Yearly"`
	AuotoRepyFlag             string  `orm:"column(auoto_repy_flag);size(2);null" description:"自动还款标识 1-Auto-repay"`
	RepayDt                   string  `orm:"column(repay_dt);type(date);null" description:"还款日期"`
	AppFee                    float64 `orm:"column(app_fee);null;digits(18);decimals(2)" description:"申请费用"`
	ProcFee                   float64 `orm:"column(proc_fee);null;digits(18);decimals(2)" description:"处理费"`
	OverFee                   float64 `orm:"column(over_fee);null;digits(18);decimals(2)" description:"逾期费用"`
	AdvRepayFee               float64 `orm:"column(adv_repay_fee);null;digits(18);decimals(2)" description:"提前还款费用"`
	ExtsnFee                  float64 `orm:"column(extsn_fee);null;digits(18);decimals(2)" description:"展期费用"`
	RepayPlanFee              float64 `orm:"column(repay_plan_fee);null;digits(18);decimals(2)" description:"还款计划调整费用"`
	Tax                       float64 `orm:"column(tax);null;digits(18);decimals(2)" description:"税"`
	MakelnEmpnbr              string  `orm:"column(makeln_empnbr);size(20);null" description:"放款人员工号"`
	MakelnComnt               string  `orm:"column(makeln_comnt);size(200);null" description:"放款说明"`
	MakelnOrgNo               string  `orm:"column(makeln_org_no);size(20);null" description:"放款机构号"`
	ActlMakelnDt              string  `orm:"column(actl_makeln_dt);type(date);null" description:"实际放款日期"`
	AplyMakelnTm              string  `orm:"column(aply_makeln_tm);type(time);null" description:"申请放款时间"`
}

func (t *T_loan_loan_app) TableName() string {
	return "t_loan_loan_app"
}

func InsertT_loan_loan_app(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_loan_app).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_loan_appTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_loan_app)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_loan_appTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_loan_app where makeln_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["MakelnAplySn"]).Exec()
	return err
}

func QueryT_loan_loan_app(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_loan_app panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_loan_app))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo && k != "Flag" {
			if k == "StartPayPlatDate" {
				qs = qs.Filter("aply_makeln_dt__gte", v)
			} else if k == "EndPayPlatDate" {
				qs = qs.Filter("aply_makeln_dt__lte", v)
			} else if k == "CustName" {
				qs = qs.Filter("cust_name__icontains", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if params["Flag"] == "9" {
		if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("-FinlModfyDt", "-FinlModfyTm").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if params["Flag"] == "8" {
		if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("aply_makeln_dt", ).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_loan_appById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_loan_app)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_loan_app(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_loan_app panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_loan_app)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
