package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_role_sens struct {
	RoleNo          string `orm:"column(role_no);size(50);pk" description:"角色编号  pk"`
	SensTypCd       string `orm:"column(sens_typ_cd);size(1)" description:"敏感性类型代码  pk"`
	CrtTelrNo       string `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号 "`
	CrtDt           string `orm:"column(crt_dt);type(time);null" description:"创建日期 "`
	CrtTm           string `orm:"column(crt_tm);type(time);null" description:"创建时间 "`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期 "`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间 "`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号 "`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号 "`
	TccState        int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_role_sens) TableName() string {
	return "t_role_sens"
}

func InsertT_role_sens(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_role_sens).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_role_sensTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_role_sens)).Filter("role_no", maps["RoleNo"]).
		Filter("sens_typ_cd", maps["SensTypCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_role_sensTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_role_sens where role_no=? and sens_typ_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["RoleNo"], maps["SensTypCd"], maps["BizSysNo"]).Exec()
	return err
}

func QueryT_role_sens(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_role_sens panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_role_sens))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_role_sensById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	RoleNo := params["RoleNo"]
	SensTypCd := params["SensTypCd"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_role_sens)).Filter("role_no", RoleNo).Filter("sens_typ_cd", SensTypCd).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_role_sens(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_role_sens panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	RoleNo := params["RoleNo"]
	SensTypCd := params["SensTypCd"]
	_, err = o.QueryTable(new(T_role_sens)).Filter("role_no", RoleNo).Filter("sens_typ_cd", SensTypCd).Filter("tcc_state", 0).Update(params)
	return err
}
func DeleteT_role_sens(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("DeleteT_role_sens panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	RoleNo := params["RoleNo"]
	sql := "DELETE FROM t_role_sens WHERE role_no=? AND tcc_state=0"
	_, err = o.Raw(sql, RoleNo).Exec()
	return err
}
