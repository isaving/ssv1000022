package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_impa_regis struct {
	LoanAcctNo      string  `orm:"column(loan_acct_no);size(32);pk" description:"贷款账号  与放款合约号一致"`
	SeqNo           int     `orm:"column(seq_no)" description:"序号 pk"`
	DvalBizAplyNo   string  `orm:"column(dval_biz_aply_no);size(40);null" description:"减值业务申请编号  pk"`
	DvalDlwthStusCd string  `orm:"column(dval_dlwth_stus_cd);size(2);null" description:"减值处理状态代码  1:申请待审批 2:审批通过 3:审批退回 4:审批拒绝"`
	CtrtNo          string  `orm:"column(ctrt_no);size(22);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+分片序号2位"`
	CustNo          string  `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性crm兼顾启信宝eid长度"`
	CustNm          string  `orm:"column(cust_nm);size(120);null" description:"客户名称  记录对公客户的完整中文名称,该名称应是经过法律程序登记并能够在公共活动中使用的名称"`
	LoanProdtNo     string  `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号 "`
	LoanProdtVersNo string  `orm:"column(loan_prodt_vers_no);size(10);null" description:"贷款产品版本编号  年月日8位+2位顺序号"`
	LoanProdtNm     string  `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称 "`
	LoanGuarManrCd  string  `orm:"column(loan_guar_manr_cd);size(2);null" description:"贷款担保方式代码  参考标准代码:cd0090 2"`
	GrantDt         string  `orm:"column(grant_dt);type(date);null" description:"发放日期 "`
	MatrDt          string  `orm:"column(matr_dt);type(date);null" description:"到期日期  白名单的到期日"`
	RepayManrCd     string  `orm:"column(repay_manr_cd);size(3);null" description:"还款方式代码  参考标准代码:cd0062"`
	LoanRiskClsfCd  string  `orm:"column(loan_risk_clsf_cd);size(5);null" description:"贷款风险分类代码  参考标准代码:cd0057"`
	TranDvalComnt   string  `orm:"column(tran_dval_comnt);size(200);null" description:"转减值说明 "`
	DvalFlg         string  `orm:"column(dval_flg);size(1);null" description:"减值标志  0否1是"`
	DuePrin         float64 `orm:"column(due_prin);null;digits(18);decimals(2)" description:"拖欠本金 "`
	DuePrinBgnDt    string  `orm:"column(due_prin_bgn_dt);type(date);null" description:"拖欠本金开始日期 "`
	OvdueIntr       float64 `orm:"column(ovdue_intr);null;digits(18);decimals(2)" description:"逾期利息  客户利息逾期金额汇总"`
	IntrOvdueBgnDt  string  `orm:"column(intr_ovdue_bgn_dt);type(date);null" description:"利息逾期开始日期 "`
	OvduePnltint    float64 `orm:"column(ovdue_pnltint);null;digits(18);decimals(2)" description:"逾期罚息 "`
	PnltintBgnDt    string  `orm:"column(pnltint_bgn_dt);type(date);null" description:"罚息开始日期 "`
	DvalAplyDt      string  `orm:"column(dval_aply_dt);type(date);null" description:"减值申请日期 "`
	AplyEmpnbr      string  `orm:"column(aply_empnbr);size(6);null" description:"申请员工号 "`
	FinlModfyDt     string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期 "`
	FinlModfyTm     string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间 "`
	FinlModfyOrgNo  string  `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号 "`
	TccState        int     `orm:"column(tcc_state);null"`
}

func (t *T_impa_regis) TableName() string {
	return "t_impa_regis"
}

func InsertT_impa_regis(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_impa_regis).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_impa_regisTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_impa_regis)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_impa_regisTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_impa_regis where loan_acct_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanAcctNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_impa_regis(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_impa_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_impa_regis))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_impa_regisById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	LoanAcctNo := params["LoanAcctNo"]
	SeqNo := params["SeqNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_impa_regis)).Filter("loan_acct_no", LoanAcctNo).Filter("seq_no", SeqNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_impa_regis(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_impa_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	LoanAcctNo := params["LoanAcctNo"]
	SeqNo := params["SeqNo"]
	_, err = o.QueryTable(new(T_impa_regis)).Filter("loan_acct_no", LoanAcctNo).Filter("seq_no", SeqNo).Filter("tcc_state", 0).Update(params)
	return err
}
