package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_itlg_wind_appr_resu struct {
	CrdtAplySn         string    `orm:"column(crdt_aply_sn);size(32);pk" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	SeqNo              int       `orm:"column(seq_no)" description:"序号  PK"`
	CustNo             string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	LoanProdtNo        string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	SggestQta          float64   `orm:"column(sggest_qta);null;digits(18);decimals(2)" description:"建议额度"`
	DcmkTypCd          string    `orm:"column(dcmk_typ_cd);size(1);null" description:"决策类型代码  1-通过,2-转人工调查,3-转人工审批,4-拒绝"`
	DcmkDescr          string    `orm:"column(dcmk_descr);size(200);null" description:"决策描述"`
	TxResultStusCd     string    `orm:"column(tx_result_stus_cd);size(4);null" description:"交易结果状态代码  0000-完成正常评分信审,9999-击中排除规则"`
	TxResultDescr      string    `orm:"column(tx_result_descr);size(200);null" description:"交易结果描述  如果满足排除规则该字段展示满足排除规则说明,多条排除规则使用,分隔"`
	CreditRatCd        string    `orm:"column(credit_rat_cd);size(2);null" description:"信用评级代码  参考标准代码:CD0026"`
	NegtRsn            string    `orm:"column(negt_rsn);size(200);null" description:"负面原因  负面原因最多展示4条"`
	Bp                 int       `orm:"column(bp);null" description:"bp"`
	AdjDrctCd          string    `orm:"column(adj_drct_cd);size(1);null" description:"调整方向代码  U-向上,D-下调"`
	HitExcludRuleComnt string    `orm:"column(hit_exclud_rule_comnt);size(512);null" description:"击中排除规则说明"`
	BlklistTypCd       string    `orm:"column(blklist_typ_cd);size(1);null" description:"黑名单类型代码"`
	MonIncomeAmt       float64   `orm:"column(mon_income_amt);null;digits(18);decimals(2)" description:"月收入金额"`
	ScoreCardGrpgInfo  string    `orm:"column(score_card_grpg_info);size(200);null" description:"评分卡分组信息"`
	EvaltScr           float64   `orm:"column(evalt_scr);null;digits(18);decimals(2)" description:"评定分数"`
	RatCd              string    `orm:"column(rat_cd);size(2);null" description:"评级代码"`
	FinlModfyDt        string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm        string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo     string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo    string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int          `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_itlg_wind_appr_resu) TableName() string {
	return "t_itlg_wind_appr_resu"
}

func InsertT_itlg_wind_appr_resu(o orm.Ormer, maps orm.Params) error {
	mapOne := []orm.Params{}
	sql := "select max(seq_no)+1 SeqNo from t_itlg_wind_appr_resu where crdt_aply_sn=? and tcc_state=0"
	o.Raw(sql, maps["CrdtAplySn"]).Values(&mapOne)
	Seq := mapOne[0]["SeqNo"]
	var SeqNo int
	if Seq == nil {
		SeqNo = 1
	} else {
		SeqNo, _ = strconv.Atoi(Seq.(string))
	}
	maps["SeqNo"] = SeqNo
	sql = util.BuildSql(maps, new(T_itlg_wind_appr_resu).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_itlg_wind_appr_resuTccState(o orm.Ormer, maps orm.Params) error {
	mapOne := []orm.Params{}
	sql := "select max(seq_no)+1 SeqNo from t_itlg_wind_appr_resu where crdt_aply_sn=? and tcc_state=0"
	o.Raw(sql, maps["CrdtAplySn"]).Values(&mapOne)
	Seq := mapOne[0]["SeqNo"]
	var SeqNo int
	if Seq == nil {
		SeqNo = 1
	} else {
		SeqNo, _ = strconv.Atoi(Seq.(string))
	}
	_, err := o.QueryTable(new(T_itlg_wind_appr_resu)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("seq_no", SeqNo).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_itlg_wind_appr_resuTccState(o orm.Ormer, maps orm.Params) error {
	mapOne := []orm.Params{}
	sql := "select max(seq_no)+1 SeqNo from t_itlg_wind_appr_resu where crdt_aply_sn=? and tcc_state=0"
	o.Raw(sql, maps["CrdtAplySn"]).Values(&mapOne)
	Seq := mapOne[0]["SeqNo"]
	var SeqNo int
	if Seq == nil {
		SeqNo = 1
	} else {
		SeqNo, _ = strconv.Atoi(Seq.(string))
	}
	sql = "delete from t_itlg_wind_appr_resu where crdt_aply_sn=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"], SeqNo).Exec()
	return err
}

func QueryT_itlg_wind_appr_resu(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_itlg_wind_appr_resu panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	//Flag := params["Flag"]
	//if Flag=="9"{
	//	maps,err =QueryT_itlg_wind_appr_resuFlag9(params,state)
	//	return
	//}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_itlg_wind_appr_resu))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_itlg_wind_appr_resuById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_itlg_wind_appr_resu)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_itlg_wind_appr_resu(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_itlg_wind_appr_resu panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_itlg_wind_appr_resu)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
//func QueryT_itlg_wind_appr_resuFlag9(params orm.Params, state int8) (maps []orm.Params, err error) {
//	o := orm.NewOrm()
//	sql :="SELECT " +
//		"t1.`sggest_qta` SggestQta," +
//		"t1.`dcmk_typ_cd` DcmkTypCd," +
//		"t1.`adj_drct_cd` IntrtFlotDrctCd," +
//		"t1.`bp` BpFlotVal," +
//		"t2.`brwmny_deadl` BrwmnyDeadl," +
//		"t2.`brwmny_deadl_unit_cd` BrwmnyDeadlUnitCd," +
//		"t2.`repay_manr_cd`RepayManrCd " +
//		"FROM t_itlg_wind_appr_resu t1 ,t_loan_term_repay_method t2 " +
//		"WHERE t1.`cust_no`=t2.`cust_no` AND t1.`loan_prodt_no`=t2.`loan_prodt_no` AND t1.`tcc_state`=0 AND t2.`tcc_state`=0"
//	if params["CustNo"] !=nil {
//		sql = sql + " and t1.cust_no=\""+params["CustNo"].(string)+"\""
//	}
//	if params["LoanProdtNo"] !=nil {
//		sql = sql + " and t1.loan_prodt_no=\""+params["LoanProdtNo"].(string)+"\""
//	}
//	sql = sql +" GROUP BY t1.cust_no,t1.loan_prodt_no "
//	_ ,err = o.Raw(sql).Values(&maps)
//	return
//}