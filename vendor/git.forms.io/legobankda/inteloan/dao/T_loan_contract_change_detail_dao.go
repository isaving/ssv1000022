package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_contract_change_detail struct {
	CtrtNo            string    `orm:"column(ctrt_no);size(32);pk" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	SeqNo             int       `orm:"column(seq_no)" description:"序号  PK"`
	CustNo            string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CustName          string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	TxSn              string    `orm:"column(tx_sn);size(32);null" description:"交易流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,随机整数8位编号规则:1',随机数生成:(int)(Math.random()*100000000)"`
	NodeNm            string    `orm:"column(node_nm);size(120);null" description:"节点名称"`
	OperTypCd         string    `orm:"column(oper_typ_cd);size(4);null" description:"操作类型代码  01-录入02-拟制03-提交04-退回05-复核06-审核07-审批08-生效09-冻结10-冻结审批11-解冻12-解冻审批13-撤销14-废除15-终止16-失效21-处理22-任务转派23-任务撤回24-转派确认25-评价26-整改"`
	OperBefCtrtStusCd string    `orm:"column(oper_bef_ctrt_stus_cd);size(2);null" description:"操作前合同状态代码  A1-合同录入A2-合同审查中A3-合同审查完毕A4-合同生效A5-合同冻结A6-合同中止A7-合同执行完毕A8-合同解冻审批中A9-合同冻结审批中A0-合同新增"`
	OperAftCtrtStusCd string    `orm:"column(oper_aft_ctrt_stus_cd);size(2);null" description:"操作后合同状态代码  A1-合同录入A2-合同审查中A3-合同审查完毕A4-合同生效A5-合同冻结A6-合同中止A7-合同执行完毕A8-合同解冻审批中A9-合同冻结审批中A0-合同新增"`
	AfltInfo          string    `orm:"column(aflt_info);size(200);null" description:"附属信息"`
	OperEmpnbr        string    `orm:"column(oper_empnbr);size(6);null" description:"操作员工号  公共服务提供类型及长度"`
	OperOrgNo         string    `orm:"column(oper_org_no);size(4);null" description:"操作机构号  公共服务提供类型及长度"`
	OperTm            string    `orm:"column(oper_tm);type(time);null" description:"操作时间"`
	OperResultCd      string    `orm:"column(oper_result_cd);size(200);null" description:"操作结果说明"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_contract_change_detail) TableName() string {
	return "t_loan_contract_change_detail"
}

func InsertT_loan_contract_change_detail(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_contract_change_detail where ctrt_no=?"
		o.Raw(sql, maps["CtrtNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_contract_change_detail).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_contract_change_detailTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_contract_change_detail where ctrt_no=?"
		o.Raw(sql, maps["CtrtNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_contract_change_detail)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("seq_no",maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_contract_change_detailTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_contract_change_detail where ctrt_no=?"
		o.Raw(sql, maps["CtrtNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_contract_change_detail where ctrt_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CtrtNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_contract_change_detail(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_contract_change_detail panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_contract_change_detail))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_contract_change_detailById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_contract_change_detail)).Filter("ctrt_no", maps["CtrtNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_contract_change_detail(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_contract_change_detail panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_contract_change_detail)).Filter("ctrt_no", maps["CtrtNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
