package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_itlg_wind_appr_lend_resu struct {
	MakelnAplySn       string    `orm:"column(makeln_aply_sn);size(32);pk" description:"放款申请流水号"`
	SeqNo              int       `orm:"column(seq_no)" description:"序号"`
	CustNo             string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	DcmkApprvResultCd  string    `orm:"column(dcmk_apprv_result_cd);size(3);null" description:"决策审批结果代码  1-通过2-拒绝"`
	DcmkApprvDescr     string    `orm:"column(dcmk_apprv_descr);size(200);null" description:"决策审批描述"`
	HitExcludRuleComnt string    `orm:"column(hit_exclud_rule_comnt);size(512);null" description:"击中排除规则说明"`
	BlklistTypCd       string    `orm:"column(blklist_typ_cd);size(1);null" description:"黑名单类型代码  等待决策贯标后补充"`
	FinlModfyDt        string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm        string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo     string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModifrEmpnbr   string    `orm:"column(finl_modifr_empnbr);size(6);null" description:"最后修改人员工号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_itlg_wind_appr_lend_resu) TableName() string {
	return "t_itlg_wind_appr_lend_resu"
}

func InsertT_itlg_wind_appr_lend_resu(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_itlg_wind_appr_lend_resu).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_itlg_wind_appr_lend_resuTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_itlg_wind_appr_lend_resu)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_itlg_wind_appr_lend_resuTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_itlg_wind_appr_lend_resu where makeln_aply_sn=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["MakelnAplySn"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_itlg_wind_appr_lend_resu(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_itlg_wind_appr_lend_resu panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_itlg_wind_appr_lend_resu))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_itlg_wind_appr_lend_resuById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_itlg_wind_appr_lend_resu)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_itlg_wind_appr_lend_resu(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_itlg_wind_appr_lend_resu panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_itlg_wind_appr_lend_resu)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
