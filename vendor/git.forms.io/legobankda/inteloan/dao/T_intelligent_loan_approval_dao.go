package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intelligent_loan_approval struct {
	BusinessTriggerType           string  `orm:"column(business_trigger_type);size(3);null"`
	BusinessType                  string  `orm:"column(business_type);size(3);null"`
	LoanProdtNo                   string  `orm:"column(loan_prodt_no);size(96);null"`
	LoanProdtVersno               string  `orm:"column(loan_prodt_versno);size(15);null"`
	Applyforthelimitofexpenditure float32 `orm:"column(applyforthelimitofexpenditure);null"`
	ChannelType                   string  `orm:"column(channel_type);size(30);null"`
	HandlerCode                   string  `orm:"column(handler_code);size(45);null"`
	AgentorganizationCode         string  `orm:"column(agentorganization_code);size(45);null"`
	Indvcrtfno                    string  `orm:"column(indvcrtfno);size(150);null"`
	Indvcrtftypcd                 string  `orm:"column(indvcrtftypcd);size(15);null"`
	CustName                      string  `orm:"column(cust_name);size(150);null"`
	CustNo                        string  `orm:"column(cust_no);pk;null"`
	ResidentialAddress            string  `orm:"column(residential_address);size(765);null"`
	Age                           int     `orm:"column(age);null"`
	BlacklistFlag                 string  `orm:"column(blacklist_flag);size(3);null"`
	Phonenumber                   string  `orm:"column(phonenumber);size(150);null"`
	DecisionType                  string  `orm:"column(decision_type);size(3);null"`
	DecisionDescription           string  `orm:"column(decision_description);size(765);null"`
	ExclusionRules                string  `orm:"column(exclusion_rules);size(765);null"`
	BlacklisttypeInvolved         string  `orm:"column(blacklisttype_involved);size(6);null"`
	TccState                      string  `orm:"column(tcc_state);size(3);null"`
}

func (t *T_intelligent_loan_approval) TableName() string {
	return "t_intelligent_loan_approval"
}

func InsertT_intelligent_loan_approval(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intelligent_loan_approval).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intelligent_loan_approvalTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intelligent_loan_approval)).Filter("cust_no", maps["CustNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intelligent_loan_approvalTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intelligent_loan_approval where cust_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustNo"]).Exec()
	return err
}

func QueryT_intelligent_loan_approval(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intelligent_loan_approval panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intelligent_loan_approval))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intelligent_loan_approvalById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intelligent_loan_approval)).Filter("cust_no", maps["CustNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intelligent_loan_approval(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intelligent_loan_approval panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intelligent_loan_approval)).Filter("cust_no", maps["CustNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
