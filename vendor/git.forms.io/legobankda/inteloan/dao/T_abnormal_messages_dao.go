package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_abnormal_messages struct {
	MessageId          string `orm:"column(message_id);size(255);pk"`
	MessageType        int    `orm:"column(message_type)" description:"0-普通消息，1-批量消息"`
	SerialNo           int    `orm:"column(serial_no)"`
	Status             int    `orm:"column(status);null" description:"消息状态：0-发送中  1-发送成功  2-发送失败 3-重试中 9-状态未明"`
	TextTmplTypCd      string `orm:"column(text_tmpl_typ_cd);size(2);null"`
	Reason             string `orm:"column(reason);size(255);null"`
	TemplateId         string `orm:"column(template_id);size(255);null"`
	CallbackId         string `orm:"column(callback_id);size(255);null"`
	ChannelId          string `orm:"column(channel_id);size(255);null"`
	ProjectId          string `orm:"column(project_id);size(255);null"`
	OrgId              string `orm:"column(org_id);size(255);null"`
	TopicId            string `orm:"column(topic_id);size(255);null"`
	SendTime           string `orm:"column(send_time);size(255);null"`
	SourceEmailAddress string `orm:"column(source_email_address);size(255);null"`
	Title              string `orm:"column(title);size(255);null"`
	TargetList         string `orm:"column(target_list);size(255);null"`
	TargetCcList       string `orm:"column(target_cc_list);size(255);null"`
	AttachList         string `orm:"column(attach_list);size(255);null"`
	Body               string `orm:"column(body);size(255);null"`
	DecodeBody         string `orm:"column(decode_body);size(255);null"`
	SendType           string `orm:"column(send_type);size(4);null" description:"发送方式 01-同步 02-异步"`
	ServeType          string `orm:"column(serve_type);size(4);null" description:"服务类型 01 授信审批 02提额审批 03还款入账"`
	ErrorMsgInfo       string `orm:"column(error_msg_info);size(200);null" description:"错误信息描述"`
	FileId             string `orm:"column(file_id);size(30);null" description:"文件ID"`
	GlobalBizSeqno     string `orm:"column(global_biz_seqno);size(40);null" description:"业务流水号"`
	ReturnCode         string `orm:"column(return_code);size(10);null" description:"返回响应码 0-邮件发送成功"`
	ReturnMsg          string `orm:"column(return_msg);size(10);null" description:"响应信息 SUCCESS-发送成功"`
	DeliveryTime       string `orm:"column(delivery_time);size(20);null" description:"发送时间"`
	ResponseTime       string `orm:"column(response_time);size(20);null" description:"响应时间"`
	Data               string `orm:"column(data);size(10);null" description:"邮件发送结果 true-成功，false-失败"`
	FinlModfyDt        string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm        string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo     string `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo    string `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState           int    `orm:"column(tcc_state);null" description:"tcc状态 0-Normal，1-Insert"`
}

func (t *T_abnormal_messages) TableName() string {
	return "t_abnormal_messages"
}

func InsertT_abnormal_messages(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_abnormal_messages).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_abnormal_messagesTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_abnormal_messages)).Filter("message_id", maps["MessageId"]).
		Filter("message_type", maps["MessageType"]).
		Filter("serial_no", maps["SerialNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_abnormal_messagesTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_abnormal_messages where message_id=? and message_type=? and serial_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["MessageId"], maps["MessageType"], maps["SerialNo"]).Exec()
	return err
}

func QueryT_abnormal_messages(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_abnormal_messages panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_abnormal_messages))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_abnormal_messagesById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_abnormal_messages)).Filter("message_id", maps["MessageId"]).
		Filter("message_type", maps["MessageType"]).
		Filter("serial_no", maps["SerialNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_abnormal_messages(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_abnormal_messages panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_abnormal_messages)).Filter("message_id", maps["MessageId"]).
		Filter("message_type", maps["MessageType"]).
		Filter("serial_no", maps["SerialNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
