package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sms_sending_details struct {
	SysId          string    `orm:"column(sys_Id);pk" description:"渠道标识"`
	ServeType      string    `orm:"column(serve_type);size(32);null" description:"服务类型"`
	BizSn          string    `orm:"column(biz_sn);size(32);null" description:"业务流水号"`
	CustNo          string   `orm:"column(cust_no);size(11);null" description:"手机号码"`
	TelNo          string    `orm:"column(tel_no);size(11);null" description:"手机号码"`
	SmsTypCd       string    `orm:"column(sms_typ_cd);size(2);null" description:"短信类型"`
	SmsTmplNo      string    `orm:"column(sms_tmpl_no);size(32);null" description:"短信模板编号"`
	SmsDate        string    `orm:"column(sms_date);type(date);null" description:"日期"`
	SmsTime        string    `orm:"column(sms_time);type(time);null" description:"时间"`
	SmsCntList     string    `orm:"column(sms_cnt_list);size(20);null" description:"短信要素列表"`
	SmsTmplContent string    `orm:"column(sms_tmpl_content);size(200);null" description:"短信文本内容"`
	TccState       int       `orm:"column(tcc_state);null"`
}

func (t *T_sms_sending_details) TableName() string {
	return "t_sms_sending_details"
}

func InsertT_sms_sending_details(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sms_sending_details).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sms_sending_detailsTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sms_sending_details)).Filter("biz_sn", maps["BizSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sms_sending_detailsTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_sms_sending_details where biz_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizSn"]).Exec()
	return err
}

func QueryT_sms_sending_details(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sms_sending_details panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_sms_sending_details))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sms_sending_detailsById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sms_sending_details)).Filter("biz_sn", maps["BizSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sms_sending_details(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sms_sending_details panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sms_sending_details)).Filter("biz_sn", maps["BizSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
