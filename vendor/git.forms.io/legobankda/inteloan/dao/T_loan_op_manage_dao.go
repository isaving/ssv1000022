package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_op_manage struct {
	LoanProdtNo      string    `orm:"column(loan_prodt_no);size(20);pk" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:b基础产品,s可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:s0200001"`
	LoanProdtNm      string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	RoleNo           string    `orm:"column(role_no);size(50)" description:"角色编号  系统目前无法识别,由员工中心维护后才知道有哪些角色"`
	Empnbr           string    `orm:"column(empnbr);size(6)" description:"员工号  描述银行为统一管理,根据既定规则生成并分配给员工的唯一编码,在全行内具有唯一性"`
	OrgNo            string    `orm:"column(org_no);size(4)" description:"机构号        "`
	OrgNm            string    `orm:"column(org_nm);size(120);null" description:"机构名称      "`
	EmplyName        string    `orm:"column(emply_name);size(60);null" description:"员工姓名      "`
	RoleNm           string    `orm:"column(role_nm);size(60);null" description:"角色名称      "`
	ValidFlg         string    `orm:"column(valid_flg);size(1);null" description:"有效标志  0-否,1-是"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  "`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  "`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModifrEmpnbr string    `orm:"column(finl_modifr_empnbr);size(6);null" description:"最后修改人员工号"`
	TccState            int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_op_manage) TableName() string {
	return "t_loan_op_manage"
}

func InsertT_loan_op_manage(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_op_manage).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_op_manageTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_op_manage)).Filter("loan_prodt_no", maps["LoanProdtNo"]).
		Filter("role_no", maps["RoleNo"]).Filter("empnbr", maps["Empnbr"]).Filter("org_no", maps["OrgNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_op_manageTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_op_manage where loan_prodt_no=? and role_no=? and empnbr=? and org_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanProdtNo"], maps["RoleNo"],maps["Empnbr"],maps["OrgNo"]).Exec()
	return err
}

func QueryT_loan_op_manage(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_op_manage panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_op_manage))
	flag := params["Flag"]
	delete(params,"Flag")
	if flag == "9" {//lzh
		sql :="select count(task_dlwth_stus_cd=01) taskcount,t2.empnbr from t_loan_op_manage t1,t_loan_onduty_control t2 left join t_loan_approve_task t3 on t2.empnbr=t3.distr_person_empnbr "+
		"where t1.empnbr=t2.empnbr "
		if params["LoanProdtNo"] != nil{
			sql = sql +" and t1.loan_prodt_no=\""+params["LoanProdtNo"].(string)+"\""
		}
		if params["RoleNo"] != nil{
			sql = sql +" and t1.role_no=\""+params["RoleNo"].(string)+"\""
		}
		if params["OrgNo"] != nil{
			sql = sql +" and t1.org_no=\""+params["OrgNo"].(string)+"\""
		}
		if params["MtguarStusCd"] != nil{
			sql = sql +" and t2.mtguar_stus_cd=\""+params["MtguarStusCd"].(string)+"\""
		}
		/*if params["TaskDlwthStusCd"] != nil{
			sql = sql +" and t3.task_dlwth_stus_cd=\""+params["TaskDlwthStusCd"].(string)+"\""
		}*/
		sql = sql + " GROUP BY t2.empnbr  ORDER BY taskcount ASC"
		_,err = o.Raw(sql).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_op_manageById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_op_manage)).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("role_no", maps["RoleNo"]).
		Filter("empnbr", maps["Empnbr"]).Filter("org_no", maps["OrgNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_op_manage(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_op_manage panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_op_manage)).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("role_no", maps["RoleNo"]).
	Filter("empnbr", maps["Empnbr"]).Filter("org_no", maps["OrgNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
