package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_contract_reg struct {
	CtrtNo          string    `orm:"column(ctrt_no);pk" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	IndvCrtfTypCd   string    `orm:"column(indv_crtf_typ_cd);size(1);null" description:"个人证件类型代码"`
	IndvCrtfNo      string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CtrtStusCd      string    `orm:"column(ctrt_stus_cd);size(5);null" description:"合同状态代码  A1-合同录入A2-合同审查中A3-合同审查完毕A4-合同生效A5-合同冻结A6-合同中止A7-合同执行完毕A8-合同解冻审批中A9-合同冻结审批中A0-合同新增"`
	CtrtStartDt     string    `orm:"column(ctrt_start_dt);type(date);null" description:"合同起始日期"`
	CtrtMatrDt      string    `orm:"column(ctrt_matr_dt);type(date);null" description:"合同到期日期"`
	ApprvDt         string    `orm:"column(apprv_dt);type(date);null" description:"审批日期"`
	SignOrgNo       string    `orm:"column(sign_org_no);size(4);null" description:"签约机构号"`
	OprOrgNo        string    `orm:"column(opr_org_no);size(4);null" description:"经办机构号"`
	OprorEmpnbr     string    `orm:"column(opror_empnbr);size(6);null" description:"经办人员工号"`
	ComslNo         string    `orm:"column(comsl_no);size(20);null" description:"公章编号"`
	TmplNo          string    `orm:"column(tmpl_no);size(32);null" description:"模板编号"`
	CtrtImgNo       string    `orm:"column(ctrt_img_no);size(40);null" description:"合同影像编号"`
	BrwmnyCtrtTypCd string    `orm:"column(brwmny_ctrt_typ_cd);size(40);null" description:"借款合同类型代码"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int8      `orm:"column(tcc_state);null"`
}

func (t *T_loan_contract_reg) TableName() string {
	return "t_loan_contract_reg"
}

func InsertT_loan_contract_reg(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_contract_reg).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_contract_regTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_contract_reg)).Filter("ctrt_no", maps["CtrtNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_contract_regTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_contract_reg where ctrt_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CtrtNo"]).Exec()
	return err
}

func QueryT_loan_contract_reg(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_contract_reg panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	if flag !=nil && flag=="9"{
		sql := "SELECT t1.`ctrt_no` CtrtNo,t1.`cust_no` CustNo,t1.`ctrt_stus_cd` CtrtStusCd,t1.`ctrt_start_dt` CtrtStartDt,t1.`ctrt_matr_dt` CtrtMatrDt,"+
			"t2.`ctrt_amt` CtrtAmt,t2.`loan_prodt_nm` LoanProdtNm,t2.`cust_name` CustName "+
		"FROM `t_loan_contract_reg` t1,`t_loan_indiv_contract_info`t2  WHERE t1.`ctrt_no`=t2.`ctrt_no` AND t1.tcc_state=0 AND t2.tcc_state=0 "
		if	params["CtrtNo"] !=nil {
			sql = sql +"And t1.`ctrt_no`=\""+params["CtrtNo"].(string)+"\" "
		}
		if	params["CustNo"] !=nil {
			sql = sql +"And t1.`cust_no`=\""+params["CustNo"].(string)+"\" "
		}
		if	params["CtrtStusCd"] !=nil {
			sql = sql +"And t1.`ctrt_stus_cd`=\""+params["CtrtStusCd"].(string)+"\" "
		}
		if 	params["CtrtStartDt"] !=nil {
			sql = sql +"And t1.`ctrt_start_dt`>=\""+params["CtrtStartDt"].(string)+"\" "
		}
		if	params["CtrtMatrDt"] !=nil {
			sql = sql +"And t1.`ctrt_matr_dt`<=\""+params["CtrtMatrDt"].(string)+"\" "
		}
		_,err =o.Raw(sql).Values(&maps)
		return
	}
	if flag !=nil && flag=="8"{
		sql :="SELECT t1.`ctrt_no` CtrtNo,t1.`cust_no` CustNo,t2.`cust_name` CustName,t2.`loan_prodt_nm` LoanProdtNm,t2.`cur_cd` CurCd,t2.`ctrt_amt` CtrtAmt,t1.`ctrt_start_dt` CtrtStartDt,t1.`ctrt_matr_dt` CtrtMatrDt,t2.`loan_usage_cd` LoanUsageCd "+
			"FROM `t_loan_contract_reg` t1,t_loan_indiv_contract_info t2 WHERE t1.`ctrt_no`=t2.`ctrt_no` AND t1.tcc_state=0 AND t2.tcc_state=0"
		if	params["CtrtNo"] !=nil {
			sql = sql +" AND t1.`ctrt_no`=\""+params["CtrtNo"].(string)+"\" "
		}
		if	params["CustNo"] !=nil {
			sql = sql +" AND t1.`cust_no`=\""+params["CustNo"].(string)+"\" "
		}
		_,err =o.Raw(sql).Values(&maps)
		return
	}

	// 给BFS贷款合约登记调，IL166625
	if flag !=nil && flag=="IL166625"{
		if _, err = o.QueryTable(new(T_loan_contract_reg)).Filter("ctrt_no", params["CtrtNo"].(string)).Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	qs := o.QueryTable(new(T_loan_contract_reg))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_contract_regById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_contract_reg)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_contract_reg(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_contract_reg panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_contract_reg)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
