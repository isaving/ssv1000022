package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_jnl_entry struct {
	JrnlzId            string    `orm:"column(jrnlz_id);pk" description:"分录id  分录的唯一编号,"`
	AcctnDt            string    `orm:"column(acctn_dt);type(date);null" description:"会计日期"`
	BizFolnNo          string    `orm:"column(biz_foln_no);size(32);null" description:"业务跟踪号"`
	SysFolnNo          string    `orm:"column(sys_foln_no);size(32);null" description:"系统跟踪号"`
	TxTm               string    `orm:"column(tx_tm);type(time);null" description:"交易时间"`
	TxOrgNo            string    `orm:"column(tx_org_no);size(4);null" description:"交易机构号"`
	ActngOrgNo         string    `orm:"column(actng_org_no);size(4);null" description:"账务机构号"`
	CurCd              string    `orm:"column(cur_cd);size(4);null" description:"币种代码  参考标准代码:CD0040"`
	DebitCrdtFlgCd     string    `orm:"column(debit_crdt_flg_cd);size(1);null" description:"借贷标志代码  D-借,C-贷,B-借贷都检查-"`
	TxAmt              float64   `orm:"column(tx_amt);null;digits(18);decimals(2)" description:"交易金额"`
	SubjNo             string    `orm:"column(subj_no);size(8);null" description:"科目编号"`
	ReqePtySoftProdtCd string    `orm:"column(reqe_pty_soft_prodt_cd);size(3);null" description:"请求方软件产品编码  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'"`
	KepacctSuccFlg     string    `orm:"column(kepacct_succ_flg);size(1);null" description:"记账成功标志  描述记账是否成功,0否1是,"`
	AbstCd             string    `orm:"column(abst_cd);size(3);null" description:"摘要代码"`
	EngineVaritCd      string    `orm:"column(engine_varit_cd);size(4);null" description:"引擎变式编码"`
	AcctBookCategCd    string    `orm:"column(acct_book_categ_cd);size(5);null" description:"账套类别代码  CN001-川农,CN002-预留1,CN003-预留2,CN004-预留3"`
	SumId              string    `orm:"column(sum_id);size(20);null" description:"汇总id      "`
	WrtffFlg           string    `orm:"column(wrtff_flg);size(1);null" description:"冲销标志    "`
	FinlModfyDt        string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm        string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo     string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo    string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_jnl_entry) TableName() string {
	return "t_ae_jnl_entry"
}

func InsertT_ae_jnl_entry(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_jnl_entry).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_jnl_entryTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_jnl_entry)).Filter("jrnlz_id", maps["JrnlzId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_jnl_entryTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_jnl_entry where jrnlz_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["JrnlzId"]).Exec()
	return err
}

func QueryT_ae_jnl_entry(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_jnl_entry panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_jnl_entry))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_jnl_entryById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_jnl_entry)).Filter("jrnlz_id", maps["JrnlzId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_jnl_entry(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_jnl_entry panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_jnl_entry)).Filter("jrnlz_id", maps["JrnlzId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
