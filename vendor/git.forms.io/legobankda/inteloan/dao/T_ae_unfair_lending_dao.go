package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_unfair_lending struct {
	AcctnDt         string    `orm:"column(acctn_dt);type(date);pk" description:"会计日期"`
	BizFolnNo       string    `orm:"column(biz_foln_no);size(32)" description:"业务跟踪号"`
	SysFolnNo       string    `orm:"column(sys_foln_no);size(32);null" description:"系统跟踪号"`
	MstkAmt         float64   `orm:"column(mstk_amt);null;digits(18);decimals(2)" description:"差错金额"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_unfair_lending) TableName() string {
	return "t_ae_unfair_lending"
}

func InsertT_ae_unfair_lending(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_unfair_lending).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_unfair_lendingTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_unfair_lending)).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("biz_foln_no", maps["BizFolnNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_unfair_lendingTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_unfair_lending where acctn_dt=? and biz_foln_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctnDt"], maps["BizFolnNo"]).Exec()
	return err
}

func QueryT_ae_unfair_lending(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_unfair_lending panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_unfair_lending))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_unfair_lendingById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_unfair_lending)).Filter("acctn_dt", maps["AcctnDt"]).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_unfair_lending(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_unfair_lending panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_unfair_lending)).Filter("acctn_dt", maps["AcctnDt"]).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
