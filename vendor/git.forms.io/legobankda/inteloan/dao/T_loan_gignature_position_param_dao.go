package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_gignature_position_param struct {
	TmplNo                   string `orm:"column(tmpl_no);pk" description:"模板编号"`
	AutogrPtyIdtyCd          string `orm:"column(autogr_pty_idty_cd);size(1);null" description:"签名方身份代码 1-个人 2-行方 3-第三方"`
	SigtPgnum                int    `orm:"column(sigt_pgnum);size(5);null" description:"签章页数"`
	UpOffset                 int    `orm:"column(up_offset);size(5);null" description:"上偏移量"`
	DownOffset               int    `orm:"column(down_offset);size(5);null" description:"下偏移量"`
	LeftOffset               int    `orm:"column(left_offset);size(5);null" description:"左偏移量"`
	RightOffset              int    `orm:"column(right_offset);size(5);null" description:"右偏移量"`
	KeywdNm                  string `orm:"column(keywd_nm);size(120);null" description:"关键字名称"`
	Offset                   int    `orm:"column(offset);null" description:"偏移量"`
	AutogrPlcinKeywdPtyBitCd string `orm:"column(autogr_plcin_keywd_pty_bit_cd);size(1);null" description:"签名置于关键字方位代码1-关键字中心对齐 2-关键字正下方 3-关键字右侧 4-关键字右下角"`
	KeywdMtchSeqCd           string `orm:"column(keywd_mtch_seq_cd);size(2);null" description:"关键字匹配顺序代码 0-全部相同关键字签名 1-正序第一个关键字签名 2-倒序第一关键字签名"`
	TccState          	     int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_gignature_position_param) TableName() string {
	return "t_loan_gignature_position_param"
}

func InsertT_loan_gignature_position_param(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_gignature_position_param).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_gignature_position_paramTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_gignature_position_param)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("autogr_pty_idty_cd", maps["AutogrPtyIdtyCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_gignature_position_paramTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_gignature_position_param where tmpl_no=? and autogr_pty_idty_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplNo"], maps["AutogrPtyIdtyCd"]).Exec()
	return err
}

func QueryT_loan_gignature_position_param(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_gignature_position_param panci "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_gignature_position_param))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_gignature_position_paramById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_gignature_position_param)).Filter("tmpl_no", maps["TmplNo"]).Filter("autogr_pty_idty_cd", maps["AutogrPtyIdtyCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_gignature_position_param(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_gignature_position_param panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_gignature_position_param)).Filter("tmpl_no", maps["TmplNo"]).Filter("autogr_pty_idty_cd", maps["AutogrPtyIdtyCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
