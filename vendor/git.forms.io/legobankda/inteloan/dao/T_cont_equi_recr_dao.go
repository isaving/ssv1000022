package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_cont_equi_recr struct {
	CustNo           string    `orm:"column(cust_no);size(14);pk" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	SeqNo            int       `orm:"column(seq_no)" description:"序号  PK"`
	CustName         string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	CtrtNo           string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	LoanDubilNo      string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	RgtsintClsfNo    string    `orm:"column(rgtsint_clsf_no);size(12);null" description:"权益分类编号"`
	RgtsintNo        string    `orm:"column(rgtsint_no);size(12);null" description:"权益编号"`
	RgtsintUseStusCd string    `orm:"column(rgtsint_use_stus_cd);size(1);null" description:"权益使用状态代码  1:已生效未执行2:已取消3:执行中4:执行完毕"`
	UseDt            string    `orm:"column(use_dt);type(date);null" description:"使用日期"`
	CnclDt           string    `orm:"column(cncl_dt);type(date);null" description:"取消日期"`
	RgtsintMatrDt    string    `orm:"column(rgtsint_matr_dt);type(date);null" description:"权益到期日期"`
	ExecFinhDt       string    `orm:"column(exec_finh_dt);type(date);null" description:"执行完毕日期"`
	TccState         int       `orm:"column(tcc_state);null"`
}

func (t *T_cont_equi_recr) TableName() string {
	return "t_cont_equi_recr"
}

func InsertT_cont_equi_recr(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cont_equi_recr).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cont_equi_recrTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cont_equi_recr)).Filter("cust_no", maps["CustNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cont_equi_recrTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cont_equi_recr where cust_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_cont_equi_recr(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cont_equi_recr panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_cont_equi_recr))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cont_equi_recrById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cont_equi_recr)).Filter("cust_no", maps["CustNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cont_equi_recr(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cont_equi_recr panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cont_equi_recr)).Filter("cust_no", maps["CustNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
