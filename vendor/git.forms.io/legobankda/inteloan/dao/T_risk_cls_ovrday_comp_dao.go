package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_risk_cls_ovrday_comp struct {
	RiskClsfNo              string `orm:"column(risk_clsf_no);pk" description:"风险分类编号 pk"`
	EfftDt                  string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	LoanProdtClsfCd         string `orm:"column(loan_prodt_clsf_cd);size(20);null" description:"贷款产品分类代码  f0211-个人住房贷款 f0219-其他消费贷款"`
	LoanGuarManrCd          string `orm:"column(loan_guar_manr_cd);size(2);null" description:"贷款担保方式代码  参考标准代码:cd0090 10-信用 20-保证 30-抵押 40-质押 51-抵押+保证 52-质押+抵押 53-质押+保证 54-抵押+质押+保证"`
	LoanRiskClsfCd          string `orm:"column(loan_risk_clsf_cd);size(5);null" description:"贷款风险分类代码  参考标准代码:cd0057 11:正常 21:关注 31:次级 41:可疑 51:损失"`
	LoanRiskClsfDescr       string `orm:"column(loan_risk_clsf_descr);size(200);null" description:"贷款风险分类描述  正常关注次级可疑损失"`
	OvdueAmtTypCd           string `orm:"column(ovdue_amt_typ_cd);size(1);null" description:"逾期金额类型代码 本金逾期 利息逾期 本金逾期或利息逾期 本金逾期及利息逾期"`
	OvdueDaysTypCd          string `orm:"column(ovdue_days_typ_cd);size(1);null" description:"逾期天数类型代码 连续逾期天数 累计逾期天数"`
	ExecCycCd               string `orm:"column(exec_cyc_cd);size(2);null" description:"执行周期代码  参考标准代码:cd0075 每日 每月"`
	BatClsfDtCd             string `orm:"column(bat_clsf_dt_cd);size(2);null" description:"批量分类日期代码  每日日终 月末日终"`
	OvdueFloorDays          int    `orm:"column(ovdue_floor_days);null" description:"逾期下限天数 "`
	OvdueCeilDays           int    `orm:"column(ovdue_ceil_days);null" description:"逾期上限天数 "`
	RiskClsfOvdueDecdManrCd string `orm:"column(risk_clsf_ovdue_decd_manr_cd);size(1);null" description:"风险分类逾期判定方式代码 1:按还本付息逾期天数, 2:按贷款到期逾期天数"`
	CrtTelrNo               string `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号 "`
	CrtDt                   string `orm:"column(crt_dt);type(time);null" description:"创建日期 "`
	CrtTm                   string `orm:"column(crt_tm);type(time);null" description:"创建时间 "`
	CrtOrgNo                string `orm:"column(crt_org_no);size(4);null" description:"创建机构号 "`
	RiskClsfValidFlg        string `orm:"column(risk_clsf_valid_flg);size(1);null" description:"风险分类有效标志  0否1是"`
	FinlModfyDt             string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期 "`
	FinlModfyTm             string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间 "`
	FinlModfyOrgNo          string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号 "`
	FinlModfyTelrNo         string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号 "`
	TccState                int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_risk_cls_ovrday_comp) TableName() string {
	return "t_risk_cls_ovrday_comp"
}

func InsertT_risk_cls_ovrday_comp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_risk_cls_ovrday_comp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_risk_cls_ovrday_compTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_risk_cls_ovrday_comp)).Filter("risk_clsf_no", maps["RiskClsfNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_risk_cls_ovrday_compTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_risk_cls_ovrday_comp where risk_clsf_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["RiskClsfNo"]).Exec()
	return err
}

func QueryT_risk_cls_ovrday_comp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_risk_cls_ovrday_comp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params,"Flag")
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_risk_cls_ovrday_comp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "EfftDtStart" {
				qs = qs.Filter("EfftDt__gte", v)
			} else if k == "EfftDtEnd" {
				qs = qs.Filter("EfftDt__lte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if flag=="9"{
		if _, err = qs.Filter("tcc_state", state).OrderBy("-crt_dt","-crt_tm").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag=="8"{
		if _, err = qs.Filter("tcc_state", state).OrderBy("-efft_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Filter("risk_clsf_valid_flg","1").Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_risk_cls_ovrday_compById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_risk_cls_ovrday_comp)).Filter("risk_clsf_no", maps["RiskClsfNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_risk_cls_ovrday_comp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_risk_cls_ovrday_comp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_risk_cls_ovrday_comp)).Filter("risk_clsf_no", maps["RiskClsfNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
