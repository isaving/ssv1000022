package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_accti_acct_deta struct {
	AcctiAcctNo            string    `orm:"column(accti_acct_no);size(32)" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	Pridnum                int       `orm:"column(pridnum)" description:"期数  分期还款计划的期数顺序号,"`
	ChgacctDt              string    `orm:"column(chgacct_dt);type(date);null" description:"动账日期"`
	DataValidFlgCd         string    `orm:"column(data_valid_flg_cd);size(1);null" description:"数据有效标志代码  1-正常2-失效,"`
	CurrPeriodIntacrBgnDt  string    `orm:"column(curr_period_intacr_bgn_dt);type(date);null" description:"当期计息开始日期"`
	CurrPeriodIntacrEndDt  string    `orm:"column(curr_period_intacr_end_dt);type(date);null" description:"当期计息结束日期"`
	BegintDt               string    `orm:"column(begint_dt);type(date);null" description:"起息日期  记录开始计息的日期,"`
	WrtoffAmt              float64   `orm:"column(wrtoff_amt);null;digits(18);decimals(2)" description:"核销金额  记录贷款核销的金额"`
	WrtoffPrerdcAmt        float64   `orm:"column(wrtoff_prerdc_amt);null;digits(18);decimals(2)" description:"核销预减金额         "`
	WrtoffPreincAmt        float64   `orm:"column(wrtoff_preinc_amt);null;digits(18);decimals(2)" description:"核销预增金额          0-try,1-confirm,2-cancel"`
	WrtoffTccStusCd        string    `orm:"column(wrtoff_tcc_stus_cd);size(1);null" description:"核销tcc状态代码  0-try,1-confirm,2-cancel"`
	ActlRepayPrinPrerdcAmt float64   `orm:"column(actl_repay_prin_prerdc_amt);null;digits(18);decimals(2)" description:"实际还款本金预减金额"`
	ActlRepayPrinPreincAmt float64   `orm:"column(actl_repay_prin_preinc_amt);null;digits(18);decimals(2)" description:"实际还款本金预增金额     "`
	ActlRepayPrinTccStusCd string    `orm:"column(actl_repay_prin_tcc_stus_cd);size(1);null" description:"实际还款本金tcc状态代码  0-try,1-confirm,2-cancel"`
	CurrPeriodRepayDt      string    `orm:"column(curr_period_repay_dt);type(date);null" description:"当期还款日期"`
	BalTypCd               string    `orm:"column(bal_typ_cd);size(3);null" description:"余额类型代码  0-正常,1-逾期"`
	PlanRepayPrin          float64   `orm:"column(plan_repay_prin);null;digits(18);decimals(2)" description:"计划还款本金"`
	ActlRepayDt            string    `orm:"column(actl_repay_dt);type(date);null" description:"实际还款日期  实际还款日期"`
	ActlRepayPrin          float64   `orm:"column(actl_repay_prin);null;digits(18);decimals(2)" description:"实际还款本金  实际还款本金"`
	FinlModfyDt            string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_accti_acct_deta) TableName() string {
	return "t_loan_accti_acct_deta"
}

func InsertT_loan_accti_acct_deta(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_accti_acct_deta).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_accti_acct_detaTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_accti_acct_deta)).Filter("accti_acct_no", maps["AcctiAcctNo"]).
		Filter("pridnum", maps["Pridnum"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_accti_acct_detaTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_accti_acct_deta where accti_acct_no=? and pridnum=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctiAcctNo"], maps["Pridnum"]).Exec()
	return err
}

func QueryT_loan_accti_acct_deta(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_accti_acct_deta panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_accti_acct_deta))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_accti_acct_detaById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_accti_acct_deta)).Filter("accti_acct_no", maps["AcctiAcctNo"]).Filter("pridnum", maps["Pridnum"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_accti_acct_deta(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_accti_acct_deta panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_accti_acct_deta)).Filter("accti_acct_no", maps["AcctiAcctNo"]).Filter("pridnum", maps["Pridnum"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
