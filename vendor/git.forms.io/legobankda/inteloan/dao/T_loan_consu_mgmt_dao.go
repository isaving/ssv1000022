package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_consu_mgmt struct {
	LoanAcctNo      string    `orm:"column(loan_acct_no);pk" description:"贷款账号"`
	LoanContNo      string    `orm:"column(loan_cont_no);size(20);null" description:"合约号"`
	LoanConcNo      string    `orm:"column(loan_conc_no);size(30);null" description:"合同号"`
	LoanAcctgAcctNo string    `orm:"column(loan_acctg_acct_no);size(20);null" description:"贷款核算账号"`
	OverdueDays     int       `orm:"column(overdue_days);size(8);null" description:"逾期天数"`
	CustId          string    `orm:"column(cust_id);size(20);null" description:"借款人客户号"`
	CustName        string    `orm:"column(cust_name);size(320);null" description:"借款人姓名"`
	IdType          string    `orm:"column(id_type);size(3);null" description:"证件类型"`
	IdNo            string    `orm:"column(id_no);size(40);null" description:"证件号码"`
	ProdId          string    `orm:"column(prod_id);size(20);null" description:"贷款产品ID"`
	ProdVersion     string    `orm:"column(prod_version);size(10);null" description:"贷款产品版本号"`
	DepAcctNo       string    `orm:"column(dep_acct_no);size(20);null" description:"存款账号"`
	AcctOpenDate    string    `orm:"column(acct_open_date);size(10);null" description:"账户开立日期"`
	ExpDate         string    `orm:"column(exp_date);size(10);null" description:"消费到期日"`
	RepayType       string    `orm:"column(repay_type);size(2);null" description:"还本付息方式 01：随借随还"`
	Status          string    `orm:"column(status);size(2);null" description:"状态 1:有效;\n9:无效"`
	PeriodBeginDate string    `orm:"column(period_begin_date);size(10);null" description:"账单周期开始日期"`
	PeriodEndDate   string    `orm:"column(period_end_date);size(10);null" description:"账单周期结束日期"`
	LastMaintDate   string    `orm:"column(last_maint_date);type(date);null" description:"最后更新日期"`
	LastMaintTime   string    `orm:"column(last_maint_time);type(time);null" description:"最后更新时间"`
	LastMaintBrno   string    `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	LastMaintTell   string    `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	TccState        int8      `orm:"column(tcc_state);null"`
}

func (t *T_loan_consu_mgmt) TableName() string {
	return "t_loan_consu_mgmt"
}

func InsertT_loan_consu_mgmt(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_consu_mgmt).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_consu_mgmtTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_consu_mgmt)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_consu_mgmtTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_consu_mgmt where loan_acct_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanAcctNo"]).Exec()
	return err
}

func QueryT_loan_consu_mgmt(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_consu_mgmt panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_consu_mgmt))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_consu_mgmtById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_consu_mgmt)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_consu_mgmt(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_consu_mgmt panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_consu_mgmt)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
