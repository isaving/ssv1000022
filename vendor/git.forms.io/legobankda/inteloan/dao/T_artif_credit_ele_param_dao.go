package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_artif_credit_ele_param struct {
	LoanProdtNo              string  `orm:"column(loan_prodt_no);size(20);pk" description:"贷款产品编号"`
	EfftDt                   string  `orm:"column(efft_dt);type(date)" description:"生效日期"`
	QtaAdjRangDegreeCeilVal  float64 `orm:"column(qta_adj_rang_degree_ceil_val);null;digits(18);decimals(2)" description:"额度调整幅度上限值"`
	QtaAdjRangDegreeFloorVal int     `orm:"column(qta_adj_rang_degree_floor_val);null" description:"额度调整幅度下限值"`
	FixdIntrtFlotCeilRatio   float64 `orm:"column(fixd_intrt_flot_ceil_ratio);null;digits(9);decimals(6)" description:"固定利率浮动上限比例"`
	FixdIntrtFlotFloorRatio  float64 `orm:"column(fixd_intrt_flot_floor_ratio);null;digits(9);decimals(6)" description:"固定利率浮动下限比例"`
	LprIntrtBpFlotCeilVal    float64 `orm:"column(lpr_intrt_bp_flot_ceil_val);null;digits(8);decimals(5)" description:"lpr利率bp浮动上限值"`
	LprIntrtBpFlotFloorVal   float64 `orm:"column(lpr_intrt_bp_flot_floor_val);null;digits(9);decimals(6)" description:"lpr利率bp浮动下限值"`
	CrtTelrNo                string  `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtTm                    string  `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	CrtOrgNo                 string  `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	CrdtElentParaStusCd      string  `orm:"column(crdt_elent_para_stus_cd);size(2);null" description:"授信要素参数状态代码"`
	FinlModfyDt              string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm              string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo           string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo          string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                 int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_artif_credit_ele_param) TableName() string {
	return "t_artif_credit_ele_param"
}

func InsertT_artif_credit_ele_param(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_artif_credit_ele_param).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_artif_credit_ele_paramTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_artif_credit_ele_param)).Filter("tx_reqe_cd", maps["TxReqeCd"]).
		Filter("chnl_no_cd", maps["ChnlNoCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_artif_credit_ele_paramTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_artif_credit_ele_param where tx_reqe_cd=? and chnl_no_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxReqeCd"], maps["ChnlNoCd"]).Exec()
	return err
}

func QueryT_artif_credit_ele_param(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_artif_credit_ele_param panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_artif_credit_ele_param))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_artif_credit_ele_paramById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_artif_credit_ele_param)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_artif_credit_ele_param(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_artif_credit_ele_param panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_artif_credit_ele_param)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
