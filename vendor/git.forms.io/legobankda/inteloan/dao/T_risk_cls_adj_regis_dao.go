package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_risk_cls_adj_regis struct {
	LoanDubilNo                      string    `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号"`
	AdjDt                            string    `orm:"column(adj_dt);type(date)" description:"调整日期"`
	SeqNo                            int       `orm:"column(seq_no)" description:"序号"`
	CustNo                           string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	LoanRiskClsfAdjPrin              float64   `orm:"column(loan_risk_clsf_adj_prin);null;digits(18);decimals(2)" description:"贷款风险分类调整本金"`
	LoanDeadl                        int       `orm:"column(loan_deadl);null" description:"贷款期限"`
	RepayManrCd                      string    `orm:"column(repay_manr_cd);size(3);null" description:"还款方式代码"`
	LoanBal                          float64   `orm:"column(loan_bal);null;digits(18);decimals(3)" description:"贷款余额"`
	BegDt                            string    `orm:"column(beg_dt);type(date);null" description:"开始日期"`
	MatrDt                           string    `orm:"column(matr_dt);type(time);null" description:"到期日期"`
	OnshetIntr                       float64   `orm:"column(onshet_intr);null;digits(18);decimals(2)" description:"表内应收利息"`
	InshetIntr                       float64   `orm:"column(inshet_intr);null;digits(18);decimals(2)" description:"表外应收利息"`
	PrinDueBgnDt                     string    `orm:"column(prin_due_bgn_dt);type(date);null" description:"本金拖欠开始日期"`
	IntDueBgnDt                      string    `orm:"column(int_due_bgn_dt);type(date);null" description:"利息拖欠开始日期"`
	DuePrin                          float64   `orm:"column(due_prin);null;digits(18);decimals(2)" description:"拖欠本金"`
	DueInt                           float64   `orm:"column(due_int);null;digits(18);decimals(2)" description:"拖欠利息"`
	CurrDuePridnum                   int       `orm:"column(curr_due_pridnum);null" description:"当前拖欠期数"`
	HighDuePridnum                   int       `orm:"column(high_due_pridnum);null" description:"最高拖欠期数"`
	AccmDuePridnum                   int       `orm:"column(accm_due_pridnum);null" description:"累计拖欠期数"`
	AdjBefCd                         string    `orm:"column(adj_bef_cd);size(2);null" description:"上期认定结果"`
	CurtprdCompEtimLnRiskClsfCd      string    `orm:"column(curtprd_comp_etim_ln_risk_clsf_cd);size(2);null" description:"本期系统分类结果"`
	CurtprdArtgclIdtfyLoanRiskClsfCd string    `orm:"column(curtprd_artgcl_idtfy_loan_risk_clsf_cd);size(2);null" description:"本期人工分类结果"`
	CurtprdCompEtimLnRiskClsfDt      string    `orm:"column(curtprd_comp_etim_ln_risk_clsf_dt);type(date);null" description:"系统分类日期"`
	CurtprdArtgclIdtfyLoanRiskClsfDt string    `orm:"column(curtprd_artgcl_idtfy_loan_risk_clsf_dt);type(date);null" description:"人工调整日期"`
	InendIdtfyLoanRiskClsfCd         string    `orm:"column(inend_idtfy_loan_risk_clsf_cd);size(2);null" description:"本期认定结果"`
	AdjManrCd                        string    `orm:"column(adj_manr_cd);size(2);null" description:"调整方式代码"`
	AdjPersonEmpnbr                  string    `orm:"column(adj_person_empnbr);size(6);null" description:"调整人员工号"`
	AdjPersonOrgNo                   string    `orm:"column(adj_person_org_no);size(4);null" description:"调整人机构号"`
	AdjBefLoanRiskClsfCd             string    `orm:"column(adj_bef_loan_risk_clsf_cd);size(2);null" description:"调整前贷款风险分类代码 1-正常 2-关注 3-次级 4-可疑 5-损失"`
	AdjAftLoanRiskClsfCd             string    `orm:"column(adj_aft_loan_risk_clsf_cd);size(2);null" description:"调整后贷款风险分类代码 1-正常 2-关注 3-次级 4-可疑 5-损失"`
	CurtprdIdtfyLoanRiskClsfCd       string    `orm:"column(curtprd_idtfy_loan_risk_clsf_cd);size(2);null" description:"本期人工认定贷款风险分类代码 1-正常 2-关注 3-次级 4-可疑 5-损失"`
	CurtprdIdtfyLoanRiskClsfDt       string    `orm:"column(curtprd_idtfy_loan_risk_clsf_dt);size(2);null" description:"本期人工认定贷款风险分类日期 1-正常 2-关注 3-次级 4-可疑 5-损失"`
	LoanRiskClsfStusCd               string    `orm:"column(loan_risk_clsf_stus_cd);size(2);null" description:"风险分类申请状态"`
	SugstnDescr                      string    `orm:"column(sugstn_descr);size(200);null" description:"人工调整意见描述"`
	FinlModfyDt                      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo                   string    `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo                  string    `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState                       int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_risk_cls_adj_regis) TableName() string {
	return "t_risk_cls_adj_regis"
}

func InsertT_risk_cls_adj_regis(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_risk_cls_adj_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_risk_cls_adj_regis).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_risk_cls_adj_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_risk_cls_adj_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_risk_cls_adj_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("adj_dt", maps["AdjDt"]).Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_risk_cls_adj_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_risk_cls_adj_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from T_risk_cls_adj_regis where loan_dubil_no=? and adj_dt=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_risk_cls_adj_regis(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_risk_cls_adj_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params,"Flag")
	if flag == "9" {	//lzd
		maps, err = QueryT_risk_cls_adj_regisFlag9(params)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_risk_cls_adj_regis))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "8"{
		if _, err = qs.Filter("tcc_state", state).OrderBy("-adj_dt","-seq_no").Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_risk_cls_adj_regisById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_risk_cls_adj_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("adj_dt", maps["AdjDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_risk_cls_adj_regis(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_risk_cls_adj_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_risk_cls_adj_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("adj_dt", maps["AdjDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_risk_cls_adj_regisFlag9(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql :="SELECT "+
		"  t4.cust_name CustName,"+
		"  t1.loan_dubil_no LoanDubilNo,"+
		"  t5.loan_prodt_nm LoanProdtNm,"+
		"  t2.adj_bef_cd AdjBefCd,"+
		"  t2.seq_no SeqNo,"+
		"  t2.adj_dt AdjDt,"+
		"  t2.curtprd_comp_etim_ln_risk_clsf_cd CurtprdCompEtimInRiskClsfCd,"+
		"  t2.curtprd_artgcl_idtfy_loan_risk_clsf_cd CurtprdArtgclIdtfyLoanRiskClsfCd,"+
		"  t2.inend_idtfy_loan_risk_clsf_cd InendIdtfyLoanRiskClsfCd,"+
		"  t2.loan_risk_clsf_stus_cd STATUS "+
		"FROM"+
		"  (SELECT "+
		"    loan_dubil_no,"+
		"    MAX(seq_no) seq_no "+
		"  FROM"+
		"    `t_risk_cls_adj_regis` "+
		"  GROUP BY loan_dubil_no) t1 "+
		"  LEFT JOIN t_risk_cls_adj_regis t2 "+
		"    ON t1.`loan_dubil_no` = t2.`loan_dubil_no` "+
		"  LEFT JOIN t_loan_loan_cont t3 "+
		"    ON t1.loan_dubil_no = t3.`loan_dubil_no` "+
		"  LEFT JOIN t_loan_cust_base_info t4 "+
		"    ON t3.indv_crtf_typ_cd = t4.indv_crtf_typ_cd "+
		"    AND t3.indv_crtf_no = t4.indv_crtf_no "+
		"  LEFT JOIN t_loan_product t5 "+
		"    ON t3.loan_prodt_no = t5.loan_prodt_no "+
		" where 1=1 "
	if maps["CustName"] != nil {
		sql = sql + " and t4.cust_name=\""+maps["CustName"].(string)+"\""
	}
	if maps["LoanDubilNo"] != nil {
		sql = sql + " and  t1.loan_dubil_no=\""+maps["LoanDubilNo"].(string)+"\""
	}
	if maps["CrtfTypCd"] != nil {
		sql = sql + " and t3.indv_crtf_typ_cd=\""+maps["CrtfTypCd"].(string)+"\""
	}
	if maps["CrtfNo"] != nil {
		sql = sql + " and t3.indv_crtf_no=\""+maps["CrtfNo"].(string)+"\""
	}
	if maps["CompEtimLnRiskClsfDtStart"] != nil {
		sql = sql + " and t2.adj_dt >=\""+maps["CompEtimLnRiskClsfDtStart"].(string)+"\""
	}
	if maps["CompEtimLnRiskClsfDtEend"] != nil {
		sql = sql + " and t2.adj_dt<=\""+maps["CompEtimLnRiskClsfDtEend"].(string)+"\""
	}
	if maps["Status"] != nil {
		sql = sql + " and t2.loan_risk_clsf_stus_cd=\""+maps["Status"].(string)+"\""
	}
	sql = sql +" GROUP BY t2.`loan_dubil_no`,t2.`seq_no`,t2.`adj_dt`"
	_, err = o.Raw(sql).Values(&mapList)
	return
}