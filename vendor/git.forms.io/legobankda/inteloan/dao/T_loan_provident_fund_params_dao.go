package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_provident_fund_params struct {
	BizSn            string `orm:"column(biz_sn);size(32);pk" description:"业务流水号  pk"`
	AcmltnfundCentNm string `orm:"column(acmltnfund_cent_nm);size(120);null" description:"公积金中心名称"`
	OpnupDt          string `orm:"column(opnup_dt);type(date);null" description:"开通日期"`
	RgstEmpnbr       string `orm:"column(rgst_empnbr);size(6);null" description:"登记员工号"`
	RgstEmplyName    string `orm:"column(rgst_emply_name);size(60);null" description:"登记员工姓名"`
	RgstOrgNo        string `orm:"column(rgst_org_no);size(4);null" description:"登记机构号"`
	RgstOrgNm        string `orm:"column(rgst_org_nm);size(120);null" description:"登记机构名称"`
	RgstDt           string `orm:"column(rgst_dt);type(date);null" description:"登记日期"`
	FinlModfyDt      string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_provident_fund_params) TableName() string {
	return "t_loan_provident_fund_params"
}

func InsertT_loan_provident_fund_params(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_provident_fund_params).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_provident_fund_paramsTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_provident_fund_params)).Filter("biz_sn", maps["BizSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_provident_fund_paramsTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_provident_fund_params where biz_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizSn"]).Exec()
	return err
}

func QueryT_loan_provident_fund_params(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_provident_fund_params panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_provident_fund_params))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_provident_fund_paramsById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	BizSn := params["BizSn"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_provident_fund_params)).Filter("biz_sn", BizSn).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_provident_fund_params(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_provident_fund_params panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	BizSn := params["BizSn"]
	_, err = o.QueryTable(new(T_loan_provident_fund_params)).Filter("biz_sn", BizSn).Filter("tcc_state", 0).Update(params)
	return err
}
