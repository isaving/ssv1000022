package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_consu_det struct {
	FlowId         int       `orm:"column(flow_id);pk" description:"自增主键"`
	CustId     	   string    `orm:"column(cust_id);size(20);null" description:"借款人客户号"`
	TradeType  	   string    `orm:"column(trade_type);size(2);null" description:"交易类型"`
	LoanAcctNo 	   string    `orm:"column(loan_acct_no);size(20);null" description:"贷款账号"`
	ConsuType  	   string    `orm:"column(consu_type);size(2);null" description:"消费类型 8：亲子9：宠物10：爱车11：酒店12：生活服务13：其他"`
	MercNo         string    `orm:"column(merc_no);size(40);null" description:"商户编号"`
	MercName       string    `orm:"column(merc_name);size(320);null" description:"商户名称"`
	OrderNo        string    `orm:"column(order_no);size(30);null" description:"订单编号"`
	DepAcctNo      string    `orm:"column(dep_acct_no);size(20);null" description:"存款账号"`
	ProdId         string    `orm:"column(prod_id);size(20);null" description:"贷款产品ID"`
	ProdVersion    string    `orm:"column(prod_version);size(10);null" description:"贷款产品版本号"`
	ConsuDate      string    `orm:"column(consu_date);size(10);null" description:"消费日期"`
	ConsuAmt       float64   `orm:"column(consu_amt);null;digits(19);decimals(2)" description:"消费金额"`
	Currency       string    `orm:"column(currency);size(3);null" description:"货币"`
	Cost           float64   `orm:"column(cost);null;digits(19);decimals(2)" description:"费用"`
	TranFlowNo     string    `orm:"column(tran_flow_no);size(42);null" description:"交易流水号"`
	AppConsuTime   string    `orm:"column(app_consu_time);size(19);null" description:"申请消费时间"`
	ActualLoanTime string    `orm:"column(actual_loan_time);size(19);null" description:"实际放款时间"`
	LastMaintDate  string    `orm:"column(last_maint_date);type(date);null" description:"最后更新日期"`
	LastMaintTime  string    `orm:"column(last_maint_time);type(time);null" description:"最后更新时间"`
	LastMaintBrno  string    `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	LastMaintTell  string    `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	TccState       int8      `orm:"column(tcc_state);null"`
}

func (t *T_loan_consu_det) TableName() string {
	return "t_loan_consu_det"
}

func InsertT_loan_consu_det(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_consu_det).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_consu_detTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_consu_det)).Filter("flow_id", maps["FlowId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_consu_detTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_consu_det where flow_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["FlowId"]).Exec()
	return err
}

func QueryT_loan_consu_det(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_consu_det panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	if flag == "9"{
		sql := " SELECT SUM(consu_amt) ConsuAmt FROM `t_loan_consu_det` WHERE cust_id =?"
		_, err =o.Raw(sql,params["CustNo"]).Values(&maps)
		return
	}
	qs := o.QueryTable(new(T_loan_consu_det))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_consu_detById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_consu_det)).Filter("flow_id", maps["FlowId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_consu_det(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_consu_det panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_consu_det)).Filter("flow_id", maps["FlowId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
