package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_seq_no struct {
	IdenCd      string    `orm:"column(iden_cd);pk" description:"识别码"`
	CurrSeqNo   int       `orm:"column(curr_seq_no);null" description:"当前序号"`
	FinlModfyDt string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_seq_no) TableName() string {
	return "t_loan_seq_no"
}

func InsertT_loan_seq_no(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_seq_no).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_seq_noTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_seq_no)).Filter("iden_cd", maps["IdenCd"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_seq_noTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_seq_no where iden_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IdenCd"]).Exec()
	return err
}

func QueryT_loan_seq_no(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_seq_no panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_seq_no))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_seq_noById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_seq_no)).Filter("iden_cd", maps["IdenCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_seq_no(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_seq_no panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_seq_no)).Filter("iden_cd", maps["IdenCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
