package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_impa_writeoff_appr_flow struct {
	LoanAcctNo       string `orm:"column(loan_acct_no);size(32);pk" description:"贷款账号  与放款合约号一致"`
	TxSn             string `orm:"column(tx_sn);size(32)" description:"交易流水号 主键"`
	BizAplyNo        string `orm:"column(biz_aply_no);size(40);null" description:"业务申请编号  dxloanbal.loanid"`
	BizTypCd         string `orm:"column(biz_typ_cd);size(1);null" description:"业务类型代码  1:减值 2:核销"`
	OperPersonEmpnbr string `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号 "`
	OperOrgNo        string `orm:"column(oper_org_no);size(4);null" description:"操作机构号  公共服务提供类型及长度"`
	OperTm           string `orm:"column(oper_tm);type(time);null" description:"操作时间 "`
	ApprvSugstnCd    string `orm:"column(apprv_sugstn_cd);size(3);null" description:"审批意见代码  1:同意 2:退回 3:拒绝"`
	SugstnDescr      string `orm:"column(sugstn_descr);size(200);null" description:"意见描述 "`
	FinlModfyDt      string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期 "`
	FinlModfyTm      string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间 "`
	FinlModfyOrgNo   string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号 "`
	FinlModfyTelrNo  string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号 "`
	TccState         int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_impa_writeoff_appr_flow) TableName() string {
	return "t_impa_writeoff_appr_flow"
}

func InsertT_impa_writeoff_appr_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_impa_writeoff_appr_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_impa_writeoff_appr_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_impa_writeoff_appr_flow)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("tx_sn", maps["TxSn"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_impa_writeoff_appr_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_impa_writeoff_appr_flow where loan_acct_no=? and tx_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanAcctNo"], maps["TxSn"]).Exec()
	return err
}

func QueryT_impa_writeoff_appr_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_impa_writeoff_appr_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_impa_writeoff_appr_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_impa_writeoff_appr_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_impa_writeoff_appr_flow)).Filter("loan_acct_no", maps["LoanAcctNo"]).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_impa_writeoff_appr_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_impa_writeoff_appr_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_impa_writeoff_appr_flow)).Filter("loan_acct_no", maps["LoanAcctNo"]).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
