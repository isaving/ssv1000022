package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_earwar_sig_proc_flow struct {
	TaskNo                  string    `orm:"column(task_no);size(30);pk" description:"任务编号  PK"`
	SeqNo                   int       `orm:"column(seq_no)" description:"序号  PK"`
	CustNo                  string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	AfBnkLnBizTypCd         string    `orm:"column(af_bnk_ln_biz_typ_cd);size(1);null" description:"贷后业务类型代码  2-贷中预警3-贷后检查4-贷后催收"`
	OperPersonEmpnbr        string    `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号"`
	OperTypCd               string    `orm:"column(oper_typ_cd);size(2);null" description:"操作类型代码  01-录入02-拟制03-提交04-退回05-复核06-审核07-审批08-生效09-冻结10-冻结审批11-解冻12-解冻审批13-撤销14-废除15-终止16-失效21-处理22-任务转派23-任务撤回24-转派确认25-评价26-整改"`
	OperOrgNo               string    `orm:"column(oper_org_no);size(4);null" description:"操作机构号"`
	OperDt                  string    `orm:"column(oper_dt);type(date);null" description:"操作日期"`
	OperBefAfBnkLnJobStusCd string    `orm:"column(oper_bef_af_bnk_ln_job_stus_cd);size(2);null" description:"操作前贷后作业状态代码  01-待处理02-转派待处理03-转派已处理04-转派已确认05-已过期09-已处理11-待整改12-待评价13-已评价14-处理成功15-处理失败"`
	TaskDlwthStusCd         string    `orm:"column(task_dlwth_stus_cd);size(2);null" description:"任务处理状态代码  01-待处理02-转派待处理03-转派已处理04-转派已确认05-已过期09-已处理11-待整改12-待评价13-已评价14-处理成功15-处理失败"`
	AfBnkLnJobDlwthResultCd string    `orm:"column(af_bnk_ln_job_dlwth_result_cd);size(1);null" description:"贷后作业处理结果代码  11-转派确认22-转派废除13-评价通过14-评价退回"`
	OperResultComnt         string    `orm:"column(oper_result_comnt);size(200);null" description:"操作结果说明"`
	TxSn                    string    `orm:"column(tx_sn);size(32);null" description:"交易流水号"`
	TaskDlwthResultNo       string    `orm:"column(task_dlwth_result_no);size(32);null" description:"任务处理结果编号"`
	FinlModfyDt             string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm             string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo          string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo         string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_earwar_sig_proc_flow) TableName() string {
	return "t_earwar_sig_proc_flow"
}

func InsertT_earwar_sig_proc_flow(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_earwar_sig_proc_flow where task_no=?"
		o.Raw(sql, maps["TaskNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_earwar_sig_proc_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_earwar_sig_proc_flowTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_earwar_sig_proc_flow where task_no=?"
		o.Raw(sql, maps["TaskNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_earwar_sig_proc_flow)).Filter("task_no", maps["TaskNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_earwar_sig_proc_flowTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_earwar_sig_proc_flow where task_no=?"
		o.Raw(sql, maps["TaskNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_earwar_sig_proc_flow where task_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TaskNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_earwar_sig_proc_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_earwar_sig_proc_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_earwar_sig_proc_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_earwar_sig_proc_flowById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	TaskNo := params["TaskNo"]
	SeqNo := params["SeqNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_earwar_sig_proc_flow)).Filter("task_no", TaskNo).Filter("seq_no", SeqNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_earwar_sig_proc_flow(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_earwar_sig_proc_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	TaskNo := params["TaskNo"]
	SeqNo := params["SeqNo"]
	_, err = o.QueryTable(new(T_earwar_sig_proc_flow)).Filter("task_no", TaskNo).Filter("seq_no", SeqNo).Filter("tcc_state", 0).Update(params)
	return err
}
