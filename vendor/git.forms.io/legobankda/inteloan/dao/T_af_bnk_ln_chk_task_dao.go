package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_af_bnk_ln_chk_task struct {
	ChkTaskNo            string `orm:"column(chk_task_no);pk" description:"检查任务编号  1)工单长度20位,由4位大写字母+16位数字组成;(2)工单编号由工单类型?创建日期?序号组成?其中:? 工单类型:取4位拼音首字母(大写),贷后检查的工单类型取拼音首字母,则为dhjc;? 创建日期:yyyymmdd格式,8位长度,例如2019年8月31日创建的贷后检查任务,则创建日期:20190831?系统日切后,新建工单的创建日期取系统日切后的日期,20190901;? 序号:8位长度,按当天工单创建的先后顺序,从“00000001”开始编号?pk"`
	CustNo               string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CustName             string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	CtrtNo               string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	LoanDubilNo          string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	AfBnkLnChkTaskTypCd  string    `orm:"column(af_bnk_ln_chk_task_typ_cd);size(1);null" description:"贷后检查任务类型代码  1-用途检查2-日常检查3-特殊检查"`
	AfBnkLnChkManrCd     string    `orm:"column(af_bnk_ln_chk_manr_cd);size(1);null" description:"贷后检查方式代码  1-现场检查2-非现场检查3-现场检查+非现场检查"`
	TaskGnrtDt           string    `orm:"column(task_gnrt_dt);type(date);null" description:"任务生成日期"`
	CrtEmpnbr            string    `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	AprsEmplyName        string    `orm:"column(aprs_emply_name);size(60);null" description:"评价员工姓名"`
	CrtOrgNo             string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	AprsOrgNm            string    `orm:"column(aprs_org_nm);size(120);null" description:"评价机构名称"`
	TaskDlwthStusCd      string    `orm:"column(task_dlwth_stus_cd);size(2);null" description:"任务处理状态代码  01-待处理02-转派待处理03-转派已处理04-转派已确认05-已过期09-已处理11-待整改12-待评价13-已评价14-处理成功15-处理失败21-转派确认22-转派废除23-评价通过24-评价退回"`
	AlrdyExecFlg         string    `orm:"column(alrdy_exec_flg);size(1);null" description:"已执行标志  0否1是"`
	AtExecWindowFlg      string    `orm:"column(at_exec_window_flg);size(1);null" description:"在执行窗口标志  0否1是"`
	ExecTms              int       `orm:"column(exec_tms);null" description:"执行次数"`
	ExecRsn              string    `orm:"column(exec_rsn);size(200);null" description:"执行原因"`
	ExecConcusComnt      string    `orm:"column(exec_concus_comnt);size(200);null" description:"执行结论说明"`
	ChkSponsorEmpnbr     string    `orm:"column(chk_sponsor_empnbr);size(6);null" description:"检查主办员工号"`
	ChkSponsorOrgNo      string    `orm:"column(chk_sponsor_org_no);size(4);null" description:"检查主办机构号"`
	ChkCorgEmpnbr        string    `orm:"column(chk_corg_empnbr);size(6);null" description:"检查协办员工号"`
	ChkCorgOrgNo         string    `orm:"column(chk_corg_org_no);size(4);null" description:"检查协办机构号"`
	AprsSugstnCd         string    `orm:"column(aprs_sugstn_cd);size(1);null" description:"评价意见代码  1-通过2-待整改"`
	RctfctnSugstnContent string    `orm:"column(rctfctn_sugstn_content);size(200);null" description:"整改意见内容"`
	AprsEmpnbr           string    `orm:"column(aprs_empnbr);size(6);null" description:"评价员工号"`
	AprsOrgNo            string    `orm:"column(aprs_org_no);size(4);null" description:"评价机构号"`
	AprsDt               string    `orm:"column(aprs_dt);type(date);null" description:"评价日期"`
	SponsorEmplyName     string    `orm:"column(sponsor_emply_name);size(60);null" description:"主办员工姓名"`
	CorgEmplyName        string    `orm:"column(corg_emply_name);size(60);null" description:"协办员工姓名"`
	SponsorOrgNm         string    `orm:"column(sponsor_org_nm);size(120);null" description:"主办机构名称"`
	CorgOrgNm            string    `orm:"column(corg_org_nm);size(120);null" description:"协办机构名称"`
	SponsorEmplyTaskNo   string    `orm:"column(sponsor_emply_task_no);size(30);null" description:"主办员工任务编号"`
	CorgEmplyTaskNo      string    `orm:"column(corg_emply_task_no);size(30);null" description:"协办员工任务编号"`
	TaskDlwthResultNo    string    `orm:"column(task_dlwth_result_no);size(32);null" description:"任务处理结果编号"`
	AprsEmplyTaskNo      string    `orm:"column(aprs_emply_task_no);size(60);null" description:"评价员工任务编号"`
	ReacTotlTms          int       `orm:"column(reac_totl_tms);null" description:"触达总次数"`
	FinlModfyDt          string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm          string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo       string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int    `orm:"column(tcc_state);null"`
}

func (t *T_af_bnk_ln_chk_task) TableName() string {
	return "t_af_bnk_ln_chk_task"
}

func InsertT_af_bnk_ln_chk_task(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_af_bnk_ln_chk_task).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_af_bnk_ln_chk_taskTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_af_bnk_ln_chk_task)).Filter("chk_task_no", maps["ChkTaskNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_af_bnk_ln_chk_taskTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_af_bnk_ln_chk_task where chk_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ChkTaskNo"]).Exec()
	return err
}

func QueryT_af_bnk_ln_chk_task(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_af_bnk_ln_chk_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	Flag :=params["Flag"]
	if  Flag== "9" {
		maps, err = QueryT_af_bnk_ln_chk_taskFlag9(params, 0)
		return
	}
	delete(params,"Flag")
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_af_bnk_ln_chk_task))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if Flag == "8" {
		if _, err = qs.Filter("tcc_state", state).Limit(-1).GroupBy("crt_empnbr","crt_org_no").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if Flag == "7" {
		if _, err = qs.Filter("tcc_state", state).Limit(-1).GroupBy("crt_empnbr","crt_org_no").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

//统计百分比
func QueryT_af_bnk_ln_chk_taskPercentage(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_af_bnk_ln_chk_taskPercentage panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	//修改之前的
	//sql := "SELECT COUNT(*) cont," +
	//	"opror_empnbr ManaNo," +
	//	"opror_nm ManaName," +
	//	"opr_org_no OrgNum," +
	//	"opror_nm OrgNm," +
	//	"GROUP_CONCAT(af_bnk_ln_chk_task_stus_cd) GroupConcat" +
	//	" FROM `t_af_bnk_ln_chk_task` WHERE LOAN_DUBIL_NO IN(SELECT loan_dubil_no FROM `t_loan_loan_cont` WHERE  contrt_stus_cd=2 AND tcc_state=0) AND tcc_state=0 " +
	//	"GROUP BY opror_empnbr, opror_nm, opr_org_no,opror_nm "
	sql :="SELECT COUNT(*) cont,"+
		"chk_sponsor_empnbr ManaNo,"+
		"sponsor_emply_name ManaName,"+
		"chk_sponsor_org_no OrgNum,"+
		"sponsor_org_nm OrgNm,"+
		"GROUP_CONCAT(task_dlwth_stus_cd) GroupConcat "+
	"FROM `t_af_bnk_ln_chk_task` "+
	"WHERE LOAN_DUBIL_NO IN	(SELECT loan_dubil_no FROM `t_loan_loan_cont` WHERE contrt_stus_cd = 2	AND tcc_state = 0) "+
	"AND tcc_state = 0	GROUP BY chk_sponsor_empnbr,sponsor_emply_name,chk_sponsor_org_no,sponsor_org_nm"
	_, err = o.Raw(sql).Values(&maps)
	return
}

func QueryT_af_bnk_ln_chk_taskById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_af_bnk_ln_chk_task)).Filter("chk_task_no", maps["ChkTaskNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_af_bnk_ln_chk_task(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_af_bnk_ln_chk_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_af_bnk_ln_chk_task)).Filter("chk_task_no", maps["ChkTaskNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
func QueryT_af_bnk_ln_chk_taskFlag9(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_af_bnk_ln_chk_task))
	if params["Flag"] == "9" {
		SortDateFlg := params["SortDateFlg"]
		delete(params, "Flag")
		delete(params, "SortDateFlg")
		for k, v := range params {
			if k != constant.PageRecCount && k != constant.PageNo {
				if k == "StartDate" {
					qs = qs.Filter("task_gnrt_dt__gte", v)
				} else if k == "EndDate" {
					qs = qs.Filter("task_gnrt_dt__lte", v)
				} else {
					qs = qs.Filter(k, v)
				}
			}
		}
		if SortDateFlg == "1" {
			_, err = qs.Filter("tcc_state", state).OrderBy("task_gnrt_dt").Values(&maps)
		} else {
			_, err = qs.Filter("tcc_state", state).OrderBy("-task_gnrt_dt").Values(&maps)
		}
		if err != nil {
			return nil, err
		}
		return
	}
	return
}
