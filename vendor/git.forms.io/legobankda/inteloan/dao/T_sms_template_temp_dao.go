package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sms_template_temp struct {
	TmplNo          string    `orm:"column(tmpl_no);pk" description:"模板编号"`
	SmsTypCd        string    `orm:"column(sms_typ_cd);size(2);null" description:"短信类型代码 1联机 2 批量"`
	SmsTmplStusCd   string    `orm:"column(sms_tmpl_stus_cd);size(2);null" description:"短信模板状态代码 01 有效 09 无效"`
	EfftDt          string    `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	SmsTmplNm       string    `orm:"column(sms_tmpl_nm);size(200);null" description:"短信模板名称"`
	SmsTmplContent  string    `orm:"column(sms_tmpl_content);size(400);null" description:"文本内容"`
	SmsTmplRmk      string    `orm:"column(sms_tmpl_rmk);size(200);null" description:"说明"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(20);null" description:"机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(20);null" description:"柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_sms_template_temp) TableName() string {
	return "t_sms_template_temp"
}

func InsertT_sms_template_temp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sms_template_temp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sms_template_tempTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sms_template_temp)).Filter("tmpl_no", maps["TmplNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sms_template_tempTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_sms_template_temp where tmpl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplNo"]).Exec()
	return err
}

func QueryT_sms_template_temp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sms_template_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_sms_template_temp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sms_template_tempById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sms_template_temp)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sms_template_temp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sms_template_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sms_template_temp)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
