package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_pro_unexp struct {
	AcctnDt         string    `orm:"column(acctn_dt);type(date)" description:"会计日期"`
	BizFolnNo       string    `orm:"column(biz_foln_no);size(32);pk" description:"业务跟踪号"`
	BizEventCd      string    `orm:"column(biz_event_cd);size(6)" description:"业务事件编码"`
	SysFolnNo       string    `orm:"column(sys_foln_no);size(32);null" description:"系统跟踪号"`
	UnuslRsn        string    `orm:"column(unusl_rsn);size(200);null" description:"异常原因"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_pro_unexp) TableName() string {
	return "t_ae_pro_unexp"
}

func InsertT_ae_pro_unexp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_pro_unexp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_pro_unexpTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_pro_unexp)).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("biz_foln_no", maps["BizFolnNo"]).Filter("biz_event_cd", maps["BizEventCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_pro_unexpTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_pro_unexp where acctn_dt=? and biz_foln_no=? and biz_event_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctnDt"], maps["BizFolnNo"], maps["BizEventCd"]).Exec()
	return err
}

func QueryT_ae_pro_unexp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_pro_unexp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_pro_unexp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_pro_unexpById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_pro_unexp)).Filter("acctn_dt", maps["AcctnDt"]).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("biz_event_cd", maps["BizEventCd"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_pro_unexp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_pro_unexp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_pro_unexp)).Filter("acctn_dt", maps["AcctnDt"]).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("biz_event_cd", maps["BizEventCd"]).Filter("tcc_state", 0).Update(maps)
	return err
}
