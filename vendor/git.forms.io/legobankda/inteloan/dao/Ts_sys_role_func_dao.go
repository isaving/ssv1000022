package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Ts_sys_role_func struct {
	RoleNo          string `orm:"column(role_no);size(50);pk" description:"角色编号"`
	FuncNo          string `orm:"column(func_no);size(20)" description:"功能编号"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *Ts_sys_role_func) TableName() string {
	return "ts_sys_role_func"
}

func InsertTs_sys_role_func(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Ts_sys_role_func).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateTs_sys_role_funcTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Ts_sys_role_func)).Filter("role_no", maps["RoleNo"]).
		Filter("func_no", maps["FuncNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteTs_sys_role_funcTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from ts_sys_role_func where role_no=? and func_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["RoleNo"], maps["FuncNo"]).Exec()
	return err
}

func QueryTs_sys_role_func(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Ts_sys_role_func))
	if params["Flag"] == "9"{
		var sql string
		if params["RoleNos"]==""{
			sql = "SELECT role_no RoleNo,func_no FuncNo FROM ts_sys_role_func "
		}else{
			sql = "SELECT role_no RoleNo,func_no FuncNo FROM ts_sys_role_func WHERE role_no IN ("+params["RoleNos"].(string)+") "
		}
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	if params["Flag"]=="8"{
		sql :="SELECT t2.`func_url_path` FuncUrl FROM `ts_sys_role_func` t1 LEFT JOIN `ts_sys_func` t2 ON t1.`func_no`=t2.`func_no`"+
		" WHERE t1.`role_no` = ? and t1.tcc_state=0 and t2.tcc_state=0"
		_, err = o.Raw(sql,params["RoleId"]).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryTs_sys_role_funcById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	RoleNo := params["RoleNo"]
	FuncNo := params["FuncNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(Ts_sys_role_func)).Filter("role_no", RoleNo).Filter("func_no", FuncNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateTs_sys_role_func(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	RoleNo := params["RoleNo"]
	FuncNo := params["FuncNo"]
	_, err = o.QueryTable(new(Ts_sys_role_func)).Filter("role_no", RoleNo).Filter("func_no", FuncNo).Filter("tcc_state", 0).Update(params)
	return err
}

func DeleteTs_sys_role_func(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	RoleNo := params["RoleNo"]
	sql :="DELETE FROM  ts_sys_role_func WHERE role_no=? AND tcc_state=0"
	_, err = o.Raw(sql,RoleNo).Exec()
	return err
}