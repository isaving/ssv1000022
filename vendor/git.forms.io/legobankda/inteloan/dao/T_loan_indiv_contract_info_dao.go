package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_indiv_contract_info struct {
	CtrtNo                   string    `orm:"column(ctrt_no);pk" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	LoanDubilNo              string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	CustNo                   string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CustName                 string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd            string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码"`
	IndvCrtfNo               string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	LoanProdtNo              string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtNm              string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	ExecIntrt                float64   `orm:"column(exec_intrt);size(20);null" description:"执行利率"`
	IntrtFlotManrCd          string    `orm:"column(intrt_flot_manr_cd);size(1);null" description:"利率浮动方式代码"`
	BpFlotVal                float64   `orm:"column(bp_flot_val);size(20);null" description:"BP浮动值"`
	IntrtFlotRatio           float64   `orm:"column(intrt_flot_ratio);size(20);null" description:"利率浮动比例"`
	CtrtTypCd                string    `orm:"column(ctrt_typ_cd);size(1);null" description:"合同类型代码"`
	LoanGuarManrCd           string    `orm:"column(loan_guar_manr_cd);size(2);null" description:"贷款担保方式代码  参考标准代码:CD0090担保方式"`
	CurCd                    string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	AprvAmt                  float64   `orm:"column(aprv_amt);null;digits(18);decimals(2)" description:"批准金额"`
	CtrtAmt                  float64   `orm:"column(ctrt_amt);null;digits(18);decimals(2)" description:"合同金额  记录贷款合同的总金额,如果为银团贷款则填写实际参与金额,而非银团贷款总金额"`
	AprvDeadlMons            int       `orm:"column(aprv_deadl_mons);null" description:"批准期限月数"`
	CtrtDeadlMons            int       `orm:"column(ctrt_deadl_mons);null" description:"合同期限月数"`
	LoanUsageCd              string    `orm:"column(loan_usage_cd);size(4);null" description:"贷款用途代码"`
	BrwmnyUsageComnt         string    `orm:"column(brwmny_usage_comnt);size(200);null" description:"借款用途说明"`
	SpmeCntLoanRiskClsfCd    string    `orm:"column(spme_cnt_loan_risk_clsf_cd);size(2);null" description:"首分贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	SpmeCntLoanRiskClsfComnt string    `orm:"column(spme_cnt_loan_risk_clsf_comnt);size(200);null" description:"首分贷款风险分类说明"`
	SpdmnyManrCd             string    `orm:"column(spdmny_manr_cd);size(4);null" description:"用款方式代码  01-一次性用款02-多次用款"`
	AutonPaymtLmt            float64   `orm:"column(auton_paymt_lmt);null;digits(18);decimals(2)" description:"自主支付限额"`
	PayWayCd                 string    `orm:"column(pay_way_cd);size(3);null" description:"支付方式代码  1-自主支付2-受托支付3-自主支付+受托支付"`
	AutonPaymtAcctNo         string    `orm:"column(auton_paymt_acct_no);size(32);null" description:"自主支付账号"`
	AutonPaymtAcctNm         string    `orm:"column(auton_paymt_acct_nm);size(120);null" description:"自主支付账户名称"`
	PaymtRptsCycCd           string    `orm:"column(paymt_rpts_cyc_cd);size(2);null" description:"支付报告周期代码  D:天M:月"`
	PaymtRptsCycQty          int       `orm:"column(paymt_rpts_cyc_qty);null" description:"支付报告周期数量  参考标准代码:CD0075"`
	RepayManrCd              string    `orm:"column(repay_manr_cd);size(3);null" description:"还款方式代码  参考标准代码:CD0062标准基础上新增:51-按月付息按季还本52-按月付息按半年还本53-按月付息按年还本"`
	IntStlManrCd             string    `orm:"column(int_stl_manr_cd);size(3);null" description:"结息方式代码  1-统一定日2-按户定日3-按指定日期4-按产品"`
	IntacrManrCd             string    `orm:"column(intacr_manr_cd);size(5);null" description:"计息方式代码  1-按期计息2-按日计息"`
	TranOvdueGraceDays       int       `orm:"column(tran_ovdue_grace_days);null" description:"转逾期宽限天数  取值参考:gdxt02分类码:040"`
	ChrgManrCd               string    `orm:"column(chrg_manr_cd);size(3);null" description:"扣款方式代码  1-委托扣款(足额)2-委托扣款(不足额)9-非委托扣款"`
	EntrstChrgAcctNo         string    `orm:"column(entrst_chrg_acct_no);size(32);null" description:"委托扣款账号"`
	AcctNm                   string    `orm:"column(acct_nm);size(120);null" description:"账户名称"`
	RepaySorcComnt           string    `orm:"column(repay_sorc_comnt);size(400);null" description:"还款来源说明  记录贷款合同中载明的还款来源,如经营收入,变卖抵押物等,"`
	IntSubsidyRatio          float64   `orm:"column(int_subsidy_ratio);null;digits(9);decimals(6)" description:"贴息比例"`
	IntSubsidyExpiryDt       string    `orm:"column(int_subsidy_expiry_dt);type(date);null" description:"贴息截止日期"`
	BilsAddr                 string    `orm:"column(bils_addr);size(200);null" description:"账单地址"`
	PostCd                   string    `orm:"column(post_cd);size(6);null" description:"邮政编码  应为联系地址的邮编"`
	EMail                    string    `orm:"column(e_mail);size(40);null" description:"电子邮箱  记录个人客户的电子邮箱信息,"`
	CotactTelNo              string    `orm:"column(cotact_tel_no);size(20);null" description:"联系电话号码"`
	MobileNo                 string    `orm:"column(mobile_no);size(11);null" description:"手机号码  记录个人客户的国内手机号码信息,"`
	DsptSltnManrCd           string    `orm:"column(dspt_sltn_manr_cd);size(4);null" description:"争议解决方式代码  1-诉讼2-仲裁"`
	ArbitCommNm              string    `orm:"column(arbit_comm_nm);size(120);null" description:"仲裁委员会名称  “争议解决方式”选项为“诉讼”,此项不可填写,"`
	ArbitCommAddr            string    `orm:"column(arbit_comm_addr);size(200);null" description:"仲裁委员会地址  “争议解决方式”选项为“诉讼”,此项不可填写,"`
	MakelnPmseCondComnt      string    `orm:"column(makeln_pmse_cond_comnt);size(200);null" description:"放款前提条件说明"`
	OthApntMtsComnt          string    `orm:"column(oth_apnt_mts_comnt);size(300);null" description:"其他约定事项说明"`
	KepacctOrgNo             string    `orm:"column(kepacct_org_no);size(4);null" description:"记账机构号"`
	OprOrgNo                 string    `orm:"column(opr_org_no);size(4);null" description:"经办机构号"`
	OprorEmpnbr              string    `orm:"column(opror_empnbr);size(6);null" description:"经办人员工号"`
	FinlModfyDt              string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm              string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo           string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo          string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int           `orm:"column(tcc_state);null"`
}

func (t *T_loan_indiv_contract_info) TableName() string {
	return "t_loan_indiv_contract_info"
}

func InsertT_loan_indiv_contract_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_indiv_contract_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_indiv_contract_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_indiv_contract_info)).Filter("ctrt_no", maps["CtrtNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_indiv_contract_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_indiv_contract_info where ctrt_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CtrtNo"]).Exec()
	return err
}

func QueryT_loan_indiv_contract_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_indiv_contract_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	if flag == "9"{
		maps, err = QueryT_loan_indiv_contract_infoFlag9(params)
		return
	}
	qs := o.QueryTable(new(T_loan_indiv_contract_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_indiv_contract_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_indiv_contract_info)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_indiv_contract_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_indiv_contract_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_indiv_contract_info)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_indiv_contract_infoFlag9(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql :="SELECT  "+
		"t1.`ctrt_no` CtrtNo, "+
		"t1.`cust_no` CustNo, "+
		"t1.`cust_name` CustNm, "+
		"t1.`loan_prodt_no` LoanProdtNo, "+
		"t1.`loan_prodt_nm` LoanProdtNm, "+
		"t1.`cur_cd` CurCd, "+
		"t1.`ctrt_amt` CtrtAmt, "+
		"t1.`exec_intrt` ExecIntrt, "+
		"t1.`loan_dubil_no` LoanAcctNo, "+
		"t1.`aprv_amt` AprvAmt, "+
		"t1.`aprv_deadl_mons` AprvDeadlMons, "+
		"t1.`ctrt_deadl_mons` CtrtDeadlMons, "+
		"t2.`ctrt_start_dt` CtrtStartDt, "+
		"t2.`ctrt_matr_dt` CtrtMatrDt, "+
		"t2.`ctrt_stus_cd` CtrtStusCd "+
		"FROM `t_loan_indiv_contract_info` t1,`t_loan_contract_reg` t2 "+
		"WHERE t1.`ctrt_no`=t2.`ctrt_no` AND t1.tcc_state=0 AND t2.tcc_state=0 "
	if params["CtrtNo"] != nil {
		sql = sql +" AND t1.ctrt_no=\""+params["CtrtNo"].(string)+"\""
	}
	if params["CustNo"] != nil {
		sql = sql +" AND t1.cust_no=\""+params["CustNo"].(string)+"\""
	}
	if params["CustNm"] != nil {
		sql = sql +" AND t1.cust_name=\""+params["CustNm"].(string)+"\""
	}
	if params["BrwmnyCtrtTypCd"] != nil {
		sql = sql +" AND t1.ctrt_typ_cd=\""+params["BrwmnyCtrtTypCd"].(string)+"\""
	}
	if params["LoanAcctNo"] != nil {
		sql = sql +" AND t1.loan_dubil_no=\""+params["LoanAcctNo"].(string)+"\""
	}
	_,err =o.Raw(sql).Values(&maps)
	return
}