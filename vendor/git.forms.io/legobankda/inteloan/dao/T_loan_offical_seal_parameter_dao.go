package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_offical_seal_parameter struct {
	ComslNo         string    `orm:"column(comsl_no);pk" description:"公章编号"`
	DgtCdtlId       string    `orm:"column(dgt_cdtl_id);size(50);null" description:"数字证书id"`
	OrgNo           string    `orm:"column(org_no);size(4);null" description:"机构号"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	ComslNm         string    `orm:"column(comsl_nm);size(120);null" description:"公章名称"`
	ComslPictInfo   string    `orm:"column(comsl_pict_info);null" description:"公章图片信息"`
	CrtTelrNo       string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtOrgNo        string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	CrtDt           string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int8      `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_offical_seal_parameter) TableName() string {
	return "t_loan_offical_seal_parameter"
}

func InsertT_loan_offical_seal_parameter(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_offical_seal_parameter).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_offical_seal_parameterTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_offical_seal_parameter)).Filter("comsl_no", maps["ComslNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_offical_seal_parameterTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_offical_seal_parameter where comsl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ComslNo"]).Exec()
	return err
}

func QueryT_loan_offical_seal_parameter(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_offical_seal_parameter panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_offical_seal_parameter))
	flag := params["Flag"]
	delete(params,"Flag")
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag=="9"{
		if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("org_no").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("comsl_no").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_offical_seal_parameterById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	ComslNo := params["ComslNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_offical_seal_parameter)).Filter("comsl_no", ComslNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_offical_seal_parameter(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_offical_seal_parameter panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	ComslNo := params["ComslNo"]
	_, err = o.QueryTable(new(T_loan_offical_seal_parameter)).Filter("comsl_no", ComslNo).Filter("tcc_state", 0).Update(params)
	return err
}
