package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strings"
)

type T_common_org struct {
	OrgId              string    `orm:"column(org_id);pk" description:"机构号"`
	Status             string    `orm:"column(status);size(1);null" description:"状态"`
	OrgType            string    `orm:"column(org_type);size(1);null" description:"机构类型"`
	OrgLvl             string    `orm:"column(org_lvl);size(1);null" description:"机构级别"`
	BillStatus         string    `orm:"column(bill_status);size(1);null" description:"扎帐状态"`
	LegalPersonCode    string    `orm:"column(legal_person_code);size(8);null" description:"法人代码"`
	SupOrgId           string    `orm:"column(sup_org_id);size(8);null" description:"上级机构号"`
	AcctingOrgId       string    `orm:"column(accting_org_id);size(8);null" description:"核算机构号"`
	LiquidatOrgId      string    `orm:"column(liquidat_org_id);size(8);null" description:"清算机构号"`
	WithdrawalOwnOrgId string    `orm:"column(withdrawal_own_org_id);size(8);null" description:"撤并后归属机构号"`
	PaymentSysLineNo   string    `orm:"column(payment_sys_line_no);size(30);null" description:"支付系统行号"`
	SwiftCode          string    `orm:"column(swift_code);size(11);null" description:"SWIFT代码"`
	OrgName            string    `orm:"column(org_name);size(60);null" description:"名称"`
	Abbreviation       string    `orm:"column(abbreviation);size(20);null" description:"简称"`
	Addr               string    `orm:"column(addr);size(100);null" description:"地址"`
	Phone              string    `orm:"column(phone);size(20);null" description:"电话"`
	OpenDate           string    `orm:"column(open_date);type(date);null" description:"开业日期"`
	ClosingDate        string    `orm:"column(closing_date);type(date);null" description:"结业日期"`
	EffDate            string    `orm:"column(eff_date);type(date);null" description:"生效日期"`
	ExpDate            string    `orm:"column(exp_date);type(date);null" description:"失效日期"`
	CreateOrgId        string    `orm:"column(create_org_id);size(8);null" description:"创建机构"`
	CreateTeller       string    `orm:"column(create_teller);size(8);null" description:"创建柜员"`
	CanceOrgId         string    `orm:"column(cance_org_id);size(8);null" description:"注销机构"`
	CanceTeller        string    `orm:"column(cance_teller);size(8);null" description:"注销柜员"`
	LastMaintDate      string    `orm:"column(last_maint_date);type(date);null" description:"最后更新日期"`
	LastMaintTime      string    `orm:"column(last_maint_time);type(time);null" description:"最后更新时间"`
	LastMaintBrno      string    `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	LastMaintTell      string    `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	TccState           int       `orm:"column(tccstatus);null"`
}

func (t *T_common_org) TableName() string {
	return "t_common_org"
}

func InsertT_common_org(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_common_org).TableName())
	sql = strings.Replace(sql,"tcc_state","tccstatus",1)
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_common_orgTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_common_org)).Filter("org_id", maps["OrgId"]).
		Update(orm.Params{"tccstatus": 0})
	return err
}

func DeleteT_common_orgTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_common_org where org_id=?  and tccstatus = 1"
	_, err := o.Raw(sql, maps["OrgId"]).Exec()
	return err
}

func QueryT_common_org(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_common_org panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_common_org))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tccstatus", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_common_orgById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_common_org)).Filter("org_id", maps["OrgId"]).
		Filter("tccstatus", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_common_org(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_common_org panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_common_org)).Filter("org_id", maps["OrgId"]).
		Filter("tccstatus", 0).Update(maps)
	return err
}
