package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_jnl_entry_sum struct {
	SumId               string    `orm:"column(sum_id);pk" description:"汇总id  同一科目汇总的汇总ID"`
	TxAcctnDt           string    `orm:"column(tx_acctn_dt);type(date);null" description:"交易会计日期"`
	BizFolnNo           string    `orm:"column(biz_foln_no);size(32);null" description:"业务跟踪号"`
	ReqeRcrdacctAcctnDt string    `orm:"column(reqe_rcrdacct_acctn_dt);type(date);null" description:"请求入账会计日期"`
	SysFolnNo           string    `orm:"column(sys_foln_no);size(32);null" description:"系统跟踪号"`
	TxOrgNo             string    `orm:"column(tx_org_no);size(4);null" description:"交易机构号"`
	ActngOrgNo          string    `orm:"column(actng_org_no);size(4);null" description:"账务机构号"`
	CurCd               string    `orm:"column(cur_cd);size(4);null" description:"币种代码  参考标准代码:CD0040"`
	DebitCrdtFlgCd      string    `orm:"column(debit_crdt_flg_cd);size(1);null" description:"借贷标志代码  D-借,C-贷,B-借贷都检查-"`
	TxAmt               float64   `orm:"column(tx_amt);null;digits(18);decimals(2)" description:"交易金额"`
	SubjNo              string    `orm:"column(subj_no);size(8);null" description:"科目编号"`
	FinlModfyDt         string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_jnl_entry_sum) TableName() string {
	return "t_ae_jnl_entry_sum"
}

func InsertT_ae_jnl_entry_sum(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_jnl_entry_sum).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_jnl_entry_sumTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_jnl_entry_sum)).Filter("sum_id", maps["SumId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_jnl_entry_sumTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_jnl_entry_sum where sum_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["SumId"]).Exec()
	return err
}

func QueryT_ae_jnl_entry_sum(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_jnl_entry_sum panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_jnl_entry_sum))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_jnl_entry_sumById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_jnl_entry_sum)).Filter("sum_id", maps["SumId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_jnl_entry_sum(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_jnl_entry_sum panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_jnl_entry_sum)).Filter("sum_id", maps["SumId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
