package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_event_itm struct {
	BizEventCd      string    `orm:"column(biz_event_cd);size(6);pk" description:"业务事件编码  PK,"`
	SeqNum          int       `orm:"column(seq_num)" description:"顺序号  PK,"`
	EngineVaritCd   string    `orm:"column(engine_varit_cd);size(4)" description:"引擎变式编码"`
	SoftProdtCd     string    `orm:"column(soft_prodt_cd);size(3)" description:"软件产品编码  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'"`
	CurCd           string    `orm:"column(cur_cd);size(4);null" description:"币种代码  参考标准代码:CD0040当借方币别使用标示为‘0’时,使用该币别进行拆分录"`
	AsstClsSubjNo   string    `orm:"column(asst_cls_subj_no);size(8);null" description:"资产类科目编号"`
	LiabClsSubjNo   string    `orm:"column(liab_cls_subj_no);size(8);null" description:"负债类科目编号"`
	CntptySubjNo    string    `orm:"column(cntpty_subj_no);size(8);null" description:"对方科目编号"`
	EfftDt          string    `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	MansbjTypCd     string    `orm:"column(mansbj_typ_cd);size(3);null" description:"主体类型代码  等待补充码值"`
	ValidFlg        string    `orm:"column(valid_flg);size(1);null" description:"有效标志    "`
	AcctBookCategCd string    `orm:"column(acct_book_categ_cd);size(5);null" description:"账套类别代码  CN001-川农,CN002-预留1,CN003-预留2,CN004-预留3"`
	FundFlgCd       string    `orm:"column(fund_flg_cd);size(4);null" description:"资金标志编码"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_ae_event_itm) TableName() string {
	return "t_ae_event_itm"
}

func InsertT_ae_event_itm(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_event_itm).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_event_itmTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_event_itm)).Filter("biz_event_cd", maps["BizEventCd"]).
		Filter("seq_num", maps["SeqNum"]).Filter("engine_varit_cd", maps["EngineVaritCd"]).Filter("soft_prodt_cd", maps["SoftProdtCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_event_itmTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_event_itm where biz_event_cd=? and seq_num=? and engine_varit_cd=? and soft_prodt_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizEventCd"], maps["SeqNum"], maps["EngineVaritCd"], maps["SoftProdtCd"]).Exec()
	return err
}

func QueryT_ae_event_itm(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_event_itm panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_event_itm))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_event_itmById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_event_itm)).Filter("biz_event_cd", maps["BizEventCd"]).Filter("seq_num", maps["SeqNum"]).
		Filter("engine_varit_cd", maps["EngineVaritCd"]).Filter("soft_prodt_cd", maps["SoftProdtCd"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_event_itm(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_event_itm panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_event_itm)).Filter("biz_event_cd", maps["BizEventCd"]).Filter("seq_num", maps["SeqNum"]).
		Filter("engine_varit_cd", maps["EngineVaritCd"]).Filter("soft_prodt_cd", maps["SoftProdtCd"]).Filter("tcc_state", 0).Update(maps)
	return err
}
