package dao

import (
	"git.forms.io/universe/solapp-sdk/config"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
	"time"
)

const (
	DB_MAX_IDLE_CONNS   = 4
	DM_MAX_OPEN_CONNS   = 4
	DEFAULT_QUERY_LIMIT = 30
	MAX_LIMIT_VALUE     = 50000
	DB_MAX_LIFE_VALUE   = 540 //比数据库中的wait_time小即可
)

func initModel() {
	orm.RegisterModel(

		new(T_intrt_plan),					//legobank的表
		new(T_object_type),					//legobank_cm-库的表
		new(T_object_send_strategy_manage),	//legobank_cm-库的表
		new(T_object_manage),				//legobank_cm-库的表
		new(T_loan_info_tmpl_cm),			//legobank_cm-库的表
		new(T_abnormal_messages),			//legobank_cm-库的表-消息发送异常流水表

		new(T_loan_cn_list),				//legobank-库的表
		new(Um_user),				//um的表
		new(T_acct_loan_acctg_period),//---乐高数据库的表
		new(T_acct_day_bal),		  //---乐高数据库的表
		new(Ts_sys_user_role),      //---乐高数据库的表
		new(T_common_org),          //---乐高数据库的表
		new(T_common_role),         //---乐高数据库的表
		new(T_common_user),         //---乐高数据库的表
		new(T_cust_base_info),      //---乐高数据库的表
		new(T_cust_finance_info),   //---乐高数据库的表
		new(T_cust_identity),       //---乐高数据库的表
		new(Ts_user_info),          //---乐高数据库的表
		new(T_loan_consu_det),
		new(T_loan_consu_mgmt),
		new(T_loan_extn_regis),
		new(T_adj_contract_reg),
		new(T_rplan_chg_regis),
		new(T_risk_cls_appr_flow),
		new(T_intrt_chg_regis),
		new(T_risk_cls_adj_regis),
		new(T_loan_save_file),
		new(T_chln_hndl_flow),
		new(T_admt_regis),
		new(T_sim_intel_decision_system),
		new(T_intelligent_decision),
		new(T_sys_control),
		new(T_sms_sending_details),
		new(T_sms_template_temp),
		new(T_intelligent_loan_approval),
		new(T_intrt_no),
		new(Credit_approval_temp),
		new(T_ae_bal_subj_unexp),
		new(T_ae_bkpm),
		new(T_ae_daycut),
		new(T_ae_event),
		new(T_ae_event_itm),
		new(T_ae_event_temp),
		new(T_ae_itm),
		new(T_ae_jnl_entry),
		new(T_ae_jnl_entry_sum),
		new(T_ae_pro_unexp),
		new(T_ae_prod),
		new(T_ae_syslist),
		new(T_ae_unfair_lending),
		new(T_bpmn_retry_param),
		new(T_busi_org_mapping_para),
		new(T_chan_lend_expend_info),
		new(T_cust_intr_info),
		new(T_itlg_wind_appr_lend_resu),
		new(T_kernel_fin_txn_flow),
		new(T_loan_batch_contract_trans_meter),
		new(T_loan_customer_list),
		new(T_loan_indiv_contract_info),
		new(T_loan_order_business_info),
		new(T_loan_provident_fund_real_estate_info),
		new(T_loan_seq_no),
		new(T_loan_term_repay_method),
		new(T_sys_biz_date_plan),
		new(T_sys_status_control),
		new(T_loan_contract_reg),
		new(T_loan_contract_suppt_info_form),
		new(T_loan_investigate_task_result),
		new(T_loan_loan_cont),
		new(T_loan_repay_plan),
		new(T_loan_repay_term),
		new(T_loan_list),
		new(T_loan_link_acct),
		new(T_loan_repay_record),
		new(T_risk_cls_adj_rec),
		new(T_cust_repmt_day_info),
		new(T_loan_quota_mgmt),
		new(T_loan_quota_change),
		new(T_loan_quota_change_apply),
		new(T_file_manage),
		new(T_file_upd_jnl),
		new(T_loan_incoming_info),
		new(T_indv_comp_cons_crdt),
		new(T_loan_cust_base_info),
		new(T_loan_credit_approval_flow),
		new(T_contract_auth),
		new(T_loan_contact),
		new(T_itlg_wind_appr_resu),
		new(T_artif_interset_rate_flow),
		new(T_loan_credit_query_flow),
		new(T_loan_loan_app),
		new(T_loan_loan_approval_flow),
		new(T_loan_elec_cont_tmpl),
		//new(T_loan_info_tmpl),
		new(T_loan_batch_control),
		new(T_loan_curr_repay_info),
		new(T_loan_collection_aftert_loan),
		new(T_earl_warn_sign_task),
		new(T_af_bnk_coll_task_refe),
		new(T_af_bnk_ln_chk_task),
		new(T_earwar_sig_proc_flow),
		new(T_impa_regis),
		new(T_impa_writeoff_appr_flow),
		new(T_loan_collection_dec_aftert_loan),
		new(T_early_warn_decision_info),
		new(T_early_warn_task_result),
		new(T_loan_inspection_task_result),
		new(T_loan_whitelist_info),
		new(T_loan_blacklist_info),
		new(T_loan_special_change_record),
		new(T_loan_manager_customer),
		new(T_cust_busi_tran_rec),
		new(T_loan_contract_info),
		new(T_loan_contract_change_detail),
		new(Ts_sys_constant),
		new(Ts_sys_func),
		new(Ts_sys_role_func),
		new(T_role_sens),
		new(Tp_option_info),
		new(T_proc_model_para),
		new(T_loan_onduty_control),
		new(T_loan_approve_task),
		new(T_loan_process_model),
		new(T_risk_cls_ovrday_comp),
		new(T_loan_product),
		new(T_prod_flow_para),
		new(T_prod_model_para),
		new(T_loan_op_manage),
		new(T_appr_mode_para),
		new(T_artif_credit_ele_param),
		new(T_cont_equi_recr),
		new(T_lpr_org_sale_quota),
		new(T_sms_template),
		new(T_loan_gignature_position_param),
		new(T_loan_customer_credit_inquiry_temp),
		new(T_loan_credit_element_params),
		new(T_loan_provident_fund_params),
		new(T_loan_offical_seal_parameter),
		new(T_early_warn_task_feedback_temp),
		new(T_org_orgi_busi_para),
		new(T_addr_match_org),
		new(T_collection_task_feedback_temp),
		new(T_loan_collection_aftert_loan_result),
		new(T_loan_investigate_task),
		new(T_loan_repayment_bill),
	)
}

func registerMysqlDatabase(dbAlias, user, password, addr, dbName string, param string) error {
	dbStr := user + ":" + password + "@tcp(" + addr + ")/" + dbName + param
	if err := orm.RegisterDataBase(dbAlias, "mysql", dbStr); err != nil {
		log.Errorf("RegisterDatabase meet error=%v", err)
		return err
	}

	orm.Debug = true
	orm.SetMaxIdleConns(dbAlias, config.CmpSvrConfig.DBMaxIdleConns)
	orm.SetMaxOpenConns(dbAlias, config.CmpSvrConfig.DBMaxOpenConns)

	db, err := orm.GetDB(dbAlias)
	if err != nil {
		log.Errorf("orm.GetDB meet error=%v", err)
		return err
	}

	db.SetConnMaxLifetime(time.Duration(config.CmpSvrConfig.DBMaxLifeValue) * time.Second)

	return nil
}

func InitDatabase() error {
	if err := orm.RegisterDriver("mysql", orm.DRMySQL); err != nil {
		logrus.Errorf("RegisterDriver failed err=%v", err)
		return err
	}

	addr := beego.AppConfig.String("mysql::addr")
	userName := beego.AppConfig.String("mysql::user")
	password := beego.AppConfig.String("mysql::password")
	dbName := beego.AppConfig.String("mysql::database")
	param := beego.AppConfig.String("mysql::param")

	if err := registerMysqlDatabase("default", userName, password, addr, dbName, param); err != nil {
		log.Errorf("registerMysqlDatabase failed,err=%v", err)
		return err
	}

	orm.Debug, _ = beego.AppConfig.Bool("mysql::debug")
	orm.RunCommand()

	initModel()

	return nil
}
