package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_provident_fund_real_estate_info struct {
	KeprcdNo           string    `orm:"column(keprcd_no);pk" description:"记录编号"`
	ArtgclInvstgTaskNo string    `orm:"column(artgcl_invstg_task_no);size(60);null" description:"人工调查任务编号"`
	SeqNo              int       `orm:"column(seq_no);null" description:"序号"`
	RecntSavedMon      string    `orm:"column(recnt_saved_mon);size(2);null" description:"最近缴存月份"`
	PermonSavedAmt     float64   `orm:"column(permon_saved_amt);null;digits(18);decimals(2)" description:"每月缴存金额"`
	HuprpNm            string    `orm:"column(huprp_nm);size(120);null" description:"房产名称"`
	HsCharcCd          string    `orm:"column(hs_charc_cd);size(2);null" description:"房屋性质代码"`
	PurhDt             string    `orm:"column(purh_dt);type(date);null" description:"购买日期"`
	PurhAmt            float64   `orm:"column(purh_amt);null;digits(18);decimals(2)" description:"购买金额"`
	CurrWorth          float64   `orm:"column(curr_worth);null;digits(18);decimals(2)" description:"当前价值"`
	Remrk              string    `orm:"column(remrk);size(200);null" description:"备注"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_provident_fund_real_estate_info) TableName() string {
	return "t_loan_provident_fund_real_estate_info"
}

func InsertT_loan_provident_fund_real_estate_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_provident_fund_real_estate_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_provident_fund_real_estate_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_provident_fund_real_estate_info)).Filter("keprcd_no", maps["KeprcdNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_provident_fund_real_estate_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_provident_fund_real_estate_info where keprcd_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_loan_provident_fund_real_estate_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_provident_fund_real_estate_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_provident_fund_real_estate_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_provident_fund_real_estate_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_provident_fund_real_estate_info)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_provident_fund_real_estate_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_provident_fund_real_estate_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_provident_fund_real_estate_info)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
