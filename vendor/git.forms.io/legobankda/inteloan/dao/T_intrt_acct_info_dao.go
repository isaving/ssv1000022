package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intrt_acct_info struct {
	AcctiAcctNo       string    `orm:"column(accti_acct_no);pk" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	CurCd             string    `orm:"column(cur_cd);size(3);null" description:"币种代码"`
	OpenAcctOrgNo     string    `orm:"column(open_acct_org_no);size(4);null" description:"开户机构号"`
	IntacrFlg         string    `orm:"column(intacr_flg);size(1);null" description:"计息标志"`
	OpenAcctDt        string    `orm:"column(open_acct_dt);type(date);null" description:"开户日期"`
	LsttmIntacrDt     string    `orm:"column(lsttm_intacr_dt);type(date);null" description:"上次计息日期"`
	LsttmIntStlDt     string    `orm:"column(lsttm_int_stl_dt);type(date);null" description:"上次结息日期"`
	LsttmCnDwDt       string    `orm:"column(lsttm_cn_dw_dt);type(date);null" description:"上次计提日期"`
	CnDwCycCd         string    `orm:"column(cn_dw_cyc_cd);size(2);null" description:"计提周期代码"`
	CnDwCycQty        int       `orm:"column(cn_dw_cyc_qty);null" description:"计提周期数量"`
	CnDwIntrFlg       string    `orm:"column(cn_dw_intr_flg);size(1);null" description:"计提利息标志"`
	IntacrCycCd       string    `orm:"column(intacr_cyc_cd);size(2);null" description:"计息周期代码"`
	IntacrCycQty      int       `orm:"column(intacr_cyc_qty);null" description:"计息周期数量"`
	IntacrSpcyDt      string    `orm:"column(intacr_spcy_dt);type(date);null" description:"计息指定日期"`
	IntStlCycCd       string    `orm:"column(int_stl_cyc_cd);size(2);null" description:"结息周期代码"`
	IntStlCycQty      int       `orm:"column(int_stl_cyc_qty);null" description:"结息周期数量"`
	IntStlSpcyDt      string    `orm:"column(int_stl_spcy_dt);type(date);null" description:"结息指定日期"`
	HoliIntacrIndCd   string    `orm:"column(holi_intacr_ind_cd);size(3);null" description:"节假日计息标识代码  0-收取1-不收取"`
	PmitBkvtFlg       string    `orm:"column(pmit_bkvt_flg);size(1);null" description:"允许倒起息标志"`
	PmitDlydBegintFlg string    `orm:"column(pmit_dlyd_begint_flg);size(1);null" description:"允许延后起息标志"`
	GthrCmpdFlg       string    `orm:"column(gthr_cmpd_flg);size(1);null" description:"收取复利标志"`
	FinlChgacctDt     string    `orm:"column(finl_chgacct_dt);type(date);null" description:"最后动账日期"`
	MgmtOrgNo         string    `orm:"column(mgmt_org_no);size(4);null" description:"管理机构号"`
	AcctiOrgNo        string    `orm:"column(accti_org_no);size(4);null" description:"核算机构号"`
	FrzAmt            float64   `orm:"column(frz_amt);null;digits(18);decimals(2)" description:"冻结金额"`
	IntStlPrtyTypCd   string    `orm:"column(int_stl_prty_typ_cd);size(1);null" description:"结息优先类型代码"`
	DataValidFlgCd    string    `orm:"column(data_valid_flg_cd);size(1);null" description:"数据有效标志代码  1-正常2-失效,"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_intrt_acct_info) TableName() string {
	return "t_intrt_acct_info"
}

func InsertT_intrt_acct_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intrt_acct_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intrt_acct_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intrt_acct_info)).Filter("accti_acct_no", maps["AcctiAcctNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intrt_acct_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intrt_acct_info where accti_acct_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctiAcctNo"]).Exec()
	return err
}

func QueryT_intrt_acct_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intrt_acct_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intrt_acct_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intrt_acct_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intrt_acct_info)).Filter("accti_acct_no", maps["AcctiAcctNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intrt_acct_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intrt_acct_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intrt_acct_info)).Filter("accti_acct_no", maps["AcctiAcctNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
