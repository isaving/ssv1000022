package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_bal_type struct {
	AcctiAcctNo                string  `orm:"column(accti_acct_no);size(32);pk" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	IntrStusCd                 string  `orm:"column(intr_stus_cd);size(2)" description:"利息状态代码  0-正常,1-逾期"`
	Pridnum                    int     `orm:"column(pridnum)" description:"期数  分期还款计划的期数顺序号,"`
	BegintDt                   string  `orm:"column(begint_dt);type(date);null" description:"起息日期  记录开始计息的日期,"`
	ExpiryDt                   string  `orm:"column(expiry_dt);type(date);null" description:"截止日期"`
	PlanRpyintAmt              float64 `orm:"column(plan_rpyint_amt);null;digits(18);decimals(2)" description:"计划还息金额"`
	ActlRpyintAmt              float64 `orm:"column(actl_rpyint_amt);null;digits(18);decimals(2)" description:"实际还息金额"`
	AccmCnDwAmt                float64 `orm:"column(accm_cn_dw_amt);null;digits(18);decimals(2)" description:"累计计提金额"`
	IntStlAmt                  float64 `orm:"column(int_stl_amt);null;digits(18);decimals(2)" description:"结息金额"`
	RecvblPrinPnltintCmpdAmt   float64 `orm:"column(recvbl_prin_pnltint_cmpd_amt);null;digits(18);decimals(2)" description:"应收本金罚息复利金额"`
	ActlRecvPrinPnltintCmpdAmt float64 `orm:"column(actl_recv_prin_pnltint_cmpd_amt);null;digits(18);decimals(2)" description:"实收本金罚息复利金额"`
	AlrdyTranOffshetIntr       float64 `orm:"column(alrdy_tran_offshet_intr);null;digits(18);decimals(2)" description:"已转表外利息"`
	OnshetIntr                 float64 `orm:"column(onshet_intr);null;digits(18);decimals(2)" description:"表内利息"`
	DataValidFlgCd             string  `orm:"column(data_valid_flg_cd);size(1);null" description:"数据有效标志代码  1-正常2-失效,"`
	AlrdyDvalIntr              float64 `orm:"column(alrdy_dval_intr);null;digits(18);decimals(2)" description:"已减值利息  已经进行减值的利息,"`
	AlrdyWrtoffIntr            float64 `orm:"column(alrdy_wrtoff_intr);null;digits(18);decimals(2)" description:"已核销利息  已经进行核销的利息"`
	AcruUnstlIntr              float64 `orm:"column(acru_unstl_intr);null;digits(18);decimals(5)" description:"已计未结利息"`
	AccmPrinPnltintCmpdAmt     float64 `orm:"column(accm_prin_pnltint_cmpd_amt);null;digits(18);decimals(5)" description:"累计本金罚息复利金额"`
	ActlRpyintPrerdcAmt        float64 `orm:"column(actl_rpyint_prerdc_amt);null;digits(18);decimals(2)" description:"实际还息预减金额 "`
	ActlRpyintPreincAmt        float64 `orm:"column(actl_rpyint_preinc_amt);null;digits(18);decimals(2)" description:"实际还息预增金额  "`
	ActlRpyintTccStusCd        string  `orm:"column(actl_rpyint_tcc_stus_cd);size(1);null" description:"实际还息tcc状态代码  0-try,1-confirm,2-cancel"`
	ActlRecvCmpdPrerdcAmt      float64 `orm:"column(actl_recv_cmpd_prerdc_amt);null;digits(18);decimals(2)" description:"实收复利预减金额   "`
	ActlRecvCmpdPreincAmt      float64 `orm:"column(actl_recv_cmpd_preinc_amt);null;digits(18);decimals(2)" description:"实收复利预增金额   "`
	ActlRecvCmpdTccStusCd      string  `orm:"column(actl_recv_cmpd_tcc_stus_cd);size(1);null" description:"实收复利tcc状态代码  0-try,1-confirm,2-cancel"`
	FrzAmt                     float64 `orm:"column(frz_amt);null;digits(18);decimals(2)" description:"冻结金额"`
	ActlstPnltintAmt           float64 `orm:"column(actlst_pnltint_amt);null;digits(18);decimals(2)" description:"实还罚息金额"`
	FinlCalcDt                 string  `orm:"column(finl_calc_dt);type(date);null" description:"最后计算日期"`
	FinlModfyDt                string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo             string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo            string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                   int     `orm:"column(tcc_state);null"`
}

func (t *T_bal_type) TableName() string {
	return "t_bal_type"
}

func InsertT_bal_type(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_bal_type).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_bal_typeTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_bal_type)).Filter("accti_acct_no", maps["AcctiAcctNo"]).
		Filter("intr_stus_cd", maps["IntrStusCd"]).Filter("pridnum", maps["Pridnum"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_bal_typeTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_bal_type where accti_acct_no=? and intr_stus_cd=? and pridnum=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctiAcctNo"], maps["IntrStusCd"], maps["Pridnum"]).Exec()
	return err
}

func QueryT_bal_type(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_bal_type panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_bal_type))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_bal_typeById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_bal_type)).Filter("accti_acct_no", maps["AcctiAcctNo"]).Filter("intr_stus_cd", maps["IntrStusCd"]).
		Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_bal_type(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_bal_type panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_bal_type)).Filter("accti_acct_no", maps["AcctiAcctNo"]).Filter("intr_stus_cd", maps["IntrStusCd"]).
		Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", 0).Update(maps)
	return err
}
