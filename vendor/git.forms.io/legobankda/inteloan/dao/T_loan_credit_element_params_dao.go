package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_credit_element_params struct {
	BizSn                  		  string    `orm:"column(biz_sn);pk" description:"业务流水号  pk"`
	LprOrgNo                      string    `orm:"column(lpr_org_no);size(4);null" description:"法人机构号"`
	LprOrgNm                      string    `orm:"column(lpr_org_nm);size(120);null" description:"法人机构名称"`
	LoanProdtNo                   string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtNm                   string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	IntrtTypCd                    string    `orm:"column(intrt_typ_cd);size(1);null" description:"利率类型代码  1-固定利率,2-浮动利率"`
	LprIntrtBpFlotCeilVal         float64   `orm:"column(lpr_intrt_bp_flot_ceil_val);null;digits(9);decimals(6)" description:"lpr利率bp浮动上限值"`
	LprIntrtBpFlotFloorVal        float64   `orm:"column(lpr_intrt_bp_flot_floor_val);null;digits(9);decimals(6)" description:"lpr利率bp浮动下限值"`
	PbocBnchmkIntrtFlotCeilRatio  float64   `orm:"column(pboc_bnchmk_intrt_flot_ceil_ratio);null;digits(9);decimals(6)" description:"人行基准利率浮动上限比例"`
	PbocBnchmkIntrtFlotFloorRatio float64   `orm:"column(pboc_bnchmk_intrt_flot_floor_ratio);null;digits(9);decimals(6)" description:"人行基准利率浮动下限比例"`
	QtaTypCd                      string    `orm:"column(qta_typ_cd);size(1);null" description:"额度类型代码"`
	QtaFlotCeilRatio              float64   `orm:"column(qta_flot_ceil_ratio);null;digits(9);decimals(6)" description:"额度浮动上限比例"`
	QtaFlotFloorRatio             float64   `orm:"column(qta_flot_floor_ratio);null;digits(9);decimals(6)" description:"额度浮动下限比例"`
	KeprcdStusCd                  string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CrtEmpnbr                     string    `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo                      string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	CrtDt                         string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	FinlModfyDt                   string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                   string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo                string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo               string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState               		  int8      `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_credit_element_params) TableName() string {
	return "t_loan_credit_element_params"
}

func InsertT_loan_credit_element_params(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_credit_element_params).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_credit_element_paramsTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_credit_element_params)).Filter("biz_sn", maps["BizSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_credit_element_paramsTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_credit_element_params where biz_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizSn"]).Exec()
	return err
}

func QueryT_loan_credit_element_params(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_credit_element_params panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_credit_element_params))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_credit_element_paramsById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	BizSn := params["BizSn"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_credit_element_params)).Filter("biz_sn", BizSn).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_credit_element_params(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_credit_element_params panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	BizSn := params["BizSn"]
	_, err = o.QueryTable(new(T_loan_credit_element_params)).Filter("biz_sn", BizSn).Filter("tcc_state", 0).Update(params)
	return err
}
