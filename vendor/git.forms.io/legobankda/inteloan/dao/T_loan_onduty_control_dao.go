package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_onduty_control struct {
	Empnbr             string `orm:"column(empnbr);pk" description:"员工号  描述银行为统一管理,根据既定规则生成并分配给员工的唯一编码,在全行内具有唯一性"`
	EmplyName          string `orm:"column(emply_name);size(60);null" description:"员工姓名  描述员工的中文名称"`
	UserOrgNo          string `orm:"column(user_org_no);size(4);null" description:"用户机构号 "`
	MtguarStusCd       string `orm:"column(mtguar_stus_cd);size(1);null" description:"上岗状态代码 1:在岗 9:离岗"`
	LgnVouchCd         string `orm:"column(lgn_vouch_cd);size(64);null" description:"登录凭证编码"`
	RecntMtguarDt      string `orm:"column(recnt_mtguar_dt);type(date);null" description:"最近上岗日期 "`
	RecntMtguarTm      string `orm:"column(recnt_mtguar_tm);type(time);null" description:"最近上岗时间 "`
	RecntLeaveOfficeDt string `orm:"column(recnt_leave_office_dt);type(date);null" description:"最近离岗日期 "`
	RecntLeaveOfficeTm string `orm:"column(recnt_leave_office_tm);type(time);null" description:"最近离岗时间 "`
	TccState           int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_onduty_control) TableName() string {
	return "t_loan_onduty_control"
}

func InsertT_loan_onduty_control(o orm.Ormer, maps orm.Params) error {
	if maps["TccState"]!=nil && maps["TccState"]=="0" {
		delete(maps,"TccState")
		sql := util.BuildSqlTcc(maps, new(T_loan_onduty_control).TableName(),0)
		_, err := o.Raw(sql).Exec()
		return err
	}
	sql := util.BuildSql(maps, new(T_loan_onduty_control).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_onduty_controlTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_onduty_control)).Filter("empnbr", maps["Empnbr"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_onduty_controlTccState(o orm.Ormer, maps orm.Params) error {
	if maps["TccState"]!=nil && maps["TccState"]=="0" {
		sql := "delete from t_loan_onduty_control where empnbr=? and tcc_state = 0"
		_, err := o.Raw(sql, maps["Empnbr"]).Exec()
		return err
	}
	sql := "delete from t_loan_onduty_control where empnbr=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["Empnbr"]).Exec()
	return err
}

func QueryT_loan_onduty_control(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_onduty_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_onduty_control))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_onduty_controlById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_onduty_control)).Filter("empnbr", maps["Empnbr"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_onduty_control(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_onduty_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_onduty_control)).Filter("empnbr", maps["Empnbr"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
