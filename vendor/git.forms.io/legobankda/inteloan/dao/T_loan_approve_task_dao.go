package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_approve_task struct {
	TaskExampId       string    `orm:"column(task_examp_id);pk" description:"任务实例id"`
	TaskNm            string    `orm:"column(task_nm);size(120);null" description:"任务名称"`
	BizId             string    `orm:"column(biz_id);size(50);null" description:"业务id  长度60"`
	ProcesExampId     string    `orm:"column(proces_examp_id);size(50);null" description:"流程实例id"`
	DistrPersonEmpnbr string    `orm:"column(distr_person_empnbr);size(6);null" description:"分配人员工号"`
	CanExctrRoleNo    string    `orm:"column(can_exctr_role_no);size(50);null" description:"可执行人角色编号"`
	CrtDt             string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtTm             string    `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	OperTm            string    `orm:"column(oper_tm);type(time);null" description:"操作时间"`
	ProcesTmplId      string    `orm:"column(proces_tmpl_id);size(50);null" description:"流程模板id"`
	CorgTaskStusCd    string    `orm:"column(corg_task_stus_cd);size(2);null" description:"协办任务状态代码  01-待协办处理,02-待确认"`
	CorgTaskExampId   string    `orm:"column(corg_task_examp_id);size(50);null" description:"协办任务实例id"`
	CustNo            string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	TaskTypCd         string    `orm:"column(task_typ_cd);size(3);null" description:"任务类型代码    01-流程任务,02-非流程任务,03-启动任务,04-协办任务"`
	LoanProdtNo       string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号"`
	LprOrgNo          string    `orm:"column(lpr_org_no);size(4);null" description:"法人机构号"`
	BizOrgNo          string    `orm:"column(biz_org_no);size(4);null" description:"业务机构号  员工中心,建立了行政机构树和业务机构树,此机构号,表示业务机构树上面的机构号,"`
	SticDt            string    `orm:"column(stic_dt);type(date);null" description:"置顶日期"`
	SticTm            string    `orm:"column(stic_tm);type(time);null" description:"置顶时间"`
	TaskTmplId        string    `orm:"column(task_tmpl_id);size(60);null" description:"任务模板id"`
	MenuFuncId        string    `orm:"column(menu_func_id);size(20);null" description:"菜单功能id"`
	TaskDlwthStusCd   string    `orm:"column(task_dlwth_stus_cd);size(2);null" description:"任务处理状态代码  01-待处理05-已过期09-已处理"`
	OperDt            string    `orm:"column(oper_dt);type(date);null" description:"操作日期"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState          int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_approve_task) TableName() string {
	return "t_loan_approve_task"
}

func InsertT_loan_approve_task(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_approve_task).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_approve_taskTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_approve_task)).Filter("task_examp_id", maps["TaskExampId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_approve_taskTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_approve_task where task_examp_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TaskExampId"]).Exec()
	return err
}

func QueryT_loan_approve_task(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_approve_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_approve_task))
	OrderBy := params["OrderBy"]
	delete(params,"OrderBy")
	flag :=params["Flag"]
	delete(params,"Flag")
	if flag=="9"{
		sql := "SELECT COUNT(0) COUNT,distr_person_empnbr DistrPersonEmpnbr FROM t_loan_approve_task WHERE task_dlwth_stus_cd=\"01\" GROUP BY distr_person_empnbr LIMIT 2"
		_, err = o.Raw(sql,).Values(&maps)
		return
	}
	if flag=="8"{
		sql := "SELECT t1.task_examp_id CollTaskNo ,t1.corg_task_examp_id CorgTaskExampId, t2.task_dlwth_stus_cd CollTaskDlwthStusCd , t2.cust_no CustNo, t2.cust_name CustName,t2.indv_crtf_typ_cd indvCrtfTypCd, "+
			"t2.indv_crtf_no IndvCrtfNo, t2.crt_dt CrtDt ,t2.loan_dubil_no LoanDubilNo FROM `t_loan_approve_task` t1 , `t_loan_collection_aftert_loan` t2 WHERE t1.task_examp_id = t2.coll_task_no and t1.tcc_state=0 and t2.tcc_state=0 "
		if params["CollTaskNo"] != nil {
			sql =sql + " and t1.task_examp_id=\"" + params["CollTaskNo"].(string)+"\""
		}
		if params["CollTaskDlwthStusCd"] != nil {
			sql =sql + " and t1.task_dlwth_stus_cd=\"" + params["CollTaskDlwthStusCd"].(string)+"\""
		}
		_, err = o.Raw(sql,).Values(&maps)
		return
	}
	if flag=="7"{
		maps, err = QueryT_loan_approve_taskFlag7(params)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "TaskStartDate" {
				qs = qs.Filter("crt_dt__gte", v)
			}else if k == "TaskEndDate" {
				qs = qs.Filter("crt_dt__lte", v)
			}else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if OrderBy != nil {
		qs = qs.OrderBy(OrderBy.(string))
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_approve_task1(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_approve_task1 panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_approve_task))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_approve_taskById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_approve_task)).Filter("task_examp_id", maps["TaskExampId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_approve_task(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_approve_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_approve_task)).Filter("task_examp_id", maps["TaskExampId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
func QueryT_loan_approve_taskFlag7(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT "+
		"  a.task_examp_id TaskExampId,"+
		"  b.loan_prodt_vers_no LoanProdtVersNo,"+
		"  b.loan_prodt_no LoanProdtNo,"+
		"  b.cust_no CustNo,"+
		"  b.cust_name CustName,"+
		"  b.init_qta InitQta,"+
		"  b.curr_crdt_qta CurrCrdtQta,"+
		"  b.crdt_aply_sn CrdtAplySn,"+
		"  b.crt_dt CrtDt,"+
		"  b.lsttm_qta_adj_manr_cd IsttmQtaAdjManrCd,"+
		"  b.qta_stus_cd QtaStusCd,"+
		"  b.qta_efft_dt QtaEfftDt,"+
		"  b.qta_matr_dt QtaMatrDt,"+
		"  b.frz_qta FrzQta,"+
		"  b.alrdy_use_qta AlrdyUseQta,"+
		"  c.chg_aply_no ChgAplyNo "+
		"FROM"+
		"  t_loan_approve_task a "+
		"  LEFT JOIN t_loan_quota_mgmt b "+
		"    ON a.biz_id = b.crdt_aply_sn "+
		"  LEFT JOIN t_loan_quota_change_apply c "+
		"    ON a.biz_id = c.crdt_aply_sn "+
		"WHERE 1=1 "
	if maps["MenuFuncId"] != nil {
		sql = sql +" and a.menu_func_id =\""+ maps["MenuFuncId"].(string)+"\""
	}
	if maps["DistrPersonEmpnbr"] != nil {
		sql = sql +" and a.distr_person_empnbr =\""+ maps["DistrPersonEmpnbr"].(string)+"\""
	}
	if maps["TaskDlwthStusCd"] != nil {
		sql = sql +" and a.task_dlwth_stus_cd =\""+ maps["TaskDlwthStusCd"].(string)+"\""
	}
	if maps["AplyAdjTypCd"] != nil {
		sql = sql +" and c.aply_adj_typ_cd =\""+ maps["AplyAdjTypCd"].(string)+"\""
	}
	if maps["CrdtAplySn"] != nil {
		sql = sql +" and b.crdt_aply_sn =\""+ maps["CrdtAplySn"].(string)+"\""
	}
	if maps["CustNo"] != nil {
		sql = sql +" and b.cust_no =\""+ maps["CustNo"].(string)+"\""
	}
	if maps["CustName"] != nil {
		sql = sql +" and b.cust_name =\""+ maps["CustName"].(string)+"\""
	}
	if maps["QtaStusCd"] != nil {
		sql = sql +" and b.qta_stus_cd =\""+ maps["QtaStusCd"].(string)+"\""
	}
	if maps["QtaEfftDtStart"] != nil {
		sql = sql +" and b.qta_efft_dt >=\""+ maps["QtaEfftDtStart"].(string)+"\""
	}
	if maps["QtaEfftDtEnd"] != nil {
		sql = sql +" and b.qta_efft_dt <=\""+ maps["QtaEfftDtStQtaEfftDtEndart"].(string)+"\""
	}

	_, err = o.Raw(sql).Values(&mapList)
	return
}