package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intrt_no struct {
	IntrtNo          string  `orm:"column(intrt_no);size(40);pk" description:"利率编号  PK"`
	OrgNo            string  `orm:"column(org_no);size(4)" description:"机构号"`
	InvalidFlg       string  `orm:"column(invalid_flg);size(1)" description:"失效标志  0否1是"`
	CurCd            string  `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	BnchmkIntrt      float64 `orm:"column(bnchmk_intrt);null;digits(9);decimals(6)" description:"基准利率"`
	EfftDt           string  `orm:"column(efft_dt);type(date);null" description:"生效日期  记录贷款合同的生效日期,"`
	TodayModfyFlg    string  `orm:"column(today_modfy_flg);size(1);null" description:"当日修改标志  0否1是"`
	IntrtNm          string  `orm:"column(intrt_nm);size(120);null" description:"利率名称"`
	IntrtMktComnt    string  `orm:"column(intrt_mkt_comnt);size(200);null" description:"利率市场说明  利率来源"`
	IntrtDeadlCycCd  string  `orm:"column(intrt_deadl_cyc_cd);size(1);null" description:"利率期限周期代码  参考标准代码CD0075"`
	BaseIntrtTypCd   string  `orm:"column(base_intrt_typ_cd);size(10);null" description:"基础利率类型代码  1-人行基准利率,,2-LPR利率"`
	IntrtDeadlCycQty int     `orm:"column(intrt_deadl_cyc_qty);null" description:"利率期限周期数量"`
	Flag1            string  `orm:"column(flag1);size(2);null" description:"标志1  2-月利率"`
	Flag2            string  `orm:"column(flag2);size(2);null" description:"标志2"`
	Bak1             float64 `orm:"column(bak1);null;digits(18);decimals(6)" description:"备用1"`
	Bak2             float64 `orm:"column(bak2);null;digits(18);decimals(6)" description:"备用2"`
	Bak3             string  `orm:"column(bak3);type(date);null" description:"备用3"`
	Bak4             string  `orm:"column(bak4);size(10);null" description:"备用4"`
	FinlModfyDt      string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccKeprcdVisFlg  string  `orm:"column(tcc_keprcd_vis_flg);size(1);null" description:"tcc记录可见标志  0否1是"`
	TccState         int     `orm:"column(tcc_state);null"`
}

func (t *T_intrt_no) TableName() string {
	return "t_intrt_no"
}

func InsertT_intrt_no(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intrt_no).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intrt_noTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intrt_no)).Filter("intrt_no", maps["IntrtNo"]).
		Filter("org_no", maps["OrgNo"]).Filter("invalid_flg", maps["InvalidFlg"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intrt_noTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intrt_no where intrt_no=? and org_no=? and invalid_flg=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IntrtNo"], maps["OrgNo"], maps["InvalidFlg"]).Exec()
	return err
}

func QueryT_intrt_no(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intrt_no panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_intrt_no))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9"{	//tc
		if _, err = qs.Filter("tcc_state", state).OrderBy("-efft_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intrt_noById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intrt_no)).Filter("intrt_no", maps["IntrtNo"]).Filter("org_no", maps["OrgNo"]).
		Filter("invalid_flg", maps["invalid_flg"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intrt_no(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intrt_no panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intrt_no)).Filter("intrt_no", maps["IntrtNo"]).Filter("org_no", maps["OrgNo"]).
		Filter("invalid_flg", maps["invalid_flg"]).Filter("tcc_state", 0).Update(maps)
	return err
}
