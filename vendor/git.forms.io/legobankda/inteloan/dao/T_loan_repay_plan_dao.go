package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_repay_plan struct {
	LoanDubilNo          string    `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	SeqNo                int       `orm:"column(seq_no)" description:"序号"`
	RprincPridnum        int       `orm:"column(rprinc_pridnum);null" description:"还本期数"`
	CustNo               string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	SpDay                string    `orm:"column(sp_day);size(2);null" description:"结本日"`
	IntStlDay            string    `orm:"column(int_stl_day);size(2);null" description:"结息日"`
	RepayPlanValidFlg    string    `orm:"column(repay_plan_valid_flg);size(1);null" description:"还款计划有效标志  0否1是"`
	PlanStartDt          string    `orm:"column(plan_start_dt);type(date);null" description:"计划起始日期"`
	PlanMatrDt           string    `orm:"column(plan_matr_dt);type(date);null" description:"计划到期日期"`
	RepayManrCd     	 string    `orm:"column(repay_manr_cd);size(2);null" description:"还本付息方式代码  等额本息,等额本金,利随本清,周期付息到期还本,周期付息周期比例还本,周期付息周期定额还本"`
	IntStlCycCd          string    `orm:"column(int_stl_cyc_cd);size(2);null" description:"结息周期代码  参考标准代码:CD0075年,季,月,双周,周,日"`
	IntStlCycQty         int       `orm:"column(int_stl_cyc_qty);null" description:"结息周期数量  描述结息日周期的次数,比如,按月付息,按年还本,那么,每个月结息一次,一年结本一次,"`
	RpyintPridnum        int       `orm:"column(rpyint_pridnum);null" description:"还息期数"`
	EqamtEvrpridRepayAmt float64   `orm:"column(eqamt_evrprid_repay_amt);null;digits(18);decimals(2)" description:"等额每期还款金额  等额:每一期的还款金额都相同,"`
	SpCycCd              string    `orm:"column(sp_cyc_cd);size(3);null" description:"结本周期代码  参考标准代码:CD0075年,季,月,双周,周,日描述结本日周期单位:日,周,月,年,结算本金日期,T日,"`
	SpCycQty             int       `orm:"column(sp_cyc_qty);null" description:"结本周期数量  描述结算本金周期的次数,比如,按月付息,按年还本,那么,每个月结息一次,一年结本一次,"`
	CycRatioRprincRatio  float64   `orm:"column(cyc_ratio_rprinc_ratio);null;digits(9);decimals(6)" description:"周期比例还本比例"`
	CycFixFrhdRprincAmt  float64   `orm:"column(cyc_fix_frhd_rprinc_amt);null;digits(18);decimals(2)" description:"周期定额还本金额"`
	FinlModfyDt          string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm          string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo       string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_repay_plan) TableName() string {
	return "t_loan_repay_plan"
}

func InsertT_loan_repay_plan(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_repay_plan where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_repay_plan).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_repay_planTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_repay_plan where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_repay_plan)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_repay_planTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_repay_plan where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_repay_plan where loan_dubil_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_repay_plan(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_repay_plan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_repay_plan))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_repay_planById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_repay_plan)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_repay_plan(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_repay_plan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := maps["Flag"]
	delete(maps,"Flag")
	if flag == "9"{		//lxf
		_, err = o.QueryTable(new(T_loan_repay_plan)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
			Filter("tcc_state", 0).Update(maps)
		return err
	}
	_, err = o.QueryTable(new(T_loan_repay_plan)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
