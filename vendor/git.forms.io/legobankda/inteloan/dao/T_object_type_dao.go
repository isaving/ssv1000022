package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_object_type struct {
	TmplType        string `orm:"column(tmpl_type);pk" description:"模板类型"`
	GenericTmplNo   string `orm:"column(generic_tmpl_no);size(40);null" description:"通用模板编号"`
	SpecialTmplNo   string `orm:"column(special_tmpl_no);size(40);null" description:"特殊模板编号"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState        int    `orm:"column(tcc_state);null" description:"tcc状态 0-Normal，1-Insert"`
}

func (t *T_object_type) TableName() string {
	return "t_object_type"
}

func InsertT_object_type(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_object_type).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_object_typeTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_object_type)).Filter("tmpl_type", maps["TmplType"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_object_typeTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_object_type where tmpl_type=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplType"]).Exec()
	return err
}

func QueryT_object_type(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_object_type panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_object_type))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_object_typeById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_object_type)).Filter("tmpl_type", maps["TmplType"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_object_type(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_object_type panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_object_type)).Filter("tmpl_type", maps["TmplType"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
