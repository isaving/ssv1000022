package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_appr_mode_para struct {
	OrgNo                         string    `orm:"column(org_no);size(4);pk" description:"机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	SeqNo                         int       `orm:"column(seq_no)" description:"序号  10PK"`
	LoanProdtNo                   string    `orm:"column(loan_prodt_no);size(20)" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo               string    `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	LoanGuarManrCd                string    `orm:"column(loan_guar_manr_cd);size(2)" description:"贷款担保方式代码  参考标准代码:CD00902"`
	KeprcdStusCd                  string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	OrgNm                         string    `orm:"column(org_nm);size(120);null" description:"机构名称"`
	LoanProdtNm                   string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	QtaTypCd                      string    `orm:"column(qta_typ_cd);size(1);null" description:"额度类型代码  循环额度,非循环额度"`
	LnrwSmlGroupPeoNo             int       `orm:"column(lnrw_sml_group_peo_no);null" description:"贷审小组人数"`
	LonRvwPeoNo                   int       `orm:"column(lon_rvw_peo_no);null" description:"贷审会人数"`
	IdpnApprvrModeFloorAmt        float64   `orm:"column(idpn_apprvr_mode_floor_amt);null;digits(18);decimals(2)" description:"独立审批人模式下限金额"`
	LnrwSmlGroupModeFloorAmt      float64   `orm:"column(lnrw_sml_group_mode_floor_amt);null;digits(18);decimals(2)" description:"贷审小组模式下限金额"`
	LonRvwModeFloorAmt            float64   `orm:"column(lon_rvw_mode_floor_amt);null;digits(18);decimals(2)" description:"贷审会模式下限金额"`
	IdpnApprvrModeAmtGtValScpCd   string    `orm:"column(idpn_apprvr_mode_amt_gt_val_scp_cd);size(2);null" description:"独立审批人模式金额取值范围代码  1-起始包含,2-终止包含,3-起止都包含,4-起止不包含"`
	LnrwSmlGroupModeAmtGtValScpCd string    `orm:"column(lnrw_sml_group_mode_amt_gt_val_scp_cd);size(2);null" description:"贷审小组模式金额取值范围代码  1-起始包含,2-终止包含,3-起止都包含,4-起止不包含"`
	LonRvwModeAmtGtValScpCd       string    `orm:"column(lon_rvw_mode_amt_gt_val_scp_cd);size(2);null" description:"贷审会模式金额取值范围代码  1-起始包含,2-终止包含,3-起止都包含,4-起止不包含"`
	IdpnApprvrCeilAmt             float64   `orm:"column(idpn_apprvr_ceil_amt);null;digits(18);decimals(2)" description:"独立审批人上限金额"`
	LnrwSmlGroupModeCeilAmt       float64   `orm:"column(lnrw_sml_group_mode_ceil_amt);null;digits(18);decimals(2)" description:"贷审小组模式上限金额"`
	LonRvwModeCeilAmt             float64   `orm:"column(lon_rvw_mode_ceil_amt);null;digits(18);decimals(2)" description:"贷审会模式上限金额"`
	EfftDt                        string    `orm:"column(efft_dt);type(date);null" description:"生效日期  记录贷款合同的生效日期,"`
	ExpiryDt                      string    `orm:"column(expiry_dt);type(date);null" description:"截止日期"`
	Remrk                         string    `orm:"column(remrk);size(200);null" description:"备注  关于其他情况的说明,320"`
	CrtDt                         string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtEmpnbr                     string    `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo                      string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt                   string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                   string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo                string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo               string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                      int       `orm:"column(tcc_state);null"`
}

func (t *T_appr_mode_para) TableName() string {
	return "t_appr_mode_para"
}

func InsertT_appr_mode_para(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_appr_mode_para where org_no=? and loan_prodt_no=? and loan_guar_manr_cd=?"
		o.Raw(sql ,maps["OrgNo"] ,maps["LoanProdtNo"],maps["LoanGuarManrCd"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_appr_mode_para).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_appr_mode_paraTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_appr_mode_para where org_no=? and loan_prodt_no=? and loan_guar_manr_cd=?"
		o.Raw(sql ,maps["OrgNo"] ,maps["LoanProdtNo"],maps["LoanGuarManrCd"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_appr_mode_para)).Filter("org_no", maps["OrgNo"]).
		Filter("seq_no", maps["SeqNo"]).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("loan_guar_manr_cd", maps["LoanGuarManrCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_appr_mode_paraTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_appr_mode_para where org_no=? and loan_prodt_no=? and loan_guar_manr_cd=?"
		o.Raw(sql ,maps["OrgNo"] ,maps["LoanProdtNo"],maps["LoanGuarManrCd"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_appr_mode_para where org_no=? and seq_no=? and loan_prodt_no=? and loan_guar_manr_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OrgNo"], maps["SeqNo"],maps["LoanProdtNo"],maps["LoanGuarManrCd"]).Exec()
	return err
}

func QueryT_appr_mode_para(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_appr_mode_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_appr_mode_para))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_appr_mode_paraById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_appr_mode_para)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("loan_guar_manr_cd", maps["LoanGuarManrCd"]).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_appr_mode_para(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_appr_mode_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_appr_mode_para)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("loan_guar_manr_cd", maps["LoanGuarManrCd"]).Filter("tcc_state", 0).Update(maps)
	return err
}
