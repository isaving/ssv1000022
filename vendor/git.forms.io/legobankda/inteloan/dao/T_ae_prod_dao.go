package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_prod struct {
	SoftProdtCd     string `orm:"column(soft_prodt_cd);pk" description:"软件产品编码  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'"`
	ProdtNm         string `orm:"column(prodt_nm);size(60);null" description:"产品名称"`
	EfftDt          string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	ValidFlg        string `orm:"column(valid_flg);size(1);null" description:"有效标志  0-否,1-是"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int    `orm:"column(tcc_state);null"`
}

func (t *T_ae_prod) TableName() string {
	return "t_ae_prod"
}

func InsertT_ae_prod(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_prod).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_prodTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_prod)).Filter("soft_prodt_cd", maps["SoftProdtCd"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_prodTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_prod where soft_prodt_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["SoftProdtCd"]).Exec()
	return err
}

func QueryT_ae_prod(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_prod panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_prod))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_prodById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_prod)).Filter("soft_prodt_cd", maps["SoftProdtCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_prod(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_prod panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_prod)).Filter("soft_prodt_cd", maps["SoftProdtCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
