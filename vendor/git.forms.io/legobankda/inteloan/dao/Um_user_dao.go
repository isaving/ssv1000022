package dao

import (
	"errors"
	"fmt"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Um_user struct {
	Id				string	`orm:"column(id);pk" description:"用户ID"`
	UserName		string	`orm:"column(user_name);"`
	Nickname		string	`orm:"column(nickname);"`
	Email			string	`orm:"column(email);"`
	Mobile			string	`orm:"column(mobile);"`
	IsActive		string	`orm:"column(is_active);"`
	IsLocked        string	`orm:"column(is_locked);"`
	LockedTime      string  `orm:"column(locked_time);"`
	FailCount       string  `orm:"column(fail_count);"`
	LastLoginTime   string  `orm:"column(last_login_time);"`
	LastLoginIp     string  `orm:"column(last_login_ip);"`
	AuthType        string	`orm:"column(auth_type);"`
	CreatedAt       string  `orm:"column(created_at);"`
	UpdatedAt       string  `orm:"column(updated_at);"`
	DeletedAt       string  `orm:"column(deleted_at);"`
	AccessBegin     string  `orm:"column(access_begin);"`
	AccessEnd       string	`orm:"column(access_end);"`

	TccState int `orm:"column(tcc_state);null"`
}

func (t *Um_user) TableName() string {
	return "Um_user"
}

func InsertUm_user(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Um_user).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateUm_userTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Um_user)).Filter("tx_reqe_cd", maps["TxReqeCd"]).
		Filter("chnl_no_cd", maps["ChnlNoCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteUm_userTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from Um_user where tx_reqe_cd=? and chnl_no_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxReqeCd"], maps["ChnlNoCd"]).Exec()
	return err
}

func QueryUm_user(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryUm_user panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	//select t1.*,group_concat(t2.`role_id`) RoleId from (
	//	select u1.`id` id ,u1.`user_name` UserId, u1.`nickname` Name,u1.`is_active` Status,u1.`created_at` CreateDate,DATE_FORMAT(u1.updated_at,'%Y-%m-%d') LastMaintDate,
	//		u1.email Email,u1.mobile Mobile,u1.locked_time LockedTime,
	//		DATE_FORMAT(u1.updated_at,'%H:%i:%s') LastMaintTime,
	//		u2.`group_id` GroupId,u3.org_id OrgId from `um_user` u1 left join `um_relation` u2 on u1.`id` = u2.`user_id`
	//		left join `um_user_group` u3 on u2.`group_id`=u3.`id`
	//		) t1 ,`um_user_role_relation` t2 where t1.id = t2. user_id
	//		group by t2. user_id;
	o := orm.NewOrm()
	sql := "SELECT t1.*,GROUP_CONCAT(t2.`role_id`) RoleId FROM ( "+
	"SELECT u1.`id` Id ,u1.`user_name` UserId, u1.`nickname` NAME,u1.`is_active` STATUS, "+
	"u1.`created_at` CreateDate,DATE_FORMAT(u1.updated_at,'%Y-%m-%d') LastMaintDate, "+
	"u1.email Email,u1.mobile Mobile,u1.locked_time LockedTime, "+
	"DATE_FORMAT(u1.updated_at,'%H:%i:%s') LastMaintTime,u2.`group_id` GroupId,u3.org_id OrgId  "+
	"FROM `um_user` u1  "+
	"LEFT JOIN `um_relation` u2 ON u1.`id` = u2.`user_id` "+
	"LEFT JOIN `um_user_group` u3 ON u2.`group_id`=u3.`id` "+
	") t1 ,`um_user_role_relation` t2 WHERE t1.id = t2. user_id "
	if nil != params["UserId"] {
		sql = sql +" AND t1.UserId='"+params["UserId"].(string)+"'"
	}
	if nil != params["OrgId"] {
		sql = sql +" AND t1.OrgId='"+params["OrgId"].(string)+"'"
	}
	if nil != params["RoleId"] {
		sql = sql +" AND t2.role_id='"+params["RoleId"].(string)+"'"
	}
	sql = sql+" GROUP BY t2. user_id"
	log.Info("sql:",sql)
	_, err =o.Raw(sql).Values(&maps)
	return
}

func QueryUm_userById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Um_user)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateUm_user(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateUm_user panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Um_user)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
