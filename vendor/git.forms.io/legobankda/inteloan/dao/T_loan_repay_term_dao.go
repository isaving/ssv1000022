package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_repay_term struct {
	LoanDubilNo           string  `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	SeqNo                 int     `orm:"column(seq_no)" description:"序号  N"`
	RecalDt               string  `orm:"column(recal_dt);type(date)" description:"重算日期"`
	CustNo                string  `orm:"column(cust_no);size(20);null" description:"客户编号"`
	RemainPrin            float64 `orm:"column(remain_prin);null;digits(18);decimals(2)" description:"剩余本金"`
	ActlstPrin            float64 `orm:"column(actlst_prin);null;digits(18);decimals(2)" description:"实还本金"`
	ActlstIntr            float64 `orm:"column(actlst_intr);null;digits(18);decimals(2)" description:"实还利息"`
	Pridnum               int     `orm:"column(pridnum);null" description:"期数  分期还款计划的期数顺序号,"`
	KeprcdStusCd          string  `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CurrPeriodRecalFlg    string  `orm:"column(curr_period_recal_flg);size(1);null" description:"当期重算标志  0否1是,初次建立,重算"`
	CurrPeriodIntacrBgnDt string  `orm:"column(curr_period_intacr_bgn_dt);type(date);null" description:"当期计息开始日期"`
	CurrPeriodIntacrEndDt string  `orm:"column(curr_period_intacr_end_dt);type(date);null" description:"当期计息结束日期"`
	RepayDt               string  `orm:"column(repay_dt);type(date);null" description:"还款日期"`
	PlanRepayTotlAmt      float64 `orm:"column(plan_repay_totl_amt);null;digits(22);decimals(2)" description:"计划还款总金额"`
	PlanRepayPrin         float64 `orm:"column(plan_repay_prin);null;digits(18);decimals(2)" description:"计划还款本金"`
	PlanRepayIntr         float64 `orm:"column(plan_repay_intr);null;digits(18);decimals(2)" description:"计划还款利息"`
	Fee                   float64 `orm:"column(fee);null;digits(18);decimals(2)" description:"费用"`
	FinlModfyDt           string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm           string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo        string  `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo       string  `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState              int8    `orm:"column(tcc_state);null"`
}

func (t *T_loan_repay_term) TableName() string {
	return "t_loan_repay_term"
}

func InsertT_loan_repay_term(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_repay_term where loan_dubil_no=? and recal_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["RecalDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_repay_term).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_repay_termTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_repay_term where loan_dubil_no=? and recal_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["RecalDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_repay_term)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("recal_dt", maps["RecalDt"]).Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_repay_termTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_repay_term where loan_dubil_no=? and recal_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["RecalDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_repay_term where loan_dubil_no=? and recal_dt=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["RecalDt"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_repay_term(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_repay_term panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params, "Flag")
	if flag == "7" {
		maps, err = QueryT_loan_repay_termFlag7(params)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_repay_term))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "RepayDate" {
				qs = qs.Filter("curr_period_intacr_bgn_dt__lt", v).Filter("curr_period_intacr_end_dt__gte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if flag == "9" { //只做排序
		if _, err = qs.Filter("tcc_state", state).OrderBy("repay_dt").Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag == "8" { //lxf
		if _, err = qs.Filter("tcc_state", state).Exclude("keprcd_stus_cd", "09").Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag == "1" { //zxt
		/*if _, err = qs.Filter("tcc_state", state).Exclude("keprcd_stus_cd", "09").Filter("repay_dt__gte", time.Now().Format("2006-01-02")).OrderBy("repay_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}*/
		if _, err = qs.Filter("tcc_state", state).Filter("keprcd_stus_cd", "01").OrderBy("repay_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_repay_termById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_repay_term)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("recal_dt", maps["RecalDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_repay_term(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_repay_term pamnic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := maps["Flag"]
	delete(maps, "Flag")
	if flag == "9" { //lxf
		_, err = o.QueryTable(new(T_loan_repay_term)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
			Filter("tcc_state", 0).Update(maps)
		return err
	}
	_, err = o.QueryTable(new(T_loan_repay_term)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("recal_dt", maps["RecalDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_repay_termFlag7(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  " +
		"t1.loan_dubil_no LoanDubilNo, " +
		"t1.cust_no CustNo, " +
		"t1.actlst_prin ActlstPrin, " +
		"t1.actlst_intr ActlstIntr, " +
		"t1.keprcd_stus_cd KeprcdStusCd, " +
		"t1.finl_modfy_dt FinlModfyDt, " +
		"t1.finl_modfy_tm FinlModfyTm, " +
		"t2.cust_name  CustName " +
		"FROM " +
		"t_loan_repay_term t1,t_loan_cust_base_info t2 " +
		"WHERE keprcd_stus_cd='03' AND t1.cust_no=t2.cust_no and t1.tcc_state=0 and t2.tcc_state=0 "
	_, err = o.Raw(sql).Values(&maps)
	return
}
