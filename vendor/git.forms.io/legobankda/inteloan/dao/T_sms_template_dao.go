package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sms_template struct {
	TmplNo           string `orm:"column(tmpl_no);pk" description:"模板编号"`
	TmplNm           string `orm:"column(tmpl_nm);size(400);null" description:"模板名称"`
	TmplContent      string `orm:"column(tmpl_content);size(200);null" description:"模板内容"`
	TmplComnt        string `orm:"column(tmpl_comnt);size(200);null" description:"模板说明"`
	TmplTypCd        string `orm:"column(tmpl_typ_cd);size(1);null" description:"模板类型"`
	PmitSndFlg       string `orm:"column(pmit_snd_flg);size(1);null" description:"发送标志"`
	EfftDt           string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	AdvSndDays       int    `orm:"column(adv_snd_days);null" description:"提前发送天数"`
	SndBgnTm         string `orm:"column(snd_bgn_tm);type(date);null" description:"发送开始时间"`
	SndEndTm         string `orm:"column(snd_end_tm);type(date);null" description:"发送结束时间"`
	RiskClsfValidFlg string `orm:"column(risk_clsf_valid_flg);size(1);null" description:"记录状态"`
	FinlModfyDt      string `orm:"column(finl_modfy_dt);type(date);null" description:"最后更新日期"`
	FinlModfyTm      string `orm:"column(finl_modfy_tm);type(time);null" description:"最后更新时间"`
	FinlModfyOrgNo   string `orm:"column(finl_modfy_org_no);size(20);null" description:"最后更新机构"`
	FinlModfyTelrNo  string `orm:"column(finl_modfy_telr_no);size(30);null" description:"最后更新柜员"`
	TccState         int    `orm:"column(tcc_state);null"`
}

func (t *T_sms_template) TableName() string {
	return "t_sms_template"
}

func InsertT_sms_template(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sms_template).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sms_templateTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sms_template)).Filter("tx_reqe_cd", maps["TmplNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sms_templateTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_sms_template where tmpl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplNo"]).Exec()
	return err
}

func QueryT_sms_template(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sms_template panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	qs := o.QueryTable(new(T_sms_template))
	if flag != nil && flag.(string) == "9" {
		qs.Exclude("risk_clsf_valid_flg", 9).Filter("tcc_state", state).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sms_templateById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sms_template)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sms_template(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sms_template panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sms_template)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
