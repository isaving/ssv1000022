package dao

import (
	"errors"
	"fmt"
	constant "git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_object_manage struct {
	ObjectType      string `orm:"column(object_type);pk;size(50)"`
	TargetObject    string `orm:"column(target_object);size(50)" description:"对象方向（01发送对象，02接收对象）"`
	ObjectKeyType   string `orm:"column(object_key_type);size(50);null" description:"对象健字类型（01客户编号，02账号，03证件)"`
	ObjectKey       string `orm:"column(object_key);size(50)" description:"对象健字 （客户编号，账号，证件类型+证件号码）"`
	SerialNo        int    `orm:"column(serial_no)" description:"序号"`
	ObjectContent   string `orm:"column(object_content);size(50);null" description:"对象内容(手机号，邮件地址）"`
	ChannelType     string `orm:"column(channel_type);size(50);null" description:"通道类型（运营商）"`
	ContactName     string `orm:"column(contact_name);size(50);null" description:"联系人名称"`
	TopicId         string `orm:"column(topic_id);size(40);null" description:"事件Id"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState        int    `orm:"column(tcc_state);null" description:"tcc状态 0-Normal，1-Insert"`
}

func (t *T_object_manage) TableName() string {
	return "t_object_manage"
}

func InsertT_object_manage(o orm.Ormer, maps orm.Params) error {
	if maps["SerialNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(serial_no)+1 SerialNo from t_object_manage where object_type=? and target_object=? and object_key_type=? and object_key=?"
		o.Raw(sql, maps["ObjectType"], maps["TargetObject"], maps["ObjectKeyType"], maps["ObjectKey"]).Values(&mapOne)
		Seq := mapOne[0]["SerialNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SerialNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_object_manage).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_object_manageTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_object_manage)).Filter("object_type", maps["ObjectType"]).
		Filter("target_object", maps["TargetObject"]).
		Filter("object_key_type", maps["ObjectKeyType"]).
		Filter("object_key", maps["ObjectKey"]).
		Filter("serial_no", maps["SerialNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_object_manageTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_object_manage where object_type=? and target_object=? and object_key_type=? and object_key=? and serial_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ObjectType"], maps["TargetObject"], maps["ObjectKey"], maps["ObjectKeyType"], maps["SerialNo"]).Exec()
	return err
}

func QueryT_object_manage(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_object_manage panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_object_manage))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "SerialNo" && v.(float64) == 0 {
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_object_manageById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_object_manage)).Filter("object_type", maps["ObjectType"]).Filter("target_object", maps["TargetObject"]).
		Filter("object_key_type", maps["ObjectKeyType"]).
		Filter("object_key", maps["ObjectKey"]).
		Filter("serial_no", maps["SerialNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func QueryT_object_manageByContent(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	qs := o.QueryTable(new(T_object_manage)).Filter("object_type", maps["ObjectType"]).Filter("target_object", maps["TargetObject"]).
		Filter("object_key_type", maps["ObjectKeyType"]).
		Filter("object_key", maps["ObjectKey"]).Filter("object_content", maps["OldObjectContent"]).
		Filter("tcc_state", state)
	if maps["SerialNo"].(float64) > 0 {
		qs = qs.Filter("serial_no", maps["SerialNo"])
	}
	_, err = qs.Values(&mapList)
	if err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_object_manage(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_object_manage panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_object_manage)).Filter("object_type", maps["ObjectType"]).Filter("target_object", maps["TargetObject"]).
		Filter("object_key_type", maps["ObjectKeyType"]).
		Filter("object_key", maps["ObjectKey"]).
		Filter("serial_no", maps["SerialNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}

func UpdateT_object_manageByContent(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_object_manage panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	sql := "UPDATE t_object_manage SET object_content=? WHERE object_type=? AND target_object=? AND object_key_type=? AND object_key=? AND object_content=? AND tcc_state = 0"
	_, err = o.Raw(sql, maps["ObjectContent"], maps["ObjectType"], maps["TargetObject"], maps["ObjectKeyType"], maps["ObjectKey"], maps["OldObjectContent"]).Exec()
	return err
}
