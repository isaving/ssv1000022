package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_collection_task_feedback_temp struct {
	TxSn               string `orm:"column(tx_sn);size(52);null" description:"流水号"`
	ExecDate           string `orm:"column(exec_date);size(8);null" description:"执行贷中模型日期"`
	CustNm             string `orm:"column(cust_nm);size(50);null" description:"客户名"`
	CustNo             string `orm:"column(cust_no);size(50);pk" description:"客户号"`
	CrtfNo             string `orm:"column(crtf_no);size(50)" description:"证件号"`
	IndvCrtfTypCd      string `orm:"column(indv_crtf_typ_cd);size(50)" description:"证件类型"`
	OrgNo              string `orm:"column(org_no);size(50);null" description:"机构代码"`
	DubilNo            string `orm:"column(dubil_no);size(50)" description:"借据号"`
	CurrLoanRiskClsfCd string `orm:"column(curr_loan_risk_clsf_cd);size(50);null" description:"当前风险等级"`
	DlqcyProb          float64 `orm:"column(dlqcy_prob);digits(50);decimals(6);null" description:"违约概率"`
	RiskRsn            string `orm:"column(risk_rsn);size(50);null" description:"风险原因风险原因最多展示4条，多条风险原因使用"|"分隔"`
	SggestMgmtManrCd   string `orm:"column(sggest_mgmt_manr_cd);size(50);null" description:"建议管理方式1无动作/2短信提醒/3电核/4额度调整/5冻结"`
	AdjustCrdtQta      float64 `orm:"column(adjust_crdt_qta);digits(50);decimals(6);null" description:"调整额度当建议管理方式为额度调整时，该字段为调整的额度。"`
	SggestExecWindow   string `orm:"column(sggest_exec_window);size(50);null" description:"建议执行窗口建议的最长执行处理天数，如10天，表示在10天内完成建议管理方式的执行。"`
	TaskStartDt        string `orm:"column(task_start_dt);size(10);null" description:"任务开始执行日期"`
	TaskEndDt          string `orm:"column(task_end_dt);size(10);null" description:"任务完成执行日期"`
	CustNo1            string `orm:"column(cust_no1);size(50);null" description:"客户号"`
	CustName1          string `orm:"column(cust_name1);size(50);null" description:"客户名"`
	IndvcrtfNo1        string `orm:"column(indv_crtf_no1);size(50);null" description:"证件号"`
	IndvCrtfTypCd1     string `orm:"column(indv_crtf_typ_cd1);size(50);null" description:"证件类型"`
	LoanDubilNo1       string `orm:"column(loan_dubil_no1);size(50);null" description:"借据号"`
	AlrdyExecFlg       string `orm:"column(alrdy_exec_flg);size(50);null" description:"是否已执行"`
	AtExecWindowFlg    string `orm:"column(at_exec_window_flg);size(50);null" description:"是否在执行窗口"`
	ExecTms            int    `orm:"column(exec_tms);null" description:"执行次数"`
	ReachTms           int    `orm:"column(reach_tms);null" description:"触达次数"`
	TccState           int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_collection_task_feedback_temp) TableName() string {
	return "t_collection_task_feedback_temp"
}

func InsertT_collection_task_feedback_temp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_collection_task_feedback_temp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_collection_task_feedback_tempTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_collection_task_feedback_temp)).Filter("cust_no", maps["CustNo"]).
		Filter("crtf_no", maps["CrtfNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_collection_task_feedback_tempTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_collection_task_feedback_temp where cust_no=? and crtf_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustNo"], maps["CrtfNo"]).Exec()
	return err
}

func QueryT_collection_task_feedback_temp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_collection_task_feedback_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_collection_task_feedback_temp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_collection_task_feedback_tempById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_collection_task_feedback_temp)).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_collection_task_feedback_temp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_collection_task_feedback_temp panic " +fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_collection_task_feedback_temp)).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
