package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sys_biz_date_plan struct {
	PlanNo          string    `orm:"column(plan_no);pk" description:"计划编号"`
	BizDt           string    `orm:"column(biz_dt);type(date);null" description:"业务日期"`
	NxtonePlanNo    string    `orm:"column(nxtone_plan_no);size(40);null" description:"下一计划编号"`
	AvalFlg         string    `orm:"column(aval_flg);size(1);null" description:"可用标志"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号  记录事件的最后修改人从属机构号,"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号  记录最后修改事件的柜员编号,柜员一般包括有交易柜员,复核柜员,授权柜员等不同角色,"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_sys_biz_date_plan) TableName() string {
	return "t_sys_biz_date_plan"
}

func InsertT_sys_biz_date_plan(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sys_biz_date_plan).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sys_biz_date_planTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sys_biz_date_plan)).Filter("plan_no", maps["PlanNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sys_biz_date_planTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_sys_biz_date_plan where plan_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["PlanNo"]).Exec()
	return err
}

func QueryT_sys_biz_date_plan(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sys_biz_date_plan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_sys_biz_date_plan))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sys_biz_date_planById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sys_biz_date_plan)).Filter("plan_no", maps["PlanNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sys_biz_date_plan(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sys_biz_date_plan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sys_biz_date_plan)).Filter("plan_no", maps["PlanNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
