package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strings"
)

type T_cust_base_info struct {
	CustId             string `orm:"column(cust_id);pk" description:"客户号"`
	CustName           string `orm:"column(cust_name);size(320)" description:"名称"`
	CustEname          string `orm:"column(cust_ename);size(320);null" description:"英文名称"`
	OtherName          string `orm:"column(other_name);size(320);null" description:"其他名称"`
	LastName           string `orm:"column(last_name);size(40);null" description:"姓氏"`
	Appellation        string `orm:"column(appellation);size(4);null" description:"称呼"`
	CustType           string `orm:"column(cust_type);size(1)" description:"客户类型"`
	CustSubclass       string `orm:"column(cust_subclass);size(10)" description:"客户子类"`
	OrgType            string `orm:"column(org_type);size(2);null" description:"组织机构类型"`
	Industry           string `orm:"column(industry);size(10);null" description:"行业"`
	Gender             string `orm:"column(gender);size(10);null" description:"性别"`
	MaritalStatus      string `orm:"column(marital_status);size(10);null" description:"婚姻状况"`
	MobileAreaCode     string `orm:"column(mobile_area_code);size(20);null" description:"手机地区号"`
	Email              string `orm:"column(email);size(120);null" description:"电子邮件地址"`
	AcctManager        string `orm:"column(acct_manager);size(10);null" description:"客户经理"`
	Country            string `orm:"column(country);size(2);null" description:"国别"`
	ResidenceCountry   string `orm:"column(residence_country);size(2);null" description:"居住地"`
	Nationality        string `orm:"column(nationality);size(2);null" description:"民族"`
	IsAmerican         string `orm:"column(is_american);size(1);null" description:"是否出生美国"`
	CustStatus         string `orm:"column(cust_status);size(1)" description:"客户状态"`
	IsDataComplete     string `orm:"column(is_data_complete);size(1);null" description:"数据完整性标识"`
	Birthday           string `orm:"column(birthday);size(8);null" description:"生日"`
	Lang               string `orm:"column(lang);size(2);null" description:"语言"`
	IsLocal            string `orm:"column(is_local);size(1);null" description:"境内外标识"`
	MarketTarget       string `orm:"column(market_target);size(10);null" description:"营销目标定位"`
	IsOwnEmployees     string `orm:"column(is_own_employees);size(1);null" description:"是否本行员工"`
	OwnEmployeesNumber string `orm:"column(own_employees_number);size(20);null" description:"本行员工编号"`
	EmployeesCode      string `orm:"column(employees_code);size(20);null" description:"员工代码"`
	SocialInsurance    string `orm:"column(social_insurance);size(40);null" description:"社保卡号"`
	ReferenceNumber    string `orm:"column(reference_number);size(20);null" description:"参考资料编号"`
	TaxRateCountry     string `orm:"column(tax_rate_country);size(2);null" description:"税率国别"`
	MediumCode         string `orm:"column(medium_code);size(20);null" description:"中征码"`
	IsFreeTradeZone    string `orm:"column(is_free_trade_zone);size(1);null" description:"是否自贸区客户"`
	OwnOrgId           string `orm:"column(own_org_id);size(20)" description:"所属机构"`
	CustCreatDate      string `orm:"column(cust_creat_date);type(date);null" description:"客户建立日期"`
	AuthInfo           string `orm:"column(auth_info);size(80);null" description:"授权信息"`
	RecordSerialNo     int    `orm:"column(record_serial_no);null" description:"记录序号"`
	TranOrgId          string `orm:"column(tran_org_id);size(20);null" description:"交易机构"`
	Operator           string `orm:"column(operator);size(10);null" description:"经办用户"`
	AuthUser           string `orm:"column(auth_user);size(10);null" description:"授权用户"`
	LastMaintDate      string `orm:"column(last_maint_date);type(date);null" description:"最后修改日期"`
	LastMaintTime      string `orm:"column(last_maint_time);type(time);null" description:"最后修改时间"`
	LastMaintBrno      string `orm:"column(last_maint_brno);size(4);null" description:"最后修改机构号"`
	LastMaintTell      string `orm:"column(last_maint_tell);size(6);null" description:"最后修改柜员号"`
	TccState           int    `orm:"column(tcc_state);null"`
}

func (t *T_cust_base_info) TableName() string {
	return "t_cust_base_info"
}

func InsertT_cust_base_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cust_base_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cust_base_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cust_base_info)).Filter("cust_id", maps["CustId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cust_base_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cust_base_info where cust_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustId"]).Exec()
	return err
}

func QueryT_cust_base_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cust_base_info panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	Flag := params["Flag"]
	if Flag != nil && Flag == "1" {
		maps, err = QueryCustId(params, 0)
		return
	}
	if Flag != nil && Flag == "9" {
		maps, err = QueryT_cust_base_infoByFlag9(params, 0)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_cust_base_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cust_base_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cust_base_info)).Filter("cust_id", maps["CustId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cust_base_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cust_base_info panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cust_base_info)).Filter("cust_id", maps["CustId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryCustId(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT a.cust_id CustNo,a.cust_name CustNm,b.id_type IndvCrtfTypCd,b.id_no IdNo,a.cust_creat_date CrtDt,a.cust_status CustStatus   FROM t_cust_base_info a ,t_cust_identity b WHERE a.cust_id = b.cust_id AND a.`acct_manager` IS NULL "

	if params["CustNo"] != "" && params["CustNo"] != nil {
		sql = sql + " and a.cust_id = \"" + params["CustNo"].(string) + "\""
	}
	if params["CustNm"] != "" && params["CustNm"] != nil {
		sql = sql + " and a.cust_name = \"" + params["CustNm"].(string) + "\""
	}
	if params["IndvCrtfTypCd"] != "" && params["IndvCrtfTypCd"] != nil {
		sql = sql + " and b.id_type = \"" + params["IndvCrtfTypCd"].(string) + "\""
	}
	if params["IdNo"] != "" && params["IdNo"] != nil {
		sql = sql + " and b.id_no = \"" + params["IdNo"].(string) + "\""
	}
	// " AND b.cust_id NOT IN(SELECT cust_no FROM t_loan_manager_customer ) "
	if params["ListCustNo"] != nil && params["ListCustNo"] != ""{
		ListCustNo := params["ListCustNo"].([]interface{})
		var build strings.Builder
		for i:=0;i<len(ListCustNo);i++{
			build.WriteString("\""+ListCustNo[i].(string)+"\"")
		}
		sql = sql +" AND b.cust_id NOT IN("+build.String()+")"
	}
		_, err = o.Raw(sql).Values(&maps)
	return
}

func QueryT_cust_base_infoByFlag9(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT t1.cust_id CustId,t1.cust_name CustName,t1.`own_org_id` OwnOrgId,t3.`name` AcctManager," +
		"t1.`cust_creat_date` CustCreatDate,t1.`cust_status` CustStatus,t2.`id_type` IdType,t2.`id_no` IdNo " +
		" FROM  (t_cust_base_info t1, t_cust_identity t2) left join t_common_user t3 " +
		" ON t1.`acct_manager`=t3.`user_id`" +
		"WHERE t1.`cust_id`=t2.`cust_id` AND t1.tcc_state=0 AND t2.tcc_state=0  "
	if params["CustId"] != "" && params["CustId"] != nil {
		sql = sql + "and t1.cust_id = \"" + params["CustId"].(string) + "\""
	}
	if params["CustName"] != "" && params["CustName"] != nil {
		sql = sql + "and t1.cust_name = \"" + params["CustName"].(string) + "\""
	}
	if params["IdType"] != "" && params["IdType"] != nil {
		sql = sql + "and t2.id_type = \"" + params["IdType"].(string) + "\""
	}
	if params["IdNo"] != "" && params["IdNo"] != nil {
		sql = sql + "and t2.id_no = \"" + params["IdNo"].(string) + "\""
	}
	if params["OwnOrgId"] != "" && params["OwnOrgId"] != nil {
		sql = sql + "and t1.own_org_id = \"" + params["OwnOrgId"].(string) + "\""
	}
	if params["AcctManager"] != "" && params["AcctManager"] != nil {
		sql = sql + "and t1.acct_manager = \"" + params["AcctManager"].(string) + "\""
	}
	_, err = o.Raw(sql).Values(&maps)
	return
}
