package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_elec_cont_tmpl struct {
	TmplNo            string `orm:"column(tmpl_no);pk" description:"模板编号"`
	TmplPdfFileId     string `orm:"column(tmpl_pdf_file_id);size(40);null" description:"模板pdf文件id"`
	CtrtModeCd        string `orm:"column(ctrt_mode_cd);size(2);null" description:"合同模式代码  1-单方,2-双方,3-三方"`
	SigtPositionTypCd string `orm:"column(sigt_position_typ_cd);size(1);null" description:"签章位置类型代码  1-关键字方式,2-坐标方式"`
	CtrtTmplTypNm     string `orm:"column(ctrt_tmpl_typ_nm);type(date);null" description:"合同模板类型名称"`
	EfftDt            string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	TmplNm            string `orm:"column(tmpl_nm);size(400);null" description:"模板名称"`
	DocTmplTypCd      string `orm:"column(doc_tmpl_typ_cd);size(50);null" description:"文档模板类型代码  loan_contract:借款合同,guart_contract:担保合同,other_temp_typ:其他模板"`
	TmplSubTypCd      string `orm:"column(tmpl_sub_typ_cd);size(2);null" description:"模板子类型代码  01-额度借款合同02-个人征信授权书03-个人信息查询及使用授权书04-公积金信息查询协议05-纳税信息授权委托书06-蜀信e贷个人借款合同"`
	TmplFileId        string `orm:"column(tmpl_file_id);size(50);null" description:"模板文件id"`
	CtrtTmplComnt     string `orm:"column(ctrt_tmpl_comnt);size(200);null" description:"合同模板说明"`
	FinlModfyDt       string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState          int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_elec_cont_tmpl) TableName() string {
	return "t_loan_elec_cont_tmpl"
}

func InsertT_loan_elec_cont_tmpl(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_elec_cont_tmpl).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_elec_cont_tmplTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_elec_cont_tmpl)).Filter("tmpl_no", maps["TmplNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_elec_cont_tmplTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_elec_cont_tmpl where tmpl_no=?and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplNo"]).Exec()
	return err
}

func QueryT_loan_elec_cont_tmpl(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_elec_cont_tmpl panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_loan_elec_cont_tmpl))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "StartDate" {
				qs = qs.Filter("efft_dt__gte", v)
			} else if k == "EndDate" {
				qs = qs.Filter("efft_dt__lte", v)
			} else if k == "TmplNm"{
				qs = qs.Filter("tmpl_nm__icontains",v)
			} else{
				qs = qs.Filter(k, v)
			}
		}
	}
	if flag == "9"{
		if _, err = qs.Filter("tcc_state", state).Limit(1).OrderBy("-efft_dt").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("tmpl_no").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_elec_cont_tmplById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_elec_cont_tmpl)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_elec_cont_tmpl(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_elec_cont_tmpl panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_elec_cont_tmpl)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
