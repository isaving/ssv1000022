package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_early_warn_decision_info struct {
	WarnTaskNo            string    `orm:"column(warn_task_no);pk" description:"预警任务编号"`
	LoanDubilNo           string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	CustNo                string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName              string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd         string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo            string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	RiskRsn               string    `orm:"column(risk_rsn);size(200);null" description:"风险原因"`
	SggestWarnSignalTypCd string    `orm:"column(sggest_warn_signal_typ_cd);size(2);null" description:"建议预警信号类型代码  11-短信提醒,,12-电核,,13-额度调整,,14-额度冻结"`
	SggestExecWindowDays  int       `orm:"column(sggest_exec_window_days);null" description:"建议执行窗口天数"`
	AdjAftCrdtQta         float64   `orm:"column(adj_aft_crdt_qta);null;digits(20);decimals(4)" description:"调整后授信额度"`
	AdjDrctCd             string    `orm:"column(adj_drct_cd);size(1);null" description:"调整方向代码  U-向上,D-下调"`
	InlnModelExecDt       string    `orm:"column(inln_model_exec_dt);type(date);null" description:"贷中模型执行日期"`
	DlqcyProb             float64   `orm:"column(dlqcy_prob);null;digits(9);decimals(6)" description:"违约概率  "`
	CurrRiskGradeCd       string    `orm:"column(curr_risk_grade_cd);size(2);null" description:"当前风险等级代码  1-20级,数字字符1-20级,数字字符"`
	FinlModfyDt           string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  10"`
	FinlModfyTm           string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  19"`
	FinlModfyOrgNo        string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo       string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                  int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_early_warn_decision_info) TableName() string {
	return "t_early_warn_decision_info"
}

func InsertT_early_warn_decision_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_early_warn_decision_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_early_warn_decision_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_early_warn_decision_info)).Filter("warn_task_no", maps["WarnTaskNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_early_warn_decision_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_early_warn_decision_info where warn_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["WarnTaskNo"]).Exec()
	return err
}

func QueryT_early_warn_decision_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_early_warn_decision_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_early_warn_decision_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_early_warn_decision_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_early_warn_decision_info)).Filter("warn_task_no", maps["WarnTaskNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_early_warn_decision_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_early_warn_decision_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_early_warn_decision_info)).Filter("warn_task_no", maps["WarnTaskNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
