package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_product struct {
	LoanProdtNo                  string  `orm:"column(loan_prodt_no);size(20);pk" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo              string  `orm:"column(loan_prodt_vers_no);size(5)" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	LoanProdtNm                  string  `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	DrawDownDate                 string  `orm:"column(draw_down_date);type(date);null" `
	StartDt                      string  `orm:"column(start_dt);type(date);null" description:"起始日期"`
	MatrDt                       string  `orm:"column(matr_dt);type(date);null" description:"到期日期"`
	LoanProdtClsfCd              string  `orm:"column(loan_prodt_clsf_cd);size(5);null" description:"贷款产品分类代码  F0211-个人住房贷款,F0219-其他消费贷款"`
	KeprcdStusCd                 string  `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效09-无效"`
	QuotaType                    string  `orm:"column(quota_type);size(1);null" description:"额度控制标志"`
	QtaCtrlFlg                   string  `orm:"column(qta_ctrl_flg);size(1);null" description:"额度控制标志"`
	LoanTypCd                    string  `orm:"column(loan_typ_cd);size(3);null" description:"贷款类型代码"`
	LoanGuarManrCd               string  `orm:"column(loan_guar_manr_cd);size(2);null" description:"贷款担保方式代码"`
	CurCd                        string  `orm:"column(cur_cd);size(4);null" description:"币种代码"`
	AplyLoanDeadl                int     `orm:"column(aply_loan_deadl);null" description:"申请贷款期限"`
	AplyLoanAmtCeilVal           float64 `orm:"column(aply_loan_amt_ceil_val);null;digits(18);decimals(2)" description:"贷款申请金额上限"`
	BegintDayDtrmnManrCd         string  `orm:"column(begint_day_dtrmn_manr_cd);size(1);null" description:"起息日确定方式代码"`
	ExpdayDtrmnManrCd            string  `orm:"column(expday_dtrmn_manr_cd);size(1);null" description:"到期日确定方式代码"`
	RepayManrCd                  string  `orm:"column(repay_manr_cd);size(3);null" description:"还款方式代码  参考标准代码:CD0062标准基础上新增:51-按月付息按季还本52-按月付息按半年还本53-按月付息按年还本"`
	IntrtNo                      string  `orm:"column(intrt_no);size(40);null" description:"利率编号"`
	ProdtIntrtCd                 string  `orm:"column(prodt_intrt_cd);size(1);null" description:"利率代码"`
	TranOvdueOperManrCd          string  `orm:"column(tran_ovdue_oper_manr_cd);size(1);null" description:"转逾期操作方式代码"`
	IntrtAdjManrCd               string  `orm:"column(intrt_adj_manr_cd);size(3);null" description:"利率调整方式代码"`
	PmitAdvRepayFlg              string  `orm:"column(pmit_adv_repay_flg);size(1);null" description:"允许提前还款标志  描述是否允许提前还款"`
	SglacctHighCrdtQta           float64 `orm:"column(sglacct_high_crdt_qta);null;digits(18);decimals(2)" description:"单户最高授信额度"`
	NormlLoanChrgSeqCtrlBnch     string  `orm:"column(norml_loan_chrg_seq_ctrl_bnch);size(5);null" description:"正常贷款扣款顺序控制串  不是枚举值类型,是控制串,将如下几种代码:1-本金,2-利息,3-本金罚息'罚息',4-利息罚息'复息',5-本金罚息复利'复利',6-利息罚息复利'复利'逾期90天前,扣款顺序是,54321逾期90天后,扣款顺序是12345"`
	DvalLoanChrgSeqCtrlBnch      string  `orm:"column(dval_loan_chrg_seq_ctrl_bnch);size(5);null" description:"减值贷款扣款顺序控制串  不是枚举值类型,是控制串,将如下几种代码:1-本金,2-利息,3-本金罚息'罚息',4-利息罚息'复息',5-本金罚息复利'复利',6-利息罚息复利'复利'逾期90天前,扣款顺序是,54321逾期90天后,扣款顺序是12345"`
	RevneCmpdCd                  string  `orm:"column(revne_cmpd_cd);size(1);null" description:"计收复利代码  0-不收复利,,1-收取复利,2-由全局参数控制"`
	BaseIntrtTypCd               string  `orm:"column(base_intrt_typ_cd);size(10);null" description:"基础利率类型"`
	OvdueIntrtFlotRatio          float64 `orm:"column(ovdue_intrt_flot_ratio);null;digits(9);decimals(6)" description:"逾期利率浮动比例"`
	OvdueIntrtFlotManrCd         string  `orm:"column(ovdue_intrt_flot_manr_cd);size(1);null" description:"逾期利率浮动方式代码  P-浮动百分比,N-浮动点数"`
	OvdueIntrtFlotDrctCd         string  `orm:"column(ovdue_intrt_flot_drct_cd);size(1);null" description:"逾期利率浮动方向代码  D-下调,U-上调"`
	OvdueIntrtBpFlotVal          float64 `orm:"column(ovdue_intrt_bp_flot_val);null" description:"逾期利率bp浮动值"`
	EmbIntrtFlotManrCd           string  `orm:"column(emb_intrt_flot_manr_cd);size(1);null" description:"挪用利率浮动方式代码  P-浮动百分比,N-浮动点数"`
	EmbIntrtFlotDrctCd           string  `orm:"column(emb_intrt_flot_drct_cd);size(1);null" description:"挪用利率浮动方向代码  D-下调,U-上调"`
	EmbIntrtBpFlotVal            float64 `orm:"column(emb_intrt_bp_flot_val);null" description:"挪用利率bp浮动值"`
	EmbLoanPnltintIntrtFlotRatio float64 `orm:"column(emb_loan_pnltint_intrt_flot_ratio);null;digits(9);decimals(6)" description:"挪用贷款罚息利率浮动比例"`
	MinInterestRate              float64 `orm:"column(min_interest_rate);null"`
	MaxInterestRate              float64 `orm:"column(max_interest_rate);null;digits(9);decimals(6)"`
	FinlModfyDt                  string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm                  string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo               string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModifrEmpnbr             string  `orm:"column(finl_modifr_empnbr);size(6);null" description:"最后修改人员工号"`
	TccState                     int     `orm:"column(tcc_state);null"`
}

func (t *T_loan_product) TableName() string {
	return "t_loan_product"
}

func InsertT_loan_product(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_product).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_productTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_product)).Filter("loan_prodt_no", maps["LoanProdtNo"]).
		Filter("loan_prodt_vers_no", maps["LoanProdtVersNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_productTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_product where loan_prodt_no=? and loan_prodt_vers_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanProdtNo"], maps["LoanProdtVersNo"]).Exec()
	return err
}

func QueryT_loan_product(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_product panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_product))
	if params["Flag"] != nil && params["Flag"] == "9" { //得到产品的版本号
		sql := "SELECT MAX(loan_prodt_vers_no)+1 LoanProdtVersNo FROM t_loan_product WHERE loan_prodt_no =?"
		_, err = o.Raw(sql, params["LoanProdtNo"]).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "StartDate" {
				qs = qs.Filter("start_dt__gte", v)
			} else if k == "StartDate1" {
				qs = qs.Filter("start_dt__lte", v)
			} else if k == "EndDate" {
				qs = qs.Filter("matr_dt__lte", v)
			} else if k == "LoanProdtNm" {
				qs = qs.Filter("loan_prodt_nm__contains", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("loan_prodt_no", "loan_prodt_vers_no").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_productById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_product)).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("loan_prodt_vers_no", maps["LoanProdtVersNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_product(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_product panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_product)).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("loan_prodt_vers_no", maps["LoanProdtVersNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
