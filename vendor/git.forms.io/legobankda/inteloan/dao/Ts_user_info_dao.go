package dao

import (
	"errors"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Ts_user_info struct {
	UserId            string `orm:"column(user_id);pk" description:"用户编号"`
	DeptId            string `orm:"column(dept_id);size(16)" description:"内网用户所属部门"`
	UserName          string `orm:"column(user_name);size(20)" description:"用户名"`
	LoginName         string `orm:"column(login_name);size(20)" description:"登陆别名"`
	Password          string `orm:"column(password);size(32)" description:"密码"`
	UserType          string `orm:"column(user_type);size(1)" description:"用户类型（U:内网用户 C:外网用户）"`
	WechatOpenId      string `orm:"column(wechat_open_id);size(32)" description:"微信OPEN ID"`
	CellPhone         string `orm:"column(cell_phone);size(11)" description:"移动电话"`
	EmailAddress      string `orm:"column(email_address);size(50)" description:"邮箱地址"`
	EmailValidFlag    string `orm:"column(email_valid_flag);size(1)" description:"邮箱验证标记（Y：已验证，N：未验证）"`
	LoginFlag         int8   `orm:"column(login_flag)" description:"登录标志（1.已登录，0未登录）"`
	WrongTimes        int8   `orm:"column(wrong_times)" description:"输入密码错误次数"`
	LockFlag          int8   `orm:"column(lock_flag)" description:"锁定标志（1 锁定，0未锁定）"`
	LastLoginIp       string `orm:"column(last_login_ip);size(50)" description:"最后登录ip"`
	LastLoginDatetime string `orm:"column(last_login_datetime);size(19)" description:"最后登录日期时间"`
	LoginTimes        int    `orm:"column(login_times)" description:"总登录次数"`
	ValidFlag         string `orm:"column(valid_flag);size(1)" description:"账号是否可用标记（Y：可用，N：禁用）"`
	TlrEffectDate     string `orm:"column(tlr_effect_date);size(10)" description:"员工启用日期（10位）"`
	AccountEffectDate string `orm:"column(account_effect_date);size(10)" description:"账户有效日期"`
	UserStatus        string `orm:"column(user_status);size(2)" description:"员工状态（0新增，1可用，2锁定，3停用，4调动,5已离职，6.解锁准备，7停用准备，8调出准备，9，调入准备，10离职准备）"`
	CreateTell        string `orm:"column(create_tell);size(20)" description:"创建柜员"`
	CreateDatetime    string `orm:"column(create_datetime);size(19)" description:"创建日期时间"`
	LastMaintDate     string `orm:"column(last_maint_date);size(10);null" description:"最后更新日期"`
	LastMaintTime     string `orm:"column(last_maint_time);size(19);null" description:"最后更新时间"`
	LastMaintBrno     string `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	LastMaintTell     string `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	TccState          int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *Ts_user_info) TableName() string {
	return "ts_user_info"
}

func InsertTs_user_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Ts_user_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateTs_user_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Ts_user_info)).Filter("user_id", maps["UserId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteTs_user_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from ts_user_info where user_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["UserId"]).Exec()
	return err
}

func QueryTs_user_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryTs_user_info panic")
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Ts_user_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryTs_user_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Ts_user_info)).Filter("user_id", maps["UserId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateTs_user_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateTs_user_info panic")
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Ts_user_info)).Filter("user_id", maps["UserId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
