package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_proc_model_para struct {
	ProcesModelId     string    `orm:"column(proces_model_id);size(50);pk" description:"流程模型id"`
	ProcesModelVersNo int       `orm:"column(proces_model_vers_no)" description:"流程模型版本编号"`
	TaskTmplTypCd     string    `orm:"column(task_tmpl_typ_cd);size(2);null" description:"任务模板类型代码  0-userTask,1-serviceTask"`
	AvalOperCd        string    `orm:"column(aval_oper_cd);size(3);null" description:"可用操作编码"`
	TaskTmplNo        string    `orm:"column(task_tmpl_no);size(60)" description:"任务模板编号"`
	TaskNm            string    `orm:"column(task_nm);size(120);null" description:"任务名称"`
	TaskConfParaInfo  string    `orm:"column(task_conf_para_info);size(200);null" description:"任务配置参数信息"`
	CrtTelrNo         string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtDt             string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtTm             string    `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int        `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_proc_model_para) TableName() string {
	return "t_proc_model_para"
}

func InsertT_proc_model_para(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_proc_model_para).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_proc_model_paraTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_proc_model_para)).Filter("proces_model_id", maps["ProcesModelId"]).
		Filter("proces_model_vers_no", maps["ProcesModelVersNo"]).Filter("task_tmpl_no", maps["TaskTmplNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_proc_model_paraTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_proc_model_para where proces_model_id=? and proces_model_vers_no=? and task_tmpl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ProcesModelId"], maps["ProcesModelVersNo"], maps["TaskTmplNo"]).Exec()
	return err
}

func QueryT_proc_model_para(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_proc_model_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_proc_model_para))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_proc_model_paraById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_proc_model_para)).Filter("proces_model_id", maps["ProcesModelId"]).
		Filter("proces_model_vers_no", maps["ProcesModelVersNo"]).Filter("task_tmpl_no", maps["TaskTmplNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_proc_model_para(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_proc_model_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_proc_model_para)).Filter("proces_model_id", maps["ProcesModelId"]).
		Filter("proces_model_vers_no", maps["ProcesModelVersNo"]).Filter("task_tmpl_no", maps["TaskTmplNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
