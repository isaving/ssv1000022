package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_busi_org_mapping_para struct {
	KeprcdNo        string    `orm:"column(keprcd_no);pk" description:"记录编号"`
	SceneTypCd      string    `orm:"column(scene_typ_cd);size(1);null" description:"场景类型代码  1-普通客户2-员工相关"`
	OrginlOrgNo     string    `orm:"column(orginl_org_no);size(4);null" description:"原机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	OprOrgNo        string    `orm:"column(opr_org_no);size(4);null" description:"经办机构号"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	KeprcdStusCd    string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效09-无效"`
	CrtTelrNo       string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtDt           string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtOrgNo        string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_busi_org_mapping_para) TableName() string {
	return "t_busi_org_mapping_para"
}

func InsertT_busi_org_mapping_para(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_busi_org_mapping_para).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_busi_org_mapping_paraTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_busi_org_mapping_para)).Filter("keprcd_no", maps["KeprcdNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_busi_org_mapping_paraTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_busi_org_mapping_para where keprcd_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_busi_org_mapping_para(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_busi_org_mapping_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_busi_org_mapping_para))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_busi_org_mapping_paraById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_busi_org_mapping_para)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_busi_org_mapping_para(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_busi_org_mapping_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_busi_org_mapping_para)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
