package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_event struct {
	BizEventCd      string `orm:"column(biz_event_cd);pk" description:"业务事件编码  T0XXXX-金融交易T8XXXX-产品调整交易T9XXXX-机构调整交易PK,"`
	BizEventNm      string `orm:"column(biz_event_nm);size(120);null" description:"业务事件名称"`
	BizEventTypCd   string `orm:"column(biz_event_typ_cd);size(1);null" description:"业务事件类型代码  0-普通服务,1-产品归并服务,2-机构变更服务"`
	BizEventTypNm   string `orm:"column(biz_event_typ_nm);size(120);null" description:"业务事件类型名称  界面字段,对应业务事件类型代码,普通服务,机构变更服务"`
	EfftDt          string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	ValidFlg        string `orm:"column(valid_flg);size(1);null" description:"有效标志"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int    `orm:"column(tcc_state);null"`
}

func (t *T_ae_event) TableName() string {
	return "t_ae_event"
}

func InsertT_ae_event(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_event).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_eventTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_event)).Filter("biz_event_cd", maps["BizEventCd"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_eventTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_event where biz_event_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizEventCd"]).Exec()
	return err
}

func QueryT_ae_event(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_event panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_event))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_eventById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_event)).Filter("biz_event_cd", maps["BizEventCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_event(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_event panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_event)).Filter("biz_event_cd", maps["BizEventCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
