package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_quota_change_apply struct {
	ChgAplyNo           string    `orm:"column(chg_aply_no);pk" description:"变更申请编号"`
	CustNo              string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CrdtAplySn          string    `orm:"column(crdt_aply_sn);size(32);null" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	AplyAdjRsnCd        string    `orm:"column(aply_adj_rsn_cd);size(2);null" description:"申请调整原因代码  1:内部要求2:风控调整3:客户申请4:其他"`
	AplyAdjTypCd        string    `orm:"column(aply_adj_typ_cd);size(3);null" description:"申请调整类型代码  01:冻结02:解冻03:调整04:终止,,,,,05:提额08:批量失效"`
	AdjDrctCd           string    `orm:"column(adj_drct_cd);size(1);null" description:"调整方向代码  D-下调,U-上调"`
	CurCd               string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	ChgBefAvalQta       float64   `orm:"column(chg_bef_aval_qta);null;digits(18);decimals(2)" description:"变更前可用额度"`
	ChgAftQta              float64   `orm:"column(chg_aft_qta);null;digits(18);decimals(2)" description:"变更额度"`
	ChgAftAvalQta       float64   `orm:"column(chg_aft_aval_qta);null;digits(18);decimals(2)" description:"变更后可用额度"`
	ChgRsn              string    `orm:"column(chg_rsn);size(200);null" description:"变更原因"`
	ExmnvrfyStusCd      string    `orm:"column(exmnvrfy_stus_cd);size(2);null" description:"审核状态代码  1-已通过2-审核中3-不需审核9-已拒绝"`
	ExmnvrfySugstnComnt string    `orm:"column(exmnvrfy_sugstn_comnt);size(2000);null" description:"审核意见说明  审核界面的审核意见说明"`
	TxSn                string    `orm:"column(tx_sn);size(32);null" description:"交易流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,随机整数8位编号规则:1',随机数生成:(int)(Math.random()*100000000)"`
	CrtDt               string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtEmpnbr           string    `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo            string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt         string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState            int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_quota_change_apply) TableName() string {
	return "t_loan_quota_change_apply"
}

func InsertT_loan_quota_change_apply(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_quota_change_apply).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_quota_change_applyTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_quota_change_apply)).Filter("chg_aply_no", maps["ChgAplyNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_quota_change_applyTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_quota_change_apply where chg_aply_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ChgAplyNo"]).Exec()
	return err
}

func QueryT_loan_quota_change_apply(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_quota_change_apply panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag :=params["Flag"]
	qs := o.QueryTable(new(T_loan_quota_change_apply))
	if flag== "9" {
		if _, err = qs.Filter("tcc_state", state).Filter("crdt_aply_sn", params["CrdtAplySn"]).Filter("exmnvrfy_stus_cd", params["ExmnvrfyStusCd"]).
			Filter("aply_adj_typ_cd", params["AplyAdjTypCd"]).OrderBy("-crt_dt", "-finl_modfy_dt", "-finl_modfy_tm").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag == "7"{
		maps,err = QueryT_loan_quota_change_applyFlag7(params)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_quota_change_applyById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_quota_change_apply)).Filter("chg_aply_no", maps["ChgAplyNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_quota_change_apply(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_quota_change_apply panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_quota_change_apply)).Filter("chg_aply_no", maps["ChgAplyNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_quota_change_applyFlag7(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  "+
		"t1.chg_aply_no ChgAplyNo, "+
		"t1.crdt_aply_sn CrdtAplySn, "+
		"t1.aply_adj_typ_cd AplyAdjTypCd, "+
		"t1.chg_aft_qta ChgAftQta, "+
		"t1.exmnvrfy_stus_cd ExmnvrfyStusCd, "+
		"t1.finl_modfy_dt FinlModfyDt, "+
		"t1.finl_modfy_tm FinlModfyTm, "+
		"t2.cust_name CustName  "+
		"FROM t_loan_quota_change_apply t1,t_loan_cust_base_info t2 "+
		"WHERE aply_adj_typ_cd='05' AND exmnvrfy_stus_cd='1'  AND t1.crdt_aply_sn=t2.crdt_aply_sn and t1.tcc_state=0 and t2.tcc_state=0 "
	_, err = o.Raw(sql).Values(&maps)
	return
}