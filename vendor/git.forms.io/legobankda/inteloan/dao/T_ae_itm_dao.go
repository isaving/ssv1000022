package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_itm struct {
	SubjNo           string `orm:"column(subj_no);size(8);pk" description:"科目编号  PK,记录与账户相关的科目信息,"`
	AcctBookCategCd  string `orm:"column(acct_book_categ_cd);size(5)" description:"账套类别代码  CN001-川农,CN002-预留1,CN003-预留2,CN004-预留3"`
	SubjEngNm        string `orm:"column(subj_eng_nm);size(120);null" description:"科目英文名称"`
	SubjNm           string `orm:"column(subj_nm);size(60);null" description:"科目名称"`
	SubjCategCd      string `orm:"column(subj_categ_cd);size(3);null" description:"科目类别代码  A-资产类'Assets',L-负债类'Liabilities',M-资产负债共同类'Assets,&,Liabilities,-,Common,Types',C-所有者权益类'Capital,or,Owners'''',Equity',I-收入类'Income',E-支出类'Expenses',O-表外类'Off,Balance,Sheet'(若科目类别为表外类,则:科目记账方式必须为,MEMO,POSTING),]',"`
	KepacctManrCd    string `orm:"column(kepacct_manr_cd);size(20);null" description:"记账方式代码  R-Real,Posting,–,表内记账,记账时需要检查账务的借贷平衡,即按账套,货币,起息日借贷平衡,M-Memo,Posting,–表外记账,支持单边记账,不进行借贷平衡检查,"`
	AtmtcGnrtGlFlg   string `orm:"column(atmtc_gnrt_gl_flg);size(1);null" description:"自动生成总账标志  描述是否自动生成总账,0否1是,Y:表示可自动生成N:表示不可自动生成P:表示该核算单元的科目号已开立一种货币的总帐账号,可自动新开其他货币"`
	DtlSubjFlg       string `orm:"column(dtl_subj_flg);size(1);null" description:"明细科目标志  描述是否明细科目,0否1是,Y-表示该科目为明细科目,可以开立总账并记账,N-表示该科目不是明细科目,不可以开立总账或记账,"`
	SubjBalZeroFlgCd string `orm:"column(subj_bal_zero_flg_cd);size(1);null" description:"科目余额零标志代码  D-科目的余额必须日终为零,M-科目的余额必须月终为零,Y-科目的余额必须年终为零,N-科目的余额不需要检查是否为零,"`
	BalDrctCd        string `orm:"column(bal_drct_cd);size(3);null" description:"余额方向代码  D-余额在借方,C-余额在贷方,A-余额可为借贷双方"`
	SubjStusCd       string `orm:"column(subj_stus_cd);size(3);null" description:"科目状态代码  A-有效'Active'P-待生效'Pending'C-注销'Cancelled'H-冻结'Suspended'S-停止使用'Closed'"`
	SubjOpenAcctDt   string `orm:"column(subj_open_acct_dt);type(date);null" description:"科目开户日期  科目的开立日期"`
	EfftDt           string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	SubjHeavyOpenDt  string `orm:"column(subj_heavy_open_dt);type(date);null" description:"科目重开日期  科目注销后的又重新使用的日期"`
	DeregisDt        string `orm:"column(deregis_dt);type(date);null" description:"注销日期"`
	InvalidDt        string `orm:"column(invalid_dt);type(date);null" description:"失效日期"`
	FinlModfyDt      string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int    `orm:"column(tcc_state);null"`
}

func (t *T_ae_itm) TableName() string {
	return "t_ae_itm"
}

func InsertT_ae_itm(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_itm).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_itmTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_itm)).Filter("subj_no", maps["SubjNo"]).
		Filter("acct_book_categ_cd", maps["AcctBookCategCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_itmTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_itm where subj_no=? and acct_book_categ_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["SubjNo"], maps["AcctBookCategCd"]).Exec()
	return err
}

func QueryT_ae_itm(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_itm panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()

	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_itm))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_itmById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_itm)).Filter("subj_no", maps["SubjNo"]).Filter("acct_book_categ_cd", maps["AcctBookCategCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_itm(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_itm panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_itm)).Filter("subj_no", maps["SubjNo"]).Filter("acct_book_categ_cd", maps["AcctBookCategCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
