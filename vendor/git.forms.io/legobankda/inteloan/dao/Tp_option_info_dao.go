package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Tp_option_info struct {
	OptnId          string `orm:"column(optn_id);pk" description:"选项id"`
	OptnNm          string `orm:"column(optn_nm);size(120);null" description:"选项名称"`
	AvalFlg         string `orm:"column(aval_flg);size(1);null" description:"可用标志"`
	PareClsId       string `orm:"column(pare_cls_id);size(50);null" description:"父类id"`
	CrtTelrNo       string `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtDt           string `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtTm           string `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
	OptionClass     string `orm:"column(option_class);size(4);null" description:"选项分类(IG-积分)"`
}

func (t *Tp_option_info) TableName() string {
	return "tp_option_info"
}

func InsertTp_option_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Tp_option_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateTp_option_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Tp_option_info)).Filter("optn_id", maps["OptnId"]).
		Filter("pare_cls_id", maps["PareClsId"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteTp_option_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from tp_option_info where optn_id=? and pare_cls_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OptnId"], maps["PareClsId"]).Exec()
	return err
}

func QueryTp_option_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryTp_option_info panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Tp_option_info))
	flag := params["Flag"]
	delete(params, "Flag")
	if flag != nil && flag.(string) == "9" { //得到所有的复选项
		sql := "SELECT optn_id OptnId,optn_nm OptnNm FROM tp_option_info WHERE optn_id IN (SELECT pare_cls_id FROM tp_option_info GROUP BY pare_cls_id ) "
		if params["PareClsId"] != nil {
			sql = sql + " AND pare_cls_id = " + params["PareClsId"].(string)
		}
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	if flag != nil && flag.(string) == "8" { //父选项查询 il001003
		sql := "SELECT optn_id PareClsId,optn_nm pareClsNm,aval_flg Aval_flg FROM tp_option_info WHERE pare_cls_id=\"ROOT\" and aval_flg=1 AND optn_id != \"ROOT\" AND tcc_state=0"
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	if flag == "7" { //选项清单查询--il001058 -flag=7
		sql := "SELECT optn_id OptnId,optn_nm OptnNm FROM tp_option_info where pare_cls_id =? and tcc_state=0 and aval_flg=1"
		_, err = o.Raw(sql, params["PareClsId"].(string)).Values(&maps)
		return
	}
	if flag == "6" { //xml--il001004
		sql := "SELECT t1.optn_id OptnId,t1.optn_nm OptnNm,t1.aval_flg AvalFlg,t1.pare_cls_id PareClsId,t2.optn_nm PareClsNm FROM tp_option_info t1 " +
			"LEFT JOIN (SELECT optn_id,optn_nm,aval_flg FROM tp_option_info WHERE pare_cls_id = \"ROOT\" AND optn_id != \"ROOT\" AND tcc_state=0) t2 ON t1.`pare_cls_id`= t2.optn_id " +
			"WHERE t1.`optn_id` != \"ROOT\" and t1.tcc_state=0 "
		if params["OptnId"] != nil {
			sql = sql + " AND t1.optn_id = \"" + params["OptnId"].(string) + "\""
		}
		if params["OptnNm"] != nil {
			sql = sql + " AND t1.optn_nm = \"" + params["OptnNm"].(string) + "\""
		}
		if params["AvalFlg"] != nil {
			sql = sql + " AND t1.aval_flg = \"" + params["AvalFlg"].(string) + "\""
		}
		if params["PareClsId"] != nil {
			sql = sql + " AND t1.pare_cls_id = \"" + params["PareClsId"].(string) + "\""
		}
		sql = sql + " ORDER BY t1.`pare_cls_id`  limit 100000 "
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	contains := params["Contains"]
	if contains == "1" {
		for k, v := range params {
			if k != "Contains" {
				if k == "ParentoptName" {
					qs = qs.Filter("parentopt_name__icontains", v)
					continue
				}
				qs = qs.Filter(k, v)
			}
		}
		if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryTp_option_infoById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	OptnId := params["OptnId"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(Tp_option_info)).Filter("optn_id", OptnId).Filter("pare_cls_id", params["PareClsId"]).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateTp_option_info(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateTp_option_info panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	OptnId := params["OptnId"]
	_, err = o.QueryTable(new(Tp_option_info)).Filter("optn_id", OptnId).Filter("pare_cls_id", params["PareClsId"]).Filter("tcc_state", 0).Update(params)
	return err
}
