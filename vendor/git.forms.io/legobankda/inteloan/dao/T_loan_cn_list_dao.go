package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_cn_list struct {
	AcctiAcctNo          string    `orm:"column(accti_acct_no);size(32);null" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	BizFolnNo            string    `orm:"column(biz_foln_no);size(32);pk" description:"业务跟踪号  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	Pridnum              int       `orm:"column(pridnum)" description:"期数"`
	CurCd                string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	TxAcctnDt            string    `orm:"column(tx_acctn_dt);type(date);null" description:"交易会计日期"`
	MgmtOrgNo            string    `orm:"column(mgmt_org_no);size(4);null" description:"管理机构号"`
	AcctiOrgNo           string    `orm:"column(accti_org_no);size(4);null" description:"核算机构号"`
	ChnlCd               string    `orm:"column(chnl_cd);size(4);null" description:"渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表TRM-柜面,IBS-网银,"`
	BgnDt                string    `orm:"column(bgn_dt);type(date);null" description:"开始日期"`
	EndDt                string    `orm:"column(end_dt);type(date);null" description:"结束日期"`
	Days                 int       `orm:"column(days);null" description:"天数"`
	PrinIntr             float64   `orm:"column(prin_intr);null;digits(18);decimals(2)" description:"本金利息"`
	IntrStusCd           string    `orm:"column(intr_stus_cd);size(2);null" description:"利息状态代码  0-正常,1-逾期"`
	CalcIntrt            float64   `orm:"column(calc_intrt);null;digits(9);decimals(6)" description:"计算利率"`
	IntacrCnDwDlwthTypCd string    `orm:"column(intacr_cn_dw_dlwth_typ_cd);size(1);null" description:"计息计提处理类型代码  0-计息,1-计提,2-结息,3,计罚息,4,计复利"`
	DlwthStusCd          string    `orm:"column(dlwth_stus_cd);size(4);null" description:"处理状态代码  N-正常,C-作废,"`
	IntrPlanNo           string    `orm:"column(intr_plan_no);size(40);null" description:"利息计划编号"`
	CalcModeCd           string    `orm:"column(calc_mode_cd);size(1);null" description:"计算模式代码  1-批次计提,2-联机计提"`
	FinlModfyDt          string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm          string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo       string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_cn_list) TableName() string {
	return "t_loan_cn_list"
}

func InsertT_loan_cn_list(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_cn_list).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_cn_listTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_cn_list)).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("sys_foln_no", maps["SysFolnNo"]).Filter("seq_no", maps["SeqNo"]).Filter("pridnum", maps["Pridnum"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_cn_listTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_cn_list where biz_foln_no=? and sys_foln_no=? and seq_no=? and pridnum=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizFolnNo"], maps["SysFolnNo"], maps["SeqNo"], maps["Pridnum"]).Exec()
	return err
}

func QueryT_loan_cn_list(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_cn_list panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_cn_list))
	flag := params["Flag"]
	delete(params, "Flag")
	if "9" == flag {
		sql := "select sum(prin_intr) PrinIntr from t_loan_cn_list where accti_acct_no=? and pridnum=? and intr_stus_cd ='1' and tcc_state=0"
		_, err =o.Raw(sql,params["AcctiAcctNo"], params["Pridnum"]).Values(&maps)
		return
	}

	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_cn_listById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_cn_list)).Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).
		Filter("seq_no", maps["SeqNo"]).Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_cn_list(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_cn_list panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_cn_list)).Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).
		Filter("seq_no", maps["SeqNo"]).Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", 0).Update(maps)
	return err
}
