package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_credit_approval_flow struct {
	CrdtAplySn       string    `orm:"column(crdt_aply_sn);size(32);pk;null" description:"交易流水号"`
	SeqNo       	 int       `orm:"column(seq_no);size(32);null" description:"序号"`
	CustNo           string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	OperPersonName   string    `orm:"column(oper_person_name);size(60);null" description:"操作人姓名"`
	LnrwModeCd       string    `orm:"column(lnrw_mode_cd);size(2);null" description:"贷审模式代码  1-独立审批人,2-贷审小组,3-贷审会"`
	ApprvManrCd      string    `orm:"column(apprv_manr_cd);size(2);null" description:"审批方式代码  1-同意,2-退回,3-终止,4-弃权,5-拒绝"`
	NodeTypCd        string    `orm:"column(node_typ_cd);size(8);null" description:"节点类型代码  01-人工调查02-人工审查03-人工审批04-智能决策审批"`
	OperPersonEmpnbr string    `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号"`
	OperOrgNo        string    `orm:"column(oper_org_no);size(4);null" description:"操作机构号"`
	OperBgnTm        string    `orm:"column(oper_bgn_tm);type(time);null" description:"操作开始时间"`
	OperEndTm        string    `orm:"column(oper_end_tm);type(time);null" description:"操作结束时间"`
	//OperBgnDt        string    `orm:"column(oper_bgn_dt);type(time);null" description:"操作开始时间"`
	ApprvSugstnCd    string    `orm:"column(apprv_sugstn_cd);size(3);null" description:"审批意见代码"`
	RplshInfo        string    `orm:"column(rplsh_info);size(200);null" description:"补充信息"`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int      `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_credit_approval_flow) TableName() string {
	return "t_loan_credit_approval_flow"
}

func InsertT_loan_credit_approval_flow(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_credit_approval_flow where crdt_aply_sn=?"
		o.Raw(sql ,maps["CrdtAplySn"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_credit_approval_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_credit_approval_flowTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_credit_approval_flow where crdt_aply_sn=?"
		o.Raw(sql ,maps["CrdtAplySn"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_credit_approval_flow)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_credit_approval_flowTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_credit_approval_flow where crdt_aply_sn=?"
		o.Raw(sql ,maps["CrdtAplySn"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_credit_approval_flow where crdt_aply_sn=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_credit_approval_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_credit_approval_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_loan_credit_approval_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9" {//获取授信审批流水表下授信申请流水号下序号最大的那条记录--lxf
		if _, err = qs.Filter("tcc_state", state).OrderBy("-SeqNo").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_credit_approval_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_credit_approval_flow)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_credit_approval_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_credit_approval_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_credit_approval_flow)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
