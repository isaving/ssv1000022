package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_investigate_task struct {
	ArtgclInvstgTaskNo     string `orm:"column(artgcl_invstg_task_no);pk" description:"人工调查任务编号  PK"`
	CrdtAplySn             string `orm:"column(crdt_aply_sn);size(32);null" description:"授信申请流水号"`
	CustNo                 string `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName               string `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd          string `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo             string `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	ArtgclInvstgTaskStusCd string `orm:"column(artgcl_invstg_task_stus_cd);size(1);null" description:"人工调查任务状态代码  1-待处理2-转派待处理5-已处理6-转派已处理7-转派已确认"`
	AlrdyExecFlg           string `orm:"column(alrdy_exec_flg);size(1);null" description:"已执行标志"`
	ExecPrdFlg             string `orm:"column(exec_prd_flg);size(2);null" description:"执行期间标志"`
	ExecTms                int    `orm:"column(exec_tms);null" description:"执行次数"`
	ExecRsn                string `orm:"column(exec_rsn);size(200);null" description:"执行原因"`
	ExecConcusComnt        string `orm:"column(exec_concus_comnt);size(200);null" description:"执行结论说明"`
	SponsorCustMgrEmpnbr   string `orm:"column(sponsor_cust_mgr_empnbr);size(6);null" description:"主办客户经理员工号  客户经理与客户关系维护表查询"`
	SponsorCustMgrName     string `orm:"column(sponsor_cust_mgr_name);size(60);null" description:"主办客户经理姓名"`
	SponsorCustMgrOrgNo    string `orm:"column(sponsor_cust_mgr_org_no);size(4);null" description:"主办客户经理机构号"`
	CorgCustMgrEmpnbr      string `orm:"column(corg_cust_mgr_empnbr);size(6);null" description:"协办客户经理员工号"`
	CorgCustMgrName        string `orm:"column(corg_cust_mgr_name);size(60);null" description:"协办客户经理姓名"`
	CorgCustMgrOrgNo       string `orm:"column(corg_cust_mgr_org_no);size(4);null" description:"协办客户经理机构号"`
	SponsorCustMgrOrgNm    string `orm:"column(sponsor_cust_mgr_org_nm);size(120);null" description:"主办客户经理机构名称"`
	CorgCustMgrOrgNm       string `orm:"column(corg_cust_mgr_org_nm);size(120);null" description:"协办客户经理机构名称"`
	CrtDt                  string `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	FinlModfyDt            string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState               int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_investigate_task) TableName() string {
	return "t_loan_investigate_task"
}

func InsertT_loan_investigate_task(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_investigate_task).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_investigate_taskTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_investigate_task)).Filter("artgcl_invstg_task_no", maps["ArtgclInvstgTaskNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_investigate_taskTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_investigate_task where artgcl_invstg_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ArtgclInvstgTaskNo"]).Exec()
	return err
}

func QueryT_loan_investigate_task(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_investigate_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	if params["Flag"] == "9" {
		maps, err = QueryT_loan_investigate_taskFlag9(params, 0)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_investigate_task))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "StartDate" {
				qs = qs.Filter("crt_dt__gte", v)
			} else if k == "EndDate" {
				qs = qs.Filter("crt_dt__lte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_investigate_taskById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_investigate_task)).Filter("artgcl_invstg_task_no", maps["ArtgclInvstgTaskNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_investigate_task(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_investigate_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_investigate_task)).Filter("artgcl_invstg_task_no", maps["ArtgclInvstgTaskNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_investigate_taskFlag9(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_investigate_task))
	if params["Flag"] == "9" {
		SortDateFlg := params["SortDateFlg"]
		delete(params, "Flag")
		delete(params, "SortDateFlg")
		for k, v := range params {
			if k != constant.PageRecCount && k != constant.PageNo {
				if k == "StartDate" {
					qs = qs.Filter("crt_dt__gte", v)
				} else if k == "EndDate" {
					qs = qs.Filter("crt_dt__lte", v)
				} else {
					qs = qs.Filter(k, v)
				}
			}
		}
		if SortDateFlg == "1" {
			_, err = qs.Filter("tcc_state", state).OrderBy("crt_dt").Values(&maps)
		} else {
			_, err = qs.Filter("tcc_state", state).OrderBy("-crt_dt").Values(&maps)
		}
		if err != nil {
			return nil, err
		}
		return
	}
	return
}
