package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_collection_aftert_loan_result struct {
	TaskDlwthResultNo         string    `orm:"column(task_dlwth_result_no);pk" description:"任务处理结果编号"`
	ResultCnfrmRemrkInfo      string    `orm:"column(result_cnfrm_remrk_info);size(2000);null" description:"结果确认备注信息"`
	CollTaskNo                string    `orm:"column(coll_task_no);size(30);null" description:"催收任务编号  PK"`
	SeqNo                     int       `orm:"column(seq_no);null" description:"序号"`
	CustNo                    string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	ReacCustManrCd            string    `orm:"column(reac_cust_manr_cd);size(20);null" description:"触达客户方式代码  1-致电2-短信3-社交软件4-拜访本人面谈5-拜访相关人面谈6-其他"`
	KeprcdStusCd              string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CorgDlwthResultCd         string    `orm:"column(corg_dlwth_result_cd);size(1);null" description:"协办处理结果代码  1-待确认,2-已确认,3-已废除"`
	CollTms                   int       `orm:"column(coll_tms);null" description:"催收次数"`
	AfBnkLnCollDlwthResultCd  string    `orm:"column(af_bnk_ln_coll_dlwth_result_cd);size(1);null" description:"贷后催收处理结果代码  1-未接通2-未回复3-失约未见4-同意约定时限内归还5-同意进一步协商还款计划6-拒绝归还7-其他"`
	CollDlwthRemrk            string    `orm:"column(coll_dlwth_remrk);size(2000);null" description:"催收处理备注"`
	FamilyRsnCd               string    `orm:"column(family_rsn_cd);size(21);null" description:"家庭原因代码  (多选项,可并存)1-家庭变故2-重大疾病3-家中被骗4-自然灾害5-遭遇盗窃6-意外事故7-不良嗜好8-涉诉赔偿"`
	TrdRsnCd                  string    `orm:"column(trd_rsn_cd);size(22);null" description:"生意原因代码  (多选项,可并存)1-应收账款未收回2-多头投资3-行业不景气4-管理不善5-涉诉赔偿6-生意被骗7-自然灾害8-遭遇盗窃9-意外事故"`
	WorkRsnCd                 string    `orm:"column(work_rsn_cd);size(23);null" description:"工作原因代码  (多选项,可并存)1-工作变故2-涉诉赔偿"`
	CotactTelNo               string    `orm:"column(cotact_tel_no);size(20);null" description:"联系电话号码"`
	CurrAddr                  string    `orm:"column(curr_addr);size(200);null" description:"当前地址"`
	Lgtd                      float64   `orm:"column(lgtd);null;digits(11);decimals(8)" description:"经度"`
	LgtdDrctCd                string    `orm:"column(lgtd_drct_cd);size(1);null" description:"经度方向代码  E东W西"`
	Lttd                      float64   `orm:"column(lttd);null;digits(11);decimals(8)" description:"纬度"`
	LttdDrctCd                string    `orm:"column(lttd_drct_cd);size(1);null" description:"纬度方向代码  N北S南"`
	UpdAftRsdnceScAdcmCd      string    `orm:"column(upd_aft_rsdnce_sc_adcm_cd);size(12);null" description:"更新后居住地四川省行政区划代码  参考标准代码:CD0153"`
	UpdAftRsdnceDtlAddr       string    `orm:"column(upd_aft_rsdnce_dtl_addr);size(200);null" description:"更新后居住地详细地址"`
	UpdAftRsdnceAddr          string    `orm:"column(upd_aft_rsdnce_addr);size(200);null" description:"更新后居住地地址"`
	UpdAftCmunicAddrScAdcmCd  string    `orm:"column(upd_aft_cmunic_addr_sc_adcm_cd);size(12);null" description:"更新后通讯地址四川省行政区划代码  参考标准代码:CD0153"`
	UpdAftCmunicDtlAddr       string    `orm:"column(upd_aft_cmunic_dtl_addr);size(200);null" description:"更新后通讯详细地址"`
	UpdAftCmunicAddr          string    `orm:"column(upd_aft_cmunic_addr);size(200);null" description:"更新后通讯地址"`
	UpdGrndPostCd             string    `orm:"column(upd_grnd_post_cd);size(6);null" description:"更新地邮政编码"`
	MrgncyConterName          string    `orm:"column(mrgncy_conter_name);size(60);null" description:"紧急联系人姓名"`
	AndBrwerRelCd             string    `orm:"column(and_brwer_rel_cd);size(2);null" description:"与借款人关系代码"`
	MrgncyConterCotactTelNo   string    `orm:"column(mrgncy_conter_cotact_tel_no);size(20);null" description:"紧急联系人联系电话号码"`
	SponsorEmpnbr             string    `orm:"column(sponsor_empnbr);size(6);null" description:"主办员工号"`
	SponsorOrgNo              string    `orm:"column(sponsor_org_no);size(4);null" description:"主办机构号"`
	CorgEmpnbr                string    `orm:"column(corg_empnbr);size(6);null" description:"协办员工号"`
	CorgOrgNo                 string    `orm:"column(corg_org_no);size(4);null" description:"协办机构号"`
	OneslfCmtdInfo            string    `orm:"column(oneslf_cmtd_info);size(1000);null" description:"本人承诺信息"`
	DlwthDt                   string    `orm:"column(dlwth_dt);type(date);null" description:"处理日期"`
	HndlrEmpnbr               string    `orm:"column(hndlr_empnbr);size(6);null" description:"处理人员工号 "`
	CorgDlwthResultCnfrmRemrk string    `orm:"column(corg_dlwth_result_cnfrm_remrk);size(200);null" description:"协办处理结果确认备注"`
	FinlModfyDt               string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  10"`
	FinlModfyTm               string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  19"`
	FinlModfyOrgNo            string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo           string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                 int     `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_collection_aftert_loan_result) TableName() string {
	return "t_loan_collection_aftert_loan_result"
}

func InsertT_loan_collection_aftert_loan_result(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_collection_aftert_loan_result).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_collection_aftert_loan_resultTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_collection_aftert_loan_result)).Filter("task_dlwth_result_no", maps["TaskDlwthResultNo"]).
		Filter("coll_task_no", maps["CollTaskNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_collection_aftert_loan_resultTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_collection_aftert_loan_result where task_dlwth_result_no=? and coll_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TaskDlwthResultNo"], maps["CollTaskNo"]).Exec()
	return err
}

func QueryT_loan_collection_aftert_loan_result(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_collection_aftert_loan_result panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_loan_collection_aftert_loan_result))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9" {//il101036 返回记录最大的一条
		if _, err = qs.Filter("tcc_state", state).OrderBy("-seq_no").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_collection_aftert_loan_resultById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_collection_aftert_loan_result)).Filter("task_dlwth_result_no", maps["TaskDlwthResultNo"]).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_collection_aftert_loan_result(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_collection_aftert_loan_result panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_collection_aftert_loan_result)).Filter("task_dlwth_result_no", maps["TaskDlwthResultNo"]).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
