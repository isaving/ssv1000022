package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intrt_chg_his struct {
	IntacrRuleNo      string    `orm:"column(intacr_rule_no);size(12);pk" description:"计息规则编号"`
	EfftDt            string    `orm:"column(efft_dt);type(date)" description:"生效日期"`
	ChgDt             string    `orm:"column(chg_dt);type(date)" description:"变动日期"`
	UseIntrtSorcCd    string    `orm:"column(use_intrt_sorc_cd);size(3)" description:"使用利率来源代码"`
	HrchQty           int       `orm:"column(hrch_qty);null" description:"层级数量"`
	OrgNo             string    `orm:"column(org_no);size(4);null" description:"机构号"`
	IntrtChgCycCd     string    `orm:"column(intrt_chg_cyc_cd);size(2);null" description:"利率变动周期代码"`
	DpstDeadlCd       string    `orm:"column(dpst_deadl_cd);size(3);null" description:"存款期限代码"`
	HrchAmt           float64   `orm:"column(hrch_amt);null;digits(18);decimals(2)" description:"层级金额"`
	IntrtNo           string    `orm:"column(intrt_no);size(40);null" description:"利率编号"`
	RelsIntrtOrgNo    string    `orm:"column(rels_intrt_org_no);size(4);null" description:"发布利率机构号"`
	Intrt             float64   `orm:"column(intrt);null;digits(9);decimals(6)" description:"利率"`
	IntrtFlotManrCd   string    `orm:"column(intrt_flot_manr_cd);size(2);null" description:"利率浮动方式代码  P-浮动百分比,N-浮动点数记录利率浮动方式,如:按上限浮动,按下限浮动,按浮动比例浮动,等,"`
	FlotDrctCd        string    `orm:"column(flot_drct_cd);size(3);null" description:"浮动方向代码"`
	IntrtFlotCeilVal  float64   `orm:"column(intrt_flot_ceil_val);null;digits(9);decimals(6)" description:"利率浮动上限值"`
	IntrtFlotFloorVal float64   `orm:"column(intrt_flot_floor_val);null;digits(9);decimals(6)" description:"利率浮动下限值"`
	DfltFlotVal       float64   `orm:"column(dflt_flot_val);null;digits(12);decimals(8)" description:"默认浮动值"`
	LowestIntrt       float64   `orm:"column(lowest_intrt);null;digits(9);decimals(6)" description:"最低利率"`
	ChgTelrNo         string    `orm:"column(chg_telr_no);size(6);null" description:"变更柜员号"`
	AuthTelrNo        string    `orm:"column(auth_telr_no);size(6);null" description:"授权柜员号"`
	Flag1             string    `orm:"column(flag1);size(2);null" description:"标志1"`
	Flag2             string    `orm:"column(flag2);size(2);null" description:"标志2"`
	Bak1              float64   `orm:"column(bak1);null;digits(18);decimals(6)" description:"备用1"`
	Bak2              float64   `orm:"column(bak2);null;digits(18);decimals(6)" description:"备用2"`
	Bak3              string    `orm:"column(bak3);size(10);null" description:"备用3"`
	Bak4              string    `orm:"column(bak4);size(10);null" description:"备用4"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_intrt_chg_his) TableName() string {
	return "t_intrt_chg_his"
}

func InsertT_intrt_chg_his(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intrt_chg_his).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intrt_chg_hisTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intrt_chg_his)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).
		Filter("efft_dt", maps["EfftDt"]).Filter("chg_dt", maps["ChgDt"]).Filter("use_intrt_sorc_cd", maps["UseIntrtSorcCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intrt_chg_hisTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intrt_chg_his where intacr_rule_no=? and efft_dt=? and chg_dt=? and use_intrt_sorc_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IntacrRuleNo"], maps["EfftDt"], maps["ChgDt"], maps["UseIntrtSorcCd"]).Exec()
	return err
}

func QueryT_intrt_chg_his(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intrt_chg_his panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intrt_chg_his))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intrt_chg_hisById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intrt_chg_his)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).Filter("efft_dt", maps["EfftDt"]).
		Filter("chg_dt", maps["ChgDt"]).Filter("use_intrt_sorc_cd", maps["UseIntrtSorcCd"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intrt_chg_his(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intrt_chg_his panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intrt_chg_his)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).Filter("efft_dt", maps["EfftDt"]).
		Filter("chg_dt", maps["ChgDt"]).Filter("use_intrt_sorc_cd", maps["UseIntrtSorcCd"]).Filter("tcc_state", 0).Update(maps)
	return err
}
