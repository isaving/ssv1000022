package dao

import (
	"errors"
	"fmt"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
	"time"
)


func UpdateOrg_Test1(maps orm.Params) (Params map[string]interface{},err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateOrg_Test panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	Flag := maps["Flag"]
	Params = make(map[string]interface{})
	var sql string
	if Flag == "1" {
		sql = "update t_common_org set status=1 where org_id=?"
	} else if Flag == "2"{
		sql = "update t_common_org set bill_status=2 where org_id=?"
	} else if Flag == "3"{
		Params, err = UpdateOrg_Test3(o,maps)
		return Params,err
	}
	Params["Status"] = "Ok"
	_, err =o.Raw(sql,maps["OrgId"]).Exec()

	return Params,err
}

func UpdateOrg_Test2(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateOrg_Test panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	if maps["AcctEnName"] != "" && maps["AcctEnName"] != nil {
		sql := "update t_dep_cont_info set acct_en_name=? where cont_id=?"
		_, err =o.Raw(sql,maps["AcctEnName"], maps["ContId"]).Exec()
	}
	if maps["AcctCnName"] != "" && maps["AcctCnName"] != nil {
		sql := "update t_dep_cont_info set acct_cn_name=? where cont_id=?"
		_, err =o.Raw(sql,maps["AcctCnName"], maps["ContId"]).Exec()
	}

	return err
}

func UpdateOrg_Test3(o orm.Ormer, maps orm.Params) (Params map[string]interface{}, err error) {
	sql := "SELECT * FROM t_acc_dept where acctg_acct_no=?"
	mapList := []orm.Params{}
	_, err =o.Raw(sql, maps["AcctgAcctNo"]).Values(&mapList)
	if len(mapList) == 0 {
		err = errors.New("t_acc_dept Not Records")
		return Params, err
	}
	intr_plan_no := mapList[0]["intr_plan_no"]
	var last_rest_date string
	if mapList[0]["last_rest_date"] == nil || mapList[0]["last_rest_date"] == "" {
		last_rest_date = mapList[0]["acct_open_date"].(string)
	} else {
		last_rest_date = mapList[0]["last_rest_date"].(string)
	}
	Fixd_int_rateList := []orm.Params{}
	sql1 := "SELECT fixd_int_rate FROM t_intr_plan WHERE int_plan_no=?"
	_, err =o.Raw(sql1, intr_plan_no).Values(&Fixd_int_rateList)
	if len(Fixd_int_rateList) == 0 {
		err = errors.New("t_intr_plan Not Records")
		return Params, err
	}
	Fixd_int_rate, _ := strconv.ParseFloat(Fixd_int_rateList[0]["fixd_int_rate"].(string),64)
	current_acct_bal, _ := strconv.ParseFloat(mapList[0]["current_acct_bal"].(string),64)
	//利息 计算利息，利息=t_acc_dept.current_acct_bal*fixd_int_rate*(当前交易日-last_rest_date)/365
	//天数
	timenow := time.Now().Format("2006-01-02")
	TolData :=Date1SubDate2(timenow, last_rest_date)
	log.Infof("TolData:%v, timenow:%v, last_rest_date：%v",TolData, timenow, last_rest_date)
	lxAmt := current_acct_bal * Fixd_int_rate * TolData /365

	Params = make(map[string]interface{})
	Params["lxAmt"] = lxAmt						//利息
	Params["CurrentAcctBal"] = current_acct_bal	//本金
	Params["IntrPlanNo"] = intr_plan_no			//利率
	Params["LastRestDate"] = last_rest_date		//计息开始日期
	Params["LastRestDate"] = timenow			//计息结束日期

	log.Info("Params",Params)
	return Params,nil
}

func Date1SubDate2(date1, date2 string) float64 {
	 var time2 time.Time
	if len(date2) == 8 {
		time2, _ = time.Parse("20060102",date2)
	}else if len(date2) == 10 {
		time2, _ = time.Parse("2006-01-02",date2)
	}

	time1, _ := time.Parse("2006-01-02",time.Now().Format("2006-01-02"))

	t1 := time1.UTC().Truncate(24 * time.Hour)
	t2 := time2.UTC().Truncate(24 * time.Hour)
	return t1.Sub(t2).Hours() / 24
}