package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_process_model struct {
	ProcesModelId       string    `orm:"column(proces_model_id);size(50);pk" description:"流程模型id"`
	ProcesModelVersNo   int       `orm:"column(proces_model_vers_no)" description:"流程模型版本编号"`
	ProcesModelNm       string    `orm:"column(proces_model_nm);size(120);null" description:"流程模型名称"`
	ProcesTmplId        string    `orm:"column(proces_tmpl_id);size(50);null" description:"流程模板id"`
	ProcesGraphBase64Cd string    `orm:"column(proces_graph_base64_cd);null" description:"流程图base64编码"`
	CrtTelrNo           string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtOrgNo            string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	CrtDt               string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtTm               string    `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	FinlModfyDt         string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState            int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_process_model) TableName() string {
	return "t_loan_process_model"
}

func InsertT_loan_process_model(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_process_model).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_process_modelTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_process_model)).Filter("proces_model_id", maps["ProcesModelId"]).
		Filter("proces_model_vers_no", maps["ProcesModelVersNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_process_modelTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_process_model where proces_model_id=? and proces_model_vers_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ProcesModelId"], maps["ProcesModelVersNo"]).Exec()
	return err
}

func QueryT_loan_process_model(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_process_model panic"+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	if flag=="9"{
		sql := "SELECT t1.`proces_tmpl_id` PocessDefId,t1.`proces_model_nm` PocessDefName,t1.`proces_graph_base64_cd`,t2.`task_tmpl_typ_cd` TaskTmplTypCd,t2.`task_tmpl_no` TaskDefId,t2.`task_nm` `Name` FROM `t_loan_process_model` t1 LEFT JOIN `t_proc_model_para` t2 "+
			"ON t1.`proces_model_id`=t2.`proces_model_id` AND t1.`proces_model_vers_no`=t2.`proces_model_vers_no` AND t1.`tcc_state`=0 AND t2.`tcc_state`=0 "
		if params["PocessDefId"] != nil{
			sql =sql +" where t1.`proces_model_id`=\""+params["PocessDefId"].(string)+"\""
		}
		_,err = o.Raw(sql).Values(&maps)
		return
	}
	qs := o.QueryTable(new(T_loan_process_model))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("proces_model_id").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_process_modelById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_process_model)).Filter("proces_model_id", maps["ProcesModelId"]).Filter("proces_model_vers_no", maps["ProcesModelVersNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_process_model(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_process_model panic"+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_process_model)).Filter("proces_model_id", maps["ProcesModelId"]).Filter("proces_model_vers_no", maps["ProcesModelVersNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
