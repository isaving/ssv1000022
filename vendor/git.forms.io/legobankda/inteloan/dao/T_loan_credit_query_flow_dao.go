package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_credit_query_flow struct {
	CrdQurySn       string    `orm:"column(crd_qury_sn);pk" description:"征信查询流水号  生成规则:应用2位,+,日期时间14位'YYYYMMDDHHMMSS'+,随机整数8位举例:应用:IL'智能贷款',类型:IMLN-减值贷款,日期时间:20191014235501生成贷款账号:IL2019101423550108223812"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	IndvCrtfTypCd   string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo      string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CrdQuryDt       string    `orm:"column(crd_qury_dt);type(date);null" description:"征信查询日期"`
	CrdQurySuccFlg  string    `orm:"column(crd_qury_succ_flg);size(1);null" description:"征信查询成功标志"`
	CrdQuryResultId string    `orm:"column(crd_qury_result_id);size(50);null" description:"征信查询结果id"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_credit_query_flow) TableName() string {
	return "t_loan_credit_query_flow"
}

func InsertT_loan_credit_query_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_credit_query_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_credit_query_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_credit_query_flow)).Filter("crd_qury_sn", maps["CrdQurySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_credit_query_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_credit_query_flow where crd_qury_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdQurySn"]).Exec()
	return err
}

func QueryT_loan_credit_query_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_credit_query_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_credit_query_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_credit_query_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_credit_query_flow)).Filter("crd_qury_sn", maps["CrdQurySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_credit_query_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_credit_query_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_credit_query_flow)).Filter("crd_qury_sn", maps["CrdQurySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
