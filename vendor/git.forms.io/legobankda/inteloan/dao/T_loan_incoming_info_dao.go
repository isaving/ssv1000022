package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_incoming_info struct {
	CrdtAplySn           string    `orm:"column(crdt_aply_sn);pk" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	CustNo               string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	StrQtyCustFlg        string    `orm:"column(str_qty_cust_flg);size(1);null" description:"存量客户标志"`
	AplyTypCd            string    `orm:"column(aply_typ_cd);size(3);null" description:"申请类型代码  1-授信申请,,2-客户提额申请,,3-客户经理提额申请"`
	RecomCustMgrEmpnbr   string    `orm:"column(recom_cust_mgr_empnbr);size(6);null" description:"推荐客户经理员工号"`
	ApyfLprOrgNo         string    `orm:"column(apyf_lpr_org_no);size(4);null" description:"申办法人机构号"`
	CrdtAplyDt           string    `orm:"column(crdt_aply_dt);type(date);null" description:"授信申请日期"`
	InitCredQta          float64    `orm:"column(init_cred_qta);type(date);null" description:"初始授信额度"`
	AplyChnlCd           string    `orm:"column(aply_chnl_cd);size(4);null" description:"申请渠道编码"`
	LoanProdtNo          string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo      string    `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	LoanProdtNm          string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	QtaMltpLendFlg       string    `orm:"column(qta_mltp_lend_flg);size(1);null" description:"额度多次放贷标志"`
	WhtlPreCrdtQta       float64   `orm:"column(whtl_pre_crdt_qta);null;digits(18);decimals(2)" description:"白名单预授信额度"`
	AplyCrdtQta          float64   `orm:"column(aply_crdt_qta);null;digits(18);decimals(2)" description:"申请授信额度"`
	LoanCurCd            string    `orm:"column(loan_cur_cd);size(3);null" description:"贷款币种代码"`
	LoanUsageCd          string    `orm:"column(loan_usage_cd);size(4);null" description:"贷款用途代码  1-个人日常消费,,2-装修,3-旅游,4-教育,5-医疗,,6-其他消费"`
	LoanUsageRplshDescr  string    `orm:"column(loan_usage_rplsh_descr);size(200);null" description:"贷款用途补充描述"`
	LoanInvtIndusCd      string    `orm:"column(loan_invt_indus_cd);size(8);null" description:"贷款投向行业代码  等待补充码值"`
	CrdtAplyStusCd       string    `orm:"column(crdt_aply_stus_cd);size(5);null" description:"授信申请状态代码  01-申请已受理02-人工调查完成03-审查通过04-审批通过05-申请成功06-申请失败"`
	CrdtAplyTm           string    `orm:"column(crdt_aply_tm);type(time);null" description:"授信申请时间"`
	SponsorCustMgrEmpnbr string    `orm:"column(sponsor_cust_mgr_empnbr);size(6);null" description:"主办客户经理员工号"`
	CorgCustMgrEmpnbr    string    `orm:"column(corg_cust_mgr_empnbr);size(6);null" description:"协办客户经理员工号"`
	ActngMgmtOrgNo       string    `orm:"column(actng_mgmt_org_no);size(4);null" description:"账户管理机构号"`
	FinlModfyDt          string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm          string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo       string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int8      `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_incoming_info) TableName() string {
	return "t_loan_incoming_info"
}

func InsertT_loan_incoming_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_incoming_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_incoming_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_incoming_info)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_incoming_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_incoming_info where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryT_loan_incoming_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_incoming_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params,"Flag")
	o := orm.NewOrm()
	if flag == "8" {	//授信申请--lzh
		maps ,err = QueryT_loan_incoming_infoFlag8(params)
		return
	}
	if flag == "7" {	//lzh
		maps ,err = QueryT_loan_incoming_infoFlag7(params)
		return
	}
	qs := o.QueryTable(new(T_loan_incoming_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "CrdtAplyStarTm" {
				qs = qs.Filter("crdt_aply_dt__gte", v)
			}else if k == "CrdtAplyEndTm" {
				qs = qs.Filter("crdt_aply_dt__lte", v)
			}else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if flag=="9"{
		if _, err = qs.Filter("tcc_state", state).Limit(1).OrderBy("-crdt_aply_dt").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_incoming_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_incoming_info)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_incoming_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_incoming_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_incoming_info)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_incoming_infoFlag8(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "select "+
		"t1.`task_examp_id` TaskExampId,"+
		"t1.`biz_id` CrdtAplySn,"+
		"t1.`cust_no` CustNo,"+
		"t3.`cust_name` CustNm,"+
		"t1.`task_dlwth_stus_cd` TaskDlwthStusCd,"+
		"t2.`loan_prodt_no` LoanProdtNo,"+
		"t2.`loan_prodt_nm` LoanProdtNm,"+
		"t2.`crdt_aply_dt` CrdtAplyDt,"+
		"t2.crdt_aply_stus_cd CrdtAplyStusCd "+
		"from t_loan_approve_task t1,t_loan_incoming_info t2 left join t_loan_cust_base_info t3 On t2.`cust_no`=t3.`cust_no` "+
		"where t1.`biz_id` = t2.`crdt_aply_sn` and  t1.`cust_no`=t2.`cust_no`"
	if params["DistrPersonEmpnbr "] != nil {
		sql = sql + " and t1.distr_person_empnbr= \""+params["DistrPersonEmpnbr "].(string)+"\""
	}
	if params["MenuFuncId"] != nil {
		sql = sql + " and t1.menu_func_id= \""+params["MenuFuncId"].(string)+"\""
	}
	if params["CustNo"] != nil {
		sql = sql + "and t1.cust_no= \""+params["CustNo"].(string)+"\""
	}
	if params["DealFlg"] != nil {
		sql = sql + " and t1.task_dlwth_stus_cd= \""+params["DealFlg"].(string)+"\""
	}
	if params["CustNm"] != nil {
		sql = sql + " and t3.cust_name like  \"%"+params["CustNm"].(string)+"%\""
	}
	if params["IndvCrtfTypCd"] != nil {
		sql = sql + " and t3.indv_crtf_typ_cd=\""+params["IndvCrtfTypCd"].(string)+"\""
	}
	if params["CrtfNo"] != nil {
		sql = sql + " and t3.indv_crtf_no=\""+params["CrtfNo"].(string)+"\""
	}
	if params["CrdtAplyStarDt"] != nil {
		sql = sql + " and t2.crdt_aply_dt >=\""+params["CrdtAplyStarDt"].(string)+"\""
	}
	if params["CrdtAplyEndDt"] != nil {
		sql = sql + " and t2.crdt_aply_dt<=\""+params["CrdtAplyEndDt"].(string)+"\""
	}
	sql = sql +" GROUP BY t1.`biz_id`"
	_, err = o.Raw(sql,).Values(&maps)
	return
}
func QueryT_loan_incoming_infoFlag7(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  "+
		"t1.crdt_aply_sn CrdtAplySn, "+
		"t1.cust_no CustNo, "+
		"t1.init_cred_qta InitCredQta, "+
		"t1.finl_modfy_dt FinlModfyDt, "+
		"t1.finl_modfy_tm FinlModfyTm, "+
		"t2.cust_name CustName "+
		"FROM  "+
		"t_loan_incoming_info t1,t_loan_cust_base_info t2 "+
		"WHERE t1.crdt_aply_stus_cd='05' AND t1.crdt_aply_sn=t2.crdt_aply_sn and t1.tcc_state=0 and t2.tcc_state=0"
	_, err = o.Raw(sql,).Values(&maps)
	return
}