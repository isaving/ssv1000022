package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intrt_plan_hrch struct {
	IntacrRuleNo    string    `orm:"column(intacr_rule_no);size(40);pk" description:"计息规则编号  计划号组合规则利息计划类型+利息计划种类+序号1,利息计划类型'TA-存款活期,TM-存款定期,LA-贷款'2,利息计划种类001-正常利率,002-逾期利率,003-提前支取利率,004-复利利率,005-活期分层利率,006-剩余金额利率,007-贷款正常本金利率008=贷款逾期本金利率009-贷款复利利率PK"`
	HrchQty         int       `orm:"column(hrch_qty)" description:"层级数量"`
	DpstDeadlCd     string    `orm:"column(dpst_deadl_cd);size(3);null" description:"存款期限代码"`
	HrchAmt         float64   `orm:"column(hrch_amt);null;digits(18);decimals(2)" description:"层级金额"`
	UseIntrtSorcCd  string    `orm:"column(use_intrt_sorc_cd);size(2);null" description:"使用利率来源代码  1-编号利率,'利率平台发布的利率'2-固定利率,'协议利率'1-编号利率,2-固定利率,-"`
	IntrtNo         string    `orm:"column(intrt_no);size(40);null" description:"利率编号"`
	FixdIntrt       float64   `orm:"column(fixd_intrt);null;digits(9);decimals(6)" description:"固定利率"`
	FixdIntrtEfftDt string    `orm:"column(fixd_intrt_efft_dt);type(date);null" description:"固定利率生效日期"`
	IntacrPrtyTypCd string    `orm:"column(intacr_prty_typ_cd);size(2);null" description:"计息优先类型代码  P-结息日优先,C-利率变化优先,O-开户日优先-"`
	LowestIntrt     float64   `orm:"column(lowest_intrt);null;digits(9);decimals(6)" description:"最低利率  表示该产品的最低执行利率"`
	IntrtFlotManrCd string    `orm:"column(intrt_flot_manr_cd);size(2);null" description:"利率浮动方式代码  P-浮动百分比,N-浮动点数记录利率浮动方式,如:按上限浮动,按下限浮动,按浮动比例浮动,等,"`
	FlotDrctCd      string    `orm:"column(flot_drct_cd);size(2);null" description:"浮动方向代码  D-下浮,U-上浮,-"`
	FlotRate        float64   `orm:"column(flot_rate);null;digits(9);decimals(6)" description:"浮动比率"`
	Flag1           string    `orm:"column(flag1);size(2);null" description:"标志1"`
	Flag2           string    `orm:"column(flag2);size(2);null" description:"标志2"`
	Bak1            float64   `orm:"column(bak1);null;digits(18);decimals(6)" description:"备用1"`
	Bak2            float64   `orm:"column(bak2);null;digits(18);decimals(6)" description:"备用2"`
	Bak3            string    `orm:"column(bak3);size(10);null" description:"备用3"`
	Bak4            string    `orm:"column(bak4);size(20);null" description:"备用4"`
	Bak5            string    `orm:"column(bak5);size(10);null" description:"备用5"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccKeprcdVisFlg string    `orm:"column(tcc_keprcd_vis_flg);size(1);null" description:"tcc记录可见标志  0否1是"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_intrt_plan_hrch) TableName() string {
	return "t_intrt_plan_hrch"
}

func InsertT_intrt_plan_hrch(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intrt_plan_hrch).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intrt_plan_hrchTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intrt_plan_hrch)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).
		Filter("hrch_qty", maps["HrchQty"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intrt_plan_hrchTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_intrt_plan_hrch where intacr_rule_no=? and hrch_qty=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IntacrRuleNo"], maps["HrchQty"]).Exec()
	return err
}

func QueryT_intrt_plan_hrch(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intrt_plan_hrch panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intrt_plan_hrch))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intrt_plan_hrchById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intrt_plan_hrch)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).Filter("hrch_qty", maps["HrchQty"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intrt_plan_hrch(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intrt_plan_hrch panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intrt_plan_hrch)).Filter("intacr_rule_no", maps["IntacrRuleNo"]).Filter("hrch_qty", maps["HrchQty"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
