package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_whitelist_info struct {
	OrgNo                string  `orm:"column(org_no);size(4);pk" description:"机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	SeqNo                int     `orm:"column(seq_no)" description:"序号"`
	WhtlNo               string  `orm:"column(whtl_no);size(32)" description:"白名单编号  生成规则:应用2位+机构4位+序号6位举例:应用:il'智能贷款',机构:0401生成贷款账号:il0401000001"`
	BizSn                string  `orm:"column(biz_sn);size(32);null" description:"业务流水号"`
	WhtlNm               string  `orm:"column(whtl_nm);size(120);null" description:"白名单名称"`
	SorcDescr            string  `orm:"column(sorc_descr);size(200);null" description:"来源描述"`
	CustNmlstNewaddTypCd string  `orm:"column(cust_nmlst_newadd_typ_cd);size(1);null" description:"客户名单新增类型代码  1-批量导入2-手工加入"`
	RecmndEmpnbr         string  `orm:"column(recmnd_empnbr);size(6);null" description:"推荐人员工号"`
	CustNo               string  `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,crm兼顾启信宝eid长度20"`
	IndvCrtfTypCd        string  `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:cd00153"`
	IndvCrtfNo           string  `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CustName             string  `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	MobileNo             string  `orm:"column(mobile_no);size(11);null" description:"手机号码  记录个人客户的国内手机号码信息,"`
	Age                  int     `orm:"column(age);null" description:"年龄"`
	GenderCd             string  `orm:"column(gender_cd);size(3);null" description:"性别代码  参考标准代码:cd0006"`
	MarrgSituationCd     string  `orm:"column(marrg_situation_cd);size(3);null" description:"婚姻状况代码  参考标准代码:cd0004"`
	EdctbkgCd            string  `orm:"column(edctbkg_cd);size(2);null" description:"学历代码  参考标准代码:cd0008"`
	DwlgSituationCd      string  `orm:"column(dwlg_situation_cd);size(1);null" description:"居住状况代码  参考标准代码:cd0128"`
	RsdnceAddr           string  `orm:"column(rsdnce_addr);size(200);null" description:"居住地地址"`
	CareerTypCd          string  `orm:"column(career_typ_cd);size(5);null" description:"职业类型代码  参考标准代码:cd0013"`
	WorkUnitNm           string  `orm:"column(work_unit_nm);size(120);null" description:"工作单位名称  记录个人客户的工作单位信息,此处应该填写工作单位的全称,"`
	UnitCharcCd          string  `orm:"column(unit_charc_cd);size(3);null" description:"单位性质代码  参考标准代码:cd0087"`
	IncomeLvlCd          string  `orm:"column(income_lvl_cd);size(2);null" description:"收入级别代码  1-1000以下2-1000-3000(含)3-3000-5000(含)4-5000-10000(含)5-10000-20000(含)6-20000-50000(含)7-50000以上"`
	WhtlRsnCd            string  `orm:"column(whtl_rsn_cd);size(2);null" description:"白名单原因代码  01-本行代发工资02-存量按揭客户03-aum达标客户04-优质公职人员05-公积金人群06-社保人群07-缴纳个税人群08-集团获客人群09-产业链人群10-其他优质人群"`
	CreditRatCd          string  `orm:"column(credit_rat_cd);size(2);null" description:"信用评级代码  参考标准代码:cd0026"`
	PreCrdtQta           float64 `orm:"column(pre_crdt_qta);null;digits(18);decimals(2)" description:"预授信额度"`
	CrdtDeadl            int     `orm:"column(crdt_deadl);null" description:"授信期限"`
	CrdtStusCd           string  `orm:"column(crdt_stus_cd);size(3);null" description:"授信状态代码  1-未授信2-授信已用款3-授信未用款"`
	CustLvlCd            string  `orm:"column(cust_lvl_cd);size(1);null" description:"客户级别代码  01-大众客户02-理财级别客户03-贵宾级别客户04-财富级别客户05-私人银行客户"`
	MatrDt               string  `orm:"column(matr_dt);type(date);null" description:"到期日期  白名单的到期日10"`
	KeprcdStusCd         string  `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	DeregisRsn           string  `orm:"column(deregis_rsn);size(200);null" description:"注销原因"`
	CrtDt                string  `orm:"column(crt_dt);type(date);null" description:"创建日期  10"`
	CrtEmpnbr            string  `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo             string  `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt          string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  10"`
	FinlModfyTm          string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  19"`
	FinlModfyOrgNo       string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int     `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_whitelist_info) TableName() string {
	return "t_loan_whitelist_info"
}

func InsertT_loan_whitelist_info(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_whitelist_info where org_no=? and whtl_no=?"
		o.Raw(sql, maps["OrgNo"], maps["WhtlNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_whitelist_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_whitelist_infoTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_whitelist_info where org_no=? and whtl_no=?"
		o.Raw(sql, maps["OrgNo"], maps["WhtlNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_whitelist_info)).Filter("org_no", maps["OrgNo"]).
		Filter("seq_no", maps["SeqNo"]).Filter("whtl_no", maps["WhtlNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_whitelist_infoTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_whitelist_info where org_no=? and whtl_no=?"
		o.Raw(sql, maps["OrgNo"], maps["WhtlNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_whitelist_info where org_no=? and seq_no=? and whtl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OrgNo"], maps["SeqNo"], maps["WhtlNo"]).Exec()
	return err
}

func QueryT_loan_whitelist_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_whitelist_info panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_whitelist_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo && k != "Flag" {
			if k == "StartDate" {
				qs = qs.Filter("crt_dt__gte", v)
			} else if k == "EndDate" {
				qs = qs.Filter("crt_dt__lte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if params["Flag"] != nil && params["Flag"] == "9" {
		sql := "SELECT * from (" +
			"SELECT whtl_no WhtlNo,whtl_nm WhtlNm, cust_nmlst_newadd_typ_cd CustNmlstNewaddTypCd,recmnd_empnbr RecmndEmpnbr,crt_dt CrtDt,keprcd_stus_cd KeprcdStusCd,COUNT(*) COUNT  FROM t_loan_whitelist_info " +
			" GROUP BY whtl_no,whtl_nm,cust_nmlst_newadd_typ_cd,recmnd_empnbr,crt_dt,keprcd_stus_cd" +
			" ) t1 where 1=1 "
		if params["WhtlNo"] != nil {
			sql = sql + " and WhtlNo=\"" + params["WhtlNo"].(string) + "\""
		}
		if params["WhtlNm"] != nil {
			sql = sql + " and WhtlNm=\"" + params["WhtlNm"].(string) + "\""
		}
		if params["CustNmlstNewaddTypCd"] != nil {
			sql = sql + " and CustNmlstNewaddTypCd=\"" + params["CustNmlstNewaddTypCd"].(string) + "\""
		}
		if params["RecmndEmpnbr"] != nil {
			sql = sql + " and RecmndEmpnbr=\"" + params["RecmndEmpnbr"].(string) + "\""
		}
		if params["KeprcdStusCd"] != nil {
			sql = sql + " and KeprcdStusCd=\"" + params["KeprcdStusCd"].(string) + "\""
		}
		if params["StartDate"] != nil {
			sql = sql + " and CrtDt>=\"" + params["StartDate"].(string) + "\""
		}
		if params["EndDate"] != nil {
			sql = sql + " and CrtDt<=\"" + params["EndDate"].(string) + "\""
		}
		sql = sql +" ORDER BY t1.CrtDt DESC"
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	if params["Flag"] == "8" {
		if _, err = qs.Filter("tcc_state", state).Limit(-1).Filter("keprcd_stus_cd__in", "1", "3").OrderBy("-finl_modfy_tm","-finl_modfy_dt").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("-crt_dt").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_whitelist_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_whitelist_info)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("whtl_no", maps["WhtlNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_whitelist_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_whitelist_info panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_whitelist_info)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("whtl_no", maps["WhtlNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
