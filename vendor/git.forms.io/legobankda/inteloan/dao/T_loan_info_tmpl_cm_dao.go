package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"time"
)

type T_loan_info_tmpl_cm struct {
	TmplNo            string `orm:"column(tmpl_no);pk" description:"模板编号"`
	TmplNm            string `orm:"column(tmpl_nm);size(400);null" description:"模板名称"`
	TmplComnt         string `orm:"column(tmpl_comnt);size(200);null" description:"模板说明"`
	EfftDt            string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	LoanProdtNo       string `orm:"column(loan_prodt_no);size(40);null" description:"产品编号"`
	ApplyScena        string `orm:"column(apply_scena);size(10);null" description:"适用场景大类"`
	TextTmplTypCd     string `orm:"column(text_tmpl_typ_cd);size(10);null" description:"模板业务类型代码"`
	EventId           string `orm:"column(event_id);size(40);null" description:"事件ID"`
	TextTmplDetailCd  string `orm:"column(text_tmpl_detail_cd);size(4);null" description:"文本模板细类代码"`
	OrgId             string `orm:"column(org_id);size(40);null" description:"机构id"`
	Area              string `orm:"column(area);size(40);null" description:"地区"`
	ChannelNumber     string `orm:"column(channel_number);size(40);null" description:"渠道号"`
	SendObjectType    string `orm:"column(send_object_type);size(4);null" description:"发送对象类型（01-机构，02-客户经理）"`
	SuitHavegProdtFlg string `orm:"column(suit_haveg_prodt_flg);size(4);null" description:"适用所有产品标志"`
	TmplContent       string `orm:"column(tmpl_content);null" description:"模板内容base64"`
	PmitSndFlg        string `orm:"column(pmit_snd_flg);size(4);null" description:"允许发送标志"`
	KeprcdStusCd      string `orm:"column(keprcd_stus_cd);size(4);null" description:"记录状态代码"`
	Priority          string `orm:"column(priority);size(4);null" description:"优先级（1最高；2较高；3中；4低）"`
	ProjectId         string `orm:"column(project_id);size(40);null" description:"项目Id"`
	Title             string `orm:"column(title);size(200);null" description:"短语或模板标题"`
	MessageType       string `orm:"column(message_type);size(4);null" description:"信息类型：01，短信；02，彩信；03，WAP_PUSH ，04-邮件、05-通信，06发送到第三方APP，"`
	Type              string `orm:"column(type);size(4);null" description:"模板内容类型: 1--静态模板,2--动态模板(含有变量) "`
	TmplType          string `orm:"column(tmpl_type);size(4);null" description:"模板类型1-普通模板  2特殊模板"`
	State             string `orm:"column(state);size(4);null" description:"0:免审;1:待审核; 2:审核通过; 3:审核不通过"`
	Subject           string `orm:"column(subject);size(40);null" description:"主题 比如邮件主题"`
	PubType           string `orm:"column(pub_type);size(4);null" description:"是否公共模板：0--私有模板，1--公共模板"`
	TmplId            string `orm:"column(tmpl_id);size(40);null" description:"模板ID(消息中心的模板ID)"`
	SendStrategyId    string `orm:"column(send_strategy_id);size(40);null" description:"发送策略ID"`
	AttachFlag        string `orm:"column(attach_flag);size(4);null" description:"是否有附件（01-是，02-否）"`
	CreateUser        string `orm:"column(create_user);size(40);null" description:"创建人"`
	CreateTime        string `orm:"column(create_time);type(time);null" description:"创建时间"`
	FinlModfyDt       string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState          int    `orm:"column(tcc_state);null" description:"tcc状态 0-Normal，1-Insert"`
}

func (t *T_loan_info_tmpl_cm) TableName() string {
	return "t_loan_info_tmpl"
}

func InsertT_loan_info_tmpl_cm(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_info_tmpl_cm).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_info_tmpl_cmTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_info_tmpl_cm)).Filter("tmpl_no", maps["TmplNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_info_tmpl_cmTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_info_tmpl where tmpl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplNo"]).Exec()
	return err
}

func QueryT_loan_info_tmpl_cm(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_info_tmpl_cm panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params, "Flag")
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_info_tmpl_cm))
	if "9" == flag {
		maps, err = QueryT_loan_info_tmplFlag9(params)

		//for k, v := range params {
		//	if k != constant.PageRecCount && k != constant.PageNo {
		//		qs = qs.Filter(k, v)
		//	}
		//}
		//if _, err = qs.Filter("tcc_state", state).Filter("efft_dt__lte", time.Now().Format("2006-01-02")).Limit(-1).Values(&maps); err != nil {
		//	return nil, err
		//}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_info_tmpl_cmById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_info_tmpl_cm)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_info_tmpl_cm(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_info_tmpl_cm panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_info_tmpl_cm)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_info_tmplFlag9(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	todayDate := time.Now().Format("2006-01-02")
	//var sqlstr strings.Builder
	sql := "SELECT " +
		"tmpl_no    TmplNo ," +
		"tmpl_nm    TmplNm ," +
		"tmpl_comnt    TmplComnt ," +
		"efft_dt    EfftDt ," +
		"loan_prodt_no    LoanProdtNo ," +
		"apply_scena    ApplyScena ," +
		"text_tmpl_typ_cd    TextTmplTypCd ," +
		"text_tmpl_typ_cd    TextTmplTypCd ," +
		"event_id    EventId ," +
		"text_tmpl_detail_cd    TextTmplDetailCd ," +
		"org_id    OrgId ," +
		"area    Area ," +
		"channel_number    ChannelNumber ," +
		"send_object_type    SendObjectType ," +
		"suit_haveg_prodt_flg    SuitHavegProdtFlg ," +
		"tmpl_content    TmplContent ," +
		"pmit_snd_flg    PmitSndFlg ," +
		"keprcd_stus_cd    KeprcdStusCd ," +
		"priority    Priority ," +
		"project_id    ProjectId ," +
		"title    Title ," +
		"message_type    MessageType ," +
		"type    Type ," +
		"tmpl_type    TmplType ," +
		"state    State ," +
		"subject    Subject ," +
		"pub_type    PubType ," +
		"tmpl_id    TmplId ," +
		"send_strategy_id    SendStrategyId ," +
		"attach_flag    AttachFlag ," +
		"create_user    CreateUser ," +
		"create_time    CreateTime ," +
		"finl_modfy_dt    FinlModfyDt ," +
		"finl_modfy_tm    FinlModfyTm ," +
		"finl_modfy_org_no    FinlModfyOrgNo ," +
		"finl_modfy_telr_no    FinlModfyTelrNo  " +
		"FROM t_loan_info_tmpl " +
		"WHERE priority = (select min(priority) from t_loan_info_tmpl where keprcd_stus_cd = '01' and state in ('0','2') and efft_dt <=\"" +
		todayDate + "\"" + " ) "

	//sqlstr.WriteString(sql)
	if maps["ApplyScena"] != nil { //适用场景大类
		sql = sql + " and apply_scena=\"" + maps["ApplyScena"].(string) + "\""
	}
	if maps["LoanProdtNo"] != nil { //产品编号
		sql = sql + " and loan_prodt_no=\"" + maps["LoanProdtNo"].(string) + "\""
	}
	if maps["TextTmplTypCd"] != nil { //模板业务类型代码
		sql = sql + " and text_tmpl_typ_cd=\"" + maps["TextTmplTypCd"].(string) + "\""
	}
	if maps["TextTmplDetailCd"] != nil { //模板类型细分
		sql = sql + " and text_tmpl_detail_cd=\"" + maps["TextTmplDetailCd"].(string) + "\""
	}
	if maps["OrgId"] != nil { //机构id
		sql = sql + " and org_id=\"" + maps["OrgId"].(string) + "\""
	}
	if maps["Area"] != nil { //地区
		sql = sql + " and area=\"" + maps["Area"].(string) + "\""
	}
	if maps["ChannelNumber"] != nil { //渠道号
		sql = sql + " and channel_number=\"" + maps["ChannelNumber"].(string) + "\""
	}
	if maps["SendObjectType"] != nil { //发送对象类型
		sql = sql + " and send_object_type=\"" + maps["SendObjectType"].(string) + "\""
	}
	if maps["EventId"] != nil { //事件ID
		sql = sql + " and event_id=\"" + maps["EventId"].(string) + "\""
	}
	_, err = o.Raw(sql).Values(&mapList)
	return
}
