package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strings"
)

type T_common_role struct {
	RoleId        string    `orm:"column(role_id);pk" description:"角色代码"`
	Status        string    `orm:"column(status);size(1);null" description:"状态"`
	RoleName      string    `orm:"column(role_name);size(20);null" description:"角色名称"`
	Description   string    `orm:"column(description);size(60);null" description:"描述"`
	RoleCategory  string    `orm:"column(role_category);size(1);null" description:"角色类别"`
	PermGroup1    string    `orm:"column(perm_group1);size(4);null" description:"权限组1"`
	PermGroup2    string    `orm:"column(perm_group2);size(4);null" description:"权限组2"`
	PermGroup3    string    `orm:"column(perm_group3);size(4);null" description:"权限组3"`
	PermGroup4    string    `orm:"column(perm_group4);size(4);null" description:"权限组4"`
	EffDate       string    `orm:"column(eff_date);type(date);null" description:"生效日期"`
	ExpDate       string    `orm:"column(exp_date);type(date);null" description:"失效日期"`
	LastMaintDate string    `orm:"column(last_maint_date);type(date);null" description:"最后更新日期"`
	LastMaintTime string    `orm:"column(last_maint_time);type(time);null" description:"最后更新时间"`
	LastMaintBrno string    `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	LastMaintTell string    `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	TccState      string    `orm:"column(tccstatus);size(1);null"`
}

func (t *T_common_role) TableName() string {
	return "t_common_role"
}

func InsertT_common_role(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_common_role).TableName())
	sql = strings.Replace(sql,"tcc_state","tccstatus",1)
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_common_roleTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_common_role)).Filter("role_id", maps["RoleId"]).
		Update(orm.Params{"tccstatus": 0})
	return err
}

func DeleteT_common_roleTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_common_role where role_id=? and tccstatus = 1"
	_, err := o.Raw(sql, maps["RoleId"]).Exec()
	return err
}

func QueryT_common_role(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_common_role panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_common_role))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tccstatus", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_common_roleById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_common_role)).Filter("role_id", maps["RoleId"]).
		Filter("tccstatus", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_common_role(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_common_role panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_common_role)).Filter("role_id", maps["RoleId"]).
		Filter("tccstatus", 0).Update(maps)
	return err
}
