package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_lpr_org_sale_quota struct {
	OrgNo                  string    `orm:"column(org_no);size(4);pk" description:"机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	SeqNo                  int       `orm:"column(seq_no)" description:"序号  10PK"`
	LoanProdtNo            string    `orm:"column(loan_prodt_no);size(20)" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	CurCd                  string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	MgctManrCd             string    `orm:"column(mgct_manr_cd);size(1);null" description:"管控方式代码  1:立即停止2:余额控制"`
	BalCtrlTypCd           string    `orm:"column(bal_ctrl_typ_cd);size(1);null" description:"余额控制类型代码  1:无限期2:有限期"`
	BalCtrlAmt             float64   `orm:"column(bal_ctrl_amt);null;digits(18);decimals(2)" description:"余额控制金额"`
	BalCtrlBgnDt           string    `orm:"column(bal_ctrl_bgn_dt);type(date);null" description:"余额控制开始日期"`
	BalCtrlEndDt           string    `orm:"column(bal_ctrl_end_dt);type(date);null" description:"余额控制结束日期"`
	NoneDeadlBalCtrlEfftDt string    `orm:"column(none_deadl_bal_ctrl_efft_dt);type(date);null" description:"无期限余额控制生效日期"`
	KeprcdStusCd           string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CrtEmpnbr              string    `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtDt                  string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	ApprvEmpnbr            string    `orm:"column(apprv_empnbr);size(6);null" description:"审批员工号  记录贷款审批流程中涉及的员工工号,"`
	ApprvDt                string    `orm:"column(apprv_dt);type(date);null" description:"审批日期"`
	ApprvSugstnCd          string    `orm:"column(apprv_sugstn_cd);size(3);null" description:"审批意见代码  1-同意,2-退回,3-终止,4-弃权,5-拒绝"`
	ApprvSugstnComnt       string    `orm:"column(apprv_sugstn_comnt);size(100);null" description:"审批意见说明"`
	FinlModfyDt            string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int        `orm:"column(tcc_state);null"`
}

func (t *T_lpr_org_sale_quota) TableName() string {
	return "t_lpr_org_sale_quota"
}

func InsertT_lpr_org_sale_quota(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_lpr_org_sale_quota).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_lpr_org_sale_quotaTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_lpr_org_sale_quota)).Filter("org_no", maps["OrgNo"]).
		Filter("seq_no", maps["SeqNo"]).Filter("loan_prodt_no", maps["LoanProdtNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_lpr_org_sale_quotaTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_lpr_org_sale_quota where org_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OrgNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_lpr_org_sale_quota(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_lpr_org_sale_quota panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_lpr_org_sale_quota))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_lpr_org_sale_quotaById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_lpr_org_sale_quota)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_lpr_org_sale_quota(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_lpr_org_sale_quota panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_lpr_org_sale_quota)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
