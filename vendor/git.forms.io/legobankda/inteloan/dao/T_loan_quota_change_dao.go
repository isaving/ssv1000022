package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_quota_change struct {
	ChgSn            string  `orm:"column(chg_sn);pk" description:"变更流水号"`
	CustNo           string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CrdtAplySn       string    `orm:"column(crdt_aply_sn);size(32);null" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	OperSorcCd       string    `orm:"column(oper_sorc_cd);size(1);null" description:"操作来源代码  O-联机,,B-批量"`
	OperBefQtaStusCd string    `orm:"column(oper_bef_qta_stus_cd);size(2);null" description:"操作前额度状态代码  01-正常02-冻结03-部分冻结04-失效05-终止13-冻结待审批14-解冻待审批15-调整待审批"`
	OperAftQtaStusCd string    `orm:"column(oper_aft_qta_stus_cd);size(2);null" description:"操作后额度状态代码  01-正常02-冻结03-部分冻结04-失效05-终止13-冻结待审批14-解冻待审批15-调整待审批"`
	QtaOperTypCd     string    `orm:"column(qta_oper_typ_cd);size(2);null" description:"额度操作类型代码  01-新建,02-提额,03-降额,04-冻结,05-解冻,06-领用,07-领用冲正,08-归还,09-归还冲正,10-更新"`
	CurCd            string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	ChgBefAvalQta    float64   `orm:"column(chg_bef_aval_qta);null;digits(18);decimals(2)" description:"变更前可用额度"`
	ChgAftQta        float64   `orm:"column(chg_aft_qta);null;digits(18);decimals(2)" description:"变更额度"`
	ChgAftAvalQta    float64   `orm:"column(chg_aft_aval_qta);null;digits(18);decimals(2)" description:"变更后可用额度"`
	CrtDt            string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtTelrNo        string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtOrgNo         string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_quota_change) TableName() string {
	return "t_loan_quota_change"
}

func InsertT_loan_quota_change(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_quota_change).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_quota_changeTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_quota_change)).Filter("chg_sn", maps["ChgSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_quota_changeTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_quota_change where chg_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ChgSn"]).Exec()
	return err
}

func QueryT_loan_quota_change(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_quota_change panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_quota_change))
	if params["Flag"] != nil && params["Flag"] == "9" {
		if _, err = qs.Filter("tcc_state", state).Filter("crdt_aply_sn", params["CrdtAplySn"]).
			OrderBy("-crt_dt", "-finl_modfy_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("-finl_modfy_dt","-finl_modfy_tm").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_quota_changeById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_quota_change)).Filter("chg_sn", maps["ChgSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_quota_change(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_quota_change panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_quota_change)).Filter("chg_sn", maps["ChgSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
