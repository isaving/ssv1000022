package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_curr_repay_info struct {
	LoanAcctNo       string    `orm:"column(loan_acct_no);size(32);pk" description:"贷款账号  与借据号一致同内部使用的合约编号"`
	AcctnDt          string    `orm:"column(acctn_dt);type(date)" description:"会计日期  记账的会计日期"`
	Pridnum          int       `orm:"column(pridnum)" description:"期数  分期还款计划的期数顺序号"`
	LoanAcctiAcctNo  string    `orm:"column(loan_accti_acct_no);size(32);null" description:"贷款核算账号 "`
	ChrgAcctNo       string    `orm:"column(chrg_acct_no);size(32);null" description:"扣款账号 "`
	AcctNm           string    `orm:"column(acct_nm);size(120);null" description:"账户名称 "`
	RepayDt          string    `orm:"column(repay_dt);type(date);null" description:"还款日期 "`
	CurCd            string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:cd0040"`
	PlanRepayTotlAmt float64   `orm:"column(plan_repay_totl_amt);null;digits(22);decimals(2)" description:"计划还款总金额 "`
	PlanRepayPrin    float64   `orm:"column(plan_repay_prin);null;digits(18);decimals(2)" description:"计划还款本金 "`
	PlanRepayIntr    float64   `orm:"column(plan_repay_intr);null;digits(18);decimals(2)" description:"计划还款利息 "`
	CoreChrgStusCd   string    `orm:"column(core_chrg_stus_cd);size(4);null" description:"核心扣款状态代码  1:扣款成功 2:部分扣款 3:扣款失败"`
	LoanRepayStusCd  string    `orm:"column(loan_repay_stus_cd);size(4);null" description:"贷款还款状态代码  1:还本还息成功 2:仅还本成功 3:仅还息成功 4:部分还本 5:部分还息 9:还款失败"`
	FailRsn		     string    `orm:"column(fail_rsn);size(20);null" description:"失败原因 "`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_curr_repay_info) TableName() string {
	return "t_loan_curr_repay_info"
}

func InsertT_loan_curr_repay_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_curr_repay_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_curr_repay_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_curr_repay_info)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("acctn_dt", maps["AcctnDt"]).Filter("pridnum", maps["Pridnum"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_curr_repay_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_curr_repay_info where loan_acct_no=? and acctn_dt=? and pridnum=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanAcctNo"], maps["AcctnDt"], maps["Pridnum"]).Exec()
	return err
}

func QueryT_loan_curr_repay_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_curr_repay_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_curr_repay_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_curr_repay_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_curr_repay_info)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("acctn_dt", maps["AcctnDt"]).Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_curr_repay_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_curr_repay_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_curr_repay_info)).Filter("loan_acct_no", maps["LoanAcctNo"]).
		Filter("acctn_dt", maps["AcctnDt"]).Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", 0).Update(maps)
	return err
}
