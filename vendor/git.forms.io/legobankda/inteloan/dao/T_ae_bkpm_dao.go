package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_ae_bkpm struct {
	AcctBookCategCd        string `orm:"column(acct_book_categ_cd);size(5);pk" description:"账套类别代码  PK,引擎变式编码:CN001-川农,CN002-预留1,CN003-预留2,CN004-预留3"`
	AcctBookStusCd         string `orm:"column(acct_book_stus_cd);size(1)" description:"账套状态代码"`
	AcctBookSeqNo          int    `orm:"column(acct_book_seq_no)" description:"账套序号"`
	SubjLength             int    `orm:"column(subj_length);null" description:"科目长度"`
	OnshetPngdgacctSubjNo  string `orm:"column(onshet_pngdgacct_subj_no);size(8);null" description:"表内挂账科目编号"`
	OffshetPngdgacctSubjNo string `orm:"column(offshet_pngdgacct_subj_no);size(8);null" description:"表外挂账科目编号"`
	UnDistrMarginSubjNo    string `orm:"column(un_distr_margin_subj_no);size(8);null" description:"未分配利润科目编号"`
	EngineVaritCd          string `orm:"column(engine_varit_cd);size(4);null" description:"引擎变式编码"`
	EngineVaritNm          string `orm:"column(engine_varit_nm);size(200);null" description:"引擎变式名称"`
	InterLiqdSubjNo        string `orm:"column(inter_liqd_subj_no);size(8);null" description:"内部清算科目编号"`
	EfftDt                 string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	InvalidDt              string `orm:"column(invalid_dt);type(date);null" description:"失效日期"`
	FinlModfyDt            string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState               int    `orm:"column(tcc_state);null"`
}

func (t *T_ae_bkpm) TableName() string {
	return "t_ae_bkpm"
}

func InsertT_ae_bkpm(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_ae_bkpm).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_ae_bkpmTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_ae_bkpm)).Filter("acct_book_categ_cd", maps["AcctBookCategCd"]).
		Filter("acct_book_stus_cd", maps["AcctBookStusCd"]).Filter("acct_book_seq_no", maps["AcctBookSeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_ae_bkpmTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_ae_bkpm where acct_book_categ_cd=? and acct_book_stus_cd=? and acct_book_seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctBookCategCd"], maps["AcctBookStusCd"], maps["AcctBookSeqNo"]).Exec()
	return err
}

func QueryT_ae_bkpm(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_ae_bkpm panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_ae_bkpm))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_ae_bkpmById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_ae_bkpm)).Filter("acct_book_categ_cd", maps["AcctBookCategCd"]).Filter("acct_book_stus_cd", maps["AcctBookStusCd"]).
		Filter("acct_book_seq_no", maps["AcctBookSeqNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_ae_bkpm(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_ae_bkpm panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_ae_bkpm)).Filter("acct_book_categ_cd", maps["AcctBookCategCd"]).Filter("acct_book_stus_cd", maps["AcctBookStusCd"]).
		Filter("acct_book_seq_no", maps["AcctBookSeqNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
