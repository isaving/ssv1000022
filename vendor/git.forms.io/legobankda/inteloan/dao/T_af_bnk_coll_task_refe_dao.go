package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_af_bnk_coll_task_refe struct {
	CollTaskNo       string `orm:"column(coll_task_no);size(30);pk" description:"催收任务编号"`
	SeqNo            int    `orm:"column(seq_no)" description:"序号"`
	GnrtDt           string `orm:"column(gnrt_dt);type(date);null" description:"生成日期"`
	CollMnrpEmpnbr   string `orm:"column(coll_mnrp_empnbr);size(6);null" description:"催收主责员工号"`
	CollMnrpOrgNo    string `orm:"column(coll_mnrp_org_no);size(4);null" description:"催收主责机构号"`
	TrnsfDt          string `orm:"column(trnsf_dt);type(date);null" description:"转派日期"`
	TrnsfManrCd      string `orm:"column(trnsf_manr_cd);size(30);null" description:"转派方式代码"`
	TsfinChnlNoCd    string `orm:"column(tsfin_chnl_no_cd);size(4);null" description:"转入渠道编号代码"`
	TsfinEmpnbr      string `orm:"column(tsfin_empnbr);size(6);null" description:"转入员工号"`
	TsfinOrgNo       string `orm:"column(tsfin_org_no);size(4);null" description:"转入机构号"`
	OperPersonEmpnbr string `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号"`
	OperOrgNo        string `orm:"column(oper_org_no);size(4);null" description:"操作机构号"`
	TrnsfStusCd      string `orm:"column(trnsf_stus_cd);size(1);null" description:"转派状态代码"`
	FinlModfyDt      string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_af_bnk_coll_task_refe) TableName() string {
	return "t_af_bnk_coll_task_refe"
}

func InsertT_af_bnk_coll_task_refe(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_af_bnk_coll_task_refe).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_af_bnk_coll_task_refeTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_af_bnk_coll_task_refe)).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_af_bnk_coll_task_refeTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_af_bnk_coll_task_refe where coll_task_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CollTaskNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_af_bnk_coll_task_refe(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_af_bnk_coll_task_refe panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_af_bnk_coll_task_refe))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_af_bnk_coll_task_refeById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	CollTaskNo := params["CollTaskNo"]
	SeqNo := params["SeqNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_af_bnk_coll_task_refe)).Filter("coll_task_no", CollTaskNo).Filter("seq_no", SeqNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_af_bnk_coll_task_refe(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_af_bnk_coll_task_refe panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	CollTaskNo := params["CollTaskNo"]
	SeqNo := params["SeqNo"]
	_, err = o.QueryTable(new(T_af_bnk_coll_task_refe)).Filter("coll_task_no", CollTaskNo).Filter("seq_no", SeqNo).Filter("tcc_state", 0).Update(params)
	return err
}
