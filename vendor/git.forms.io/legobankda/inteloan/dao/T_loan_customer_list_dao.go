package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_customer_list struct {
	CustNo          string    `orm:"column(cust_no);pk" description:"客户编号  PK"`
	CustName        string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd   string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo      string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	FsttmBizDt      string    `orm:"column(fsttm_biz_dt);type(date);null" description:"首次业务日期"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_customer_list) TableName() string {
	return "t_loan_customer_list"
}

func InsertT_loan_customer_list(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_customer_list).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_customer_listTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_customer_list)).Filter("cust_no", maps["CustNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_customer_listTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_customer_list where cust_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustNo"]).Exec()
	return err
}

func QueryT_loan_customer_list(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_customer_list panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_customer_list))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_customer_listById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_customer_list)).Filter("cust_no", maps["CustNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_customer_list(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_customer_list panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_customer_list)).Filter("cust_no", maps["CustNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
