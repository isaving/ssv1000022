package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_earl_warn_sign_task struct {
	WarnTaskNo          string    `orm:"column(warn_task_no);pk" description:"预警任务编号"`
	LoanDubilNo         string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	CustNo              string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName            string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd       string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo          string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	OrgNo               string    `orm:"column(org_no);size(4);null" description:"机构号"`
	OrgNm               string    `orm:"column(org_nm);size(120);null" description:"机构名称"`
	CrtDt               string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	TaskDlwthStusCd     string    `orm:"column(task_dlwth_stus_cd);size(2);null" description:"任务处理状态代码  01-待处理02-转派待处理03-转派已处理04-转派已确认05-已过期09-已处理11-待整改12-待评价13-已评价14-处理成功15-处理失败21-转派确认22-转派废除23-评价通过24-评价退回"`
	WarnSponsorEmpnbr   string    `orm:"column(warn_sponsor_empnbr);size(6);null" description:"预警主办员工号"`
	WarnSponsorOrgNo    string    `orm:"column(warn_sponsor_org_no);size(4);null" description:"预警主办机构号"`
	WarnCorgEmpnbr      string    `orm:"column(warn_corg_empnbr);size(6);null" description:"预警协办员工号"`
	WarnCorgOrgNo       string    `orm:"column(warn_corg_org_no);size(4);null" description:"预警协办机构号"`
	AlrdyExecFlg        string    `orm:"column(alrdy_exec_flg);size(1);null" description:"已执行标志  0否1是"`
	AtExecWindowFlg     string    `orm:"column(at_exec_window_flg);size(1);null" description:"在执行窗口标志  0否1是"`
	ExecTms             int       `orm:"column(exec_tms);null" description:"执行次数"`
	ExecConcusCd        string    `orm:"column(exec_concus_cd);size(4);null" description:"执行结论代码  预警解除/维持/升级"`
	ExecRsn             string    `orm:"column(exec_rsn);size(200);null" description:"执行原因"`
	CtrtNo              string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	SponsorEmplyName    string    `orm:"column(sponsor_emply_name);size(60);null" description:"主办员工姓名"`
	CorgEmplyName       string    `orm:"column(corg_emply_name);size(60);null" description:"协办员工姓名"`
	SponsorOrgNm        string    `orm:"column(sponsor_org_nm);size(120);null" description:"主办机构名称"`
	CorgOrgNm           string    `orm:"column(corg_org_nm);size(120);null" description:"协办机构名称"`
	SponsorEmplyTaskNo  string    `orm:"column(sponsor_emply_task_no);size(30);null" description:"主办员工任务编号"`
	CorgEmplyTaskNo     string    `orm:"column(corg_emply_task_no);size(30);null" description:"协办员工任务编号"`
	TaskDlwthResultNo   string    `orm:"column(task_dlwth_result_no);size(32);null" description:"任务处理结果编号"`
	WarnSignalTypCd     string    `orm:"column(warn_signal_typ_cd);size(2);null" description:"预警信号类型代码  11-短信提醒,,12-电核,,13-额度调整,,14-额度冻结"`
	AdjQtaDlwthResultCd string    `orm:"column(adj_qta_dlwth_result_cd);size(2);null" description:"调整额度处理结果代码  01-确认,02-拒绝"`
	DlwthDt             string    `orm:"column(dlwth_dt);type(date);null" description:"处理日期"`
	FinlModfyDt         string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState               int    `orm:"column(tcc_state);null"`
}

func (t *T_earl_warn_sign_task) TableName() string {
	return "t_earl_warn_sign_task"
}

func InsertT_earl_warn_sign_task(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_earl_warn_sign_task).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_earl_warn_sign_taskTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_earl_warn_sign_task)).Filter("warn_task_no", maps["WarnTaskNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_earl_warn_sign_taskTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_earl_warn_sign_task where warn_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["WarnTaskNo"]).Exec()
	return err
}

func QueryT_earl_warn_sign_task(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_earl_warn_sign_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	if params["Flag"] == "9" {
		maps, err = QueryT_earl_warn_sign_taskFlag9(params, 0)
		return
	}
	if params["Flag"] == "8" {
		maps, err = QueryT_earl_warn_sign_taskFlag8(params, 0)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_earl_warn_sign_task))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "StartCrtDt" {
				qs = qs.Filter("crt_dt__gte", v)
			}else if k == "EndCrtDt" {
				qs = qs.Filter("crt_dt__lte", v)
			}else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_earl_warn_sign_taskById(params orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	WarnTaskNo := params["WarnTaskNo"]
	maps := []orm.Params{}
	if _, err = o.QueryTable(new(T_earl_warn_sign_task)).Filter("warn_task_no", WarnTaskNo).Filter("tcc_state", state).Values(&maps); err != nil {
		return nil, err
	}
	if len(maps) > 0 {
		return maps[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_earl_warn_sign_task(params orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_earl_warn_sign_task panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	WarnTaskNo := params["WarnTaskNo"]
	_, err = o.QueryTable(new(T_earl_warn_sign_task)).Filter("warn_task_no", WarnTaskNo).Filter("tcc_state", 0).Update(params)
	return err
}

func QueryT_earl_warn_sign_taskFlag9(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_earl_warn_sign_task))
	if params["Flag"] == "9" {
		SortDateFlg := params["SortDateFlg"]
		delete(params, "Flag")
		delete(params, "SortDateFlg")
		for k, v := range params {
			if k != constant.PageRecCount && k != constant.PageNo {
				if k == "StartDate" {
					qs = qs.Filter("crt_dt__gte", v)
				} else if k == "EndDate" {
					qs = qs.Filter("crt_dt__lte", v)
				} else {
					qs = qs.Filter(k, v)
				}
			}
		}
		if SortDateFlg == "1" {
			_, err = qs.Filter("tcc_state", state).OrderBy("crt_dt").Values(&maps)
		} else {
			_, err = qs.Filter("tcc_state", state).OrderBy("-crt_dt").Values(&maps)
		}
		if err != nil {
			return nil, err
		}
		return
	}
	return
}
func QueryT_earl_warn_sign_taskFlag8(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql :="SELECT warn_task_no WarnTaskNo,"+
		"t1.loan_dubil_no LoanDubilNo," +
		"t1.ctrt_no CtrtNo," +
		"t1.cust_no CustNo," +
		"t1.cust_name CustName," +
		"t1.crt_dt CrtDt," +
		"t1.warn_signal_typ_cd SggestMgmtManrCd," +
		"t1.task_dlwth_stus_cd WarnSignalTaskDlwthStusCd," +
		"t1.warn_sponsor_empnbr WarnSponsorEmpnbr," +
		"t1.warn_sponsor_org_no WarnSponsorOrgNo," +
		"t1.warn_task_no TaskExampId FROM t_earl_warn_sign_task t1 LEFT JOIN t_loan_approve_task t2 ON t1.`warn_task_no`=t2.`biz_id`" +
		"  WHERE 1=1 "
	//sql :="SELECT biz_id FROM t_loan_approve_task WHERE tcc_state=0 "
	if params["TaskDlwthStusCd"] !=nil{
		sql = sql +"and t1.task_dlwth_stus_cd=\""+params["TaskDlwthStusCd"].(string)+"\""
	}
	if params["CustNo"] !=nil{
		sql = sql +"and t1.cust_no=\""+params["CustNo"].(string)+"\""
	}
	if params["CrtDt1"] !=nil{
		sql = sql +"and t1.crt_dt>=\""+params["CrtDt1"].(string)+"\""
	}
	if params["CrtDt2"] !=nil{
		sql = sql +"and t1.crt_dt<=\""+params["CrtDt2"].(string)+"\""
	}
	//if params["DistrPersonEmpnbr"] !=nil{
	//	sql = sql +"and t1.distr_person_empnbr=\""+params["DistrPersonEmpnbr"].(string)+"\""
	//}
	if params["SggestMgmtManrCd"] != nil {
		sql =sql +" and t1.warn_signal_typ_cd=\""+params["SggestMgmtManrCd"].(string)+"\""
	}
	_,err =o.Raw(sql).Values(&maps)
	return
}