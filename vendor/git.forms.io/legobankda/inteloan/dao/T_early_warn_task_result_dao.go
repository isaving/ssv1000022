package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_early_warn_task_result struct {
	TaskDlwthResultNo        string    `orm:"column(task_dlwth_result_no);pk" description:"任务处理结果编号"`
	WarnTaskNo               string    `orm:"column(warn_task_no);size(30);null" description:"预警任务编号"`
	SeqNo                    int       `orm:"column(seq_no);null" description:"序号"`
	CustNo                   string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	HndlrEmpnbr              string    `orm:"column(hndlr_empnbr);size(6);null" description:"处理人员工号"`
	ReacCustManrCd           string    `orm:"column(reac_cust_manr_cd);size(20);null" description:"触达客户方式代码  1-致电2-短信3-社交软件4-拜访本人面谈5-拜访相关人面谈6-其他"`
	KeprcdStusCd             string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CorgDlwthResultCd        string    `orm:"column(corg_dlwth_result_cd);size(1);null" description:"协办处理结果代码  1-待确认,2-已确认,3-已废除"`
	CollTms                  int       `orm:"column(coll_tms);null" description:"触达客户次数"`
	WarnSignalDlwthResultCd  string    `orm:"column(warn_signal_dlwth_result_cd);size(30);null" description:"预警信号处理结果代码  (多选项,可并存)1未接通2未回复3失约未见4变更还款计划5追加担保6同意部分提前归还7同意全部提前归还8拒绝沟通或解释9拒绝提前归还A其他"`
	WarnDlwthRemrk           string    `orm:"column(warn_dlwth_remrk);size(2000);null" description:"预警处理备注"`
	CotactTelNo              string    `orm:"column(cotact_tel_no);size(20);null" description:"联系电话号码"`
	CurrAddr                 string    `orm:"column(curr_addr);size(200);null" description:"当前地址"`
	Lgtd                     float64   `orm:"column(lgtd);null;digits(11);decimals(8)" description:"经度"`
	LgtdDrctCd               string    `orm:"column(lgtd_drct_cd);size(1);null" description:"经度方向代码  E东W西"`
	Lttd                     float64   `orm:"column(lttd);null;digits(11);decimals(8)" description:"纬度"`
	LttdDrctCd               string    `orm:"column(lttd_drct_cd);size(1);null" description:"纬度方向代码  N北S南"`
	UpdAftRsdnceScAdcmCd     string    `orm:"column(upd_aft_rsdnce_sc_adcm_cd);size(12);null" description:"更新后居住地四川省行政区划代码  参考标准代码:CD0153"`
	UpdAftRsdnceDtlAddr      string    `orm:"column(upd_aft_rsdnce_dtl_addr);size(200);null" description:"更新后居住地详细地址"`
	UpdAftRsdnceAddr         string    `orm:"column(upd_aft_rsdnce_addr);size(200);null" description:"更新后居住地地址"`
	UpdAftCmunicAddrScAdcmCd string    `orm:"column(upd_aft_cmunic_addr_sc_adcm_cd);size(12);null" description:"更新后通讯地址四川省行政区划代码  参考标准代码:CD0153"`
	UpdAftCmunicDtlAddr      string    `orm:"column(upd_aft_cmunic_dtl_addr);size(200);null" description:"更新后通讯详细地址"`
	UpdAftCmunicAddr         string    `orm:"column(upd_aft_cmunic_addr);size(200);null" description:"更新后通讯地址"`
	UpdGrndPostCd            string    `orm:"column(upd_grnd_post_cd);size(6);null" description:"更新地邮政编码"`
	OneslfCmtdInfo           string    `orm:"column(oneslf_cmtd_info);size(1000);null" description:"本人承诺信息"`
	MrgncyConterName         string    `orm:"column(mrgncy_conter_name);size(60);null" description:"紧急联系人姓名"`
	PtyRelKindCd             string    `orm:"column(pty_rel_kind_cd);size(3);null" description:"当事人关系种类代码  00-配偶10-父母11-父亲12-母亲20-子女21-儿子22-女儿30-兄弟姐妹41-祖父母42-外祖父母51-朋友52-同事99-其它"`
	MrgncyConterTelNo        string    `orm:"column(mrgncy_conter_tel_no);size(20);null" description:"紧急联系人电话号码"`
	SponsorEmpnbr            string    `orm:"column(sponsor_empnbr);size(6);null" description:"主办员工号"`
	SponsorOrgNo             string    `orm:"column(sponsor_org_no);size(4);null" description:"主办机构号"`
	CorgEmpnbr               string    `orm:"column(corg_empnbr);size(6);null" description:"协办员工号"`
	CorgOrgNo                string    `orm:"column(corg_org_no);size(4);null" description:"协办机构号"`
	DlwthDt                  string    `orm:"column(dlwth_dt);type(date);null" description:"处理日期"`
	FinlModfyDt              string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  10"`
	FinlModfyTm              string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  19"`
	FinlModfyOrgNo           string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo          string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                 int8      `orm:"column(tcc_state);null"`
}

func (t *T_early_warn_task_result) TableName() string {
	return "t_early_warn_task_result"
}

func InsertT_early_warn_task_result(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_early_warn_task_result).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_early_warn_task_resultTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_early_warn_task_result)).Filter("task_dlwth_result_no", maps["TaskDlwthResultNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_early_warn_task_resultTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_early_warn_task_result where task_dlwth_result_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TaskDlwthResultNo"]).Exec()
	return err
}

func QueryT_early_warn_task_result(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_early_warn_task_result panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_early_warn_task_result))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_early_warn_task_resultById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_early_warn_task_result)).Filter("task_dlwth_result_no", maps["TaskDlwthResultNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_early_warn_task_result(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_early_warn_task_result panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_early_warn_task_result)).Filter("task_dlwth_result_no", maps["TaskDlwthResultNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
