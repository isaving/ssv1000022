package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_addr_match_org struct {
	KeprcdNo                 string    `orm:"column(keprcd_no);sice(11);pk;" description:"记录编号  技术字段"`
	SeqNo                    int       `orm:"column(seq_no);null" description:"序号"`
	ProvAdminCmprmntCd       string    `orm:"column(prov_admin_cmprmnt_cd);size(9);null" description:"省行政区划代码  与行政区划代码对应"`
	PrvMncpltyAdminCmprmntNm string    `orm:"column(prv_mncplty_admin_cmprmnt_nm);size(120);null" description:"省或直辖市行政区划名称  与行政区划代码对应"`
	CityAdminCmprmntCd       string    `orm:"column(city_admin_cmprmnt_cd);size(9);null" description:"市行政区划代码  与行政区划代码对应"`
	CityAdminCmprmntNm       string    `orm:"column(city_admin_cmprmnt_nm);size(120);null" description:"市行政区划名称  与行政区划代码对应"`
	ZoneOrCntyAdminCmprmntCd string    `orm:"column(zone_or_cnty_admin_cmprmnt_cd);size(9);null" description:"区或县行政区划代码  与行政区划代码对应"`
	ZoneOrCntyAdminCmprmntNm string    `orm:"column(zone_or_cnty_admin_cmprmnt_nm);size(120);null" description:"区或县行政区划名称  与行政区划代码对应"`
	LprOrgNo                 string    `orm:"column(lpr_org_no);size(4);null" description:"法人机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	CrtTelrNo                string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtDt                    string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtOrgNo                 string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	KeprcdStusCd             string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	FinlModfyDt              string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm              string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo           string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo          string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                 int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_addr_match_org) TableName() string {
	return "t_addr_match_org"
}

func InsertT_addr_match_org(o orm.Ormer, maps orm.Params) error {
	delete(maps, "Flag")
	sql := util.BuildSql(maps, new(T_addr_match_org).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_addr_match_orgTccState(o orm.Ormer, maps orm.Params) error {
	if maps["Flag"] != nil && maps["Flag"] == "9" {
		delete(maps, "Flag")
		_, err := o.QueryTable(new(T_addr_match_org)).Filter("prv_mncplty_admin_cmprmnt_nm", maps["PrvMncpltyAdminCmprmntNm"]).
			Filter("city_admin_cmprmnt_nm", maps["CityAdminCmprmntNm"]).
			Filter("zone_or_cnty_admin_cmprmnt_nm", maps["ZoneOrCntyAdminCmprmntNm"]).
			Filter("lpr_org_no", maps["LprOrgNo"]).Update(orm.Params{"tcc_state": 0})
		return err
	}
	_, err := o.QueryTable(new(T_addr_match_org)).Filter("keprcd_no", maps["KeprcdNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_addr_match_orgTccState(o orm.Ormer, maps orm.Params) error {
	if maps["Flag"] != nil && maps["Flag"] == "9" {
		delete(maps, "Flag")
		sql := "delete from t_addr_match_org where prov_admin_cmprmnt_cd=? and city_admin_cmprmnt_cd=? zone_or_cnty_admin_cmprmnt_cd=? and crt_telr_no=? and tcc_state = 1"
		_, err := o.Raw(sql, maps["ProvAdminCmprmntCd"], maps["CityAdminCmprmntCd"], maps["ZoneOrCntyAdminCmprmntCd"], maps["CrtTelrNo"]).Exec()
		return err
	}
	sql := "delete from t_addr_match_org where keprcd_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_addr_match_org(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_addr_match_org panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_addr_match_org))
	flag := params["Flag"]
	delete(params,"Flag")
	if flag != nil && flag.(string) == "9" {
		sql := "SELECT * FROM t_addr_match_org WHERE  tcc_state = 0 AND ( keprcd_stus_cd != 09 OR keprcd_stus_cd IS NULL ) "
		if params["PrvMncpltyAdminCmprmntNm"] != nil {
			sql = sql + " and prv_mncplty_admin_cmprmnt_nm=\"" + params["PrvMncpltyAdminCmprmntNm"].(string) + "\""
		}
		if params["CityAdminCmprmntNm"] != nil {
			sql = sql + " and city_admin_cmprmnt_nm=\"" + params["CityAdminCmprmntNm"].(string) + "\""
		}
		if params["ZoneOrCntyAdminCmprmntNm"] != nil {
			sql = sql + " and zone_or_cnty_admin_cmprmnt_nm=\"" + params["ZoneOrCntyAdminCmprmntNm"].(string) + "\""
		}
		if params["LprOrgNo"] != nil {
			sql = sql + " and lpr_org_no=\"" + params["LprOrgNo"].(string) + "\""
		}
		_, err = o.Raw(sql).Values(&maps)
		//_, err = qs.Exclude("keprcd_stus_cd", "09").Filter("tcc_state", state).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "8"{
		if _, err = qs.Filter("tcc_state", state).Exclude("keprcd_stus_cd","09").Limit(-1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("-finl_modfy_dt","-finl_modfy_tm").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_addr_match_orgById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_addr_match_org)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_addr_match_org(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_addr_match_org panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_addr_match_org)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_addr_match_org1(params orm.Params, state int8) (maps []orm.Params, err error) {
	delete(params, "Flag")

	o := orm.NewOrm()
	qs := o.QueryTable(new(T_addr_match_org))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Exclude("keprcd_stus_cd", "09").Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}
