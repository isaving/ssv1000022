package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_kernel_fin_txn_flow struct {
	BizSn	          string    `orm:"column(biz_sn);sicze(32);pk" description:"业务流水号"`
	TxAcctnDt         string    `orm:"column(tx_acctn_dt);type(date)" description:"交易会计日期"`
	TxTm              string    `orm:"column(tx_tm);type(time);null" description:"交易时间"`
	BizFolnNo         string    `orm:"column(biz_foln_no);size(32)" description:"业务跟踪号  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	SysFolnNo         string    `orm:"column(sys_foln_no);size(32)" description:"系统跟踪号  标识一笔渠道接入交易在四川农信内部各系统之间交互的唯一识别流水号,32位组成为:客户端接入软件产品编码3位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号15位"`
	TxOrgNo           string    `orm:"column(tx_org_no);size(4);null" description:"交易机构号"`
	TxTelrNo          string    `orm:"column(tx_telr_no);size(6);null" description:"交易柜员号"`
	AuthTelrNo        string    `orm:"column(auth_telr_no);size(6);null" description:"授权柜员号"`
	TxLunchChnlCd     string    `orm:"column(tx_lunch_chnl_cd);size(4);null" description:"交易发起渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表"`
	AccessChnlCd      string    `orm:"column(access_chnl_cd);size(4);null" description:"接入渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表"`
	BizSysSoftProdtCd string    `orm:"column(biz_sys_soft_prodt_cd);size(3);null" description:"业务系统软件产品编码  三位字母的系统编码,参考'软件产品列表'2018年10月版'_V0.18_20190806'"`
	DebitCrdtFlg      string    `orm:"column(debit_crdt_flg);size(1);null" description:"借贷标志"`
	CardNoOrAcctNo    string    `orm:"column(card_no_or_acct_no);size(32);null" description:"卡号或账号"`
	CntptyAcctNo      string    `orm:"column(cntpty_acct_no);size(32);null" description:"对方账号"`
	AcctiAcctNo       string    `orm:"column(accti_acct_no);size(32);null" description:"核算账号"`
	TxCurCd           string    `orm:"column(tx_cur_cd);size(3);null" description:"交易币种代码  参考标准代码:CD0040"`
	Amount            float64   `orm:"column(amount);null;digits(18);decimals(2)" description:"发生额"`
	OrginlTxAcctnDt   string    `orm:"column(orginl_tx_acctn_dt);type(date);null" description:"原交易会计日期"`
	OrginlBizFolnNo   string    `orm:"column(orginl_biz_foln_no);size(32);null" description:"原业务跟踪号  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	OrginlTxTelrNo    string    `orm:"column(orginl_tx_telr_no);size(6);null" description:"原交易柜员号"`
	TxStusCd          string    `orm:"column(tx_stus_cd);size(4);null" description:"交易状态代码"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_kernel_fin_txn_flow) TableName() string {
	return "t_kernel_fin_txn_flow"
}

func InsertT_kernel_fin_txn_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_kernel_fin_txn_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_kernel_fin_txn_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_kernel_fin_txn_flow)).Filter("biz_sn", maps["BizSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_kernel_fin_txn_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_kernel_fin_txn_flow where biz_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizSn"]).Exec()
	return err
}

func QueryT_kernel_fin_txn_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_kernel_fin_txn_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_kernel_fin_txn_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_kernel_fin_txn_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_kernel_fin_txn_flow)).Filter("biz_sn", maps["BizSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_kernel_fin_txn_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_kernel_fin_txn_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_kernel_fin_txn_flow)).Filter("biz_sn", maps["BizSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
