package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_adj_contract_reg struct {
	CtrtNo         string    `orm:"column(ctrt_no);pk" description:"合同号"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	IndvCrtfTypCd   string    `orm:"column(indv_crtf_typ_cd);size(3);null" description:"个人证件类型代码"`
	IndvCrtfNo      string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CtrtStusCd      string    `orm:"column(ctrt_stus_cd);size(5);null" description:"合同状态代码 A1-合同录入 A2-合同审查中 A3-合同审查完毕 A4-合同生效 A5-合同冻结 A6-合同中止 A7-合同执行完毕 A8-合同解冻审批中 A9-合同冻结审批中 A0-合同新增"`
	CtrtStartDt     string    `orm:"column(ctrt_start_dt);type(date);null" description:"合同起始日期"`
	CtrtMatrDt      string    `orm:"column(ctrt_matr_dt);type(date);null" description:"合同到期日期"`
	ApprvDt         string    `orm:"column(apprv_dt);type(date);null" description:"审批日期"`
	SignOrgNo       string    `orm:"column(sign_org_no);size(20);null" description:"签约机构号"`
	OprOrgNo        string    `orm:"column(opr_org_no);size(20);null" description:"经办机构号"`
	OprorEmpnbr     string    `orm:"column(opror_empnbr);size(20);null" description:"经办人员工号"`
	ComslNo         string    `orm:"column(comsl_no);size(20);null" description:"公章编号"`
	TmplNo          string    `orm:"column(tmpl_no);size(32);null" description:"模板编号"`
	CtrtImgNo       string    `orm:"column(ctrt_img_no);size(40);null" description:"合同影像编号"`
	BrwmnyCtrtTypCd string    `orm:"column(brwmny_ctrt_typ_cd);size(1);null" description:"借款合同类型代码 1-非循环  2-循环"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState        int       `orm:"column(tcc_state);null"`
}

func (t *T_adj_contract_reg) TableName() string {
	return "t_adj_contract_reg"
}

func InsertT_adj_contract_reg(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_adj_contract_reg).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_adj_contract_regTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_adj_contract_reg)).Filter("ctrt_no", maps["CtrtNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_adj_contract_regTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_adj_contract_reg where ctrt_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CtrtNo"]).Exec()
	return err
}

func QueryT_adj_contract_reg(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_adj_contract_reg panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_adj_contract_reg))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_adj_contract_regById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_adj_contract_reg)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_adj_contract_reg(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_adj_contract_reg panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_adj_contract_reg)).Filter("ctrt_no", maps["CtrtNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
