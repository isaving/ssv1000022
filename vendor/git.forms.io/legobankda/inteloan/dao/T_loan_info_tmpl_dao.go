package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_info_tmpl struct {
	TmplNo            string    `orm:"column(tmpl_no);pk" description:"模板编号"`
	EfftDt            string    `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	LoanProdtNo       string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	SuitHavegProdtFlg string    `orm:"column(suit_haveg_prodt_flg);size(1);null" description:"适用所有产品标志"`
	TmplNm            string    `orm:"column(tmpl_nm);size(400);null" description:"模板名称"`
	TmplContent       string    `orm:"column(tmpl_content);size(200);null" description:"模板内容"`
	TmplComnt         string    `orm:"column(tmpl_comnt);size(200);null" description:"模板说明"`
	TextTmplTypCd     string    `orm:"column(text_tmpl_typ_cd);size(2);null" description:"文本模板类型代码  01-授信额度合同模板02-个人征信授权书模板03-个人信息查询及使用授权书模板04-公积金授权书模板05-涉税信息授权委托书模板06-蜀信e贷个人借款合同模板20191205更新:1-营销-未获取额度2-营销-白名单营销3-营销-已获取额度4-审批结果-成功且已开卡5-审批结果-成功且未开卡6-审批结果-失败7-提额-成功8-提额-失败9-放款-到账通知10-到期提醒-月供到期11-到期提醒-本金到期12-催收提醒13-贷中额度调整-上调14-贷中额度调整-下调15-贷中额度调整-冻结16-贷中额度调整-恢复17-贷中预警通知-客户经理"`
	PmitSndFlg        string    `orm:"column(pmit_snd_flg);size(1);null" description:"允许发送标志"`
	AdvSndDays        int       `orm:"column(adv_snd_days);null" description:"提前发送天数"`
	KeprcdStusCd      string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	SndBgnTm          string    `orm:"column(snd_bgn_tm);type(time);null" description:"发送开始时间"`
	SndEndTm          string    `orm:"column(snd_end_tm);type(time);null" description:"发送结束时间"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState          int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_info_tmpl) TableName() string {
	return "t_loan_info_tmpl"
}

func InsertT_loan_info_tmpl(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_info_tmpl).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_info_tmplTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_info_tmpl)).Filter("tmpl_no", maps["TmplNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_info_tmplTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_info_tmpl where tmpl_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TmplNo"]).Exec()
	return err
}

func QueryT_loan_info_tmpl(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_info_tmpl panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_info_tmpl))
	flag := params["Flag"]
	if flag != nil && flag.(string) == "9" {
		sql := "SELECT * FROM t_loan_info_tmpl WHERE  tcc_state = 0 AND ( keprcd_stus_cd != 9 OR keprcd_stus_cd IS NULL ) "
		if params["TmplNo"] != nil {
			sql = sql + " and tmpl_no=\"" + params["TmplNo"].(string) + "\""
		}
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_info_tmplById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_info_tmpl)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_info_tmpl(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_info_tmpl panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_info_tmpl)).Filter("tmpl_no", maps["TmplNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
