package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_contact struct {
	MakelnAplySn    string `orm:"column(makeln_aply_sn);size(32);pk" description:"授信申请流水号  n "`
	SeqNo           int    `orm:"column(seq_no)" description:"序号  n "`
	CustNo    		string `orm:"column(cust_no);size(14);null" description:客户编号 "`
	ConterCustNo    string `orm:"column(conter_cust_no);size(14);null" description:"联系人客户编号 "`
	ConterNm        string `orm:"column(conter_nm);size(60);null" description:"联系人名称 "`
	ConterTelNo     string `orm:"column(conter_tel_no);size(20);null" description:"联系人电话号码  记录重要联系人的联系电话"`
	KinRelCd        string `orm:"column(kin_rel_cd);size(2);null" description:"亲属关系代码  1:父母 2:子女 3:配偶 4:朋友 5:同事 6:兄弟 7:兄妹 8:其他亲属"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  n "`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  n "`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号  n "`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号  n "`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_contact) TableName() string {
	return "t_loan_contact"
}

func InsertT_loan_contact(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_contact).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_contactTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_contact)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_contactTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_contact where makeln_aply_sn=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["MakelnAplySn"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_contact(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_contact panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_contact))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_contactById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_contact)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_contact(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_contact panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_contact)).Filter("makeln_aply_sn", maps["MakelnAplySn"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
