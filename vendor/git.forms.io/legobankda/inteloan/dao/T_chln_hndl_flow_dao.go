package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_chln_hndl_flow struct {
	LoanDubilNo      string    `orm:"column(loan_dubil_no);pk;size(32)" description:"贷款借据号"`
	TxSn             string    `orm:"column(tx_sn);size(32)" description:"交易流水号"`
	CustNo           string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	NodeNm           string    `orm:"column(node_nm);size(14);null" description:"节点名称"`
	BizAplyNo        string    `orm:"column(biz_aply_no);size(20);null" description:"业务申请编号"`
	BizTypCd         string    `orm:"column(biz_typ_cd);size(1);null" description:"业务代码类型 1-利率变更 2-还款计划变更 3-还款账户变更 4-提前到期 5-展期"`
	OperPersonEmpnbr string    `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号"`
	OperOrgNo        string    `orm:"column(oper_org_no);size(4);null" description:"操作机构号"`
	OperTm           string    `orm:"column(oper_tm);type(timestamp);null" description:"操作时间"`
	ApprvSugstnCd    string    `orm:"column(apprv_sugstn_cd);size(3);null" description:"审批意见代码 1-同意 2-不同意 3-退回 4-弃权"`
	SugstnDescr      string    `orm:"column(sugstn_descr);size(200);null" description:"意见描述"`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState         int       `orm:"column(tcc_state);null"`
}

func (t *T_chln_hndl_flow) TableName() string {
	return "t_chln_hndl_flow"
}

func InsertT_chln_hndl_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_chln_hndl_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_chln_hndl_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_chln_hndl_flow)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("tx_sn", maps["TxSn"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_chln_hndl_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_chln_hndl_flow where loan_dubil_no=? and tx_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["TxSn"]).Exec()
	return err
}

func QueryT_chln_hndl_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_chln_hndl_flow panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params,"Flag")
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_chln_hndl_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9"{
		if _, err = qs.Filter("tcc_state", state).OrderBy("-tx_sn").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if flag == "8"{
		if _, err = qs.Filter("tcc_state", state).OrderBy("-oper_tm").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_chln_hndl_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_chln_hndl_flow)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_chln_hndl_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_chln_hndl_flow panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_chln_hndl_flow)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
