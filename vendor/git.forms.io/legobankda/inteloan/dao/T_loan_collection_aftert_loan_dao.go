package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_collection_aftert_loan struct {
	CollTaskNo         string    `orm:"column(coll_task_no);pk" description:"催收任务编号"`
	LoanDubilNo        string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	CustNo             string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName           string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd      string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo         string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	OrgNo              string    `orm:"column(org_no);size(4);null" description:"机构号"`
	OrgNm              string    `orm:"column(org_nm);size(120);null" description:"机构名称"`
	CrtDt              string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	TaskDlwthStusCd    string    `orm:"column(task_dlwth_stus_cd);size(2);null" description:"任务处理状态代码  01-待处理02-转派待处理03-转派已处理04-转派已确认05-已过期09-已处理11-待整改12-待评价13-已评价14-处理成功15-处理失败21-转派确认22-转派废除23-评价通过24-评价退回"`
	AlrdyExecFlg       string    `orm:"column(alrdy_exec_flg);size(1);null" description:"已执行标志  0否1是"`
	AtExecWindowFlg    string    `orm:"column(at_exec_window_flg);size(1);null" description:"在执行窗口标志  0否1是"`
	ExecTms            int       `orm:"column(exec_tms);null" description:"执行次数"`
	ExecRsn            string    `orm:"column(exec_rsn);size(200);null" description:"执行原因"`
	ExecConcusComnt    string    `orm:"column(exec_concus_comnt);size(200);null" description:"执行结论说明"`
	CollSponsorEmpnbr  string    `orm:"column(coll_sponsor_empnbr);size(6);null" description:"催收主办员工号"`
	CollSponsorOrgNo   string    `orm:"column(coll_sponsor_org_no);size(4);null" description:"催收主办机构号"`
	CollCorgEmpnbr     string    `orm:"column(coll_corg_empnbr);size(6);null" description:"催收协办员工号"`
	CollCorgOrgNo      string    `orm:"column(coll_corg_org_no);size(4);null" description:"催收协办机构号"`
	CtrtNo             string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	ReacTotlTms        int       `orm:"column(reac_totl_tms);null" description:"触达总次数"`
	DlwthDt            string    `orm:"column(dlwth_dt);type(date);null" description:"处理日期"`
	SponsorEmplyTaskNo string    `orm:"column(sponsor_emply_task_no);size(30);null" description:"主办员工任务编号"`
	CorgEmplyTaskNo    string    `orm:"column(corg_emply_task_no);size(30);null" description:"协办员工任务编号"`
	TaskDlwthResultNo  string    `orm:"column(task_dlwth_result_no);size(32);null" description:"任务处理结果编号"`
	SponsorEmplyName   string    `orm:"column(sponsor_emply_name);size(60);null" description:"主办员工姓名 "`
	CorgEmplyName      string    `orm:"column(corg_emply_name);size(60);null" description:"协办员工姓名 "`
	SponsorOrgNm       string    `orm:"column(sponsor_org_nm);size(120);null" description:"主办机构名称 "`
	CorgOrgNm          string    `orm:"column(corg_org_nm);size(120);null" description:"协办机构名称 "`
	ReacCustTms        int       `orm:"column(reac_cust_tms);null" description:"触达客户次数"`
	FinlModfyDt        string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  19"`
	FinlModfyTm        string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo     string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo    string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState            int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_collection_aftert_loan) TableName() string {
	return "t_loan_collection_aftert_loan"
}

func InsertT_loan_collection_aftert_loan(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_collection_aftert_loan).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_collection_aftert_loanTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_collection_aftert_loan)).Filter("coll_task_no", maps["CollTaskNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_collection_aftert_loanTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_collection_aftert_loan where coll_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CollTaskNo"]).Exec()
	return err
}

func QueryT_loan_collection_aftert_loan(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_collection_aftert_loan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	if params["Flag"] == "9" {
		maps, err = QueryT_loan_collection_aftert_loanFlag9(params, 0)
		return
	}
	if params["Flag"] == "8" {
		maps, err = QueryT_loan_collection_aftert_loanFlag8(params, 0)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_collection_aftert_loan))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_collection_aftert_loanById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_collection_aftert_loan)).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_collection_aftert_loan(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_collection_aftert_loan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_collection_aftert_loan)).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_collection_aftert_loanFlag9(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_collection_aftert_loan))
	if params["Flag"] == "9" {
		SortDateFlg := params["SortDateFlg"]
		delete(params, "Flag")
		delete(params, "SortDateFlg")
		for k, v := range params {
			if k != constant.PageRecCount && k != constant.PageNo {
				if k == "StartDate" {
					qs = qs.Filter("crt_dt__gte", v)
				} else if k == "EndDate" {
					qs = qs.Filter("crt_dt__lte", v)
				} else {
					qs = qs.Filter(k, v)
				}
			}
		}
		if SortDateFlg == "1" {
			_, err = qs.Filter("tcc_state", state).OrderBy("crt_dt").Values(&maps)
		} else {
			_, err = qs.Filter("tcc_state", state).OrderBy("-crt_dt").Values(&maps)
		}
		if err != nil {
			return nil, err
		}
		return
	}
	return
}

func QueryT_loan_collection_aftert_loanFlag8(params orm.Params, state int8) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql :="SELECT t1.coll_task_no CollTaskNo," +
		"t1.cust_no CustNo," +
		"t1.cust_name CustName, " +
		"t1.indv_crtf_typ_cd IndvCrtfTypCd," +
		"t1.indv_crtf_no IndvCrtfNo," +
		"t1.ctrt_no CtrtNo," +
		"t1.loan_dubil_no LoanDubilNo,"+
		"t1.crt_dt CrtDt," +
		"t1.task_dlwth_stus_cd CollTaskDlwthStusCd," +
		"t1.coll_sponsor_empnbr CollSponsorEmpNbr," +
		"t1.coll_corg_empnbr CollCorgEmpNbr," +
		"t2.`sggest_coll_mgmt_manr_cd` CollMgmtManrCd "+
	"FROM t_loan_collection_aftert_loan t1 LEFT JOIN t_loan_collection_dec_aftert_loan t2 ON t1.`coll_task_no`=t2.`coll_task_no`"+
		"WHERE t1.`tcc_state`=0 AND t2.`tcc_state`=0 "
	if params["LoanDubilNo"] !=nil {
		sql = sql +"and t1.loan_dubil_no=\""+params["LoanDubilNo"].(string)+"\""
	}
	if params["CustNo"] !=nil {
		sql = sql +"and t1.cust_no=\""+params["CustNo"].(string)+"\""
	}
	if params["TaskDlwthStusCd"] !=nil {
		sql = sql +"and t1.task_dlwth_stus_cd=\""+params["TaskDlwthStusCd"].(string)+"\""
	}
	if params["InqBeginDt"] !=nil {
		sql = sql +"and t1.crt_dt>=\""+params["InqBeginDt"].(string)+"\""
	}
	if params["InqEndDt"] !=nil {
		sql = sql +"and t1.crt_dt<=\""+params["InqEndDt"].(string)+"\""
	}
	if params["CollTaskNo"] !=nil {
		sql = sql +"and t1.coll_task_no =\""+params["CollTaskNo"].(string)+"\""
	}else {
		sql= sql + " and t1.coll_task_no in(select biz_id from t_loan_approve_task where tcc_state=0 "
		if params["DistrPersonEmpnbr"] !=nil {
			sql = sql +" and distr_person_empnbr=\""+params["DistrPersonEmpnbr"].(string)+"\""
		}
		sql = sql +")"
	}
	_,err = o.Raw(sql).Values(&maps)
	return
}