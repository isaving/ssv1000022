package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_bpmn_retry_param struct {
	TaskExampId   string `orm:"column(task_examp_id);pk" description:"任务实例id"`
	ProcesParaVal string `orm:"column(proces_para_val);null" description:"流程参数值"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_bpmn_retry_param) TableName() string {
	return "t_bpmn_retry_param"
}

func InsertT_bpmn_retry_param(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_bpmn_retry_param).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_bpmn_retry_paramTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_bpmn_retry_param)).Filter("task_examp_id", maps["TaskExampId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_bpmn_retry_paramTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_bpmn_retry_param where task_examp_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TaskExampId"]).Exec()
	return err
}

func QueryT_bpmn_retry_param(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_bpmn_retry_param panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_bpmn_retry_param))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_bpmn_retry_paramById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_bpmn_retry_param)).Filter("task_examp_id", maps["TaskExampId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_bpmn_retry_param(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_bpmn_retry_param panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_bpmn_retry_param)).Filter("task_examp_id", maps["TaskExampId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
