package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_list struct {
	LoanDubilNo     string  `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	LoanProdtNo     string  `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo string  `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	AcctnDt         string  `orm:"column(acctn_dt);type(date)" description:"会计日期  记账的会计日期"`
	SeqNo           int     `orm:"column(seq_no)" description:"序号"`
	TxSn            string  `orm:"column(tx_sn);size(32);null" description:"交易流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,随机整数8位编号规则:1',随机数生成:(int)(Math.random()*100000000)"`
	MakelnStusCd    string  `orm:"column(makeln_stus_cd);size(2);null" description:"放款状态代码  1-成功2-风控拒绝3-冲正"`
	CustNo          string  `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CustName        string  `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	MakelnAcctNo    string  `orm:"column(makeln_acct_no);size(32);null" description:"放款账号"`
	CurCd           string  `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	LoanAmt         float64 `orm:"column(loan_amt);null;digits(18);decimals(2)" description:"贷款金额  如果多次贷放,或者信用卡模式,此金额如何处理?"`
	MakelnOrgNo     string  `orm:"column(makeln_org_no);size(4);null" description:"放款机构号"`
	TxTm            string  `orm:"column(tx_tm);type(time);null" description:"交易时间  记录交易发生的时间,"`
	FinlModfyDt     string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int     `orm:"column(tcc_state);null"`
}

func (t *T_loan_list) TableName() string {
	return "t_loan_list"
}

func InsertT_loan_list(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_list).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_listTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_list)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("acctn_dt", maps["AcctnDt"]).Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_listTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_list where loan_dubil_no=? and acctn_dt=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["AcctnDt"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_list(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_list panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_list))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_listById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_list)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_list(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_list panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_list)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
