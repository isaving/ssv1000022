package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_intelligent_decision struct {
	CrdtAplySn            string  `orm:"column(crdt_aply_sn);pk" description:"授信申请流水号"`
	BizTriggerTypCd       string  `orm:"column(biz_trigger_typ_cd);size(2);null" description:"业务触发类型代码"`
	LoanCharcCd           string  `orm:"column(loan_charc_cd);size(6);null" description:"贷款性质代码"`
	LoanProdtNo           string  `orm:"column(loan_prodt_no);size(50);null" description:"贷款产品编号"`
	LoanProdtVersNo       string  `orm:"column(loan_prodt_vers_no);size(50);null" description:"贷款产品版本编号"`
	AplyCrdtQta           float64 `orm:"column(aply_crdt_qta);null;digits(18);decimals(2)" description:"申请授信额度"`
	TerminalTypCd         string  `orm:"column(terminal_typ_cd);size(2);null" description:"终端类型代码"`
	OprorEmpnbr           string  `orm:"column(opror_empnbr);size(6);null" description:"经办人员工号"`
	ChnlTypCd             string  `orm:"column(chnl_typ_cd);size(4);null" description:"经办机构号"`
	IndvCrtfNo            string  `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	IndvCrtfTypCd         string  `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码"`
	CustName              string  `orm:"column(cust_name);size(60);null" description:"客户姓名"`
	CustNo                string  `orm:"column(cust_no);size(14);null" description:"客户编号"`
	RsdnceDtlAddr         string  `orm:"column(rsdnce_dtl_addr);size(200);null" description:"居住地详细地址"`
	Age                   int     `orm:"column(age);null" description:"年龄"`
	GenderCd              string  `orm:"column(gender_cd);size(3);null" description:"性别代码"`
	MarrgSituationCd      string  `orm:"column(marrg_situation_cd);size(3);null" description:"婚姻状况代码"`
	EdctbkgCd             string  `orm:"column(edctbkg_cd);size(2);null" description:"学历代码"`
	EntrInsdbnkBlklistFlg string  `orm:"column(entr_insdbnk_blklist_flg);size(1);null" description:"进入行内黑名单标志"`
	EntrInsdbnkWhtlFlg    string  `orm:"column(entr_insdbnk_whtl_flg);size(1);null" description:"进入行内白名单标志"`
	PreCrdtQta            float64 `orm:"column(pre_crdt_qta);null;digits(18);decimals(2)" description:"预授信额度"`
	MobileNo              string  `orm:"column(mobile_no);size(11);null" description:"手机号码"`
	TxResultStusCd        string  `orm:"column(tx_result_stus_cd);size(4);null" description:"交易结果状态码"`
	HitExcludRuleComnt    string  `orm:"column(hit_exclud_rule_comnt);size(500);null" description:"击中排除规则说明"`
	BlklistTypCd          string  `orm:"column(blklist_typ_cd);size(10);null" description:"黑名单类型代码"`
	DcmkTypCd             string  `orm:"column(dcmk_typ_cd);size(1);null" description:"决策类型代码"`
	DcmkDescr             string  `orm:"column(dcmk_descr);size(200);null" description:"决策描述"`
	ScoreCardGrpgInfo     string  `orm:"column(score_card_grpg_info);size(200);null" description:"评分卡分组信息"`
	EvaltScr              float64 `orm:"column(evalt_scr);null;digits(18);decimals(2)" description:"评定分数"`
	CreditRatCd           string  `orm:"column(credit_rat_cd);size(2);null" description:"信用评级代码"`
	NegtRsn               string  `orm:"column(negt_rsn);size(200);null" description:"负面原因"`
	MonIncomeAmt          float64 `orm:"column(mon_income_amt);null;digits(18);decimals(2)" description:"月收入金额"`
	SysSggestCrdtQta      float64 `orm:"column(sys_sggest_crdt_qta);null;digits(18);decimals(2)" description:"系统建议授信额度"`
	Bp                    int     `orm:"column(bp);null" description:"BP"`
	BpFlotDrctCd          string  `orm:"column(bp_flot_drct_cd);size(1);null" description:"BP浮动方向代码"`
	AplyDt		          string  `orm:"column(aply_dt);size(1);null" description:"BP浮动方向代码"`
	TccState              int     `orm:"column(tcc_state);null"`
}

func (t *T_intelligent_decision) TableName() string {
	return "t_intelligent_decision"
}

func InsertT_intelligent_decision(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_intelligent_decision).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intelligent_decisionTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_intelligent_decision)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intelligent_decisionTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_intelligent_decision where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryT_intelligent_decision(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intelligent_decision panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intelligent_decision))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intelligent_decisionById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intelligent_decision)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intelligent_decision(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intelligent_decision panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intelligent_decision)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
