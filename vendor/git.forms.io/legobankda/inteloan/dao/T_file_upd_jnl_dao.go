package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_file_upd_jnl struct {
	TxSn             string    `orm:"column(tx_sn);size(10);pk" description:"交易流水号  记录系统中随着交易发生自动生成的编号,"`
	CustNo           string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	DocMgmtNo        string    `orm:"column(doc_mgmt_no);size(24)" description:"文档管理编号"`
	OperTypCd        string    `orm:"column(oper_typ_cd);size(4);null" description:"操作类型代码  C-新增,D-删除,U-修改,R-查询,I-作废"`
	OperPersonEmpnbr string    `orm:"column(oper_person_empnbr);size(6);null" description:"操作人员工号"`
	OperDt           string    `orm:"column(oper_dt);type(date);null" description:"操作日期 "`
	OperTm           string    `orm:"column(oper_tm);type(time);null" description:"操作时间 "`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo  string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_file_upd_jnl) TableName() string {
	return "t_file_upd_jnl"
}

func InsertT_file_upd_jnl(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_file_upd_jnl).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_file_upd_jnlTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_file_upd_jnl)).Filter("tx_sn", maps["TxSn"]).Filter("doc_mgmt_no", maps["DocMgmtNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_file_upd_jnlTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_file_upd_jnl where tx_sn=? and doc_mgmt_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxSn"], maps["DocMgmtNo"]).Exec()
	return err
}

func QueryT_file_upd_jnl(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_file_upd_jnl panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_file_upd_jnl))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_file_upd_jnlById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_file_upd_jnl)).Filter("tx_sn", maps["TxSn"]).Filter("doc_mgmt_no", maps["DocMgmtNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_file_upd_jnl(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_file_upd_jnl panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_file_upd_jnl)).Filter("tx_sn", maps["TxSn"]).Filter("doc_mgmt_no", maps["DocMgmtNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
