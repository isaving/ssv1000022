package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_file_manage struct {
	DocMgmtNo       string    `orm:"column(doc_mgmt_no);pk;size(24);null" description:"文档管理编号"`
	MobileNo        string    `orm:"column(mobile_no)" description:"手机号码"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	ClsfLablNm      string    `orm:"column(clsf_labl_nm);size(120);null" description:"分类标签名称  50位"`
	BizSn           string    `orm:"column(biz_sn);size(32);null" description:"业务流水号  32位,保存各业务流水号"`
	CustName        string    `orm:"column(cust_name);size(60);null" description:"客户姓名"`
	AtchTypCd       string    `orm:"column(atch_typ_cd);size(2);null" description:"附件类型代码"`
	IndvCrtfTypCd   string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo      string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CtrtNo          string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	LoanDubilNo     string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	AchvsValidFlg   string    `orm:"column(achvs_valid_flg);size(1);null" description:"档案有效标志  0-否,,1-是"`
	DocPrdusStageCd string    `orm:"column(doc_prdus_stage_cd);size(4);null" description:"文档产生阶段代码  01-授信申请02-放款申请03-授信调查04-授信审查审批05-放款审批11-贷后预警12-贷后催收13-贷后检查21-变更利率22-变更还款计划23-展期申报及审批31-减值申报及审批32-核销申报及审批"`
	DocTypNm        string    `orm:"column(doc_typ_nm);size(10);null" description:"文档类型名称  长度为10位,image,PDF,word,video,html,xml"`
	DocInfoTypCd    string    `orm:"column(doc_info_typ_cd);size(1);null" description:"文档信息类型代码  01-基本信息02-资产及证明信息03-收入及证明信息04-授信申请信息05-合同信息06-放款申请信息11-授信调查信息12-授信审查审批信息13-放款审批信息14-贷后预警信息15-贷后催收信息16-贷后检查影像信息17-贷后检查附件信息21-变更利率信息22-变更还款计划信息23-展期申报及审批信息31-减值申报及审批信息32-核销申报及审批信息"`
	DocNm           string    `orm:"column(doc_nm);size(120);null" description:"文档名称"`
	DocNo           string    `orm:"column(doc_no);size(12);null" description:"文档编号"`
	DocPgNo         string    `orm:"column(doc_pg_no);size(10);null" description:"文档页号码"`
	DocPath         string    `orm:"column(doc_path);size(200);null" description:"文档路径"`
	DocCrtDt        string    `orm:"column(doc_crt_dt);type(date);null" description:"文档创建日期"`
	DocCrtTm        string    `orm:"column(doc_crt_tm);type(time);null" description:"文档创建时间"`
	DocDescr        string    `orm:"column(doc_descr);size(1000);null" description:"文档描述"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState            int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_file_manage) TableName() string {
	return "t_file_manage"
}

func InsertT_file_manage(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_file_manage).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_file_manageTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_file_manage)).Filter("doc_mgmt_no", maps["DocMgmtNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_file_manageTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_file_manage where doc_mgmt_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["DocMgmtNo"]).Exec()
	return err
}

func QueryT_file_manage(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_file_manage panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_file_manage))
	if params["Flag"] != nil && params["Flag"]=="9"{
		delete(params,"Flag")
		for k, v := range params {
			if k != constant.PageRecCount && k != constant.PageNo {
				qs = qs.Filter(k, v)
			}
		}
		if _, err = qs.Filter("tcc_state", state).Limit(-1).GroupBy("cust_no","mobile_no","loan_dubil_no","indv_crtf_typ_cd","indv_crtf_no").
			Values(&maps,"loan_dubil_no","ctrt_no","cust_no","cust_name","loan_prodt_no","achvs_valid_flg"); err != nil {
			return nil, err
		}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_file_manageById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_file_manage)).Filter("doc_mgmt_no", maps["DocMgmtNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_file_manage(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_file_manage panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_file_manage)).Filter("doc_mgmt_no", maps["DocMgmtNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func DeleteT_file_manage(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	maps["TccState"]=2
	_, err = o.QueryTable(new(T_file_manage)).Filter("mobile_no", maps["MobileNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}