package dao

import (
	"errors"
	"fmt"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_acct_loan_acctg_period struct {
	AcctgAcctNo string `orm:"column(acctg_acct_no);pk;null"`
	TccState    int    `orm:"column(tcc_state);null"`
}

func (t *T_acct_loan_acctg_period) TableName() string {
	return "T_acct_loan_acctg_period"
}

func InsertT_acct_loan_acctg_period(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_acct_loan_acctg_period).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_acct_loan_acctg_periodTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_acct_loan_acctg_period)).Filter("tx_reqe_cd", maps["TxReqeCd"]).
		Filter("chnl_no_cd", maps["ChnlNoCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_acct_loan_acctg_periodTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_acct_loan_acctg_period where tx_reqe_cd=? and chnl_no_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxReqeCd"], maps["ChnlNoCd"]).Exec()
	return err
}

func QueryT_acct_loan_acctg_period(in []map[string]interface{}, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_acct_loan_acctg_period panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	//qs := o.QueryTable(new(T_acct_loan_acctg_period))
	//for k, v := range params {
	//	if k != constant.PageRecCount && k != constant.PageNo {
	//		qs = qs.Filter(k, v)
	//	}
	//}
	//if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
	//	return nil, err
	//}
	for i := 0; i < len(in); i++ {
		//根据贷款核算账号+期数 查询分期还本表记录t_acct_loan_acctg_period，获取plan_repay_bal计划还本金额
		var PerMap []orm.Params
		var PerQuery string
		var PlanRepayBal float64 //还本金额
		PerQuery = fmt.Sprintf("select * from t_acct_loan_acctg_period where acctg_acct_no = '%s' and period_num = '%v'", in[i]["AcctiAcctNo"], in[i]["Pridnum"].(string))
		if _, err := o.Raw(PerQuery).Values(&PerMap); err != nil {
			log.Error(err)
			return nil, err
		}
		if len(PerMap) == 0 { //查不到信息，PlanRepayBal暂时赋值为0
			PlanRepayBal = 0.00
		} else { //获取分期还本的计划话本金额给PlanRepayBal赋值
			PlanRepayBal, _ = strconv.ParseFloat(PerMap[0]["plan_repay_bal"].(string), 10)
		}
		//查询对应的还息计划表记录t_loan_bal_type_int_calc,获取累计结息金额 AccmIntSetlAmt，已计未结利息/罚息 AccmCmpdAmt
		var BalIntMap []orm.Params
		var SqlBalIntQuery string
		var AccmIntSetlAmt, AcruUnstlIntr float64
		SqlBalIntQuery = fmt.Sprintf("select * from t_loan_bal_type_int_calc where acctg_acct_no = '%s' and period_num = '%v'", in[i]["AcctiAcctNo"], in[i]["Pridnum"].(string))
		if _, err = o.Raw(SqlBalIntQuery).Values(&BalIntMap); err != nil {
			log.Error(err)
			return nil, err
		}
		if len(BalIntMap) == 0 {
			AccmIntSetlAmt = 0
			AcruUnstlIntr = 0
		} else {
			AccmIntSetlAmt, _ = strconv.ParseFloat(BalIntMap[0]["accm_int_setl_amt"].(string), 64)
			AcruUnstlIntr, _ = strconv.ParseFloat(BalIntMap[0]["acru_unstl_intr"].(string), 64)
		}
		mapArr := make(map[string]interface{})
		mapArr["PlanRepayBal"] = PlanRepayBal
		mapArr["AccmIntSetlAmt"] = AccmIntSetlAmt
		mapArr["AcruUnstlIntr"] = AcruUnstlIntr
		maps = append(maps, mapArr)
	}

	return
}

func QueryT_acct_loan_acctg_periodById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_acct_loan_acctg_period)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_acct_loan_acctg_period(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_acct_loan_acctg_period panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_acct_loan_acctg_period)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
