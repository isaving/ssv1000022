package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_object_send_strategy_manage struct {
	SendStrategyId      string `orm:"column(send_strategy_id);pk" description:"发送策略ID"`
	MessageType         string `orm:"column(message_type);size(4);null" description:"信息类型：01，短信；02，彩信；03，WAP_PUSH ，04-邮件、05-通信，06发送到第三方APP，"`
	MessageObjectNumber int    `orm:"column(message_object_number);null" description:"消息对象选择数量"`
	PriorityFlag        string `orm:"column(priority_flag);size(4);null" description:"是否优先成功及可"`
	AdvSndDays          int    `orm:"column(adv_snd_days);null" description:"提前发送天数"`
	SndBgnTm            string `orm:"column(snd_bgn_tm);type(time);null" description:"发送开始时间"`
	SndEndTm            string `orm:"column(snd_end_tm);type(time);null" description:"发送结束时间"`
	SendSequence        string `orm:"column(send_sequence);size(4);null" description:"发送顺序（0-串行，1-并行）"`
	RepeatGapTime       string `orm:"column(repeat_gap_time);size(10);null" description:"等待状态报告的时间。单位：分钟"`
	ResponseFlag        string `orm:"column(response_flag);size(4);null" description:"是否要回复"`
	ResponseMaxDay      string `orm:"column(response_max_day);size(10);null" description:"回复的天数上限"`
	Channel             string `orm:"column(channel);size(4);null" description:"渠道（01-邮箱，02-短信平台，03-Call Center)"`
	InfoType            string `orm:"column(info_type);size(4);null" description:"信息类型（1.纸质，2.语音， 3.文本）"`
	EntrustFlag         string `orm:"column(entrust_flag);size(4);null" description:"是否委托第三方"`
	EntrustInfo         string `orm:"column(entrust_info);size(50);null" description:"委托第三方信息"`
	RepeatFlag          string `orm:"column(repeat_flag);size(4);null" description:"是否要重发"`
	RepeatWaitTime      string `orm:"column(repeat_wait_time);size(50);null" description:"重发等待间隔的时间。单位：分钟"`
	RepeatMaxFrequency  string `orm:"column(repeat_max_frequency);size(50);null" description:"重发次数上限"`
	SendTypeCd          string `orm:"column(send_type_cd);size(4);null" description:"发送类型"`
	SendType            string `orm:"column(send_type);size(4);null" description:"发送方式"`
	FinlModfyDt         string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState            int    `orm:"column(tcc_state);null" description:"tcc状态 0-Normal，1-Insert"`
}

func (t *T_object_send_strategy_manage) TableName() string {
	return "t_object_send_strategy_manage"
}

func InsertT_object_send_strategy_manage(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_object_send_strategy_manage).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_object_send_strategy_manageTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_object_send_strategy_manage)).Filter("send_strategy_id", maps["SendStrategyId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_object_send_strategy_manageTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_object_send_strategy_manage where send_strategy_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["SendStrategyId"]).Exec()
	return err
}

func QueryT_object_send_strategy_manage(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_object_send_strategy_manage panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_object_send_strategy_manage))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_object_send_strategy_manageById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_object_send_strategy_manage)).Filter("send_strategy_id", maps["SendStrategyId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_object_send_strategy_manage(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_object_send_strategy_manage panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_object_send_strategy_manage)).Filter("send_strategy_id", maps["SendStrategyId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
