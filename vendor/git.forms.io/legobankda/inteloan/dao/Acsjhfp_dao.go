package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Acsjhfp struct {
	TccState int `orm:"column(tcc_state);null"`
}

func (t *Acsjhfp) TableName() string {
	return "Acsjhfp"
}

func InsertAcsjhfp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Acsjhfp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateAcsjhfpTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Acsjhfp)).Filter("tx_reqe_cd", maps["TxReqeCd"]).
		Filter("chnl_no_cd", maps["ChnlNoCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteAcsjhfpTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from Acsjhfp where tx_reqe_cd=? and chnl_no_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxReqeCd"], maps["ChnlNoCd"]).Exec()
	return err
}

func QueryAcsjhfp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryAcsjhfp panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Acsjhfp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryAcsjhfpById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Acsjhfp)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateAcsjhfp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateAcsjhfp panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Acsjhfp)).Filter("tx_reqe_cd", maps["TxReqeCd"]).Filter("chnl_no_cd", maps["ChnlNoCd"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
