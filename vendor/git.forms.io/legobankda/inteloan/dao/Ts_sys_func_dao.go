package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Ts_sys_func struct {
	FuncNo               string `orm:"column(func_no);pk" description:"功能编号"`
	OrdNo                int    `orm:"column(ord_no);size(1);null" description:"排序编号"`
	SoftProdtCd          string `orm:"column(soft_prodt_cd);size(3);null" description:"软件产品编码  三位字母的系统"`
	FuncUrlPath          string `orm:"column(func_url_path);size(200);null" description:"功能url路径"`
	FuncLablEngNm        string `orm:"column(func_labl_eng_nm);size(120);null" description:"功能标签英文名称"`
	FuncLablLoclLangugNm string `orm:"column(func_labl_locl_langug_nm);size(120);null" description:"功能"`
	ButnKeyVal           string `orm:"column(butn_key_val);size(50);null" description:"按钮键值"`
	FuncRouteSeqNo       int    `orm:"column(func_route_seq_no);null" description:"功能路由序号"`
	LstoneBrthNodeFuncNo string `orm:"column(lstone_brth_node_func_no);size(20);null" description:"上一兄"`
	MgmtPageNextButnFlg  string `orm:"column(mgmt_page_next_butn_flg);size(1);null" description:"管理页面下"`
	SprFuncId            string `orm:"column(spr_func_id);size(50);null" description:"上级功能id"`
	NeedLgnAccessFlg     string `orm:"column(need_lgn_access_flg);size(1);null" description:"需登录访问标志"`
	NeedAuthAccessFlg    string `orm:"column(need_auth_access_flg);size(1);null" description:"需授权访问标志""`
	FuncTypCd            string `orm:"column(func_typ_cd);size(1);null" description:"功能类型代码  M-菜单B-按钮L"`
	MenuTypCd            string `orm:"column(menu_typ_cd);size(2);null" description:"菜单类型代码  L-左菜单 R-右"`
	FuncValidFlg         string `orm:"column(func_valid_flg);size(1);null" description:"功能有效标志"`
	UserTaskNodeFlg      string `orm:"column(user_task_node_flg);size(1);null" description:"用户任务节点标志"`
	Remrk                string `orm:"column(remrk);size(200);null" description:"备注"`
	MenuIconPath         string `orm:"column(menu_icon_path);size(200);null" description:"菜单图标路径"`
	FinlModfyDt          string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm          string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo       string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int    `orm:"column(tcc_state);null"`
}

func (t *Ts_sys_func) TableName() string {
	return "ts_sys_func"
}

func InsertTs_sys_func(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Ts_sys_func).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateTs_sys_funcTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Ts_sys_func)).Filter("func_no", maps["FuncNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteTs_sys_funcTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from ts_sys_func where func_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["FuncNo"]).Exec()
	return err
}

func QueryTs_sys_func(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	if flag == "9"{
		sql := "select a.* from ts_sys_func a left join  t_proc_model_para b  on a.func_no=b.TASK_CONF_PARA_INFO where 1=1 "
		if params["UserTaskNodeFlg"] != nil {
			sql = sql +" and a.user_task_node_flg=\""+params["UserTaskNodeFlg"].(string)+"\""
		}
		if params["TaskTmplTypCd="] != nil {
			sql = sql +" and b.task_tmpl_typ_cd==\""+params["TaskTmplTypCd"].(string)+"\""
		}
		if _, err =o.Raw(sql).Values(&maps);err != nil {
			return nil, err
		}
		return
	}
	qs := o.QueryTable(new(Ts_sys_func))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryTs_sys_funcById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Ts_sys_func)).Filter("func_no", maps["FuncNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateTs_sys_func(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New(fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Ts_sys_func)).Filter("func_no", maps["FuncNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
