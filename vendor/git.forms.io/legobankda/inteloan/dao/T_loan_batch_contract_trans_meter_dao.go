package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_batch_contract_trans_meter struct {
	LoanDubilNo      string    `orm:"column(loan_dubil_no);pk;size(32)" description:"贷款借据号"`
	SeqNo            int       `orm:"column(seq_no);size(14)" description:"客户编号"`
	Pridnum          int       `orm:"column(pridnum);size(14)" description:"客户编号"`
	AcctnDt          string    `orm:"column(acctn_dt);type(date)" description:"会计日期"`
	LoanBatBizTypCd  string    `orm:"column(loan_bat_biz_typ_cd);size(2)" description:"贷款批量业务类型代码  01-利息计提02-账单处理03-逾期处理04-不良处理05-计息余额同步06-还款提醒07-还款计划重算"`
	CustNo           string    `orm:"column(cust_no);size(14)" description:"客户编号"`
	BizSn            string    `orm:"column(biz_sn);size(32);null" description:"业务流水号"`
	BatJobStepStusCd string    `orm:"column(bat_job_step_stus_cd);size(2);null" description:"批量作业步骤状态代码  00-执行中,01-第一步,,02-第二步,,03-第三步,,04-第四步,,05-第五步,,06-第六步,,07-第七步,,08-第八步,,99-成功"`
	TccState             int    `orm:"column(tcc_state);null"`
}

func (t *T_loan_batch_contract_trans_meter) TableName() string {
	return "t_loan_batch_contract_trans_meter"
}

func InsertT_loan_batch_contract_trans_meter(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_batch_contract_trans_meter where loan_dubil_no=? and acctn_dt=? and loan_bat_biz_typ_cd=?"
		o.Raw(sql,  maps["LoanDubilNo"], maps["AcctnDt"], maps["LoanBatBizTypCd"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	flag := maps["Flag"]
	delete(maps,"Flag")
	if flag == "9"{
		sql := util.BuildSqlTcc(maps, new(T_loan_batch_contract_trans_meter).TableName(),0)
		_, err := o.Raw(sql).Exec()
		maps["Flag"]=flag
		return err
	}
	sql := util.BuildSql(maps, new(T_loan_batch_contract_trans_meter).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_batch_contract_trans_meterTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_batch_contract_trans_meter where loan_dubil_no=? and acctn_dt=? and loan_bat_biz_typ_cd=?"
		o.Raw(sql,  maps["LoanDubilNo"], maps["AcctnDt"], maps["LoanBatBizTypCd"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	flag := maps["Flag"]
	delete(maps,"Flag")
	if flag == "9"{
		maps["Flag"]=flag
		return nil
	}
	_, err := o.QueryTable(new(T_loan_batch_contract_trans_meter)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("acctn_dt", maps["AcctnDt"]).Filter("loan_bat_biz_typ_cd", maps["LoanBatBizTypCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_batch_contract_trans_meterTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_batch_contract_trans_meter where loan_dubil_no=? and acctn_dt=? and loan_bat_biz_typ_cd=? and cust_no=?"
		o.Raw(sql,  maps["LoanDubilNo"], maps["AcctnDt"], maps["LoanBatBizTypCd"], maps["CustNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	flag := maps["Flag"]
	delete(maps,"Flag")
	if flag == "9"{
		maps["Flag"]=flag
		return nil
	}
	sql := "delete from t_loan_batch_contract_trans_meter where loan_dubil_no=? and acctn_dt=? and loan_bat_biz_typ_cd=?"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["AcctnDt"], maps["LoanBatBizTypCd"]).Exec()
	return err
}

func QueryT_loan_batch_contract_trans_meter(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_batch_contract_trans_meter panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_loan_batch_contract_trans_meter))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9"{
		if _, err = qs.Filter("loan_dubil_no", params["LoanDubilNo"]).Filter("acctn_dt", params["AcctnDt"]).
			Filter("loan_bat_biz_typ_cd", params["LoanBatBizTypCd"]).Limit(1).OrderBy("-seq_no").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_batch_contract_trans_meterById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_batch_contract_trans_meter)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("loan_bat_biz_typ_cd", maps["LoanBatBizTypCd"]).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_batch_contract_trans_meter(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_batch_contract_trans_meter panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_batch_contract_trans_meter)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("loan_bat_biz_typ_cd", maps["LoanBatBizTypCd"]).Filter("seq_no",maps["SeqNo"]).Update(maps)
	return err
}
