package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_prod_flow_para struct {
	LoanProdtNo       string    `orm:"column(loan_prodt_no);size(20);pk" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	ProdtProcesVersNo int       `orm:"column(prodt_proces_vers_no)" description:"产品流程版本编号  PK"`
	OrgNo             string    `orm:"column(org_no);size(4);null" description:"机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	BizProcesNm       string    `orm:"column(biz_proces_nm);size(120);null" description:"业务流程名称"`
	ProcesTypCd       string    `orm:"column(proces_typ_cd);size(2);null" description:"流程类型代码  01-授信申请02-放款申请03-提额申请04-额度调整05-额度冻结06-额度解冻11-人工调查12-贷后任务13-贷后变更还款计划14-贷款变更还款利率21-减值管理22-核销管理"`
	ProcesModelId     string    `orm:"column(proces_model_id);size(50);null" description:"流程模型id  包括4位识别码+32为UUID"`
	ProcesModelVersNo int       `orm:"column(proces_model_vers_no);null" description:"流程模型版本编号  PK,,新增字段"`
	CrtTelrNo         string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtDt             string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtTm             string    `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState          int       `orm:"column(tcc_state);null"`
}

func (t *T_prod_flow_para) TableName() string {
	return "t_prod_flow_para"
}

func InsertT_prod_flow_para(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_prod_flow_para).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_prod_flow_paraTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_prod_flow_para)).Filter("loan_prodt_no", maps["LoanProdtNo"]).
		Filter("prodt_proces_vers_no", maps["ProdtProcesVersNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_prod_flow_paraTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_prod_flow_para where loan_prodt_no=? and prodt_proces_vers_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanProdtNo"], maps["ProdtProcesVersNo"]).Exec()
	return err
}

func QueryT_prod_flow_para(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_prod_flow_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_prod_flow_para))
	if params["Flag"] !=nil && params["Flag"]=="9"{
		sql :="SELECT loan_prodt_no LoanProdtNo,proces_typ_cd ProcesTypCd,GROUP_CONCAT(prodt_proces_vers_no) Concat," +
			"GROUP_CONCAT(org_no) OrgNo,"+
			"GROUP_CONCAT(proces_model_id) ProcesModelId,"+
			"GROUP_CONCAT(proces_model_vers_No) ProcesModelVersNo,"+
			"GROUP_CONCAT( crt_dt) CrtDt"+
			"  FROM ("+
			"SELECT * FROM t_prod_flow_para WHERE tcc_state=0 "
		if params["LoanProdtNo"] !=nil {
			sql = sql + " and loan_prodt_no=\""+params["LoanProdtNo"].(string)+"\""
		}
		if params["ProcesTypCd"] !=nil {
			sql = sql + " and proces_typ_cd=\""+params["ProcesTypCd"].(string)+"\""
		}
		if params["OrgNo"] !=nil {
			sql = sql + " and org_no=\""+params["OrgNo"].(string)+"\""
		}
		sql = sql +" GROUP  BY loan_prodt_no ,proces_typ_cd,prodt_proces_vers_no "+
			") t1 GROUP BY loan_prodt_no,proces_typ_cd "
		_, err = o.Raw(sql).Values(&maps)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_prod_flow_paraById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_prod_flow_para)).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("prodt_proces_vers_no", maps["ProdtProcesVersNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_prod_flow_para(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_prod_flow_para panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_prod_flow_para)).Filter("loan_prodt_no", maps["LoanProdtNo"]).Filter("prodt_proces_vers_no", maps["ProdtProcesVersNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
