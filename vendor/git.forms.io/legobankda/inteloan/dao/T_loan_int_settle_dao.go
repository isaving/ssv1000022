package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_int_settle struct {
	AcctiAcctNo     string    `orm:"column(accti_acct_no);size(32);null" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	BizFolnNo       string    `orm:"column(biz_foln_no);pk;size(32)" description:"业务跟踪号  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	SysFolnNo       string    `orm:"column(sys_foln_no);size(32)" description:"系统跟踪号  标识一笔渠道接入交易在四川农信内部各系统之间交互的唯一识别流水号,32位组成为:客户端接入软件产品编码3位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号15位"`
	Pridnum         int       `orm:"column(pridnum)" description:"期数"`
	IntrStusCd      string    `orm:"column(intr_stus_cd);size(2);null" description:"利息状态代码  0-正常,1-逾期"`
	TxAcctnDt       string    `orm:"column(tx_acctn_dt);type(date);null" description:"交易会计日期"`
	TxReqeCd        string    `orm:"column(tx_reqe_cd);size(4);null" description:"交易请求代码"`
	DlwthStusCd     string    `orm:"column(dlwth_stus_cd);size(4);null" description:"处理状态代码"`
	IntacrBgnDt     string    `orm:"column(intacr_bgn_dt);type(date);null" description:"计息开始日期"`
	IntacrEndDt     string    `orm:"column(intacr_end_dt);type(date);null" description:"计息结束日期"`
	IntacrDays      int       `orm:"column(intacr_days);null" description:"计息天数"`
	IntacrAmt       float64   `orm:"column(intacr_amt);null;digits(18);decimals(2)" description:"计息金额"`
	IntacrIntrt     float64   `orm:"column(intacr_intrt);null;digits(9);decimals(6)" description:"计息利率"`
	IntrAmt         float64   `orm:"column(intr_amt);null;digits(18);decimals(2)" description:"利息金额  用于登记记录是否允许冲正,当该记录不允许冲正时,置为’N’,当该记录允许冲正时,置为’Y’,对置为’’'空'的记录,冲正时也判断为允许冲正,占用第一位"`
	PmitRvrsFlg     string    `orm:"column(pmit_rvrs_flg);size(1);null" description:"允许冲正标志  0否1是"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_int_settle) TableName() string {
	return "t_loan_int_settle"
}

func InsertT_loan_int_settle(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_int_settle).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_int_settleTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_int_settle)).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("sys_foln_no", maps["SysFolnNo"]).Filter("pridnum", maps["Pridnum"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_int_settleTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_int_settle where biz_foln_no=? and sys_foln_no=? and pridnum=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizFolnNo"], maps["SysFolnNo"], maps["SysFolnNo"]).Exec()
	return err
}

func QueryT_loan_int_settle(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_int_settle panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_int_settle))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_int_settleById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_int_settle)).Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).
		Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_int_settle(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_int_settle panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_int_settle)).Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).
		Filter("pridnum", maps["Pridnum"]).Filter("tcc_state", 0).Update(maps)
	return err
}
