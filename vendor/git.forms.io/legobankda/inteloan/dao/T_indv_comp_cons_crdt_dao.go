package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_indv_comp_cons_crdt struct {
	CrdtAplySn       string    `orm:"column(crdt_aply_sn);pk" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	CustNo           string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	MonIncomeAmt     float64   `orm:"column(mon_income_amt);null;digits(18);decimals(2)" description:"月收入金额  公共服务提供类型及长度"`
	IncomeRemrk      string    `orm:"column(income_remrk);size(200);null" description:"收入备注"`
	SocietyEnsrNo    string    `orm:"column(society_ensr_no);size(32);null" description:"社会保障号码"`
	AcmltnfundAcctNo string    `orm:"column(acmltnfund_acct_no);size(32);null" description:"公积金账号  公积金账号"`
	TaxpyerIdtfyNo   string    `orm:"column(taxpyer_idtfy_no);size(20);null" description:"纳税人识别号  业务回复:国税地税已合并"`
	FinlModfyDt      string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  N"`
	FinlModfyTm      string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  N"`
	FinlModfyOrgNo   string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号  N"`
	FinlModfyTelrNo  string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号  N"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_indv_comp_cons_crdt) TableName() string {
	return "t_indv_comp_cons_crdt"
}

func InsertT_indv_comp_cons_crdt(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_indv_comp_cons_crdt).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_indv_comp_cons_crdtTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_indv_comp_cons_crdt)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_indv_comp_cons_crdtTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_indv_comp_cons_crdt where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryT_indv_comp_cons_crdt(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_indv_comp_cons_crdt panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_indv_comp_cons_crdt))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_indv_comp_cons_crdtById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_indv_comp_cons_crdt)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_indv_comp_cons_crdt(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_indv_comp_cons_crdt panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_indv_comp_cons_crdt)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
