package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_special_change_record struct {
	NmSnglTypCd     string `orm:"column(nm_sngl_typ_cd);size(2);null" description:"名单类型代码  01-白名单02-黑名单"`
	OrgNo           string `orm:"column(org_no);size(4);null" description:"机构号"`
	NmSnglNo        string `orm:"column(nm_sngl_no);size(32);null" description:"名单编号"`
	NmSnglSeqNo     int    `orm:"column(nm_sngl_seq_no);null" description:"名单序号"`
	SorcDescr       string `orm:"column(sorc_descr);size(200);null" description:"来源描述"`
	RecmndEmpnbr    string `orm:"column(recmnd_empnbr);size(6);null" description:"推荐人员工号"`
	CustNo          string `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName        string `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd   string `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:cd00153"`
	IndvCrtfNo      string `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	MobileNo        string `orm:"column(mobile_no);size(11);null" description:"手机号码"`
	OperTypCd       string `orm:"column(oper_typ_cd);size(4);null" description:"操作类型代码"`
	OperProcesCd    string `orm:"column(oper_proces_cd);size(4);null" description:"操作流程代码  01-申请02-审批"`
	OperDt          string `orm:"column(oper_dt);type(date);null" description:"操作日期"`
	ApprvSugstnCd   string `orm:"column(apprv_sugstn_cd);size(3);null" description:"审批意见代码  0-不适用,1-同意,2-退回,3-终止,4-弃权,5-拒绝"`
	RplshInfo       string `orm:"column(rplsh_info);size(200);null" description:"补充信息"`
	TxSn            string `orm:"column(tx_sn);pk" description:"交易流水号  生成规则:应用2位,+,日期时间14位+'yyyymmddhhmmss'+,随机整数8位编号规则:1',随机数生成:(int)(math.random()*100000000)"`
	OprOrgNo        string `orm:"column(opr_org_no);size(4);null" description:"经办机构号"`
	OprorEmpnbr     string `orm:"column(opror_empnbr);size(6);null" description:"经办人员工号"`
	CrtDt           string `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtEmpnbr       string `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo        string `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_special_change_record) TableName() string {
	return "t_loan_special_change_record"
}

func InsertT_loan_special_change_record(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_special_change_record).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_special_change_recordTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_special_change_record)).Filter("tx_sn", maps["TxSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_special_change_recordTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_special_change_record where tx_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxSn"]).Exec()
	return err
}

func QueryT_loan_special_change_record(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_special_change_record panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_special_change_record))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_special_change_recordById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_special_change_record)).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_special_change_record(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_special_change_record panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_special_change_record)).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
