package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_quota_mgmt struct {
	CrdtAplySn            string  `orm:"column(crdt_aply_sn);pk" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	CustNo                string  `orm:"column(cust_no);size(14);null" description:"客户编号  客户编号,ECIF生成"`
	LoanProdtVersNo       string  `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	QtaFinlAdjDt          string  `orm:"column(qta_finl_adj_dt);type(date);null" description:"额度最后调整日期"`
	QtaEfftDt             string  `orm:"column(qta_efft_dt);type(date);null" description:"额度生效日期"`
	QtaInvalidDt          string  `orm:"column(qta_invalid_dt);type(date);null" description:"额度失效日期"`
	PmitOvrqtMakelnFlg    string  `orm:"column(pmit_ovrqt_makeln_flg);size(1);null" description:"允许超额放款标志   0-否,1-是"`
	OvrqtMakelnRatio      float64 `orm:"column(ovrqt_makeln_ratio);null;digits(9);decimals(6)" description:"超额放款比例"`
	LoanGuarManrCd        string  `orm:"column(loan_guar_manr_cd);size(2);null;" description:"贷款担保方式代码"`
	LoanProdtNo           string  `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	QtaStusCd             string  `orm:"column(qta_stus_cd);size(2);null" description:"额度状态代码  01-正常02-冻结03-部分冻结04-失效05-终止"`
	QtaMltpDistrFlg       string  `orm:"column(qta_mltp_distr_flg);size(1);null" description:"额度多次分配标志  0否1是"`
	RevlQtaFlg            string  `orm:"column(revl_qta_flg);size(1);null" description:"循环额度标志  0:非循环额度,1:循环额度是否"`
	QtaCanGuarThirdPtyFlg string  `orm:"column(qta_can_guar_third_pty_flg);size(1);null" description:"额度可担保第三方标志  0否1是"`
	IndvCrtfTypCd         string  `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo            string  `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	CustName              string  `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	CurCd                 string  `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	InitQta               float64 `orm:"column(init_qta);null;digits(18);decimals(2)" description:"初始额度"`
	CurrCrdtQta           float64 `orm:"column(curr_crdt_qta);null;digits(18);decimals(2)" description:"当前授信额度  7月新增"`
	FrzQta                float64 `orm:"column(frz_qta);null;digits(18);decimals(2)" description:"冻结额度"`
	AlrdyUseQta           float64 `orm:"column(alrdy_use_qta);null;digits(18);decimals(2)" description:"已使用额度  记录客户已经使用的额度,单位元"`
	QtaFsttmAprvlDt       string  `orm:"column(qta_fsttm_aprvl_dt);type(date);null" description:"额度首次核准日期"`
	QtaMatrDt             string  `orm:"column(qta_matr_dt);type(date);null" description:"额度到期日期"`
	QtaGrtOrgNo           string  `orm:"column(qta_grt_org_no);size(4);null" description:"额度授予机构号"`
	CrtDt                 string  `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtEmpnbr             string  `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo              string  `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	ArtgclAdjTms          int     `orm:"column(artgcl_adj_tms);null" description:"人工调整次数"`
	LsttmQtaAdjManrCd     string  `orm:"column(lsttm_qta_adj_manr_cd);size(1);null" description:"上次额度调整方式代码  0-系统自动调整1-人工调整-调高2-人工调整-调低"`
	FinlModfyDt           string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm           string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo        string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo       string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState              int     `orm:"column(tcc_state);null"`
}

func (t *T_loan_quota_mgmt) TableName() string {
	return "t_loan_quota_mgmt"
}

func InsertT_loan_quota_mgmt(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_quota_mgmt).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_quota_mgmtTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_quota_mgmt)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_quota_mgmtTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_quota_mgmt where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryT_loan_quota_mgmt(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_quota_mgmt panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params, "Flag")
	qs := o.QueryTable(new(T_loan_quota_mgmt))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "QtaEfftDtStart" {
				qs = qs.Filter("qta_efft_dt__gte", v)
			} else if k == "QtaEfftDtEnd" {
				qs = qs.Filter("qta_efft_dt__lte", v)
			} else if k == "QtaFsttmAprvlDtStart" {
				qs = qs.Filter("qta_fsttm_aprvl_dt__gte", v)
			} else if k == "QtaFsttmAprvlDtEnd" {
				qs = qs.Filter("qta_fsttm_aprvl_dt__lte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if flag == "9" {
		if _, err = qs.Filter("tcc_state", state).OrderBy("-qta_finl_adj_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
	}
	if flag == "8" {
		if _, err = qs.Filter("tcc_state", state).OrderBy("crt_dt", "finl_modfy_dt", "finl_modfy_tm").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_quota_mgmtById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_quota_mgmt)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_quota_mgmt(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_quota_mgmt panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_quota_mgmt)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
