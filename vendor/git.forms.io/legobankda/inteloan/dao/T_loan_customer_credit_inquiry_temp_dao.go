package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_customer_credit_inquiry_temp struct {
	IndvCrtftypCd   string `orm:"column(indv_crtftyp_cd);size(1);pk" description:"个人证件类型代码"`
	CrtfNo          string `orm:"column(crtf_no);size(20)" description:"证件号码"`
	CrdQuryDt       string `orm:"column(crd_qury_dt);size(10);null" description:"征信查询日期"`
	CrdQuryResultCd string `orm:"column(crd_qury_result_cd);size(2);null" description:"征信查询结果代码"`
	CrdQuryResultId string `orm:"column(crd_qury_result_id);size(50);null" description:"征信查询结果ID"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_customer_credit_inquiry_temp) TableName() string {
	return "t_loan_customer_credit_inquiry_temp"
}

func InsertT_loan_customer_credit_inquiry_temp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_customer_credit_inquiry_temp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_customer_credit_inquiry_tempTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_customer_credit_inquiry_temp)).Filter("indv_crtftyp_cd", maps["IndvCrtftypCd"]).
		Filter("crtf_no", maps["CrtfNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_customer_credit_inquiry_tempTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_customer_credit_inquiry_temp where indv_crtftyp_cd=? and crtf_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IndvCrtftypCd"], maps["CrtfNo"]).Exec()
	return err
}

func QueryT_loan_customer_credit_inquiry_temp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_customer_credit_inquiry_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_customer_credit_inquiry_temp))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_customer_credit_inquiry_tempById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_customer_credit_inquiry_temp)).Filter("indv_crtftyp_cd", maps["IndvCrtftypCd"]).Filter("crtf_no", maps["CrtfNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_customer_credit_inquiry_temp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_customer_credit_inquiry_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_customer_credit_inquiry_temp)).Filter("indv_crtftyp_cd", maps["IndvCrtftypCd"]).Filter("crtf_no", maps["CrtfNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
