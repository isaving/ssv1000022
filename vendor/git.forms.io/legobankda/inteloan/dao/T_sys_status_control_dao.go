package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sys_status_control struct {
	KeprcdNo        string    `orm:"column(keprcd_no);pk" description:"记录编号"`
	SysStusCd       string    `orm:"column(sys_stus_cd);size(1);null" description:"系统状态代码  O-日间服务F-日终服务"`
	SysCtofModeCd   string    `orm:"column(sys_ctof_mode_cd);size(2);null" description:"系统日切模式代码  N-自然日切C-受控日切"`
	OnlineBizDt     string    `orm:"column(online_biz_dt);type(date);null" description:"联机业务日期"`
	BatBizDt        string    `orm:"column(bat_biz_dt);type(date);null" description:"批量业务日期"`
	NxtoneBizDt     string    `orm:"column(nxtone_biz_dt);type(date);null" description:"下一业务日期"`
	LstoneBizDt     string    `orm:"column(lstone_biz_dt);type(date);null" description:"上一业务日期 "`
	CtofTm          string    `orm:"column(ctof_tm);type(timestamp);null" description:"日切时间  描述事件过程长短和发生顺序的度量,格式:HH:MM:SS,"`
	TranOnlineTm    string    `orm:"column(tran_online_tm);type(timestamp);null" description:"转联机时间  描述事件过程长短和发生顺序的度量,格式:HH:MM:SS,"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号  记录事件的最后修改人从属机构号,"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号  记录最后修改事件的柜员编号,柜员一般包括有交易柜员,复核柜员,授权柜员等不同角色,"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_sys_status_control) TableName() string {
	return "t_sys_status_control"
}

func InsertT_sys_status_control(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sys_status_control).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sys_status_controlTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sys_status_control)).Filter("keprcd_no", maps["KeprcdNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sys_status_controlTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_sys_status_control where keprcd_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_sys_status_control(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sys_status_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_sys_status_control))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sys_status_controlById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sys_status_control)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sys_status_control(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sys_status_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sys_status_control)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
