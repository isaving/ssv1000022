package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_blacklist_info struct {
	OrgNo                string `orm:"column(org_no);size(4);pk" description:"机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	SeqNo                int    `orm:"column(seq_no)" description:"序号"`
	BlklistNo            string `orm:"column(blklist_no);size(32)" description:"黑名单编号  生成规则:应用2位+机构4位+序号6位举例:应用:il'智能贷款',机构:0401生成贷款账号:il0401000001"`
	BizSn                string `orm:"column(biz_sn);size(32);null" description:"业务流水号"`
	SorcDescr            string `orm:"column(sorc_descr);size(200);null" description:"来源描述"`
	BlklistTypCd         string `orm:"column(blklist_typ_cd);size(1);null" description:"黑名单类型代码  01-征信黑名单02-同行黑名单03-冻结黑名单04-行业黑名单05-地区黑名单06-法院黑名单"`
	CustNmlstNewaddTypCd string `orm:"column(cust_nmlst_newadd_typ_cd);size(1);null" description:"客户名单新增类型代码  1-导入,2-加入"`
	RecmndEmpnbr         string `orm:"column(recmnd_empnbr);size(6);null" description:"推荐人员工号"`
	RecmndNm             string `orm:"column(recmnd_nm);size(6);null" description:"推荐人姓名"`
	MobileNo             string `orm:"column(mobile_no);size(6);null" description:"手机号"`
	CustNo               string `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,crm兼顾启信宝eid长度20"`
	CustName             string `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd        string `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:cd00153"`
	IndvCrtfNo           string `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	BlklistCustClsfCd    string `orm:"column(blklist_cust_clsf_cd);size(2);null" description:"黑名单客户分类代码  1-内控客户2-禁入客户"`
	BckltRsnCd           string `orm:"column(bcklt_rsn_cd);size(2);null" description:"拉黑原因代码  1.客户态度恶劣,刁难我行员工,大肆破坏我行设备和营业环境,带来不良社会影响,2.生活恶习'赌博,家庭暴力,打架斗殴等'3.人品败坏'不孝父母,不抚养子女等'4.不诚信'恶意拖欠工资,税款等'5.民间借贷'高额借贷,投机倒把等'6.,从事非法活动'诈骗,偷盗等'7.,其他原因'选择本项目,需手工输入'"`
	BckltRsnComnt        string `orm:"column(bcklt_rsn_comnt);size(200);null" description:"拉黑原因说明"`
	KeprcdStusCd         string `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	DeregisRsn           string `orm:"column(deregis_rsn);size(200);null" description:"注销原因"`
	CrtDt                string `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	CrtEmpnbr            string `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo             string `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt          string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm          string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo       string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo      string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_blacklist_info) TableName() string {
	return "t_loan_blacklist_info"
}

func InsertT_loan_blacklist_info(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_blacklist_info where org_no=? and blklist_no=?"
		o.Raw(sql, maps["OrgNo"], maps["BlklistNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_blacklist_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_blacklist_infoTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_blacklist_info where org_no=? and blklist_no=?"
		o.Raw(sql, maps["OrgNo"], maps["BlklistNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_blacklist_info)).Filter("org_no", maps["OrgNo"]).
		Filter("seq_no", maps["SeqNo"]).Filter("blklist_no", maps["BlklistNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_blacklist_infoTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_blacklist_info where org_no=? and blklist_no=?"
		o.Raw(sql, maps["OrgNo"] ,maps["BlklistNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_blacklist_info where org_no=? and seq_no=? and blklist_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["OrgNo"], maps["SeqNo"], maps["BlklistNo"]).Exec()
	return err
}

func QueryT_loan_blacklist_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_blacklist_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_blacklist_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo && k != "Flag" {
			if k == "StartDate" {
				qs = qs.Filter("crt_dt__gte", v)
			} else if k == "EndDate" {
				qs = qs.Filter("crt_dt__lte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if params["Flag"]=="9"{
		if _, err = qs.Filter("tcc_state", state).Limit(-1).Filter("keprcd_stus_cd__in","1","3").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).OrderBy("-crt_dt").Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_blacklist_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_blacklist_info)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("blklist_no", maps["BlklistNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_blacklist_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_blacklist_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_blacklist_info)).Filter("org_no", maps["OrgNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("blklist_no", maps["BlklistNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
