package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_loan_extn_regis struct {
	LoanDubilNo        string    `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号"`
	SeqNo              int       `orm:"column(seq_no)" description:"序号"`
	CustNo             string    `orm:"column(cust_no);size(12);null" description:"客户编号"`
	LoanProdtNo        string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号"`
	CtrtNo             string    `orm:"column(ctrt_no);size(32);null" description:"合同编号"`
	BizAplyNo          string    `orm:"column(biz_aply_no);size(20);null" description:"贷款展期业务申请编号"`
	UpdStusCd          string    `orm:"column(upd_stus_cd);size(2);null" description:"变更状态代码 1-待审核 2-审核通过 3-审核拒绝 4-待审查 5-审查通过 6-审查拒绝 7-待审批 8-审批通过 9-审批拒绝 10-合同拟制 11-合同签订"`
	BefBegnDt          string    `orm:"column(bef_begn_dt);type(date);null" description:"展期前开始日期"`
	BefMatrDt          string    `orm:"column(bef_matr_dt);type(date);null" description:"展期前到期日期"`
	AftBegnDt          string    `orm:"column(aft_begn_dt);type(date);null" description:"展期后开始日期"`
	AftMatrDt          string    `orm:"column(aft_matr_dt);type(date);null" description:"展期后到期日期"`
	BefIntrt           float64   `orm:"column(bef_intrt);null;digits(9);decimals(6)" description:"展期前贷款利率"`
	IntrtAdjFlag       string    `orm:"column(intrt_adj_flag);size(1);null" description:"是否调整利率 0-否 1-是"`
	IntrtTypCd         string    `orm:"column(intrt_typ_cd);size(10);null" description:"展期前利率类别"`
	AdjIntrtTypCd      string    `orm:"column(adj_intrt_typ_cd);size(10);null" description:"展期后利率类别"`
	LoanIntrtAdjManrCd string    `orm:"column(loan_intrt_adj_manr_cd);size(1);null" description:"利率调整方式 1-百分比 2基点"`
	IntrtFlotDrctCd    string    `orm:"column(intrt_flot_drct_cd);size(1);null" description:"利率调整方向 D-下调 U-上调"`
	ChgRintrtFlotRatio float64   `orm:"column(chg_rintrt_flot_ratio);null;digits(9);decimals(6)" description:"利率调整百分比"`
	ChgBpFlotVal       float64   `orm:"column(chg_bp_flot_val);null;digits(9);decimals(6)" description:"利率调整BP点"`
	AftIntrt           float64   `orm:"column(aft_intrt);null;digits(9);decimals(6)" description:"展期后调整利率"`
	BefRepayManrCd     string    `orm:"column(bef_repay_manr_cd);size(3);null" description:"展期前还款方式代码 01-按月等额本息还款法 02-按月等额本金还款法 03-按月结息、到期一次性还本法 10-按季等额本息还款法 11-按季等额本金还款法 12-按季结息、到期一次性还本法 21-按年结息（12月）、到期一次性还本法 22-按年结息（6月）、到期一次性还本法 23-按年结息（9月）、到期一次性还本法 31-按计划还本、按月结息法 32-按计划还本、按季结息法 33-按计划还本、按年结息法 34-按计划还本、利随本清 90-利随本清 99-带宽限期的等额本息还款方式"`
	AftRepayManrCd     string    `orm:"column(aft_repay_manr_cd);size(3);null" description:"展期后还款方式代码 01-按月等额本息还款法 02-按月等额本金还款法 03-按月结息、到期一次性还本法 10-按季等额本息还款法 11-按季等额本金还款法 12-按季结息、到期一次性还本法 21-按年结息（12月）、到期一次性还本法 22-按年结息（6月）、到期一次性还本法 23-按年结息（9月）、到期一次性还本法 31-按计划还本、按月结息法 32-按计划还本、按季结息法 33-按计划还本、按年结息法 34-按计划还本、利随本清 90-利随本清 99-带宽限期的等额本息还款方式"`
	BefCycCd           string    `orm:"column(bef_cyc_cd);size(2);null" description:"展期前周期代码 01-按天 02-按周 03-按半月/双周 04-按月 05-按双月 06-按季 07-按半年 08-按年"`
	BefCycQty          int       `orm:"column(bef_cyc_qty);null" description:"展期前周期数量"`
	AftCycCd           string    `orm:"column(aft_cyc_cd);size(3);null" description:"展期周期代码 01-按天 02-按周 03-按半月/双周 04-按月 05-按双月 06-按季 07-按半年 08-按年"`
	AftCycQty          int       `orm:"column(aft_cyc_qty);null" description:"展期周期数量"`
	AdjDate            string    `orm:"column(adj_date);type(date);null" description:"申请展期日期"`
	AdjReason          string    `orm:"column(adj_reason);type(date);null" description:"申请展期原因"`
	AplyOrgNo          string    `orm:"column(aply_org_no);size(20);null" description:"经办机构编号"`
	AdjAftBpFlotVal    string    `orm:"column(adj_aft_bp_flot_val);size(20);null" description:"经办人员工编号"`
	AdjBefFlotRatio    string    `orm:"column(adj_bef_flot_ratio);size(60);null" description:"经办人名称"`
	UpdCtrtNo          string    `orm:"column(upd_ctrt_no);size(32);null" description:"展期协议合同编号"`
	FinlModfyDt        string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm        string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo     string    `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo    string    `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState           int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_extn_regis) TableName() string {
	return "t_loan_extn_regis"
}

func InsertT_loan_extn_regis(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_extn_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_loan_extn_regis).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_extn_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_extn_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_loan_extn_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_extn_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_loan_extn_regis where loan_dubil_no=?"
		o.Raw(sql, maps["LoanDubilNo"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from t_loan_extn_regis where loan_dubil_no=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["SeqNo"]).Exec()
	return err
}

func QueryT_loan_extn_regis(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_extn_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_extn_regis))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_extn_regisById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_extn_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_extn_regis(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_extn_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_extn_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("seq_no", maps["SeqNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
