package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_artif_interset_rate_flow struct {
	IntrtAdjSn         string    `orm:"column(intrt_adj_sn);pk" description:"利率调整流水号  生成规则:应用2位,+,日期时间14位'YYYYMMDDHHMMSS'+,随机整数8位举例:应用:IL'智能贷款',类型:IMLN-减值贷款,日期时间:20191014235501生成贷款账号:IL2019101423550108223812"`
	CustNo            string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName          string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	CrdtAplySn        string    `orm:"column(crdt_aply_sn);size(32);null" description:"授信申请流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,序号6位+,尾号2位编号规则:1',序号顺序生成2',如单元化方案要求key值包含分片ID,则编号最后两位为分片ID3',如单元化方案key值不包含分片ID,编号中拼接的序号最后两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'"`
	IndvCrtfTypCd     string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo        string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	BaseIntrtTypCd    string    `orm:"column(base_intrt_typ_cd);size(10);null" description:"基础利率类型代码  1-人行基准利率,2-LPR利率"`
	CurCd             string    `orm:"column(cur_cd);size(4);null" description:"币种代码"`
	IntrtNo           string    `orm:"column(intrt_no);size(40);null" description:"利率编号"`
	IntrtFlotDrctCd   string    `orm:"column(intrt_flot_drct_cd);size(1);null" description:"利率浮动方向代码  D-下调,U-上调"`
	BpFlotVal         float64   `orm:"column(bp_flot_val);null" description:"bp浮动值"`
	KeprcdStusCd      string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	CrtTelrNo         string    `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtTm             string    `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	CrtOrgNo          string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	ApprvSugstnCd     string    `orm:"column(apprv_sugstn_cd);size(3);null" description:"审批意见代码  1-同意,2-退回,3-终止,4-弃权,5-拒绝"`
	ApprvSugstnComnt  string    `orm:"column(apprv_sugstn_comnt);size(100);null" description:"审批意见说明"`
	ApprvTelrNo       string    `orm:"column(apprv_telr_no);size(6);null" description:"审批柜员号"`
	ApprvTm           string    `orm:"column(apprv_tm);type(time);null" description:"审批时间"`
	ApprvOrgNo        string    `orm:"column(apprv_org_no);size(4);null" description:"审批机构号"`
	IntrtFlotManrCd   string    `orm:"column(intrt_flot_manr_cd);size(2);null" description:"利率浮动方式代码  P-浮动百分比,N-浮动点数记录利率浮动方式,如:按上限浮动,按下限浮动,按浮动比例浮动,等,"`
	IntrtFlotRatio    float64   `orm:"column(intrt_flot_ratio);null;digits(9);decimals(6)" description:"利率浮动比例"`
	AdjRsn    		  string    `orm:"column(adj_rsn);size(200);decimals(6)" description:"调整原因"`
	CrtTelrName 	  string    `orm:"column(crt_telr_name);size(200);decimals(6)" description:"经办人名称"`
	ApprvTelrName     string    `orm:"column(apprv_telr_name);size(200);decimals(6)" description:"审批人名称"`
	FinlModfyDt       string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm       string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo    string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo   string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState           int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_artif_interset_rate_flow) TableName() string {
	return "t_artif_interset_rate_flow"
}

func InsertT_artif_interset_rate_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_artif_interset_rate_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_artif_interset_rate_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_artif_interset_rate_flow)).Filter("intrt_adj_sn", maps["IntrtAdjSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_artif_interset_rate_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_artif_interset_rate_flow where intrt_adj_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["IntrtAdjSn"]).Exec()
	return err
}

func QueryT_artif_interset_rate_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_artif_interset_rate_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params,"Flag")
	o := orm.NewOrm()
	if flag=="9"{
		sql := "SELECT t1.*,t2.`qta_stus_cd`,t3.`loan_prodt_nm` FROM t_artif_interset_rate_flow t1,t_loan_quota_mgmt t2 LEFT JOIN t_loan_product t3 ON t2.`loan_prodt_no`=t3.`loan_prodt_no` WHERE t1.`crdt_aply_sn`=t2.`crdt_aply_sn`"
		if params["CrdtAplySn"]!=nil && params["CrdtAplySn"] !="" {		//授信申请流水号
			sql =sql +" and crdt_aply_sn=\""+params["CrdtAplySn"].(string)+"\""
		}
		if params["QtaStusCd"]!=nil && params["QtaStusCd"] !="" {	//额度状态
			sql =sql +" and t2.qta_stus_cd=\""+params["QtaStusCd"].(string)+"\""
		}
		if params["CustNo"]!=nil && params["CustNo"] !="" {			//客户编号
			sql =sql +" and t1.cust_no=\""+params["CustNo"].(string)+"\""
		}
		if params["CustName"]!=nil && params["CustName"] !="" {		//客户姓名
			sql =sql +" and t1.cust_name like \"%"+params["CustName"].(string)+"%\""
		}
		if params["IndvCrtfTypCd"]!=nil && params["IndvCrtfTypCd"] !="" {	//证件类型
			sql =sql +" and t1.IndvCrtfTypCd=\""+params["IndvCrtfTypCd"].(string)+"\""
		}
		if params["IndvCrtfNo"]!=nil && params["IndvCrtfNo"] !="" {			//证件号码
			sql =sql +" and t1.indv_crtf_no=\""+params["IndvCrtfNo"].(string)+"\""
		}
		if params["KeprcdStusCd"]!=nil && params["KeprcdStusCd"] !="" {		//处理状态
			sql =sql +" and t1.keprcd_stus_cd=\""+params["KeprcdStusCd"].(string)+"\""
		}
		o.Raw(sql).Values(&maps)
		return
	}
	qs := o.QueryTable(new(T_artif_interset_rate_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag=="8"{
		if _, err = qs.Filter("tcc_state", state).Limit(1).OrderBy("-finl_modfy_dt","-finl_modfy_tm").Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_artif_interset_rate_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_artif_interset_rate_flow)).Filter("intrt_adj_sn", maps["IntrtAdjSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_artif_interset_rate_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_artif_interset_rate_flow panic"+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_artif_interset_rate_flow)).Filter("intrt_adj_sn", maps["IntrtAdjSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
