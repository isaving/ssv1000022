package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_repay_record struct {
	TxSn            string    `orm:"column(tx_sn);pk" description:"交易流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,随机整数8位编号规则:1',随机数生成:(int)(Math.random()*100000000)"`
	QtaChgSn        string    `orm:"column(qta_chg_sn);null" description:"序号  10某期进行多次还款时,序号从1开始加1递增"`
	LoanDubilNo     string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	PeriodNum       int       `orm:"column(period_num);null" description:"期次"`
	SeqNo           int       `orm:"column(seq_no);null" description:"序号  10某期进行多次还款时,序号从1开始加1递增"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号  记录银行为统一管理,根据既定规则生成并分配给客户的唯一编码,在全行内具有唯一性,CRM兼顾启信宝EID长度"`
	CustName        string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd   string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo      string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	AcctiAcctNo     string    `orm:"column(accti_acct_no);size(32);null" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo string    `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	AcctnDt         string    `orm:"column(acctn_dt);type(date);null" description:"会计日期  记账的会计日期"`
	RepayAcctNo     string    `orm:"column(repay_acct_no);size(32);null" description:"还款账号  用于还款的结算账户账号,"`
	RepayTotlAmt    float64   `orm:"column(repay_totl_amt);null;digits(22);decimals(2)" description:"还款总金额  公共服务提供类型及长度"`
	RepbyTotlAmt    float64   `orm:"column(repby_totl_amt);null;digits(22);decimals(2)" description:"应还总金额"`
	RepayPrin       float64   `orm:"column(repay_prin);null;digits(18);decimals(2)" description:"还款本金  公共服务提供类型及长度"`
	RepayIntr       float64   `orm:"column(repay_intr);null;digits(18);decimals(2)" description:"还款利息  公共服务提供类型及长度"`
	RepayPnltint    float64   `orm:"column(repay_pnltint);null;digits(18);decimals(2)" description:"还款罚息  公共服务提供类型及长度"`
	RepayCmpndint   float64   `orm:"column(repay_cmpndint);null;digits(18);decimals(2)" description:"还款复息  公共服务提供类型及长度"`
	RepayCmpdAmt    float64   `orm:"column(repay_cmpd_amt);null;digits(18);decimals(2)" description:"还款复利金额  公共服务提供类型及长度"`
	RepayPondg      float64   `orm:"column(repay_pondg);null;digits(18);decimals(2)" description:"还款手续费  公共服务提供类型及长度"`
	RepayOthFee     float64   `orm:"column(repay_oth_fee);null;digits(18);decimals(2)" description:"还款其他费用  公共服务提供类型及长度"`
	RepayCurCd      string    `orm:"column(repay_cur_cd);size(3);null" description:"还款币种代码  参考标准代码:CD0040公共服务提供类型及长度"`
	RepayStusCd     string    `orm:"column(repay_stus_cd);size(2);null" description:"还款状态代码  0:还款申请已受理1:还款成功2:还款冲正,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,3:不足额还款9:还款失败"`
	FailRsn         string    `orm:"column(fail_rsn);size(200);null" description:"失败原因  还款失败时的原因信息"`
	RepayFuncCd     string    `orm:"column(repay_func_cd);size(4);null" description:"还款功能代码  01-提前部分还款02-提前结清还款03-逾期转正常还款04-正常还款'还当期'05-月结还款06-逾期追扣07-减免还款11-减值收回12-部分减值收回13-核销收回"`
	RepayAplyDt     string    `orm:"column(repay_aply_dt);type(date);null" description:"还款申请日期"`
	ActlRepayDt     string    `orm:"column(actl_repay_dt);type(date);null" description:"实际还款日期  实际还款日期"`
	CrtTm           string    `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	TranType        string    `orm:"column(tran_type);type(time);null" description:"交易类型 03.RPYM  04.DISB "`
	TranDesc        string    `orm:"column(tran_desc);type(time);null" description:"交易详情 1.Repayment  2.Disbursement"`
	OsgPrin         string    `orm:"column(osg_prin);type(time);null" description:"未还本金"`
	OsgIntr         string    `orm:"column(osg_intr);type(time);null" description:"未还利息"`
	Fee             string    `orm:"column(fee);type(time);null" description:"费用"`
	CrtEmpnbr       string    `orm:"column(crt_empnbr);size(6);null" description:"创建员工号"`
	CrtOrgNo        string    `orm:"column(crt_org_no);size(4);null" description:"创建机构号"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_repay_record) TableName() string {
	return "t_loan_repay_record"
}
func InsertT_loan_repay_record_No_Tcc(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSqlTcc(maps, new(T_loan_repay_record).TableName(),0)
	_, err := o.Raw(sql).Exec()
	return err
}

func InsertT_loan_repay_record(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_repay_record).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_repay_recordTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_repay_record)).Filter("tx_sn", maps["TxSn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_repay_recordTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_repay_record where tx_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["TxSn"]).Exec()
	return err
}

func QueryT_loan_repay_record(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_repay_record panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params,"Flag")
	qs := o.QueryTable(new(T_loan_repay_record))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9" {	//lxf
		if _, err = qs.Filter("tcc_state", state).OrderBy("-seq_no","-acctn_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_repay_recordById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_repay_record)).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_repay_record(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_repay_record panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_repay_record)).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
