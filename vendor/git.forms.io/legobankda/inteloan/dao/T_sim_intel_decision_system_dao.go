package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sim_intel_decision_system struct {
	CrdtAplySn		string       `orm:"column(crdt_aply_sn);pk;null"`
	SeqNo           int          `orm:"column(seq_no);null"`
	LoanDeadl       int          `orm:"column(loan_deadl);null"`
	RepayManrCd     string       `orm:"column(repay_manr_cd);null"`
	//TccState         int         `orm:"column(tcc_state);null"`
}

func (t *T_sim_intel_decision_system) TableName() string {
	return "t_sim_intel_decision_system"
}

func InsertT_sim_intel_decision_system(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sim_intel_decision_system).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sim_intel_decision_systemTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sim_intel_decision_system)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sim_intel_decision_systemTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_sim_intel_decision_system where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryT_sim_intel_decision_system(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sim_intel_decision_system panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_sim_intel_decision_system))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sim_intel_decision_systemById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sim_intel_decision_system)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sim_intel_decision_system(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sim_intel_decision_system panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sim_intel_decision_system)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
