package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type Ts_sys_user_role struct {
	//Id        	  string `orm:"column(id);pk;size(10)"`
	UserId        string `orm:"column(user_id);pk;size(10)" description:"用户ID"`
	RoleId        string `orm:"column(role_id);size(10)" description:"角色代码"`
	//UpdateAt      string `orm:"column(update_at);size(10)"`
	//LastMaintDate string `orm:"column(last_maint_date);size(10);null" description:"最后更新日期"`
	//LastMaintTime string `orm:"column(last_maint_time);size(19);null" description:"最后更新时间"`
	//LastMaintBrno string `orm:"column(last_maint_brno);size(20);null" description:"最后更新机构"`
	//LastMaintTell string `orm:"column(last_maint_tell);size(30);null" description:"最后更新柜员"`
	//TccState      int    `orm:"column(tcc_state);null"`
}

func (t *Ts_sys_user_role) TableName() string {
	return "um_user_role_relation"
}

func InsertTs_sys_user_role(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Ts_sys_user_role).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateTs_sys_user_roleTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Ts_sys_user_role)).Filter("user_id", maps["UserId"]).
		Filter("role_id", maps["RoleId"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteTs_sys_user_roleTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from ts_sys_user_role where user_id=? and role_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["UserId"], maps["RoleId"]).Exec()
	return err
}

func QueryTs_sys_user_role(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryTs_sys_user_role panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Ts_sys_user_role))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Limit(-1).Values(&maps,"user_id","role_id"); err != nil {
		return nil, err
	}
	return
}

func QueryTs_sys_user_roleById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Ts_sys_user_role)).Filter("user_id", maps["UserId"]).Filter("role_id", maps["RoleId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateTs_sys_user_role(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateTs_sys_user_role panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Ts_sys_user_role)).Filter("user_id", maps["UserId"]).Filter("role_id", maps["RoleId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
