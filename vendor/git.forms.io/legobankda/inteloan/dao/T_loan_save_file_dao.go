package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_save_file struct {
	FileType        string    `orm:"column(file_type);size(32);null"`
	FileNm          string    `orm:"column(file_nm);size(32);null"`
	ObjectId        string    `orm:"column(object_id);pk" description:"文件编号"`
	FileInfo        string    `orm:"column(file_info);null"`
	Remark          string    `orm:"column(remark);size(32);null"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(20);null"`
	TccState        int8      `orm:"column(tcc_state);null"`
}

func (t *T_loan_save_file) TableName() string {
	return "t_loan_save_file"
}

func InsertT_loan_save_file(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_save_file).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_save_fileTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_save_file)).Filter("object_id", maps["ObjectId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_save_fileTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_loan_save_file where object_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ObjectId"]).Exec()
	return err
}

func QueryT_loan_save_file(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_save_file panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_save_file))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_save_fileById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_save_file)).Filter("object_id", maps["ObjectId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_save_file(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_save_file panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_save_file)).Filter("object_id", maps["ObjectId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
