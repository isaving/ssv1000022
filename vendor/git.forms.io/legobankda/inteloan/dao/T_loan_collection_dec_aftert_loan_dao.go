package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_collection_dec_aftert_loan struct {
	CollTaskNo               string    `orm:"column(coll_task_no);pk" description:"催收任务编号"`
	CurrLoanRiskClsfCd       string    `orm:"column(curr_loan_risk_clsf_cd);size(2);null" description:"当前贷款风险分类代码"`
	DlqcyProb                float64   `orm:"column(dlqcy_prob);null;digits(9);decimals(6)" description:"违约概率"`
	RiskRsn                  string    `orm:"column(risk_rsn);size(200);null" description:"风险原因"`
	SggestCollMgmtManrCd     string    `orm:"column(sggest_coll_mgmt_manr_cd);size(2);null" description:"建议催收管理方式代码  等待决策贯标结果"`
	AfBnkLnModelExecDt       string    `orm:"column(af_bnk_ln_model_exec_dt);type(date);null" description:"贷后模型执行日期"`
	CustName                 string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	CustNo                   string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	IndvCrtfNo               string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	IndvCrtfTypCd            string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD0015"`
	LoanDubilNo              string    `orm:"column(loan_dubil_no);size(32);null" description:"贷款借据号"`
	SggestCollExecWindowDays int       `orm:"column(sggest_coll_exec_window_days);null" description:"建议催收执行窗口天数"`
	FinlModfyDt              string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  19"`
	FinlModfyTm              string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo           string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo          string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState                  int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_collection_dec_aftert_loan) TableName() string {
	return "t_loan_collection_dec_aftert_loan"
}

func InsertT_loan_collection_dec_aftert_loan(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_collection_dec_aftert_loan).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_collection_dec_aftert_loanTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_collection_dec_aftert_loan)).Filter("coll_task_no", maps["CollTaskNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_collection_dec_aftert_loanTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_collection_dec_aftert_loan where coll_task_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CollTaskNo"]).Exec()
	return err
}

func QueryT_loan_collection_dec_aftert_loan(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_collection_dec_aftert_loan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_collection_dec_aftert_loan))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_collection_dec_aftert_loanById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_collection_dec_aftert_loan)).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_collection_dec_aftert_loan(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_collection_dec_aftert_loan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_collection_dec_aftert_loan)).Filter("coll_task_no", maps["CollTaskNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
