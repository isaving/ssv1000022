package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_prod_model_para struct {
	KeprcdNo        string `orm:"column(keprcd_no);size(30);pk" description:"记录编号"`
	DocTmplTypCd    string `orm:"column(doc_tmpl_typ_cd);size(50)" description:"文档模板类型代码  LOAN_CONTRACT:借款合同,GUART_CONTRACT:担保合同,OTHER_TEMP_TYP:其他模板"`
	LoanProdtNo     string `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	TmplSubTypCd    string `orm:"column(tmpl_sub_typ_cd);size(2);null" description:"模板子类型代码  01-额度借款合同02-个人征信授权书03-个人信息查询及使用授权书04-公积金信息查询协议05-纳税信息授权委托书06-蜀信e贷个人借款合同"`
	KeprcdStusCd    string `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	OrgNo           string `orm:"column(org_no);size(4);null" description:"机构号  描述银行为统一管理,根据既定规则生成并分配给内部机构的唯一编码,在全行内具有唯一性"`
	EfftDt          string `orm:"column(efft_dt);type(date);null" description:"生效日期"`
	CrtTelrNo       string `orm:"column(crt_telr_no);size(6);null" description:"创建柜员号"`
	CrtTm           string `orm:"column(crt_tm);type(time);null" description:"创建时间"`
	FinlModfyDt     string `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState        int8   `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_prod_model_para) TableName() string {
	return "t_prod_model_para"
}

func InsertT_prod_model_para(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_prod_model_para).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_prod_model_paraTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_prod_model_para)).Filter("keprcd_no", maps["KeprcdNo"]).
		Filter("doc_tmpl_typ_cd", maps["DocTmplTypCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_prod_model_paraTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_prod_model_para where keprcd_no=? and doc_tmpl_typ_cd=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"], maps["DocTmplTypCd"]).Exec()
	return err
}

func QueryT_prod_model_para(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_prod_model_para panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params, "Flag")
	if flag == "9" {
		maps, err = QueryT_prod_model_paraFlag9(params)
		return
	}
	qs := o.QueryTable(new(T_prod_model_para))
	if flag == "8" {
		maps, err = QueryT_prod_model_paraFlag8(params)
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			if k == "EfftStartDate" {
				qs = qs.Filter("efft_dt__gte", v)
			} else if k == "EfftEndDate" {
				qs = qs.Filter("efft_dt__lte", v)
			} else {
				qs = qs.Filter(k, v)
			}
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_prod_model_paraById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}

	flag := maps["Flag"]
	if flag == "3" {
		maps, err = QueryT_prod_model_paraByIdFlag3(maps, 0)
		return
	} else {
		if _, err = o.QueryTable(new(T_prod_model_para)).Filter("keprcd_no", maps["KeprcdNo"]).Filter("doc_tmpl_typ_cd", maps["DocTmplTypCd"]).
			Filter("tcc_state", state).Values(&mapList); err != nil {
			return nil, err
		}
	}

	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_prod_model_para(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_prod_model_para panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()

	flag := maps["Flag"]
	log.Debugf("flag == %v", flag)
	delete(maps, "Flag")
	if flag == "3" {
		log.Debugf("flag == 3")
		err = UpdateT_prod_model_paraFlag3(maps)
		return
	} else {
		log.Debugf("According to PrimaryKey")
		_, err = o.QueryTable(new(T_prod_model_para)).Filter("keprcd_no", maps["KeprcdNo"]).Filter("doc_tmpl_typ_cd", maps["DocTmplTypCd"]).
			Filter("tcc_state", 0).Update(maps)
		return
	}

	return err
}
func DeleteT_prod_model_para(params orm.Params) (err error) {
	o := orm.NewOrm()
	KeprcdNo := params["KeprcdNo"]
	DocTmplTypCd := params["DocTmplTypCd"]
	sql := "delete from T_prod_model_para where keprcd_no=? and doc_tmpl_typ_cd=? and tcc_state = 0"
	_, err = o.Raw(sql, KeprcdNo, DocTmplTypCd).Exec()
	return
}

func QueryT_prod_model_paraFlag9(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT * FROM (" +
		"select t1.keprcd_no KeprcdNo,t1.loan_prodt_no LoanProdtNo,t1.efft_dt EfftDt,t1.org_no OrgNo,t1.crt_telr_no CrtTelrNo," +
		"t1.crt_tm CrtTm,t1.keprcd_stus_cd KeprcdStusCd,t2.loan_prodt_nm LoanProdtNm " +
		" from t_prod_model_para t1 left join t_loan_product t2 on t1.loan_prodt_no=t2.loan_prodt_no " +
		") t3 where 1=1 "
	if params["KeprcdNo"] != nil {
		sql = sql + " and t3.KeprcdNo=\"" + params["KeprcdNo"].(string) + "\""
	}
	if params["EfftStartDate"] != nil {
		sql = sql + " and t3.EfftDt>=\"" + params["EfftStartDate"].(string) + "\""
	}
	if params["EfftEndDate"] != nil {
		sql = sql + " and t3.EfftDt<=\"" + params["EfftEndDate"].(string) + "\""
	}
	if params["LoanProdtNm"] != nil {
		sql = sql + " and t3.LoanProdtNm like \"%" + params["LoanProdtNm"].(string) + "%\""
	}
	if params["KeprcdStusCd"] != nil {
		sql = sql + " and t3.KeprcdStusCd=\"" + params["KeprcdStusCd"].(string) + "\""
	}
	sql = sql + " GROUP BY LoanProdtNo ORDER BY LoanProdtNo"
	_, err = o.Raw(sql).Values(&maps)
	return
}
func QueryT_prod_model_paraFlag8(params orm.Params) (maps []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  " +
		"t1. keprcd_stus_cd KeprcdStusCd,  " +
		"t1.`efft_dt` EfftDt, " +
		"t1.`keprcd_no` KeprcdNo, " +
		"t1.`crt_telr_no` CrtTelrNo, " +
		"t1.`crt_tm` CrtTm, " +
		"t1.org_no OrgNo, " +
		"t1.`doc_tmpl_typ_cd` TmplTypCd, " +
		"t1.`tmpl_sub_typ_cd` TmplSubTypCd, " +
		"t2.`tmpl_pdf_file_id` TmplPdfFileId, " +
		"t2.`tmpl_file_id` TmplFileId, " +
		"t3.`loan_prodt_no` LoanProdtNo, " +
		"t3.`loan_prodt_nm` LoanProdtNm, " +
		"t2.tmpl_nm  TmplNm " +
		"FROM t_prod_model_para t1 LEFT JOIN t_loan_elec_cont_tmpl t2 " +
		"ON t1.doc_tmpl_typ_cd=t2.doc_tmpl_typ_cd AND t1.tmpl_sub_typ_cd=t2.tmpl_sub_typ_cd " +
		"LEFT JOIN t_loan_product t3 ON t1.`loan_prodt_no`=t3.`loan_prodt_no` " +
		"WHERE t1.keprcd_stus_cd=01 AND t1.loan_prodt_no=? " +
		"GROUP BY t1.`keprcd_no`"
	_, err = o.Raw(sql, params["LoanProdtNo"]).Values(&maps)
	return
}

func QueryT_prod_model_paraByIdFlag3(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_prod_model_para)).Filter("keprcd_no", maps["KeprcdNo"]).Filter("loan_prodt_no", maps["LoanProdtNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_prod_model_paraFlag3(maps orm.Params) (err error) {
	log.Debugf("UpdateT_prod_model_paraFlag3 start")
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_prod_model_para panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_prod_model_para)).Filter("keprcd_no", maps["KeprcdNo"]).Filter("loan_prodt_no", maps["LoanProdtNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
