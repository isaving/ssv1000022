package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type T_intrt_chg_regis struct {
	LoanDubilNo         string    `orm:"column(loan_dubil_no);size(32);pk" description:"借据号"`
	AdjDt               string    `orm:"column(adj_dt);type(date)" description:"变更日期"`
	SeqNo               int       `orm:"column(seq_no)" description:"序号"`
	DvalBizAplyNo       string    `orm:"column(dval_biz_aply_no);size(20);null" description:"利率变更业务申请编号"`
	DvalDlwthStusCd     string    `orm:"column(dval_dlwth_stus_cd);size(2);null" description:"利率变更状态代码"`
	CtrtNo              string    `orm:"column(ctrt_no);size(32);null" description:"合同编号"`
	CustNo              string    `orm:"column(cust_no);size(12);null" description:"客户编号"`
	CustName            string    `orm:"column(cust_name);size(60);null" description:"客户名称"`
	LoanProdtNo         string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号"`
	LoanProdtNm         string    `orm:"column(loan_prodt_nm);size(60);null" description:"贷款产品名称"`
	GrantDt             string    `orm:"column(grant_dt);type(date);null" description:"发放日期"`
	MatrDt              string    `orm:"column(matr_dt);type(date);null" description:"到期日期"`
	LoanIntrtAdjManrCd  string    `orm:"column(loan_intrt_adj_manr_cd);size(1);null" description:"利率调整方式"`
	IntrtFlotDrctCd     string    `orm:"column(intrt_flot_drct_cd);size(1);null" description:"利率调整方向"`
	ChgRintrtFlotRatio  float64   `orm:"column(chg_rintrt_flot_ratio);null;digits(9);decimals(6)" description:"调整百分比"`
	ChgBpFlotVal        float64   `orm:"column(chg_bp_flot_val);null;digits(9);decimals(6)" description:"调整基点"`
	AdjAftIntrt         float64   `orm:"column(adj_aft_intrt);null;digits(9);decimals(6)" description:"调整后利率"`
	AdjDate             string    `orm:"column(adj_date);type(date);null" description:"调整日期"`
	AdjReason           string    `orm:"column(adj_reason);null" description:"调整原因"`
	AdjAftBpFlotVal     string    `orm:"column(adj_aft_bp_flot_val);size(20);null" description:"经办人员工编号"`
	AdjBefFlotRatio     string    `orm:"column(adj_bef_flot_ratio);size(60);null" description:"经办人名称"`
	ExmnvrfyStusCd      string    `orm:"column(exmnvrfy_stus_cd);size(1);null" description:"审核意见"`
	ExmnvrfySugstnComnt string    `orm:"column(exmnvrfy_sugstn_comnt);null" description:"审核意见说明"`
	ApprvSugstnCd       string    `orm:"column(apprv_sugstn_cd);size(1);null" description:"审批意见"`
	ApprvSugstnComnt    string    `orm:"column(apprv_sugstn_comnt);null" description:"审批意见说明"`
	FinlModfyDt         string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm         string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo      string    `orm:"column(finl_modfy_org_no);size(20);null" description:"最后修改机构号"`
	FinlModfyTelrNo     string    `orm:"column(finl_modfy_telr_no);size(20);null" description:"最后修改柜员号"`
	TccState            int       `orm:"column(tcc_state);null"`
}

func (t *T_intrt_chg_regis) TableName() string {
	return "t_intrt_chg_regis"
}

func InsertT_intrt_chg_regis(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_intrt_chg_regis where loan_dubil_no=? and adj_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := util.BuildSql(maps, new(T_intrt_chg_regis).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_intrt_chg_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_intrt_chg_regis where loan_dubil_no=? and adj_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	_, err := o.QueryTable(new(T_intrt_chg_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("adj_dt", maps["AdjDt"]).Filter("seq_no", maps["SeqNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_intrt_chg_regisTccState(o orm.Ormer, maps orm.Params) error {
	if maps["SeqNo"] == nil {
		mapOne := []orm.Params{}
		sql := "select max(seq_no)+1 SeqNo from t_intrt_chg_regis where loan_dubil_no=? and adj_dt=?"
		o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"]).Values(&mapOne)
		Seq := mapOne[0]["SeqNo"]
		var SeqNo int
		if Seq == nil {
			SeqNo = 1
		} else {
			SeqNo, _ = strconv.Atoi(Seq.(string))
		}
		maps["SeqNo"] = SeqNo
	}
	sql := "delete from T_intrt_chg_regis where loan_dubil_no=? and adj_dt=? and seq_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["AdjDt"],maps["SeqNo"]).Exec()
	return err
}

func QueryT_intrt_chg_regis(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_intrt_chg_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_intrt_chg_regis))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_intrt_chg_regisById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_intrt_chg_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("adj_dt", maps["AdjDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_intrt_chg_regis(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_intrt_chg_regis panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_intrt_chg_regis)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("adj_dt", maps["AdjDt"]).
		Filter("seq_no", maps["SeqNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
