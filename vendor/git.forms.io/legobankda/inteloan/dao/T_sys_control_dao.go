package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_sys_control struct {
	InstId             string    `orm:"column(inst_id);pk" description:"机构ID"`
	SystemStatus       string    `orm:"column(system_status);size(1);null" description:"系统状态"`
	CurrentProcessDate string    `orm:"column(current_process_date);type(date);null" description:"当期处理日期"`
	LastProcessDate    string    `orm:"column(last_process_date);type(date);null" description:"上一处理日期"`
	NextProcessDate    string    `orm:"column(next_process_date);type(date);null" description:"下一处理日期"`
	FiscalYearEndDate  string    `orm:"column(fiscal_year_end_date);type(date);null" description:"年终结算日期"`
	BatchId            int       `orm:"column(batch_id);null" description:"批量处理ID"`
	DateFormatCode     string    `orm:"column(date_format_code);size(4);null" description:"日期格式"`
	DateSeparator      string    `orm:"column(date_separator);size(4);null" description:"日期分隔符"`
	LastMaintDate      string    `orm:"column(last_maint_date);type(date)" description:"最后更新日期"`
	LastMaintTime      string    `orm:"column(last_maint_time);type(time)" description:"最后更新时间"`
	LastMaintBrno      string    `orm:"column(last_maint_brno);size(20)" description:"最后更新机构"`
	LastMaintTell      string    `orm:"column(last_maint_tell);size(30)" description:"最后更新柜员"`
	TccState           int       `orm:"column(tcc_state);null"`
}

func (t *T_sys_control) TableName() string {
	return "t_sys_control"
}

func InsertT_sys_control(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_sys_control).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_sys_controlTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_sys_control)).Filter("inst_id", maps["InstId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_sys_controlTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from T_sys_control where inst_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["InstId"]).Exec()
	return err
}

func QueryT_sys_control(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_sys_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_sys_control))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_sys_controlById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_sys_control)).Filter("inst_id", maps["InstId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_sys_control(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_sys_control panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_sys_control)).Filter("inst_id", maps["InstId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
