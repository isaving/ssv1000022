package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_cust_busi_tran_rec struct {
	ProcesId              string    `orm:"column(proces_id);pk" description:"流程id  技术字段"`
	DelvrDt               string    `orm:"column(delvr_dt);type(date);null" description:"移交日期"`
	CustNo                string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	CustName              string    `orm:"column(cust_name);size(60);null" description:"客户姓名  记录个人客户的中文名称,对于境内居民应该是经过法律程序登记并能够在公共活动中使用的名称,"`
	IndvCrtfTypCd         string    `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD00153"`
	IndvCrtfNo            string    `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	MobileNo              string    `orm:"column(mobile_no);size(11);null" description:"手机号码"`
	LoanProdtNo           string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo       string    `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	DelvrBefCustMgrEmpnbr string    `orm:"column(delvr_bef_cust_mgr_empnbr);size(6);null" description:"移交前客户经理员工号"`
	DelvrBefOrgNo         string    `orm:"column(delvr_bef_org_no);size(4);null" description:"移交前机构号"`
	DelvrAftCustMgrEmpnbr string    `orm:"column(delvr_aft_cust_mgr_empnbr);size(6);null" description:"移交后客户经理员工号"`
	DelvrAftOrgNo         string    `orm:"column(delvr_aft_org_no);size(4);null" description:"移交后机构号"`
	DelvrPrvlgTypCd       string    `orm:"column(delvr_prvlg_typ_cd);size(5);null" description:"移交权限类型代码  1-管户权2-白名单维护权3-贷后管理权及合作方管理权"`
	DelvrPersonRelCd      string    `orm:"column(delvr_person_rel_cd);size(4);null" description:"移交人关系代码  1-本机构2-同法人机构"`
	DelvrComnt            string    `orm:"column(delvr_comnt);size(200);null" description:"移交说明"`
	FinlModfyDt           string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm           string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo        string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo       string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_cust_busi_tran_rec) TableName() string {
	return "t_cust_busi_tran_rec"
}

func InsertT_cust_busi_tran_rec(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cust_busi_tran_rec).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cust_busi_tran_recTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cust_busi_tran_rec)).Filter("proces_id", maps["ProcesId"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cust_busi_tran_recTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cust_busi_tran_rec where proces_id=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["ProcesId"]).Exec()
	return err
}

func QueryT_cust_busi_tran_rec(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cust_busi_tran_rec panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_cust_busi_tran_rec))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cust_busi_tran_recById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cust_busi_tran_rec)).Filter("proces_id", maps["ProcesId"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cust_busi_tran_rec(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cust_busi_tran_rec panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cust_busi_tran_rec)).Filter("proces_id", maps["ProcesId"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
