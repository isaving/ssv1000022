package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_chan_lend_expend_info struct {
	KeprcdNo        string    `orm:"column(keprcd_no);pk" description:"记录编号"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	CtrtNo          string    `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	ForsprtDt       string    `orm:"column(forsprt_dt);type(date);null" description:"支用日期"`
	ForsprtEquipNo  string    `orm:"column(forsprt_equip_no);size(40);null" description:"支用设备编号"`
	KeprcdStusCd    string    `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效,09-无效"`
	CrtDt           string    `orm:"column(crt_dt);type(date);null" description:"创建日期"`
	ForsprtAmt      float64   `orm:"column(forsprt_amt);null;digits(18);decimals(2)" description:"支用金额"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_chan_lend_expend_info) TableName() string {
	return "t_chan_lend_expend_info"
}

func InsertT_chan_lend_expend_info(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_chan_lend_expend_info).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_chan_lend_expend_infoTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_chan_lend_expend_info)).Filter("KeprcdNo", maps["KeprcdNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_chan_lend_expend_infoTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_chan_lend_expend_info where KeprcdNo=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["KeprcdNo"]).Exec()
	return err
}

func QueryT_chan_lend_expend_info(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_chan_lend_expend_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_chan_lend_expend_info))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_chan_lend_expend_infoById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_chan_lend_expend_info)).Filter("KeprcdNo", maps["KeprcdNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_chan_lend_expend_info(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_chan_lend_expend_info panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_chan_lend_expend_info)).Filter("KeprcdNo", maps["KeprcdNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
