package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"strings"
)

type T_loan_loan_cont struct {
	LoanDubilNo                    string  `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号  智能贷款中贷款账号和借据号相同?27位贷款账号生成规则:合同号+序号(3位)+尾号(2位)编号规则:尾号两位使用6?8(66?68?86?88,4个数字随机拼接到序号最后)序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号?生成贷款账号:040113201910140000016800188"`
	AcctiAcctNo                    string  `orm:"column(accti_acct_no);size(32);null" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	ContrtStusCd                   string  `orm:"column(contrt_stus_cd);size(2);null" description:"合约状态代码  01-正常02-逾期03-减值04-部分核销05-核销06-结清07-减免结清"`
	CustNo                         string  `orm:"column(cust_no);size(14);null" description:"客户编号  02-逾期"`
	LoanProdtNo                    string  `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	LoanProdtVersNo                string  `orm:"column(loan_prodt_vers_no);size(5);null" description:"贷款产品版本编号  5位产品版本号生成规则:产品编号独立,修改产品参数产品编号不变,提升版本号,整形数,顺序加,转为前面填充0的5位字符串存储,举例:原贷款产品O0200001版本号00010最高授信额度50,000.00,修改为最高授信额度100,000.00,版本号变更为00011,"`
	MakelnOrgNo                    string  `orm:"column(makeln_org_no);size(4);null" description:"放款机构号"`
	IndvCrtfTypCd                  string  `orm:"column(indv_crtf_typ_cd);size(4);null" description:"个人证件类型代码  参考标准代码:CD0015"`
	IndvCrtfNo                     string  `orm:"column(indv_crtf_no);size(20);null" description:"个人证件号码"`
	RevneCmpdCd                    string  `orm:"column(revne_cmpd_cd);size(1);null" description:"计收复利代码     0-不收复利,,1-收取复利,2-由全局参数控制"`
	MakelnManrCd                   string  `orm:"column(makeln_manr_cd);size(1);null" description:"放款方式代码  0-自主支付,,1-受托支付,2-混合支付"`
	RelaAcctDtrmnManrCd            string  `orm:"column(rela_acct_dtrmn_manr_cd);size(2);null" description:"关联账户确定方式代码  01-客户层,02-产品层,03-合同层,04-借据层"`
	RepayDayDtrmnManrCd            string  `orm:"column(repay_day_dtrmn_manr_cd);size(2);null" description:"还款日确定方式代码       01-客户层,02-产品层,03-合同层,04-借据层"`
	LoanDeadl                      int     `orm:"column(loan_deadl);null" description:"贷款周期"`
	LoanDeadlCycCd                 string  `orm:"column(loan_deadl_cyc_cd);null" description:"贷款周期代码"`
	PmitAdvRepayTms                int     `orm:"column(pmit_adv_repay_tms);null" description:"允许提前还款次数"`
	AdvRepayLimitBgnDt             string  `orm:"column(adv_repay_limit_bgn_dt);type(date);null" description:"提前还款限制开始日期"`
	AdvRepayLimitDays              int     `orm:"column(adv_repay_limit_days);null" description:"提前还款限制天数"`
	LimitTrmInsidPmitPartlRepayFlg string  `orm:"column(limit_trm_insid_pmit_partl_repay_flg);size(1);null" description:"限制期内允许部分还款标志  0否1是"`
	LimitTrmInsidPmitPayOffFlg     string  `orm:"column(limit_trm_insid_pmit_pay_off_flg);size(1);null" description:"限制期内允许结清标志  0否1是"`
	OpenAcctDt                     string  `orm:"column(open_acct_dt);type(date);null" description:"开户日期  记录客户首次办理登记的具体日期,"`
	FsttmForsprtDt                 string  `orm:"column(fsttm_forsprt_dt);type(date);null" description:"首次支用日期"`
	BegintDt                       string  `orm:"column(begint_dt);type(date);null" description:"起息日期  是否应在计息中心记录"`
	OrgnlMatrDt                    string  `orm:"column(orgnl_matr_dt);type(date);null" description:"原始到期日期"`
	CurCd                          string  `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	LoanAmt                        float64 `orm:"column(loan_amt);null;digits(18);decimals(2)" description:"贷款金额  如果多次贷放,或者信用卡模式,此金额如何处理?"`
	EmbFlg                         string  `orm:"column(emb_flg);size(1);null" description:"挪用标志  0否1是"`
	DecdEmbDt                      string  `orm:"column(decd_emb_dt);type(date);null" description:"判定挪用日期"`
	MansbjTypCd                    string  `orm:"column(mansbj_typ_cd);size(3);null" description:"主体类型代码  01-农户02-非农个人03-农村个体工商户90-农户小额91-农村个体户小额92-农村小微企业小额93-非农小微企业小额94-非农个体工商户小额"`
	CtrtNo                         string  `orm:"column(ctrt_no);size(32);null" description:"合同号  22位合同号生成规则:机构号4位+业务属性2位+年月日8位+顺序号6位+尾号2位编号规则:1,尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'2,业务属性:,02-微贷,,,13-个贷,14-小企业,01-公司,08-承兑,02-普贷举例:机构:0401,,业务属性:个人贷款,日期:20191014生成合同号:0401132019101400000168"`
	LoanGuarManrCd                 string  `orm:"column(loan_guar_manr_cd);size(2);null" description:"贷款担保方式代码  参考标准代码:CD0090"`
	OthConsmTypCd                  string  `orm:"column(oth_consm_typ_cd);size(3);null" description:"其他消费类型代码      1-个人日常消费,2-装修,3-旅游,4-教育,5-医疗,6-其他消费,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"`
	RepayManrCd                    string  `orm:"column(repay_manr_cd);size(3);null" description:"还款方式代码      参考标准代码:CD0062标准基础上新增:51-按月付息按季还本52-按月付息按半年还本53-按月付息按年还本"`
	IntStlDayDtrmnManrCd           string  `orm:"column(int_stl_day_dtrmn_manr_cd);size(3);null" description:"结息日确定方式代码    1-统一定日,2-按户定日,3-按指定日期4-按产品,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"`
	AdvRepayTms                    int     `orm:"column(adv_repay_tms);null" description:"提前还款次数"`
	TranNormlLoanDt                string  `orm:"column(tran_norml_loan_dt);type(date);null" description:"转正常贷款日期"`
	BldInstltRepayFlg              string  `orm:"column(bld_instlt_repay_flg);size(1);null" description:"建立分期还款标志  描述是否建立分期还款,0否1是,"`
	IntStlPrtyTypCd                string  `orm:"column(int_stl_prty_typ_cd);size(1);null" description:"结息优先类型代码  1-以计划还息金额为结息金额,2-以计提金额为结息金额"`
	LoanIntrtAdjCycCd              string  `orm:"column(loan_intrt_adj_cyc_cd);size(3);null" description:"贷款利率调整周期代码"`
	LoanIntrtAdjCycQty             int     `orm:"column(loan_intrt_adj_cyc_qty);null" description:"贷款利率调整周期数量"`
	StpDeductFlg                   string  `orm:"column(stp_deduct_flg);size(1);null" description:"停止批扣标志  描述银行与客户达成减免协议后,停止通过批量扣款方式进行追扣,由客户经理通过其他交易手工发起扣款,0否1是,"`
	StpDeductRsnTypCd              string  `orm:"column(stp_deduct_rsn_typ_cd);size(1);null" description:"停止批扣原因类型代码  描述停止批扣的原因类型信息,1-建立减免计划,2-确认无法收回"`
	StpDeductCnfrmDt               string  `orm:"column(stp_deduct_cnfrm_dt);type(date);null" description:"停止批扣确认日期"`
	LoanIntrtAdjManrCd             string  `orm:"column(loan_intrt_adj_manr_cd);size(3);null" description:"贷款利率调整方式代码  1-对日,2-周期开始日,3-固定"`
	ExpdayPayOffManrCd             string  `orm:"column(expday_pay_off_manr_cd);size(2);null" description:"到期日结清方式代码  0-系统自动1-手工"`
	GraceTrmIntacrFlg              string  `orm:"column(grace_trm_intacr_flg);size(1);null" description:"宽限期计息标志  0否1是"`
	EvrpridMaxGraceTrmDays         int     `orm:"column(evrprid_max_grace_trm_days);null" description:"每期最大宽限期天数"`
	ContrtPrdGraceTrmTotlDays      int     `orm:"column(contrt_prd_grace_trm_totl_days);null" description:"合约期间宽限期总天数"`
	AdvRepayColtfeFlg              string  `orm:"column(adv_repay_coltfe_flg);size(1);null" description:"提前还款收费标志  0否1是"`
	ColtfeManrCd                   string  `orm:"column(coltfe_manr_cd);size(1);null" description:"收费方式代码  1:按笔2:按金额比例,"`
	SglColtfeAmt                   float64 `orm:"column(sgl_coltfe_amt);null;digits(18);decimals(2)" description:"单笔收费金额"`
	ColtfeAmtCrdnlnbrCd            string  `orm:"column(coltfe_amt_crdnlnbr_cd);size(4);null" description:"收费金额基数代码  1:还款金额2:贷款余额3:借款金额"`
	ColtfeRatio                    float64 `orm:"column(coltfe_ratio);null;digits(9);decimals(6)" description:"收费比例"`
	AcrdgRatioColtfeCeilAmt        float64 `orm:"column(acrdg_ratio_coltfe_ceil_amt);null;digits(18);decimals(2)" description:"按比例收费上限金额"`
	AcrdgRatioColtfeFloorAmt       float64 `orm:"column(acrdg_ratio_coltfe_floor_amt);null;digits(18);decimals(2)" description:"按比例收费下限金额"`
	CurrExecTmprd                  int     `orm:"column(curr_exec_tmprd);null" description:"当前执行期次"`
	RepayPlanAdjFlg                string  `orm:"column(repay_plan_adj_flg);size(1);null" description:"还款计划调整标志  0否1是"`
	RestFlg                        string  `orm:"column(rest_flg);size(1);null" description:"停息标志  0否1是"`
	RestDt                         string  `orm:"column(rest_dt);type(date);null" description:"停息日期"`
	TranDvalDt                     string  `orm:"column(tran_dval_dt);type(date);null" description:"转减值日期"`
	TranDvalFlg                    string  `orm:"column(tran_dval_flg);size(1);null" description:"转减值标志  0否1是"`
	ExtsnTms                       int     `orm:"column(extsn_tms);null" description:"展期次数  展期的总次数,"`
	WaitExtsnFlg                   string  `orm:"column(wait_extsn_flg);size(1);null" description:"待展期标志  0-否1-是"`
	CurrLoanRiskClsfCd             string  `orm:"column(curr_loan_risk_clsf_cd);size(2);null" description:"当前贷款风险分类代码  1,正常2,关注3,次级4,可疑5,损失11,正常一级12,正常二级13,正常三级14,正常四级15,正常五级21,关注一级22,关注二级23,关注三级31,次级一级32,次级二级41,可疑级51,损失级"`
	MatrDt                         string  `orm:"column(matr_dt);type(date);null" description:"到期日期  记录到期结清的日期,"`
	PayOffDt                       string  `orm:"column(pay_off_dt);type(date);null" description:"结清日期"`
	FinlModfyDt                    string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期  N"`
	FinlModfyTm                    string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间  N"`
	FinlModfyOrgNo                 string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号  N"`
	FinlModfyTelrNo                string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号  N"`
	TccState                       int8    `orm:"column(tcc_state)" description:"TCC状态值[0:正常 1:新增 2:删除]"`
}

func (t *T_loan_loan_cont) TableName() string {
	return "t_loan_loan_cont"
}

func InsertT_loan_loan_cont(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_loan_cont).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_loan_contTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_loan_cont)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_loan_contTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_loan_cont where loan_dubil_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"]).Exec()
	return err
}

func QueryT_loan_loan_cont(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_loan_cont panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	flag := params["Flag"]
	delete(params, "Flag")
	switch flag {
	case "9":
		maps, err = QueryT_loan_loan_contFlag9(params)
		return
	case "8":
		maps, err = QueryT_loan_loan_contFlag8(params)
		return
	case "7":
		maps, err = QueryT_loan_loan_contFlag7(params)
		return
	case "6": //lzd
		maps, err = QueryT_loan_loan_contFlag6(params)
		return
	case "5":
		maps, err = QueryT_loan_loan_contFlag5(params)
		return
	case "4":
		maps, err = QueryT_loan_loan_contFlag4(params)
		return
	case "3":
		maps, err = QueryT_loan_loan_contFlag3(params)
		return
	case "2":
		maps, err = QueryT_loan_loan_contFlag2(params)
		return
	case "1":
		maps, err = QueryT_loan_loan_contFlag1(params)
		return
	case "0":
		maps, err = QueryT_loan_loan_contFlag0(params)
		return
	case "10":
		maps, err = QueryT_loan_loan_contFlag10(params)
		return
	case "11":
		maps, err = QueryT_loan_loan_contFlag11(params)
		return
	case "12":
		maps, err = QueryT_loan_loan_contFlag12(params)
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_loan_cont))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_loan_contById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	tccstate := maps["TccState"]
	if tccstate == "1" { //一边新增一边修改--（特殊处理）
		if _, err = o.QueryTable(new(T_loan_loan_cont)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
			Filter("tcc_state", 1).Values(&mapList); err != nil {
			return nil, err
		}
		return
	}
	if _, err = o.QueryTable(new(T_loan_loan_cont)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_loan_cont(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_loan_cont panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	tccstate := maps["TccState"]
	delete(maps, "TccState")
	if tccstate == "1" { //一边新增一边修改--（特殊处理）
		_, err = o.QueryTable(new(T_loan_loan_cont)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
			Update(maps)
		return err
	}
	_, err = o.QueryTable(new(T_loan_loan_cont)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}

func QueryT_loan_loan_contFlag9(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	var sqlstr strings.Builder
	sql := "SELECT a.cust_no CustNo,a.ctrt_no CtrtNo,a.loan_amt LoanAmt,b.loan_prodt_nm LoanProdNnm,b.start_dt StartDt,b.matr_dt MatrDt,b.keprcd_stus_cd KeprcdStusCd, c.cust_name CustName,c.tran_org_id TranOrgId ,c.operator Operator FROM " +
		"t_loan_loan_cont a, t_loan_product b, t_cust_base_info c " +
		"WHERE a.loan_prodt_no = b.loan_prodt_no " +
		"AND a.loan_prodt_vers_no = b.loan_prodt_vers_no " +
		"AND c.cust_id=a.cust_no "
	sqlstr.WriteString(sql)
	if maps["CustNo"] != nil { //合约号
		sqlstr.WriteString(" and a.cust_no=\"" + maps["CustNo"].(string) + "\"")
	}
	if maps["CtrtNo"] != nil { //客户号
		sqlstr.WriteString(" and a.ctrt_no=\"" + maps["CtrtNo"].(string) + "\"")
	}
	if maps["LoanDubilNo"] != nil { //借据号
		sqlstr.WriteString(" and a.loan_dubil_no=\"" + maps["LoanDubilNo"].(string) + "\"")
	}
	if maps["KeprcdStusCd"] != nil { //合同状态
		sqlstr.WriteString(" and b.keprcd_stus_cd=\"" + maps["KeprcdStusCd"].(string) + "\"")
	}
	if maps["LoanGuarManrCd"] != nil { //贷款方式
		sqlstr.WriteString(" and a.loan_guar_manr_cd=\"" + maps["LoanGuarManrCd"].(string) + "\"")
	}
	if maps["StartDt"] != nil { //合同生效日期
		sqlstr.WriteString(" and b.start_dt>=\"" + maps["StartDt"].(string) + "\"")
	}
	if maps["MatrDt"] != nil { //合同到期日
		sqlstr.WriteString(" and b.matr_dt<=\"" + maps["MatrDt"].(string) + "\"")
	}
	if maps["CustName"] != nil { //客户名称
		sqlstr.WriteString(" and c.cust_name=\"" + maps["CustName"].(string) + "\"")
	}
	_, err = o.Raw(sqlstr.String()).Values(&mapList)
	if err != nil {
		return nil, err
	}
	return
}

//查询，放款机构贷款总余额
func QueryT_loan_loan_contFlag8(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT SUM(loan_amt) SUM FROM `t_loan_loan_cont` WHERE makeln_org_no =? and tcc_state=0 "
	_, err = o.Raw(sql, maps["MakelnOrgNo"]).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag7(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT " +
		"t1.`ctrt_no`					CtrtNo				," + //合同编号
		"t1.`loan_dubil_no`         	LoanDubilNo			," + //贷款借据号
		"t1.`cust_no`               	CustNo				," + //客户编号
		"t1.`fsttm_forsprt_dt`      	OpenAcctDt			," + //发放日期
		"t1.`matr_dt`	           		MatrDt				," + //到期日期
		"t1.`contrt_stus_cd`        	ContrtStusCd		," + //贷款状态
		"t1.accti_acct_no				AcctiAcctNo			," + //核算账号
		"t2.`loan_prodt_nm`         	LoanProdtNm			," + //产品名称
		"t1.`curr_loan_risk_clsf_cd` CurrLoanRiskClsfCd		," + //风险分类
		"t3.`cust_name`	 				CustName 			" + //客户姓名
		"FROM `t_loan_loan_cont` t1 left join`t_loan_product` t2 on t1.`loan_prodt_no`=t2.`loan_prodt_no` AND t1.`loan_prodt_vers_no`=t2.`loan_prodt_vers_no` " +
		"left join`t_loan_indiv_contract_info` t3  on t1.`ctrt_no`=t3.`ctrt_no` " +
		"AND t1.tcc_state=0 AND t2.tcc_state=0 AND t3.tcc_state=0  where 1=1 "
	if maps["LoanDubilNo"] != nil {
		sql = sql + "and t1.`loan_dubil_no`=\"" + maps["LoanDubilNo"].(string) + "\""
	}
	if maps["CustNo"] != nil {
		sql = sql + "and t1.`cust_no`=\"" + maps["CustNo"].(string) + "\""
	}
	if maps["BegingStartDt"] != nil {
		sql = sql + "and t1.`fsttm_forsprt_dt`>=\"" + maps["BegingStartDt"].(string) + "\""
	}
	if maps["BegingEndDt"] != nil {
		sql = sql + "and t1.`fsttm_forsprt_dt`<=\"" + maps["BegingEndDt"].(string) + "\""
	}
	if maps["ContrtStusCd"] != nil {
		sql = sql + "and t1.`contrt_stus_cd`=\"" + maps["ContrtStusCd"].(string) + "\""
	}
	if maps["LoanProdtNm"] != nil {
		sql = sql + "and t2.`loan_prodt_nm`=\"" + maps["LoanProdtNm"].(string) + "\""
	}
	if maps["CurrLoanRiskClsfCd"] != nil {
		sql = sql + "and t1.`curr_loan_risk_clsf_cd`=\"" + maps["CurrLoanRiskClsfCd"].(string) + "\""
	}
	_, err = o.Raw(sql).Values(&mapList)
	return
}

func QueryT_loan_loan_contFlag6(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  " +
		"  t1.`loan_dubil_no` LoanDubilNo, " +
		"  t2.`adj_dt` AdjDt, " +
		"  t2.`seq_no` SeqNo, " +
		"  t4.`cust_name` CustNm, " +
		"  t2.adj_bef_loan_risk_clsf_cd Adjbefloanriskclsfcd, " +
		"  t2.curtprd_comp_etim_ln_risk_clsf_cd CurtprdCompEtimLnRiskClsfCd, " +
		"  t2.curtprd_artgcl_idtfy_loan_risk_clsf_cd CurtprdArtgclIdtfyLoanRiskClsfCd, " +
		"  t2.inend_idtfy_loan_risk_clsf_cd InendIdtfyLoanRiskClsfCd, " +
		"  t2.adj_manr_cd AdjManrCd, " +
		"  t5.`loan_prodt_nm` LoanProdtNm, " +
		"  t6.`loan_risk_clsf_stus_cd` LoanRiskClsfStusCd  " +
		"FROM " +
		"(SELECT loan_dubil_no,MAX(seq_no) seq_no FROM `t_risk_cls_adj_rec`GROUP BY loan_dubil_no) t1 " +
		"LEFT JOIN `t_risk_cls_adj_rec` t2 " +
		"	ON t1.`loan_dubil_no` = t2.`loan_dubil_no` AND t1.`seq_no` = t2.seq_no  " +
		" LEFT JOIN t_loan_loan_cont t3  " +
		"	ON t3.`loan_dubil_no`=t1.`loan_dubil_no` " +
		"  LEFT JOIN t_loan_cust_base_info t4  " +
		"	ON t3.`cust_no` = t4.`cust_no` AND t3.`indv_crtf_typ_cd` = t4.`indv_crtf_typ_cd`AND t4.`indv_crtf_no` = t4.`indv_crtf_no`  " +
		"  LEFT JOIN t_loan_product t5  " +
		"	ON t3.`loan_prodt_no` = t5.`loan_prodt_no`  " +
		"	AND t3.`loan_prodt_vers_no` = t5.`loan_prodt_vers_no`  " +
		"  LEFT JOIN t_risk_cls_adj_regis t6 " +
		" ON t2.`loan_dubil_no`=t6.`loan_dubil_no` AND t2.`adj_dt`=t6.`adj_dt` AND t2.`seq_no`=t6.`seq_no` " +
		" where 1=1 "
	if maps["CustNm"] != nil {
		sql = sql + " and t4.cust_name=\"" + maps["CustNm"].(string) + "\""
	}
	if maps["LoanDubilNo"] != nil {
		sql = sql + " and t1.loan_dubil_no=\"" + maps["LoanDubilNo"].(string) + "\""
	}
	if maps["CrtfTypCd"] != nil {
		sql = sql + " and t3.indv_crtf_typ_cd=\"" + maps["CrtfTypCd"].(string) + "\""
	}
	if maps["CrtfNo"] != nil {
		sql = sql + " and t3.indv_crtf_no=\"" + maps["CrtfNo"].(string) + "\""
	}
	if maps["CompEtimLnRiskClsfDtStart"] != nil {
		sql = sql + " and t2.adj_dt >=\"" + maps["CompEtimLnRiskClsfDtStart"].(string) + "\""
	}
	if maps["CompEtimLnRiskClsfDtEend"] != nil {
		sql = sql + " and t2.adj_dt<=\"" + maps["CompEtimLnRiskClsfDtEend"].(string) + "\""
	}
	if maps["LoanRiskClsfStusCd"] != nil {
		sql = sql + " and t2.loan_risk_clsf_stus_cd=\"" + maps["LoanRiskClsfStusCd"].(string) + "\""
	}
	if maps["AdjType"] != nil {
		if maps["AdjType"] == "1" {
			sql = sql + " and t2.`adj_bef_loan_risk_clsf_cd` > t2.`adj_aft_loan_risk_clsf_cd`"
		}
		if maps["AdjType"] == "2" {
			sql = sql + " and t2.`adj_bef_loan_risk_clsf_cd` < t2.`adj_aft_loan_risk_clsf_cd`"
		}
		if maps["AdjType"] == "3" {
			sql = sql + " and t2.`adj_bef_loan_risk_clsf_cd` = t2.`adj_aft_loan_risk_clsf_cd`"
		}
	}
	sql = sql + " ORDER BY t2.`adj_dt` LIMIT 1"
	_, err = o.Raw(sql).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag5(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  " +
		"  t1.loan_dubil_no LoanDubilNo,  " +
		"  t1.accti_acct_no AcctiAcctNo,  " +
		"  t1.int_stl_prty_typ_cd IntStlPrtTypCd,  " +
		"  t1.cust_no CustNo,  " +
		"  t1.makeln_org_no MakelnOrgNo,  " +
		"  t1.contrt_stus_cd ContrtStusCd,  " +
		"  t1.repay_day_dtrmn_manr_cd RepayDayDtrmnManrCd,  " +
		"  t2.keprcd_no KeprcdNo,  " +
		"  t2.crt_dt CrtDt,  " +
		"  t2.finl_modfy_tm FinlModfyTm,  " +
		"  t2.int_stl_day IntStlDay,  " +
		"  t3.keprcd_stus_cd KeprcdStusCd,  " +
		"  t3.repay_dt RepayDt,  " +
		"  t3.curr_period_intacr_bgn_dt CurrPeriodIntacrBgnDt,  " +
		"  t3.curr_period_intacr_end_dt CurrPeriodIntacrEndDt,  " +
		"  t3.plan_repay_intr PlanRepayIntr,  " +
		"  t3.pridnum Pridnum,  " +
		"  t3.plan_repay_prin PlanRepayPrin,  " +
		"  t3.plan_repay_totl_amt PlanRepayTotlAmt  " +
		"FROM  (t_loan_loan_cont AS t1,t_cust_repmt_day_info AS t2, (SELECT keprcd_no,CONCAT(DATE_FORMAT(NOW(),'%Y-%m-'),  " +
		"LPAD(int_stl_day-1,2,0)) pending_settlement_dt FROM t_cust_repmt_day_info) AS t4)  " +
		"LEFT JOIN t_loan_repay_term  AS t3  " +
		"ON  " +
		"((t1.contrt_stus_cd='02' AND t1.loan_dubil_no=t3.loan_dubil_no) OR (t1.contrt_stus_cd NOT IN('02') "

	//"AND DATE_ADD(pending_settlement_dt, INTERVAL 1 DAY)=t3.repay_dt))  "
	if maps["RepayDt"] != nil {
		sql = sql + " and t3.repay_dt=\"" + maps["RepayDt"].(string) + "\")"
	}
	sql = sql +
		" AND t1.loan_dubil_no = t3.loan_dubil_no )" +
		"WHERE   " +
		"CASE  " +
		"    WHEN t1.repay_day_dtrmn_manr_cd = '01' THEN t1.cust_no=t2.cust_no  " +
		"    WHEN t1.repay_day_dtrmn_manr_cd = '02' THEN t1.loan_prodt_no=t2.loan_prodt_no  " +
		"    WHEN t1.repay_day_dtrmn_manr_cd = '03'  THEN t1.ctrt_no=t2.ctrt_no  " +
		"    WHEN t1.repay_day_dtrmn_manr_cd = '04'  THEN t1.loan_dubil_no=t2.loan_dubil_no  " +
		"    WHEN t1.repay_day_dtrmn_manr_cd NOT IN('01','02','03','04')  THEN t1.loan_dubil_no=t2.loan_dubil_no    " +
		"END  " +
		"   AND t2.keprcd_no = t4.keprcd_no   "
	//if maps["PendingSettlementDt"] != nil {
	//	sql = sql +" and t4.pending_settlement_dt=\""+maps["PendingSettlementDt"].(string)+"\""
	//}
	if maps["RepayDt"] != nil {
		sql = sql + " and t3.repay_dt<=\"" + maps["RepayDt"].(string) + "\""
	}
	if maps["ContrtStusCd"] != nil {
		if maps["ContrtStusCd"] == "02" {
			sql = sql + " and contrt_stus_cd='02' and t3.keprcd_stus_cd not in ('03') "
		} else {
			sql = sql + " and contrt_stus_cd=\"" + maps["ContrtStusCd"].(string) + "\" and t3.keprcd_stus_cd in ('01')"
		}
	}
	sql = sql + "  GROUP BY  t1.loan_dubil_no DESC,t3.pridnum DESC"
	_, err = o.Raw(sql).Values(&mapList)
	return
}

func QueryT_loan_loan_contFlag4(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT " +
		"t1.loan_dubil_no LoanDubilNo, " +
		"t1.contrt_stus_cd ContrtStusCd, " +
		"t1.accti_acct_no AcctiAcctNo, " +
		"t1.cust_no CustNo, " +
		"t1.loan_prodt_no LoanProdtNo, " +
		"t1.loan_prodt_vers_no LoanProdtVersNo, " +
		"t1.loan_deadl LoanDeadl, " +
		"t1.loan_deadl_cyc_cd LoanDeadlCycCd, " +
		"t1.ctrt_no CtrtNo, " +
		"t1.loan_amt LoanAmt, " +
		"t1.repay_manr_cd RepayManrCd, " +
		"t1.indv_crtf_typ_cd IndvCrtfTypCd, " +
		"t1.indv_crtf_no IndvCrtfNo, " +
		"t2.cust_name CustName, " +
		"t2.mobile_no MobileNo, " +
		"t3.loan_prodt_nm LoanProdtNm, " +
		"t4.eralest_repay_dt EralestRepayDt  " +
		"FROM  (t_loan_loan_cont t1,t_loan_cust_base_info t2) " +
		"LEFT JOIN  t_loan_product t3 ON  t1.loan_prodt_no=t3.loan_prodt_no AND t1.loan_prodt_vers_no=t3.loan_prodt_vers_no " +
		"LEFT JOIN (SELECT loan_dubil_no,MIN(repay_dt) AS eralest_repay_dt,MIN(finl_modfy_tm) AS recent_finl_modfy_tm FROM t_loan_repay_term WHERE keprcd_stus_cd='01' GROUP BY loan_dubil_no) t4 " +
		"ON t1.loan_dubil_no=t4.loan_dubil_no WHERE  t1.cust_no=t2.cust_no  "
	if maps["LoanDubilNo"] != nil {
		sql = sql + " AND t1.loan_dubil_no=\"" + maps["LoanDubilNo"].(string) + "\""
	}
	if maps["ContrtStusCd"] != nil {
		sql = sql + " AND t1.contrt_stus_cd=\"" + maps["ContrtStusCd"].(string) + "\""
	}
	if maps["CustName"] != nil {
		sql = sql + " AND t2.cust_name=\"" + maps["CustName"].(string) + "\""
	}
	if maps["LoanProdtNm"] != nil {
		sql = sql + " AND t3.loan_prodt_nm like \"%" + maps["LoanProdtNm"].(string) + "%\""
	}
	if maps["LoanProdtNo"] != nil {
		sql = sql + " AND t3.loan_prodt_no=\"" + maps["LoanProdtNo"].(string) + "\""
	}
	if maps["IndvCrtfTypCd"] != nil {
		sql = sql + " AND t1.indv_crtf_typ_cd=\"" + maps["IndvCrtfTypCd"].(string) + "\""
	}
	if maps["IndvCrtfNo"] != nil {
		sql = sql + " AND t1.indv_crtf_no=\"" + maps["IndvCrtfNo"].(string) + "\""
	}
	if maps["CustNo"] != nil {
		sql = sql + " AND t1.cust_no=\"" + maps["CustNo"].(string) + "\""
	}
	sql = sql + "  GROUP BY  t1.loan_dubil_no HAVING MAX(t2.finl_modfy_dt) AND MAX(t2.finl_modfy_tm)"
	_, err = o.Raw(sql).Values(&mapList)
	return
}

func QueryT_loan_loan_contFlag3(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  " +
		"t1.loan_dubil_no LoanDubilNo,  " +
		"t1.cust_no CustNo,  " +
		"t1.indv_crtf_typ_cd IndvCrtfTypCd,  " +
		"t1.indv_crtf_no IndvCrtfNo,  " +
		"t1.matr_dt MatrDt,  " +
		"t1.begint_dt GrantDt,  " +
		"t1.cust_no CustNo,  " +
		"t2.cust_name CustNm,  " +
		"t3.loan_prodt_nm LoanProdtNm  " +
		"FROM  (t_loan_loan_cont t1,t_loan_cust_base_info t2)  " +
		"LEFT JOIN  t_loan_product t3 ON  t1.loan_prodt_no=t3.loan_prodt_no AND t1.loan_prodt_vers_no=t3.loan_prodt_vers_no  " +
		"WHERE t1.contrt_stus_cd NOT IN('06','07') AND t1.cust_no=t2.cust_no AND t1.indv_crtf_no=t2.indv_crtf_no AND t1.indv_crtf_typ_cd=t2.indv_crtf_typ_cd  "
	if maps["LoanDubilNo"] != nil {
		sql = sql + " and t1.loan_dubil_no=\"" + maps["LoanDubilNo"].(string) + "\""
	}
	if maps["IndvCrtfTypCd"] != nil {
		sql = sql + " and t1.indv_crtf_typ_cd=\"" + maps["IndvCrtfTypCd"].(string) + "\""
	}
	if maps["IndvCrtfNo"] != nil {
		sql = sql + " and t1.indv_crtf_no=\"" + maps["IndvCrtfNo"].(string) + "\""
	}
	if maps["CustNm"] != nil {
		sql = sql + " and t2.cust_name=\"" + maps["CustNm"].(string) + "\""
	}
	if maps["CustNo"] != nil {
		sql = sql + " and t1.cust_no=\"" + maps["CustNo"].(string) + "\""
	}
	sql = sql + "  GROUP BY  t1.loan_dubil_no HAVING MAX(t1.finl_modfy_dt) AND MAX(t1.finl_modfy_tm)"
	_, err = o.Raw(sql).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag2(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT accti_acct_no AcctiAcctNo,curr_loan_risk_clsf_cd curr_LoanRiskClsfCd FROM `t_loan_loan_cont` WHERE curr_loan_risk_clsf_cd IN(1,2,3,4,5) AND contrt_stus_cd !='07'" +
		"AND cust_no IN (SELECT cust_no FROM t_loan_manager_customer WHERE  1=1 "
	if maps["CustMgrEmpnbr"] != nil {
		sql = sql + " and cust_mgr_empnbr=\"" + maps["CustMgrEmpnbr"].(string) + "\""
	}
	if maps["BlngtoOrgNo"] != nil {
		sql = sql + " and blngto_org_no=\"" + maps["BlngtoOrgNo"].(string) + "\""
	}
	sql = sql + "  )"
	_, err = o.Raw(sql).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag1(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT cust_no CustNo,fsttm_forsprt_dt FsttmForsprtDt,loan_dubil_no LoanDubilNo FROM t_loan_loan_cont WHERE 1=1 "
	if maps["CustNo"] != nil {
		sql = sql + " and cust_no='" + maps["CustNo"].(string) + "'"
	}
	sql = sql + "  GROUP BY cust_no HAVING MIN(fsttm_forsprt_dt)"
	_, err = o.Raw(sql).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag11(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT  " +
		"t1.`accti_acct_no` AcctiAcctNo, " +
		"t1.`loan_dubil_no` LoanDubilNo, " +
		"t1.`curr_loan_risk_clsf_cd` CurrLoanRiskClsfCd, " +
		"t1.`cust_no` CustNo, " +
		"t2.`cust_name` CustNm, " +
		"t1.`indv_crtf_no` IndvCrtfNo, " +
		"t1.`indv_crtf_typ_cd` IndvCrtfTypCd, " +
		"t3.`loan_prodt_nm` LoanProdtNm, " +
		"t1.`begint_dt` GrantDt, " +
		"t1.`matr_dt` MatrDt, " +
		"t1.repay_manr_cd RepayManrCd  " +
		"FROM (t_loan_loan_cont t1,t_loan_cust_base_info t2)  " +
		"LEFT JOIN t_loan_product t3 ON t1.`loan_prodt_no`=t3.`loan_prodt_no` AND t1.`loan_prodt_vers_no`=t3.`loan_prodt_vers_no` " +
		"WHERE t1.`cust_no`=t2.cust_no  AND t1.indv_crtf_typ_cd=t2.indv_crtf_typ_cd AND t1.indv_crtf_no=t2.indv_crtf_no AND contrt_stus_cd='01' "
	if maps["CustNo"] != nil {
		sql = sql + " and t1.cust_no='" + maps["CustNo"].(string) + "'"
	}
	if maps["CustNm"] != nil {
		sql = sql + " and t2.cust_name='" + maps["CustNm"].(string) + "'"
	}
	if maps["LoanProdtNm"] != nil {
		sql = sql + " and t3.loan_prodt_nm !='" + maps["LoanProdtNm"].(string) + "'"
	}
	if maps["IndvCrtfTypCd"] != nil {
		sql = sql + " and t1.indv_crtf_typ_cd='" + maps["IndvCrtfTypCd"].(string) + "'"
	}
	if maps["IndvCrtfNo"] != nil {
		sql = sql + " and t1.indv_crtf_no='" + maps["IndvCrtfNo"].(string) + "'"
	}
	if maps["LoanDubilNo"] != nil {
		sql = sql + " and t1.loan_dubil_no='" + maps["LoanDubilNo"].(string) + "'"
	}
	sql = sql + " group by t1.loan_dubil_no"
	_, err = o.Raw(sql).Values(&mapList)
	return
}

func QueryT_loan_loan_contFlag0(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT accti_acct_no AcctiAcctNo,cust_no CustNo,fsttm_forsprt_dt FsttmForsprtDt,pay_off_dt PayOffDt,loan_amt LoanAmt FROM t_loan_loan_cont WHERE contrt_stus_cd <> '06'  "
	if maps["CustNo"] != nil {
		sql = sql + " and cust_no='" + maps["CustNo"].(string) + "'"
	}
	_, err = o.Raw(sql).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag10(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	sql := "SELECT " +
		"  t1.loan_prodt_no LoanProdtNo, " +
		"  t2.`loan_prodt_nm` LoanProdtNm," +
		"  SUM(t1.loan_amt) LoanAmt," +
		"  GROUP_CONCAT(t1.`accti_acct_no`) AcctiAcctNoList " +
		"FROM" +
		"  `t_loan_loan_cont` t1 " +
		"  LEFT JOIN t_loan_product t2 " +
		"    ON t1.`loan_prodt_no` = t2.`loan_prodt_no` " +
		"WHERE t1.`cust_no` = ?  " +
		"GROUP BY t1.`loan_prodt_no` "
	_, err = o.Raw(sql, maps["CustNo"]).Values(&mapList)
	return
}
func QueryT_loan_loan_contFlag12(maps orm.Params) (mapList []orm.Params, err error) {
	o := orm.NewOrm()
	var LoanProdtNo, ProdId, LoanProdtNm1, LoanProdtNm2, CustNo, StartDate, EndDate string
	LoanProdtNo = "S0200046"
	ProdId = "S0200089"
	LoanProdtNm1 = "乐高e贷"
	LoanProdtNm2 = "乐高消费贷"
	CustNo = maps["CustNo"].(string)
	StartDate = maps["StartDate"].(string)
	EndDate = maps["EndDate"].(string)
	sql := "SELECT loan_amt AS TranAmt,'1' AS TranType,'" + LoanProdtNo + "' as LoanProdtNo,'" + LoanProdtNm1 + "' as LoanProdtNm,open_acct_dt As TranDate FROM `t_loan_loan_cont` WHERE loan_prodt_no='" + LoanProdtNo + "' AND cust_no='" + CustNo + "' AND tcc_state=0 " +
		"UNION " +
		"SELECT actlst_totl_amt,'2','" + LoanProdtNo + "','" + LoanProdtNm1 + "',repay_dt   FROM `t_loan_repayment_bill`  " +
		"WHERE loan_dubil_no IN (SELECT loan_dubil_no FROM `t_loan_loan_cont` WHERE loan_prodt_no='" + LoanProdtNo + "' GROUP BY loan_dubil_no) " +
		"AND repay_dt>='" + StartDate + "' AND repay_dt<'" + EndDate + "' AND tcc_state=0 " +
		"UNION " +
		"SELECT consu_amt,'3','" + ProdId + "','" + LoanProdtNm2 + "',consu_date  FROM `t_loan_consu_det`  " +
		"WHERE loan_acct_no IN (SELECT loan_acct_no FROM `t_loan_consu_mgmt`WHERE cust_id='" + CustNo + "'AND STATUS='1' AND prod_id='" + ProdId + "') AND trade_type= 'B' AND tcc_state=0 " +
		"AND consu_date>='" + StartDate + "' AND consu_date<'" + EndDate + "' " +
		"UNION  " +
		"SELECT consu_amt,'4','" + ProdId + "','" + LoanProdtNm2 + "',consu_date  FROM `t_loan_consu_det`  " +
		"WHERE loan_acct_no IN (SELECT loan_acct_no FROM `t_loan_consu_mgmt`WHERE cust_id='" + CustNo + "'AND STATUS='2' AND prod_id='" + ProdId + "') AND trade_type= 'B' AND tcc_state=0 " +
		"AND consu_date>='" + StartDate + "' AND consu_date<'" + EndDate + "' "
	_, err = o.Raw(sql).Values(&mapList)
	return
}
