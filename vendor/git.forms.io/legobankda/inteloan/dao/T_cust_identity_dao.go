package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_cust_identity struct {
	CustId          string `orm:"column(cust_id);size(20);pk" description:"客户号"`
	IdType          string `orm:"column(id_type);size(3)" description:"证件类型"`
	IdNo            string `orm:"column(id_no);size(40)" description:"证件号码"`
	IssueAuthority  string `orm:"column(issue_authority);size(80);null" description:"发证机关"`
	IssueDate       string `orm:"column(issue_date);size(8);null" description:"发证日期"`
	IdExpDate       string `orm:"column(id_exp_date);size(8);null" description:"证件到期日"`
	LastMaintDate   string `orm:"column(last_maint_date);type(date);null" description:"最后修改日期 "`
	LastMaintTime   string `orm:"column(last_maint_time);type(time);null" description:"最后修改时间 "`
	LastMaintBrno   string `orm:"column(last_maint_brno);size(4);null" description:"最后修改机构号 "`
	LastMaintTell   string `orm:"column(last_maint_tell);size(6);null" description:"最后修改柜员号 "`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_cust_identity) TableName() string {
	return "t_cust_identity"
}

func InsertT_cust_identity(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_cust_identity).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_cust_identityTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_cust_identity)).Filter("id_type", maps["IdType"]).
		Filter("id_no", maps["IdNo"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_cust_identityTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_cust_identity where cust_id=? and id_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CustId"] ,maps["IdNo"]).Exec()
	return err
}

func QueryT_cust_identity(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_cust_identity panci "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_cust_identity))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_cust_identityById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_cust_identity)).Filter("cust_id", maps["CustId"]).
		Filter("id_no", maps["IdNo"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_cust_identity(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_cust_identity panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_cust_identity)).Filter("cust_id", maps["CustId"]).
		Filter("id_no", maps["IdNo"]).Filter("tcc_state", 0).Update(maps)
	return err
}
