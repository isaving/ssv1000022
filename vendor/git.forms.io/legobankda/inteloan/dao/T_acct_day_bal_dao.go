package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_acct_day_bal struct {
	AcctiAcctNo     string    `orm:"column(accti_acct_no);size(32);pk" description:"核算账号 "`
	Pridnum         int       `orm:"column(pridnum)" description:"期数"`
	ChgacctDt       string    `orm:"column(chgacct_dt);type(date)" description:"动账日期"`
	BalTypCd        string    `orm:"column(bal_typ_cd);size(3);null" description:"余额类型代码   0-正常,1-逾期"`
	AcctStusCd      string    `orm:"column(acct_stus_cd);size(5);null" description:"账户状态代码  1-正常,2-已还清,3-逾期,4-减值,5-核销"`
	IntacrPrinBal   float64   `orm:"column(intacr_prin_bal);null;digits(18);decimals(2)" description:"计息本金余额"`
	WrtoffAmt       float64   `orm:"column(wrtoff_amt);null;digits(18);decimals(2)" description:"核销金额"`
	DataValidFlgCd  string    `orm:"column(data_valid_flg_cd);size(1);null" description:"数据有效标志代码  1-正常,2-失效"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_acct_day_bal) TableName() string {
	return "t_acct_day_bal"
}

func InsertT_acct_day_bal(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_acct_day_bal).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_acct_day_balTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_acct_day_bal)).Filter("accti_acct_no", maps["AcctiAcctNo"]).
		Filter("pridnum", maps["Pridnum"]).Filter("chgacct_dt", maps["ChgacctDt"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_acct_day_balTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_acct_day_bal where accti_acct_no=? and pridnum=? and chgacct_dt=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctiAcctNo"], maps["Pridnum"],maps["ChgacctDt"]).Exec()
	return err
}

func QueryT_acct_day_bal(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_acct_day_bal panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_acct_day_bal))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_acct_day_balById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_acct_day_bal)).Filter("accti_acct_no", maps["AcctiAcctNo"]).Filter("pridnum", maps["Pridnum"]).
		Filter("chgacct_dt", maps["ChgacctDt"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_acct_day_bal(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_acct_day_bal panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_acct_day_bal)).Filter("accti_acct_no", maps["AcctiAcctNo"]).Filter("pridnum", maps["Pridnum"]).
		Filter("chgacct_dt", maps["ChgacctDt"]).Filter("tcc_state", 0).Update(maps)
	return err
}
