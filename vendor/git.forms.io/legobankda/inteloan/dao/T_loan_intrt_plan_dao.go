package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_intrt_plan struct {
	AcctiAcctNo     string    `orm:"column(accti_acct_no);size(32);null" description:"核算账号  16位核算账号生成规则:机构码4位+币种代码3位+系统顺序号或流水号9位编号规则:举例:机构:0401,币种代码:156或CNY生成贷款核算账号:0401156010000006"`
	BizFolnNo       string    `orm:"column(biz_foln_no);size(32);pk" description:"业务跟踪号  标识一笔业务的唯一识别流水号,32位组成为:客户端类型编码6位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号12位"`
	SysFolnNo       string    `orm:"column(sys_foln_no);size(32)" description:"系统跟踪号  标识一笔渠道接入交易在四川农信内部各系统之间交互的唯一识别流水号,32位组成为:客户端接入软件产品编码3位+客户端接入软件产品部署节点编码6位+自然日期8位+客户端接入顺序号15位"`
	DlwthStusCd     string    `orm:"column(dlwth_stus_cd);size(4);null" description:"处理状态代码  N-正常,C-作废,B-冲正"`
	CurCd           string    `orm:"column(cur_cd);size(3);null" description:"币种代码  参考标准代码:CD0040"`
	TxAcctnDt       string    `orm:"column(tx_acctn_dt);type(date);null" description:"交易会计日期"`
	MgmtOrgNo       string    `orm:"column(mgmt_org_no);size(4);null" description:"管理机构号  记录存款账户管理归属机构编号,引用自机构主题的机构编号,"`
	AcctiOrgNo      string    `orm:"column(accti_org_no);size(4);null" description:"核算机构号  会计核算机构的编号"`
	ChnlCd          string    `orm:"column(chnl_cd);size(4);null" description:"渠道编码  参考''SCR-TS-GUI-028'四川省农村信用社联合社信息科技中心渠道信息采集及公共报文头定义规范.pdf'中渠道类型编码定义表TRM-柜面,IBS-网银,"`
	RpyintAmt       float64   `orm:"column(rpyint_amt);null;digits(18);decimals(2)" description:"还息金额"`
	RpyintModeCd    string    `orm:"column(rpyint_mode_cd);size(2);null" description:"还息模式代码  1-批次还息2-联机还息,"`
	Tmprd           int       `orm:"column(tmprd)" description:"期次"`
	IntrStusCd      string    `orm:"column(intr_stus_cd);size(2);null" description:"利息状态代码  0-正常,1-逾期"`
	RpyintTypCd     string    `orm:"column(rpyint_typ_cd);size(1)" description:"还息类型代码  1-表内利息,2-复利或罚息复利,3-减值,4-核销"`
	TrsctnId        string    `orm:"column(trsctn_id);size(50);null" description:"事务id"`
	TccStusCd       string    `orm:"column(tcc_stus_cd);size(1);null" description:"tcc状态代码  0-try,1-confirm,2-cancel"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_intrt_plan) TableName() string {
	return "t_loan_intrt_plan"
}

func InsertT_loan_intrt_plan(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_intrt_plan).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_intrt_planTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_intrt_plan)).Filter("biz_foln_no", maps["BizFolnNo"]).
		Filter("sys_foln_no", maps["SysFolnNo"]).Filter("tmprd", maps["Tmprd"]).Filter("rpyint_typ_cd", maps["RpyintTypCd"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_intrt_planTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_intrt_plan where biz_foln_no=? and sys_foln_no=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["BizFolnNo"], maps["SysFolnNo"]).Exec()
	return err
}

func QueryT_loan_intrt_plan(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_intrt_plan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_intrt_plan))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_intrt_planById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_intrt_plan)).Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).
		Filter("tmprd", maps["Tmprd"]).Filter("rpyint_typ_cd", maps["RpyintTypCd"]).Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_intrt_plan(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_intrt_plan panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_intrt_plan)).Filter("biz_foln_no", maps["BizFolnNo"]).Filter("sys_foln_no", maps["SysFolnNo"]).
		Filter("tmprd", maps["Tmprd"]).Filter("rpyint_typ_cd", maps["RpyintTypCd"]).Filter("tcc_state", 0).Update(maps)
	return err
}
