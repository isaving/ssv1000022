package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_orgaz_daily_flow struct {
	AcctnDt         string    `orm:"column(acctn_dt);pk;type(date)" description:"会计日期"`
	TxSn            string    `orm:"column(tx_sn);size(32)" description:"交易流水号  生成规则:应用2位,+,日期时间14位+'YYYYMMDDHHMMSS'+,随机整数8位编号规则:1',随机数生成:(int)(Math.random()*100000000)"`
	TxDt            string    `orm:"column(tx_dt);type(date);null" description:"交易日期"`
	TxTm            string    `orm:"column(tx_tm);type(time);null" description:"交易时间"`
	TxChnlCd        string    `orm:"column(tx_chnl_cd);size(4);null" description:"交易渠道编码"`
	LprOrgNo        string    `orm:"column(lpr_org_no);size(4);null" description:"法人机构号"`
	OprOrgNo        string    `orm:"column(opr_org_no);size(4);null" description:"经办机构号"`
	OprorTelrNo     string    `orm:"column(opror_telr_no);size(6);null" description:"经办人柜员号"`
	TerminalNo      string    `orm:"column(terminal_no);size(20);null" description:"终端编号"`
	AuthTelrNo      string    `orm:"column(auth_telr_no);size(6);null" description:"授权柜员号"`
	LoanProdtNo     string    `orm:"column(loan_prodt_no);size(20);null" description:"贷款产品编号  8位产品编号生成规则:产品设计阶段'1位'+产品分类1段'1位'+产品分类2段'1位'+序号'5位'产品设计阶段:B基础产品,S可售产品,产品分类'1段':0金融,1衍生,产品分类'1段'+,产品分类'2段',,,,,,,金融类产品:01:存款,02:贷款,03:,理财,,,,,,,,,,,,,,,,,,,,,,,,,,,,04:基金,05:保险,,,,,,,,衍生:11:积分,12:红包,,13:,卡券,14:商品举例:产品设计阶段:可售产品+产品分类1段:金融,产品分类:'金融'贷款,序号:00001生成产品编号:S0200001"`
	CurCd           string    `orm:"column(cur_cd);size(4);null" description:"币种代码  参考标准代码:CD0040"`
	CustNo          string    `orm:"column(cust_no);size(14);null" description:"客户编号"`
	TxTypCd         string    `orm:"column(tx_typ_cd);size(4);null" description:"交易类型代码  参考标准代码:CD00961-放款,2-还款,3-放款冲正,4-还款冲正,5-核销,7-核销贷款收回"`
	TxAmt           float64   `orm:"column(tx_amt);null;digits(18);decimals(2)" description:"交易金额"`
	Prin            float64   `orm:"column(prin);null;digits(18);decimals(2)" description:"本金"`
	Intr            float64   `orm:"column(intr);null;digits(18);decimals(2)" description:"利息"`
	Pondg           float64   `orm:"column(pondg);null;digits(18);decimals(2)" description:"手续费"`
	OthFee          float64   `orm:"column(oth_fee);null;digits(18);decimals(2)" description:"其他费用"`
	RvrsIndCd       string    `orm:"column(rvrs_ind_cd);size(3);null" description:"冲正标识代码  1-正常,2-冲正撤销抹账"`
	FinlModfyDt     string    `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm     string    `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo  string    `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo string    `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState             int       `orm:"column(tcc_state);null"`
}

func (t *T_loan_orgaz_daily_flow) TableName() string {
	return "t_loan_orgaz_daily_flow"
}

func InsertT_loan_orgaz_daily_flow(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_orgaz_daily_flow).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_orgaz_daily_flowTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_orgaz_daily_flow)).Filter("acctn_dt", maps["AcctnDt"]).
		Filter("tx_sn", maps["TxSn"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_orgaz_daily_flowTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_orgaz_daily_flow where acctn_dt=? and tx_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["AcctnDt"], maps["TxSn"]).Exec()
	return err
}

func QueryT_loan_orgaz_daily_flow(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_orgaz_daily_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(T_loan_orgaz_daily_flow))
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_orgaz_daily_flowById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_orgaz_daily_flow)).Filter("acctn_dt", maps["AcctnDt"]).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_orgaz_daily_flow(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_orgaz_daily_flow panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_orgaz_daily_flow)).Filter("acctn_dt", maps["AcctnDt"]).Filter("tx_sn", maps["TxSn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
