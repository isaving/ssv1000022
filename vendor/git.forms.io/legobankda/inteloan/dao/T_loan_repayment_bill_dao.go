package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

type T_loan_repayment_bill struct {
	LoanDubilNo            string  `orm:"column(loan_dubil_no);size(32);pk" description:"贷款借据号  智能贷款中贷款账号和借据号相同,27位贷款账号生成规则:合同号+序号'3位'+尾号'2位'编号规则:尾号两位使用6,8'66,68,86,88,4个数字随机拼接到序号最后'序号:合同项下借据的序号递增举例:合同号0401132019101400000168的第一笔借据,随机获取到88为尾号,生成贷款账号:040113201910140000016800188"`
	Pridnum                int     `orm:"column(pridnum)" description:"期数"`
	CustNo                 string  `orm:"column(cust_no);size(20)" description:"客户编号"`
	KeprcdStusCd           string  `orm:"column(keprcd_stus_cd);size(2);null" description:"记录状态代码  01-有效02-正常03-结清04-冻结05-中止06-待还款07-终止08-废除09-无效11-待审批12-新增待审批13-修改待审批14-移除待审批15-删除待审批21-转派废除,22-撤回废除,23-终止废除"`
	OperSorcCd             string  `orm:"column(oper_sorc_cd);size(1);null" description:"操作来源代码  技术字段,批量发起,还是联机发起,o-联机,,,b-批量"`
	PrinOvdueBgnDt         string  `orm:"column(prin_ovdue_bgn_dt);type(date);null" description:"本金逾期开始日期"`
	IntrOvdueBgnDt         string  `orm:"column(intr_ovdue_bgn_dt);type(date);null" description:"利息逾期开始日期"`
	CurrPeriodUseGraceDays int     `orm:"column(curr_period_use_grace_days);null" description:"当期使用宽限天数  产品层设置了还款允许使用宽限期,系统记录某一期使用了几天,"`
	ActlstTotlAmt          float64 `orm:"column(actlst_totl_amt);null;digits(22);decimals(2)" description:"实还总金额"`
	ActlRepayPrin          float64 `orm:"column(actl_repay_prin);null;digits(18);decimals(2)" description:"实际还款本金"`
	ActlRepayIntr          float64 `orm:"column(actl_repay_intr);null;digits(18);decimals(2)" description:"实际还款利息"`
	FinlRepayDt            string  `orm:"column(finl_repay_dt);type(date);null" description:"最后还款日期"`
	ActlPyfDt              string  `orm:"column(actl_pyf_dt);type(date);null" description:"实际还清日期"`
	RepayDt                string  `orm:"column(repay_dt);type(date);null" description:"还款日期"`
	FinlModfyDt            string  `orm:"column(finl_modfy_dt);type(date);null" description:"最后修改日期"`
	FinlModfyTm            string  `orm:"column(finl_modfy_tm);type(time);null" description:"最后修改时间"`
	FinlModfyOrgNo         string  `orm:"column(finl_modfy_org_no);size(4);null" description:"最后修改机构号"`
	FinlModfyTelrNo        string  `orm:"column(finl_modfy_telr_no);size(6);null" description:"最后修改柜员号"`
	TccState               int     `orm:"column(tcc_state);null"`
}

func (t *T_loan_repayment_bill) TableName() string {
	return "t_loan_repayment_bill"
}

func InsertT_loan_repayment_bill(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(T_loan_repayment_bill).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateT_loan_repayment_billTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(T_loan_repayment_bill)).Filter("loan_dubil_no", maps["LoanDubilNo"]).
		Filter("pridnum", maps["Pridnum"]).Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteT_loan_repayment_billTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from t_loan_repayment_bill where loan_dubil_no=? and pridnum=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["LoanDubilNo"], maps["Pridnum"]).Exec()
	return err
}

func QueryT_loan_repayment_bill(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryT_loan_repayment_bill panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	flag := params["Flag"]
	delete(params, "Flag")
	qs := o.QueryTable(new(T_loan_repayment_bill))
	if flag == "8" {
		sql := "SELECT  t2.accti_acct_no AcctiAcctNo,t1.* FROM loan.t_loan_repayment_bill t1,loan.t_loan_loan_cont t2 "+
		" WHERE t1.loan_dubil_no=t2.loan_dubil_no AND keprcd_stus_cd='1' "+
		" ORDER BY CUST_NO,loan_dubil_no,pridnum "
		_,err =o.Raw(sql).Values(&maps)
		if err != nil {
			return nil,err
		}
		return
	}
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			qs = qs.Filter(k, v)
		}
	}
	if flag == "9" {
		if _, err = qs.Filter("tcc_state", state).OrderBy("prin_ovdue_bgn_dt").Limit(1).Values(&maps); err != nil {
			return nil, err
		}
		return
	}

	if _, err = qs.Filter("tcc_state", state).Limit(-1).Values(&maps); err != nil {
		return nil, err
	}
	return
}

func QueryT_loan_repayment_billById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(T_loan_repayment_bill)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("pridnum", maps["Pridnum"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateT_loan_repayment_bill(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateT_loan_repayment_bill panic " + fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(T_loan_repayment_bill)).Filter("loan_dubil_no", maps["LoanDubilNo"]).Filter("pridnum", maps["Pridnum"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
