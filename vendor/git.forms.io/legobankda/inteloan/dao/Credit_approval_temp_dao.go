package dao

import (
	"errors"
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	util "git.forms.io/legobankda/inteloan/utils"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"reflect"
	"strconv"
)

type Credit_approval_temp struct {
	ApprovalDt                       string    `orm:"column(approval_dt);type(time);null" description:"人工审批日期"`
	CrdtAplySn                       string    `orm:"column(crdt_aply_sn);pk" description:"流水号"`
	CorrespondingBusinessTriggerType string    `orm:"column(corresponding_business_trigger_type);size(1);null" description:"对应业务触发类型"`
	RplshInfo                        string    `orm:"column(rplsh_info);size(10);null" description:"人工审批结果"`
	ApprovalAdjAftQta                float64   `orm:"column(approval_adj_aft_qta);null;digits(9);decimals(2)" description:"人工调整授信额度"`
	ApprovalNm                       string    `orm:"column(approval_nm);size(50);null" description:"审批人员姓名"`
	ApprovalNo                       string    `orm:"column(approval_no);size(50);null" description:"审批人员工号"`
	ApprovalAuthority                string    `orm:"column(approval_authority);size(100);null" description:"审批人审批权限"`
	TccState            			 int       `orm:"column(tcc_state);null"`
}

func (t *Credit_approval_temp) TableName() string {
	return "credit_approval_temp"
}

func InsertCredit_approval_temp(o orm.Ormer, maps orm.Params) error {
	sql := util.BuildSql(maps, new(Credit_approval_temp).TableName())
	_, err := o.Raw(sql).Exec()
	return err
}

func UpdateCredit_approval_tempTccState(o orm.Ormer, maps orm.Params) error {
	_, err := o.QueryTable(new(Credit_approval_temp)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Update(orm.Params{"tcc_state": 0})
	return err
}

func DeleteCredit_approval_tempTccState(o orm.Ormer, maps orm.Params) error {
	sql := "delete from credit_approval_temp where crdt_aply_sn=? and tcc_state = 1"
	_, err := o.Raw(sql, maps["CrdtAplySn"]).Exec()
	return err
}

func QueryCredit_approval_temp(params orm.Params, state int8) (maps []orm.Params, err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("QueryCredit_approval_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	qs := o.QueryTable(new(Credit_approval_temp))
	var Fields []string
	var Limit interface{}
	Limit = -1
	for k, v := range params {
		if k != constant.PageRecCount && k != constant.PageNo {
			switch k {
			case "OrderBy":
				qs = qs.OrderBy(util.HandleInterfaceToArr(params["OrderBy"].([]interface{}))...)
			case "Fields":
				Fields = util.HandleInterfaceToArr(params["Fields"].([]interface{}))
			case "Limit":
				if reflect.TypeOf(params["Limit"]).String() == "string"{
					Limit,_ = strconv.Atoi(params["Limit"].(string))
				}else if reflect.TypeOf(params["Limit"]).String() == "float64"{
					Limit,_ = strconv.Atoi(fmt.Sprintf("%v",params["Limit"].(float64)))
				}
				//Limit,_ = strconv.Atoi(params["Limit"].(string))
			default:
				qs = qs.Filter(k, v)
			}
		}
	}
	if _, err = qs.Filter("tcc_state", state).Limit(Limit).Values(&maps,Fields...); err != nil {
		return nil, err
	}
	//HandleDate()
	util.TestBuildsheet()
	return
}

func QueryCredit_approval_tempById(maps orm.Params, state int8) (info orm.Params, err error) {
	o := orm.NewOrm()
	mapList := []orm.Params{}
	if _, err = o.QueryTable(new(Credit_approval_temp)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", state).Values(&mapList); err != nil {
		return nil, err
	}
	if len(mapList) > 0 {
		return mapList[0], nil
	} else {
		err = errors.New("records not found")
		return nil, err
	}
}

func UpdateCredit_approval_temp(maps orm.Params) (err error) {
	defer func() {
		if e := recover(); e != nil {
			log.Error(e.(interface{}))
			err = errors.New("UpdateCredit_approval_temp panic "+fmt.Sprintf("%s", e.(interface{})))
		}
	}()
	o := orm.NewOrm()
	_, err = o.QueryTable(new(Credit_approval_temp)).Filter("crdt_aply_sn", maps["CrdtAplySn"]).
		Filter("tcc_state", 0).Update(maps)
	return err
}
func HandleDate(){
	o := orm.NewOrm()
	var maps []orm.Params
	sql := "select no No,源系统 A1,系统代码 A2,所属领域 A3,服务名称 A4,服务名称键值 A5,服务代码 A6,冒烟版本 A7,服务类型 A8 from lego_services"
	o.Raw(sql).Values(&maps)
	var slice []map[string]interface{}
	for _, m := range maps {
		slice = append(slice, m)
	}

	list := make([]string,0)
	for i,data := range slice{
		if i == 0 {
			list = append(list,data["No"].(string))
			continue
		}

		if data["A1"] == "" || data["A1"] == nil {
			n,_ := strconv.Atoi(list[len(list)-1])
			mapOne := slice[n-1]
			sql1 :="update lego_services set 源系统=?,系统代码=?,所属领域=?,服务名称=?,服务名称键值=?,服务代码=?,冒烟版本=?,服务类型=? where no=?"
			o.Raw(sql1,mapOne["A1"],mapOne["A2"],mapOne["A3"],mapOne["A4"],mapOne["A5"],mapOne["A6"],mapOne["A7"],mapOne["A8"],data["No"]).Exec()
		}else {
			list = append(list,data["No"].(string))
		}
	}
}
