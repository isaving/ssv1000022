// @APIVersion 1.0.0
// @Title mobile API
// @Description mobile has every tool to get any job done, so codename for the new mobile APIs.
// @Contact astaxie@gmail.com
package routers

import (
	"git.forms.io/legobankda/inteloan/controllers"
	"github.com/astaxie/beego"
)

func init() {
	ns :=
		beego.NewNamespace("/v1",

			beego.NSNamespace("/Acsjhfp",
				beego.NSInclude(
					&controllers.AcsjhfpController{},
				),
			),
		)
	beego.AddNamespace(ns)
	//ns := beego.NewNamespace("/v1")
	////beego.NSNamespace("/acsprep",
	////	beego.NSInclude(
	////		&controllers.AcsprepController{},
	////	),
	////),
	//
	////beego.NSNamespace("/lloanIncomingController", beego.NSInclude(&controllers.AcsprepController{})),
	//
	//beego.AddNamespace(ns)

	beego.Router("/IL8R0063", &controllers.T_loan_consu_detController{},"*:QueryT_loan_consu_det")
}
