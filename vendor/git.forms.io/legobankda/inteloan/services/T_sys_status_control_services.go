package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sys_status_controlCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sys_status_control",
	ConfirmMethod: "ConfirmInsertT_sys_status_control",
	CancelMethod:  "CancelInsertT_sys_status_control",
}

type T_sys_status_controlService interface {
	TryInsertT_sys_status_control(maps orm.Params) error
	ConfirmInsertT_sys_status_control(maps orm.Params) error
	CancelInsertT_sys_status_control(maps orm.Params) error
	TryUpdateT_sys_status_control(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sys_status_control(maps orm.Params) error
	CancelUpdateT_sys_status_control(maps orm.Params) error
}

type T_sys_status_controlServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sys_status_controlServiceImpl) TryInsertT_sys_status_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sys_status_control(o, maps)
}

func (impl *T_sys_status_controlServiceImpl) ConfirmInsertT_sys_status_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sys_status_controlTccState(o, maps)
}

func (impl *T_sys_status_controlServiceImpl) CancelInsertT_sys_status_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sys_status_controlTccState(o, maps)
}

//智能查询
func (impl *T_sys_status_controlServiceImpl) QueryT_sys_status_control(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sys_status_control(params, 0)
}

var UpdateT_sys_status_controlCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sys_status_control",
	ConfirmMethod: "ConfirmUpdateT_sys_status_control",
	CancelMethod:  "CancelUpdateT_sys_status_control",
}

func (impl *T_sys_status_controlServiceImpl) TryUpdateT_sys_status_control(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sys_status_controlById(params, 0)
	return
}

func (impl *T_sys_status_controlServiceImpl) ConfirmUpdateT_sys_status_control(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sys_status_control(maps)
}

func (impl *T_sys_status_controlServiceImpl) CancelUpdateT_sys_status_control(maps orm.Params) (err error) {
	return
}
