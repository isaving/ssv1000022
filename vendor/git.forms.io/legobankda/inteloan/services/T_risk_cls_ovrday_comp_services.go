package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_risk_cls_ovrday_compCompensable = client.Compensable{
	TryMethod:     "TryInsertT_risk_cls_ovrday_comp",
	ConfirmMethod: "ConfirmInsertT_risk_cls_ovrday_comp",
	CancelMethod:  "CancelInsertT_risk_cls_ovrday_comp",
}

type T_risk_cls_ovrday_compService interface {
	TryInsertT_risk_cls_ovrday_comp(maps orm.Params) error
	ConfirmInsertT_risk_cls_ovrday_comp(maps orm.Params) error
	CancelInsertT_risk_cls_ovrday_comp(maps orm.Params) error
	TryUpdateT_risk_cls_ovrday_comp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_risk_cls_ovrday_comp(maps orm.Params) error
	CancelUpdateT_risk_cls_ovrday_comp(maps orm.Params) error
}

type T_risk_cls_ovrday_compServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_risk_cls_ovrday_compServiceImpl) TryInsertT_risk_cls_ovrday_comp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_risk_cls_ovrday_comp(o, maps)
}

func (impl *T_risk_cls_ovrday_compServiceImpl) ConfirmInsertT_risk_cls_ovrday_comp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_risk_cls_ovrday_compTccState(o, maps)
}

func (impl *T_risk_cls_ovrday_compServiceImpl) CancelInsertT_risk_cls_ovrday_comp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_risk_cls_ovrday_compTccState(o, maps)
}

//智能查询
func (impl *T_risk_cls_ovrday_compServiceImpl) QueryT_risk_cls_ovrday_comp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_risk_cls_ovrday_comp(params, 0)
}

var UpdateT_risk_cls_ovrday_compCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_risk_cls_ovrday_comp",
	ConfirmMethod: "ConfirmUpdateT_risk_cls_ovrday_comp",
	CancelMethod:  "CancelUpdateT_risk_cls_ovrday_comp",
}

func (impl *T_risk_cls_ovrday_compServiceImpl) TryUpdateT_risk_cls_ovrday_comp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_risk_cls_ovrday_compById(params, 0)
	return
}

func (impl *T_risk_cls_ovrday_compServiceImpl) ConfirmUpdateT_risk_cls_ovrday_comp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_risk_cls_ovrday_comp(maps)
}

func (impl *T_risk_cls_ovrday_compServiceImpl) CancelUpdateT_risk_cls_ovrday_comp(maps orm.Params) (err error) {
	return
}
