package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sms_template_tempCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sms_template_temp",
	ConfirmMethod: "ConfirmInsertT_sms_template_temp",
	CancelMethod:  "CancelInsertT_sms_template_temp",
}

type T_sms_template_tempService interface {
	TryInsertT_sms_template_temp(maps orm.Params) error
	ConfirmInsertT_sms_template_temp(maps orm.Params) error
	CancelInsertT_sms_template_temp(maps orm.Params) error
	TryUpdateT_sms_template_temp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sms_template_temp(maps orm.Params) error
	CancelUpdateT_sms_template_temp(maps orm.Params) error
}

type T_sms_template_tempServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sms_template_tempServiceImpl) TryInsertT_sms_template_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sms_template_temp(o, maps)
}

func (impl *T_sms_template_tempServiceImpl) ConfirmInsertT_sms_template_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sms_template_tempTccState(o, maps)
}

func (impl *T_sms_template_tempServiceImpl) CancelInsertT_sms_template_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sms_template_tempTccState(o, maps)
}

//智能查询
func (impl *T_sms_template_tempServiceImpl) QueryT_sms_template_temp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sms_template_temp(params, 0)
}

var UpdateT_sms_template_tempCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sms_template_temp",
	ConfirmMethod: "ConfirmUpdateT_sms_template_temp",
	CancelMethod:  "CancelUpdateT_sms_template_temp",
}

func (impl *T_sms_template_tempServiceImpl) TryUpdateT_sms_template_temp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sms_template_tempById(params, 0)
	return
}

func (impl *T_sms_template_tempServiceImpl) ConfirmUpdateT_sms_template_temp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sms_template_temp(maps)
}

func (impl *T_sms_template_tempServiceImpl) CancelUpdateT_sms_template_temp(maps orm.Params) (err error) {
	return
}
