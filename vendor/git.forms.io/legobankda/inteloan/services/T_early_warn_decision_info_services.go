package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_early_warn_decision_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertT_early_warn_decision_info",
	ConfirmMethod: "ConfirmInsertT_early_warn_decision_info",
	CancelMethod:  "CancelInsertT_early_warn_decision_info",
}

type T_early_warn_decision_infoService interface {
	TryInsertT_early_warn_decision_info(maps orm.Params) error
	ConfirmInsertT_early_warn_decision_info(maps orm.Params) error
	CancelInsertT_early_warn_decision_info(maps orm.Params) error
	TryUpdateT_early_warn_decision_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_early_warn_decision_info(maps orm.Params) error
	CancelUpdateT_early_warn_decision_info(maps orm.Params) error
}

type T_early_warn_decision_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_early_warn_decision_infoServiceImpl) TryInsertT_early_warn_decision_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_early_warn_decision_info(o, maps)
}

func (impl *T_early_warn_decision_infoServiceImpl) ConfirmInsertT_early_warn_decision_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_early_warn_decision_infoTccState(o, maps)
}

func (impl *T_early_warn_decision_infoServiceImpl) CancelInsertT_early_warn_decision_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_early_warn_decision_infoTccState(o, maps)
}

//智能查询
func (impl *T_early_warn_decision_infoServiceImpl) QueryT_early_warn_decision_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_early_warn_decision_info(params, 0)
}

var UpdateT_early_warn_decision_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_early_warn_decision_info",
	ConfirmMethod: "ConfirmUpdateT_early_warn_decision_info",
	CancelMethod:  "CancelUpdateT_early_warn_decision_info",
}

func (impl *T_early_warn_decision_infoServiceImpl) TryUpdateT_early_warn_decision_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_early_warn_decision_infoById(params, 0)
	return
}

func (impl *T_early_warn_decision_infoServiceImpl) ConfirmUpdateT_early_warn_decision_info(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_early_warn_decision_info(maps)
}

func (impl *T_early_warn_decision_infoServiceImpl) CancelUpdateT_early_warn_decision_info(maps orm.Params) (err error) {
	return
}
