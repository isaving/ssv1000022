package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertTs_sys_user_roleCompensable = client.Compensable{
	TryMethod:     "TryInsertTs_sys_user_role",
	ConfirmMethod: "ConfirmInsertTs_sys_user_role",
	CancelMethod:  "CancelInsertTs_sys_user_role",
}

type Ts_sys_user_roleService interface {
	TryInsertTs_sys_user_role(maps orm.Params) error
	ConfirmInsertTs_sys_user_role(maps orm.Params) error
	CancelInsertTs_sys_user_role(maps orm.Params) error
	TryUpdateTs_sys_user_role(maps orm.Params) (orm.Params, error)
	ConfirmUpdateTs_sys_user_role(maps orm.Params) error
	CancelUpdateTs_sys_user_role(maps orm.Params) error
}

type Ts_sys_user_roleServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Ts_sys_user_roleServiceImpl) TryInsertTs_sys_user_role(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertTs_sys_user_role(o, maps)
}

func (impl *Ts_sys_user_roleServiceImpl) ConfirmInsertTs_sys_user_role(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateTs_sys_user_roleTccState(o, maps)
}

func (impl *Ts_sys_user_roleServiceImpl) CancelInsertTs_sys_user_role(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteTs_sys_user_roleTccState(o, maps)
}

//智能查询
func (impl *Ts_sys_user_roleServiceImpl) QueryTs_sys_user_role(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryTs_sys_user_role(params, 0)
}

var UpdateTs_sys_user_roleCompensable = client.Compensable{
	TryMethod:     "TryUpdateTs_sys_user_role",
	ConfirmMethod: "ConfirmUpdateTs_sys_user_role",
	CancelMethod:  "CancelUpdateTs_sys_user_role",
}

func (impl *Ts_sys_user_roleServiceImpl) TryUpdateTs_sys_user_role(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTs_sys_user_roleById(params, 0)
	return
}

func (impl *Ts_sys_user_roleServiceImpl) ConfirmUpdateTs_sys_user_role(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateTs_sys_user_role(maps)
}

func (impl *Ts_sys_user_roleServiceImpl) CancelUpdateTs_sys_user_role(maps orm.Params) (err error) {
	return
}
