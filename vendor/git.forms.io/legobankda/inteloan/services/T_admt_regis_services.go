package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_admt_regisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_admt_regis",
	ConfirmMethod: "ConfirmInsertT_admt_regis",
	CancelMethod:  "CancelInsertT_admt_regis",
}

type T_admt_regisService interface {
	TryInsertT_admt_regis(maps orm.Params) error
	ConfirmInsertT_admt_regis(maps orm.Params) error
	CancelInsertT_admt_regis(maps orm.Params) error
	TryUpdateT_admt_regis(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_admt_regis(maps orm.Params) error
	CancelUpdateT_admt_regis(maps orm.Params) error
}

type T_admt_regisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_admt_regisServiceImpl) TryInsertT_admt_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_admt_regis(o, maps)
}

func (impl *T_admt_regisServiceImpl) ConfirmInsertT_admt_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_admt_regisTccState(o, maps)
}

func (impl *T_admt_regisServiceImpl) CancelInsertT_admt_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_admt_regisTccState(o, maps)
}

//智能查询
func (impl *T_admt_regisServiceImpl) QueryT_admt_regis(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_admt_regis(params, 0)
}

var UpdateT_admt_regisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_admt_regis",
	ConfirmMethod: "ConfirmUpdateT_admt_regis",
	CancelMethod:  "CancelUpdateT_admt_regis",
}

func (impl *T_admt_regisServiceImpl) TryUpdateT_admt_regis(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_admt_regisById(params, 0)
	return
}

func (impl *T_admt_regisServiceImpl) ConfirmUpdateT_admt_regis(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_admt_regis(maps)
}

func (impl *T_admt_regisServiceImpl) CancelUpdateT_admt_regis(maps orm.Params) (err error) {
	return
}
