package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_seq_noCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_seq_no",
	ConfirmMethod: "ConfirmInsertT_loan_seq_no",
	CancelMethod:  "CancelInsertT_loan_seq_no",
}

type T_loan_seq_noService interface {
	TryInsertT_loan_seq_no(maps orm.Params) error
	ConfirmInsertT_loan_seq_no(maps orm.Params) error
	CancelInsertT_loan_seq_no(maps orm.Params) error
	TryUpdateT_loan_seq_no(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_seq_no(maps orm.Params) error
	CancelUpdateT_loan_seq_no(maps orm.Params) error
}

type T_loan_seq_noServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_seq_noServiceImpl) TryInsertT_loan_seq_no(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_seq_no(o, maps)
}

func (impl *T_loan_seq_noServiceImpl) ConfirmInsertT_loan_seq_no(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_seq_noTccState(o, maps)
}

func (impl *T_loan_seq_noServiceImpl) CancelInsertT_loan_seq_no(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_seq_noTccState(o, maps)
}

//智能查询
func (impl *T_loan_seq_noServiceImpl) QueryT_loan_seq_no(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_seq_no(params, 0)
}

var UpdateT_loan_seq_noCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_seq_no",
	ConfirmMethod: "ConfirmUpdateT_loan_seq_no",
	CancelMethod:  "CancelUpdateT_loan_seq_no",
}

func (impl *T_loan_seq_noServiceImpl) TryUpdateT_loan_seq_no(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_seq_noById(params, 0)
	return
}

func (impl *T_loan_seq_noServiceImpl) ConfirmUpdateT_loan_seq_no(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_seq_no(maps)
}

func (impl *T_loan_seq_noServiceImpl) CancelUpdateT_loan_seq_no(maps orm.Params) (err error) {
	return
}
