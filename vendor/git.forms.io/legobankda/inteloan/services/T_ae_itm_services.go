package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_itmCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_itm",
	ConfirmMethod: "ConfirmInsertT_ae_itm",
	CancelMethod:  "CancelInsertT_ae_itm",
}

type T_ae_itmService interface {
	TryInsertT_ae_itm(maps orm.Params) error
	ConfirmInsertT_ae_itm(maps orm.Params) error
	CancelInsertT_ae_itm(maps orm.Params) error
	TryUpdateT_ae_itm(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_itm(maps orm.Params) error
	CancelUpdateT_ae_itm(maps orm.Params) error
}

type T_ae_itmServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_itmServiceImpl) TryInsertT_ae_itm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_itm(o, maps)
}

func (impl *T_ae_itmServiceImpl) ConfirmInsertT_ae_itm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_itmTccState(o, maps)
}

func (impl *T_ae_itmServiceImpl) CancelInsertT_ae_itm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_itmTccState(o, maps)
}

//智能查询
func (impl *T_ae_itmServiceImpl) QueryT_ae_itm(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_itm(params, 0)
}

var UpdateT_ae_itmCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_itm",
	ConfirmMethod: "ConfirmUpdateT_ae_itm",
	CancelMethod:  "CancelUpdateT_ae_itm",
}

func (impl *T_ae_itmServiceImpl) TryUpdateT_ae_itm(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_itmById(params, 0)
	return
}

func (impl *T_ae_itmServiceImpl) ConfirmUpdateT_ae_itm(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_itm(maps)
}

func (impl *T_ae_itmServiceImpl) CancelUpdateT_ae_itm(maps orm.Params) (err error) {
	return
}
