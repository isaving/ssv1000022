package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intrt_planCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intrt_plan",
	ConfirmMethod: "ConfirmInsertT_intrt_plan",
	CancelMethod:  "CancelInsertT_intrt_plan",
}

type T_intrt_planService interface {
	TryInsertT_intrt_plan(maps orm.Params) error
	ConfirmInsertT_intrt_plan(maps orm.Params) error
	CancelInsertT_intrt_plan(maps orm.Params) error
	TryUpdateT_intrt_plan(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intrt_plan(maps orm.Params) error
	CancelUpdateT_intrt_plan(maps orm.Params) error
}

type T_intrt_planServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intrt_planServiceImpl) TryInsertT_intrt_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intrt_plan(o, maps)
}

func (impl *T_intrt_planServiceImpl) ConfirmInsertT_intrt_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intrt_planTccState(o, maps)
}

func (impl *T_intrt_planServiceImpl) CancelInsertT_intrt_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intrt_planTccState(o, maps)
}

//智能查询
func (impl *T_intrt_planServiceImpl) QueryT_intrt_plan(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intrt_plan(params, 0)
}

var UpdateT_intrt_planCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intrt_plan",
	ConfirmMethod: "ConfirmUpdateT_intrt_plan",
	CancelMethod:  "CancelUpdateT_intrt_plan",
}

func (impl *T_intrt_planServiceImpl) TryUpdateT_intrt_plan(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intrt_planById(params, 0)
	return
}

func (impl *T_intrt_planServiceImpl) ConfirmUpdateT_intrt_plan(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intrt_plan(maps)
}

func (impl *T_intrt_planServiceImpl) CancelUpdateT_intrt_plan(maps orm.Params) (err error) {
	return
}
