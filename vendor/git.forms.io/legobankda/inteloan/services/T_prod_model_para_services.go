package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_prod_model_paraCompensable = client.Compensable{
	TryMethod:     "TryInsertT_prod_model_para",
	ConfirmMethod: "ConfirmInsertT_prod_model_para",
	CancelMethod:  "CancelInsertT_prod_model_para",
}

type T_prod_model_paraService interface {
	TryInsertT_prod_model_para(maps orm.Params) error
	ConfirmInsertT_prod_model_para(maps orm.Params) error
	CancelInsertT_prod_model_para(maps orm.Params) error
	TryUpdateT_prod_model_para(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_prod_model_para(maps orm.Params) error
	CancelUpdateT_prod_model_para(maps orm.Params) error
}

type T_prod_model_paraServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_prod_model_paraServiceImpl) TryInsertT_prod_model_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_prod_model_para(o, maps)
}

func (impl *T_prod_model_paraServiceImpl) ConfirmInsertT_prod_model_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_prod_model_paraTccState(o, maps)
}

func (impl *T_prod_model_paraServiceImpl) CancelInsertT_prod_model_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_prod_model_paraTccState(o, maps)
}

//智能查询
func (impl *T_prod_model_paraServiceImpl) QueryT_prod_model_para(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_prod_model_para(params, 0)
}

var UpdateT_prod_model_paraCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_prod_model_para",
	ConfirmMethod: "ConfirmUpdateT_prod_model_para",
	CancelMethod:  "CancelUpdateT_prod_model_para",
}

func (impl *T_prod_model_paraServiceImpl) TryUpdateT_prod_model_para(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_prod_model_paraById(params, 0)
	return
}

func (impl *T_prod_model_paraServiceImpl) ConfirmUpdateT_prod_model_para(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_prod_model_para(maps)
}

func (impl *T_prod_model_paraServiceImpl) CancelUpdateT_prod_model_para(maps orm.Params) (err error) {
	return
}

var DeleteT_prod_model_paraCompensable = client.Compensable{
	TryMethod:     "TryDeleteT_prod_model_para",
	ConfirmMethod: "ConfirmDeleteT_prod_model_para",
	CancelMethod:  "CancelDeleteT_prod_model_para",
}

func (impl *T_prod_model_paraServiceImpl) TryDeleteT_prod_model_para(params orm.Params) (data map[string]interface{}, err error) {
	data, err = dao.QueryT_prod_model_paraById(params, 0)
	return

}

func (impl *T_prod_model_paraServiceImpl) ConfirmDeleteT_prod_model_para(params orm.Params) (err error) {
	return dao.DeleteT_prod_model_para(params)
}

func (impl *T_prod_model_paraServiceImpl) CancelDeleteT_prod_model_para(params orm.Params) (err error) {
	return
}
