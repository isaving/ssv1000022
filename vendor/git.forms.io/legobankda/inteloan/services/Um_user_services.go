package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertUm_userCompensable = client.Compensable{
	TryMethod:     "TryInsertUm_user",
	ConfirmMethod: "ConfirmInsertUm_user",
	CancelMethod:  "CancelInsertUm_user",
}

type Um_userService interface {
	TryInsertUm_user(maps orm.Params) error
	ConfirmInsertUm_user(maps orm.Params) error
	CancelInsertUm_user(maps orm.Params) error
	TryUpdateUm_user(maps orm.Params) (orm.Params, error)
	ConfirmUpdateUm_user(maps orm.Params) error
	CancelUpdateUm_user(maps orm.Params) error
}

type Um_userServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Um_userServiceImpl) TryInsertUm_user(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertUm_user(o, maps)
}

func (impl *Um_userServiceImpl) ConfirmInsertUm_user(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateUm_userTccState(o, maps)
}

func (impl *Um_userServiceImpl) CancelInsertUm_user(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteUm_userTccState(o, maps)
}

//智能查询
func (impl *Um_userServiceImpl) QueryUm_user(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryUm_user(params, 0)
}

var UpdateUm_userCompensable = client.Compensable{
	TryMethod:     "TryUpdateUm_user",
	ConfirmMethod: "ConfirmUpdateUm_user",
	CancelMethod:  "CancelUpdateUm_user",
}

func (impl *Um_userServiceImpl) TryUpdateUm_user(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryUm_userById(params, 0)
	return
}

func (impl *Um_userServiceImpl) ConfirmUpdateUm_user(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateUm_user(maps)
}

func (impl *Um_userServiceImpl) CancelUpdateUm_user(maps orm.Params) (err error) {
	return
}
