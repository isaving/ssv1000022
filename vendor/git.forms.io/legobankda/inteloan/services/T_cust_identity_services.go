package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_cust_identityCompensable = client.Compensable{
	TryMethod:     "TryInsertT_cust_identity",
	ConfirmMethod: "ConfirmInsertT_cust_identity",
	CancelMethod:  "CancelInsertT_cust_identity",
}

type T_cust_identityService interface {
	TryInsertT_cust_identity(maps orm.Params) error
	ConfirmInsertT_cust_identity(maps orm.Params) error
	CancelInsertT_cust_identity(maps orm.Params) error
	TryUpdateT_cust_identity(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_cust_identity(maps orm.Params) error
	CancelUpdateT_cust_identity(maps orm.Params) error
}

type T_cust_identityServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_cust_identityServiceImpl) TryInsertT_cust_identity(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_cust_identity(o, maps)
}

func (impl *T_cust_identityServiceImpl) ConfirmInsertT_cust_identity(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_cust_identityTccState(o, maps)
}

func (impl *T_cust_identityServiceImpl) CancelInsertT_cust_identity(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_cust_identityTccState(o, maps)
}

//智能查询
func (impl *T_cust_identityServiceImpl) QueryT_cust_identity(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_cust_identity(params, 0)
}

var UpdateT_cust_identityCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_cust_identity",
	ConfirmMethod: "ConfirmUpdateT_cust_identity",
	CancelMethod:  "CancelUpdateT_cust_identity",
}

func (impl *T_cust_identityServiceImpl) TryUpdateT_cust_identity(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_cust_identityById(params, 0)
	return
}

func (impl *T_cust_identityServiceImpl) ConfirmUpdateT_cust_identity(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_cust_identity(maps)
}

func (impl *T_cust_identityServiceImpl) CancelUpdateT_cust_identity(maps orm.Params) (err error) {
	return
}
