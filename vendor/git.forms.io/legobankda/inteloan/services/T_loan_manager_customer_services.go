package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_manager_customerCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_manager_customer",
	ConfirmMethod: "ConfirmInsertT_loan_manager_customer",
	CancelMethod:  "CancelInsertT_loan_manager_customer",
}

type T_loan_manager_customerService interface {
	TryInsertT_loan_manager_customer(maps orm.Params) error
	ConfirmInsertT_loan_manager_customer(maps orm.Params) error
	CancelInsertT_loan_manager_customer(maps orm.Params) error
	TryUpdateT_loan_manager_customer(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_manager_customer(maps orm.Params) error
	CancelUpdateT_loan_manager_customer(maps orm.Params) error
}

type T_loan_manager_customerServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_manager_customerServiceImpl) TryInsertT_loan_manager_customer(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_manager_customer(o, maps)
}

func (impl *T_loan_manager_customerServiceImpl) ConfirmInsertT_loan_manager_customer(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_manager_customerTccState(o, maps)
}

func (impl *T_loan_manager_customerServiceImpl) CancelInsertT_loan_manager_customer(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_manager_customerTccState(o, maps)
}

//智能查询
func (impl *T_loan_manager_customerServiceImpl) QueryT_loan_manager_customer(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_manager_customer(params, 0)
}

var UpdateT_loan_manager_customerCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_manager_customer",
	ConfirmMethod: "ConfirmUpdateT_loan_manager_customer",
	CancelMethod:  "CancelUpdateT_loan_manager_customer",
}

func (impl *T_loan_manager_customerServiceImpl) TryUpdateT_loan_manager_customer(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_manager_customerById(params, 0)
	return
}

func (impl *T_loan_manager_customerServiceImpl) ConfirmUpdateT_loan_manager_customer(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_manager_customer(maps)
}

func (impl *T_loan_manager_customerServiceImpl) CancelUpdateT_loan_manager_customer(maps orm.Params) (err error) {
	return
}
