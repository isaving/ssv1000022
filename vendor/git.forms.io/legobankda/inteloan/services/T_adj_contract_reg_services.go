package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_adj_contract_regCompensable = client.Compensable{
	TryMethod:     "TryInsertT_adj_contract_reg",
	ConfirmMethod: "ConfirmInsertT_adj_contract_reg",
	CancelMethod:  "CancelInsertT_adj_contract_reg",
}

type T_adj_contract_regService interface {
	TryInsertT_adj_contract_reg(maps orm.Params) error
	ConfirmInsertT_adj_contract_reg(maps orm.Params) error
	CancelInsertT_adj_contract_reg(maps orm.Params) error
	TryUpdateT_adj_contract_reg(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_adj_contract_reg(maps orm.Params) error
	CancelUpdateT_adj_contract_reg(maps orm.Params) error
}

type T_adj_contract_regServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_adj_contract_regServiceImpl) TryInsertT_adj_contract_reg(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_adj_contract_reg(o, maps)
}

func (impl *T_adj_contract_regServiceImpl) ConfirmInsertT_adj_contract_reg(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_adj_contract_regTccState(o, maps)
}

func (impl *T_adj_contract_regServiceImpl) CancelInsertT_adj_contract_reg(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_adj_contract_regTccState(o, maps)
}

//智能查询
func (impl *T_adj_contract_regServiceImpl) QueryT_adj_contract_reg(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_adj_contract_reg(params, 0)
}

var UpdateT_adj_contract_regCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_adj_contract_reg",
	ConfirmMethod: "ConfirmUpdateT_adj_contract_reg",
	CancelMethod:  "CancelUpdateT_adj_contract_reg",
}

func (impl *T_adj_contract_regServiceImpl) TryUpdateT_adj_contract_reg(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_adj_contract_regById(params, 0)
	return
}

func (impl *T_adj_contract_regServiceImpl) ConfirmUpdateT_adj_contract_reg(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_adj_contract_reg(maps)
}

func (impl *T_adj_contract_regServiceImpl) CancelUpdateT_adj_contract_reg(maps orm.Params) (err error) {
	return
}
