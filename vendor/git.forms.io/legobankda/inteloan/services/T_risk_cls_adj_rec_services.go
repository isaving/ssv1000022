package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_risk_cls_adj_recCompensable = client.Compensable{
	TryMethod:     "TryInsertT_risk_cls_adj_rec",
	ConfirmMethod: "ConfirmInsertT_risk_cls_adj_rec",
	CancelMethod:  "CancelInsertT_risk_cls_adj_rec",
}

type T_risk_cls_adj_recService interface {
	TryInsertT_risk_cls_adj_rec(maps orm.Params) error
	ConfirmInsertT_risk_cls_adj_rec(maps orm.Params) error
	CancelInsertT_risk_cls_adj_rec(maps orm.Params) error
	TryUpdateT_risk_cls_adj_rec(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_risk_cls_adj_rec(maps orm.Params) error
	CancelUpdateT_risk_cls_adj_rec(maps orm.Params) error
}

type T_risk_cls_adj_recServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_risk_cls_adj_recServiceImpl) TryInsertT_risk_cls_adj_rec(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_risk_cls_adj_rec(o, maps)
}

func (impl *T_risk_cls_adj_recServiceImpl) ConfirmInsertT_risk_cls_adj_rec(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_risk_cls_adj_recTccState(o, maps)
}

func (impl *T_risk_cls_adj_recServiceImpl) CancelInsertT_risk_cls_adj_rec(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_risk_cls_adj_recTccState(o, maps)
}

//智能查询
func (impl *T_risk_cls_adj_recServiceImpl) QueryT_risk_cls_adj_rec(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_risk_cls_adj_rec(params, 0)
}

var UpdateT_risk_cls_adj_recCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_risk_cls_adj_rec",
	ConfirmMethod: "ConfirmUpdateT_risk_cls_adj_rec",
	CancelMethod:  "CancelUpdateT_risk_cls_adj_rec",
}

func (impl *T_risk_cls_adj_recServiceImpl) TryUpdateT_risk_cls_adj_rec(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_risk_cls_adj_recById(params, 0)
	return
}

func (impl *T_risk_cls_adj_recServiceImpl) ConfirmUpdateT_risk_cls_adj_rec(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_risk_cls_adj_rec(maps)
}

func (impl *T_risk_cls_adj_recServiceImpl) CancelUpdateT_risk_cls_adj_rec(maps orm.Params) (err error) {
	return
}
