package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_common_userCompensable = client.Compensable{
	TryMethod:     "TryInsertT_common_user",
	ConfirmMethod: "ConfirmInsertT_common_user",
	CancelMethod:  "CancelInsertT_common_user",
}

type T_common_userService interface {
	TryInsertT_common_user(maps orm.Params) error
	ConfirmInsertT_common_user(maps orm.Params) error
	CancelInsertT_common_user(maps orm.Params) error
	TryUpdateT_common_user(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_common_user(maps orm.Params) error
	CancelUpdateT_common_user(maps orm.Params) error
}

type T_common_userServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_common_userServiceImpl) TryInsertT_common_user(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_common_user(o, maps)
}

func (impl *T_common_userServiceImpl) ConfirmInsertT_common_user(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_common_userTccState(o, maps)
}

func (impl *T_common_userServiceImpl) CancelInsertT_common_user(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_common_userTccState(o, maps)
}

//智能查询
func (impl *T_common_userServiceImpl) QueryT_common_user(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_common_user(params, 0)
}

var UpdateT_common_userCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_common_user",
	ConfirmMethod: "ConfirmUpdateT_common_user",
	CancelMethod:  "CancelUpdateT_common_user",
}

func (impl *T_common_userServiceImpl) TryUpdateT_common_user(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_common_userById(params, 0)
	return
}

func (impl *T_common_userServiceImpl) ConfirmUpdateT_common_user(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_common_user(maps)
}

func (impl *T_common_userServiceImpl) CancelUpdateT_common_user(maps orm.Params) (err error) {
	return
}
