package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sys_biz_date_planCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sys_biz_date_plan",
	ConfirmMethod: "ConfirmInsertT_sys_biz_date_plan",
	CancelMethod:  "CancelInsertT_sys_biz_date_plan",
}

type T_sys_biz_date_planService interface {
	TryInsertT_sys_biz_date_plan(maps orm.Params) error
	ConfirmInsertT_sys_biz_date_plan(maps orm.Params) error
	CancelInsertT_sys_biz_date_plan(maps orm.Params) error
	TryUpdateT_sys_biz_date_plan(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sys_biz_date_plan(maps orm.Params) error
	CancelUpdateT_sys_biz_date_plan(maps orm.Params) error
}

type T_sys_biz_date_planServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sys_biz_date_planServiceImpl) TryInsertT_sys_biz_date_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sys_biz_date_plan(o, maps)
}

func (impl *T_sys_biz_date_planServiceImpl) ConfirmInsertT_sys_biz_date_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sys_biz_date_planTccState(o, maps)
}

func (impl *T_sys_biz_date_planServiceImpl) CancelInsertT_sys_biz_date_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sys_biz_date_planTccState(o, maps)
}

//智能查询
func (impl *T_sys_biz_date_planServiceImpl) QueryT_sys_biz_date_plan(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sys_biz_date_plan(params, 0)
}

var UpdateT_sys_biz_date_planCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sys_biz_date_plan",
	ConfirmMethod: "ConfirmUpdateT_sys_biz_date_plan",
	CancelMethod:  "CancelUpdateT_sys_biz_date_plan",
}

func (impl *T_sys_biz_date_planServiceImpl) TryUpdateT_sys_biz_date_plan(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sys_biz_date_planById(params, 0)
	return
}

func (impl *T_sys_biz_date_planServiceImpl) ConfirmUpdateT_sys_biz_date_plan(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sys_biz_date_plan(maps)
}

func (impl *T_sys_biz_date_planServiceImpl) CancelUpdateT_sys_biz_date_plan(maps orm.Params) (err error) {
	return
}
