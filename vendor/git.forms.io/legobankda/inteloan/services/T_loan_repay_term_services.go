package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_repay_termCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_repay_term",
	ConfirmMethod: "ConfirmInsertT_loan_repay_term",
	CancelMethod:  "CancelInsertT_loan_repay_term",
}

type T_loan_repay_termService interface {
	TryInsertT_loan_repay_term(maps orm.Params) error
	ConfirmInsertT_loan_repay_term(maps orm.Params) error
	CancelInsertT_loan_repay_term(maps orm.Params) error
	TryUpdateT_loan_repay_term(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_repay_term(maps orm.Params) error
	CancelUpdateT_loan_repay_term(maps orm.Params) error
}

type T_loan_repay_termServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_repay_termServiceImpl) TryInsertT_loan_repay_term(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_repay_term(o, maps)
}

func (impl *T_loan_repay_termServiceImpl) ConfirmInsertT_loan_repay_term(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_repay_termTccState(o, maps)
}

func (impl *T_loan_repay_termServiceImpl) CancelInsertT_loan_repay_term(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_repay_termTccState(o, maps)
}

//智能查询
func (impl *T_loan_repay_termServiceImpl) QueryT_loan_repay_term(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_repay_term(params, 0)
}

var UpdateT_loan_repay_termCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_repay_term",
	ConfirmMethod: "ConfirmUpdateT_loan_repay_term",
	CancelMethod:  "CancelUpdateT_loan_repay_term",
}

func (impl *T_loan_repay_termServiceImpl) TryUpdateT_loan_repay_term(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_repay_termById(params, 0)
	return
}

func (impl *T_loan_repay_termServiceImpl) ConfirmUpdateT_loan_repay_term(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_repay_term(maps)
}

func (impl *T_loan_repay_termServiceImpl) CancelUpdateT_loan_repay_term(maps orm.Params) (err error) {
	return
}
