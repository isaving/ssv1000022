package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertTp_option_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertTp_option_info",
	ConfirmMethod: "ConfirmInsertTp_option_info",
	CancelMethod:  "CancelInsertTp_option_info",
}

type Tp_option_infoService interface {
	TryInsertTp_option_info(maps orm.Params) error
	ConfirmInsertTp_option_info(maps orm.Params) error
	CancelInsertTp_option_info(maps orm.Params) error
	TryUpdateTp_option_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateTp_option_info(maps orm.Params) error
	CancelUpdateTp_option_info(maps orm.Params) error
}

type Tp_option_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Tp_option_infoServiceImpl) TryInsertTp_option_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertTp_option_info(o, maps)
}

func (impl *Tp_option_infoServiceImpl) ConfirmInsertTp_option_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateTp_option_infoTccState(o, maps)
}

func (impl *Tp_option_infoServiceImpl) CancelInsertTp_option_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteTp_option_infoTccState(o, maps)
}

//智能查询
func (impl *Tp_option_infoServiceImpl) QueryTp_option_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryTp_option_info(params, 0)
}

var UpdateTp_option_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateTp_option_info",
	ConfirmMethod: "ConfirmUpdateTp_option_info",
	CancelMethod:  "CancelUpdateTp_option_info",
}

func (impl *Tp_option_infoServiceImpl) TryUpdateTp_option_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTp_option_infoById(params, 0)
	return
}

func (impl *Tp_option_infoServiceImpl) ConfirmUpdateTp_option_info(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateTp_option_info(maps)
}

func (impl *Tp_option_infoServiceImpl) CancelUpdateTp_option_info(maps orm.Params) (err error) {
	return
}
