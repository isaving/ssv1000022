package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_process_modelCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_process_model",
	ConfirmMethod: "ConfirmInsertT_loan_process_model",
	CancelMethod:  "CancelInsertT_loan_process_model",
}

type T_loan_process_modelService interface {
	TryInsertT_loan_process_model(maps orm.Params) error
	ConfirmInsertT_loan_process_model(maps orm.Params) error
	CancelInsertT_loan_process_model(maps orm.Params) error
	TryUpdateT_loan_process_model(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_process_model(maps orm.Params) error
	CancelUpdateT_loan_process_model(maps orm.Params) error
}

type T_loan_process_modelServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_process_modelServiceImpl) TryInsertT_loan_process_model(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_process_model(o, maps)
}

func (impl *T_loan_process_modelServiceImpl) ConfirmInsertT_loan_process_model(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_process_modelTccState(o, maps)
}

func (impl *T_loan_process_modelServiceImpl) CancelInsertT_loan_process_model(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_process_modelTccState(o, maps)
}

//智能查询
func (impl *T_loan_process_modelServiceImpl) QueryT_loan_process_model(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_process_model(params, 0)
}

var UpdateT_loan_process_modelCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_process_model",
	ConfirmMethod: "ConfirmUpdateT_loan_process_model",
	CancelMethod:  "CancelUpdateT_loan_process_model",
}

func (impl *T_loan_process_modelServiceImpl) TryUpdateT_loan_process_model(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_process_modelById(params, 0)
	return
}

func (impl *T_loan_process_modelServiceImpl) ConfirmUpdateT_loan_process_model(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_process_model(maps)
}

func (impl *T_loan_process_modelServiceImpl) CancelUpdateT_loan_process_model(maps orm.Params) (err error) {
	return
}
