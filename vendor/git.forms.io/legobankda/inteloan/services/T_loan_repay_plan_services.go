package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_repay_planCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_repay_plan",
	ConfirmMethod: "ConfirmInsertT_loan_repay_plan",
	CancelMethod:  "CancelInsertT_loan_repay_plan",
}

type T_loan_repay_planService interface {
	TryInsertT_loan_repay_plan(maps orm.Params) error
	ConfirmInsertT_loan_repay_plan(maps orm.Params) error
	CancelInsertT_loan_repay_plan(maps orm.Params) error
	TryUpdateT_loan_repay_plan(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_repay_plan(maps orm.Params) error
	CancelUpdateT_loan_repay_plan(maps orm.Params) error
}

type T_loan_repay_planServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_repay_planServiceImpl) TryInsertT_loan_repay_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_repay_plan(o, maps)
}

func (impl *T_loan_repay_planServiceImpl) ConfirmInsertT_loan_repay_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_repay_planTccState(o, maps)
}

func (impl *T_loan_repay_planServiceImpl) CancelInsertT_loan_repay_plan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_repay_planTccState(o, maps)
}

//智能查询
func (impl *T_loan_repay_planServiceImpl) QueryT_loan_repay_plan(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_repay_plan(params, 0)
}

var UpdateT_loan_repay_planCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_repay_plan",
	ConfirmMethod: "ConfirmUpdateT_loan_repay_plan",
	CancelMethod:  "CancelUpdateT_loan_repay_plan",
}

func (impl *T_loan_repay_planServiceImpl) TryUpdateT_loan_repay_plan(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_repay_planById(params, 0)
	return
}

func (impl *T_loan_repay_planServiceImpl) ConfirmUpdateT_loan_repay_plan(params orm.Params) (err error) {
	params["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	params["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_repay_plan(params)
}

func (impl *T_loan_repay_planServiceImpl) CancelUpdateT_loan_repay_plan(maps orm.Params) (err error) {
	return
}
