package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertTs_sys_funcCompensable = client.Compensable{
	TryMethod:     "TryInsertTs_sys_func",
	ConfirmMethod: "ConfirmInsertTs_sys_func",
	CancelMethod:  "CancelInsertTs_sys_func",
}

type Ts_sys_funcService interface {
	TryInsertTs_sys_func(maps orm.Params) error
	ConfirmInsertTs_sys_func(maps orm.Params) error
	CancelInsertTs_sys_func(maps orm.Params) error
	TryUpdateTs_sys_func(maps orm.Params) (orm.Params, error)
	ConfirmUpdateTs_sys_func(maps orm.Params) error
	CancelUpdateTs_sys_func(maps orm.Params) error
}

type Ts_sys_funcServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Ts_sys_funcServiceImpl) TryInsertTs_sys_func(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertTs_sys_func(o, maps)
}

func (impl *Ts_sys_funcServiceImpl) ConfirmInsertTs_sys_func(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateTs_sys_funcTccState(o, maps)
}

func (impl *Ts_sys_funcServiceImpl) CancelInsertTs_sys_func(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteTs_sys_funcTccState(o, maps)
}

//智能查询
func (impl *Ts_sys_funcServiceImpl) QueryTs_sys_func(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryTs_sys_func(params, 0)
}

var UpdateTs_sys_funcCompensable = client.Compensable{
	TryMethod:     "TryUpdateTs_sys_func",
	ConfirmMethod: "ConfirmUpdateTs_sys_func",
	CancelMethod:  "CancelUpdateTs_sys_func",
}

func (impl *Ts_sys_funcServiceImpl) TryUpdateTs_sys_func(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTs_sys_funcById(params, 0)
	return
}

func (impl *Ts_sys_funcServiceImpl) ConfirmUpdateTs_sys_func(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateTs_sys_func(maps)
}

func (impl *Ts_sys_funcServiceImpl) CancelUpdateTs_sys_func(maps orm.Params) (err error) {
	return
}
