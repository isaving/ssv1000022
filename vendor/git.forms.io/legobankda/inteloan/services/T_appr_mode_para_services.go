package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_appr_mode_paraCompensable = client.Compensable{
	TryMethod:     "TryInsertT_appr_mode_para",
	ConfirmMethod: "ConfirmInsertT_appr_mode_para",
	CancelMethod:  "CancelInsertT_appr_mode_para",
}

type T_appr_mode_paraService interface {
	TryInsertT_appr_mode_para(maps orm.Params) error
	ConfirmInsertT_appr_mode_para(maps orm.Params) error
	CancelInsertT_appr_mode_para(maps orm.Params) error
	TryUpdateT_appr_mode_para(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_appr_mode_para(maps orm.Params) error
	CancelUpdateT_appr_mode_para(maps orm.Params) error
}

type T_appr_mode_paraServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_appr_mode_paraServiceImpl) TryInsertT_appr_mode_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_appr_mode_para(o, maps)
}

func (impl *T_appr_mode_paraServiceImpl) ConfirmInsertT_appr_mode_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_appr_mode_paraTccState(o, maps)
}

func (impl *T_appr_mode_paraServiceImpl) CancelInsertT_appr_mode_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_appr_mode_paraTccState(o, maps)
}

//智能查询
func (impl *T_appr_mode_paraServiceImpl) QueryT_appr_mode_para(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_appr_mode_para(params, 0)
}

var UpdateT_appr_mode_paraCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_appr_mode_para",
	ConfirmMethod: "ConfirmUpdateT_appr_mode_para",
	CancelMethod:  "CancelUpdateT_appr_mode_para",
}

func (impl *T_appr_mode_paraServiceImpl) TryUpdateT_appr_mode_para(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_appr_mode_paraById(params, 0)
	return
}

func (impl *T_appr_mode_paraServiceImpl) ConfirmUpdateT_appr_mode_para(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_appr_mode_para(maps)
}

func (impl *T_appr_mode_paraServiceImpl) CancelUpdateT_appr_mode_para(maps orm.Params) (err error) {
	return
}
