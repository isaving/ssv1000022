package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_syslistCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_syslist",
	ConfirmMethod: "ConfirmInsertT_ae_syslist",
	CancelMethod:  "CancelInsertT_ae_syslist",
}

type T_ae_syslistService interface {
	TryInsertT_ae_syslist(maps orm.Params) error
	ConfirmInsertT_ae_syslist(maps orm.Params) error
	CancelInsertT_ae_syslist(maps orm.Params) error
	TryUpdateT_ae_syslist(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_syslist(maps orm.Params) error
	CancelUpdateT_ae_syslist(maps orm.Params) error
}

type T_ae_syslistServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_syslistServiceImpl) TryInsertT_ae_syslist(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_syslist(o, maps)
}

func (impl *T_ae_syslistServiceImpl) ConfirmInsertT_ae_syslist(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_syslistTccState(o, maps)
}

func (impl *T_ae_syslistServiceImpl) CancelInsertT_ae_syslist(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_syslistTccState(o, maps)
}

//智能查询
func (impl *T_ae_syslistServiceImpl) QueryT_ae_syslist(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_syslist(params, 0)
}

var UpdateT_ae_syslistCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_syslist",
	ConfirmMethod: "ConfirmUpdateT_ae_syslist",
	CancelMethod:  "CancelUpdateT_ae_syslist",
}

func (impl *T_ae_syslistServiceImpl) TryUpdateT_ae_syslist(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_syslistById(params, 0)
	return
}

func (impl *T_ae_syslistServiceImpl) ConfirmUpdateT_ae_syslist(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_syslist(maps)
}

func (impl *T_ae_syslistServiceImpl) CancelUpdateT_ae_syslist(maps orm.Params) (err error) {
	return
}
