package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_impa_writeoff_appr_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_impa_writeoff_appr_flow",
	ConfirmMethod: "ConfirmInsertT_impa_writeoff_appr_flow",
	CancelMethod:  "CancelInsertT_impa_writeoff_appr_flow",
}

type T_impa_writeoff_appr_flowService interface {
	TryInsertT_impa_writeoff_appr_flow(maps orm.Params) error
	ConfirmInsertT_impa_writeoff_appr_flow(maps orm.Params) error
	CancelInsertT_impa_writeoff_appr_flow(maps orm.Params) error
	TryUpdateT_impa_writeoff_appr_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_impa_writeoff_appr_flow(maps orm.Params) error
	CancelUpdateT_impa_writeoff_appr_flow(maps orm.Params) error
}

type T_impa_writeoff_appr_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_impa_writeoff_appr_flowServiceImpl) TryInsertT_impa_writeoff_appr_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_impa_writeoff_appr_flow(o, maps)
}

func (impl *T_impa_writeoff_appr_flowServiceImpl) ConfirmInsertT_impa_writeoff_appr_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_impa_writeoff_appr_flowTccState(o, maps)
}

func (impl *T_impa_writeoff_appr_flowServiceImpl) CancelInsertT_impa_writeoff_appr_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_impa_writeoff_appr_flowTccState(o, maps)
}

//智能查询
func (impl *T_impa_writeoff_appr_flowServiceImpl) QueryT_impa_writeoff_appr_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_impa_writeoff_appr_flow(params, 0)
}

var UpdateT_impa_writeoff_appr_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_impa_writeoff_appr_flow",
	ConfirmMethod: "ConfirmUpdateT_impa_writeoff_appr_flow",
	CancelMethod:  "CancelUpdateT_impa_writeoff_appr_flow",
}

func (impl *T_impa_writeoff_appr_flowServiceImpl) TryUpdateT_impa_writeoff_appr_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_impa_writeoff_appr_flowById(params, 0)
	return
}

func (impl *T_impa_writeoff_appr_flowServiceImpl) ConfirmUpdateT_impa_writeoff_appr_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_impa_writeoff_appr_flow(maps)
}

func (impl *T_impa_writeoff_appr_flowServiceImpl) CancelUpdateT_impa_writeoff_appr_flow(maps orm.Params) (err error) {
	return
}
