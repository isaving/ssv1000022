package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_indv_comp_cons_crdtCompensable = client.Compensable{
	TryMethod:     "TryInsertT_indv_comp_cons_crdt",
	ConfirmMethod: "ConfirmInsertT_indv_comp_cons_crdt",
	CancelMethod:  "CancelInsertT_indv_comp_cons_crdt",
}

type T_indv_comp_cons_crdtService interface {
	TryInsertT_indv_comp_cons_crdt(maps orm.Params) error
	ConfirmInsertT_indv_comp_cons_crdt(maps orm.Params) error
	CancelInsertT_indv_comp_cons_crdt(maps orm.Params) error
	TryUpdateT_indv_comp_cons_crdt(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_indv_comp_cons_crdt(maps orm.Params) error
	CancelUpdateT_indv_comp_cons_crdt(maps orm.Params) error
}

type T_indv_comp_cons_crdtServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_indv_comp_cons_crdtServiceImpl) TryInsertT_indv_comp_cons_crdt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_indv_comp_cons_crdt(o, maps)
}

func (impl *T_indv_comp_cons_crdtServiceImpl) ConfirmInsertT_indv_comp_cons_crdt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_indv_comp_cons_crdtTccState(o, maps)
}

func (impl *T_indv_comp_cons_crdtServiceImpl) CancelInsertT_indv_comp_cons_crdt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_indv_comp_cons_crdtTccState(o, maps)
}

//智能查询
func (impl *T_indv_comp_cons_crdtServiceImpl) QueryT_indv_comp_cons_crdt(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_indv_comp_cons_crdt(params, 0)
}

var UpdateT_indv_comp_cons_crdtCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_indv_comp_cons_crdt",
	ConfirmMethod: "ConfirmUpdateT_indv_comp_cons_crdt",
	CancelMethod:  "CancelUpdateT_indv_comp_cons_crdt",
}

func (impl *T_indv_comp_cons_crdtServiceImpl) TryUpdateT_indv_comp_cons_crdt(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_indv_comp_cons_crdtById(params, 0)
	return
}

func (impl *T_indv_comp_cons_crdtServiceImpl) ConfirmUpdateT_indv_comp_cons_crdt(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_indv_comp_cons_crdt(maps)
}

func (impl *T_indv_comp_cons_crdtServiceImpl) CancelUpdateT_indv_comp_cons_crdt(maps orm.Params) (err error) {
	return
}
