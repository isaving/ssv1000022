package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_prodCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_prod",
	ConfirmMethod: "ConfirmInsertT_ae_prod",
	CancelMethod:  "CancelInsertT_ae_prod",
}

type T_ae_prodService interface {
	TryInsertT_ae_prod(maps orm.Params) error
	ConfirmInsertT_ae_prod(maps orm.Params) error
	CancelInsertT_ae_prod(maps orm.Params) error
	TryUpdateT_ae_prod(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_prod(maps orm.Params) error
	CancelUpdateT_ae_prod(maps orm.Params) error
}

type T_ae_prodServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_prodServiceImpl) TryInsertT_ae_prod(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_prod(o, maps)
}

func (impl *T_ae_prodServiceImpl) ConfirmInsertT_ae_prod(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_prodTccState(o, maps)
}

func (impl *T_ae_prodServiceImpl) CancelInsertT_ae_prod(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_prodTccState(o, maps)
}

//智能查询
func (impl *T_ae_prodServiceImpl) QueryT_ae_prod(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_prod(params, 0)
}

var UpdateT_ae_prodCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_prod",
	ConfirmMethod: "ConfirmUpdateT_ae_prod",
	CancelMethod:  "CancelUpdateT_ae_prod",
}

func (impl *T_ae_prodServiceImpl) TryUpdateT_ae_prod(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_prodById(params, 0)
	return
}

func (impl *T_ae_prodServiceImpl) ConfirmUpdateT_ae_prod(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_prod(maps)
}

func (impl *T_ae_prodServiceImpl) CancelUpdateT_ae_prod(maps orm.Params) (err error) {
	return
}
