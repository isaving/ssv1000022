package services

import (
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
)

var InsertT_intelligent_decisionCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intelligent_decision",
	ConfirmMethod: "ConfirmInsertT_intelligent_decision",
	CancelMethod:  "CancelInsertT_intelligent_decision",
}

type T_intelligent_decisionService interface {
	TryInsertT_intelligent_decision(maps orm.Params) error
	ConfirmInsertT_intelligent_decision(maps orm.Params) error
	CancelInsertT_intelligent_decision(maps orm.Params) error
	TryUpdateT_intelligent_decision(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intelligent_decision(maps orm.Params) error
	CancelUpdateT_intelligent_decision(maps orm.Params) error
}

type T_intelligent_decisionServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intelligent_decisionServiceImpl) TryInsertT_intelligent_decision(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intelligent_decision(o, maps)
}

func (impl *T_intelligent_decisionServiceImpl) ConfirmInsertT_intelligent_decision(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intelligent_decisionTccState(o, maps)
}

func (impl *T_intelligent_decisionServiceImpl) CancelInsertT_intelligent_decision(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intelligent_decisionTccState(o, maps)
}

//智能查询
func (impl *T_intelligent_decisionServiceImpl) QueryT_intelligent_decision(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intelligent_decision(params, 0)
}

var UpdateT_intelligent_decisionCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intelligent_decision",
	ConfirmMethod: "ConfirmUpdateT_intelligent_decision",
	CancelMethod:  "CancelUpdateT_intelligent_decision",
}

func (impl *T_intelligent_decisionServiceImpl) TryUpdateT_intelligent_decision(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intelligent_decisionById(params, 0)
	return
}

func (impl *T_intelligent_decisionServiceImpl) ConfirmUpdateT_intelligent_decision(maps orm.Params) (err error) {
	//maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	//maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intelligent_decision(maps)
}

func (impl *T_intelligent_decisionServiceImpl) CancelUpdateT_intelligent_decision(maps orm.Params) (err error) {
	return
}
