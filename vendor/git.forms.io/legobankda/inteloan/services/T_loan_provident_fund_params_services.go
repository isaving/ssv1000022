package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_provident_fund_paramsCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_provident_fund_params",
	ConfirmMethod: "ConfirmInsertT_loan_provident_fund_params",
	CancelMethod:  "CancelInsertT_loan_provident_fund_params",
}

type T_loan_provident_fund_paramsService interface {
	TryInsertT_loan_provident_fund_params(maps orm.Params) error
	ConfirmInsertT_loan_provident_fund_params(maps orm.Params) error
	CancelInsertT_loan_provident_fund_params(maps orm.Params) error
	TryUpdateT_loan_provident_fund_params(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_provident_fund_params(maps orm.Params) error
	CancelUpdateT_loan_provident_fund_params(maps orm.Params) error
}

type T_loan_provident_fund_paramsServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_provident_fund_paramsServiceImpl) TryInsertT_loan_provident_fund_params(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_provident_fund_params(o, maps)
}

func (impl *T_loan_provident_fund_paramsServiceImpl) ConfirmInsertT_loan_provident_fund_params(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_provident_fund_paramsTccState(o, maps)
}

func (impl *T_loan_provident_fund_paramsServiceImpl) CancelInsertT_loan_provident_fund_params(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_provident_fund_paramsTccState(o, maps)
}

//智能查询
func (impl *T_loan_provident_fund_paramsServiceImpl) QueryT_loan_provident_fund_params(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_provident_fund_params(params, 0)
}

var UpdateT_loan_provident_fund_paramsCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_provident_fund_params",
	ConfirmMethod: "ConfirmUpdateT_loan_provident_fund_params",
	CancelMethod:  "CancelUpdateT_loan_provident_fund_params",
}

func (impl *T_loan_provident_fund_paramsServiceImpl) TryUpdateT_loan_provident_fund_params(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_provident_fund_paramsById(params, 0)
	return
}

func (impl *T_loan_provident_fund_paramsServiceImpl) ConfirmUpdateT_loan_provident_fund_params(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_provident_fund_params(maps)
}

func (impl *T_loan_provident_fund_paramsServiceImpl) CancelUpdateT_loan_provident_fund_params(maps orm.Params) (err error) {
	return
}
