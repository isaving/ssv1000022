package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_daycutCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_daycut",
	ConfirmMethod: "ConfirmInsertT_ae_daycut",
	CancelMethod:  "CancelInsertT_ae_daycut",
}

type T_ae_daycutService interface {
	TryInsertT_ae_daycut(maps orm.Params) error
	ConfirmInsertT_ae_daycut(maps orm.Params) error
	CancelInsertT_ae_daycut(maps orm.Params) error
	TryUpdateT_ae_daycut(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_daycut(maps orm.Params) error
	CancelUpdateT_ae_daycut(maps orm.Params) error
}

type T_ae_daycutServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_daycutServiceImpl) TryInsertT_ae_daycut(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_daycut(o, maps)
}

func (impl *T_ae_daycutServiceImpl) ConfirmInsertT_ae_daycut(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_daycutTccState(o, maps)
}

func (impl *T_ae_daycutServiceImpl) CancelInsertT_ae_daycut(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_daycutTccState(o, maps)
}

//智能查询
func (impl *T_ae_daycutServiceImpl) QueryT_ae_daycut(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_daycut(params, 0)
}

var UpdateT_ae_daycutCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_daycut",
	ConfirmMethod: "ConfirmUpdateT_ae_daycut",
	CancelMethod:  "CancelUpdateT_ae_daycut",
}

func (impl *T_ae_daycutServiceImpl) TryUpdateT_ae_daycut(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_daycutById(params, 0)
	return
}

func (impl *T_ae_daycutServiceImpl) ConfirmUpdateT_ae_daycut(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_daycut(maps)
}

func (impl *T_ae_daycutServiceImpl) CancelUpdateT_ae_daycut(maps orm.Params) (err error) {
	return
}
