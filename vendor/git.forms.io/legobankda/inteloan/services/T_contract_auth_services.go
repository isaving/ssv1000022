package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_contract_authCompensable = client.Compensable{
	TryMethod:     "TryInsertT_contract_auth",
	ConfirmMethod: "ConfirmInsertT_contract_auth",
	CancelMethod:  "CancelInsertT_contract_auth",
}

type T_contract_authService interface {
	TryInsertT_contract_auth(maps orm.Params) error
	ConfirmInsertT_contract_auth(maps orm.Params) error
	CancelInsertT_contract_auth(maps orm.Params) error
	TryUpdateT_contract_auth(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_contract_auth(maps orm.Params) error
	CancelUpdateT_contract_auth(maps orm.Params) error
}

type T_contract_authServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_contract_authServiceImpl) TryInsertT_contract_auth(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_contract_auth(o, maps)
}

func (impl *T_contract_authServiceImpl) ConfirmInsertT_contract_auth(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_contract_authTccState(o, maps)
}

func (impl *T_contract_authServiceImpl) CancelInsertT_contract_auth(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_contract_authTccState(o, maps)
}

//智能查询
func (impl *T_contract_authServiceImpl) QueryT_contract_auth(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_contract_auth(params, 0)
}

var UpdateT_contract_authCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_contract_auth",
	ConfirmMethod: "ConfirmUpdateT_contract_auth",
	CancelMethod:  "CancelUpdateT_contract_auth",
}

func (impl *T_contract_authServiceImpl) TryUpdateT_contract_auth(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_contract_authById(params, 0)
	return
}

func (impl *T_contract_authServiceImpl) ConfirmUpdateT_contract_auth(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_contract_auth(maps)
}

func (impl *T_contract_authServiceImpl) CancelUpdateT_contract_auth(maps orm.Params) (err error) {
	return
}
