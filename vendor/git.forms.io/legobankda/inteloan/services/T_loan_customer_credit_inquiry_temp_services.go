package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_customer_credit_inquiry_tempCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_customer_credit_inquiry_temp",
	ConfirmMethod: "ConfirmInsertT_loan_customer_credit_inquiry_temp",
	CancelMethod:  "CancelInsertT_loan_customer_credit_inquiry_temp",
}

type T_loan_customer_credit_inquiry_tempService interface {
	TryInsertT_loan_customer_credit_inquiry_temp(maps orm.Params) error
	ConfirmInsertT_loan_customer_credit_inquiry_temp(maps orm.Params) error
	CancelInsertT_loan_customer_credit_inquiry_temp(maps orm.Params) error
	TryUpdateT_loan_customer_credit_inquiry_temp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_customer_credit_inquiry_temp(maps orm.Params) error
	CancelUpdateT_loan_customer_credit_inquiry_temp(maps orm.Params) error
}

type T_loan_customer_credit_inquiry_tempServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) TryInsertT_loan_customer_credit_inquiry_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_customer_credit_inquiry_temp(o, maps)
}

func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) ConfirmInsertT_loan_customer_credit_inquiry_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_customer_credit_inquiry_tempTccState(o, maps)
}

func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) CancelInsertT_loan_customer_credit_inquiry_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_customer_credit_inquiry_tempTccState(o, maps)
}

//智能查询
func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) QueryT_loan_customer_credit_inquiry_temp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_customer_credit_inquiry_temp(params, 0)
}

var UpdateT_loan_customer_credit_inquiry_tempCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_customer_credit_inquiry_temp",
	ConfirmMethod: "ConfirmUpdateT_loan_customer_credit_inquiry_temp",
	CancelMethod:  "CancelUpdateT_loan_customer_credit_inquiry_temp",
}

func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) TryUpdateT_loan_customer_credit_inquiry_temp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_customer_credit_inquiry_tempById(params, 0)
	return
}

func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) ConfirmUpdateT_loan_customer_credit_inquiry_temp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_customer_credit_inquiry_temp(maps)
}

func (impl *T_loan_customer_credit_inquiry_tempServiceImpl) CancelUpdateT_loan_customer_credit_inquiry_temp(maps orm.Params) (err error) {
	return
}
