package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_contactCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_contact",
	ConfirmMethod: "ConfirmInsertT_loan_contact",
	CancelMethod:  "CancelInsertT_loan_contact",
}

type T_loan_contactService interface {
	TryInsertT_loan_contact(maps orm.Params) error
	ConfirmInsertT_loan_contact(maps orm.Params) error
	CancelInsertT_loan_contact(maps orm.Params) error
	TryUpdateT_loan_contact(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_contact(maps orm.Params) error
	CancelUpdateT_loan_contact(maps orm.Params) error
}

type T_loan_contactServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_contactServiceImpl) TryInsertT_loan_contact(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_contact(o, maps)
}

func (impl *T_loan_contactServiceImpl) ConfirmInsertT_loan_contact(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_contactTccState(o, maps)
}

func (impl *T_loan_contactServiceImpl) CancelInsertT_loan_contact(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_contactTccState(o, maps)
}

//智能查询
func (impl *T_loan_contactServiceImpl) QueryT_loan_contact(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_contact(params, 0)
}

var UpdateT_loan_contactCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_contact",
	ConfirmMethod: "ConfirmUpdateT_loan_contact",
	CancelMethod:  "CancelUpdateT_loan_contact",
}

func (impl *T_loan_contactServiceImpl) TryUpdateT_loan_contact(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_contactById(params, 0)
	return
}

func (impl *T_loan_contactServiceImpl) ConfirmUpdateT_loan_contact(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_contact(maps)
}

func (impl *T_loan_contactServiceImpl) CancelUpdateT_loan_contact(maps orm.Params) (err error) {
	return
}
