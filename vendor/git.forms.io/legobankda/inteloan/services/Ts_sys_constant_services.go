package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertTs_sys_constantCompensable = client.Compensable{
	TryMethod:     "TryInsertTs_sys_constant",
	ConfirmMethod: "ConfirmInsertTs_sys_constant",
	CancelMethod:  "CancelInsertTs_sys_constant",
}

type Ts_sys_constantService interface {
	TryInsertTs_sys_constant(maps orm.Params) error
	ConfirmInsertTs_sys_constant(maps orm.Params) error
	CancelInsertTs_sys_constant(maps orm.Params) error
	TryUpdateTs_sys_constant(maps orm.Params) (orm.Params, error)
	ConfirmUpdateTs_sys_constant(maps orm.Params) error
	CancelUpdateTs_sys_constant(maps orm.Params) error
}

type Ts_sys_constantServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Ts_sys_constantServiceImpl) TryInsertTs_sys_constant(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertTs_sys_constant(o, maps)
}

func (impl *Ts_sys_constantServiceImpl) ConfirmInsertTs_sys_constant(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateTs_sys_constantTccState(o, maps)
}

func (impl *Ts_sys_constantServiceImpl) CancelInsertTs_sys_constant(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteTs_sys_constantTccState(o, maps)
}

//智能查询
func (impl *Ts_sys_constantServiceImpl) QueryTs_sys_constant(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryTs_sys_constant(params, 0)
}

var UpdateTs_sys_constantCompensable = client.Compensable{
	TryMethod:     "TryUpdateTs_sys_constant",
	ConfirmMethod: "ConfirmUpdateTs_sys_constant",
	CancelMethod:  "CancelUpdateTs_sys_constant",
}

func (impl *Ts_sys_constantServiceImpl) TryUpdateTs_sys_constant(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTs_sys_constantById(params, 0)
	return
}

func (impl *Ts_sys_constantServiceImpl) ConfirmUpdateTs_sys_constant(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateTs_sys_constant(maps)
}

func (impl *Ts_sys_constantServiceImpl) CancelUpdateTs_sys_constant(maps orm.Params) (err error) {
	return
}
