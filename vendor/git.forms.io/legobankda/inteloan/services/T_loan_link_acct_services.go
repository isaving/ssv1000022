package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_link_acctCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_link_acct",
	ConfirmMethod: "ConfirmInsertT_loan_link_acct",
	CancelMethod:  "CancelInsertT_loan_link_acct",
}

type T_loan_link_acctService interface {
	TryInsertT_loan_link_acct(maps orm.Params) error
	ConfirmInsertT_loan_link_acct(maps orm.Params) error
	CancelInsertT_loan_link_acct(maps orm.Params) error
	TryUpdateT_loan_link_acct(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_link_acct(maps orm.Params) error
	CancelUpdateT_loan_link_acct(maps orm.Params) error
}

type T_loan_link_acctServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_link_acctServiceImpl) TryInsertT_loan_link_acct(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_link_acct(o, maps)
}

func (impl *T_loan_link_acctServiceImpl) ConfirmInsertT_loan_link_acct(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_link_acctTccState(o, maps)
}

func (impl *T_loan_link_acctServiceImpl) CancelInsertT_loan_link_acct(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_link_acctTccState(o, maps)
}

//智能查询
func (impl *T_loan_link_acctServiceImpl) QueryT_loan_link_acct(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_link_acct(params, 0)
}

var UpdateT_loan_link_acctCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_link_acct",
	ConfirmMethod: "ConfirmUpdateT_loan_link_acct",
	CancelMethod:  "CancelUpdateT_loan_link_acct",
}

func (impl *T_loan_link_acctServiceImpl) TryUpdateT_loan_link_acct(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_link_acctById(params, 0)
	return
}

func (impl *T_loan_link_acctServiceImpl) ConfirmUpdateT_loan_link_acct(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_link_acct(maps)
}

func (impl *T_loan_link_acctServiceImpl) CancelUpdateT_loan_link_acct(maps orm.Params) (err error) {
	return
}
