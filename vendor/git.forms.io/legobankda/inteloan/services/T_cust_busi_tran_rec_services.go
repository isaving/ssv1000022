package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_cust_busi_tran_recCompensable = client.Compensable{
	TryMethod:     "TryInsertT_cust_busi_tran_rec",
	ConfirmMethod: "ConfirmInsertT_cust_busi_tran_rec",
	CancelMethod:  "CancelInsertT_cust_busi_tran_rec",
}

type T_cust_busi_tran_recService interface {
	TryInsertT_cust_busi_tran_rec(maps orm.Params) error
	ConfirmInsertT_cust_busi_tran_rec(maps orm.Params) error
	CancelInsertT_cust_busi_tran_rec(maps orm.Params) error
	TryUpdateT_cust_busi_tran_rec(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_cust_busi_tran_rec(maps orm.Params) error
	CancelUpdateT_cust_busi_tran_rec(maps orm.Params) error
}

type T_cust_busi_tran_recServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_cust_busi_tran_recServiceImpl) TryInsertT_cust_busi_tran_rec(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_cust_busi_tran_rec(o, maps)
}

func (impl *T_cust_busi_tran_recServiceImpl) ConfirmInsertT_cust_busi_tran_rec(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_cust_busi_tran_recTccState(o, maps)
}

func (impl *T_cust_busi_tran_recServiceImpl) CancelInsertT_cust_busi_tran_rec(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_cust_busi_tran_recTccState(o, maps)
}

//智能查询
func (impl *T_cust_busi_tran_recServiceImpl) QueryT_cust_busi_tran_rec(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_cust_busi_tran_rec(params, 0)
}

var UpdateT_cust_busi_tran_recCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_cust_busi_tran_rec",
	ConfirmMethod: "ConfirmUpdateT_cust_busi_tran_rec",
	CancelMethod:  "CancelUpdateT_cust_busi_tran_rec",
}

func (impl *T_cust_busi_tran_recServiceImpl) TryUpdateT_cust_busi_tran_rec(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_cust_busi_tran_recById(params, 0)
	return
}

func (impl *T_cust_busi_tran_recServiceImpl) ConfirmUpdateT_cust_busi_tran_rec(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_cust_busi_tran_rec(maps)
}

func (impl *T_cust_busi_tran_recServiceImpl) CancelUpdateT_cust_busi_tran_rec(maps orm.Params) (err error) {
	return
}
