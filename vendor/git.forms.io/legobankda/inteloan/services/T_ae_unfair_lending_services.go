package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_unfair_lendingCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_unfair_lending",
	ConfirmMethod: "ConfirmInsertT_ae_unfair_lending",
	CancelMethod:  "CancelInsertT_ae_unfair_lending",
}

type T_ae_unfair_lendingService interface {
	TryInsertT_ae_unfair_lending(maps orm.Params) error
	ConfirmInsertT_ae_unfair_lending(maps orm.Params) error
	CancelInsertT_ae_unfair_lending(maps orm.Params) error
	TryUpdateT_ae_unfair_lending(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_unfair_lending(maps orm.Params) error
	CancelUpdateT_ae_unfair_lending(maps orm.Params) error
}

type T_ae_unfair_lendingServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_unfair_lendingServiceImpl) TryInsertT_ae_unfair_lending(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_unfair_lending(o, maps)
}

func (impl *T_ae_unfair_lendingServiceImpl) ConfirmInsertT_ae_unfair_lending(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_unfair_lendingTccState(o, maps)
}

func (impl *T_ae_unfair_lendingServiceImpl) CancelInsertT_ae_unfair_lending(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_unfair_lendingTccState(o, maps)
}

//智能查询
func (impl *T_ae_unfair_lendingServiceImpl) QueryT_ae_unfair_lending(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_unfair_lending(params, 0)
}

var UpdateT_ae_unfair_lendingCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_unfair_lending",
	ConfirmMethod: "ConfirmUpdateT_ae_unfair_lending",
	CancelMethod:  "CancelUpdateT_ae_unfair_lending",
}

func (impl *T_ae_unfair_lendingServiceImpl) TryUpdateT_ae_unfair_lending(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_unfair_lendingById(params, 0)
	return
}

func (impl *T_ae_unfair_lendingServiceImpl) ConfirmUpdateT_ae_unfair_lending(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_unfair_lending(maps)
}

func (impl *T_ae_unfair_lendingServiceImpl) CancelUpdateT_ae_unfair_lending(maps orm.Params) (err error) {
	return
}
