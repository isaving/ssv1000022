package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_bal_chg_hisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_bal_chg_his",
	ConfirmMethod: "ConfirmInsertT_bal_chg_his",
	CancelMethod:  "CancelInsertT_bal_chg_his",
}

type T_bal_chg_hisService interface {
	TryInsertT_bal_chg_his(maps orm.Params) error
	ConfirmInsertT_bal_chg_his(maps orm.Params) error
	CancelInsertT_bal_chg_his(maps orm.Params) error
	TryUpdateT_bal_chg_his(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_bal_chg_his(maps orm.Params) error
	CancelUpdateT_bal_chg_his(maps orm.Params) error
}

type T_bal_chg_hisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_bal_chg_hisServiceImpl) TryInsertT_bal_chg_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_bal_chg_his(o, maps)
}

func (impl *T_bal_chg_hisServiceImpl) ConfirmInsertT_bal_chg_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_bal_chg_hisTccState(o, maps)
}

func (impl *T_bal_chg_hisServiceImpl) CancelInsertT_bal_chg_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_bal_chg_hisTccState(o, maps)
}

//智能查询
func (impl *T_bal_chg_hisServiceImpl) QueryT_bal_chg_his(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_bal_chg_his(params, 0)
}

var UpdateT_bal_chg_hisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_bal_chg_his",
	ConfirmMethod: "ConfirmUpdateT_bal_chg_his",
	CancelMethod:  "CancelUpdateT_bal_chg_his",
}

func (impl *T_bal_chg_hisServiceImpl) TryUpdateT_bal_chg_his(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_bal_chg_hisById(params, 0)
	return
}

func (impl *T_bal_chg_hisServiceImpl) ConfirmUpdateT_bal_chg_his(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_bal_chg_his(maps)
}

func (impl *T_bal_chg_hisServiceImpl) CancelUpdateT_bal_chg_his(maps orm.Params) (err error) {
	return
}
