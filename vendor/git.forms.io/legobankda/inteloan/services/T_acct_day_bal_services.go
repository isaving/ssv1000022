package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_acct_day_balCompensable = client.Compensable{
	TryMethod:     "TryInsertT_acct_day_bal",
	ConfirmMethod: "ConfirmInsertT_acct_day_bal",
	CancelMethod:  "CancelInsertT_acct_day_bal",
}

type T_acct_day_balService interface {
	TryInsertT_acct_day_bal(maps orm.Params) error
	ConfirmInsertT_acct_day_bal(maps orm.Params) error
	CancelInsertT_acct_day_bal(maps orm.Params) error
	TryUpdateT_acct_day_bal(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_acct_day_bal(maps orm.Params) error
	CancelUpdateT_acct_day_bal(maps orm.Params) error
}

type T_acct_day_balServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_acct_day_balServiceImpl) TryInsertT_acct_day_bal(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_acct_day_bal(o, maps)
}

func (impl *T_acct_day_balServiceImpl) ConfirmInsertT_acct_day_bal(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_acct_day_balTccState(o, maps)
}

func (impl *T_acct_day_balServiceImpl) CancelInsertT_acct_day_bal(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_acct_day_balTccState(o, maps)
}

//智能查询
func (impl *T_acct_day_balServiceImpl) QueryT_acct_day_bal(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_acct_day_bal(params, 0)
}

var UpdateT_acct_day_balCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_acct_day_bal",
	ConfirmMethod: "ConfirmUpdateT_acct_day_bal",
	CancelMethod:  "CancelUpdateT_acct_day_bal",
}

func (impl *T_acct_day_balServiceImpl) TryUpdateT_acct_day_bal(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_acct_day_balById(params, 0)
	return
}

func (impl *T_acct_day_balServiceImpl) ConfirmUpdateT_acct_day_bal(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_acct_day_bal(maps)
}

func (impl *T_acct_day_balServiceImpl) CancelUpdateT_acct_day_bal(maps orm.Params) (err error) {
	return
}
