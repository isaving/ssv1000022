package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_bpmn_retry_paramCompensable = client.Compensable{
	TryMethod:     "TryInsertT_bpmn_retry_param",
	ConfirmMethod: "ConfirmInsertT_bpmn_retry_param",
	CancelMethod:  "CancelInsertT_bpmn_retry_param",
}

type T_bpmn_retry_paramService interface {
	TryInsertT_bpmn_retry_param(maps orm.Params) error
	ConfirmInsertT_bpmn_retry_param(maps orm.Params) error
	CancelInsertT_bpmn_retry_param(maps orm.Params) error
	TryUpdateT_bpmn_retry_param(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_bpmn_retry_param(maps orm.Params) error
	CancelUpdateT_bpmn_retry_param(maps orm.Params) error
}

type T_bpmn_retry_paramServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_bpmn_retry_paramServiceImpl) TryInsertT_bpmn_retry_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_bpmn_retry_param(o, maps)
}

func (impl *T_bpmn_retry_paramServiceImpl) ConfirmInsertT_bpmn_retry_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_bpmn_retry_paramTccState(o, maps)
}

func (impl *T_bpmn_retry_paramServiceImpl) CancelInsertT_bpmn_retry_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_bpmn_retry_paramTccState(o, maps)
}

//智能查询
func (impl *T_bpmn_retry_paramServiceImpl) QueryT_bpmn_retry_param(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_bpmn_retry_param(params, 0)
}

var UpdateT_bpmn_retry_paramCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_bpmn_retry_param",
	ConfirmMethod: "ConfirmUpdateT_bpmn_retry_param",
	CancelMethod:  "CancelUpdateT_bpmn_retry_param",
}

func (impl *T_bpmn_retry_paramServiceImpl) TryUpdateT_bpmn_retry_param(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_bpmn_retry_paramById(params, 0)
	return
}

func (impl *T_bpmn_retry_paramServiceImpl) ConfirmUpdateT_bpmn_retry_param(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_bpmn_retry_param(maps)
}

func (impl *T_bpmn_retry_paramServiceImpl) CancelUpdateT_bpmn_retry_param(maps orm.Params) (err error) {
	return
}
