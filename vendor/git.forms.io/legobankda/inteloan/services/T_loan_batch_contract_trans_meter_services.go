package services

import (
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
)

var InsertT_loan_batch_contract_trans_meterCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_batch_contract_trans_meter",
	ConfirmMethod: "ConfirmInsertT_loan_batch_contract_trans_meter",
	CancelMethod:  "CancelInsertT_loan_batch_contract_trans_meter",
}

type T_loan_batch_contract_trans_meterService interface {
	TryInsertT_loan_batch_contract_trans_meter(maps orm.Params) error
	ConfirmInsertT_loan_batch_contract_trans_meter(maps orm.Params) error
	CancelInsertT_loan_batch_contract_trans_meter(maps orm.Params) error
	TryUpdateT_loan_batch_contract_trans_meter(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_batch_contract_trans_meter(maps orm.Params) error
	CancelUpdateT_loan_batch_contract_trans_meter(maps orm.Params) error
}

type T_loan_batch_contract_trans_meterServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_batch_contract_trans_meterServiceImpl) TryInsertT_loan_batch_contract_trans_meter(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_batch_contract_trans_meter(o, maps)
}

func (impl *T_loan_batch_contract_trans_meterServiceImpl) ConfirmInsertT_loan_batch_contract_trans_meter(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_batch_contract_trans_meterTccState(o, maps)
}

func (impl *T_loan_batch_contract_trans_meterServiceImpl) CancelInsertT_loan_batch_contract_trans_meter(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_batch_contract_trans_meterTccState(o, maps)
}

//智能查询
func (impl *T_loan_batch_contract_trans_meterServiceImpl) QueryT_loan_batch_contract_trans_meter(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_batch_contract_trans_meter(params, 0)
}

var UpdateT_loan_batch_contract_trans_meterCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_batch_contract_trans_meter",
	ConfirmMethod: "ConfirmUpdateT_loan_batch_contract_trans_meter",
	CancelMethod:  "CancelUpdateT_loan_batch_contract_trans_meter",
}

func (impl *T_loan_batch_contract_trans_meterServiceImpl) TryUpdateT_loan_batch_contract_trans_meter(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_batch_contract_trans_meterById(params, 0)
	return
}

func (impl *T_loan_batch_contract_trans_meterServiceImpl) ConfirmUpdateT_loan_batch_contract_trans_meter(maps orm.Params) (err error) {
	//maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	//maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_batch_contract_trans_meter(maps)
}

func (impl *T_loan_batch_contract_trans_meterServiceImpl) CancelUpdateT_loan_batch_contract_trans_meter(maps orm.Params) (err error) {
	return
}
