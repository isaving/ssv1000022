package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_batch_controlCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_batch_control",
	ConfirmMethod: "ConfirmInsertT_loan_batch_control",
	CancelMethod:  "CancelInsertT_loan_batch_control",
}

type T_loan_batch_controlService interface {
	TryInsertT_loan_batch_control(maps orm.Params) error
	ConfirmInsertT_loan_batch_control(maps orm.Params) error
	CancelInsertT_loan_batch_control(maps orm.Params) error
	TryUpdateT_loan_batch_control(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_batch_control(maps orm.Params) error
	CancelUpdateT_loan_batch_control(maps orm.Params) error
}

type T_loan_batch_controlServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_batch_controlServiceImpl) TryInsertT_loan_batch_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_batch_control(o, maps)
}

func (impl *T_loan_batch_controlServiceImpl) ConfirmInsertT_loan_batch_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_batch_controlTccState(o, maps)
}

func (impl *T_loan_batch_controlServiceImpl) CancelInsertT_loan_batch_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_batch_controlTccState(o, maps)
}

//智能查询
func (impl *T_loan_batch_controlServiceImpl) QueryT_loan_batch_control(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_batch_control(params, 0)
}

var UpdateT_loan_batch_controlCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_batch_control",
	ConfirmMethod: "ConfirmUpdateT_loan_batch_control",
	CancelMethod:  "CancelUpdateT_loan_batch_control",
}

func (impl *T_loan_batch_controlServiceImpl) TryUpdateT_loan_batch_control(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_batch_controlById(params, 0)
	return
}

func (impl *T_loan_batch_controlServiceImpl) ConfirmUpdateT_loan_batch_control(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_batch_control(maps)
}

func (impl *T_loan_batch_controlServiceImpl) CancelUpdateT_loan_batch_control(maps orm.Params) (err error) {
	return
}
