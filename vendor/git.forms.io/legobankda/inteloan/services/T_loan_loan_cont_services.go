package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_loan_contCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_loan_cont",
	ConfirmMethod: "ConfirmInsertT_loan_loan_cont",
	CancelMethod:  "CancelInsertT_loan_loan_cont",
}

type T_loan_loan_contService interface {
	TryInsertT_loan_loan_cont(maps orm.Params) error
	ConfirmInsertT_loan_loan_cont(maps orm.Params) error
	CancelInsertT_loan_loan_cont(maps orm.Params) error
	TryUpdateT_loan_loan_cont(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_loan_cont(maps orm.Params) error
	CancelUpdateT_loan_loan_cont(maps orm.Params) error
}

type T_loan_loan_contServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_loan_contServiceImpl) TryInsertT_loan_loan_cont(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_loan_cont(o, maps)
}

func (impl *T_loan_loan_contServiceImpl) ConfirmInsertT_loan_loan_cont(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_loan_contTccState(o, maps)
}

func (impl *T_loan_loan_contServiceImpl) CancelInsertT_loan_loan_cont(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_loan_contTccState(o, maps)
}

//智能查询
func (impl *T_loan_loan_contServiceImpl) QueryT_loan_loan_cont(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_loan_cont(params, 0)
}

var UpdateT_loan_loan_contCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_loan_cont",
	ConfirmMethod: "ConfirmUpdateT_loan_loan_cont",
	CancelMethod:  "CancelUpdateT_loan_loan_cont",
}

func (impl *T_loan_loan_contServiceImpl) TryUpdateT_loan_loan_cont(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_loan_contById(params, 0)
	return
}

func (impl *T_loan_loan_contServiceImpl) ConfirmUpdateT_loan_loan_cont(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_loan_cont(maps)
}

func (impl *T_loan_loan_contServiceImpl) CancelUpdateT_loan_loan_cont(maps orm.Params) (err error) {
	return
}
