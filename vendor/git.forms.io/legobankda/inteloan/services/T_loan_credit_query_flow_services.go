package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_credit_query_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_credit_query_flow",
	ConfirmMethod: "ConfirmInsertT_loan_credit_query_flow",
	CancelMethod:  "CancelInsertT_loan_credit_query_flow",
}

type T_loan_credit_query_flowService interface {
	TryInsertT_loan_credit_query_flow(maps orm.Params) error
	ConfirmInsertT_loan_credit_query_flow(maps orm.Params) error
	CancelInsertT_loan_credit_query_flow(maps orm.Params) error
	TryUpdateT_loan_credit_query_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_credit_query_flow(maps orm.Params) error
	CancelUpdateT_loan_credit_query_flow(maps orm.Params) error
}

type T_loan_credit_query_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_credit_query_flowServiceImpl) TryInsertT_loan_credit_query_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_credit_query_flow(o, maps)
}

func (impl *T_loan_credit_query_flowServiceImpl) ConfirmInsertT_loan_credit_query_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_credit_query_flowTccState(o, maps)
}

func (impl *T_loan_credit_query_flowServiceImpl) CancelInsertT_loan_credit_query_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_credit_query_flowTccState(o, maps)
}

//智能查询
func (impl *T_loan_credit_query_flowServiceImpl) QueryT_loan_credit_query_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_credit_query_flow(params, 0)
}

var UpdateT_loan_credit_query_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_credit_query_flow",
	ConfirmMethod: "ConfirmUpdateT_loan_credit_query_flow",
	CancelMethod:  "CancelUpdateT_loan_credit_query_flow",
}

func (impl *T_loan_credit_query_flowServiceImpl) TryUpdateT_loan_credit_query_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_credit_query_flowById(params, 0)
	return
}

func (impl *T_loan_credit_query_flowServiceImpl) ConfirmUpdateT_loan_credit_query_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_credit_query_flow(maps)
}

func (impl *T_loan_credit_query_flowServiceImpl) CancelUpdateT_loan_credit_query_flow(maps orm.Params) (err error) {
	return
}
