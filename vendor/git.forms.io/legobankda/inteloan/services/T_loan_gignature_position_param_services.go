package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_gignature_position_paramCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_gignature_position_param",
	ConfirmMethod: "ConfirmInsertT_loan_gignature_position_param",
	CancelMethod:  "CancelInsertT_loan_gignature_position_param",
}

type T_loan_gignature_position_paramService interface {
	TryInsertT_loan_gignature_position_param(maps orm.Params) error
	ConfirmInsertT_loan_gignature_position_param(maps orm.Params) error
	CancelInsertT_loan_gignature_position_param(maps orm.Params) error
	TryUpdateT_loan_gignature_position_param(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_gignature_position_param(maps orm.Params) error
	CancelUpdateT_loan_gignature_position_param(maps orm.Params) error
}

type T_loan_gignature_position_paramServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_gignature_position_paramServiceImpl) TryInsertT_loan_gignature_position_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_gignature_position_param(o, maps)
}

func (impl *T_loan_gignature_position_paramServiceImpl) ConfirmInsertT_loan_gignature_position_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_gignature_position_paramTccState(o, maps)
}

func (impl *T_loan_gignature_position_paramServiceImpl) CancelInsertT_loan_gignature_position_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_gignature_position_paramTccState(o, maps)
}

//智能查询
func (impl *T_loan_gignature_position_paramServiceImpl) QueryT_loan_gignature_position_param(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_gignature_position_param(params, 0)
}

var UpdateT_loan_gignature_position_paramCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_gignature_position_param",
	ConfirmMethod: "ConfirmUpdateT_loan_gignature_position_param",
	CancelMethod:  "CancelUpdateT_loan_gignature_position_param",
}

func (impl *T_loan_gignature_position_paramServiceImpl) TryUpdateT_loan_gignature_position_param(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_gignature_position_paramById(params, 0)
	return
}

func (impl *T_loan_gignature_position_paramServiceImpl) ConfirmUpdateT_loan_gignature_position_param(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_gignature_position_param(maps)
}

func (impl *T_loan_gignature_position_paramServiceImpl) CancelUpdateT_loan_gignature_position_param(maps orm.Params) (err error) {
	return
}
