package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_artif_interset_rate_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_artif_interset_rate_flow",
	ConfirmMethod: "ConfirmInsertT_artif_interset_rate_flow",
	CancelMethod:  "CancelInsertT_artif_interset_rate_flow",
}

type T_artif_interset_rate_flowService interface {
	TryInsertT_artif_interset_rate_flow(maps orm.Params) error
	ConfirmInsertT_artif_interset_rate_flow(maps orm.Params) error
	CancelInsertT_artif_interset_rate_flow(maps orm.Params) error
	TryUpdateT_artif_interset_rate_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_artif_interset_rate_flow(maps orm.Params) error
	CancelUpdateT_artif_interset_rate_flow(maps orm.Params) error
}

type T_artif_interset_rate_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_artif_interset_rate_flowServiceImpl) TryInsertT_artif_interset_rate_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_artif_interset_rate_flow(o, maps)
}

func (impl *T_artif_interset_rate_flowServiceImpl) ConfirmInsertT_artif_interset_rate_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_artif_interset_rate_flowTccState(o, maps)
}

func (impl *T_artif_interset_rate_flowServiceImpl) CancelInsertT_artif_interset_rate_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_artif_interset_rate_flowTccState(o, maps)
}

//智能查询
func (impl *T_artif_interset_rate_flowServiceImpl) QueryT_artif_interset_rate_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_artif_interset_rate_flow(params, 0)
}

var UpdateT_artif_interset_rate_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_artif_interset_rate_flow",
	ConfirmMethod: "ConfirmUpdateT_artif_interset_rate_flow",
	CancelMethod:  "CancelUpdateT_artif_interset_rate_flow",
}

func (impl *T_artif_interset_rate_flowServiceImpl) TryUpdateT_artif_interset_rate_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_artif_interset_rate_flowById(params, 0)
	return
}

func (impl *T_artif_interset_rate_flowServiceImpl) ConfirmUpdateT_artif_interset_rate_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_artif_interset_rate_flow(maps)
}

func (impl *T_artif_interset_rate_flowServiceImpl) CancelUpdateT_artif_interset_rate_flow(maps orm.Params) (err error) {
	return
}
