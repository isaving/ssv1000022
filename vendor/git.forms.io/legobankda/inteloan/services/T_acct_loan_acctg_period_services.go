package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_acct_loan_acctg_periodCompensable = client.Compensable{
	TryMethod:     "TryInsertT_acct_loan_acctg_period",
	ConfirmMethod: "ConfirmInsertT_acct_loan_acctg_period",
	CancelMethod:  "CancelInsertT_acct_loan_acctg_period",
}

type T_acct_loan_acctg_periodService interface {
	TryInsertT_acct_loan_acctg_period(maps orm.Params) error
	ConfirmInsertT_acct_loan_acctg_period(maps orm.Params) error
	CancelInsertT_acct_loan_acctg_period(maps orm.Params) error
	TryUpdateT_acct_loan_acctg_period(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_acct_loan_acctg_period(maps orm.Params) error
	CancelUpdateT_acct_loan_acctg_period(maps orm.Params) error
}

type T_acct_loan_acctg_periodServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_acct_loan_acctg_periodServiceImpl) TryInsertT_acct_loan_acctg_period(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_acct_loan_acctg_period(o, maps)
}

func (impl *T_acct_loan_acctg_periodServiceImpl) ConfirmInsertT_acct_loan_acctg_period(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_acct_loan_acctg_periodTccState(o, maps)
}

func (impl *T_acct_loan_acctg_periodServiceImpl) CancelInsertT_acct_loan_acctg_period(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_acct_loan_acctg_periodTccState(o, maps)
}

//智能查询
func (impl *T_acct_loan_acctg_periodServiceImpl) QueryT_acct_loan_acctg_period(params []map[string]interface{}) (info []orm.Params, err error) {
	return dao.QueryT_acct_loan_acctg_period(params, 0)
}

var UpdateT_acct_loan_acctg_periodCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_acct_loan_acctg_period",
	ConfirmMethod: "ConfirmUpdateT_acct_loan_acctg_period",
	CancelMethod:  "CancelUpdateT_acct_loan_acctg_period",
}

func (impl *T_acct_loan_acctg_periodServiceImpl) TryUpdateT_acct_loan_acctg_period(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_acct_loan_acctg_periodById(params, 0)
	return
}

func (impl *T_acct_loan_acctg_periodServiceImpl) ConfirmUpdateT_acct_loan_acctg_period(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_acct_loan_acctg_period(maps)
}

func (impl *T_acct_loan_acctg_periodServiceImpl) CancelUpdateT_acct_loan_acctg_period(maps orm.Params) (err error) {
	return
}
