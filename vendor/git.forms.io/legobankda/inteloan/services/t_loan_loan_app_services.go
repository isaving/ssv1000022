package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_loan_appCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_loan_app",
	ConfirmMethod: "ConfirmInsertT_loan_loan_app",
	CancelMethod:  "CancelInsertT_loan_loan_app",
}

type T_loan_loan_appService interface {
	TryInsertT_loan_loan_app(maps orm.Params) error
	ConfirmInsertT_loan_loan_app(maps orm.Params) error
	CancelInsertT_loan_loan_app(maps orm.Params) error
	TryUpdateT_loan_loan_app(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_loan_app(maps orm.Params) error
	CancelUpdateT_loan_loan_app(maps orm.Params) error
}

type T_loan_loan_appServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_loan_appServiceImpl) TryInsertT_loan_loan_app(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_loan_app(o, maps)
}

func (impl *T_loan_loan_appServiceImpl) ConfirmInsertT_loan_loan_app(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_loan_appTccState(o, maps)
}

func (impl *T_loan_loan_appServiceImpl) CancelInsertT_loan_loan_app(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_loan_appTccState(o, maps)
}

//智能查询
func (impl *T_loan_loan_appServiceImpl) QueryT_loan_loan_app(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_loan_app(params, 0)
}

var UpdateT_loan_loan_appCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_loan_app",
	ConfirmMethod: "ConfirmUpdateT_loan_loan_app",
	CancelMethod:  "CancelUpdateT_loan_loan_app",
}

func (impl *T_loan_loan_appServiceImpl) TryUpdateT_loan_loan_app(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_loan_appById(params, 0)
	return
}

func (impl *T_loan_loan_appServiceImpl) ConfirmUpdateT_loan_loan_app(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_loan_app(maps)
}

func (impl *T_loan_loan_appServiceImpl) CancelUpdateT_loan_loan_app(maps orm.Params) (err error) {
	return
}
