package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_bal_subj_unexpCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_bal_subj_unexp",
	ConfirmMethod: "ConfirmInsertT_ae_bal_subj_unexp",
	CancelMethod:  "CancelInsertT_ae_bal_subj_unexp",
}

type T_ae_bal_subj_unexpService interface {
	TryInsertT_ae_bal_subj_unexp(maps orm.Params) error
	ConfirmInsertT_ae_bal_subj_unexp(maps orm.Params) error
	CancelInsertT_ae_bal_subj_unexp(maps orm.Params) error
	TryUpdateT_ae_bal_subj_unexp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_bal_subj_unexp(maps orm.Params) error
	CancelUpdateT_ae_bal_subj_unexp(maps orm.Params) error
}

type T_ae_bal_subj_unexpServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_bal_subj_unexpServiceImpl) TryInsertT_ae_bal_subj_unexp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_bal_subj_unexp(o, maps)
}

func (impl *T_ae_bal_subj_unexpServiceImpl) ConfirmInsertT_ae_bal_subj_unexp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_bal_subj_unexpTccState(o, maps)
}

func (impl *T_ae_bal_subj_unexpServiceImpl) CancelInsertT_ae_bal_subj_unexp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_bal_subj_unexpTccState(o, maps)
}

//智能查询
func (impl *T_ae_bal_subj_unexpServiceImpl) QueryT_ae_bal_subj_unexp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_bal_subj_unexp(params, 0)
}

var UpdateT_ae_bal_subj_unexpCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_bal_subj_unexp",
	ConfirmMethod: "ConfirmUpdateT_ae_bal_subj_unexp",
	CancelMethod:  "CancelUpdateT_ae_bal_subj_unexp",
}

func (impl *T_ae_bal_subj_unexpServiceImpl) TryUpdateT_ae_bal_subj_unexp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_bal_subj_unexpById(params, 0)
	return
}

func (impl *T_ae_bal_subj_unexpServiceImpl) ConfirmUpdateT_ae_bal_subj_unexp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_bal_subj_unexp(maps)
}

func (impl *T_ae_bal_subj_unexpServiceImpl) CancelUpdateT_ae_bal_subj_unexp(maps orm.Params) (err error) {
	return
}
