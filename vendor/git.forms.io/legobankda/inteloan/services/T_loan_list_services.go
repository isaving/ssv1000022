package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_listCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_list",
	ConfirmMethod: "ConfirmInsertT_loan_list",
	CancelMethod:  "CancelInsertT_loan_list",
}

type T_loan_listService interface {
	TryInsertT_loan_list(maps orm.Params) error
	ConfirmInsertT_loan_list(maps orm.Params) error
	CancelInsertT_loan_list(maps orm.Params) error
	TryUpdateT_loan_list(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_list(maps orm.Params) error
	CancelUpdateT_loan_list(maps orm.Params) error
}

type T_loan_listServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_listServiceImpl) TryInsertT_loan_list(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_list(o, maps)
}

func (impl *T_loan_listServiceImpl) ConfirmInsertT_loan_list(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_listTccState(o, maps)
}

func (impl *T_loan_listServiceImpl) CancelInsertT_loan_list(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_listTccState(o, maps)
}

//智能查询
func (impl *T_loan_listServiceImpl) QueryT_loan_list(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_list(params, 0)
}

var UpdateT_loan_listCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_list",
	ConfirmMethod: "ConfirmUpdateT_loan_list",
	CancelMethod:  "CancelUpdateT_loan_list",
}

func (impl *T_loan_listServiceImpl) TryUpdateT_loan_list(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_listById(params, 0)
	return
}

func (impl *T_loan_listServiceImpl) ConfirmUpdateT_loan_list(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_list(maps)
}

func (impl *T_loan_listServiceImpl) CancelUpdateT_loan_list(maps orm.Params) (err error) {
	return
}
