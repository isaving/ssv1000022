package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intelligent_loan_approvalCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intelligent_loan_approval",
	ConfirmMethod: "ConfirmInsertT_intelligent_loan_approval",
	CancelMethod:  "CancelInsertT_intelligent_loan_approval",
}

type T_intelligent_loan_approvalService interface {
	TryInsertT_intelligent_loan_approval(maps orm.Params) error
	ConfirmInsertT_intelligent_loan_approval(maps orm.Params) error
	CancelInsertT_intelligent_loan_approval(maps orm.Params) error
	TryUpdateT_intelligent_loan_approval(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intelligent_loan_approval(maps orm.Params) error
	CancelUpdateT_intelligent_loan_approval(maps orm.Params) error
}

type T_intelligent_loan_approvalServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intelligent_loan_approvalServiceImpl) TryInsertT_intelligent_loan_approval(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intelligent_loan_approval(o, maps)
}

func (impl *T_intelligent_loan_approvalServiceImpl) ConfirmInsertT_intelligent_loan_approval(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intelligent_loan_approvalTccState(o, maps)
}

func (impl *T_intelligent_loan_approvalServiceImpl) CancelInsertT_intelligent_loan_approval(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intelligent_loan_approvalTccState(o, maps)
}

//智能查询
func (impl *T_intelligent_loan_approvalServiceImpl) QueryT_intelligent_loan_approval(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intelligent_loan_approval(params, 0)
}

var UpdateT_intelligent_loan_approvalCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intelligent_loan_approval",
	ConfirmMethod: "ConfirmUpdateT_intelligent_loan_approval",
	CancelMethod:  "CancelUpdateT_intelligent_loan_approval",
}

func (impl *T_intelligent_loan_approvalServiceImpl) TryUpdateT_intelligent_loan_approval(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intelligent_loan_approvalById(params, 0)
	return
}

func (impl *T_intelligent_loan_approvalServiceImpl) ConfirmUpdateT_intelligent_loan_approval(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intelligent_loan_approval(maps)
}

func (impl *T_intelligent_loan_approvalServiceImpl) CancelUpdateT_intelligent_loan_approval(maps orm.Params) (err error) {
	return
}
