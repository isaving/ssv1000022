package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_chan_lend_expend_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertT_chan_lend_expend_info",
	ConfirmMethod: "ConfirmInsertT_chan_lend_expend_info",
	CancelMethod:  "CancelInsertT_chan_lend_expend_info",
}

type T_chan_lend_expend_infoService interface {
	TryInsertT_chan_lend_expend_info(maps orm.Params) error
	ConfirmInsertT_chan_lend_expend_info(maps orm.Params) error
	CancelInsertT_chan_lend_expend_info(maps orm.Params) error
	TryUpdateT_chan_lend_expend_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_chan_lend_expend_info(maps orm.Params) error
	CancelUpdateT_chan_lend_expend_info(maps orm.Params) error
}

type T_chan_lend_expend_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_chan_lend_expend_infoServiceImpl) TryInsertT_chan_lend_expend_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_chan_lend_expend_info(o, maps)
}

func (impl *T_chan_lend_expend_infoServiceImpl) ConfirmInsertT_chan_lend_expend_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_chan_lend_expend_infoTccState(o, maps)
}

func (impl *T_chan_lend_expend_infoServiceImpl) CancelInsertT_chan_lend_expend_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_chan_lend_expend_infoTccState(o, maps)
}

//智能查询
func (impl *T_chan_lend_expend_infoServiceImpl) QueryT_chan_lend_expend_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_chan_lend_expend_info(params, 0)
}

var UpdateT_chan_lend_expend_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_chan_lend_expend_info",
	ConfirmMethod: "ConfirmUpdateT_chan_lend_expend_info",
	CancelMethod:  "CancelUpdateT_chan_lend_expend_info",
}

func (impl *T_chan_lend_expend_infoServiceImpl) TryUpdateT_chan_lend_expend_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_chan_lend_expend_infoById(params, 0)
	return
}

func (impl *T_chan_lend_expend_infoServiceImpl) ConfirmUpdateT_chan_lend_expend_info(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_chan_lend_expend_info(maps)
}

func (impl *T_chan_lend_expend_infoServiceImpl) CancelUpdateT_chan_lend_expend_info(maps orm.Params) (err error) {
	return
}
