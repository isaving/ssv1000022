package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_cont_equi_recrCompensable = client.Compensable{
	TryMethod:     "TryInsertT_cont_equi_recr",
	ConfirmMethod: "ConfirmInsertT_cont_equi_recr",
	CancelMethod:  "CancelInsertT_cont_equi_recr",
}

type T_cont_equi_recrService interface {
	TryInsertT_cont_equi_recr(maps orm.Params) error
	ConfirmInsertT_cont_equi_recr(maps orm.Params) error
	CancelInsertT_cont_equi_recr(maps orm.Params) error
	TryUpdateT_cont_equi_recr(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_cont_equi_recr(maps orm.Params) error
	CancelUpdateT_cont_equi_recr(maps orm.Params) error
}

type T_cont_equi_recrServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_cont_equi_recrServiceImpl) TryInsertT_cont_equi_recr(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_cont_equi_recr(o, maps)
}

func (impl *T_cont_equi_recrServiceImpl) ConfirmInsertT_cont_equi_recr(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_cont_equi_recrTccState(o, maps)
}

func (impl *T_cont_equi_recrServiceImpl) CancelInsertT_cont_equi_recr(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_cont_equi_recrTccState(o, maps)
}

//智能查询
func (impl *T_cont_equi_recrServiceImpl) QueryT_cont_equi_recr(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_cont_equi_recr(params, 0)
}

var UpdateT_cont_equi_recrCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_cont_equi_recr",
	ConfirmMethod: "ConfirmUpdateT_cont_equi_recr",
	CancelMethod:  "CancelUpdateT_cont_equi_recr",
}

func (impl *T_cont_equi_recrServiceImpl) TryUpdateT_cont_equi_recr(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_cont_equi_recrById(params, 0)
	return
}

func (impl *T_cont_equi_recrServiceImpl) ConfirmUpdateT_cont_equi_recr(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_cont_equi_recr(maps)
}

func (impl *T_cont_equi_recrServiceImpl) CancelUpdateT_cont_equi_recr(maps orm.Params) (err error) {
	return
}
