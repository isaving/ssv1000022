package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_consu_detCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_consu_det",
	ConfirmMethod: "ConfirmInsertT_loan_consu_det",
	CancelMethod:  "CancelInsertT_loan_consu_det",
}

type T_loan_consu_detService interface {
	TryInsertT_loan_consu_det(maps orm.Params) error
	ConfirmInsertT_loan_consu_det(maps orm.Params) error
	CancelInsertT_loan_consu_det(maps orm.Params) error
	TryUpdateT_loan_consu_det(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_consu_det(maps orm.Params) error
	CancelUpdateT_loan_consu_det(maps orm.Params) error
}

type T_loan_consu_detServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_consu_detServiceImpl) TryInsertT_loan_consu_det(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_consu_det(o, maps)
}

func (impl *T_loan_consu_detServiceImpl) ConfirmInsertT_loan_consu_det(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_consu_detTccState(o, maps)
}

func (impl *T_loan_consu_detServiceImpl) CancelInsertT_loan_consu_det(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_consu_detTccState(o, maps)
}

//智能查询
func (impl *T_loan_consu_detServiceImpl) QueryT_loan_consu_det(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_consu_det(params, 0)
}

var UpdateT_loan_consu_detCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_consu_det",
	ConfirmMethod: "ConfirmUpdateT_loan_consu_det",
	CancelMethod:  "CancelUpdateT_loan_consu_det",
}

func (impl *T_loan_consu_detServiceImpl) TryUpdateT_loan_consu_det(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_consu_detById(params, 0)
	return
}

func (impl *T_loan_consu_detServiceImpl) ConfirmUpdateT_loan_consu_det(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_consu_det(maps)
}

func (impl *T_loan_consu_detServiceImpl) CancelUpdateT_loan_consu_det(maps orm.Params) (err error) {
	return
}
