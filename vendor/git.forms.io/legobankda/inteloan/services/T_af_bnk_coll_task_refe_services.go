package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_af_bnk_coll_task_refeCompensable = client.Compensable{
	TryMethod:     "TryInsertT_af_bnk_coll_task_refe",
	ConfirmMethod: "ConfirmInsertT_af_bnk_coll_task_refe",
	CancelMethod:  "CancelInsertT_af_bnk_coll_task_refe",
}

type T_af_bnk_coll_task_refeService interface {
	TryInsertT_af_bnk_coll_task_refe(maps orm.Params) error
	ConfirmInsertT_af_bnk_coll_task_refe(maps orm.Params) error
	CancelInsertT_af_bnk_coll_task_refe(maps orm.Params) error
	TryUpdateT_af_bnk_coll_task_refe(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_af_bnk_coll_task_refe(maps orm.Params) error
	CancelUpdateT_af_bnk_coll_task_refe(maps orm.Params) error
}

type T_af_bnk_coll_task_refeServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_af_bnk_coll_task_refeServiceImpl) TryInsertT_af_bnk_coll_task_refe(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_af_bnk_coll_task_refe(o, maps)
}

func (impl *T_af_bnk_coll_task_refeServiceImpl) ConfirmInsertT_af_bnk_coll_task_refe(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_af_bnk_coll_task_refeTccState(o, maps)
}

func (impl *T_af_bnk_coll_task_refeServiceImpl) CancelInsertT_af_bnk_coll_task_refe(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_af_bnk_coll_task_refeTccState(o, maps)
}

//智能查询
func (impl *T_af_bnk_coll_task_refeServiceImpl) QueryT_af_bnk_coll_task_refe(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_af_bnk_coll_task_refe(params, 0)
}

var UpdateT_af_bnk_coll_task_refeCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_af_bnk_coll_task_refe",
	ConfirmMethod: "ConfirmUpdateT_af_bnk_coll_task_refe",
	CancelMethod:  "CancelUpdateT_af_bnk_coll_task_refe",
}

func (impl *T_af_bnk_coll_task_refeServiceImpl) TryUpdateT_af_bnk_coll_task_refe(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_af_bnk_coll_task_refeById(params, 0)
	return
}

func (impl *T_af_bnk_coll_task_refeServiceImpl) ConfirmUpdateT_af_bnk_coll_task_refe(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_af_bnk_coll_task_refe(maps)
}

func (impl *T_af_bnk_coll_task_refeServiceImpl) CancelUpdateT_af_bnk_coll_task_refe(maps orm.Params) (err error) {
	return
}
