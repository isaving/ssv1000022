package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_addr_match_orgCompensable = client.Compensable{
	TryMethod:     "TryInsertT_addr_match_org",
	ConfirmMethod: "ConfirmInsertT_addr_match_org",
	CancelMethod:  "CancelInsertT_addr_match_org",
}

type T_addr_match_orgService interface {
	TryInsertT_addr_match_org(maps orm.Params) error
	ConfirmInsertT_addr_match_org(maps orm.Params) error
	CancelInsertT_addr_match_org(maps orm.Params) error
	TryUpdateT_addr_match_org(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_addr_match_org(maps orm.Params) error
	CancelUpdateT_addr_match_org(maps orm.Params) error
}

type T_addr_match_orgServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_addr_match_orgServiceImpl) TryInsertT_addr_match_org(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_addr_match_org(o, maps)
}

func (impl *T_addr_match_orgServiceImpl) ConfirmInsertT_addr_match_org(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_addr_match_orgTccState(o, maps)
}

func (impl *T_addr_match_orgServiceImpl) CancelInsertT_addr_match_org(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_addr_match_orgTccState(o, maps)
}

//智能查询
func (impl *T_addr_match_orgServiceImpl) QueryT_addr_match_org(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_addr_match_org(params, 0)
}

var UpdateT_addr_match_orgCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_addr_match_org",
	ConfirmMethod: "ConfirmUpdateT_addr_match_org",
	CancelMethod:  "CancelUpdateT_addr_match_org",
}

func (impl *T_addr_match_orgServiceImpl) TryUpdateT_addr_match_org(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_addr_match_orgById(params, 0)
	return
}

func (impl *T_addr_match_orgServiceImpl) ConfirmUpdateT_addr_match_org(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_addr_match_org(maps)
}

func (impl *T_addr_match_orgServiceImpl) CancelUpdateT_addr_match_org(maps orm.Params) (err error) {
	return
}
