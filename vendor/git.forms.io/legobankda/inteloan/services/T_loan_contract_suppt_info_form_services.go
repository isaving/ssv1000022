package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_contract_suppt_info_formCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_contract_suppt_info_form",
	ConfirmMethod: "ConfirmInsertT_loan_contract_suppt_info_form",
	CancelMethod:  "CancelInsertT_loan_contract_suppt_info_form",
}

type T_loan_contract_suppt_info_formService interface {
	TryInsertT_loan_contract_suppt_info_form(maps orm.Params) error
	ConfirmInsertT_loan_contract_suppt_info_form(maps orm.Params) error
	CancelInsertT_loan_contract_suppt_info_form(maps orm.Params) error
	TryUpdateT_loan_contract_suppt_info_form(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_contract_suppt_info_form(maps orm.Params) error
	CancelUpdateT_loan_contract_suppt_info_form(maps orm.Params) error
}

type T_loan_contract_suppt_info_formServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_contract_suppt_info_formServiceImpl) TryInsertT_loan_contract_suppt_info_form(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_contract_suppt_info_form(o, maps)
}

func (impl *T_loan_contract_suppt_info_formServiceImpl) ConfirmInsertT_loan_contract_suppt_info_form(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_contract_suppt_info_formTccState(o, maps)
}

func (impl *T_loan_contract_suppt_info_formServiceImpl) CancelInsertT_loan_contract_suppt_info_form(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_contract_suppt_info_formTccState(o, maps)
}

//智能查询
func (impl *T_loan_contract_suppt_info_formServiceImpl) QueryT_loan_contract_suppt_info_form(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_contract_suppt_info_form(params, 0)
}

var UpdateT_loan_contract_suppt_info_formCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_contract_suppt_info_form",
	ConfirmMethod: "ConfirmUpdateT_loan_contract_suppt_info_form",
	CancelMethod:  "CancelUpdateT_loan_contract_suppt_info_form",
}

func (impl *T_loan_contract_suppt_info_formServiceImpl) TryUpdateT_loan_contract_suppt_info_form(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_contract_suppt_info_formById(params, 0)
	return
}

func (impl *T_loan_contract_suppt_info_formServiceImpl) ConfirmUpdateT_loan_contract_suppt_info_form(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_contract_suppt_info_form(maps)
}

func (impl *T_loan_contract_suppt_info_formServiceImpl) CancelUpdateT_loan_contract_suppt_info_form(maps orm.Params) (err error) {
	return
}
