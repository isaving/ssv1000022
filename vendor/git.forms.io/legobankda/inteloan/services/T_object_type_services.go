package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_object_typeCompensable = client.Compensable{
	TryMethod:     "TryInsertT_object_type",
	ConfirmMethod: "ConfirmInsertT_object_type",
	CancelMethod:  "CancelInsertT_object_type",
}

type T_object_typeService interface {
	TryInsertT_object_type(maps orm.Params) error
	ConfirmInsertT_object_type(maps orm.Params) error
	CancelInsertT_object_type(maps orm.Params) error
	TryUpdateT_object_type(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_object_type(maps orm.Params) error
	CancelUpdateT_object_type(maps orm.Params) error
}

type T_object_typeServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_object_typeServiceImpl) TryInsertT_object_type(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_object_type(o, maps)
}

func (impl *T_object_typeServiceImpl) ConfirmInsertT_object_type(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_object_typeTccState(o, maps)
}

func (impl *T_object_typeServiceImpl) CancelInsertT_object_type(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_object_typeTccState(o, maps)
}

//智能查询
func (impl *T_object_typeServiceImpl) QueryT_object_type(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_object_type(params, 0)
}

var UpdateT_object_typeCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_object_type",
	ConfirmMethod: "ConfirmUpdateT_object_type",
	CancelMethod:  "CancelUpdateT_object_type",
}

func (impl *T_object_typeServiceImpl) TryUpdateT_object_type(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_object_typeById(params, 0)
	return
}

func (impl *T_object_typeServiceImpl) ConfirmUpdateT_object_type(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_object_type(maps)
}

func (impl *T_object_typeServiceImpl) CancelUpdateT_object_type(maps orm.Params) (err error) {
	return
}
