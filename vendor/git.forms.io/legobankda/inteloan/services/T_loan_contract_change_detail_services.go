package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_contract_change_detailCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_contract_change_detail",
	ConfirmMethod: "ConfirmInsertT_loan_contract_change_detail",
	CancelMethod:  "CancelInsertT_loan_contract_change_detail",
}

type T_loan_contract_change_detailService interface {
	TryInsertT_loan_contract_change_detail(maps orm.Params) error
	ConfirmInsertT_loan_contract_change_detail(maps orm.Params) error
	CancelInsertT_loan_contract_change_detail(maps orm.Params) error
	TryUpdateT_loan_contract_change_detail(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_contract_change_detail(maps orm.Params) error
	CancelUpdateT_loan_contract_change_detail(maps orm.Params) error
}

type T_loan_contract_change_detailServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_contract_change_detailServiceImpl) TryInsertT_loan_contract_change_detail(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_contract_change_detail(o, maps)
}

func (impl *T_loan_contract_change_detailServiceImpl) ConfirmInsertT_loan_contract_change_detail(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_contract_change_detailTccState(o, maps)
}

func (impl *T_loan_contract_change_detailServiceImpl) CancelInsertT_loan_contract_change_detail(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_contract_change_detailTccState(o, maps)
}

//智能查询
func (impl *T_loan_contract_change_detailServiceImpl) QueryT_loan_contract_change_detail(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_contract_change_detail(params, 0)
}

var UpdateT_loan_contract_change_detailCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_contract_change_detail",
	ConfirmMethod: "ConfirmUpdateT_loan_contract_change_detail",
	CancelMethod:  "CancelUpdateT_loan_contract_change_detail",
}

func (impl *T_loan_contract_change_detailServiceImpl) TryUpdateT_loan_contract_change_detail(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_contract_change_detailById(params, 0)
	return
}

func (impl *T_loan_contract_change_detailServiceImpl) ConfirmUpdateT_loan_contract_change_detail(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_contract_change_detail(maps)
}

func (impl *T_loan_contract_change_detailServiceImpl) CancelUpdateT_loan_contract_change_detail(maps orm.Params) (err error) {
	return
}
