package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_offical_seal_parameterCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_offical_seal_parameter",
	ConfirmMethod: "ConfirmInsertT_loan_offical_seal_parameter",
	CancelMethod:  "CancelInsertT_loan_offical_seal_parameter",
}

type T_loan_offical_seal_parameterService interface {
	TryInsertT_loan_offical_seal_parameter(maps orm.Params) error
	ConfirmInsertT_loan_offical_seal_parameter(maps orm.Params) error
	CancelInsertT_loan_offical_seal_parameter(maps orm.Params) error
	TryUpdateT_loan_offical_seal_parameter(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_offical_seal_parameter(maps orm.Params) error
	CancelUpdateT_loan_offical_seal_parameter(maps orm.Params) error
}

type T_loan_offical_seal_parameterServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_offical_seal_parameterServiceImpl) TryInsertT_loan_offical_seal_parameter(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_offical_seal_parameter(o, maps)
}

func (impl *T_loan_offical_seal_parameterServiceImpl) ConfirmInsertT_loan_offical_seal_parameter(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_offical_seal_parameterTccState(o, maps)
}

func (impl *T_loan_offical_seal_parameterServiceImpl) CancelInsertT_loan_offical_seal_parameter(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_offical_seal_parameterTccState(o, maps)
}

//智能查询
func (impl *T_loan_offical_seal_parameterServiceImpl) QueryT_loan_offical_seal_parameter(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_offical_seal_parameter(params, 0)
}

var UpdateT_loan_offical_seal_parameterCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_offical_seal_parameter",
	ConfirmMethod: "ConfirmUpdateT_loan_offical_seal_parameter",
	CancelMethod:  "CancelUpdateT_loan_offical_seal_parameter",
}

func (impl *T_loan_offical_seal_parameterServiceImpl) TryUpdateT_loan_offical_seal_parameter(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_offical_seal_parameterById(params, 0)
	return
}

func (impl *T_loan_offical_seal_parameterServiceImpl) ConfirmUpdateT_loan_offical_seal_parameter(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_offical_seal_parameter(maps)
}

func (impl *T_loan_offical_seal_parameterServiceImpl) CancelUpdateT_loan_offical_seal_parameter(maps orm.Params) (err error) {
	return
}
