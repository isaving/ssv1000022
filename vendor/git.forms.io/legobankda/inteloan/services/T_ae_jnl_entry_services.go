package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_jnl_entryCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_jnl_entry",
	ConfirmMethod: "ConfirmInsertT_ae_jnl_entry",
	CancelMethod:  "CancelInsertT_ae_jnl_entry",
}

type T_ae_jnl_entryService interface {
	TryInsertT_ae_jnl_entry(maps orm.Params) error
	ConfirmInsertT_ae_jnl_entry(maps orm.Params) error
	CancelInsertT_ae_jnl_entry(maps orm.Params) error
	TryUpdateT_ae_jnl_entry(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_jnl_entry(maps orm.Params) error
	CancelUpdateT_ae_jnl_entry(maps orm.Params) error
}

type T_ae_jnl_entryServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_jnl_entryServiceImpl) TryInsertT_ae_jnl_entry(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_jnl_entry(o, maps)
}

func (impl *T_ae_jnl_entryServiceImpl) ConfirmInsertT_ae_jnl_entry(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_jnl_entryTccState(o, maps)
}

func (impl *T_ae_jnl_entryServiceImpl) CancelInsertT_ae_jnl_entry(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_jnl_entryTccState(o, maps)
}

//智能查询
func (impl *T_ae_jnl_entryServiceImpl) QueryT_ae_jnl_entry(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_jnl_entry(params, 0)
}

var UpdateT_ae_jnl_entryCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_jnl_entry",
	ConfirmMethod: "ConfirmUpdateT_ae_jnl_entry",
	CancelMethod:  "CancelUpdateT_ae_jnl_entry",
}

func (impl *T_ae_jnl_entryServiceImpl) TryUpdateT_ae_jnl_entry(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_jnl_entryById(params, 0)
	return
}

func (impl *T_ae_jnl_entryServiceImpl) ConfirmUpdateT_ae_jnl_entry(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_jnl_entry(maps)
}

func (impl *T_ae_jnl_entryServiceImpl) CancelUpdateT_ae_jnl_entry(maps orm.Params) (err error) {
	return
}
