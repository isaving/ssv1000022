package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sys_controlCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sys_control",
	ConfirmMethod: "ConfirmInsertT_sys_control",
	CancelMethod:  "CancelInsertT_sys_control",
}

type T_sys_controlService interface {
	TryInsertT_sys_control(maps orm.Params) error
	ConfirmInsertT_sys_control(maps orm.Params) error
	CancelInsertT_sys_control(maps orm.Params) error
	TryUpdateT_sys_control(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sys_control(maps orm.Params) error
	CancelUpdateT_sys_control(maps orm.Params) error
}

type T_sys_controlServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sys_controlServiceImpl) TryInsertT_sys_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sys_control(o, maps)
}

func (impl *T_sys_controlServiceImpl) ConfirmInsertT_sys_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sys_controlTccState(o, maps)
}

func (impl *T_sys_controlServiceImpl) CancelInsertT_sys_control(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sys_controlTccState(o, maps)
}

//智能查询
func (impl *T_sys_controlServiceImpl) QueryT_sys_control(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sys_control(params, 0)
}

var UpdateT_sys_controlCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sys_control",
	ConfirmMethod: "ConfirmUpdateT_sys_control",
	CancelMethod:  "CancelUpdateT_sys_control",
}

func (impl *T_sys_controlServiceImpl) TryUpdateT_sys_control(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sys_controlById(params, 0)
	return
}

func (impl *T_sys_controlServiceImpl) ConfirmUpdateT_sys_control(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sys_control(maps)
}

func (impl *T_sys_controlServiceImpl) CancelUpdateT_sys_control(maps orm.Params) (err error) {
	return
}
