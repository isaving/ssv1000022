package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_busi_org_mapping_paraCompensable = client.Compensable{
	TryMethod:     "TryInsertT_busi_org_mapping_para",
	ConfirmMethod: "ConfirmInsertT_busi_org_mapping_para",
	CancelMethod:  "CancelInsertT_busi_org_mapping_para",
}

type T_busi_org_mapping_paraService interface {
	TryInsertT_busi_org_mapping_para(maps orm.Params) error
	ConfirmInsertT_busi_org_mapping_para(maps orm.Params) error
	CancelInsertT_busi_org_mapping_para(maps orm.Params) error
	TryUpdateT_busi_org_mapping_para(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_busi_org_mapping_para(maps orm.Params) error
	CancelUpdateT_busi_org_mapping_para(maps orm.Params) error
}

type T_busi_org_mapping_paraServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_busi_org_mapping_paraServiceImpl) TryInsertT_busi_org_mapping_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_busi_org_mapping_para(o, maps)
}

func (impl *T_busi_org_mapping_paraServiceImpl) ConfirmInsertT_busi_org_mapping_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_busi_org_mapping_paraTccState(o, maps)
}

func (impl *T_busi_org_mapping_paraServiceImpl) CancelInsertT_busi_org_mapping_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_busi_org_mapping_paraTccState(o, maps)
}

//智能查询
func (impl *T_busi_org_mapping_paraServiceImpl) QueryT_busi_org_mapping_para(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_busi_org_mapping_para(params, 0)
}

var UpdateT_busi_org_mapping_paraCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_busi_org_mapping_para",
	ConfirmMethod: "ConfirmUpdateT_busi_org_mapping_para",
	CancelMethod:  "CancelUpdateT_busi_org_mapping_para",
}

func (impl *T_busi_org_mapping_paraServiceImpl) TryUpdateT_busi_org_mapping_para(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_busi_org_mapping_paraById(params, 0)
	return
}

func (impl *T_busi_org_mapping_paraServiceImpl) ConfirmUpdateT_busi_org_mapping_para(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_busi_org_mapping_para(maps)
}

func (impl *T_busi_org_mapping_paraServiceImpl) CancelUpdateT_busi_org_mapping_para(maps orm.Params) (err error) {
	return
}
