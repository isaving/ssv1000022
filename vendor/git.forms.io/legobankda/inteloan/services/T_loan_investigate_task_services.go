package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_investigate_taskCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_investigate_task",
	ConfirmMethod: "ConfirmInsertT_loan_investigate_task",
	CancelMethod:  "CancelInsertT_loan_investigate_task",
}

type T_loan_investigate_taskService interface {
	TryInsertT_loan_investigate_task(maps orm.Params) error
	ConfirmInsertT_loan_investigate_task(maps orm.Params) error
	CancelInsertT_loan_investigate_task(maps orm.Params) error
	TryUpdateT_loan_investigate_task(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_investigate_task(maps orm.Params) error
	CancelUpdateT_loan_investigate_task(maps orm.Params) error
}

type T_loan_investigate_taskServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_investigate_taskServiceImpl) TryInsertT_loan_investigate_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_investigate_task(o, maps)
}

func (impl *T_loan_investigate_taskServiceImpl) ConfirmInsertT_loan_investigate_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_investigate_taskTccState(o, maps)
}

func (impl *T_loan_investigate_taskServiceImpl) CancelInsertT_loan_investigate_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_investigate_taskTccState(o, maps)
}

//智能查询
func (impl *T_loan_investigate_taskServiceImpl) QueryT_loan_investigate_task(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_investigate_task(params, 0)
}

var UpdateT_loan_investigate_taskCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_investigate_task",
	ConfirmMethod: "ConfirmUpdateT_loan_investigate_task",
	CancelMethod:  "CancelUpdateT_loan_investigate_task",
}

func (impl *T_loan_investigate_taskServiceImpl) TryUpdateT_loan_investigate_task(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_investigate_taskById(params, 0)
	return
}

func (impl *T_loan_investigate_taskServiceImpl) ConfirmUpdateT_loan_investigate_task(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_investigate_task(maps)
}

func (impl *T_loan_investigate_taskServiceImpl) CancelUpdateT_loan_investigate_task(maps orm.Params) (err error) {
	return
}
