package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_itlg_wind_appr_resuCompensable = client.Compensable{
	TryMethod:     "TryInsertT_itlg_wind_appr_resu",
	ConfirmMethod: "ConfirmInsertT_itlg_wind_appr_resu",
	CancelMethod:  "CancelInsertT_itlg_wind_appr_resu",
}

type T_itlg_wind_appr_resuService interface {
	TryInsertT_itlg_wind_appr_resu(maps orm.Params) error
	ConfirmInsertT_itlg_wind_appr_resu(maps orm.Params) error
	CancelInsertT_itlg_wind_appr_resu(maps orm.Params) error
	TryUpdateT_itlg_wind_appr_resu(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_itlg_wind_appr_resu(maps orm.Params) error
	CancelUpdateT_itlg_wind_appr_resu(maps orm.Params) error
}

type T_itlg_wind_appr_resuServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_itlg_wind_appr_resuServiceImpl) TryInsertT_itlg_wind_appr_resu(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_itlg_wind_appr_resu(o, maps)
}

func (impl *T_itlg_wind_appr_resuServiceImpl) ConfirmInsertT_itlg_wind_appr_resu(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_itlg_wind_appr_resuTccState(o, maps)
}

func (impl *T_itlg_wind_appr_resuServiceImpl) CancelInsertT_itlg_wind_appr_resu(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_itlg_wind_appr_resuTccState(o, maps)
}

//智能查询
func (impl *T_itlg_wind_appr_resuServiceImpl) QueryT_itlg_wind_appr_resu(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_itlg_wind_appr_resu(params, 0)
}

var UpdateT_itlg_wind_appr_resuCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_itlg_wind_appr_resu",
	ConfirmMethod: "ConfirmUpdateT_itlg_wind_appr_resu",
	CancelMethod:  "CancelUpdateT_itlg_wind_appr_resu",
}

func (impl *T_itlg_wind_appr_resuServiceImpl) TryUpdateT_itlg_wind_appr_resu(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_itlg_wind_appr_resuById(params, 0)
	return
}

func (impl *T_itlg_wind_appr_resuServiceImpl) ConfirmUpdateT_itlg_wind_appr_resu(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_itlg_wind_appr_resu(maps)
}

func (impl *T_itlg_wind_appr_resuServiceImpl) CancelUpdateT_itlg_wind_appr_resu(maps orm.Params) (err error) {
	return
}
