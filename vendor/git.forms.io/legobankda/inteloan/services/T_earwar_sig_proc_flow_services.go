package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_earwar_sig_proc_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_earwar_sig_proc_flow",
	ConfirmMethod: "ConfirmInsertT_earwar_sig_proc_flow",
	CancelMethod:  "CancelInsertT_earwar_sig_proc_flow",
}

type T_earwar_sig_proc_flowService interface {
	TryInsertT_earwar_sig_proc_flow(maps orm.Params) error
	ConfirmInsertT_earwar_sig_proc_flow(maps orm.Params) error
	CancelInsertT_earwar_sig_proc_flow(maps orm.Params) error
	TryUpdateT_earwar_sig_proc_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_earwar_sig_proc_flow(maps orm.Params) error
	CancelUpdateT_earwar_sig_proc_flow(maps orm.Params) error
}

type T_earwar_sig_proc_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_earwar_sig_proc_flowServiceImpl) TryInsertT_earwar_sig_proc_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_earwar_sig_proc_flow(o, maps)
}

func (impl *T_earwar_sig_proc_flowServiceImpl) ConfirmInsertT_earwar_sig_proc_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_earwar_sig_proc_flowTccState(o, maps)
}

func (impl *T_earwar_sig_proc_flowServiceImpl) CancelInsertT_earwar_sig_proc_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_earwar_sig_proc_flowTccState(o, maps)
}

//智能查询
func (impl *T_earwar_sig_proc_flowServiceImpl) QueryT_earwar_sig_proc_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_earwar_sig_proc_flow(params, 0)
}

var UpdateT_earwar_sig_proc_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_earwar_sig_proc_flow",
	ConfirmMethod: "ConfirmUpdateT_earwar_sig_proc_flow",
	CancelMethod:  "CancelUpdateT_earwar_sig_proc_flow",
}

func (impl *T_earwar_sig_proc_flowServiceImpl) TryUpdateT_earwar_sig_proc_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_earwar_sig_proc_flowById(params, 0)
	return
}

func (impl *T_earwar_sig_proc_flowServiceImpl) ConfirmUpdateT_earwar_sig_proc_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_earwar_sig_proc_flow(maps)
}

func (impl *T_earwar_sig_proc_flowServiceImpl) CancelUpdateT_earwar_sig_proc_flow(maps orm.Params) (err error) {
	return
}
