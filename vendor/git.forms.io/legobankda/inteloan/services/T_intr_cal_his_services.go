package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intr_cal_hisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intr_cal_his",
	ConfirmMethod: "ConfirmInsertT_intr_cal_his",
	CancelMethod:  "CancelInsertT_intr_cal_his",
}

type T_intr_cal_hisService interface {
	TryInsertT_intr_cal_his(maps orm.Params) error
	ConfirmInsertT_intr_cal_his(maps orm.Params) error
	CancelInsertT_intr_cal_his(maps orm.Params) error
	TryUpdateT_intr_cal_his(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intr_cal_his(maps orm.Params) error
	CancelUpdateT_intr_cal_his(maps orm.Params) error
}

type T_intr_cal_hisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intr_cal_hisServiceImpl) TryInsertT_intr_cal_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intr_cal_his(o, maps)
}

func (impl *T_intr_cal_hisServiceImpl) ConfirmInsertT_intr_cal_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intr_cal_hisTccState(o, maps)
}

func (impl *T_intr_cal_hisServiceImpl) CancelInsertT_intr_cal_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intr_cal_hisTccState(o, maps)
}

//智能查询
func (impl *T_intr_cal_hisServiceImpl) QueryT_intr_cal_his(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intr_cal_his(params, 0)
}

var UpdateT_intr_cal_hisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intr_cal_his",
	ConfirmMethod: "ConfirmUpdateT_intr_cal_his",
	CancelMethod:  "CancelUpdateT_intr_cal_his",
}

func (impl *T_intr_cal_hisServiceImpl) TryUpdateT_intr_cal_his(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intr_cal_hisById(params, 0)
	return
}

func (impl *T_intr_cal_hisServiceImpl) ConfirmUpdateT_intr_cal_his(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intr_cal_his(maps)
}

func (impl *T_intr_cal_hisServiceImpl) CancelUpdateT_intr_cal_his(maps orm.Params) (err error) {
	return
}
