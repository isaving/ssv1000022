package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intrt_chg_hisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intrt_chg_his",
	ConfirmMethod: "ConfirmInsertT_intrt_chg_his",
	CancelMethod:  "CancelInsertT_intrt_chg_his",
}

type T_intrt_chg_hisService interface {
	TryInsertT_intrt_chg_his(maps orm.Params) error
	ConfirmInsertT_intrt_chg_his(maps orm.Params) error
	CancelInsertT_intrt_chg_his(maps orm.Params) error
	TryUpdateT_intrt_chg_his(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intrt_chg_his(maps orm.Params) error
	CancelUpdateT_intrt_chg_his(maps orm.Params) error
}

type T_intrt_chg_hisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intrt_chg_hisServiceImpl) TryInsertT_intrt_chg_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intrt_chg_his(o, maps)
}

func (impl *T_intrt_chg_hisServiceImpl) ConfirmInsertT_intrt_chg_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intrt_chg_hisTccState(o, maps)
}

func (impl *T_intrt_chg_hisServiceImpl) CancelInsertT_intrt_chg_his(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intrt_chg_hisTccState(o, maps)
}

//智能查询
func (impl *T_intrt_chg_hisServiceImpl) QueryT_intrt_chg_his(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intrt_chg_his(params, 0)
}

var UpdateT_intrt_chg_hisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intrt_chg_his",
	ConfirmMethod: "ConfirmUpdateT_intrt_chg_his",
	CancelMethod:  "CancelUpdateT_intrt_chg_his",
}

func (impl *T_intrt_chg_hisServiceImpl) TryUpdateT_intrt_chg_his(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intrt_chg_hisById(params, 0)
	return
}

func (impl *T_intrt_chg_hisServiceImpl) ConfirmUpdateT_intrt_chg_his(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intrt_chg_his(maps)
}

func (impl *T_intrt_chg_hisServiceImpl) CancelUpdateT_intrt_chg_his(maps orm.Params) (err error) {
	return
}
