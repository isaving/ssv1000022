package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_impa_regisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_impa_regis",
	ConfirmMethod: "ConfirmInsertT_impa_regis",
	CancelMethod:  "CancelInsertT_impa_regis",
}

type T_impa_regisService interface {
	TryInsertT_impa_regis(maps orm.Params) error
	ConfirmInsertT_impa_regis(maps orm.Params) error
	CancelInsertT_impa_regis(maps orm.Params) error
	TryUpdateT_impa_regis(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_impa_regis(maps orm.Params) error
	CancelUpdateT_impa_regis(maps orm.Params) error
}

type T_impa_regisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_impa_regisServiceImpl) TryInsertT_impa_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_impa_regis(o, maps)
}

func (impl *T_impa_regisServiceImpl) ConfirmInsertT_impa_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_impa_regisTccState(o, maps)
}

func (impl *T_impa_regisServiceImpl) CancelInsertT_impa_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_impa_regisTccState(o, maps)
}

//智能查询
func (impl *T_impa_regisServiceImpl) QueryT_impa_regis(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_impa_regis(params, 0)
}

var UpdateT_impa_regisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_impa_regis",
	ConfirmMethod: "ConfirmUpdateT_impa_regis",
	CancelMethod:  "CancelUpdateT_impa_regis",
}

func (impl *T_impa_regisServiceImpl) TryUpdateT_impa_regis(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_impa_regisById(params, 0)
	return
}

func (impl *T_impa_regisServiceImpl) ConfirmUpdateT_impa_regis(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_impa_regis(maps)
}

func (impl *T_impa_regisServiceImpl) CancelUpdateT_impa_regis(maps orm.Params) (err error) {
	return
}
