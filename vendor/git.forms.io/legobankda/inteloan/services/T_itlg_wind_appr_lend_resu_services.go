package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_itlg_wind_appr_lend_resuCompensable = client.Compensable{
	TryMethod:     "TryInsertT_itlg_wind_appr_lend_resu",
	ConfirmMethod: "ConfirmInsertT_itlg_wind_appr_lend_resu",
	CancelMethod:  "CancelInsertT_itlg_wind_appr_lend_resu",
}

type T_itlg_wind_appr_lend_resuService interface {
	TryInsertT_itlg_wind_appr_lend_resu(maps orm.Params) error
	ConfirmInsertT_itlg_wind_appr_lend_resu(maps orm.Params) error
	CancelInsertT_itlg_wind_appr_lend_resu(maps orm.Params) error
	TryUpdateT_itlg_wind_appr_lend_resu(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_itlg_wind_appr_lend_resu(maps orm.Params) error
	CancelUpdateT_itlg_wind_appr_lend_resu(maps orm.Params) error
}

type T_itlg_wind_appr_lend_resuServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_itlg_wind_appr_lend_resuServiceImpl) TryInsertT_itlg_wind_appr_lend_resu(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_itlg_wind_appr_lend_resu(o, maps)
}

func (impl *T_itlg_wind_appr_lend_resuServiceImpl) ConfirmInsertT_itlg_wind_appr_lend_resu(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_itlg_wind_appr_lend_resuTccState(o, maps)
}

func (impl *T_itlg_wind_appr_lend_resuServiceImpl) CancelInsertT_itlg_wind_appr_lend_resu(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_itlg_wind_appr_lend_resuTccState(o, maps)
}

//智能查询
func (impl *T_itlg_wind_appr_lend_resuServiceImpl) QueryT_itlg_wind_appr_lend_resu(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_itlg_wind_appr_lend_resu(params, 0)
}

var UpdateT_itlg_wind_appr_lend_resuCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_itlg_wind_appr_lend_resu",
	ConfirmMethod: "ConfirmUpdateT_itlg_wind_appr_lend_resu",
	CancelMethod:  "CancelUpdateT_itlg_wind_appr_lend_resu",
}

func (impl *T_itlg_wind_appr_lend_resuServiceImpl) TryUpdateT_itlg_wind_appr_lend_resu(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_itlg_wind_appr_lend_resuById(params, 0)
	return
}

func (impl *T_itlg_wind_appr_lend_resuServiceImpl) ConfirmUpdateT_itlg_wind_appr_lend_resu(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_itlg_wind_appr_lend_resu(maps)
}

func (impl *T_itlg_wind_appr_lend_resuServiceImpl) CancelUpdateT_itlg_wind_appr_lend_resu(maps orm.Params) (err error) {
	return
}
