package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intrt_chg_regisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intrt_chg_regis",
	ConfirmMethod: "ConfirmInsertT_intrt_chg_regis",
	CancelMethod:  "CancelInsertT_intrt_chg_regis",
}

type T_intrt_chg_regisService interface {
	TryInsertT_intrt_chg_regis(maps orm.Params) error
	ConfirmInsertT_intrt_chg_regis(maps orm.Params) error
	CancelInsertT_intrt_chg_regis(maps orm.Params) error
	TryUpdateT_intrt_chg_regis(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intrt_chg_regis(maps orm.Params) error
	CancelUpdateT_intrt_chg_regis(maps orm.Params) error
}

type T_intrt_chg_regisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intrt_chg_regisServiceImpl) TryInsertT_intrt_chg_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intrt_chg_regis(o, maps)
}

func (impl *T_intrt_chg_regisServiceImpl) ConfirmInsertT_intrt_chg_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intrt_chg_regisTccState(o, maps)
}

func (impl *T_intrt_chg_regisServiceImpl) CancelInsertT_intrt_chg_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intrt_chg_regisTccState(o, maps)
}

//智能查询
func (impl *T_intrt_chg_regisServiceImpl) QueryT_intrt_chg_regis(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intrt_chg_regis(params, 0)
}

var UpdateT_intrt_chg_regisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intrt_chg_regis",
	ConfirmMethod: "ConfirmUpdateT_intrt_chg_regis",
	CancelMethod:  "CancelUpdateT_intrt_chg_regis",
}

func (impl *T_intrt_chg_regisServiceImpl) TryUpdateT_intrt_chg_regis(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intrt_chg_regisById(params, 0)
	return
}

func (impl *T_intrt_chg_regisServiceImpl) ConfirmUpdateT_intrt_chg_regis(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intrt_chg_regis(maps)
}

func (impl *T_intrt_chg_regisServiceImpl) CancelUpdateT_intrt_chg_regis(maps orm.Params) (err error) {
	return
}
