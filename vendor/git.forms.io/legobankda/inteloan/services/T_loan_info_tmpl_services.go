package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_info_tmplCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_info_tmpl",
	ConfirmMethod: "ConfirmInsertT_loan_info_tmpl",
	CancelMethod:  "CancelInsertT_loan_info_tmpl",
}

type T_loan_info_tmplService interface {
	TryInsertT_loan_info_tmpl(maps orm.Params) error
	ConfirmInsertT_loan_info_tmpl(maps orm.Params) error
	CancelInsertT_loan_info_tmpl(maps orm.Params) error
	TryUpdateT_loan_info_tmpl(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_info_tmpl(maps orm.Params) error
	CancelUpdateT_loan_info_tmpl(maps orm.Params) error
}

type T_loan_info_tmplServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_info_tmplServiceImpl) TryInsertT_loan_info_tmpl(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_info_tmpl(o, maps)
}

func (impl *T_loan_info_tmplServiceImpl) ConfirmInsertT_loan_info_tmpl(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_info_tmplTccState(o, maps)
}

func (impl *T_loan_info_tmplServiceImpl) CancelInsertT_loan_info_tmpl(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_info_tmplTccState(o, maps)
}

//智能查询
func (impl *T_loan_info_tmplServiceImpl) QueryT_loan_info_tmpl(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_info_tmpl(params, 0)
}

var UpdateT_loan_info_tmplCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_info_tmpl",
	ConfirmMethod: "ConfirmUpdateT_loan_info_tmpl",
	CancelMethod:  "CancelUpdateT_loan_info_tmpl",
}

func (impl *T_loan_info_tmplServiceImpl) TryUpdateT_loan_info_tmpl(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_info_tmplById(params, 0)
	return
}

func (impl *T_loan_info_tmplServiceImpl) ConfirmUpdateT_loan_info_tmpl(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_info_tmpl(maps)
}

func (impl *T_loan_info_tmplServiceImpl) CancelUpdateT_loan_info_tmpl(maps orm.Params) (err error) {
	return
}
