package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_save_fileCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_save_file",
	ConfirmMethod: "ConfirmInsertT_loan_save_file",
	CancelMethod:  "CancelInsertT_loan_save_file",
}

type T_loan_save_fileService interface {
	TryInsertT_loan_save_file(maps orm.Params) error
	ConfirmInsertT_loan_save_file(maps orm.Params) error
	CancelInsertT_loan_save_file(maps orm.Params) error
	TryUpdateT_loan_save_file(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_save_file(maps orm.Params) error
	CancelUpdateT_loan_save_file(maps orm.Params) error
}

type T_loan_save_fileServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_save_fileServiceImpl) TryInsertT_loan_save_file(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_save_file(o, maps)
}

func (impl *T_loan_save_fileServiceImpl) ConfirmInsertT_loan_save_file(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_save_fileTccState(o, maps)
}

func (impl *T_loan_save_fileServiceImpl) CancelInsertT_loan_save_file(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_save_fileTccState(o, maps)
}

//智能查询
func (impl *T_loan_save_fileServiceImpl) QueryT_loan_save_file(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_save_file(params, 0)
}

var UpdateT_loan_save_fileCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_save_file",
	ConfirmMethod: "ConfirmUpdateT_loan_save_file",
	CancelMethod:  "CancelUpdateT_loan_save_file",
}

func (impl *T_loan_save_fileServiceImpl) TryUpdateT_loan_save_file(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_save_fileById(params, 0)
	return
}

func (impl *T_loan_save_fileServiceImpl) ConfirmUpdateT_loan_save_file(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_save_file(maps)
}

func (impl *T_loan_save_fileServiceImpl) CancelUpdateT_loan_save_file(maps orm.Params) (err error) {
	return
}
