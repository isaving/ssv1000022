package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intrt_noCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intrt_no",
	ConfirmMethod: "ConfirmInsertT_intrt_no",
	CancelMethod:  "CancelInsertT_intrt_no",
}

type T_intrt_noService interface {
	TryInsertT_intrt_no(maps orm.Params) error
	ConfirmInsertT_intrt_no(maps orm.Params) error
	CancelInsertT_intrt_no(maps orm.Params) error
	TryUpdateT_intrt_no(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intrt_no(maps orm.Params) error
	CancelUpdateT_intrt_no(maps orm.Params) error
}

type T_intrt_noServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intrt_noServiceImpl) TryInsertT_intrt_no(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intrt_no(o, maps)
}

func (impl *T_intrt_noServiceImpl) ConfirmInsertT_intrt_no(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intrt_noTccState(o, maps)
}

func (impl *T_intrt_noServiceImpl) CancelInsertT_intrt_no(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intrt_noTccState(o, maps)
}

//智能查询
func (impl *T_intrt_noServiceImpl) QueryT_intrt_no(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intrt_no(params, 0)
}

var UpdateT_intrt_noCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intrt_no",
	ConfirmMethod: "ConfirmUpdateT_intrt_no",
	CancelMethod:  "CancelUpdateT_intrt_no",
}

func (impl *T_intrt_noServiceImpl) TryUpdateT_intrt_no(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intrt_noById(params, 0)
	return
}

func (impl *T_intrt_noServiceImpl) ConfirmUpdateT_intrt_no(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intrt_no(maps)
}

func (impl *T_intrt_noServiceImpl) CancelUpdateT_intrt_no(maps orm.Params) (err error) {
	return
}
