package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_risk_cls_appr_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_risk_cls_appr_flow",
	ConfirmMethod: "ConfirmInsertT_risk_cls_appr_flow",
	CancelMethod:  "CancelInsertT_risk_cls_appr_flow",
}

type T_risk_cls_appr_flowService interface {
	TryInsertT_risk_cls_appr_flow(maps orm.Params) error
	ConfirmInsertT_risk_cls_appr_flow(maps orm.Params) error
	CancelInsertT_risk_cls_appr_flow(maps orm.Params) error
	TryUpdateT_risk_cls_appr_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_risk_cls_appr_flow(maps orm.Params) error
	CancelUpdateT_risk_cls_appr_flow(maps orm.Params) error
}

type T_risk_cls_appr_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_risk_cls_appr_flowServiceImpl) TryInsertT_risk_cls_appr_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_risk_cls_appr_flow(o, maps)
}

func (impl *T_risk_cls_appr_flowServiceImpl) ConfirmInsertT_risk_cls_appr_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_risk_cls_appr_flowTccState(o, maps)
}

func (impl *T_risk_cls_appr_flowServiceImpl) CancelInsertT_risk_cls_appr_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_risk_cls_appr_flowTccState(o, maps)
}

//智能查询
func (impl *T_risk_cls_appr_flowServiceImpl) QueryT_risk_cls_appr_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_risk_cls_appr_flow(params, 0)
}

var UpdateT_risk_cls_appr_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_risk_cls_appr_flow",
	ConfirmMethod: "ConfirmUpdateT_risk_cls_appr_flow",
	CancelMethod:  "CancelUpdateT_risk_cls_appr_flow",
}

func (impl *T_risk_cls_appr_flowServiceImpl) TryUpdateT_risk_cls_appr_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_risk_cls_appr_flowById(params, 0)
	return
}

func (impl *T_risk_cls_appr_flowServiceImpl) ConfirmUpdateT_risk_cls_appr_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_risk_cls_appr_flow(maps)
}

func (impl *T_risk_cls_appr_flowServiceImpl) CancelUpdateT_risk_cls_appr_flow(maps orm.Params) (err error) {
	return
}
