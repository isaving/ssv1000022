package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_object_manageCompensable = client.Compensable{
	TryMethod:     "TryInsertT_object_manage",
	ConfirmMethod: "ConfirmInsertT_object_manage",
	CancelMethod:  "CancelInsertT_object_manage",
}

type T_object_manageService interface {
	TryInsertT_object_manage(maps orm.Params) error
	ConfirmInsertT_object_manage(maps orm.Params) error
	CancelInsertT_object_manage(maps orm.Params) error
	TryUpdateT_object_manage(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_object_manage(maps orm.Params) error
	CancelUpdateT_object_manage(maps orm.Params) error
}

type T_object_manageServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_object_manageServiceImpl) TryInsertT_object_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_object_manage(o, maps)
}

func (impl *T_object_manageServiceImpl) ConfirmInsertT_object_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_object_manageTccState(o, maps)
}

func (impl *T_object_manageServiceImpl) CancelInsertT_object_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_object_manageTccState(o, maps)
}

//智能查询
func (impl *T_object_manageServiceImpl) QueryT_object_manage(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_object_manage(params, 0)
}

var UpdateT_object_manageCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_object_manage",
	ConfirmMethod: "ConfirmUpdateT_object_manage",
	CancelMethod:  "CancelUpdateT_object_manage",
}

func (impl *T_object_manageServiceImpl) TryUpdateT_object_manage(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_object_manageByContent(params, 0)
	return
}

func (impl *T_object_manageServiceImpl) ConfirmUpdateT_object_manage(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_object_manageByContent(maps)
}

func (impl *T_object_manageServiceImpl) CancelUpdateT_object_manage(maps orm.Params) (err error) {
	return
}
