package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_inspection_task_resultCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_inspection_task_result",
	ConfirmMethod: "ConfirmInsertT_loan_inspection_task_result",
	CancelMethod:  "CancelInsertT_loan_inspection_task_result",
}

type T_loan_inspection_task_resultService interface {
	TryInsertT_loan_inspection_task_result(maps orm.Params) error
	ConfirmInsertT_loan_inspection_task_result(maps orm.Params) error
	CancelInsertT_loan_inspection_task_result(maps orm.Params) error
	TryUpdateT_loan_inspection_task_result(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_inspection_task_result(maps orm.Params) error
	CancelUpdateT_loan_inspection_task_result(maps orm.Params) error
}

type T_loan_inspection_task_resultServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_inspection_task_resultServiceImpl) TryInsertT_loan_inspection_task_result(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_inspection_task_result(o, maps)
}

func (impl *T_loan_inspection_task_resultServiceImpl) ConfirmInsertT_loan_inspection_task_result(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_inspection_task_resultTccState(o, maps)
}

func (impl *T_loan_inspection_task_resultServiceImpl) CancelInsertT_loan_inspection_task_result(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_inspection_task_resultTccState(o, maps)
}

//智能查询
func (impl *T_loan_inspection_task_resultServiceImpl) QueryT_loan_inspection_task_result(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_inspection_task_result(params, 0)
}

var UpdateT_loan_inspection_task_resultCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_inspection_task_result",
	ConfirmMethod: "ConfirmUpdateT_loan_inspection_task_result",
	CancelMethod:  "CancelUpdateT_loan_inspection_task_result",
}

func (impl *T_loan_inspection_task_resultServiceImpl) TryUpdateT_loan_inspection_task_result(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_inspection_task_resultById(params, 0)
	return
}

func (impl *T_loan_inspection_task_resultServiceImpl) ConfirmUpdateT_loan_inspection_task_result(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_inspection_task_result(maps)
}

func (impl *T_loan_inspection_task_resultServiceImpl) CancelUpdateT_loan_inspection_task_result(maps orm.Params) (err error) {
	return
}
