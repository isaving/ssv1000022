package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_abnormal_messagesCompensable = client.Compensable{
	TryMethod:     "TryInsertT_abnormal_messages",
	ConfirmMethod: "ConfirmInsertT_abnormal_messages",
	CancelMethod:  "CancelInsertT_abnormal_messages",
}

type T_abnormal_messagesService interface {
	TryInsertT_abnormal_messages(maps orm.Params) error
	ConfirmInsertT_abnormal_messages(maps orm.Params) error
	CancelInsertT_abnormal_messages(maps orm.Params) error
	TryUpdateT_abnormal_messages(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_abnormal_messages(maps orm.Params) error
	CancelUpdateT_abnormal_messages(maps orm.Params) error
}

type T_abnormal_messagesServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_abnormal_messagesServiceImpl) TryInsertT_abnormal_messages(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_abnormal_messages(o, maps)
}

func (impl *T_abnormal_messagesServiceImpl) ConfirmInsertT_abnormal_messages(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_abnormal_messagesTccState(o, maps)
}

func (impl *T_abnormal_messagesServiceImpl) CancelInsertT_abnormal_messages(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_abnormal_messagesTccState(o, maps)
}

//智能查询
func (impl *T_abnormal_messagesServiceImpl) QueryT_abnormal_messages(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_abnormal_messages(params, 0)
}

var UpdateT_abnormal_messagesCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_abnormal_messages",
	ConfirmMethod: "ConfirmUpdateT_abnormal_messages",
	CancelMethod:  "CancelUpdateT_abnormal_messages",
}

func (impl *T_abnormal_messagesServiceImpl) TryUpdateT_abnormal_messages(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_abnormal_messagesById(params, 0)
	return
}

func (impl *T_abnormal_messagesServiceImpl) ConfirmUpdateT_abnormal_messages(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_abnormal_messages(maps)
}

func (impl *T_abnormal_messagesServiceImpl) CancelUpdateT_abnormal_messages(maps orm.Params) (err error) {
	return
}
