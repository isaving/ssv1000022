package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_event_tempCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_event_temp",
	ConfirmMethod: "ConfirmInsertT_ae_event_temp",
	CancelMethod:  "CancelInsertT_ae_event_temp",
}

type T_ae_event_tempService interface {
	TryInsertT_ae_event_temp(maps orm.Params) error
	ConfirmInsertT_ae_event_temp(maps orm.Params) error
	CancelInsertT_ae_event_temp(maps orm.Params) error
	TryUpdateT_ae_event_temp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_event_temp(maps orm.Params) error
	CancelUpdateT_ae_event_temp(maps orm.Params) error
}

type T_ae_event_tempServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_event_tempServiceImpl) TryInsertT_ae_event_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_event_temp(o, maps)
}

func (impl *T_ae_event_tempServiceImpl) ConfirmInsertT_ae_event_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_event_tempTccState(o, maps)
}

func (impl *T_ae_event_tempServiceImpl) CancelInsertT_ae_event_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_event_tempTccState(o, maps)
}

//智能查询
func (impl *T_ae_event_tempServiceImpl) QueryT_ae_event_temp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_event_temp(params, 0)
}

var UpdateT_ae_event_tempCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_event_temp",
	ConfirmMethod: "ConfirmUpdateT_ae_event_temp",
	CancelMethod:  "CancelUpdateT_ae_event_temp",
}

func (impl *T_ae_event_tempServiceImpl) TryUpdateT_ae_event_temp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_event_tempById(params, 0)
	return
}

func (impl *T_ae_event_tempServiceImpl) ConfirmUpdateT_ae_event_temp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_event_temp(maps)
}

func (impl *T_ae_event_tempServiceImpl) CancelUpdateT_ae_event_temp(maps orm.Params) (err error) {
	return
}
