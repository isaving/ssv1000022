package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_accti_acct_detaCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_accti_acct_deta",
	ConfirmMethod: "ConfirmInsertT_loan_accti_acct_deta",
	CancelMethod:  "CancelInsertT_loan_accti_acct_deta",
}

type T_loan_accti_acct_detaService interface {
	TryInsertT_loan_accti_acct_deta(maps orm.Params) error
	ConfirmInsertT_loan_accti_acct_deta(maps orm.Params) error
	CancelInsertT_loan_accti_acct_deta(maps orm.Params) error
	TryUpdateT_loan_accti_acct_deta(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_accti_acct_deta(maps orm.Params) error
	CancelUpdateT_loan_accti_acct_deta(maps orm.Params) error
}

type T_loan_accti_acct_detaServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_accti_acct_detaServiceImpl) TryInsertT_loan_accti_acct_deta(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_accti_acct_deta(o, maps)
}

func (impl *T_loan_accti_acct_detaServiceImpl) ConfirmInsertT_loan_accti_acct_deta(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_accti_acct_detaTccState(o, maps)
}

func (impl *T_loan_accti_acct_detaServiceImpl) CancelInsertT_loan_accti_acct_deta(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_accti_acct_detaTccState(o, maps)
}

//智能查询
func (impl *T_loan_accti_acct_detaServiceImpl) QueryT_loan_accti_acct_deta(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_accti_acct_deta(params, 0)
}

var UpdateT_loan_accti_acct_detaCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_accti_acct_deta",
	ConfirmMethod: "ConfirmUpdateT_loan_accti_acct_deta",
	CancelMethod:  "CancelUpdateT_loan_accti_acct_deta",
}

func (impl *T_loan_accti_acct_detaServiceImpl) TryUpdateT_loan_accti_acct_deta(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_accti_acct_detaById(params, 0)
	return
}

func (impl *T_loan_accti_acct_detaServiceImpl) ConfirmUpdateT_loan_accti_acct_deta(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_accti_acct_deta(maps)
}

func (impl *T_loan_accti_acct_detaServiceImpl) CancelUpdateT_loan_accti_acct_deta(maps orm.Params) (err error) {
	return
}
