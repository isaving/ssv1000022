package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertAcsjhfpCompensable = client.Compensable{
	TryMethod:     "TryInsertAcsjhfp",
	ConfirmMethod: "ConfirmInsertAcsjhfp",
	CancelMethod:  "CancelInsertAcsjhfp",
}

type AcsjhfpService interface {
	TryInsertAcsjhfp(maps orm.Params) error
	ConfirmInsertAcsjhfp(maps orm.Params) error
	CancelInsertAcsjhfp(maps orm.Params) error
	TryUpdateAcsjhfp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateAcsjhfp(maps orm.Params) error
	CancelUpdateAcsjhfp(maps orm.Params) error
}

type AcsjhfpServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *AcsjhfpServiceImpl) TryInsertAcsjhfp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertAcsjhfp(o, maps)
}

func (impl *AcsjhfpServiceImpl) ConfirmInsertAcsjhfp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateAcsjhfpTccState(o, maps)
}

func (impl *AcsjhfpServiceImpl) CancelInsertAcsjhfp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteAcsjhfpTccState(o, maps)
}

//智能查询
func (impl *AcsjhfpServiceImpl) QueryAcsjhfp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryAcsjhfp(params, 0)
}

var UpdateAcsjhfpCompensable = client.Compensable{
	TryMethod:     "TryUpdateAcsjhfp",
	ConfirmMethod: "ConfirmUpdateAcsjhfp",
	CancelMethod:  "CancelUpdateAcsjhfp",
}

func (impl *AcsjhfpServiceImpl) TryUpdateAcsjhfp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryAcsjhfpById(params, 0)
	return
}

func (impl *AcsjhfpServiceImpl) ConfirmUpdateAcsjhfp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateAcsjhfp(maps)
}

func (impl *AcsjhfpServiceImpl) CancelUpdateAcsjhfp(maps orm.Params) (err error) {
	return
}
