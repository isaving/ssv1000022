package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_approve_taskCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_approve_task",
	ConfirmMethod: "ConfirmInsertT_loan_approve_task",
	CancelMethod:  "CancelInsertT_loan_approve_task",
}

type T_loan_approve_taskService interface {
	TryInsertT_loan_approve_task(maps orm.Params) error
	ConfirmInsertT_loan_approve_task(maps orm.Params) error
	CancelInsertT_loan_approve_task(maps orm.Params) error
	TryUpdateT_loan_approve_task(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_approve_task(maps orm.Params) error
	CancelUpdateT_loan_approve_task(maps orm.Params) error
}

type T_loan_approve_taskServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_approve_taskServiceImpl) TryInsertT_loan_approve_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_approve_task(o, maps)
}

func (impl *T_loan_approve_taskServiceImpl) ConfirmInsertT_loan_approve_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_approve_taskTccState(o, maps)
}

func (impl *T_loan_approve_taskServiceImpl) CancelInsertT_loan_approve_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_approve_taskTccState(o, maps)
}

//智能查询
func (impl *T_loan_approve_taskServiceImpl) QueryT_loan_approve_task(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_approve_task(params, 0)
}

func (impl *T_loan_approve_taskServiceImpl) QueryT_loan_approve_task1(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_approve_task1(params, 0)
}

var UpdateT_loan_approve_taskCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_approve_task",
	ConfirmMethod: "ConfirmUpdateT_loan_approve_task",
	CancelMethod:  "CancelUpdateT_loan_approve_task",
}

func (impl *T_loan_approve_taskServiceImpl) TryUpdateT_loan_approve_task(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_approve_taskById(params, 0)
	return
}

func (impl *T_loan_approve_taskServiceImpl) ConfirmUpdateT_loan_approve_task(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_approve_task(maps)
}

func (impl *T_loan_approve_taskServiceImpl) CancelUpdateT_loan_approve_task(maps orm.Params) (err error) {
	return
}
