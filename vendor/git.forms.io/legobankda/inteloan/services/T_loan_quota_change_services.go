package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_quota_changeCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_quota_change",
	ConfirmMethod: "ConfirmInsertT_loan_quota_change",
	CancelMethod:  "CancelInsertT_loan_quota_change",
}

type T_loan_quota_changeService interface {
	TryInsertT_loan_quota_change(maps orm.Params) error
	ConfirmInsertT_loan_quota_change(maps orm.Params) error
	CancelInsertT_loan_quota_change(maps orm.Params) error
	TryUpdateT_loan_quota_change(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_quota_change(maps orm.Params) error
	CancelUpdateT_loan_quota_change(maps orm.Params) error
}

type T_loan_quota_changeServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_quota_changeServiceImpl) TryInsertT_loan_quota_change(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_quota_change(o, maps)
}

func (impl *T_loan_quota_changeServiceImpl) ConfirmInsertT_loan_quota_change(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_quota_changeTccState(o, maps)
}

func (impl *T_loan_quota_changeServiceImpl) CancelInsertT_loan_quota_change(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_quota_changeTccState(o, maps)
}

//智能查询
func (impl *T_loan_quota_changeServiceImpl) QueryT_loan_quota_change(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_quota_change(params, 0)
}

var UpdateT_loan_quota_changeCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_quota_change",
	ConfirmMethod: "ConfirmUpdateT_loan_quota_change",
	CancelMethod:  "CancelUpdateT_loan_quota_change",
}

func (impl *T_loan_quota_changeServiceImpl) TryUpdateT_loan_quota_change(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_quota_changeById(params, 0)
	return
}

func (impl *T_loan_quota_changeServiceImpl) ConfirmUpdateT_loan_quota_change(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_quota_change(maps)
}

func (impl *T_loan_quota_changeServiceImpl) CancelUpdateT_loan_quota_change(maps orm.Params) (err error) {
	return
}
