package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_proc_model_paraCompensable = client.Compensable{
	TryMethod:     "TryInsertT_proc_model_para",
	ConfirmMethod: "ConfirmInsertT_proc_model_para",
	CancelMethod:  "CancelInsertT_proc_model_para",
}

type T_proc_model_paraService interface {
	TryInsertT_proc_model_para(maps orm.Params) error
	ConfirmInsertT_proc_model_para(maps orm.Params) error
	CancelInsertT_proc_model_para(maps orm.Params) error
	TryUpdateT_proc_model_para(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_proc_model_para(maps orm.Params) error
	CancelUpdateT_proc_model_para(maps orm.Params) error
}

type T_proc_model_paraServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_proc_model_paraServiceImpl) TryInsertT_proc_model_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_proc_model_para(o, maps)
}

func (impl *T_proc_model_paraServiceImpl) ConfirmInsertT_proc_model_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_proc_model_paraTccState(o, maps)
}

func (impl *T_proc_model_paraServiceImpl) CancelInsertT_proc_model_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_proc_model_paraTccState(o, maps)
}

//智能查询
func (impl *T_proc_model_paraServiceImpl) QueryT_proc_model_para(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_proc_model_para(params, 0)
}

var UpdateT_proc_model_paraCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_proc_model_para",
	ConfirmMethod: "ConfirmUpdateT_proc_model_para",
	CancelMethod:  "CancelUpdateT_proc_model_para",
}

func (impl *T_proc_model_paraServiceImpl) TryUpdateT_proc_model_para(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_proc_model_paraById(params, 0)
	return
}

func (impl *T_proc_model_paraServiceImpl) ConfirmUpdateT_proc_model_para(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_proc_model_para(maps)
}

func (impl *T_proc_model_paraServiceImpl) CancelUpdateT_proc_model_para(maps orm.Params) (err error) {
	return
}
