package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertTs_user_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertTs_user_info",
	ConfirmMethod: "ConfirmInsertTs_user_info",
	CancelMethod:  "CancelInsertTs_user_info",
}

type Ts_user_infoService interface {
	TryInsertTs_user_info(maps orm.Params) error
	ConfirmInsertTs_user_info(maps orm.Params) error
	CancelInsertTs_user_info(maps orm.Params) error
	TryUpdateTs_user_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateTs_user_info(maps orm.Params) error
	CancelUpdateTs_user_info(maps orm.Params) error
}

type Ts_user_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Ts_user_infoServiceImpl) TryInsertTs_user_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertTs_user_info(o, maps)
}

func (impl *Ts_user_infoServiceImpl) ConfirmInsertTs_user_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateTs_user_infoTccState(o, maps)
}

func (impl *Ts_user_infoServiceImpl) CancelInsertTs_user_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteTs_user_infoTccState(o, maps)
}

//智能查询
func (impl *Ts_user_infoServiceImpl) QueryTs_user_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryTs_user_info(params, 0)
}

var UpdateTs_user_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateTs_user_info",
	ConfirmMethod: "ConfirmUpdateTs_user_info",
	CancelMethod:  "CancelUpdateTs_user_info",
}

func (impl *Ts_user_infoServiceImpl) TryUpdateTs_user_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTs_user_infoById(params, 0)
	return
}

func (impl *Ts_user_infoServiceImpl) ConfirmUpdateTs_user_info(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateTs_user_info(maps)
}

func (impl *Ts_user_infoServiceImpl) CancelUpdateTs_user_info(maps orm.Params) (err error) {
	return
}
