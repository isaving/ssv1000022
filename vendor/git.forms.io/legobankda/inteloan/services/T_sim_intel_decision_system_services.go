package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sim_intel_decision_systemCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sim_intel_decision_system",
	ConfirmMethod: "ConfirmInsertT_sim_intel_decision_system",
	CancelMethod:  "CancelInsertT_sim_intel_decision_system",
}

type T_sim_intel_decision_systemService interface {
	TryInsertT_sim_intel_decision_system(maps orm.Params) error
	ConfirmInsertT_sim_intel_decision_system(maps orm.Params) error
	CancelInsertT_sim_intel_decision_system(maps orm.Params) error
	TryUpdateT_sim_intel_decision_system(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sim_intel_decision_system(maps orm.Params) error
	CancelUpdateT_sim_intel_decision_system(maps orm.Params) error
}

type T_sim_intel_decision_systemServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sim_intel_decision_systemServiceImpl) TryInsertT_sim_intel_decision_system(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sim_intel_decision_system(o, maps)
}

func (impl *T_sim_intel_decision_systemServiceImpl) ConfirmInsertT_sim_intel_decision_system(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sim_intel_decision_systemTccState(o, maps)
}

func (impl *T_sim_intel_decision_systemServiceImpl) CancelInsertT_sim_intel_decision_system(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sim_intel_decision_systemTccState(o, maps)
}

//智能查询
func (impl *T_sim_intel_decision_systemServiceImpl) QueryT_sim_intel_decision_system(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sim_intel_decision_system(params, 0)
}

var UpdateT_sim_intel_decision_systemCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sim_intel_decision_system",
	ConfirmMethod: "ConfirmUpdateT_sim_intel_decision_system",
	CancelMethod:  "CancelUpdateT_sim_intel_decision_system",
}

func (impl *T_sim_intel_decision_systemServiceImpl) TryUpdateT_sim_intel_decision_system(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sim_intel_decision_systemById(params, 0)
	return
}

func (impl *T_sim_intel_decision_systemServiceImpl) ConfirmUpdateT_sim_intel_decision_system(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sim_intel_decision_system(maps)
}

func (impl *T_sim_intel_decision_systemServiceImpl) CancelUpdateT_sim_intel_decision_system(maps orm.Params) (err error) {
	return
}
