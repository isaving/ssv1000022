package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_prod_flow_paraCompensable = client.Compensable{
	TryMethod:     "TryInsertT_prod_flow_para",
	ConfirmMethod: "ConfirmInsertT_prod_flow_para",
	CancelMethod:  "CancelInsertT_prod_flow_para",
}

type T_prod_flow_paraService interface {
	TryInsertT_prod_flow_para(maps orm.Params) error
	ConfirmInsertT_prod_flow_para(maps orm.Params) error
	CancelInsertT_prod_flow_para(maps orm.Params) error
	TryUpdateT_prod_flow_para(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_prod_flow_para(maps orm.Params) error
	CancelUpdateT_prod_flow_para(maps orm.Params) error
}

type T_prod_flow_paraServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_prod_flow_paraServiceImpl) TryInsertT_prod_flow_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_prod_flow_para(o, maps)
}

func (impl *T_prod_flow_paraServiceImpl) ConfirmInsertT_prod_flow_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_prod_flow_paraTccState(o, maps)
}

func (impl *T_prod_flow_paraServiceImpl) CancelInsertT_prod_flow_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_prod_flow_paraTccState(o, maps)
}

//智能查询
func (impl *T_prod_flow_paraServiceImpl) QueryT_prod_flow_para(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_prod_flow_para(params, 0)
}

var UpdateT_prod_flow_paraCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_prod_flow_para",
	ConfirmMethod: "ConfirmUpdateT_prod_flow_para",
	CancelMethod:  "CancelUpdateT_prod_flow_para",
}

func (impl *T_prod_flow_paraServiceImpl) TryUpdateT_prod_flow_para(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_prod_flow_paraById(params, 0)
	return
}

func (impl *T_prod_flow_paraServiceImpl) ConfirmUpdateT_prod_flow_para(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_prod_flow_para(maps)
}

func (impl *T_prod_flow_paraServiceImpl) CancelUpdateT_prod_flow_para(maps orm.Params) (err error) {
	return
}
