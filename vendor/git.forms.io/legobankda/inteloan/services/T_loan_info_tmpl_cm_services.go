package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_info_tmpl_cmCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_info_tmpl_cm",
	ConfirmMethod: "ConfirmInsertT_loan_info_tmpl_cm",
	CancelMethod:  "CancelInsertT_loan_info_tmpl_cm",
}

type T_loan_info_tmpl_cmService interface {
	TryInsertT_loan_info_tmpl_cm(maps orm.Params) error
	ConfirmInsertT_loan_info_tmpl_cm(maps orm.Params) error
	CancelInsertT_loan_info_tmpl_cm(maps orm.Params) error
	TryUpdateT_loan_info_tmpl_cm(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_info_tmpl_cm(maps orm.Params) error
	CancelUpdateT_loan_info_tmpl_cm(maps orm.Params) error
}

type T_loan_info_tmpl_cmServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_info_tmpl_cmServiceImpl) TryInsertT_loan_info_tmpl_cm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_info_tmpl_cm(o, maps)
}

func (impl *T_loan_info_tmpl_cmServiceImpl) ConfirmInsertT_loan_info_tmpl_cm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_info_tmpl_cmTccState(o, maps)
}

func (impl *T_loan_info_tmpl_cmServiceImpl) CancelInsertT_loan_info_tmpl_cm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_info_tmpl_cmTccState(o, maps)
}

//智能查询
func (impl *T_loan_info_tmpl_cmServiceImpl) QueryT_loan_info_tmpl_cm(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_info_tmpl_cm(params, 0)
}

var UpdateT_loan_info_tmpl_cmCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_info_tmpl_cm",
	ConfirmMethod: "ConfirmUpdateT_loan_info_tmpl_cm",
	CancelMethod:  "CancelUpdateT_loan_info_tmpl_cm",
}

func (impl *T_loan_info_tmpl_cmServiceImpl) TryUpdateT_loan_info_tmpl_cm(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_info_tmpl_cmById(params, 0)
	return
}

func (impl *T_loan_info_tmpl_cmServiceImpl) ConfirmUpdateT_loan_info_tmpl_cm(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_info_tmpl_cm(maps)
}

func (impl *T_loan_info_tmpl_cmServiceImpl) CancelUpdateT_loan_info_tmpl_cm(maps orm.Params) (err error) {
	return
}
