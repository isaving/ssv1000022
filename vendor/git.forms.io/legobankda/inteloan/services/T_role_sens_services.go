package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_role_sensCompensable = client.Compensable{
	TryMethod:     "TryInsertT_role_sens",
	ConfirmMethod: "ConfirmInsertT_role_sens",
	CancelMethod:  "CancelInsertT_role_sens",
}

type T_role_sensService interface {
	TryInsertT_role_sens(maps orm.Params) error
	ConfirmInsertT_role_sens(maps orm.Params) error
	CancelInsertT_role_sens(maps orm.Params) error
	TryUpdateT_role_sens(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_role_sens(maps orm.Params) error
	CancelUpdateT_role_sens(maps orm.Params) error
}

type T_role_sensServiceImpl struct {
	aspect.DTSBaseService
}

func (impl *T_role_sensServiceImpl) TryInsertT_role_sens(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_role_sens(o, maps)
}

func (impl *T_role_sensServiceImpl) ConfirmInsertT_role_sens(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_role_sensTccState(o, maps)
}

func (impl *T_role_sensServiceImpl) CancelInsertT_role_sens(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_role_sensTccState(o, maps)
}

//智能查询
func (impl *T_role_sensServiceImpl) QueryT_role_sens(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_role_sens(params, 0)
}

var UpdateT_role_sensCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_role_sens",
	ConfirmMethod: "ConfirmUpdateT_role_sens",
	CancelMethod:  "CancelUpdateT_role_sens",
}

func (impl *T_role_sensServiceImpl) TryUpdateT_role_sens(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_role_sensById(params, 0)
	return
}

func (impl *T_role_sensServiceImpl) ConfirmUpdateT_role_sens(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_role_sens(maps)
}

func (impl *T_role_sensServiceImpl) CancelUpdateT_role_sens(maps orm.Params) (err error) {
	return
}
var DeleteT_role_sensCompensable = client.Compensable{
	TryMethod:     "TryDeleteT_role_sens",
	ConfirmMethod: "ConfirmDeleteT_role_sens",
	CancelMethod:  "CancelDeleteT_role_sens",
}

func (impl *T_role_sensServiceImpl) TryDeleteT_role_sens(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_role_sensById(params, 0)
	return
}

func (impl *T_role_sensServiceImpl) ConfirmDeleteT_role_sens(maps orm.Params) (err error) {
	return dao.DeleteT_role_sens(maps)
}

func (impl *T_role_sensServiceImpl) CancelDeleteT_role_sens(maps orm.Params) (err error) {
	return
}