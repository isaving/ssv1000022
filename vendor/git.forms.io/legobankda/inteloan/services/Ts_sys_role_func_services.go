package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertTs_sys_role_funcCompensable = client.Compensable{
	TryMethod:     "TryInsertTs_sys_role_func",
	ConfirmMethod: "ConfirmInsertTs_sys_role_func",
	CancelMethod:  "CancelInsertTs_sys_role_func",
}

type Ts_sys_role_funcService interface {
	TryInsertTs_sys_role_func(maps orm.Params) error
	ConfirmInsertTs_sys_role_func(maps orm.Params) error
	CancelInsertTs_sys_role_func(maps orm.Params) error
	TryUpdateTs_sys_role_func(maps orm.Params) (orm.Params, error)
	ConfirmUpdateTs_sys_role_func(maps orm.Params) error
	CancelUpdateTs_sys_role_func(maps orm.Params) error
}

type Ts_sys_role_funcServiceImpl struct {
	aspect.DTSBaseService
}

func (impl *Ts_sys_role_funcServiceImpl) TryInsertTs_sys_role_func(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertTs_sys_role_func(o, maps)
}

func (impl *Ts_sys_role_funcServiceImpl) ConfirmInsertTs_sys_role_func(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateTs_sys_role_funcTccState(o, maps)
}

func (impl *Ts_sys_role_funcServiceImpl) CancelInsertTs_sys_role_func(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteTs_sys_role_funcTccState(o, maps)
}

//智能查询
func (impl *Ts_sys_role_funcServiceImpl) QueryTs_sys_role_func(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryTs_sys_role_func(params, 0)
}

var UpdateTs_sys_role_funcCompensable = client.Compensable{
	TryMethod:     "TryUpdateTs_sys_role_func",
	ConfirmMethod: "ConfirmUpdateTs_sys_role_func",
	CancelMethod:  "CancelUpdateTs_sys_role_func",
}

func (impl *Ts_sys_role_funcServiceImpl) TryUpdateTs_sys_role_func(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTs_sys_role_funcById(params, 0)
	return
}

func (impl *Ts_sys_role_funcServiceImpl) ConfirmUpdateTs_sys_role_func(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateTs_sys_role_func(maps)
}

func (impl *Ts_sys_role_funcServiceImpl) CancelUpdateTs_sys_role_func(maps orm.Params) (err error) {
	return
}

var DeleteTs_sys_role_funcCompensable = client.Compensable{
	TryMethod:     "TryDeleteTs_sys_role_func",
	ConfirmMethod: "ConfirmDeleteTs_sys_role_func",
	CancelMethod:  "CancelDeleteTs_sys_role_func",
}

func (impl *Ts_sys_role_funcServiceImpl) TryDeleteTs_sys_role_func(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryTs_sys_role_funcById(params, 0)
	return
}

func (impl *Ts_sys_role_funcServiceImpl) ConfirmDeleteTs_sys_role_func(maps orm.Params) (err error) {
	return dao.DeleteTs_sys_role_func(maps)
}

func (impl *Ts_sys_role_funcServiceImpl) CancelDeleteTs_sys_role_func(maps orm.Params) (err error) {
	return
}