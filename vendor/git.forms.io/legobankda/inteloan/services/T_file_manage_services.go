package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_file_manageCompensable = client.Compensable{
	TryMethod:     "TryInsertT_file_manage",
	ConfirmMethod: "ConfirmInsertT_file_manage",
	CancelMethod:  "CancelInsertT_file_manage",
}

type T_file_manageService interface {
	TryInsertT_file_manage(maps orm.Params) error
	ConfirmInsertT_file_manage(maps orm.Params) error
	CancelInsertT_file_manage(maps orm.Params) error
	TryUpdateT_file_manage(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_file_manage(maps orm.Params) error
	CancelUpdateT_file_manage(maps orm.Params) error
}

type  T_file_manageServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_file_manageServiceImpl) TryInsertT_file_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_file_manage(o, maps)
}

func (impl *T_file_manageServiceImpl) ConfirmInsertT_file_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_file_manageTccState(o, maps)
}

func (impl *T_file_manageServiceImpl) CancelInsertT_file_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_file_manageTccState(o, maps)
}

//智能查询
func (impl *T_file_manageServiceImpl) QueryT_file_manage(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_file_manage(params, 0)
}

var UpdateT_file_manageCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_file_manage",
	ConfirmMethod: "ConfirmUpdateT_file_manage",
	CancelMethod:  "CancelUpdateT_file_manage",
}

func (impl *T_file_manageServiceImpl) TryUpdateT_file_manage(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_file_manageById(params, 0)
	return
}

func (impl *T_file_manageServiceImpl) ConfirmUpdateT_file_manage(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_file_manage(maps)
}

func (impl *T_file_manageServiceImpl) CancelUpdateT_file_manage(maps orm.Params) (err error) {
	return
}

var DeleteT_file_manageCompensable = client.Compensable{
	TryMethod:     "TryDeleteT_file_manage",
	ConfirmMethod: "ConfirmDeleteT_file_manage",
	CancelMethod:  "CancelDeleteT_file_manage",
}

func (impl *T_file_manageServiceImpl) TryDeleteT_file_manage(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_file_manageById(params, 0)
	return
}

func (impl *T_file_manageServiceImpl) ConfirmDeleteT_file_manage(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.DeleteT_file_manage(maps)
}

func (impl *T_file_manageServiceImpl) CancelDeleteT_file_manage(maps orm.Params) (err error) {
	return
}
