package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_bal_typeCompensable = client.Compensable{
	TryMethod:     "TryInsertT_bal_type",
	ConfirmMethod: "ConfirmInsertT_bal_type",
	CancelMethod:  "CancelInsertT_bal_type",
}

type T_bal_typeService interface {
	TryInsertT_bal_type(maps orm.Params) error
	ConfirmInsertT_bal_type(maps orm.Params) error
	CancelInsertT_bal_type(maps orm.Params) error
	TryUpdateT_bal_type(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_bal_type(maps orm.Params) error
	CancelUpdateT_bal_type(maps orm.Params) error
}

type T_bal_typeServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_bal_typeServiceImpl) TryInsertT_bal_type(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_bal_type(o, maps)
}

func (impl *T_bal_typeServiceImpl) ConfirmInsertT_bal_type(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_bal_typeTccState(o, maps)
}

func (impl *T_bal_typeServiceImpl) CancelInsertT_bal_type(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_bal_typeTccState(o, maps)
}

//智能查询
func (impl *T_bal_typeServiceImpl) QueryT_bal_type(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_bal_type(params, 0)
}

var UpdateT_bal_typeCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_bal_type",
	ConfirmMethod: "ConfirmUpdateT_bal_type",
	CancelMethod:  "CancelUpdateT_bal_type",
}

func (impl *T_bal_typeServiceImpl) TryUpdateT_bal_type(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_bal_typeById(params, 0)
	return
}

func (impl *T_bal_typeServiceImpl) ConfirmUpdateT_bal_type(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_bal_type(maps)
}

func (impl *T_bal_typeServiceImpl) CancelUpdateT_bal_type(maps orm.Params) (err error) {
	return
}
