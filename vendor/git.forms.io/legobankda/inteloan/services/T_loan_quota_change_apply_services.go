package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_quota_change_applyCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_quota_change_apply",
	ConfirmMethod: "ConfirmInsertT_loan_quota_change_apply",
	CancelMethod:  "CancelInsertT_loan_quota_change_apply",
}

type T_loan_quota_change_applyService interface {
	TryInsertT_loan_quota_change_apply(maps orm.Params) error
	ConfirmInsertT_loan_quota_change_apply(maps orm.Params) error
	CancelInsertT_loan_quota_change_apply(maps orm.Params) error
	TryUpdateT_loan_quota_change_apply(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_quota_change_apply(maps orm.Params) error
	CancelUpdateT_loan_quota_change_apply(maps orm.Params) error
}

type T_loan_quota_change_applyServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_quota_change_applyServiceImpl) TryInsertT_loan_quota_change_apply(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_quota_change_apply(o, maps)
}

func (impl *T_loan_quota_change_applyServiceImpl) ConfirmInsertT_loan_quota_change_apply(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_quota_change_applyTccState(o, maps)
}

func (impl *T_loan_quota_change_applyServiceImpl) CancelInsertT_loan_quota_change_apply(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_quota_change_applyTccState(o, maps)
}

//智能查询
func (impl *T_loan_quota_change_applyServiceImpl) QueryT_loan_quota_change_apply(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_quota_change_apply(params, 0)
}

var UpdateT_loan_quota_change_applyCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_quota_change_apply",
	ConfirmMethod: "ConfirmUpdateT_loan_quota_change_apply",
	CancelMethod:  "CancelUpdateT_loan_quota_change_apply",
}

func (impl *T_loan_quota_change_applyServiceImpl) TryUpdateT_loan_quota_change_apply(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_quota_change_applyById(params, 0)
	return
}

func (impl *T_loan_quota_change_applyServiceImpl) ConfirmUpdateT_loan_quota_change_apply(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_quota_change_apply(maps)
}

func (impl *T_loan_quota_change_applyServiceImpl) CancelUpdateT_loan_quota_change_apply(maps orm.Params) (err error) {
	return
}
