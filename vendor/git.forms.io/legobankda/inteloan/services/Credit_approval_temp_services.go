package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertCredit_approval_tempCompensable = client.Compensable{
	TryMethod:     "TryInsertCredit_approval_temp",
	ConfirmMethod: "ConfirmInsertCredit_approval_temp",
	CancelMethod:  "CancelInsertCredit_approval_temp",
}

type Credit_approval_tempService interface {
	TryInsertCredit_approval_temp(maps orm.Params) error
	ConfirmInsertCredit_approval_temp(maps orm.Params) error
	CancelInsertCredit_approval_temp(maps orm.Params) error
	TryUpdateCredit_approval_temp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateCredit_approval_temp(maps orm.Params) error
	CancelUpdateCredit_approval_temp(maps orm.Params) error
}

type Credit_approval_tempServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *Credit_approval_tempServiceImpl) TryInsertCredit_approval_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertCredit_approval_temp(o, maps)
}

func (impl *Credit_approval_tempServiceImpl) ConfirmInsertCredit_approval_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateCredit_approval_tempTccState(o, maps)
}

func (impl *Credit_approval_tempServiceImpl) CancelInsertCredit_approval_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteCredit_approval_tempTccState(o, maps)
}

//智能查询
func (impl *Credit_approval_tempServiceImpl) QueryCredit_approval_temp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryCredit_approval_temp(params, 0)
}

var UpdateCredit_approval_tempCompensable = client.Compensable{
	TryMethod:     "TryUpdateCredit_approval_temp",
	ConfirmMethod: "ConfirmUpdateCredit_approval_temp",
	CancelMethod:  "CancelUpdateCredit_approval_temp",
}

func (impl *Credit_approval_tempServiceImpl) TryUpdateCredit_approval_temp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryCredit_approval_tempById(params, 0)
	return
}

func (impl *Credit_approval_tempServiceImpl) ConfirmUpdateCredit_approval_temp(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateCredit_approval_temp(maps)
}

func (impl *Credit_approval_tempServiceImpl) CancelUpdateCredit_approval_temp(maps orm.Params) (err error) {
	return
}
