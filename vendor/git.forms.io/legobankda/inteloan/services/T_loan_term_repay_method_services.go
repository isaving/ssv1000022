package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_term_repay_methodCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_term_repay_method",
	ConfirmMethod: "ConfirmInsertT_loan_term_repay_method",
	CancelMethod:  "CancelInsertT_loan_term_repay_method",
}

type T_loan_term_repay_methodService interface {
	TryInsertT_loan_term_repay_method(maps orm.Params) error
	ConfirmInsertT_loan_term_repay_method(maps orm.Params) error
	CancelInsertT_loan_term_repay_method(maps orm.Params) error
	TryUpdateT_loan_term_repay_method(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_term_repay_method(maps orm.Params) error
	CancelUpdateT_loan_term_repay_method(maps orm.Params) error
}

type T_loan_term_repay_methodServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_term_repay_methodServiceImpl) TryInsertT_loan_term_repay_method(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_term_repay_method(o, maps)
}

func (impl *T_loan_term_repay_methodServiceImpl) ConfirmInsertT_loan_term_repay_method(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_term_repay_methodTccState(o, maps)
}

func (impl *T_loan_term_repay_methodServiceImpl) CancelInsertT_loan_term_repay_method(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_term_repay_methodTccState(o, maps)
}

//智能查询
func (impl *T_loan_term_repay_methodServiceImpl) QueryT_loan_term_repay_method(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_term_repay_method(params, 0)
}

var UpdateT_loan_term_repay_methodCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_term_repay_method",
	ConfirmMethod: "ConfirmUpdateT_loan_term_repay_method",
	CancelMethod:  "CancelUpdateT_loan_term_repay_method",
}

func (impl *T_loan_term_repay_methodServiceImpl) TryUpdateT_loan_term_repay_method(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_term_repay_methodById(params, 0)
	return
}

func (impl *T_loan_term_repay_methodServiceImpl) ConfirmUpdateT_loan_term_repay_method(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_term_repay_method(maps)
}

func (impl *T_loan_term_repay_methodServiceImpl) CancelUpdateT_loan_term_repay_method(maps orm.Params) (err error) {
	return
}
