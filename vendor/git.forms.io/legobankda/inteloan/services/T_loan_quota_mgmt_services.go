package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_quota_mgmtCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_quota_mgmt",
	ConfirmMethod: "ConfirmInsertT_loan_quota_mgmt",
	CancelMethod:  "CancelInsertT_loan_quota_mgmt",
}

type T_loan_quota_mgmtService interface {
	TryInsertT_loan_quota_mgmt(maps orm.Params) error
	ConfirmInsertT_loan_quota_mgmt(maps orm.Params) error
	CancelInsertT_loan_quota_mgmt(maps orm.Params) error
	TryUpdateT_loan_quota_mgmt(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_quota_mgmt(maps orm.Params) error
	CancelUpdateT_loan_quota_mgmt(maps orm.Params) error
}

type T_loan_quota_mgmtServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_quota_mgmtServiceImpl) TryInsertT_loan_quota_mgmt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_quota_mgmt(o, maps)
}

func (impl *T_loan_quota_mgmtServiceImpl) ConfirmInsertT_loan_quota_mgmt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_quota_mgmtTccState(o, maps)
}

func (impl *T_loan_quota_mgmtServiceImpl) CancelInsertT_loan_quota_mgmt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_quota_mgmtTccState(o, maps)
}

//智能查询
func (impl *T_loan_quota_mgmtServiceImpl) QueryT_loan_quota_mgmt(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_quota_mgmt(params, 0)
}

var UpdateT_loan_quota_mgmtCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_quota_mgmt",
	ConfirmMethod: "ConfirmUpdateT_loan_quota_mgmt",
	CancelMethod:  "CancelUpdateT_loan_quota_mgmt",
}

func (impl *T_loan_quota_mgmtServiceImpl) TryUpdateT_loan_quota_mgmt(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_quota_mgmtById(params, 0)
	return
}

func (impl *T_loan_quota_mgmtServiceImpl) ConfirmUpdateT_loan_quota_mgmt(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_quota_mgmt(maps)
}

func (impl *T_loan_quota_mgmtServiceImpl) CancelUpdateT_loan_quota_mgmt(maps orm.Params) (err error) {
	return
}
