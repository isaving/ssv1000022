package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_jnl_entry_sumCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_jnl_entry_sum",
	ConfirmMethod: "ConfirmInsertT_ae_jnl_entry_sum",
	CancelMethod:  "CancelInsertT_ae_jnl_entry_sum",
}

type T_ae_jnl_entry_sumService interface {
	TryInsertT_ae_jnl_entry_sum(maps orm.Params) error
	ConfirmInsertT_ae_jnl_entry_sum(maps orm.Params) error
	CancelInsertT_ae_jnl_entry_sum(maps orm.Params) error
	TryUpdateT_ae_jnl_entry_sum(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_jnl_entry_sum(maps orm.Params) error
	CancelUpdateT_ae_jnl_entry_sum(maps orm.Params) error
}

type T_ae_jnl_entry_sumServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_jnl_entry_sumServiceImpl) TryInsertT_ae_jnl_entry_sum(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_jnl_entry_sum(o, maps)
}

func (impl *T_ae_jnl_entry_sumServiceImpl) ConfirmInsertT_ae_jnl_entry_sum(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_jnl_entry_sumTccState(o, maps)
}

func (impl *T_ae_jnl_entry_sumServiceImpl) CancelInsertT_ae_jnl_entry_sum(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_jnl_entry_sumTccState(o, maps)
}

//智能查询
func (impl *T_ae_jnl_entry_sumServiceImpl) QueryT_ae_jnl_entry_sum(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_jnl_entry_sum(params, 0)
}

var UpdateT_ae_jnl_entry_sumCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_jnl_entry_sum",
	ConfirmMethod: "ConfirmUpdateT_ae_jnl_entry_sum",
	CancelMethod:  "CancelUpdateT_ae_jnl_entry_sum",
}

func (impl *T_ae_jnl_entry_sumServiceImpl) TryUpdateT_ae_jnl_entry_sum(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_jnl_entry_sumById(params, 0)
	return
}

func (impl *T_ae_jnl_entry_sumServiceImpl) ConfirmUpdateT_ae_jnl_entry_sum(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_jnl_entry_sum(maps)
}

func (impl *T_ae_jnl_entry_sumServiceImpl) CancelUpdateT_ae_jnl_entry_sum(maps orm.Params) (err error) {
	return
}
