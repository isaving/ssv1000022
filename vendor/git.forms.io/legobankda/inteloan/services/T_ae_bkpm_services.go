package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_bkpmCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_bkpm",
	ConfirmMethod: "ConfirmInsertT_ae_bkpm",
	CancelMethod:  "CancelInsertT_ae_bkpm",
}

type T_ae_bkpmService interface {
	TryInsertT_ae_bkpm(maps orm.Params) error
	ConfirmInsertT_ae_bkpm(maps orm.Params) error
	CancelInsertT_ae_bkpm(maps orm.Params) error
	TryUpdateT_ae_bkpm(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_bkpm(maps orm.Params) error
	CancelUpdateT_ae_bkpm(maps orm.Params) error
}

type T_ae_bkpmServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_bkpmServiceImpl) TryInsertT_ae_bkpm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_bkpm(o, maps)
}

func (impl *T_ae_bkpmServiceImpl) ConfirmInsertT_ae_bkpm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_bkpmTccState(o, maps)
}

func (impl *T_ae_bkpmServiceImpl) CancelInsertT_ae_bkpm(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_bkpmTccState(o, maps)
}

//智能查询
func (impl *T_ae_bkpmServiceImpl) QueryT_ae_bkpm(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_bkpm(params, 0)
}

var UpdateT_ae_bkpmCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_bkpm",
	ConfirmMethod: "ConfirmUpdateT_ae_bkpm",
	CancelMethod:  "CancelUpdateT_ae_bkpm",
}

func (impl *T_ae_bkpmServiceImpl) TryUpdateT_ae_bkpm(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_bkpmById(params, 0)
	return
}

func (impl *T_ae_bkpmServiceImpl) ConfirmUpdateT_ae_bkpm(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_bkpm(maps)
}

func (impl *T_ae_bkpmServiceImpl) CancelUpdateT_ae_bkpm(maps orm.Params) (err error) {
	return
}
