package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_artif_credit_ele_paramCompensable = client.Compensable{
	TryMethod:     "TryInsertT_artif_credit_ele_param",
	ConfirmMethod: "ConfirmInsertT_artif_credit_ele_param",
	CancelMethod:  "CancelInsertT_artif_credit_ele_param",
}

type T_artif_credit_ele_paramService interface {
	TryInsertT_artif_credit_ele_param(maps orm.Params) error
	ConfirmInsertT_artif_credit_ele_param(maps orm.Params) error
	CancelInsertT_artif_credit_ele_param(maps orm.Params) error
	TryUpdateT_artif_credit_ele_param(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_artif_credit_ele_param(maps orm.Params) error
	CancelUpdateT_artif_credit_ele_param(maps orm.Params) error
}

type T_artif_credit_ele_paramServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_artif_credit_ele_paramServiceImpl) TryInsertT_artif_credit_ele_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_artif_credit_ele_param(o, maps)
}

func (impl *T_artif_credit_ele_paramServiceImpl) ConfirmInsertT_artif_credit_ele_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_artif_credit_ele_paramTccState(o, maps)
}

func (impl *T_artif_credit_ele_paramServiceImpl) CancelInsertT_artif_credit_ele_param(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_artif_credit_ele_paramTccState(o, maps)
}

//智能查询
func (impl *T_artif_credit_ele_paramServiceImpl) QueryT_artif_credit_ele_param(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_artif_credit_ele_param(params, 0)
}

var UpdateT_artif_credit_ele_paramCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_artif_credit_ele_param",
	ConfirmMethod: "ConfirmUpdateT_artif_credit_ele_param",
	CancelMethod:  "CancelUpdateT_artif_credit_ele_param",
}

func (impl *T_artif_credit_ele_paramServiceImpl) TryUpdateT_artif_credit_ele_param(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_artif_credit_ele_paramById(params, 0)
	return
}

func (impl *T_artif_credit_ele_paramServiceImpl) ConfirmUpdateT_artif_credit_ele_param(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_artif_credit_ele_param(maps)
}

func (impl *T_artif_credit_ele_paramServiceImpl) CancelUpdateT_artif_credit_ele_param(maps orm.Params) (err error) {
	return
}
