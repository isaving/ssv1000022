package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_common_roleCompensable = client.Compensable{
	TryMethod:     "TryInsertT_common_role",
	ConfirmMethod: "ConfirmInsertT_common_role",
	CancelMethod:  "CancelInsertT_common_role",
}

type T_common_roleService interface {
	TryInsertT_common_role(maps orm.Params) error
	ConfirmInsertT_common_role(maps orm.Params) error
	CancelInsertT_common_role(maps orm.Params) error
	TryUpdateT_common_role(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_common_role(maps orm.Params) error
	CancelUpdateT_common_role(maps orm.Params) error
}

type T_common_roleServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_common_roleServiceImpl) TryInsertT_common_role(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_common_role(o, maps)
}

func (impl *T_common_roleServiceImpl) ConfirmInsertT_common_role(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_common_roleTccState(o, maps)
}

func (impl *T_common_roleServiceImpl) CancelInsertT_common_role(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_common_roleTccState(o, maps)
}

//智能查询
func (impl *T_common_roleServiceImpl) QueryT_common_role(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_common_role(params, 0)
}

var UpdateT_common_roleCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_common_role",
	ConfirmMethod: "ConfirmUpdateT_common_role",
	CancelMethod:  "CancelUpdateT_common_role",
}

func (impl *T_common_roleServiceImpl) TryUpdateT_common_role(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_common_roleById(params, 0)
	return
}

func (impl *T_common_roleServiceImpl) ConfirmUpdateT_common_role(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_common_role(maps)
}

func (impl *T_common_roleServiceImpl) CancelUpdateT_common_role(maps orm.Params) (err error) {
	return
}
