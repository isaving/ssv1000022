package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_special_change_recordCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_special_change_record",
	ConfirmMethod: "ConfirmInsertT_loan_special_change_record",
	CancelMethod:  "CancelInsertT_loan_special_change_record",
}

type T_loan_special_change_recordService interface {
	TryInsertT_loan_special_change_record(maps orm.Params) error
	ConfirmInsertT_loan_special_change_record(maps orm.Params) error
	CancelInsertT_loan_special_change_record(maps orm.Params) error
	TryUpdateT_loan_special_change_record(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_special_change_record(maps orm.Params) error
	CancelUpdateT_loan_special_change_record(maps orm.Params) error
}

type T_loan_special_change_recordServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_special_change_recordServiceImpl) TryInsertT_loan_special_change_record(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_special_change_record(o, maps)
}

func (impl *T_loan_special_change_recordServiceImpl) ConfirmInsertT_loan_special_change_record(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_special_change_recordTccState(o, maps)
}

func (impl *T_loan_special_change_recordServiceImpl) CancelInsertT_loan_special_change_record(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_special_change_recordTccState(o, maps)
}

//智能查询
func (impl *T_loan_special_change_recordServiceImpl) QueryT_loan_special_change_record(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_special_change_record(params, 0)
}

var UpdateT_loan_special_change_recordCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_special_change_record",
	ConfirmMethod: "ConfirmUpdateT_loan_special_change_record",
	CancelMethod:  "CancelUpdateT_loan_special_change_record",
}

func (impl *T_loan_special_change_recordServiceImpl) TryUpdateT_loan_special_change_record(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_special_change_recordById(params, 0)
	return
}

func (impl *T_loan_special_change_recordServiceImpl) ConfirmUpdateT_loan_special_change_record(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_special_change_record(maps)
}

func (impl *T_loan_special_change_recordServiceImpl) CancelUpdateT_loan_special_change_record(maps orm.Params) (err error) {
	return
}
