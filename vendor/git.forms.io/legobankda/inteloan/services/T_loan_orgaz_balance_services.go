package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_orgaz_balanceCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_orgaz_balance",
	ConfirmMethod: "ConfirmInsertT_loan_orgaz_balance",
	CancelMethod:  "CancelInsertT_loan_orgaz_balance",
}

type T_loan_orgaz_balanceService interface {
	TryInsertT_loan_orgaz_balance(maps orm.Params) error
	ConfirmInsertT_loan_orgaz_balance(maps orm.Params) error
	CancelInsertT_loan_orgaz_balance(maps orm.Params) error
	TryUpdateT_loan_orgaz_balance(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_orgaz_balance(maps orm.Params) error
	CancelUpdateT_loan_orgaz_balance(maps orm.Params) error
}

type T_loan_orgaz_balanceServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_orgaz_balanceServiceImpl) TryInsertT_loan_orgaz_balance(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_orgaz_balance(o, maps)
}

func (impl *T_loan_orgaz_balanceServiceImpl) ConfirmInsertT_loan_orgaz_balance(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_orgaz_balanceTccState(o, maps)
}

func (impl *T_loan_orgaz_balanceServiceImpl) CancelInsertT_loan_orgaz_balance(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_orgaz_balanceTccState(o, maps)
}

//智能查询
func (impl *T_loan_orgaz_balanceServiceImpl) QueryT_loan_orgaz_balance(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_orgaz_balance(params, 0)
}

var UpdateT_loan_orgaz_balanceCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_orgaz_balance",
	ConfirmMethod: "ConfirmUpdateT_loan_orgaz_balance",
	CancelMethod:  "CancelUpdateT_loan_orgaz_balance",
}

func (impl *T_loan_orgaz_balanceServiceImpl) TryUpdateT_loan_orgaz_balance(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_orgaz_balanceById(params, 0)
	return
}

func (impl *T_loan_orgaz_balanceServiceImpl) ConfirmUpdateT_loan_orgaz_balance(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_orgaz_balance(maps)
}

func (impl *T_loan_orgaz_balanceServiceImpl) CancelUpdateT_loan_orgaz_balance(maps orm.Params) (err error) {
	return
}
