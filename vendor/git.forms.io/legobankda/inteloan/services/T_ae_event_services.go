package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_ae_eventCompensable = client.Compensable{
	TryMethod:     "TryInsertT_ae_event",
	ConfirmMethod: "ConfirmInsertT_ae_event",
	CancelMethod:  "CancelInsertT_ae_event",
}

type T_ae_eventService interface {
	TryInsertT_ae_event(maps orm.Params) error
	ConfirmInsertT_ae_event(maps orm.Params) error
	CancelInsertT_ae_event(maps orm.Params) error
	TryUpdateT_ae_event(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_ae_event(maps orm.Params) error
	CancelUpdateT_ae_event(maps orm.Params) error
}

type T_ae_eventServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_ae_eventServiceImpl) TryInsertT_ae_event(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_ae_event(o, maps)
}

func (impl *T_ae_eventServiceImpl) ConfirmInsertT_ae_event(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_ae_eventTccState(o, maps)
}

func (impl *T_ae_eventServiceImpl) CancelInsertT_ae_event(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_ae_eventTccState(o, maps)
}

//智能查询
func (impl *T_ae_eventServiceImpl) QueryT_ae_event(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_ae_event(params, 0)
}

var UpdateT_ae_eventCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_ae_event",
	ConfirmMethod: "ConfirmUpdateT_ae_event",
	CancelMethod:  "CancelUpdateT_ae_event",
}

func (impl *T_ae_eventServiceImpl) TryUpdateT_ae_event(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_ae_eventById(params, 0)
	return
}

func (impl *T_ae_eventServiceImpl) ConfirmUpdateT_ae_event(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_ae_event(maps)
}

func (impl *T_ae_eventServiceImpl) CancelUpdateT_ae_event(maps orm.Params) (err error) {
	return
}
