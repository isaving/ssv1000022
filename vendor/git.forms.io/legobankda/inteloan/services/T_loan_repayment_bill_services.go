package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_repayment_billCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_repayment_bill",
	ConfirmMethod: "ConfirmInsertT_loan_repayment_bill",
	CancelMethod:  "CancelInsertT_loan_repayment_bill",
}

type T_loan_repayment_billService interface {
	TryInsertT_loan_repayment_bill(maps orm.Params) error
	ConfirmInsertT_loan_repayment_bill(maps orm.Params) error
	CancelInsertT_loan_repayment_bill(maps orm.Params) error
	TryUpdateT_loan_repayment_bill(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_repayment_bill(maps orm.Params) error
	CancelUpdateT_loan_repayment_bill(maps orm.Params) error
}

type T_loan_repayment_billServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_repayment_billServiceImpl) TryInsertT_loan_repayment_bill(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_repayment_bill(o, maps)
}

func (impl *T_loan_repayment_billServiceImpl) ConfirmInsertT_loan_repayment_bill(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_repayment_billTccState(o, maps)
}

func (impl *T_loan_repayment_billServiceImpl) CancelInsertT_loan_repayment_bill(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_repayment_billTccState(o, maps)
}

//智能查询
func (impl *T_loan_repayment_billServiceImpl) QueryT_loan_repayment_bill(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_repayment_bill(params, 0)
}

var UpdateT_loan_repayment_billCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_repayment_bill",
	ConfirmMethod: "ConfirmUpdateT_loan_repayment_bill",
	CancelMethod:  "CancelUpdateT_loan_repayment_bill",
}

func (impl *T_loan_repayment_billServiceImpl) TryUpdateT_loan_repayment_bill(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_repayment_billById(params, 0)
	return
}

func (impl *T_loan_repayment_billServiceImpl) ConfirmUpdateT_loan_repayment_bill(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_repayment_bill(maps)
}

func (impl *T_loan_repayment_billServiceImpl) CancelUpdateT_loan_repayment_bill(maps orm.Params) (err error) {
	return
}
