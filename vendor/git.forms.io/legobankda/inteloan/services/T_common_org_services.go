package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_common_orgCompensable = client.Compensable{
	TryMethod:     "TryInsertT_common_org",
	ConfirmMethod: "ConfirmInsertT_common_org",
	CancelMethod:  "CancelInsertT_common_org",
}

type T_common_orgService interface {
	TryInsertT_common_org(maps orm.Params) error
	ConfirmInsertT_common_org(maps orm.Params) error
	CancelInsertT_common_org(maps orm.Params) error
	TryUpdateT_common_org(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_common_org(maps orm.Params) error
	CancelUpdateT_common_org(maps orm.Params) error
}

type T_common_orgServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_common_orgServiceImpl) TryInsertT_common_org(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_common_org(o, maps)
}

func (impl *T_common_orgServiceImpl) ConfirmInsertT_common_org(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_common_orgTccState(o, maps)
}

func (impl *T_common_orgServiceImpl) CancelInsertT_common_org(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_common_orgTccState(o, maps)
}

//智能查询
func (impl *T_common_orgServiceImpl) QueryT_common_org(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_common_org(params, 0)
}

var UpdateT_common_orgCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_common_org",
	ConfirmMethod: "ConfirmUpdateT_common_org",
	CancelMethod:  "CancelUpdateT_common_org",
}

func (impl *T_common_orgServiceImpl) TryUpdateT_common_org(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_common_orgById(params, 0)
	return
}

func (impl *T_common_orgServiceImpl) ConfirmUpdateT_common_org(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_common_org(maps)
}

func (impl *T_common_orgServiceImpl) CancelUpdateT_common_org(maps orm.Params) (err error) {
	return
}
