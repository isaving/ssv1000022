package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_op_manageCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_op_manage",
	ConfirmMethod: "ConfirmInsertT_loan_op_manage",
	CancelMethod:  "CancelInsertT_loan_op_manage",
}

type T_loan_op_manageService interface {
	TryInsertT_loan_op_manage(maps orm.Params) error
	ConfirmInsertT_loan_op_manage(maps orm.Params) error
	CancelInsertT_loan_op_manage(maps orm.Params) error
	TryUpdateT_loan_op_manage(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_op_manage(maps orm.Params) error
	CancelUpdateT_loan_op_manage(maps orm.Params) error
}

type T_loan_op_manageServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_op_manageServiceImpl) TryInsertT_loan_op_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_op_manage(o, maps)
}

func (impl *T_loan_op_manageServiceImpl) ConfirmInsertT_loan_op_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_op_manageTccState(o, maps)
}

func (impl *T_loan_op_manageServiceImpl) CancelInsertT_loan_op_manage(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_op_manageTccState(o, maps)
}

//智能查询
func (impl *T_loan_op_manageServiceImpl) QueryT_loan_op_manage(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_op_manage(params, 0)
}

var UpdateT_loan_op_manageCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_op_manage",
	ConfirmMethod: "ConfirmUpdateT_loan_op_manage",
	CancelMethod:  "CancelUpdateT_loan_op_manage",
}

func (impl *T_loan_op_manageServiceImpl) TryUpdateT_loan_op_manage(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_op_manageById(params, 0)
	return
}

func (impl *T_loan_op_manageServiceImpl) ConfirmUpdateT_loan_op_manage(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_op_manage(maps)
}

func (impl *T_loan_op_manageServiceImpl) CancelUpdateT_loan_op_manage(maps orm.Params) (err error) {
	return
}
