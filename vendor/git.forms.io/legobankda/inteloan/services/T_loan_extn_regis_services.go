package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_extn_regisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_extn_regis",
	ConfirmMethod: "ConfirmInsertT_loan_extn_regis",
	CancelMethod:  "CancelInsertT_loan_extn_regis",
}

type T_loan_extn_regisService interface {
	TryInsertT_loan_extn_regis(maps orm.Params) error
	ConfirmInsertT_loan_extn_regis(maps orm.Params) error
	CancelInsertT_loan_extn_regis(maps orm.Params) error
	TryUpdateT_loan_extn_regis(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_extn_regis(maps orm.Params) error
	CancelUpdateT_loan_extn_regis(maps orm.Params) error
}

type T_loan_extn_regisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_extn_regisServiceImpl) TryInsertT_loan_extn_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_extn_regis(o, maps)
}

func (impl *T_loan_extn_regisServiceImpl) ConfirmInsertT_loan_extn_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_extn_regisTccState(o, maps)
}

func (impl *T_loan_extn_regisServiceImpl) CancelInsertT_loan_extn_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_extn_regisTccState(o, maps)
}

//智能查询
func (impl *T_loan_extn_regisServiceImpl) QueryT_loan_extn_regis(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_extn_regis(params, 0)
}

var UpdateT_loan_extn_regisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_extn_regis",
	ConfirmMethod: "ConfirmUpdateT_loan_extn_regis",
	CancelMethod:  "CancelUpdateT_loan_extn_regis",
}

func (impl *T_loan_extn_regisServiceImpl) TryUpdateT_loan_extn_regis(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_extn_regisById(params, 0)
	return
}

func (impl *T_loan_extn_regisServiceImpl) ConfirmUpdateT_loan_extn_regis(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_extn_regis(maps)
}

func (impl *T_loan_extn_regisServiceImpl) CancelUpdateT_loan_extn_regis(maps orm.Params) (err error) {
	return
}
