package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_fin_txn_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_fin_txn_flow",
	ConfirmMethod: "ConfirmInsertT_fin_txn_flow",
	CancelMethod:  "CancelInsertT_fin_txn_flow",
}

type T_fin_txn_flowService interface {
	TryInsertT_fin_txn_flow(maps orm.Params) error
	ConfirmInsertT_fin_txn_flow(maps orm.Params) error
	CancelInsertT_fin_txn_flow(maps orm.Params) error
	TryUpdateT_fin_txn_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_fin_txn_flow(maps orm.Params) error
	CancelUpdateT_fin_txn_flow(maps orm.Params) error
}

type T_fin_txn_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_fin_txn_flowServiceImpl) TryInsertT_fin_txn_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_fin_txn_flow(o, maps)
}

func (impl *T_fin_txn_flowServiceImpl) ConfirmInsertT_fin_txn_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_fin_txn_flowTccState(o, maps)
}

func (impl *T_fin_txn_flowServiceImpl) CancelInsertT_fin_txn_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_fin_txn_flowTccState(o, maps)
}

//智能查询
func (impl *T_fin_txn_flowServiceImpl) QueryT_fin_txn_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_fin_txn_flow(params, 0)
}

var UpdateT_fin_txn_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_fin_txn_flow",
	ConfirmMethod: "ConfirmUpdateT_fin_txn_flow",
	CancelMethod:  "CancelUpdateT_fin_txn_flow",
}

func (impl *T_fin_txn_flowServiceImpl) TryUpdateT_fin_txn_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_fin_txn_flowById(params, 0)
	return
}

func (impl *T_fin_txn_flowServiceImpl) ConfirmUpdateT_fin_txn_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_fin_txn_flow(maps)
}

func (impl *T_fin_txn_flowServiceImpl) CancelUpdateT_fin_txn_flow(maps orm.Params) (err error) {
	return
}
