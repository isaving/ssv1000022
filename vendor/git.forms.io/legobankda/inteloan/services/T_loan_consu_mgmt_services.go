package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_consu_mgmtCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_consu_mgmt",
	ConfirmMethod: "ConfirmInsertT_loan_consu_mgmt",
	CancelMethod:  "CancelInsertT_loan_consu_mgmt",
}

type T_loan_consu_mgmtService interface {
	TryInsertT_loan_consu_mgmt(maps orm.Params) error
	ConfirmInsertT_loan_consu_mgmt(maps orm.Params) error
	CancelInsertT_loan_consu_mgmt(maps orm.Params) error
	TryUpdateT_loan_consu_mgmt(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_consu_mgmt(maps orm.Params) error
	CancelUpdateT_loan_consu_mgmt(maps orm.Params) error
}

type T_loan_consu_mgmtServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_consu_mgmtServiceImpl) TryInsertT_loan_consu_mgmt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_consu_mgmt(o, maps)
}

func (impl *T_loan_consu_mgmtServiceImpl) ConfirmInsertT_loan_consu_mgmt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_consu_mgmtTccState(o, maps)
}

func (impl *T_loan_consu_mgmtServiceImpl) CancelInsertT_loan_consu_mgmt(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_consu_mgmtTccState(o, maps)
}

//智能查询
func (impl *T_loan_consu_mgmtServiceImpl) QueryT_loan_consu_mgmt(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_consu_mgmt(params, 0)
}

var UpdateT_loan_consu_mgmtCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_consu_mgmt",
	ConfirmMethod: "ConfirmUpdateT_loan_consu_mgmt",
	CancelMethod:  "CancelUpdateT_loan_consu_mgmt",
}

func (impl *T_loan_consu_mgmtServiceImpl) TryUpdateT_loan_consu_mgmt(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_consu_mgmtById(params, 0)
	return
}

func (impl *T_loan_consu_mgmtServiceImpl) ConfirmUpdateT_loan_consu_mgmt(maps orm.Params) (err error) {
	maps["LastMaintDate"] = time.Now().Format(constant.DATE_FORMAT)
	maps["LastMaintTime"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_consu_mgmt(maps)
}

func (impl *T_loan_consu_mgmtServiceImpl) CancelUpdateT_loan_consu_mgmt(maps orm.Params) (err error) {
	return
}
