package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_loan_approval_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_loan_approval_flow",
	ConfirmMethod: "ConfirmInsertT_loan_loan_approval_flow",
	CancelMethod:  "CancelInsertT_loan_loan_approval_flow",
}

type T_loan_loan_approval_flowService interface {
	TryInsertT_loan_loan_approval_flow(maps orm.Params) error
	ConfirmInsertT_loan_loan_approval_flow(maps orm.Params) error
	CancelInsertT_loan_loan_approval_flow(maps orm.Params) error
	TryUpdateT_loan_loan_approval_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_loan_approval_flow(maps orm.Params) error
	CancelUpdateT_loan_loan_approval_flow(maps orm.Params) error
}

type T_loan_loan_approval_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_loan_approval_flowServiceImpl) TryInsertT_loan_loan_approval_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_loan_approval_flow(o, maps)
}

func (impl *T_loan_loan_approval_flowServiceImpl) ConfirmInsertT_loan_loan_approval_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_loan_approval_flowTccState(o, maps)
}

func (impl *T_loan_loan_approval_flowServiceImpl) CancelInsertT_loan_loan_approval_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_loan_approval_flowTccState(o, maps)
}

//智能查询
func (impl *T_loan_loan_approval_flowServiceImpl) QueryT_loan_loan_approval_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_loan_approval_flow(params, 0)
}

var UpdateT_loan_loan_approval_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_loan_approval_flow",
	ConfirmMethod: "ConfirmUpdateT_loan_loan_approval_flow",
	CancelMethod:  "CancelUpdateT_loan_loan_approval_flow",
}

func (impl *T_loan_loan_approval_flowServiceImpl) TryUpdateT_loan_loan_approval_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_loan_approval_flowById(params, 0)
	return
}

func (impl *T_loan_loan_approval_flowServiceImpl) ConfirmUpdateT_loan_loan_approval_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_loan_approval_flow(maps)
}

func (impl *T_loan_loan_approval_flowServiceImpl) CancelUpdateT_loan_loan_approval_flow(maps orm.Params) (err error) {
	return
}
