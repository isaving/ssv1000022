package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intrt_acct_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intrt_acct_info",
	ConfirmMethod: "ConfirmInsertT_intrt_acct_info",
	CancelMethod:  "CancelInsertT_intrt_acct_info",
}

type T_intrt_acct_infoService interface {
	TryInsertT_intrt_acct_info(maps orm.Params) error
	ConfirmInsertT_intrt_acct_info(maps orm.Params) error
	CancelInsertT_intrt_acct_info(maps orm.Params) error
	TryUpdateT_intrt_acct_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intrt_acct_info(maps orm.Params) error
	CancelUpdateT_intrt_acct_info(maps orm.Params) error
}

type T_intrt_acct_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intrt_acct_infoServiceImpl) TryInsertT_intrt_acct_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intrt_acct_info(o, maps)
}

func (impl *T_intrt_acct_infoServiceImpl) ConfirmInsertT_intrt_acct_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intrt_acct_infoTccState(o, maps)
}

func (impl *T_intrt_acct_infoServiceImpl) CancelInsertT_intrt_acct_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intrt_acct_infoTccState(o, maps)
}

//智能查询
func (impl *T_intrt_acct_infoServiceImpl) QueryT_intrt_acct_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intrt_acct_info(params, 0)
}

var UpdateT_intrt_acct_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intrt_acct_info",
	ConfirmMethod: "ConfirmUpdateT_intrt_acct_info",
	CancelMethod:  "CancelUpdateT_intrt_acct_info",
}

func (impl *T_intrt_acct_infoServiceImpl) TryUpdateT_intrt_acct_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intrt_acct_infoById(params, 0)
	return
}

func (impl *T_intrt_acct_infoServiceImpl) ConfirmUpdateT_intrt_acct_info(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intrt_acct_info(maps)
}

func (impl *T_intrt_acct_infoServiceImpl) CancelUpdateT_intrt_acct_info(maps orm.Params) (err error) {
	return
}
