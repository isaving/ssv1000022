package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_org_orgi_busi_paraCompensable = client.Compensable{
	TryMethod:     "TryInsertT_org_orgi_busi_para",
	ConfirmMethod: "ConfirmInsertT_org_orgi_busi_para",
	CancelMethod:  "CancelInsertT_org_orgi_busi_para",
}

type T_org_orgi_busi_paraService interface {
	TryInsertT_org_orgi_busi_para(maps orm.Params) error
	ConfirmInsertT_org_orgi_busi_para(maps orm.Params) error
	CancelInsertT_org_orgi_busi_para(maps orm.Params) error
	TryUpdateT_org_orgi_busi_para(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_org_orgi_busi_para(maps orm.Params) error
	CancelUpdateT_org_orgi_busi_para(maps orm.Params) error
}

type T_org_orgi_busi_paraServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_org_orgi_busi_paraServiceImpl) TryInsertT_org_orgi_busi_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_org_orgi_busi_para(o, maps)
}

func (impl *T_org_orgi_busi_paraServiceImpl) ConfirmInsertT_org_orgi_busi_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_org_orgi_busi_paraTccState(o, maps)
}

func (impl *T_org_orgi_busi_paraServiceImpl) CancelInsertT_org_orgi_busi_para(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_org_orgi_busi_paraTccState(o, maps)
}

//智能查询
func (impl *T_org_orgi_busi_paraServiceImpl) QueryT_org_orgi_busi_para(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_org_orgi_busi_para(params, 0)
}

var UpdateT_org_orgi_busi_paraCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_org_orgi_busi_para",
	ConfirmMethod: "ConfirmUpdateT_org_orgi_busi_para",
	CancelMethod:  "CancelUpdateT_org_orgi_busi_para",
}

func (impl *T_org_orgi_busi_paraServiceImpl) TryUpdateT_org_orgi_busi_para(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_org_orgi_busi_paraById(params, 0)
	return
}

func (impl *T_org_orgi_busi_paraServiceImpl) ConfirmUpdateT_org_orgi_busi_para(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_org_orgi_busi_para(maps)
}

func (impl *T_org_orgi_busi_paraServiceImpl) CancelUpdateT_org_orgi_busi_para(maps orm.Params) (err error) {
	return
}
