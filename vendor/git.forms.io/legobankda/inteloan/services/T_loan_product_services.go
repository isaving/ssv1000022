package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_productCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_product",
	ConfirmMethod: "ConfirmInsertT_loan_product",
	CancelMethod:  "CancelInsertT_loan_product",
}

type T_loan_productService interface {
	TryInsertT_loan_product(maps orm.Params) error
	ConfirmInsertT_loan_product(maps orm.Params) error
	CancelInsertT_loan_product(maps orm.Params) error
	TryUpdateT_loan_product(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_product(maps orm.Params) error
	CancelUpdateT_loan_product(maps orm.Params) error
}

type T_loan_productServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_productServiceImpl) TryInsertT_loan_product(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_product(o, maps)
}

func (impl *T_loan_productServiceImpl) ConfirmInsertT_loan_product(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_productTccState(o, maps)
}

func (impl *T_loan_productServiceImpl) CancelInsertT_loan_product(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_productTccState(o, maps)
}

//智能查询
func (impl *T_loan_productServiceImpl) QueryT_loan_product(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_product(params, 0)
}

var UpdateT_loan_productCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_product",
	ConfirmMethod: "ConfirmUpdateT_loan_product",
	CancelMethod:  "CancelUpdateT_loan_product",
}

func (impl *T_loan_productServiceImpl) TryUpdateT_loan_product(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_productById(params, 0)
	return
}

func (impl *T_loan_productServiceImpl) ConfirmUpdateT_loan_product(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_product(maps)
}

func (impl *T_loan_productServiceImpl) CancelUpdateT_loan_product(maps orm.Params) (err error) {
	return
}
