package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sms_sending_detailsCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sms_sending_details",
	ConfirmMethod: "ConfirmInsertT_sms_sending_details",
	CancelMethod:  "CancelInsertT_sms_sending_details",
}

type T_sms_sending_detailsService interface {
	TryInsertT_sms_sending_details(maps orm.Params) error
	ConfirmInsertT_sms_sending_details(maps orm.Params) error
	CancelInsertT_sms_sending_details(maps orm.Params) error
	TryUpdateT_sms_sending_details(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sms_sending_details(maps orm.Params) error
	CancelUpdateT_sms_sending_details(maps orm.Params) error
}

type T_sms_sending_detailsServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sms_sending_detailsServiceImpl) TryInsertT_sms_sending_details(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sms_sending_details(o, maps)
}

func (impl *T_sms_sending_detailsServiceImpl) ConfirmInsertT_sms_sending_details(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sms_sending_detailsTccState(o, maps)
}

func (impl *T_sms_sending_detailsServiceImpl) CancelInsertT_sms_sending_details(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sms_sending_detailsTccState(o, maps)
}

//智能查询
func (impl *T_sms_sending_detailsServiceImpl) QueryT_sms_sending_details(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sms_sending_details(params, 0)
}

var UpdateT_sms_sending_detailsCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sms_sending_details",
	ConfirmMethod: "ConfirmUpdateT_sms_sending_details",
	CancelMethod:  "CancelUpdateT_sms_sending_details",
}

func (impl *T_sms_sending_detailsServiceImpl) TryUpdateT_sms_sending_details(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sms_sending_detailsById(params, 0)
	return
}

func (impl *T_sms_sending_detailsServiceImpl) ConfirmUpdateT_sms_sending_details(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sms_sending_details(maps)
}

func (impl *T_sms_sending_detailsServiceImpl) CancelUpdateT_sms_sending_details(maps orm.Params) (err error) {
	return
}
