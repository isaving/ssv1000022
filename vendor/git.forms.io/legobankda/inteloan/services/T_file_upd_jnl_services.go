package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_file_upd_jnlCompensable = client.Compensable{
	TryMethod:     "TryInsertT_file_upd_jnl",
	ConfirmMethod: "ConfirmInsertT_file_upd_jnl",
	CancelMethod:  "CancelInsertT_file_upd_jnl",
}

type T_file_upd_jnlService interface {
	TryInsertT_file_upd_jnl(maps orm.Params) error
	ConfirmInsertT_file_upd_jnl(maps orm.Params) error
	CancelInsertT_file_upd_jnl(maps orm.Params) error
	TryUpdateT_file_upd_jnl(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_file_upd_jnl(maps orm.Params) error
	CancelUpdateT_file_upd_jnl(maps orm.Params) error
}

type T_file_upd_jnlServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_file_upd_jnlServiceImpl) TryInsertT_file_upd_jnl(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_file_upd_jnl(o, maps)
}

func (impl *T_file_upd_jnlServiceImpl) ConfirmInsertT_file_upd_jnl(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_file_upd_jnlTccState(o, maps)
}

func (impl *T_file_upd_jnlServiceImpl) CancelInsertT_file_upd_jnl(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_file_upd_jnlTccState(o, maps)
}

//智能查询
func (impl *T_file_upd_jnlServiceImpl) QueryT_file_upd_jnl(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_file_upd_jnl(params, 0)
}

var UpdateT_file_upd_jnlCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_file_upd_jnl",
	ConfirmMethod: "ConfirmUpdateT_file_upd_jnl",
	CancelMethod:  "CancelUpdateT_file_upd_jnl",
}

func (impl *T_file_upd_jnlServiceImpl) TryUpdateT_file_upd_jnl(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_file_upd_jnlById(params, 0)
	return
}

func (impl *T_file_upd_jnlServiceImpl) ConfirmUpdateT_file_upd_jnl(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_file_upd_jnl(maps)
}

func (impl *T_file_upd_jnlServiceImpl) CancelUpdateT_file_upd_jnl(maps orm.Params) (err error) {
	return
}
