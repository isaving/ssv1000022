package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_int_settleCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_int_settle",
	ConfirmMethod: "ConfirmInsertT_loan_int_settle",
	CancelMethod:  "CancelInsertT_loan_int_settle",
}

type T_loan_int_settleService interface {
	TryInsertT_loan_int_settle(maps orm.Params) error
	ConfirmInsertT_loan_int_settle(maps orm.Params) error
	CancelInsertT_loan_int_settle(maps orm.Params) error
	TryUpdateT_loan_int_settle(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_int_settle(maps orm.Params) error
	CancelUpdateT_loan_int_settle(maps orm.Params) error
}

type T_loan_int_settleServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_int_settleServiceImpl) TryInsertT_loan_int_settle(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_int_settle(o, maps)
}

func (impl *T_loan_int_settleServiceImpl) ConfirmInsertT_loan_int_settle(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_int_settleTccState(o, maps)
}

func (impl *T_loan_int_settleServiceImpl) CancelInsertT_loan_int_settle(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_int_settleTccState(o, maps)
}

//智能查询
func (impl *T_loan_int_settleServiceImpl) QueryT_loan_int_settle(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_int_settle(params, 0)
}

var UpdateT_loan_int_settleCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_int_settle",
	ConfirmMethod: "ConfirmUpdateT_loan_int_settle",
	CancelMethod:  "CancelUpdateT_loan_int_settle",
}

func (impl *T_loan_int_settleServiceImpl) TryUpdateT_loan_int_settle(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_int_settleById(params, 0)
	return
}

func (impl *T_loan_int_settleServiceImpl) ConfirmUpdateT_loan_int_settle(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_int_settle(maps)
}

func (impl *T_loan_int_settleServiceImpl) CancelUpdateT_loan_int_settle(maps orm.Params) (err error) {
	return
}
