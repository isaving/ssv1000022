package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_lpr_org_sale_quotaCompensable = client.Compensable{
	TryMethod:     "TryInsertT_lpr_org_sale_quota",
	ConfirmMethod: "ConfirmInsertT_lpr_org_sale_quota",
	CancelMethod:  "CancelInsertT_lpr_org_sale_quota",
}

type T_lpr_org_sale_quotaService interface {
	TryInsertT_lpr_org_sale_quota(maps orm.Params) error
	ConfirmInsertT_lpr_org_sale_quota(maps orm.Params) error
	CancelInsertT_lpr_org_sale_quota(maps orm.Params) error
	TryUpdateT_lpr_org_sale_quota(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_lpr_org_sale_quota(maps orm.Params) error
	CancelUpdateT_lpr_org_sale_quota(maps orm.Params) error
}

type T_lpr_org_sale_quotaServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_lpr_org_sale_quotaServiceImpl) TryInsertT_lpr_org_sale_quota(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_lpr_org_sale_quota(o, maps)
}

func (impl *T_lpr_org_sale_quotaServiceImpl) ConfirmInsertT_lpr_org_sale_quota(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_lpr_org_sale_quotaTccState(o, maps)
}

func (impl *T_lpr_org_sale_quotaServiceImpl) CancelInsertT_lpr_org_sale_quota(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_lpr_org_sale_quotaTccState(o, maps)
}

//智能查询
func (impl *T_lpr_org_sale_quotaServiceImpl) QueryT_lpr_org_sale_quota(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_lpr_org_sale_quota(params, 0)
}

var UpdateT_lpr_org_sale_quotaCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_lpr_org_sale_quota",
	ConfirmMethod: "ConfirmUpdateT_lpr_org_sale_quota",
	CancelMethod:  "CancelUpdateT_lpr_org_sale_quota",
}

func (impl *T_lpr_org_sale_quotaServiceImpl) TryUpdateT_lpr_org_sale_quota(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_lpr_org_sale_quotaById(params, 0)
	return
}

func (impl *T_lpr_org_sale_quotaServiceImpl) ConfirmUpdateT_lpr_org_sale_quota(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_lpr_org_sale_quota(maps)
}

func (impl *T_lpr_org_sale_quotaServiceImpl) CancelUpdateT_lpr_org_sale_quota(maps orm.Params) (err error) {
	return
}
