package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_risk_cls_adj_regisCompensable = client.Compensable{
	TryMethod:     "TryInsertT_risk_cls_adj_regis",
	ConfirmMethod: "ConfirmInsertT_risk_cls_adj_regis",
	CancelMethod:  "CancelInsertT_risk_cls_adj_regis",
}

type T_risk_cls_adj_regisService interface {
	TryInsertT_risk_cls_adj_regis(maps orm.Params) error
	ConfirmInsertT_risk_cls_adj_regis(maps orm.Params) error
	CancelInsertT_risk_cls_adj_regis(maps orm.Params) error
	TryUpdateT_risk_cls_adj_regis(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_risk_cls_adj_regis(maps orm.Params) error
	CancelUpdateT_risk_cls_adj_regis(maps orm.Params) error
}

type T_risk_cls_adj_regisServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_risk_cls_adj_regisServiceImpl) TryInsertT_risk_cls_adj_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_risk_cls_adj_regis(o, maps)
}

func (impl *T_risk_cls_adj_regisServiceImpl) ConfirmInsertT_risk_cls_adj_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_risk_cls_adj_regisTccState(o, maps)
}

func (impl *T_risk_cls_adj_regisServiceImpl) CancelInsertT_risk_cls_adj_regis(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_risk_cls_adj_regisTccState(o, maps)
}

//智能查询
func (impl *T_risk_cls_adj_regisServiceImpl) QueryT_risk_cls_adj_regis(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_risk_cls_adj_regis(params, 0)
}

var UpdateT_risk_cls_adj_regisCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_risk_cls_adj_regis",
	ConfirmMethod: "ConfirmUpdateT_risk_cls_adj_regis",
	CancelMethod:  "CancelUpdateT_risk_cls_adj_regis",
}

func (impl *T_risk_cls_adj_regisServiceImpl) TryUpdateT_risk_cls_adj_regis(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_risk_cls_adj_regisById(params, 0)
	return
}

func (impl *T_risk_cls_adj_regisServiceImpl) ConfirmUpdateT_risk_cls_adj_regis(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_risk_cls_adj_regis(maps)
}

func (impl *T_risk_cls_adj_regisServiceImpl) CancelUpdateT_risk_cls_adj_regis(maps orm.Params) (err error) {
	return
}
