package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_earl_warn_sign_taskCompensable = client.Compensable{
	TryMethod:     "TryInsertT_earl_warn_sign_task",
	ConfirmMethod: "ConfirmInsertT_earl_warn_sign_task",
	CancelMethod:  "CancelInsertT_earl_warn_sign_task",
}

type T_earl_warn_sign_taskService interface {
	TryInsertT_earl_warn_sign_task(maps orm.Params) error
	ConfirmInsertT_earl_warn_sign_task(maps orm.Params) error
	CancelInsertT_earl_warn_sign_task(maps orm.Params) error
	TryUpdateT_earl_warn_sign_task(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_earl_warn_sign_task(maps orm.Params) error
	CancelUpdateT_earl_warn_sign_task(maps orm.Params) error
}

type T_earl_warn_sign_taskServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_earl_warn_sign_taskServiceImpl) TryInsertT_earl_warn_sign_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_earl_warn_sign_task(o, maps)
}

func (impl *T_earl_warn_sign_taskServiceImpl) ConfirmInsertT_earl_warn_sign_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_earl_warn_sign_taskTccState(o, maps)
}

func (impl *T_earl_warn_sign_taskServiceImpl) CancelInsertT_earl_warn_sign_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_earl_warn_sign_taskTccState(o, maps)
}

//智能查询
func (impl *T_earl_warn_sign_taskServiceImpl) QueryT_earl_warn_sign_task(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_earl_warn_sign_task(params, 0)
}

var UpdateT_earl_warn_sign_taskCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_earl_warn_sign_task",
	ConfirmMethod: "ConfirmUpdateT_earl_warn_sign_task",
	CancelMethod:  "CancelUpdateT_earl_warn_sign_task",
}

func (impl *T_earl_warn_sign_taskServiceImpl) TryUpdateT_earl_warn_sign_task(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_earl_warn_sign_taskById(params, 0)
	return
}

func (impl *T_earl_warn_sign_taskServiceImpl) ConfirmUpdateT_earl_warn_sign_task(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_earl_warn_sign_task(maps)
}

func (impl *T_earl_warn_sign_taskServiceImpl) CancelUpdateT_earl_warn_sign_task(maps orm.Params) (err error) {
	return
}
