package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_af_bnk_ln_chk_taskCompensable = client.Compensable{
	TryMethod:     "TryInsertT_af_bnk_ln_chk_task",
	ConfirmMethod: "ConfirmInsertT_af_bnk_ln_chk_task",
	CancelMethod:  "CancelInsertT_af_bnk_ln_chk_task",
}

type T_af_bnk_ln_chk_taskService interface {
	TryInsertT_af_bnk_ln_chk_task(maps orm.Params) error
	ConfirmInsertT_af_bnk_ln_chk_task(maps orm.Params) error
	CancelInsertT_af_bnk_ln_chk_task(maps orm.Params) error
	TryUpdateT_af_bnk_ln_chk_task(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_af_bnk_ln_chk_task(maps orm.Params) error
	CancelUpdateT_af_bnk_ln_chk_task(maps orm.Params) error
}

type T_af_bnk_ln_chk_taskServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_af_bnk_ln_chk_taskServiceImpl) TryInsertT_af_bnk_ln_chk_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_af_bnk_ln_chk_task(o, maps)
}

func (impl *T_af_bnk_ln_chk_taskServiceImpl) ConfirmInsertT_af_bnk_ln_chk_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_af_bnk_ln_chk_taskTccState(o, maps)
}

func (impl *T_af_bnk_ln_chk_taskServiceImpl) CancelInsertT_af_bnk_ln_chk_task(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_af_bnk_ln_chk_taskTccState(o, maps)
}

//智能查询
func (impl *T_af_bnk_ln_chk_taskServiceImpl) QueryT_af_bnk_ln_chk_task(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_af_bnk_ln_chk_task(params, 0)
}

//统计百分比
func (impl *T_af_bnk_ln_chk_taskServiceImpl) QueryT_af_bnk_ln_chk_taskPercentage(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_af_bnk_ln_chk_taskPercentage(params, 0)
}

var UpdateT_af_bnk_ln_chk_taskCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_af_bnk_ln_chk_task",
	ConfirmMethod: "ConfirmUpdateT_af_bnk_ln_chk_task",
	CancelMethod:  "CancelUpdateT_af_bnk_ln_chk_task",
}

func (impl *T_af_bnk_ln_chk_taskServiceImpl) TryUpdateT_af_bnk_ln_chk_task(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_af_bnk_ln_chk_taskById(params, 0)
	return
}

func (impl *T_af_bnk_ln_chk_taskServiceImpl) ConfirmUpdateT_af_bnk_ln_chk_task(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_af_bnk_ln_chk_task(maps)
}

func (impl *T_af_bnk_ln_chk_taskServiceImpl) CancelUpdateT_af_bnk_ln_chk_task(maps orm.Params) (err error) {
	return
}
