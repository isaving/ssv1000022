package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_cust_repmt_day_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertT_cust_repmt_day_info",
	ConfirmMethod: "ConfirmInsertT_cust_repmt_day_info",
	CancelMethod:  "CancelInsertT_cust_repmt_day_info",
}

type T_cust_repmt_day_infoService interface {
	TryInsertT_cust_repmt_day_info(maps orm.Params) error
	ConfirmInsertT_cust_repmt_day_info(maps orm.Params) error
	CancelInsertT_cust_repmt_day_info(maps orm.Params) error
	TryUpdateT_cust_repmt_day_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_cust_repmt_day_info(maps orm.Params) error
	CancelUpdateT_cust_repmt_day_info(maps orm.Params) error
}

type T_cust_repmt_day_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_cust_repmt_day_infoServiceImpl) TryInsertT_cust_repmt_day_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_cust_repmt_day_info(o, maps)
}

func (impl *T_cust_repmt_day_infoServiceImpl) ConfirmInsertT_cust_repmt_day_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_cust_repmt_day_infoTccState(o, maps)
}

func (impl *T_cust_repmt_day_infoServiceImpl) CancelInsertT_cust_repmt_day_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_cust_repmt_day_infoTccState(o, maps)
}

//智能查询
func (impl *T_cust_repmt_day_infoServiceImpl) QueryT_cust_repmt_day_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_cust_repmt_day_info(params, 0)
}

var UpdateT_cust_repmt_day_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_cust_repmt_day_info",
	ConfirmMethod: "ConfirmUpdateT_cust_repmt_day_info",
	CancelMethod:  "CancelUpdateT_cust_repmt_day_info",
}

func (impl *T_cust_repmt_day_infoServiceImpl) TryUpdateT_cust_repmt_day_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_cust_repmt_day_infoById(params, 0)
	return
}

func (impl *T_cust_repmt_day_infoServiceImpl) ConfirmUpdateT_cust_repmt_day_info(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_cust_repmt_day_info(maps)
}

func (impl *T_cust_repmt_day_infoServiceImpl) CancelUpdateT_cust_repmt_day_info(maps orm.Params) (err error) {
	return
}
