package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_collection_dec_aftert_loanCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_collection_dec_aftert_loan",
	ConfirmMethod: "ConfirmInsertT_loan_collection_dec_aftert_loan",
	CancelMethod:  "CancelInsertT_loan_collection_dec_aftert_loan",
}

type T_loan_collection_dec_aftert_loanService interface {
	TryInsertT_loan_collection_dec_aftert_loan(maps orm.Params) error
	ConfirmInsertT_loan_collection_dec_aftert_loan(maps orm.Params) error
	CancelInsertT_loan_collection_dec_aftert_loan(maps orm.Params) error
	TryUpdateT_loan_collection_dec_aftert_loan(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_collection_dec_aftert_loan(maps orm.Params) error
	CancelUpdateT_loan_collection_dec_aftert_loan(maps orm.Params) error
}

type T_loan_collection_dec_aftert_loanServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_collection_dec_aftert_loanServiceImpl) TryInsertT_loan_collection_dec_aftert_loan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_collection_dec_aftert_loan(o, maps)
}

func (impl *T_loan_collection_dec_aftert_loanServiceImpl) ConfirmInsertT_loan_collection_dec_aftert_loan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_collection_dec_aftert_loanTccState(o, maps)
}

func (impl *T_loan_collection_dec_aftert_loanServiceImpl) CancelInsertT_loan_collection_dec_aftert_loan(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_collection_dec_aftert_loanTccState(o, maps)
}

//智能查询
func (impl *T_loan_collection_dec_aftert_loanServiceImpl) QueryT_loan_collection_dec_aftert_loan(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_collection_dec_aftert_loan(params, 0)
}

var UpdateT_loan_collection_dec_aftert_loanCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_collection_dec_aftert_loan",
	ConfirmMethod: "ConfirmUpdateT_loan_collection_dec_aftert_loan",
	CancelMethod:  "CancelUpdateT_loan_collection_dec_aftert_loan",
}

func (impl *T_loan_collection_dec_aftert_loanServiceImpl) TryUpdateT_loan_collection_dec_aftert_loan(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_collection_dec_aftert_loanById(params, 0)
	return
}

func (impl *T_loan_collection_dec_aftert_loanServiceImpl) ConfirmUpdateT_loan_collection_dec_aftert_loan(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_collection_dec_aftert_loan(maps)
}

func (impl *T_loan_collection_dec_aftert_loanServiceImpl) CancelUpdateT_loan_collection_dec_aftert_loan(maps orm.Params) (err error) {
	return
}
