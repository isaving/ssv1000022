package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_whitelist_infoCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_whitelist_info",
	ConfirmMethod: "ConfirmInsertT_loan_whitelist_info",
	CancelMethod:  "CancelInsertT_loan_whitelist_info",
}

type T_loan_whitelist_infoService interface {
	TryInsertT_loan_whitelist_info(maps orm.Params) error
	ConfirmInsertT_loan_whitelist_info(maps orm.Params) error
	CancelInsertT_loan_whitelist_info(maps orm.Params) error
	TryUpdateT_loan_whitelist_info(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_loan_whitelist_info(maps orm.Params) error
	CancelUpdateT_loan_whitelist_info(maps orm.Params) error
}

type T_loan_whitelist_infoServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_loan_whitelist_infoServiceImpl) TryInsertT_loan_whitelist_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_whitelist_info(o, maps)
}

func (impl *T_loan_whitelist_infoServiceImpl) ConfirmInsertT_loan_whitelist_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_whitelist_infoTccState(o, maps)
}

func (impl *T_loan_whitelist_infoServiceImpl) CancelInsertT_loan_whitelist_info(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_whitelist_infoTccState(o, maps)
}

//智能查询
func (impl *T_loan_whitelist_infoServiceImpl) QueryT_loan_whitelist_info(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_whitelist_info(params, 0)
}

var UpdateT_loan_whitelist_infoCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_whitelist_info",
	ConfirmMethod: "ConfirmUpdateT_loan_whitelist_info",
	CancelMethod:  "CancelUpdateT_loan_whitelist_info",
}

func (impl *T_loan_whitelist_infoServiceImpl) TryUpdateT_loan_whitelist_info(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_loan_whitelist_infoById(params, 0)
	return
}

func (impl *T_loan_whitelist_infoServiceImpl) ConfirmUpdateT_loan_whitelist_info(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_whitelist_info(maps)
}

func (impl *T_loan_whitelist_infoServiceImpl) CancelUpdateT_loan_whitelist_info(maps orm.Params) (err error) {
	return
}
