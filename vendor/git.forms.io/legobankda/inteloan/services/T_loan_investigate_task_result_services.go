package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_loan_investigate_task_resultCompensable = client.Compensable{
	TryMethod:     "TryInsertT_loan_investigate_task_result",
	ConfirmMethod: "ConfirmInsertT_loan_investigate_task_result",
	CancelMethod:  "CancelInsertT_loan_investigate_task_result",
}

type T_loan_investigate_task_resultService interface {
	TryInsertT_loan_investigate_task_result(maps map[string]interface{}) error
	ConfirmInsertT_loan_investigate_task_result(maps map[string]interface{}) error
	CancelInsertT_loan_investigate_task_result(maps map[string]interface{}) error
	TryUpdateT_loan_investigate_task_result(params orm.Params) error
	ConfirmUpdateT_loan_investigate_task_result(params orm.Params) error
	CancelUpdateT_loan_investigate_task_result(params orm.Params) error
}

type T_loan_investigate_task_resultServiceImpl struct {
	aspect.DTSBaseService
}

func (impl *T_loan_investigate_task_resultServiceImpl) TryInsertT_loan_investigate_task_result(maps map[string]interface{}) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_loan_investigate_task_result(o, maps)
}

func (impl *T_loan_investigate_task_resultServiceImpl) ConfirmInsertT_loan_investigate_task_result(maps map[string]interface{}) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_loan_investigate_task_resultTccState(o, maps)
}

func (impl *T_loan_investigate_task_resultServiceImpl) CancelInsertT_loan_investigate_task_result(maps map[string]interface{}) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_loan_investigate_task_resultTccState(o, maps)
}

//智能查询
func (impl *T_loan_investigate_task_resultServiceImpl) QueryT_loan_investigate_task_result(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_loan_investigate_task_result(params, 0)
}

var UpdateT_loan_investigate_task_resultCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_loan_investigate_task_result",
	ConfirmMethod: "ConfirmUpdateT_loan_investigate_task_result",
	CancelMethod:  "CancelUpdateT_loan_investigate_task_result",
}

func (impl *T_loan_investigate_task_resultServiceImpl) TryUpdateT_loan_investigate_task_result(params orm.Params) (data map[string]interface{}, err error) {
	data, err = dao.QueryT_loan_investigate_task_resultById(params, 0)
	return
}

func (impl *T_loan_investigate_task_resultServiceImpl) ConfirmUpdateT_loan_investigate_task_result(params orm.Params) (err error) {
	params["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	params["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_loan_investigate_task_result(params)
}

func (impl *T_loan_investigate_task_resultServiceImpl) CancelUpdateT_loan_investigate_task_result(params orm.Params) (err error) {
	return
}
