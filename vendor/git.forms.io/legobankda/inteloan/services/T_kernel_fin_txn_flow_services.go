package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_kernel_fin_txn_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_kernel_fin_txn_flow",
	ConfirmMethod: "ConfirmInsertT_kernel_fin_txn_flow",
	CancelMethod:  "CancelInsertT_kernel_fin_txn_flow",
}

type T_kernel_fin_txn_flowService interface {
	TryInsertT_kernel_fin_txn_flow(maps orm.Params) error
	ConfirmInsertT_kernel_fin_txn_flow(maps orm.Params) error
	CancelInsertT_kernel_fin_txn_flow(maps orm.Params) error
	TryUpdateT_kernel_fin_txn_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_kernel_fin_txn_flow(maps orm.Params) error
	CancelUpdateT_kernel_fin_txn_flow(maps orm.Params) error
}

type T_kernel_fin_txn_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_kernel_fin_txn_flowServiceImpl) TryInsertT_kernel_fin_txn_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_kernel_fin_txn_flow(o, maps)
}

func (impl *T_kernel_fin_txn_flowServiceImpl) ConfirmInsertT_kernel_fin_txn_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_kernel_fin_txn_flowTccState(o, maps)
}

func (impl *T_kernel_fin_txn_flowServiceImpl) CancelInsertT_kernel_fin_txn_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_kernel_fin_txn_flowTccState(o, maps)
}

//智能查询
func (impl *T_kernel_fin_txn_flowServiceImpl) QueryT_kernel_fin_txn_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_kernel_fin_txn_flow(params, 0)
}

var UpdateT_kernel_fin_txn_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_kernel_fin_txn_flow",
	ConfirmMethod: "ConfirmUpdateT_kernel_fin_txn_flow",
	CancelMethod:  "CancelUpdateT_kernel_fin_txn_flow",
}

func (impl *T_kernel_fin_txn_flowServiceImpl) TryUpdateT_kernel_fin_txn_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_kernel_fin_txn_flowById(params, 0)
	return
}

func (impl *T_kernel_fin_txn_flowServiceImpl) ConfirmUpdateT_kernel_fin_txn_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_kernel_fin_txn_flow(maps)
}

func (impl *T_kernel_fin_txn_flowServiceImpl) CancelUpdateT_kernel_fin_txn_flow(maps orm.Params) (err error) {
	return
}
