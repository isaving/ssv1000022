package services

import (
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
)

var InsertT_early_warn_task_feedback_tempCompensable = client.Compensable{
	TryMethod:     "TryInsertT_early_warn_task_feedback_temp",
	ConfirmMethod: "ConfirmInsertT_early_warn_task_feedback_temp",
	CancelMethod:  "CancelInsertT_early_warn_task_feedback_temp",
}

type T_early_warn_task_feedback_tempService interface {
	TryInsertT_early_warn_task_feedback_temp(maps orm.Params) error
	ConfirmInsertT_early_warn_task_feedback_temp(maps orm.Params) error
	CancelInsertT_early_warn_task_feedback_temp(maps orm.Params) error
	TryUpdateT_early_warn_task_feedback_temp(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_early_warn_task_feedback_temp(maps orm.Params) error
	CancelUpdateT_early_warn_task_feedback_temp(maps orm.Params) error
}

type T_early_warn_task_feedback_tempServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_early_warn_task_feedback_tempServiceImpl) TryInsertT_early_warn_task_feedback_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_early_warn_task_feedback_temp(o, maps)
}

func (impl *T_early_warn_task_feedback_tempServiceImpl) ConfirmInsertT_early_warn_task_feedback_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_early_warn_task_feedback_tempTccState(o, maps)
}

func (impl *T_early_warn_task_feedback_tempServiceImpl) CancelInsertT_early_warn_task_feedback_temp(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_early_warn_task_feedback_tempTccState(o, maps)
}

//智能查询
func (impl *T_early_warn_task_feedback_tempServiceImpl) QueryT_early_warn_task_feedback_temp(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_early_warn_task_feedback_temp(params, 0)
}

var UpdateT_early_warn_task_feedback_tempCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_early_warn_task_feedback_temp",
	ConfirmMethod: "ConfirmUpdateT_early_warn_task_feedback_temp",
	CancelMethod:  "CancelUpdateT_early_warn_task_feedback_temp",
}

func (impl *T_early_warn_task_feedback_tempServiceImpl) TryUpdateT_early_warn_task_feedback_temp(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_early_warn_task_feedback_tempById(params, 0)
	return
}

func (impl *T_early_warn_task_feedback_tempServiceImpl) ConfirmUpdateT_early_warn_task_feedback_temp(maps orm.Params) (err error) {
	//maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	//maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_early_warn_task_feedback_temp(maps)
}

func (impl *T_early_warn_task_feedback_tempServiceImpl) CancelUpdateT_early_warn_task_feedback_temp(maps orm.Params) (err error) {
	return
}
