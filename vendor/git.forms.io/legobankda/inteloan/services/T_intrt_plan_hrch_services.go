package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_intrt_plan_hrchCompensable = client.Compensable{
	TryMethod:     "TryInsertT_intrt_plan_hrch",
	ConfirmMethod: "ConfirmInsertT_intrt_plan_hrch",
	CancelMethod:  "CancelInsertT_intrt_plan_hrch",
}

type T_intrt_plan_hrchService interface {
	TryInsertT_intrt_plan_hrch(maps orm.Params) error
	ConfirmInsertT_intrt_plan_hrch(maps orm.Params) error
	CancelInsertT_intrt_plan_hrch(maps orm.Params) error
	TryUpdateT_intrt_plan_hrch(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_intrt_plan_hrch(maps orm.Params) error
	CancelUpdateT_intrt_plan_hrch(maps orm.Params) error
}

type T_intrt_plan_hrchServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_intrt_plan_hrchServiceImpl) TryInsertT_intrt_plan_hrch(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_intrt_plan_hrch(o, maps)
}

func (impl *T_intrt_plan_hrchServiceImpl) ConfirmInsertT_intrt_plan_hrch(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_intrt_plan_hrchTccState(o, maps)
}

func (impl *T_intrt_plan_hrchServiceImpl) CancelInsertT_intrt_plan_hrch(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_intrt_plan_hrchTccState(o, maps)
}

//智能查询
func (impl *T_intrt_plan_hrchServiceImpl) QueryT_intrt_plan_hrch(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_intrt_plan_hrch(params, 0)
}

var UpdateT_intrt_plan_hrchCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_intrt_plan_hrch",
	ConfirmMethod: "ConfirmUpdateT_intrt_plan_hrch",
	CancelMethod:  "CancelUpdateT_intrt_plan_hrch",
}

func (impl *T_intrt_plan_hrchServiceImpl) TryUpdateT_intrt_plan_hrch(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_intrt_plan_hrchById(params, 0)
	return
}

func (impl *T_intrt_plan_hrchServiceImpl) ConfirmUpdateT_intrt_plan_hrch(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_intrt_plan_hrch(maps)
}

func (impl *T_intrt_plan_hrchServiceImpl) CancelUpdateT_intrt_plan_hrch(maps orm.Params) (err error) {
	return
}
