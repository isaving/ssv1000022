package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_sms_templateCompensable = client.Compensable{
	TryMethod:     "TryInsertT_sms_template",
	ConfirmMethod: "ConfirmInsertT_sms_template",
	CancelMethod:  "CancelInsertT_sms_template",
}

type T_sms_templateService interface {
	TryInsertT_sms_template(maps orm.Params) error
	ConfirmInsertT_sms_template(maps orm.Params) error
	CancelInsertT_sms_template(maps orm.Params) error
	TryUpdateT_sms_template(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_sms_template(maps orm.Params) error
	CancelUpdateT_sms_template(maps orm.Params) error
}

type T_sms_templateServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_sms_templateServiceImpl) TryInsertT_sms_template(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_sms_template(o, maps)
}

func (impl *T_sms_templateServiceImpl) ConfirmInsertT_sms_template(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_sms_templateTccState(o, maps)
}

func (impl *T_sms_templateServiceImpl) CancelInsertT_sms_template(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_sms_templateTccState(o, maps)
}

//智能查询
func (impl *T_sms_templateServiceImpl) QueryT_sms_template(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_sms_template(params, 0)
}

var UpdateT_sms_templateCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_sms_template",
	ConfirmMethod: "ConfirmUpdateT_sms_template",
	CancelMethod:  "CancelUpdateT_sms_template",
}

func (impl *T_sms_templateServiceImpl) TryUpdateT_sms_template(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_sms_templateById(params, 0)
	return
}

func (impl *T_sms_templateServiceImpl) ConfirmUpdateT_sms_template(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_sms_template(maps)
}

func (impl *T_sms_templateServiceImpl) CancelUpdateT_sms_template(maps orm.Params) (err error) {
	return
}
