package services

import (
	constant "git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/dao"
	"git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"github.com/astaxie/beego/orm"
	"time"
)

var InsertT_chln_hndl_flowCompensable = client.Compensable{
	TryMethod:     "TryInsertT_chln_hndl_flow",
	ConfirmMethod: "ConfirmInsertT_chln_hndl_flow",
	CancelMethod:  "CancelInsertT_chln_hndl_flow",
}

type T_chln_hndl_flowService interface {
	TryInsertT_chln_hndl_flow(maps orm.Params) error
	ConfirmInsertT_chln_hndl_flow(maps orm.Params) error
	CancelInsertT_chln_hndl_flow(maps orm.Params) error
	TryUpdateT_chln_hndl_flow(maps orm.Params) (orm.Params, error)
	ConfirmUpdateT_chln_hndl_flow(maps orm.Params) error
	CancelUpdateT_chln_hndl_flow(maps orm.Params) error
}

type T_chln_hndl_flowServiceImpl struct {
	aspect.DTSBaseService
}

//智能新增
func (impl *T_chln_hndl_flowServiceImpl) TryInsertT_chln_hndl_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.InsertT_chln_hndl_flow(o, maps)
}

func (impl *T_chln_hndl_flowServiceImpl) ConfirmInsertT_chln_hndl_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.UpdateT_chln_hndl_flowTccState(o, maps)
}

func (impl *T_chln_hndl_flowServiceImpl) CancelInsertT_chln_hndl_flow(maps orm.Params) (err error) {
	o := orm.NewOrm()
	return dao.DeleteT_chln_hndl_flowTccState(o, maps)
}

//智能查询
func (impl *T_chln_hndl_flowServiceImpl) QueryT_chln_hndl_flow(params orm.Params) (info []orm.Params, err error) {
	return dao.QueryT_chln_hndl_flow(params, 0)
}

var UpdateT_chln_hndl_flowCompensable = client.Compensable{
	TryMethod:     "TryUpdateT_chln_hndl_flow",
	ConfirmMethod: "ConfirmUpdateT_chln_hndl_flow",
	CancelMethod:  "CancelUpdateT_chln_hndl_flow",
}

func (impl *T_chln_hndl_flowServiceImpl) TryUpdateT_chln_hndl_flow(params orm.Params) (maps orm.Params, err error) {
	maps, err = dao.QueryT_chln_hndl_flowById(params, 0)
	return
}

func (impl *T_chln_hndl_flowServiceImpl) ConfirmUpdateT_chln_hndl_flow(maps orm.Params) (err error) {
	maps["FinlModfyDt"] = time.Now().Format(constant.DATE_FORMAT)
	maps["FinlModfyTm"] = time.Now().Format(constant.TIME_FORMAT)
	return dao.UpdateT_chln_hndl_flow(maps)
}

func (impl *T_chln_hndl_flowServiceImpl) CancelUpdateT_chln_hndl_flow(maps orm.Params) (err error) {
	return
}
