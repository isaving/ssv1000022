pwd=`pwd`

t=$(echo $1 | tr '[A-Z]' '[a-z]')
echo $t

controller=`echo _controller.go`
cp $pwd/controllers/Acsjhfp_controller.go $pwd/controllers/$1${controller}
sed -i "s/Acsjhfp/$1/g" ./controllers/$1${controller}

dao=`echo _dao.go`
cp $pwd/dao/Acsjhfp_dao.go $pwd/dao/$1${dao}
sed -i "s/Acsjhfp/$1/g" ./dao/$1${dao}

model=`echo _event.go`
cp $pwd/model/Acsjhfp_event.go $pwd/model/$1${model}
sed -i "s/Acsjhfp/$1/g" ./model/$1${model}

services=`echo _services.go`
cp $pwd/services/Acsjhfp_services.go $pwd/services/$1${services}
sed -i "s/Acsjhfp/$1/g" ./services/$1${services}

sed -i "23i new($1)," $pwd/dao/init.go

sed -i "64i Update$1	=	\"IL8U00$2\"" $pwd/const/const.go
sed -i "64i Query$1     =	\"IL8R00$2\"" $pwd/const/const.go
sed -i "64i Insert$1	=	\"IL8C00$2\"" $pwd/const/const.go

sed -i "12i eventRouterRegister.Router(\"IL8U00$2\", &controllers.$1Controller{}, \"Update$1\")" $pwd/event_routers/event_router.go
sed -i "12i eventRouterRegister.Router(\"IL8R00$2\", &controllers.$1Controller{}, \"Query$1\")"  $pwd/event_routers/event_router.go
sed -i "12i eventRouterRegister.Router(\"IL8C00$2\", &controllers.$1Controller{}, \"Insert$1\")" $pwd/event_routers/event_router.go