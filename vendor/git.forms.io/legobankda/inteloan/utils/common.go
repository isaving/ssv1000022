package util

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/universe/comm-agent/client"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"reflect"
	"time"
)

func CheckRequest(bodyBuf []byte, v interface{}) error {
	//json := jsoniter.ConfigCompatibleWithStandardLibrary
	/*body := deposit_model.Response{}
	if err := json.Unmarshal(bodyBuf, &body); err!=nil {
		return err
	}
	if len(body.Form) < 1 {
		return errors.Errorf("response data invalid")
	}*/

	if err := json.Unmarshal(bodyBuf, v); err != nil {
		return err
	}
	return nil
}

func StructToMap(s interface{}) map[string]interface{} {
	t := reflect.TypeOf(s)
	v := reflect.ValueOf(s)
	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		data[t.Field(i).Name] = v.Field(i).Interface()
	}
	return data
}

func QueryPage(maps []orm.Params, pageNo int, pageRecCount int) []orm.Params {
	if pageNo == 0 {
		pageNo = 1
	}
	if pageRecCount == 0 {
		pageRecCount = 10
	}

	start := (pageNo-1)*pageRecCount + 1
	if start > len(maps) {
		return nil
	}
	var end int
	if pageNo*pageRecCount > len(maps) {
		end = len(maps)
	} else {
		end = pageNo * pageRecCount
	}
	return maps[start-1 : end]
}

func CombineMaps(maps []orm.Params) orm.Params {
	ms := make(map[string]interface{})
	for _, m := range maps {
		for k, v := range m {
			ms[k] = v
		}
	}
	return ms
}

func ContParamHandler(event interface{}) (params orm.Params, err error) {
	if event == nil {
		return nil, nil
	}
	params = orm.Params{}
	valueF := reflect.ValueOf(event).Elem()
	typeF := reflect.TypeOf(event).Elem()
	for i := 0; i < valueF.NumField(); i++ {
		if valueF.Field(i).Kind() == reflect.String || valueF.Field(i).Kind() == reflect.Int64 || valueF.Field(i).Kind() == reflect.Float32 {
			sName := typeF.Field(i).Name
			sValue := valueF.Field(i)
			params[sName] = sValue.Interface()
			continue
		}
		// nullField: null.String or null.Float
		nullField := reflect.ValueOf(valueF.Field(i).Interface())
		if &nullField != nil {
			IsZero := nullField.MethodByName("IsZero").Call([]reflect.Value{})
			if IsZero != nil && !IsZero[0].Bool() {
				Ptr := nullField.MethodByName("Ptr").Call([]reflect.Value{})
				if Ptr != nil {
					sName := typeF.Field(i).Name
					sValue := Ptr[0].Interface()
					params[sName] = sValue
				}
			}
		}
	}
	if len(params) == 0 {
		return nil, nil
	}
	return params, nil
}

func HandleSysHeader(req *client.UserMessage, rsp *client.UserMessage, topicId string) {
	reqMap := req.AppProps
	rspMap := rsp.AppProps
	// 原始系统信息
	rspMap[constant.GLOBAL_BIZ_SEQNO] = reqMap[constant.GLOBAL_BIZ_SEQNO]
	rspMap["OrgSysId"] = reqMap["OrgSysId"]
	rspMap["OrgChannelType"] = reqMap["OrgChannelType"]
	rspMap["OrgScenarioId"] = reqMap["OrgScenarioId"]
	rspMap["OrgPartnerId"] = reqMap["OrgPartnerId"]
	rspMap["LastActEntryNo"] = reqMap["LastActEntryNo"]

	// 交易信息
	rspMap["TxWorkStation"] = reqMap["TxWorkStation"]
	rspMap["TxDeviceId"] = reqMap["TxDeviceId"]
	rspMap["TxDeptCode"] = reqMap["TxDeptCode"]
	rspMap["ReverseSeqNo"] = reqMap["ReverseSeqNo"]
	rspMap["UserLang"] = reqMap["UserLang"]
	rspMap["TxUserId"] = reqMap["TxUserId"]
	rspMap["TxUserLvl"] = reqMap["TxUserLvl"]
	rspMap["TxAuthorizer"] = reqMap["TxAuthorizer"]
	rspMap["TxAuthUserId"] = reqMap["TxAuthUserId"]
	rspMap["TxAuthUserLvl"] = reqMap["TxAuthUserLvl"]
	rspMap["TxAuthFlag"] = reqMap["TxAuthFlag"]

	// 目标服务信息
	rspMap["TrgServiceId"] = topicId                                               // 目前serviceId与topicId相同，以后应该会调整
	rspMap["TrgServiceVersion"] = beego.AppConfig.String("service_props::version") // 目前多个服务在一个程序中，不同服务的version可能不同
	rspMap["TrgTopicId"] = topicId
	rspMap["TrgBizDate"] = time.Now().Format(constant.DATE_FORMAT)
	rspMap[constant.TRG_BIZ_SEQNO] = GenerateSeqNo("0", "001", beego.AppConfig.String("service_props::appFlag"), "001")

	// 正式发布时，dcn应从配置文件中读取，instanceId应从环境变量中读取:
	//fullDcn := beego.AppConfig.String("service::groupDcn")
	//last3Dcn := fullDcn[len(fullDcn)-3:]
	//fullInsId := os.Getenv("INSTANCE_ID")
	//last3InsId := fullInsId[len(fullInsId)-3:]
	//rspMap[constant.TRG_BIZ_SEQNO] = GenerateSeqNo("0",last3Dcn,"DASV",last3InsId)

	rspMap["TrgTimeStamp"] = time.Now().Format(constant.TIMESTAMP_FORMAT)
}
