package util

import (
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/universe/comm-agent/client"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
)

func CallDts(formId string, service interface{}, compens dtsClient.Compensable, params interface{}, c *compensable.TxnCtx) *client.UserMessage {
	response := &client.UserMessage{}
	proxy, err := aspect.NewDTSProxy(service, compens, c)
	if err != nil {
		log.Errorf("New DTS Proxy failed, error message: %s", err.Error())
		response = BuildErrResponse(constant.DTS_PROXY_FAILED, err.Error())
		return response
	}
	if ret := proxy.Do(params); ret[len(ret)-1].Interface() != nil {
		response = BuildErrResponse(constant.DTS_DO_FAILED, ret[len(ret)-1].Interface().(error).Error())
	} else {
		response = BuildSucResponse(formId, map[string]interface{}{"status": "ok"})
	}
	return response
}

func CallDtsWithResult(formId string, service interface{}, compens dtsClient.Compensable, params interface{}, c *compensable.TxnCtx) *client.UserMessage {
	response := &client.UserMessage{}
	proxy, err := aspect.NewDTSProxy(service, compens, c)
	if err != nil {
		log.Errorf("New DTS Proxy failed, error message: %s", err.Error())
		response = BuildErrResponse(constant.DTS_PROXY_FAILED, err.Error())
		return response
	}
	if ret := proxy.Do(params); ret[len(ret)-1].Interface() != nil {
		response = BuildErrResponse(constant.DTS_DO_FAILED, ret[len(ret)-1].Interface().(error).Error())
	} else {
		log.Debugf("ret[0]", ret[0].Interface().(map[string]interface{}))
		response = BuildSucResponse(formId, ret[0].Interface().(map[string]interface{}))
	}
	return response
}

func CallDtsWithArrResult(formId string, service interface{}, compens dtsClient.Compensable, params interface{}, c *compensable.TxnCtx) *client.UserMessage {
	response := &client.UserMessage{}
	proxy, err := aspect.NewDTSProxy(service, compens, c)
	if err != nil {
		log.Errorf("New DTS Proxy failed, error message: %s", err.Error())
		response = BuildErrResponse(constant.DTS_PROXY_FAILED, err.Error())
		return response
	}
	if ret := proxy.Do(params); ret[len(ret)-1].Interface() != nil {
		response = BuildErrResponse(constant.DTS_DO_FAILED, ret[len(ret)-1].Interface().(error).Error())
	} else {
		log.Debugf("ret[0]", ret[0].Interface().([]orm.Params))
		response = BuildArrResponse(formId, ret[0].Interface().([]orm.Params))
	}
	return response
}

func CallCheckLimitDts(formId string, service interface{}, compens dtsClient.Compensable, params interface{}, c *compensable.TxnCtx) (response *client.UserMessage) {
	//defer func() {
	//	if err := recover(); err != nil {
	//		switch x := err.(type) {
	//		case string:
	//			err = errors.New(x)
	//		case error:
	//			err = x
	//		default:
	//			err = errors.New("")
	//		}
	//		response = BuildErrResponse(constant.DTS_PROXY_FAILED, err.(error).Error())
	//	}
	//}()
	response = &client.UserMessage{}
	proxy, err := aspect.NewDTSProxy(service, compens, c)
	if err != nil {
		log.Errorf("New DTS Proxy failed, error message: %s", err.Error())
		response = BuildErrResponse(constant.DTS_PROXY_FAILED, err.Error())
		return response
	}
	if ret := proxy.Do(params); ret[len(ret)-1].Interface() != nil {
		limitCtlId := ret[0].Interface().(string)
		if limitCtlId != "" {
			response = BuildErrResponse(ret[len(ret)-1].Interface().(error).Error(), limitCtlId)
		} else {
			response = BuildErrResponse(constant.DTS_DO_FAILED, ret[len(ret)-1].Interface().(error).Error())
		}
	} else {
		response = BuildSucResponse(formId, map[string]interface{}{"status": "ok"})
	}
	return response
}

/*func CallDtses(formId string, service interface{}, compens dtsClient.Compensable, params interface{}, c *compensable.TxnCtx) *client.UserMessage {
	response := &client.UserMessage{}

	proxy, err := aspect.NewDTSProxy(service, compens, c)
	if err != nil {
		log.Errorf("New DTS Proxy failed, error message: %s", err.Error())
		response = BuildErrResponse(constant.DTS_PROXY_FAILED, err.Error())
		return response
	}

	if ret := proxy.Do(params); ret[len(ret)-1].Interface() != nil {
		if ret[len(ret)-1].Interface().(error).Error() == constant.PK_DUPLICATE {
			response = BuildErrResponse(constant.PK_CONFLICT, constant.PK_DUPLICATE)
		} else {
			response = BuildErrResponse(constant.DTS_DO_FAILED, ret[len(ret)-1].Interface().(error).Error())
		}
	} else {
		response = BuildSucResponse(formId, map[string]interface{}{"status": "ok"})
	}
	return response
}*/
