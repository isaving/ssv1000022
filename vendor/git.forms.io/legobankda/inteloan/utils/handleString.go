package util

import (
	"strings"
	"unicode"
)

func Handle(st string) string {
	var builder strings.Builder
	byte := []rune(st)
	for i := 0; i < len(byte); i++ {
		if i == 0 { //判断是不是大写
			builder.WriteRune(unicode.ToLower(byte[i]))
		} else if unicode.IsUpper(byte[i]) && i != 0 {
			builder.WriteString("_")
			builder.WriteRune(unicode.ToLower(byte[i]))
		} else {
			builder.WriteRune(byte[i])
		}
	}
	//for k, _ := range params {
	//	byte :=[]rune(k)
	//	for i:=0; i<len(byte); i++ {
	//		if unicode.IsUpper(byte[i]) {//判断是不是大写
	//			builder.WriteRune(byte[i])
	//			builder.WriteString("_")
	//		}
	//		builder.WriteRune(byte[i])
	//	}
	//
	//}
	return builder.String()
}
