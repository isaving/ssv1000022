package util

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

type SeqNo struct {
	SNDate      string `valid:"Length(8)"`  //查询起始日期-"YYYYMMDD"
	SNType      string `valid:"Length(1)"`  //1位流水号类型： 业务流水号取值0，系统调用流水号取值1
	SNDcn       string `valid:"Length(3)"`  //3位DCN编号
	SNOrgSys    string `valid:"Length(4)"`  //4位发起方子系统号
	SNIns       string `valide:"Length(3)"` //3位实例ID
	SNLocalTime string `valide:"Length(9)"` //9位机器时间
	SNSeq       int    `valide:"Length(6)"` //6位顺序号
}

func GenerateSeqNo(snType, dcn, orgSysId, ins string) string {
	var SN SeqNo
	SN.SNType = snType
	SN.SNDcn = dcn
	SN.SNOrgSys = orgSysId
	SN.SNIns = ins
	SN.SNSeq = Getseq()
	t := time.Now()

	return fmt.Sprintf("%s"+
		"%s"+
		"%s"+
		"%s"+
		"%s"+
		"%s%s"+
		"%06d",
		t.Format("060102"),
		SN.SNType,
		SN.SNDcn,
		SN.SNOrgSys,
		SN.SNIns,
		t.Format("150405"), strconv.Itoa(t.Nanosecond())[0:3],
		SN.SNSeq)
}

var flagNo int64 = 0
var lock sync.Mutex

func Getseq() int {
	lock.Lock()
	flagNo++
	lock.Unlock()
	flagNoStr := strconv.FormatInt(flagNo, 10)
	flagNoStr = "000000"[0:6-len(flagNoStr)] + flagNoStr
	if flagNo > 999999 {
		flagNo = 1
		flagNoStr = "000001"
	}
	flagN, _ := strconv.Atoi(flagNoStr)
	return flagN
}
