package util

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"strings"
)

//一张表所对应的服务
func TestBuildsheet() {

	o := orm.NewOrm()
	excelFileName := "d:/data/5.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("open failed: %s\n", err)
	}else {
		for _, sheet := range xlFile.Sheets {
			fmt.Println("开始读取-----", sheet.Name)
			fmt.Println("一共有", len(sheet.Rows))

			for i, row := range sheet.Rows {	//row--行  cell--列

				if i >= len(sheet.Rows){
					break
				}
				c0 := row.Cells[0].String()
				c1 := row.Cells[1].String()
				c2 := row.Cells[2].String()
				c3 := row.Cells[3].String()
				c4 := row.Cells[4].String()
				fmt.Println(c0,c1,c2,c3,c4)
				sql := "SELECT t2.`topicid` FROM `loan_bss_bfs_das` t2 WHERE t2.`das_or_services`=? group by t2.`topicid`"
				list :=orm.ParamsList{}
				o.Raw(sql,c2).ValuesFlat(&list)


				var slice []string
				if len(list) == 0 || list == nil  {
					cell := row.AddCell()
					cell.Value = ""
				}else {
					for _, m := range list {
						slice = append(slice, m.(string))
					}
					cell := row.AddCell()
					cell.Value = strings.Join(slice, ",")
				}

			}
			fmt.Println("数据读取结束-----",sheet.Name)
		}
	}
	 //xlFile.Save(excelFileName)
	err = xlFile.Save(excelFileName)
	//"d:/data/9.xlsx"
	//err = xlFile.Save(newSheet)
	if err != nil {
		fmt.Printf(err.Error())
	}
}

//影响到整个调用链
func TestBuildsheet1() {

	file1 := xlsx.NewFile()
	sheet1, err := file1.AddSheet("调用链")
	//添加一列数据
	//title := sheet1.AddRow()
	//title.AddCell().Value = "name"
	//title.AddCell().Value = "表名"
	//title.AddCell().Value = "C"
	//title.AddCell().Value = "R"
	//title.AddCell().Value = "U"
	//title.AddCell().Value = "姓名"
	//title.AddCell().Value = "服务名tocipid"
	//title.AddCell().Value = "服务名"
	//title.AddCell().Value = "下游的服务"

	o := orm.NewOrm()
	excelFileName := "d:/data/5.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("open failed: %s\n", err)
	}else {
		for _, sheet := range xlFile.Sheets {
			fmt.Println("开始读取-----", sheet.Name)
			fmt.Println("一共有", len(sheet.Rows))

			for i, row := range sheet.Rows {	//row--行  cell--列

				if i >= len(sheet.Rows){
					break
				}
				c0 := row.Cells[0].String()
				c1 := row.Cells[1].String()
				c2 := row.Cells[2].String()
				c3 := row.Cells[3].String()
				c4 := row.Cells[4].String()

				sql := "SELECT t1.`name`,t1.`topicid`,t1.`service_name`,t1.`das_or_services` FROM `loan_services`t1 WHERE t1.`das_or_services` LIKE \"%"+c2+"%\" " +
					"UNION " +
					"SELECT t1.`name`,t1.`topicid`,t1.`service_name`,t1.`das_or_services` FROM `loan_services`t1 WHERE t1.`das_or_services` LIKE \"%"+c3+"%\" " +
					"UNION " +
					"SELECT t1.`name`,t1.`topicid`,t1.`service_name`,t1.`das_or_services` FROM `loan_services`t1 WHERE t1.`das_or_services` LIKE \"%"+c4+"%\" "+
					" GROUP BY t1.`topicid`"
				list := []orm.Params{}
				o.Raw(sql).Values(&list)

				if len(list) == 0 || list == nil  {
					span := sheet1.AddRow()
					span.AddCell().Value= c0
					span.AddCell().Value= c1
					span.AddCell().Value= c2
					span.AddCell().Value= c3
					span.AddCell().Value= c4
				}else {

					for _, maps1 := range list{
						span := sheet1.AddRow()
						span.AddCell().Value= c0
						span.AddCell().Value= c1
						span.AddCell().Value= c2
						span.AddCell().Value= c3
						span.AddCell().Value= c4

						span.AddCell().Value = maps1["name"].(string)
						span.AddCell().Value = maps1["topicid"].(string)
						span.AddCell().Value = maps1["service_name"].(string)
						span.AddCell().Value = maps1["das_or_services"].(string)

						//span5 := span.AddCell()
						//span5.VMerge=1
						//span5.Value = maps1["name"].(string)
						//span6 := span.AddCell()
						//span6.VMerge=1
						//span6.Value = maps1["topicid"].(string)
						//span7 := span.AddCell()
						//span7.VMerge=1
						//span7.Value = maps1["service_name"].(string)
						//span8 := span.AddCell()
						//span8.VMerge=1
						//span8.Value = maps1["das_or_services"].(string)


					}
				}

			}
			fmt.Println("数据读取结束-----",sheet.Name)
			file1.Save("d:/data/9.xlsx")
		}
	}
}

//处理影响到的das
func TestBuildsheetDas() {

	file1 := xlsx.NewFile()
	sheet1, err := file1.AddSheet("调用链")
	//添加一列数据
	//title := sheet1.AddRow()
	//title.AddCell().Value = "name"
	//title.AddCell().Value = "表名"
	//title.AddCell().Value = "C"
	//title.AddCell().Value = "R"
	//title.AddCell().Value = "U"
	//title.AddCell().Value = "姓名"
	//title.AddCell().Value = "服务名tocipid"
	//title.AddCell().Value = "服务名"
	//title.AddCell().Value = "下游的服务"

	o := orm.NewOrm()
	excelFileName := "d:/data/4.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("open failed: %s\n", err)
	}else {
		for _, sheet := range xlFile.Sheets {
			fmt.Println("开始读取-----", sheet.Name)
			fmt.Println("一共有", len(sheet.Rows))

			for i, row := range sheet.Rows {	//row--行  cell--列

				if i >= len(sheet.Rows){
					break
				}
				c0 := row.Cells[0].String()

				sql := "SELECT * FROM `das_tocipid` t WHERE t.`table_name` =? "
				list := []orm.Params{}
				o.Raw(sql,c0).Values(&list)

				if len(list) == 0 || list == nil  {

				}else {

					for _, maps1 := range list{
						span := sheet1.AddRow()
						span.AddCell().Value= maps1["name"].(string)
						span.AddCell().Value= maps1["table_name"].(string)
						span.AddCell().Value= maps1["C"].(string)
						span.AddCell().Value= maps1["R"].(string)
						span.AddCell().Value=maps1["U"].(string)
					}
				}

			}
			err :=file1.Save("d:/data/10.xlsx")
			fmt.Println("err = ",err)
			fmt.Println("数据读取结束-----",sheet.Name)
		}
	}
}