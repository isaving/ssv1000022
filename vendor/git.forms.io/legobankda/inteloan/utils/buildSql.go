package util

import (
	"git.forms.io/universe/solapp-sdk/log"
	"reflect"
	"strconv"
	"strings"
	"unicode"
)

func BuildSql(maps map[string]interface{}, tabname string) string {
	defer func() {
		if e := recover(); e != nil {
			log.Error("error is ", e.(interface{}))
		}
	}()
	var buildsql strings.Builder
	var filedsql strings.Builder
	var valuesql strings.Builder
	buildsql.WriteString("INSERT INTO " + tabname + " (")
	for k, v := range maps {
		var strBuilder strings.Builder
		for i, val := range k {
			if i == 0 {
				strBuilder.WriteString(strings.ToLower(string(val)))
				continue
			} else if unicode.IsUpper(val) {
				strBuilder.WriteString("_")
			}
			strBuilder.WriteString(strings.ToLower(string(val)))
		}
		filedsql.WriteString(strBuilder.String() + ",")
		if v == nil {
			valuesql.WriteString("'',")
			continue
		}
		if reflect.TypeOf(v).String() == "int" {
			valuesql.WriteString("'" + strconv.Itoa(v.(int)) + "',")
		} else if reflect.TypeOf(v).String() == "float32" {
			v = strconv.FormatFloat(float64(v.(float32)), 'f', 6, 64)
			valuesql.WriteString("'" + v.(string) + "',")
		} else if reflect.TypeOf(v).String() == "float64" {
			v = strconv.FormatFloat(v.(float64), 'f', 6, 64)
			valuesql.WriteString("'" + v.(string) + "',")
		} else {
			valuesql.WriteString("'" + v.(string) + "',")
		}
	}
	filedsql.WriteString("tcc_state) VALUES (")
	valuesql.WriteString("1)")
	return buildsql.String() + filedsql.String() + valuesql.String()
}

func BuildSqlTcc(maps map[string]interface{}, tabname string, TccState int) string {
	//TccState:0--不做tcc的新增（一边新增一边修改），1：做tcc的新增
	defer func() {
		if e := recover(); e != nil {
			log.Error("error is ", e.(interface{}))
		}
	}()
	var buildsql strings.Builder
	var filedsql strings.Builder
	var valuesql strings.Builder
	buildsql.WriteString("INSERT INTO " + tabname + " (")
	for k, v := range maps {
		var strBuilder strings.Builder
		for i, val := range k {
			if i == 0 {
				strBuilder.WriteString(strings.ToLower(string(val)))
				continue
			} else if unicode.IsUpper(val) {
				strBuilder.WriteString("_")
			}
			strBuilder.WriteString(strings.ToLower(string(val)))
		}
		filedsql.WriteString(strBuilder.String() + ",")
		if v == nil {
			valuesql.WriteString("'',")
			continue
		}
		if reflect.TypeOf(v).String() == "int" {
			valuesql.WriteString("'" + strconv.Itoa(v.(int)) + "',")
		} else if reflect.TypeOf(v).String() == "float32" {
			v = strconv.FormatFloat(float64(v.(float32)), 'f', 6, 64)
			valuesql.WriteString("'" + v.(string) + "',")
		} else if reflect.TypeOf(v).String() == "float64" {
			v = strconv.FormatFloat(v.(float64), 'f', 6, 64)
			valuesql.WriteString("'" + v.(string) + "',")
		} else {
			valuesql.WriteString("'" + v.(string) + "',")
		}
	}
	filedsql.WriteString("tcc_state) VALUES (")
	valuesql.WriteString(strconv.Itoa(TccState) + ")")
	return buildsql.String() + filedsql.String() + valuesql.String()
}

func HandleInterfaceToArr(params []interface{}) (stringArr []string) {
	for _, param := range params {
		stringArr = append(stringArr, param.(string))
	}
	return stringArr
}

//做um改造，主要是机构，角色，用户----三个das的新增
func BuildSqlUm(maps map[string]interface{}, tabname string) string {
	defer func() {
		if e := recover(); e != nil {
			log.Error("error is ", e.(interface{}))
		}
	}()
	var buildsql strings.Builder
	var filedsql strings.Builder
	var valuesql strings.Builder
	buildsql.WriteString("INSERT INTO " + tabname + " (")
	for k, v := range maps {
		var strBuilder strings.Builder
		for i, val := range k {
			if i == 0 {
				strBuilder.WriteString(strings.ToLower(string(val)))
				continue
			} else if unicode.IsUpper(val) {
				strBuilder.WriteString("_")
			}
			strBuilder.WriteString(strings.ToLower(string(val)))
		}
		filedsql.WriteString(strBuilder.String()+ ",")
		if v == nil {
			valuesql.WriteString("'',")
			continue
		}
		if reflect.TypeOf(v).String() == "int" {
			valuesql.WriteString("'" + strconv.Itoa(v.(int)) + "',")
		} else if reflect.TypeOf(v).String() == "float32" {
			v = strconv.FormatFloat(float64(v.(float32)), 'f', 6, 64)
			valuesql.WriteString("'" + v.(string) + "',")
		} else if reflect.TypeOf(v).String() == "float64" {
			v = strconv.FormatFloat(v.(float64), 'f', 6, 64)
			valuesql.WriteString("'" + v.(string) + "',")
		} else {
			valuesql.WriteString("'" + v.(string) + "',")
		}
	}
	umfiledsql :=strings.TrimRight(filedsql.String(),",")+") VALUES ("
	//filedsql.WriteString(") VALUES (")
	umsql := strings.TrimRight(valuesql.String(),",")+")"
	//valuesql.WriteString("1)")
	return buildsql.String() + umfiledsql + umsql
}