package model

type T_loan_credit_element_paramsEvent struct {
	Form []struct {
		FormData T_loan_credit_element_paramsFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_credit_element_paramsFormData struct {
	BizSn                  		  string
	LprOrgNo                      string
	LprOrgNm                      string
	LoanProdtNo                   string
	LoanProdtNm                   string
	IntrtTypCd                    string
	LprIntrtBpFlotCeilVal         float64
	LprIntrtBpFlotFloorVal        float64
	PbocBnchmkIntrtFlotCeilRatio  float64
	PbocBnchmkIntrtFlotFloorRatio float64
	QtaTypCd                      string
	QtaFlotCeilRatio              float64
	QtaFlotFloorRatio             float64
	KeprcdStusCd                  string
	CrtEmpnbr                     string
	CrtOrgNo                      string
	CrtDt                         string
	FinlModfyDt                   string
	FinlModfyTm                   string
	FinlModfyOrgNo                string
	FinlModfyTelrNo               string

	PageNo       int
	PageRecCount int
}
