package model

type T_intrt_planEvent struct {
	Form []struct {
		FormData T_intrt_planFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intrt_planFormData struct {
	IntacrRuleNo           string
	OrgNo                  string
	CurCd                  string
	IntacrPrtyTypCd        string
	IntacrAgrthmTypCd      string
	LowestIntStlAmt        float64
	LyrdFlg                string
	AmtHrchTypCd           string
	DeadlHrchTypCd         string
	DeadlAmtPrtyTypCd      string
	UseIntrtSorcCd         string
	IntrtNo                string
	FixdIntrt              float64
	LowestIntrt            float64
	MonIntacrDaysCd        string
	YrIntacrDaysCd         string
	IntrtFlotManrCd        string
	FlotDrctCd             string
	IntrtFlotCeilVal       float64
	IntrtFlotFloorVal      float64
	DfltFlotVal            float64
	CntRoleBitPtpIntacrFlg string
	CrtclDotDistClsfCd     string
	HrchIntrtAmtTypCd      string
	HrchTotlQty            int
	LyrdDeadlUnitCd        string
	IntrtCycCd             string
	LoanIntrtAdjCycCd      string
	LoanIntrtAdjCycQty     int
	IntacrRuleDescr        string
	Flag1                  string
	Flag2                  string
	Bak1                   float64
	Bak2                   float64
	Bak3                   string
	NoIntrtEfftDt          string
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string
	TccKeprcdVisFlg        string
	PageNo       int
	PageRecCount int
}
