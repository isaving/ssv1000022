package model

type Um_userEvent struct {
	Form []struct {
		FormData Um_userFormData
		FormHead struct {
			FormID string
		}
	}
}

type Um_userFormData struct {
	Id         		string
	UserName        string
	Password        string
	Email           string
	Mobile          string
	Nickname        string
	IsActive        string
	IsLocked        string
	LockedTime      string
	FailCount       string
	LastLoginTime   string
	LastLoginIp     string
	AuthType        string
	CreatedAt       string
	UpdatedAt       string
	DeletedAt       string
	AccessBegin     string
	AccessEnd       string

	PageNo       int
	PageRecCount int
}
