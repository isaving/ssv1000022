package model

type T_loan_customer_credit_inquiry_tempEvent struct {
	Form []struct {
		FormData T_loan_customer_credit_inquiry_tempFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_customer_credit_inquiry_tempFormData struct {
	IndvCrtftypCd   string
	CrtfNo          string
	CrdQuryDt       string
	CrdQuryResultCd string
	CrdQuryResultId string
	PageNo       int
	PageRecCount int
}
