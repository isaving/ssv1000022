package model

type T_itlg_wind_appr_resuEvent struct {
	Form []struct {
		FormData T_itlg_wind_appr_resuFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_itlg_wind_appr_resuFormData struct {
	CrdtAplySn         string
	SeqNo              int
	CustNo             string
	LoanProdtNo        string
	SggestQta          float64
	DcmkTypCd          string
	DcmkDescr          string
	TxResultStusCd     string
	TxResultDescr      string
	CreditRatCd        string
	NegtRsn            string
	Bp                 int
	AdjDrctCd          string
	HitExcludRuleComnt string
	BlklistTypCd       string
	MonIncomeAmt       float64
	ScoreCardGrpgInfo  string
	EvaltScr           float64
	RatCd              string
	FinlModfyDt        string
	FinlModfyTm        string
	FinlModfyOrgNo     string
	FinlModfyTelrNo    string

	PageNo       int
	PageRecCount int
}
