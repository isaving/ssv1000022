package model

type T_early_warn_task_resultEvent struct {
	Form []struct {
		FormData T_early_warn_task_resultFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_early_warn_task_resultFormData struct {
	TaskDlwthResultNo        string
	WarnTaskNo               string
	SeqNo                    int
	CustNo                   string
	HndlrEmpnbr              string
	ReacCustManrCd           string
	KeprcdStusCd             string
	CorgDlwthResultCd        string
	CollTms                  int
	WarnSignalDlwthResultCd  string
	WarnDlwthRemrk           string
	CotactTelNo              string
	CurrAddr                 string
	Lgtd                     float64
	LgtdDrctCd               string
	Lttd                     float64
	LttdDrctCd               string
	UpdAftRsdnceScAdcmCd     string
	UpdAftRsdnceDtlAddr      string
	UpdAftRsdnceAddr         string
	UpdAftCmunicAddrScAdcmCd string
	UpdAftCmunicDtlAddr      string
	UpdAftCmunicAddr         string
	UpdGrndPostCd            string
	OneslfCmtdInfo           string
	MrgncyConterName         string
	PtyRelKindCd             string
	MrgncyConterTelNo        string
	SponsorEmpnbr            string
	SponsorOrgNo             string
	CorgEmpnbr               string
	CorgOrgNo                string
	DlwthDt                  string
	FinlModfyDt              string
	FinlModfyTm              string
	FinlModfyOrgNo           string
	FinlModfyTelrNo          string
	PageNo       int
	PageRecCount int
}
