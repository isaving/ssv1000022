package model

type T_loan_contract_infoEvent struct {
	Form []struct {
		FormData T_loan_contract_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_contract_infoFormData struct {
	CtrtNo                   string
	LoanAcctNo               string
	CustNo                   string
	CustNm                   string
	LoanProdtNo              string
	LoanProdtNm              string
	BrwmnyCtrtTypCd          string
	LoanGuarManrCd           string
	CurCd                    string
	AprvAmt                  float64
	CtrtAmt                  float64
	AprvDeadlMons            int
	CtrtDeadlMons            int
	CtrtStartDt              string
	CtrtMatrDt               string
	LoanUsageCd              string
	BrwmnyUsageComnt         string
	SpmeCntLoanRiskClsfCd    string
	SpmeCntLoanRiskClsfComnt string
	SpdmnyManrCd             string
	AutonPaymtLmt            float64
	PayWayCd                 string
	AutonPaymtAcctNo         string
	AutonPaymtAcctNm         string
	PaymtRptsCycCd           string
	PaymtRptsCycQty          int
	RepayManrCd              string
	IntStlManrCd             string
	IntacrManrCd             string
	TranOvdueGraceDays       int
	ChrgManrCd               string
	EntrstChrgAcctNo         string
	AcctNm                   string
	RepaySorcComnt           string
	IntSubsidyRatio          float64
	IntSubsidyExpiryDt       string
	BilsAddr                 string
	PostCd                   string
	EMail                    string
	CotactTelNo              string
	MobileNo                 string
	DsptSltnManrCd           string
	ArbitCommNm              string
	ArbitCommAddr            string
	MakelnPmseCondComnt      string
	OthApntMtsComnt          string
	ApprvDt                  string
	KepacctOrgNo             string
	OprOrgNo                 string
	OprorEmpnbr              string
	CtrtImgNo                string
	FinlModfyDt              string
	FinlModfyTm              string
	FinlModfyOrgNo           string
	FinlModfyTelrNo          string
	PageNo       int
	PageRecCount int
}
