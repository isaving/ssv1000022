package model

type T_proc_model_paraEvent struct {
	Form []struct {
		FormData T_proc_model_paraFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_proc_model_paraFormData struct {
	ProcesModelId     string
	ProcesModelVersNo int
	TaskTmplTypCd     string
	AvalOperCd        string
	TaskTmplNo        string
	TaskNm            string
	TaskConfParaInfo  string
	CrtTelrNo         string
	CrtDt             string
	CrtTm             string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string

	PageNo       int
	PageRecCount int
}
