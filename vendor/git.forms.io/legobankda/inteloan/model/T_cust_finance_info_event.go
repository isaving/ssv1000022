package model

type T_cust_finance_infoEvent struct {
	Form []struct {
		FormData T_cust_finance_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cust_finance_infoFormData struct {
	CustId             string
	Education          string
	JobStatus          string
	Job                string
	JobTitle           string
	CompanyName        string
	Department         string
	CompanySize        string
	JobClass           string
	IndustryNature     string
	Position           string
	IndustryCode       string
	WorkYears          int
	Employer           string
	EmployerAddr       string
	EmployerBussType   string
	WorkStartDate      string
	PayCurrency        string
	Salary             float64
	AnnualIncome       float64
	PayrollFreq        string
	MonthlyIncome      float64
	MonthlyExpenditure float64
	Remarks            string
	HousingStatus      string
	HousingType        string
	StayOnDate         string
	HousingValuation   float64
	HomeMortgageAmt    float64
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string

	PageNo       int
	PageRecCount int
}
