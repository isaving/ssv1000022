package model

type T_loan_approve_taskEvent struct {
	Form []struct {
		FormData T_loan_approve_taskFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_approve_taskFormData struct {
	TaskExampId       string
	TaskNm            string
	BizId             string
	ProcesExampId     string
	DistrPersonEmpnbr string
	CanExctrRoleNo    string
	CrtTm             string
	OperTm            string
	ProcesTmplId      string
	CorgTaskStusCd    string
	CorgTaskExampId   string
	CustNo            string
	TaskTypCd         string
	LoanProdtNo       string
	LprOrgNo          string
	BizOrgNo          string
	SticDt            string
	SticTm            string
	TaskTmplId        string
	MenuFuncId        string
	TaskDlwthStusCd   string
	CrtDt             string
	OperDt            string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
