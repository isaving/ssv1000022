package model

type T_role_sensEvent struct {
	Form []struct {
		FormData T_role_sensFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_role_sensFormData struct {
	RoleNo          string
	SensTypCd       string
	CrtTelrNo       string
	CrtDt           string
	CrtTm           string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
