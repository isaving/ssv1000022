package model

type T_earl_warn_sign_taskEvent struct {
	Form []struct {
		FormData T_earl_warn_sign_taskFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_earl_warn_sign_taskFormData struct {
	WarnTaskNo          string
	LoanDubilNo         string
	CustNo              string
	CustName            string
	IndvCrtfTypCd       string
	IndvCrtfNo          string
	OrgNo               string
	OrgNm               string
	CrtDt               string
	TaskDlwthStusCd     string
	WarnSponsorEmpnbr   string
	WarnSponsorOrgNo    string
	WarnCorgEmpnbr      string
	WarnCorgOrgNo       string
	AlrdyExecFlg        string
	AtExecWindowFlg     string
	ExecTms             int
	ExecConcusCd        string
	ExecRsn             string
	CtrtNo              string
	SponsorEmplyName    string
	CorgEmplyName       string
	SponsorOrgNm        string
	CorgOrgNm           string
	SponsorEmplyTaskNo  string
	CorgEmplyTaskNo     string
	TaskDlwthResultNo   string
	WarnSignalTypCd     string
	AdjQtaDlwthResultCd string
	DlwthDt             string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string

	PageNo       int
	PageRecCount int
}
