package model

type T_loan_contract_change_detailEvent struct {
	Form []struct {
		FormData T_loan_contract_change_detailFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_contract_change_detailFormData struct {
	CtrtNo            string
	SeqNo             int
	CustNo            string
	CustName          string
	TxSn              string
	NodeNm            string
	OperTypCd         string
	OperBefCtrtStusCd string
	OperAftCtrtStusCd string
	AfltInfo          string
	OperEmpnbr        string
	OperOrgNo         string
	OperTm            string
	OperResultCd      string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
