package model

type T_rplan_chg_regisEvent struct {
	Form []struct {
		FormData T_rplan_chg_regisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_rplan_chg_regisFormData struct {
	LoanDubilNo     string
	SeqNo           int
	CustNo          string
	LoanProdtNo     string
	CtrtNo          string
	BizAplyNo       string
	UpdStusCd       string
	BefRepayManrCd  string
	AftRepayManrCd  string
	BefRepayDay     string
	AftRepayDay     string
	BefCycCd        string
	BefCycQty       int
	AftCycCd        string
	AftCycQty       int
	AdjDate         string
	AdjReason       string
	AplyOrgNo       string
	AdjAftBpFlotVal string
	AdjBefFlotRatio string
	UpdCtrtNo       string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
