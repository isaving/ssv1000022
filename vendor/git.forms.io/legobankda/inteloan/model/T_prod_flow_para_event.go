package model

type T_prod_flow_paraEvent struct {
	Form []struct {
		FormData T_prod_flow_paraFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_prod_flow_paraFormData struct {
	LoanProdtNo       string
	ProdtProcesVersNo int
	OrgNo             string
	BizProcesNm       string
	ProcesTypCd       string
	ProcesModelId     string
	ProcesModelVersNo int
	CrtTelrNo         string
	CrtDt             string
	CrtTm             string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
