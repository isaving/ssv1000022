package model

type T_loan_save_fileEvent struct {
	Form []struct {
		FormData T_loan_save_fileFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_save_fileFormData struct {
	FileType        string
	FileNm          string
	ObjectId        string
	FileInfo        string
	Remark          string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyTelrNo string
	FinlModfyOrgNo  string

	PageNo       int
	PageRecCount int
}
