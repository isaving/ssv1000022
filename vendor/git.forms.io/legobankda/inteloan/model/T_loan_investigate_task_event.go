package model

type T_loan_investigate_taskEvent struct {
	Form []struct {
		FormData T_loan_investigate_taskFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_investigate_taskFormData struct {
	ArtgclInvstgTaskNo     string
	CrdtAplySn             string
	CustNo                 string
	CustName               string
	IndvCrtfTypCd          string
	IndvCrtfNo             string
	ArtgclInvstgTaskStusCd string
	AlrdyExecFlg           string
	ExecPrdFlg             string
	ExecTms                int
	ExecRsn                string
	ExecConcusComnt        string
	SponsorCustMgrEmpnbr   string
	SponsorCustMgrName     string
	SponsorCustMgrOrgNo    string
	CorgCustMgrEmpnbr      string
	CorgCustMgrName        string
	CorgCustMgrOrgNo       string
	SponsorCustMgrOrgNm    string
	CorgCustMgrOrgNm       string
	CrtDt                  string
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string

	PageNo       int
	PageRecCount int
}
