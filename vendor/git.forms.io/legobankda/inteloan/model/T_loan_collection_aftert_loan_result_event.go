package model

type T_loan_collection_aftert_loan_resultEvent struct {
	Form []struct {
		FormData T_loan_collection_aftert_loan_resultFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_collection_aftert_loan_resultFormData struct {
	TaskDlwthResultNo         string
	ResultCnfrmRemrkInfo      string
	CollTaskNo                string
	SeqNo                     int
	CustNo                    string
	ReacCustManrCd            string
	KeprcdStusCd              string
	CorgDlwthResultCd         string
	CollTms                   int
	AfBnkLnCollDlwthResultCd  string
	CollDlwthRemrk            string
	FamilyRsnCd               string
	TrdRsnCd                  string
	WorkRsnCd                 string
	CotactTelNo               string
	CurrAddr                  string
	Lgtd                      float64
	LgtdDrctCd                string
	Lttd                      float64
	LttdDrctCd                string
	UpdAftRsdnceScAdcmCd      string
	UpdAftRsdnceDtlAddr       string
	UpdAftRsdnceAddr          string
	UpdAftCmunicAddrScAdcmCd  string
	UpdAftCmunicDtlAddr       string
	UpdAftCmunicAddr          string
	UpdGrndPostCd             string
	MrgncyConterName          string
	AndBrwerRelCd             string
	MrgncyConterCotactTelNo   string
	SponsorEmpnbr             string
	SponsorOrgNo              string
	CorgEmpnbr                string
	CorgOrgNo                 string
	OneslfCmtdInfo            string
	DlwthDt                   string
	HndlrEmpnbr               string
	CorgDlwthResultCnfrmRemrk string
	FinlModfyDt               string
	FinlModfyTm               string
	FinlModfyOrgNo            string
	FinlModfyTelrNo           string

	PageNo       int
	PageRecCount int
}
