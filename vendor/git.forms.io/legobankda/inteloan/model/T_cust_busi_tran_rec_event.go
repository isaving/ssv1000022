package model

type T_cust_busi_tran_recEvent struct {
	Form []struct {
		FormData T_cust_busi_tran_recFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cust_busi_tran_recFormData struct {
	ProcesId              string
	DelvrDt               string
	CustNo                string
	CustName              string
	IndvCrtfTypCd         string
	IndvCrtfNo            string
	MobileNo              string
	LoanProdtNo           string
	LoanProdtVersNo       string
	DelvrBefCustMgrEmpnbr string
	DelvrBefOrgNo         string
	DelvrAftCustMgrEmpnbr string
	DelvrAftOrgNo         string
	DelvrPrvlgTypCd       string
	DelvrPersonRelCd      string
	DelvrComnt            string
	FinlModfyDt           string
	FinlModfyTm           string
	FinlModfyOrgNo        string
	FinlModfyTelrNo       string
	PageNo       int
	PageRecCount int
}
