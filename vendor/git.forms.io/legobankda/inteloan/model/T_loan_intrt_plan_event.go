package model

type T_loan_intrt_planEvent struct {
	Form []struct {
		FormData T_loan_intrt_planFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_intrt_planFormData struct {
	AcctiAcctNo     string
	BizFolnNo       string
	SysFolnNo       string
	DlwthStusCd     string
	CurCd           string
	TxAcctnDt       string
	MgmtOrgNo       string
	AcctiOrgNo      string
	ChnlCd          string
	RpyintAmt       float64
	RpyintModeCd    string
	Tmprd           int
	IntrStusCd      string
	RpyintTypCd     string
	TrsctnId        string
	TccStusCd       string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
