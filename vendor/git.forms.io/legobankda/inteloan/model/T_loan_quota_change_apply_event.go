package model

type T_loan_quota_change_applyEvent struct {
	Form []struct {
		FormData T_loan_quota_change_applyFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_quota_change_applyFormData struct {
	ChgAplyNo           string
	CustNo              string
	CrdtAplySn          string
	AplyAdjRsnCd        string
	AplyAdjTypCd        string
	AdjDrctCd           string
	CurCd               string
	ChgBefAvalQta       float64
	ChgAftQta           float64
	ChgAftAvalQta       float64
	ChgRsn              string
	ExmnvrfyStusCd      string
	ExmnvrfySugstnComnt string
	TxSn                string
	CrtDt               string
	CrtEmpnbr           string
	CrtOrgNo            string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string

	PageNo       int
	PageRecCount int
}
