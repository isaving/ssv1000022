package model

type T_loan_op_manageEvent struct {
	Form []struct {
		FormData T_loan_op_manageFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_op_manageFormData struct {
	LoanProdtNo      string
	LoanProdtNm      string
	RoleNo           string
	Empnbr           string
	OrgNo            string
	OrgNm            string
	EmplyName        string
	RoleNm           string
	ValidFlg         string
	FinlModfyTm      string
	FinlModfyDt      string
	FinlModfyOrgNo   string
	FinlModifrEmpnbr string

	PageNo       int
	PageRecCount int
}
