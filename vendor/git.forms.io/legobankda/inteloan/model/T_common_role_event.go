package model

type T_common_roleEvent struct {
	Form []struct {
		FormData T_common_roleFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_common_roleFormData struct {
	RoleId        string
	Status        string
	RoleName      string
	Description   string
	RoleCategory  string
	PermGroup1    string
	PermGroup2    string
	PermGroup3    string
	PermGroup4    string
	EffDate       string
	ExpDate       string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string

	PageNo       int
	PageRecCount int
}
