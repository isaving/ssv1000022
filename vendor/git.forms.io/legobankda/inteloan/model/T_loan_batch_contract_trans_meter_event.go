package model

type T_loan_batch_contract_trans_meterEvent struct {
	Form []struct {
		FormData T_loan_batch_contract_trans_meterFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_batch_contract_trans_meterFormData struct {
	LoanDubilNo      string
	SeqNo            int
	Pridnum          int
	AcctnDt          string
	LoanBatBizTypCd  string
	CustNo           string
	BizSn            string
	BatJobStepStusCd string
	PageNo       int
	PageRecCount int
}
