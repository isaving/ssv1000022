package model

type T_risk_cls_adj_recEvent struct {
	Form []struct {
		FormData T_risk_cls_adj_recFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_risk_cls_adj_recFormData struct {
	LoanDubilNo                      string
	AdjDt                            string
	SeqNo                            int
	CustNo                           string
	AdjManrCd                        string
	AdjPersonEmpnbr                  string
	AdjPersonOrgNo                   string
	AdjBefLoanRiskClsfCd             string
	AdjAftLoanRiskClsfCd             string
	CurtprdCompEtimLnRiskClsfDt      string
	InendIdtfyLoanRiskClsfDt         string
	InendIdtfyLoanRiskClsCd          string
	CurtprdMchLoanRiskClsfDt         string
	CurtprdArtgclIdtfyLoanRiskClsfCd string
	CurtprdArtgclIdtfyLoanRiskClsfDt string
	LoanRiskClsfStusCd  			 string
	SugstnDescr		       	  		 string
	FinlModfyDt                      string
	FinlModfyTm                      string
	FinlModfyOrgNo                   string
	FinlModfyTelrNo                  string
	PageNo       int
	PageRecCount int
}
