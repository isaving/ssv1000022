package model

type T_ae_eventEvent struct {
	Form []struct {
		FormData T_ae_eventFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_eventFormData struct {
	BizEventCd      string
	BizEventNm      string
	BizEventTypCd   string
	BizEventTypNm   string
	EfftDt          string
	ValidFlg        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
