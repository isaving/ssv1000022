package model

type T_collection_task_feedback_tempEvent struct {
	Form []struct {
		FormData T_collection_task_feedback_tempFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_collection_task_feedback_tempFormData struct {
	TxSn               string
	ExecDate           string
	CustNm             string
	CustNo             string
	CrtfNo             string
	IndvCrtfTypCd      string
	OrgNo              string
	DubilNo            string
	CurrLoanRiskClsfCd string
	DlqcyProb          float64
	RiskRsn            string
	SggestMgmtManrCd   string
	AdjustCrdtQta      float64
	SggestExecWindow   string
	TaskStartDt        string
	TaskEndDt          string
	CustNo1            string
	CustName1          string
	IndvCrtfNo1        string
	IndvCrtfTypCd1     string
	LoanDubilNo1       string
	AlrdyExecFlg       string
	AtExecWindowFlg    string
	ExecTms            int
	ReachTms           int

	PageNo       int
	PageRecCount int
}
