package model

type T_loan_curr_repay_infoEvent struct {
	Form []struct {
		FormData T_loan_curr_repay_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_curr_repay_infoFormData struct {
	LoanAcctNo       string
	AcctnDt          string
	Pridnum          int
	LoanAcctiAcctNo  string
	ChrgAcctNo       string
	AcctNm           string
	RepayDt          string
	CurCd            string
	PlanRepayTotlAmt float64
	PlanRepayPrin    float64
	PlanRepayIntr    float64
	CoreChrgStusCd   string
	LoanRepayStusCd  string
	FailRsn		     string
	PageNo       int
	PageRecCount int
}
