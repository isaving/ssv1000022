package model

type T_impa_regisEvent struct {
	Form []struct {
		FormData T_impa_regisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_impa_regisFormData struct {
	LoanAcctNo      string
	SeqNo           int
	DvalBizAplyNo   string
	DvalDlwthStusCd string
	CtrtNo          string
	CustNo          string
	CustNm          string
	LoanProdtNo     string
	LoanProdtVersNo string
	LoanProdtNm     string
	LoanGuarManrCd  string
	GrantDt         string
	MatrDt          string
	RepayManrCd     string
	LoanRiskClsfCd  string
	TranDvalComnt   string
	DvalFlg         string
	DuePrin         float64
	DuePrinBgnDt    string
	OvdueIntr       float64
	IntrOvdueBgnDt  string
	OvduePnltint    float64
	PnltintBgnDt    string
	DvalAplyDt      string
	AplyEmpnbr      string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo          int
	PageRecCount    int
}
