package model

type T_loan_elec_cont_tmplEvent struct {
	Form []struct {
		FormData T_loan_elec_cont_tmplFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_elec_cont_tmplFormData struct {
	TmplNo            string
	TmplPdfFileId     string
	CtrtModeCd        string
	SigtPositionTypCd string
	CtrtTmplTypNm     string
	EfftDt            string
	TmplNm            string
	DocTmplTypCd      string
	TmplSubTypCd      string
	TmplFileId        string
	CtrtTmplComnt     string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string

	PageNo       int
	PageRecCount int
}
