package model

type T_artif_credit_ele_paramEvent struct {
	Form []struct {
		FormData T_artif_credit_ele_paramFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_artif_credit_ele_paramFormData struct {
	LoanProdtNo              string
	EfftDt                   string
	QtaAdjRangDegreeCeilVal  float64
	QtaAdjRangDegreeFloorVal int
	FixdIntrtFlotCeilRatio   float64
	FixdIntrtFlotFloorRatio  float64
	LprIntrtBpFlotCeilVal    float64
	LprIntrtBpFlotFloorVal   float64
	CrtTelrNo                string
	CrtTm                    string
	CrtOrgNo                 string
	CrdtElentParaStusCd      string
	FinlModfyDt              string
	FinlModfyTm              string
	FinlModfyOrgNo           string
	FinlModfyTelrNo          string
	PageNo       int
	PageRecCount int
}
