package model

type T_loan_accti_acct_detaEvent struct {
	Form []struct {
		FormData T_loan_accti_acct_detaFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_accti_acct_detaFormData struct {
	AcctiAcctNo            string
	Pridnum                int
	ChgacctDt              string
	DataValidFlgCd         string
	CurrPeriodIntacrBgnDt  string
	CurrPeriodIntacrEndDt  string
	BegintDt               string
	WrtoffAmt              float64
	WrtoffPrerdcAmt        float64
	WrtoffPreincAmt        float64
	WrtoffTccStusCd        string
	ActlRepayPrinPrerdcAmt float64
	ActlRepayPrinPreincAmt float64
	ActlRepayPrinTccStusCd string
	CurrPeriodRepayDt      string
	BalTypCd               string
	PlanRepayPrin          float64
	ActlRepayDt            string
	ActlRepayPrin          float64
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string
	PageNo       int
	PageRecCount int
}
