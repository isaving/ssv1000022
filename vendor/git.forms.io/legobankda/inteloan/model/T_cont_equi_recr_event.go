package model

type T_cont_equi_recrEvent struct {
	Form []struct {
		FormData T_cont_equi_recrFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cont_equi_recrFormData struct {
	CustNo           string
	SeqNo            int
	CustName         string
	CtrtNo           string
	LoanDubilNo      string
	RgtsintClsfNo    string
	RgtsintNo        string
	RgtsintUseStusCd string
	UseDt            string
	CnclDt           string
	RgtsintMatrDt    string
	ExecFinhDt       string
	PageNo       int
	PageRecCount int
}
