package model

type T_loan_extn_regisEvent struct {
	Form []struct {
		FormData T_loan_extn_regisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_extn_regisFormData struct {
	LoanDubilNo        string
	SeqNo              int
	CustNo             string
	LoanProdtNo        string
	CtrtNo             string
	BizAplyNo          string
	UpdStusCd          string
	BefBegnDt          string
	BefMatrDt          string
	AftBegnDt          string
	AftMatrDt          string
	BefIntrt           float64
	IntrtAdjFlag       string
	IntrtTypCd         string
	AdjIntrtTypCd      string
	LoanIntrtAdjManrCd string
	IntrtFlotDrctCd    string
	ChgRintrtFlotRatio float64
	ChgBpFlotVal       float64
	AftIntrt           float64
	BefRepayManrCd     string
	AftRepayManrCd     string
	BefCycCd           string
	BefCycQty          int
	AftCycCd           string
	AftCycQty          int
	AdjDate            string
	AdjReason          string
	AplyOrgNo          string
	AdjAftBpFlotVal    string
	AdjBefFlotRatio    string
	UpdCtrtNo          string
	FinlModfyDt        string
	FinlModfyTm        string
	FinlModfyOrgNo     string
	FinlModfyTelrNo    string

	PageNo       int
	PageRecCount int
}
