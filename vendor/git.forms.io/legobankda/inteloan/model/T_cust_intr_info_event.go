package model

type T_cust_intr_infoEvent struct {
	Form []struct {
		FormData T_cust_intr_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cust_intr_infoFormData struct {
	AplyBizNo              string
	CustNo                 string
	IndvCrtfTypCd          string
	IndvCrtfNo             string
	LoanProdtNo            string
	LoanProdtVersNo        string
	ExecIntrt              float64
	SpcyBnchmkIntrt        float64
	BaseIntrtTypCd         string
	IntrtTypCd             string
	IntrtNo                string
	IntrtFlotDrctCd        string
	BpFlotVal              float64
	IntrtFlotRatio         float64
	KeprcdStusCd           string
	FilesTrmCd             string
	FixdIntrtPricingManrCd string
	IntrtFlotManrCd        string
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string
	PageNo       int
	PageRecCount int
}
