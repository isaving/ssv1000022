package model

type T_loan_customer_listEvent struct {
	Form []struct {
		FormData T_loan_customer_listFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_customer_listFormData struct {
	PageNo       int
	PageRecCount int
}
