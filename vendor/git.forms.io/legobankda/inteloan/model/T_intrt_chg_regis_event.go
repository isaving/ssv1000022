package model

type T_intrt_chg_regisEvent struct {
	Form []struct {
		FormData T_intrt_chg_regisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intrt_chg_regisFormData struct {
	LoanDubilNo         string
	AdjDt               string
	SeqNo               int
	DvalBizAplyNo       string
	DvalDlwthStusCd     string
	CtrtNo              string
	CustNo              string
	CustName            string
	LoanProdtNo         string
	LoanProdtNm         string
	GrantDt             string
	MatrDt              string
	LoanIntrtAdjManrCd  string
	IntrtFlotDrctCd     string
	ChgRintrtFlotRatio  float64
	ChgBpFlotVal        float64
	AdjAftIntrt         float64
	AdjDate             string
	AdjReason           string
	AdjAftBpFlotVal     string
	AdjBefFlotRatio     string
	ExmnvrfyStusCd      string
	ExmnvrfySugstnComnt string
	ApprvSugstnCd       string
	ApprvSugstnComnt    string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string


	PageNo       int
	PageRecCount int
}
