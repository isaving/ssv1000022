package model

type T_loan_contract_suppt_info_formEvent struct {
	Form []struct {
		FormData T_loan_contract_suppt_info_formFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_contract_suppt_info_formFormData struct {
	LoanDubilNo      				 string
	CustNo                           string
	LoanTotlPridnum                  int
	CurrExecPridnum                  int
	LoanRemainPridnum                int
	PrinDueBgnDt                     string
	IntrDueBgnDt                     string
	AccmDuePridnum                   int
	AccmWrtoffAmt                    float64
	LsttrmCompEtimLnRiskClsfDt       string
	LsttrmCompEtimLnRiskClsfCd       string
	CurtprdCompEtimLnRiskClsfDt      string
	CurtprdCompEtimLnRiskClsfCd      string
	LsttrmArtgclIdtfyLoanRiskClsfDt  string
	LsttrmArtgclIdtfyLoanRiskClsfCd  string
	CurtprdArtgclIdtfyLoanRiskClsfDt string
	CurtprdArtgclIdtfyLoanRiskClsfCd string
	FinlModfyDt                      string
	FinlModfyTm                      string
	FinlModfyOrgNo                   string
	FinlModfyTelrNo                  string

	PageNo       int
	PageRecCount int
}
