package model

type T_prod_model_paraEvent struct {
	Form []struct {
		FormData T_prod_model_paraFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_prod_model_paraFormData struct {
	KeprcdNo        string
	DocTmplTypCd    string
	LoanProdtNo     string
	TmplSubTypCd    string
	KeprcdStusCd    string
	OrgNo           string
	EfftDt          string
	CrtTelrNo       string
	CrtTm           string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
