package model

type Ts_user_infoEvent struct {
	Form []struct {
		FormData Ts_user_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type Ts_user_infoFormData struct {
	UserId            string
	DeptId            string
	UserName          string
	LoginName         string
	Password          string
	UserType          string
	WechatOpenId      string
	CellPhone         string
	EmailAddress      string
	EmailValidFlag    string
	LoginFlag         int8
	WrongTimes        int8
	LockFlag          int8
	LastLoginIp       string
	LastLoginDatetime string
	LoginTimes        int
	ValidFlag         string
	TlrEffectDate     string
	AccountEffectDate string
	UserStatus        string
	CreateTell        string
	CreateDatetime    string
	LastMaintDate     string
	LastMaintTime     string
	LastMaintBrno     string
	LastMaintTell     string

	PageNo       int
	PageRecCount int
}
