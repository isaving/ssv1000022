package model

type T_early_warn_task_feedback_tempEvent struct {
	Form []struct {
		FormData T_early_warn_task_feedback_tempFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_early_warn_task_feedback_tempFormData struct {
	TxSn               string
	ExecDate           string
	CustNm             string
	CustNo             string
	CrtfNo             string
	IndvCrtfTypCd      string
	OprorEmpnbr        string
	OprOrgNo           string
	OrgNo              string
	OrgNm              string
	DubilNo            string
	CurrLoanRiskClsfCd string
	DlqcyProb          float64
	RiskRsn            string
	SggestMgmtManrCd   string
	AdjustCrdtQta      float64
	AdjustType         string
	SggestExecWindow   string
	WarnPushManrCd     string
	TaskStartDt        string
	TaskEndDt          string
	CustNo1            string
	CustName1          string
	IndvCrtfNo1        string
	IndvCrtfTypCd1     string
	LoanDubilNo1       string
	AlrdyExecFlg       string
	AtExecWindowFlg    string
	ExecTms            int

	PageNo       int
	PageRecCount int
}
