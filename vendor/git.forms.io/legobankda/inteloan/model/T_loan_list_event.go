package model

type T_loan_listEvent struct {
	Form []struct {
		FormData T_loan_listFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_listFormData struct {
	LoanDubilNo     string
	LoanProdtNo     string
	LoanProdtVersNo string
	AcctnDt         string
	SeqNo           int
	TxSn            string
	MakelnStusCd    string
	CustNo          string
	CustName        string
	MakelnAcctNo    string
	CurCd           string
	LoanAmt         float64
	MakelnOrgNo     string
	TxTm            string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
