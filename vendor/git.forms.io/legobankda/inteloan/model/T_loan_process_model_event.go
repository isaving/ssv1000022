package model

type T_loan_process_modelEvent struct {
	Form []struct {
		FormData T_loan_process_modelFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_process_modelFormData struct {
	ProcesModelId       string
	ProcesModelVersNo   int
	ProcesModelNm       string
	ProcesTmplId        string
	ProcesGraphBase64Cd string
	CrtTelrNo           string
	CrtOrgNo            string
	CrtDt               string
	CrtTm               string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string

	PageNo       int
	PageRecCount int
}
