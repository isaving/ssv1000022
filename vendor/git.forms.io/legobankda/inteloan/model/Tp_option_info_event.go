package model

type Tp_option_infoEvent struct {
	Form []struct {
		FormData Tp_option_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type Tp_option_infoFormData struct {
	OptnNm          string
	AvalFlg         string
	PareClsId       string
	CrtTelrNo       string
	CrtDt           string
	CrtTm           string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	OptionClass     string

	PageNo       int
	PageRecCount int
}
