package model

type T_loan_investigate_task_resultEvent struct {
	Form []struct {
		FormData T_loan_investigate_task_resultFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_investigate_task_resultFormData struct {
	ArtgclInvstgTaskNo    string
	SeqNo                 int
	ReacCustTypCd         string
	ReacCustTms           int
	KeprcdStusCd          string
	PutawyGatherResultCd  string
	CotactTelNo           string
	CurrAddr              string
	Lgtd                  float64
	LgtdDrctCd            string
	Lttd                  float64
	LttdDrctCd            string
	RsdnceCmprmntCd       string
	RsdnceDtlAddr         string
	RsdnceAddr            string
	CotactCmprmntCd       string
	CmunicDtlAddr         string
	CmunicAddr            string
	WechatNo              string
	QqNo                  string
	IndusTypCd            string
	CareerTypCd           string
	WorkUnitNm            string
	DutyCd                string
	AlrdyVrfyMonIncomeAmt float64
	MonIncomeVrfyMthdCd   string
	AlrdyVrfyAsstAmt      float64
	AsstVrfyMthdCd        string
	CmtdInfo              string
	SponsorCustMgrEmpnbr  string
	SponsorOrgNo          string
	CorgCustMgrEmpnbr     string
	CorgOrgNo             string
	DlwthDt               string
	FinlModfyDt           string
	FinlModfyTm           string
	FinlModfyOrgNo        string
	FinlModfyTelrNo       string

	PageNo       int
	PageRecCount int
}
