package model

type T_intrt_acct_infoEvent struct {
	Form []struct {
		FormData T_intrt_acct_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intrt_acct_infoFormData struct {
	AcctiAcctNo       string
	CurCd             string
	OpenAcctOrgNo     string
	IntacrFlg         string
	OpenAcctDt        string
	LsttmIntacrDt     string
	LsttmIntStlDt     string
	LsttmCnDwDt       string
	CnDwCycCd         string
	CnDwCycQty        int
	CnDwIntrFlg       string
	IntacrCycCd       string
	IntacrCycQty      int
	IntacrSpcyDt      string
	IntStlCycCd       string
	IntStlCycQty      int
	IntStlSpcyDt      string
	HoliIntacrIndCd   string
	PmitBkvtFlg       string
	PmitDlydBegintFlg string
	GthrCmpdFlg       string
	FinlChgacctDt     string
	MgmtOrgNo         string
	AcctiOrgNo        string
	FrzAmt            float64
	IntStlPrtyTypCd   string
	DataValidFlgCd    string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
