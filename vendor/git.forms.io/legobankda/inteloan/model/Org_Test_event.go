package model

type Org_TestEvent struct {
	Form []struct {
		FormData Org_TestFormData
		FormHead struct {
			FormID string
		}
	}
}

type Org_TestFormData struct {
	PageNo       int
	PageRecCount int
}
