package model

type T_object_typeEvent struct {
	Form []struct {
		FormData T_object_typeFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_object_typeFormData struct {
	TmplType        string
	GenericTmplNo   string
	SpecialTmplNo   string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
