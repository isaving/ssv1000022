package model

type T_ae_event_tempEvent struct {
	Form []struct {
		FormData T_ae_event_tempFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_event_tempFormData struct {
	AcctnDt         string
	SeqNo           int
	TxTm            string
	BizFolnNo       string
	BizEventCd      string
	SysFolnNo       string
	FundFlgCd       string
	TxOrgNo         string
	AccessChnlCd    string
	SoftProdtCd     string
	ActngOrgNo      string
	AmtDrctCd       string
	ProdtNo         string
	EventUdfInfo    string
	CurCd           string
	Amount          float64
	MansbjTypCd     string
	WastStusCd      string
	AbstCd          string
	WrtffFlg        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	TelrNo          string
	PageNo       int
	PageRecCount int
}
