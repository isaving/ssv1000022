package model

type T_indv_comp_cons_crdtEvent struct {
	Form []struct {
		FormData T_indv_comp_cons_crdtFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_indv_comp_cons_crdtFormData struct {
	CrdtAplySn       string
	CustNo           string
	MonIncomeAmt     float64
	IncomeRemrk      string
	SocietyEnsrNo    string
	AcmltnfundAcctNo string
	TaxpyerIdtfyNo   string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string
	PageNo       int
	PageRecCount int
}
