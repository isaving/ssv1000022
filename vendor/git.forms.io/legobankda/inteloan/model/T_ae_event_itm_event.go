package model

type T_ae_event_itmEvent struct {
	Form []struct {
		FormData T_ae_event_itmFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_event_itmFormData struct {
	BizEventCd      string
	SeqNum          int
	EngineVaritCd   string
	SoftProdtCd     string
	CurCd           string
	AsstClsSubjNo   string
	LiabClsSubjNo   string
	CntptySubjNo    string
	EfftDt          string
	MansbjTypCd     string
	ValidFlg        string
	AcctBookCategCd string
	FundFlgCd       string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
