package model

type T_ae_pro_unexpEvent struct {
	Form []struct {
		FormData T_ae_pro_unexpFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_pro_unexpFormData struct {
	AcctnDt         string
	BizFolnNo       string
	BizEventCd      string
	SysFolnNo       string
	UnuslRsn        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
