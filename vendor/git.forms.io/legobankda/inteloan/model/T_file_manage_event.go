package model

type T_file_manageEvent struct {
	Form []struct {
		FormData T_file_manageFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_file_manageFormData struct {
	DocMgmtNo       string
	MobileNo        string
	LoanProdtNo     string
	CustNo          string
	ClsfLablNm      string
	BizSn           string
	CustName        string
	AtchTypCd       string
	IndvCrtfTypCd   string
	IndvCrtfNo      string
	CtrtNo          string
	LoanDubilNo     string
	AchvsValidFlg   string
	DocPrdusStageCd string
	DocTypNm        string
	DocInfoTypCd    string
	DocNm           string
	DocNo           string
	DocPgNo         string
	DocPath         string
	DocCrtDt        string
	DocCrtTm        string
	DocDescr        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
