package model

type T_lpr_org_sale_quotaEvent struct {
	Form []struct {
		FormData T_lpr_org_sale_quotaFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_lpr_org_sale_quotaFormData struct {
	OrgNo                  string
	SeqNo                  int
	LoanProdtNo            string
	CurCd                  string
	MgctManrCd             string
	BalCtrlTypCd           string
	BalCtrlAmt             float64
	BalCtrlBgnDt           string
	BalCtrlEndDt           string
	NoneDeadlBalCtrlEfftDt string
	KeprcdStusCd           string
	CrtEmpnbr              string
	CrtDt                  string
	ApprvEmpnbr            string
	ApprvDt                string
	ApprvSugstnCd          string
	ApprvSugstnComnt       string
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string
	PageNo       int
	PageRecCount int
}
