package model

type T_loan_onduty_controlEvent struct {
	Form []struct {
		FormData T_loan_onduty_controlFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_onduty_controlFormData struct {
	Empnbr             string
	EmplyName          string
	UserOrgNo          string
	MtguarStusCd       string
	LgnVouchCd         string
	RecntMtguarDt      string
	RecntMtguarTm      string
	RecntLeaveOfficeDt string
	RecntLeaveOfficeTm string

	PageNo       int
	PageRecCount int
}
