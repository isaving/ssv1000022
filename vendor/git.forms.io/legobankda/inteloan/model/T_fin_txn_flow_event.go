package model

type T_fin_txn_flowEvent struct {
	Form []struct {
		FormData T_fin_txn_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_fin_txn_flowFormData struct {
	TxAcctnDt         string
	TxTm              string
	BizFolnNo         string
	SysFolnNo         string
	SeqNo             int
	RcrdacctAcctnDt   string
	RcrdacctAcctnTm   string
	TxOrgNo           string
	TxTelrNo          string
	AuthTelrNo        string
	TxLunchChnlCd     string
	AccessChnlCd      string
	BizSysNo          string
	DebitCrdtFlgCd    string
	CustNo            string
	MerchtNo          string
	CardNoOrAcctNo    string
	AcctiAcctNo       string
	FinTxAmtTypCd     string
	TxCurCd           string
	Amount            float64
	PostvReblnTxFlgCd string
	RvrsTxFlgCd       string
	OrginlHostTxSn    string
	OrginlTxAcctnDt   string
	OrginlTxTelrNo    string
	CustMgrTelrNo     string
	MsgId             string
	AbstCd            string
	Abst              string
	PmitRvrsFlg       string
	FinChgacctFlg     string
	OperTypCd         string
	Pridnum           int
	WrtoffAmt         float64
	TrsctnId          string
	TccStusCd         string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
