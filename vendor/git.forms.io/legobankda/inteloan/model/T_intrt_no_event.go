package model

type T_intrt_noEvent struct {
	Form []struct {
		FormData T_intrt_noFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intrt_noFormData struct {
	IntrtNo 		 string
	OrgNo            string
	InvalidFlg       string
	CurCd            string
	BnchmkIntrt      float64
	EfftDt           string
	TodayModfyFlg    string
	IntrtNm          string
	IntrtMktComnt    string
	IntrtDeadlCycCd  string
	BaseIntrtTypCd   string
	IntrtDeadlCycQty int
	Flag1            string
	Flag2            string
	Bak1             float64
	Bak2             float64
	Bak3             string
	Bak4             string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string
	TccKeprcdVisFlg  string
	PageNo       int
	PageRecCount int
}
