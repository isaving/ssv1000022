package model

type T_loan_productEvent struct {
	Form []struct {
		FormData T_loan_productFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_productFormData struct {
	LoanProdtNo                  string
	LoanProdtVersNo              string
	LoanProdtNm                  string
	DrawDownDate                 string
	StartDt                      string
	MatrDt                       string
	LoanProdtClsfCd              string
	KeprcdStusCd                 string
	QuotaType                    string
	QtaCtrlFlg                   string
	LoanTypCd                    string
	LoanGuarManrCd               string
	CurCd                        string
	AplyLoanDeadl                int
	AplyLoanAmtCeilVal           float64
	BegintDayDtrmnManrCd         string
	ExpdayDtrmnManrCd            string
	RepayManrCd                  string
	IntrtNo                      string
	ProdtIntrtCd                 string
	TranOvdueOperManrCd          string
	IntrtAdjManrCd               string
	PmitAdvRepayFlg              string
	SglacctHighCrdtQta           float64
	NormlLoanChrgSeqCtrlBnch     string
	DvalLoanChrgSeqCtrlBnch      string
	RevneCmpdCd                  string
	BaseIntrtTypCd               string
	OvdueIntrtFlotRatio          float64
	OvdueIntrtFlotManrCd         string
	OvdueIntrtFlotDrctCd         string
	OvdueIntrtBpFlotVal          float64
	EmbIntrtFlotManrCd           string
	EmbIntrtFlotDrctCd           string
	EmbIntrtBpFlotVal            float64
	EmbLoanPnltintIntrtFlotRatio float64
	MinInterestRate              float64
	MaxInterestRate              float64
	FinlModfyDt                  string
	FinlModfyTm                  string
	FinlModfyOrgNo               string
	FinlModifrEmpnbr             string
	PageNo       int
	PageRecCount int
}
