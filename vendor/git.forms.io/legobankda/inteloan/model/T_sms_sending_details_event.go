package model

type T_sms_sending_detailsEvent struct {
	Form []struct {
		FormData T_sms_sending_detailsFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sms_sending_detailsFormData struct {
	SysId          string
	ServeType      string
	BizSn          string
	CustNo		   string
	TelNo          string
	SmsTypCd       string
	SmsTmplNo      string
	SmsDate        string
	SmsTime        string
	SmsCntList     string
	SmsTmplContent string

	PageNo       int
	PageRecCount int
}
