package model

type T_cust_repmt_day_infoEvent struct {
	Form []struct {
		FormData T_cust_repmt_day_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cust_repmt_day_infoFormData struct {
	KeprcdNo        string
	CustNo          string
	LoanProdtNo     string
	KeprcdStusCd    string
	MgmtOrgNo       string
	RepayDtChgFlg   string
	CrtDt           string
	CtrtNo          string
	LoanDubilNo     string
	SpCycCd         string
	SpCycQty        int
	IntStlDtCyc     string
	IntStlCycQty    int
	IntStlDay       string
	SpDay           string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
