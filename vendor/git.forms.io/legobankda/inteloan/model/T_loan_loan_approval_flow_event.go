package model

type T_loan_loan_approval_flowEvent struct {
	Form []struct {
		FormData T_loan_loan_approval_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_loan_approval_flowFormData struct {
	MakelnAplySn            string
	SeqNo                   int
	CustNo                  string
	NodeNm                  string
	TxSn                    string
	ApprvNodeTypCd          string
	OperPersonEmpnbr        string
	OperOrgNo               string
	OperTm                  string
	MakelnApprvOperResultCd string
	AfltInfo                string
	FinlModfyDt             string
	FinlModfyTm             string
	FinlModfyOrgNo          string
	FinlModfyTelrNo         string
	PageNo       int
	PageRecCount int
}
