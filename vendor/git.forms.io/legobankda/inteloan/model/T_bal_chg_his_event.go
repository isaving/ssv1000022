package model

type T_bal_chg_hisEvent struct {
	Form []struct {
		FormData T_bal_chg_hisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_bal_chg_hisFormData struct {
	IntrtAdjSn        string
	CustNo            string
	CustName          string
	CrdtAplySn        string
	IndvCrtfTypCd     string
	IndvCrtfNo        string
	BaseIntrtTypCd    string
	CurCd             string
	IntrtNo           string
	AdjAftBnchmkIntrt float64
	LprIntrt          float64
	IntrtFlotDrctCd   string
	BpFlotVal         int
	KeprcdStusCd      string
	CrtTelrNo         string
	CrtTm             string
	CrtOrgNo          string
	ApprvSugstnCd     string
	ApprvSugstnComnt  string
	ApprvTelrNo       string
	ApprvTm           string
	ApprvOrgNo        string
	IntrtFlotManrCd   string
	IntrtFlotRatio    float64
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
