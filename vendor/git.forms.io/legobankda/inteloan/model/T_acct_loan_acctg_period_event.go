package model

type T_acct_loan_acctg_periodEvent struct {
	Form []struct {
		FormData []T_acct_loan_acctg_periodFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_acct_loan_acctg_periodFormData struct {
	AcctiAcctNo	string
	Pridnum		string
}
