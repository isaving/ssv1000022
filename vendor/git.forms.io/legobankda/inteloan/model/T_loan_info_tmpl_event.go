package model

type T_loan_info_tmplEvent struct {
	Form []struct {
		FormData T_loan_info_tmplFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_info_tmplFormData struct {
	TmplNo            string
	EfftDt            string
	LoanProdtNo       string
	SuitHavegProdtFlg string
	TmplNm            string
	TmplContent       string
	TmplComnt         string
	TextTmplTypCd     string
	PmitSndFlg        string
	AdvSndDays        int
	KeprcdStusCd      string
	SndBgnTm          string
	SndEndTm          string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string

	PageNo       int
	PageRecCount int
}
