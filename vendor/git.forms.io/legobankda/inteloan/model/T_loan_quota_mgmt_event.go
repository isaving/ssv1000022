package model

type T_loan_quota_mgmtEvent struct {
	Form []struct {
		FormData T_loan_quota_mgmtFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_quota_mgmtFormData struct {
	CrdtAplySn            string
	CustNo                string
	LoanProdtVersNo       string
	QtaFinlAdjDt          string
	QtaEfftDt             string
	QtaInvalidDt          string
	PmitOvrqtMakelnFlg    string
	OvrqtMakelnRatio      float64
	LoanGuarManrCd        string
	LoanProdtNo           string
	QtaStusCd             string
	QtaMltpDistrFlg       string
	RevlQtaFlg            string
	QtaCanGuarThirdPtyFlg string
	IndvCrtfTypCd         string
	IndvCrtfNo            string
	CustName              string
	CurCd                 string
	InitQta               float64
	CurrCrdtQta           float64
	FrzQta                float64
	AlrdyUseQta           float64
	QtaFsttmAprvlDt       string
	QtaMatrDt             string
	QtaGrtOrgNo           string
	CrtDt                 string
	CrtEmpnbr             string
	CrtOrgNo              string
	ArtgclAdjTms          int
	LsttmQtaAdjManrCd     string
	FinlModfyDt           string
	FinlModfyTm           string
	FinlModfyOrgNo        string
	FinlModfyTelrNo       string
	PageNo       int
	PageRecCount int
}
