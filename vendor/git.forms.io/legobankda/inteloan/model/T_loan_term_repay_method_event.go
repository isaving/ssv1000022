package model

type T_loan_term_repay_methodEvent struct {
	Form []struct {
		FormData T_loan_term_repay_methodFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_term_repay_methodFormData struct {
	KeprcdNo          string
	CustNo            string
	LoanProdtNo       string
	BrwmnyDeadl       string
	BrwmnyDeadlUnitCd string
	RepayManrCd       string
	KeprcdStusCd      string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
