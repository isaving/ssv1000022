package model

type T_risk_cls_appr_flowEvent struct {
	Form []struct {
		FormData T_risk_cls_appr_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_risk_cls_appr_flowFormData struct {
	LoanDubilNo      string
	TxSn             string
	CustNo           string
	BizAplyNo        string
	AdjAnrCd         string
	BizTypCd         string
	OperPersonEmpnbr string
	OperOrgNo        string
	OperTm           string
	ApprvSugstnCd    string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string


	PageNo       int
	PageRecCount int
}
