package model

type T_ae_daycutEvent struct {
	Form []struct {
		FormData T_ae_daycutFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_daycutFormData struct {
	SoftProdtCd     string
	CurrAcctnDt     string
	LstoneAcctnDt   string
	NxtoneAcctnDt   string
	AcctnDayFlgCd   string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
