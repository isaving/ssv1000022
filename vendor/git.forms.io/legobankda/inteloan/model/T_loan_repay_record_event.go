package model

type T_loan_repay_recordEvent struct {
	Form []struct {
		FormData T_loan_repay_recordFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_repay_recordFormData struct {
	TxSn            string
	QtaChgSn        string
	LoanDubilNo     string
	SeqNo           int
	CustNo          string
	CustName        string
	IndvCrtfTypCd   string
	IndvCrtfNo      string
	AcctiAcctNo     string
	LoanProdtNo     string
	LoanProdtVersNo string
	AcctnDt         string
	RepayAcctNo     string
	RepayTotlAmt    float64
	RepayPrin       float64
	RepayIntr       float64
	RepayPnltint    float64
	RepayCmpndint   float64
	RepayCmpdAmt    float64
	RepbyTotlAmt    float64
	RepayPondg      float64
	RepayOthFee     float64
	RepayCurCd      string
	RepayStusCd     string
	FailRsn         string
	RepayFuncCd     string
	RepayAplyDt     string
	ActlRepayDt     string
	CrtTm           string
	CrtEmpnbr       string
	CrtOrgNo        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int

	TranType        string
	TranDesc        string
	OsgPrin         string
	OsgIntr         string
	Fee             string

}
