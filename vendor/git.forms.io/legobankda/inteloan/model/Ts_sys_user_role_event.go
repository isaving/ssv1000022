package model

type Ts_sys_user_roleEvent struct {
	Form []struct {
		FormData Ts_sys_user_roleFormData
		FormHead struct {
			FormID string
		}
	}
}

type Ts_sys_user_roleFormData struct {
	UserId        string
	RoleId        string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string

	PageNo       int
	PageRecCount int
}
