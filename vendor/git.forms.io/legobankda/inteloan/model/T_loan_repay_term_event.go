package model

type T_loan_repay_termEvent struct {
	Form []struct {
		FormData T_loan_repay_termFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_repay_termFormData struct {
	LoanDubilNo           string
	SeqNo                 int
	RecalDt               string
	CustNo                string
	RemainPrin            float64
	ActlstPrin            float64
	ActlstIntr            float64
	Pridnum               int
	KeprcdStusCd          string
	CurrPeriodRecalFlg    string
	CurrPeriodIntacrBgnDt string
	CurrPeriodIntacrEndDt string
	RepayDt               string
	PlanRepayTotlAmt      float64
	PlanRepayPrin         float64
	PlanRepayIntr         float64
	Fee                   float64
	FinlModfyDt           string
	FinlModfyTm           string
	FinlModfyOrgNo        string
	FinlModfyTelrNo       string
	PageNo                int
	PageRecCount          int
}
