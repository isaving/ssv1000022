package model

type T_ae_prodEvent struct {
	Form []struct {
		FormData T_ae_prodFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_prodFormData struct {
	SoftProdtCd     string
	ProdtNm         string
	EfftDt          string
	ValidFlg        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
