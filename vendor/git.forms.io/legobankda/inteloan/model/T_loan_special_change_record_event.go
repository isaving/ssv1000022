package model

type T_loan_special_change_recordEvent struct {
	Form []struct {
		FormData T_loan_special_change_recordFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_special_change_recordFormData struct {
	NmSnglTypCd     string
	OrgNo           string
	NmSnglNo        string
	NmSnglSeqNo     int
	SorcDescr       string
	RecmndEmpnbr    string
	CustNo          string
	CustName        string
	IndvCrtfTypCd   string
	IndvCrtfNo      string
	MobileNo        string
	OperTypCd       string
	OperProcesCd    string
	OperDt          string
	ApprvSugstnCd   string
	RplshInfo       string
	TxSn            string
	OprOrgNo        string
	OprorEmpnbr     string
	CrtDt           string
	CrtEmpnbr       string
	CrtOrgNo        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
