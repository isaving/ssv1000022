package model

type Response struct {
	Form []Data
}

type Data struct {
	FormHead struct {
		FormId string
	}
	FormData map[string]interface{}
}

type ArrayResponse struct {
	Form []Datas
}

type Datas struct {
	FormHead struct {
		FormId string
	}
	FormData []map[string]interface{}
}

type PageResponse struct {
	Form []PageDatas
}
type PageDatas struct {
	FormHead struct {
		FormId string
	}
	FormData struct {
		PageTotCount int
		PageNo       int
		Records      []map[string]interface{}
	}
}

type ErrorResponse struct {
	Form []ErrorMsg
}

type ErrorMsg struct {
	FormHead struct {
		FormId string
	}
	FormData struct {
		RetMsgCode string
		RetAuthLvl string
		RetMessage string
	}
}
