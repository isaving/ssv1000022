package model

type T_loan_incoming_infoEvent struct {
	Form []struct {
		FormData T_loan_incoming_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_incoming_infoFormData struct {
	CrdtAplySn           string
	CustNo               string
	StrQtyCustFlg        string
	AplyTypCd            string
	RecomCustMgrEmpnbr   string
	ApyfLprOrgNo         string
	CrdtAplyDt           string
	InitCredQta          float64
	AplyChnlCd           string
	LoanProdtNo          string
	LoanProdtVersNo      string
	LoanProdtNm          string
	QtaMltpLendFlg       string
	WhtlPreCrdtQta       float64
	AplyCrdtQta          float64
	LoanCurCd            string
	LoanUsageCd          string
	LoanUsageRplshDescr  string
	LoanInvtIndusCd      string
	CrdtAplyStusCd       string
	CrdtAplyTm           string
	SponsorCustMgrEmpnbr string
	CorgCustMgrEmpnbr    string
	ActngMgmtOrgNo       string
	FinlModfyDt          string
	FinlModfyTm          string
	FinlModfyOrgNo       string
	FinlModfyTelrNo      string

	PageNo       int
	PageRecCount int
}
