package model

type Ts_sys_funcEvent struct {
	Form []struct {
		FormData Ts_sys_funcFormData
		FormHead struct {
			FormID string
		}
	}
}

type Ts_sys_funcFormData struct {
	FuncNo               string
	OrdNo          		 int
	SoftProdtCd          string
	FuncUrlPath          string
	FuncLablEngNm        string
	FuncLablLoclLangugNm string
	ButnKeyVal           string
	FuncRouteSeqNo       int
	LstoneBrthNodeFuncNo string
	MgmtPageNextButnFlg  string
	SprFuncId            string
	NeedLgnAccessFlg     string
	NeedAuthAccessFlg    string
	FuncTypCd            string
	MenuTypCd 			 string
	FuncValidFlg         string
	UserTaskNodeFlg      string
	Remrk                string
	MenuIconPath         string
	FinlModfyDt          string
	FinlModfyTm          string
	FinlModfyOrgNo       string
	FinlModfyTelrNo      string

	PageNo       int
	PageRecCount int
}
