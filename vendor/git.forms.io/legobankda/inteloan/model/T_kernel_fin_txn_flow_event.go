package model

type T_kernel_fin_txn_flowEvent struct {
	Form []struct {
		FormData T_kernel_fin_txn_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_kernel_fin_txn_flowFormData struct {
	BizSn	          string
	TxAcctnDt         string
	TxTm              string
	BizFolnNo         string
	SysFolnNo         string
	TxOrgNo           string
	TxTelrNo          string
	AuthTelrNo        string
	TxLunchChnlCd     string
	AccessChnlCd      string
	BizSysSoftProdtCd string
	DebitCrdtFlg      string
	CardNoOrAcctNo    string
	CntptyAcctNo      string
	AcctiAcctNo       string
	TxCurCd           string
	Amount            float64
	OrginlTxAcctnDt   string
	OrginlBizFolnNo   string
	OrginlTxTelrNo    string
	TxStusCd          string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
