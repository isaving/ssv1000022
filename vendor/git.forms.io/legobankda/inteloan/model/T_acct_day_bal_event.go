package model

type T_acct_day_balEvent struct {
	Form []struct {
		FormData T_acct_day_balFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_acct_day_balFormData struct {
	AcctiAcctNo     string
	Pridnum         int
	ChgacctDt       string
	BalTypCd        string
	AcctStusCd      string
	IntacrPrinBal   float64
	WrtoffAmt       float64
	DataValidFlgCd  string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
