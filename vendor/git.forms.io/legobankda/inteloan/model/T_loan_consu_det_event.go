package model

type T_loan_consu_detEvent struct {
	Form []struct {
		FormData T_loan_consu_detFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_consu_detFormData struct {
	FlowId         int
	CustId     	   string
	TradeType  	   string
	LoanAcctNo 	   string
	ConsuType  	   string
	MercNo         string
	MercName       string
	OrderNo        string
	DepAcctNo      string
	ProdId         string
	ProdVersion    string
	ConsuDate      string
	ConsuAmt       float64
	Currency       string
	Cost           float64
	TranFlowNo     string
	AppConsuTime   string
	ActualLoanTime string
	LastMaintDate  string
	LastMaintTime  string
	LastMaintBrno  string
	LastMaintTell  string
	PageNo       int
	PageRecCount int
}
