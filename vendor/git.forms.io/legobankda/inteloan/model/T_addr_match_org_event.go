package model

type T_addr_match_orgEvent struct {
	Form []struct {
		FormData T_addr_match_orgFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_addr_match_orgFormData struct {
	KeprcdNo                 string
	SeqNo                    int
	ProvAdminCmprmntCd       string
	PrvMncpltyAdminCmprmntNm string
	CityAdminCmprmntCd       string
	CityAdminCmprmntNm       string
	ZoneOrCntyAdminCmprmntCd string
	ZoneOrCntyAdminCmprmntNm string
	LprOrgNo                 string
	CrtTelrNo                string
	CrtDt                    string
	CrtOrgNo                 string
	KeprcdStusCd             string
	FinlModfyDt              string
	FinlModfyTm              string
	FinlModfyOrgNo           string
	FinlModfyTelrNo          string

	PageNo       int
	PageRecCount int
}
