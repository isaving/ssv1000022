package model

type T_loan_orgaz_daily_flowEvent struct {
	Form []struct {
		FormData T_loan_orgaz_daily_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_orgaz_daily_flowFormData struct {
	AcctnDt         string
	TxSn            string
	TxDt            string
	TxTm            string
	TxChnlCd        string
	LprOrgNo        string
	OprOrgNo        string
	OprorTelrNo     string
	TerminalNo      string
	AuthTelrNo      string
	LoanProdtNo     string
	CurCd           string
	CustNo          string
	TxTypCd         string
	TxAmt           float64
	Prin            float64
	Intr            float64
	Pondg           float64
	OthFee          float64
	RvrsIndCd       string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
