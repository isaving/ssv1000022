package model

type T_ae_jnl_entryEvent struct {
	Form []struct {
		FormData T_ae_jnl_entryFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_jnl_entryFormData struct {
	JrnlzId            string
	AcctnDt            string
	BizFolnNo          string
	SysFolnNo          string
	TxTm               string
	TxOrgNo            string
	ActngOrgNo         string
	CurCd              string
	DebitCrdtFlgCd     string
	TxAmt              float64
	SubjNo             string
	ReqePtySoftProdtCd string
	KepacctSuccFlg     string
	AbstCd             string
	EngineVaritCd      string
	AcctBookCategCd    string
	SumId              string
	WrtffFlg           string
	FinlModfyDt        string
	FinlModfyTm        string
	FinlModfyOrgNo     string
	FinlModfyTelrNo    string
	PageNo       int
	PageRecCount int
}
