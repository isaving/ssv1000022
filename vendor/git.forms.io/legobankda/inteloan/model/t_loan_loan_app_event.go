package model

type T_loan_loan_appEvent struct {
	Form []struct {
		FormData T_loan_loan_appFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_loan_appFormData struct {
	MakelnAplySn              string
	CrdtAplySn                string
	LoanDubilNo               string
	CtrtNo                    string
	CustNo                    string
	CustName                  string
	IndvCrtfTypCd             string
	IndvCrtfNo                string
	OthConsmTypCd             string
	UsageRplshComnt           string
	DpstAcctNo                string
	LoanProdtNo               string
	LoanProdtVersNo           string
	LoanAmt                   float64
	LoanCurCd                 string
	Fee                       float64
	RprincPayIntCycCd         string
	LoanDeadl                 int
	LoanDeadlUnitCd           string
	RepayManrCd               string
	MakelnAplyStusCd          string
	AplyMakelnDt              string
	ActlMakelnTm              string
	ExecIntrt                 float64
	RgtsintTypCd              string
	RgtsintNo                 string
	LoanUsageCmtd             string
	BaseIntrtTypCd            string
	IntrtNo                   string
	BnchmkIntrt               float64
	IntrtFlotDrctCd           string
	BpFlotVal                 float64
	IntrtFlotRatio            float64
	FixdIntrtPricingManrCd    string
	IntrtFlotManrCd           string
	RelaAcctOpnAcctBnkBnkNo   string
	RelaAcctOpnAcctBnkBnkName string
	RelaAcctBlngtoRwCategCd   string
	RelaAcctNoTypCd           string
	RsdnceProvScAdcmCd        string
	RsdnceCityScAdcmCd        string
	RsdnceCntyScAdcmCd        string
	RsdnceTwnScAdcmCd         string
	RsdnceVillgrpScAdcmCd     string
	RsdnceDtlAddr             string
	MakelnManrCd              string
	QtaChgSn                  string
	LoanIntrtAdjCycCd         string
	LoanIntrtAdjCycQty        int
	LoanIntrtAdjManrCd        string
	CareerTypCd               string
	EMail                     string
	FinlModfyDt               string
	FinlModfyTm               string
	FinlModfyTelrNo           string
	FinlModfyOrgNo            string
	TccState                  int8
	LoanGuarManrCd            string
	GuarName                  string
	GuarIdType                string
	GuarIdNo                  string
	GuarPassCountry           string
	LoanProdtNm               string
	LoanPur                   string
	MatrDt                    string
	RepayDay                  string
	LoanStatus                string
	LoanChannel               string
	RepayCyc                  string
	AuotoRepyFlag             string
	RepayDt                   string
	AppFee                    float64
	ProcFee                   float64
	OverFee                   float64
	AdvRepayFee               float64
	ExtsnFee                  float64
	RepayPlanFee              float64
	Tax                       float64
	MakelnEmpnbr              string
	MakelnComnt               string
	MakelnOrgNo               string
	ActlMakelnDt              string
	AplyMakelnTm              string
	PageNo                    int
	PageRecCount              int
}
