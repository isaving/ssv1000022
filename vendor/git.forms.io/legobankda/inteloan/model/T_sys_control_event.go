package model

type T_sys_controlEvent struct {
	Form []struct {
		FormData T_sys_controlFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sys_controlFormData struct {
	InstId             string
	SystemStatus       string
	CurrentProcessDate string
	LastProcessDate    string
	NextProcessDate    string
	FiscalYearEndDate  string
	BatchId            int
	DateFormatCode     string
	DateSeparator      string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string

	PageNo       int
	PageRecCount int
}
