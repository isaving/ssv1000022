package model

type T_cust_identityEvent struct {
	Form []struct {
		FormData T_cust_identityFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cust_identityFormData struct {
	CustId          string
	IdType          string
	IdNo            string
	IssueAuthority  string
	IssueDate       string
	IdExpDate       string
	LastMaintDate     string
	LastMaintTime     string
	LastMaintBrno  string
	LastMaintTell string

	PageNo       int
	PageRecCount int
}
