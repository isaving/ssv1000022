package model

type T_ae_jnl_entry_sumEvent struct {
	Form []struct {
		FormData T_ae_jnl_entry_sumFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_jnl_entry_sumFormData struct {
	SumId               string
	TxAcctnDt           string
	BizFolnNo           string
	ReqeRcrdacctAcctnDt string
	SysFolnNo           string
	TxOrgNo             string
	ActngOrgNo          string
	CurCd               string
	DebitCrdtFlgCd      string
	TxAmt               float64
	SubjNo              string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string
	PageNo       int
	PageRecCount int
}
