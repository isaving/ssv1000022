package model

type T_chln_hndl_flowEvent struct {
	Form []struct {
		FormData T_chln_hndl_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_chln_hndl_flowFormData struct {
	LoanDubilNo      string
	TxSn             string
	CustNo           string
	NodeNm           string
	BizAplyNo        string
	BizTypCd         string
	OperPersonEmpnbr string
	OperOrgNo        string
	OperTm           string
	ApprvSugstnCd    string
	SugstnDescr      string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string

	PageNo       int
	PageRecCount int
}
