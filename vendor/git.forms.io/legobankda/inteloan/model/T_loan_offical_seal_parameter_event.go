package model

type T_loan_offical_seal_parameterEvent struct {
	Form []struct {
		FormData T_loan_offical_seal_parameterFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_offical_seal_parameterFormData struct {
	ComslNo         string
	DgtCdtlId       string
	OrgNo           string
	LoanProdtNo     string
	ComslNm         string
	ComslPictInfo   string
	CrtTelrNo       string
	CrtOrgNo        string
	CrtDt           string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
