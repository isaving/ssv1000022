package model

type T_sms_template_tempEvent struct {
	Form []struct {
		FormData T_sms_template_tempFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sms_template_tempFormData struct {
	TmplNo          string
	SmsTypCd        string
	SmsTmplStusCd   string
	EfftDt          string
	SmsTmplNm       string
	SmsTmplContent  string
	SmsTmplRmk      string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
