package model

type T_adj_contract_regEvent struct {
	Form []struct {
		FormData T_adj_contract_regFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_adj_contract_regFormData struct {
	CtrtNo         string
	CustNo          string
	IndvCrtfTypCd   string
	IndvCrtfNo      string
	CtrtStusCd      string
	CtrtStartDt     string
	CtrtMatrDt      string
	ApprvDt         string
	SignOrgNo       string
	OprOrgNo        string
	OprorEmpnbr     string
	ComslNo         string
	TmplNo          string
	CtrtImgNo       string
	BrwmnyCtrtTypCd string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	TccState        int

	PageNo       int
	PageRecCount int
}
