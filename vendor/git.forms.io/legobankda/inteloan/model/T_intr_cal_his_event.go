package model

type T_intr_cal_hisEvent struct {
	Form []struct {
		FormData T_intr_cal_hisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intr_cal_hisFormData struct {
	IntacrRuleNo      string
	EfftDt            string
	InvalidDt         string
	IntacrAgrthmTypCd string
	Flag              string
	Bak               string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	TccKeprcdVisFlg   string
	PageNo       int
	PageRecCount int
}
