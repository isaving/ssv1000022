package model

type T_appr_mode_paraEvent struct {
	Form []struct {
		FormData T_appr_mode_paraFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_appr_mode_paraFormData struct {
	OrgNo                         string
	SeqNo                         int
	LoanProdtNo                   string
	LoanProdtVersNo               string
	LoanGuarManrCd                string
	KeprcdStusCd                  string
	OrgNm                         string
	LoanProdtNm                   string
	QtaTypCd                      string
	LnrwSmlGroupPeoNo             int
	LonRvwPeoNo                   int
	IdpnApprvrModeFloorAmt        float64
	LnrwSmlGroupModeFloorAmt      float64
	LonRvwModeFloorAmt            float64
	IdpnApprvrModeAmtGtValScpCd   string
	LnrwSmlGroupModeAmtGtValScpCd string
	LonRvwModeAmtGtValScpCd       string
	IdpnApprvrCeilAmt             float64
	LnrwSmlGroupModeCeilAmt       float64
	LonRvwModeCeilAmt             float64
	EfftDt                        string
	ExpiryDt                      string
	Remrk                         string
	CrtDt                         string
	CrtEmpnbr                     string
	CrtOrgNo                      string
	FinlModfyDt                   string
	FinlModfyTm                   string
	FinlModfyOrgNo                string
	FinlModfyTelrNo               string

	PageNo       int
	PageRecCount int
}
