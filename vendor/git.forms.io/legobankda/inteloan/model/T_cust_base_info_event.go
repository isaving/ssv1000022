package model

type T_cust_base_infoEvent struct {
	Form []struct {
		FormData T_cust_base_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_cust_base_infoFormData struct {
	CustId             string
	CustName           string
	CustEname          string
	OtherName          string
	LastName           string
	Appellation        string
	CustType           string
	CustSubclass       string
	OrgType            string
	Industry           string
	Gender             string
	MaritalStatus      string
	MobileAreaCode     string
	Email              string
	AcctManager        string
	Country            string
	ResidenceCountry   string
	Nationality        string
	IsAmerican         string
	CustStatus         string
	IsDataComplete     string
	Birthday           string
	Lang               string
	IsLocal            string
	MarketTarget       string
	IsOwnEmployees     string
	OwnEmployeesNumber string
	EmployeesCode      string
	SocialInsurance    string
	ReferenceNumber    string
	TaxRateCountry     string
	MediumCode         string
	IsFreeTradeZone    string
	OwnOrgId           string
	CustCreatDate      string
	AuthInfo           string
	RecordSerialNo     int
	TranOrgId          string
	Operator           string
	AuthUser           string
	LastMaintDate        string
	LastMaintTime        string
	LastMaintBrno     string
	LastMaintTell    string
	PageNo       int
	PageRecCount int
}
