package model

type T_risk_cls_adj_regisEvent struct {
	Form []struct {
		FormData T_risk_cls_adj_regisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_risk_cls_adj_regisFormData struct {
	LoanDubilNo                      string
	AdjDt                            string
	SeqNo                            int
	CustNo                           string
	LoanRiskClsfAdjPrin              float64
	LoanDeadl                        int
	RepayManrCd                      string
	LoanBal                          float64
	BegDt                            string
	MatrDt                           string
	OnshetIntr                       float64
	InshetIntr                       float64
	PrinDueBgnDt                     string
	IntDueBgnDt                      string
	DuePrin                          float64
	DueInt                           float64
	CurrDuePridnum                   int
	HighDuePridnum                   int
	AccmDuePridnum                   int
	AdjBefCd                         string
	CurtprdCompEtimLnRiskClsfCd      string
	CurtprdArtgclIdtfyLoanRiskClsfCd string
	CurtprdCompEtimLnRiskClsfDt      string
	CurtprdArtgclIdtfyLoanRiskClsfDt string
	InendIdtfyLoanRiskClsfCd         string
	AdjManrCd                        string
	AdjPersonEmpnbr                  string
	AdjPersonOrgNo                   string
	AdjBefLoanRiskClsfCd             string
	AdjAftLoanRiskClsfCd             string
	CurtprdIdtfyLoanRiskClsfCd       string
	CurtprdIdtfyLoanRiskClsfDt       string
	LoanRiskClsfStusCd               string
	SugstnDescr                      string
	FinlModfyDt                      string
	FinlModfyTm                      string
	FinlModfyOrgNo                   string
	FinlModfyTelrNo                  string


	PageNo       int
	PageRecCount int
}
