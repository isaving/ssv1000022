package model

type AcsjhfpEvent struct {
	Form []struct {
		FormData AcsjhfpFormData
		FormHead struct {
			FormID string
		}
	}
}

type AcsjhfpFormData struct {
	PageNo       int
	PageRecCount int
}
