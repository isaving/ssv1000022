package model

type T_common_userEvent struct {
	Form []struct {
		FormData T_common_userFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_common_userFormData struct {
	UserId             string
	Status             string
	Name               string
	OrgId              string
	EmployeeId         string
	RoleId             string
	UserCategory       string
	UserLvl            string
	SuperiorUser       string
	BillStatus         string
	UserPassword       string
	PasswordErrorCount int
	PasswordUpdateDate string
	CreateDate         string
	CanceDate          string
	CreateTeller       string
	CanceTeller        string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string

	PageNo       int
	PageRecCount int
}
