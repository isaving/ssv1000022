package model

type T_ae_bal_subj_unexpEvent struct {
	Form []struct {
		FormData T_ae_bal_subj_unexpFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_bal_subj_unexpFormData struct {
	AcctnDt         string
	SubjNo          string
	AcctBookCategCd string
	SubjNm          string
	SubjStusCd      string
	SubjBal         float64
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
