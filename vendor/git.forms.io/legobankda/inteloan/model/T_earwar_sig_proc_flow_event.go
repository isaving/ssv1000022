package model

type T_earwar_sig_proc_flowEvent struct {
	Form []struct {
		FormData T_earwar_sig_proc_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_earwar_sig_proc_flowFormData struct {
	TaskNo                  string
	SeqNo                   int
	CustNo                  string
	AfBnkLnBizTypCd         string
	OperPersonEmpnbr        string
	OperTypCd               string
	OperOrgNo               string
	OperDt                  string
	OperBefAfBnkLnJobStusCd string
	TaskDlwthStusCd         string
	AfBnkLnJobDlwthResultCd string
	OperResultComnt         string
	TxSn                    string
	TaskDlwthResultNo       string
	FinlModfyDt             string
	FinlModfyTm             string
	FinlModfyOrgNo          string
	FinlModfyTelrNo         string

	PageNo       int
	PageRecCount int
}
