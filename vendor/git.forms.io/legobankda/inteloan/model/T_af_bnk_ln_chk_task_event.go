package model

type T_af_bnk_ln_chk_taskEvent struct {
	Form []struct {
		FormData T_af_bnk_ln_chk_taskFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_af_bnk_ln_chk_taskFormData struct {
	ChkTaskNo            string
	CustNo               string
	CustName             string
	CtrtNo               string
	LoanDubilNo          string
	AfBnkLnChkTaskTypCd  string
	AfBnkLnChkManrCd     string
	TaskGnrtDt           string
	CrtEmpnbr            string
	AprsEmplyName        string
	CrtOrgNo             string
	AprsOrgNm            string
	TaskDlwthStusCd      string
	AlrdyExecFlg         string
	AtExecWindowFlg      string
	ExecTms              int
	ExecRsn              string
	ExecConcusComnt      string
	ChkSponsorEmpnbr     string
	ChkSponsorOrgNo      string
	ChkCorgEmpnbr        string
	ChkCorgOrgNo         string
	AprsSugstnCd         string
	RctfctnSugstnContent string
	AprsEmpnbr           string
	AprsOrgNo            string
	AprsDt               string
	SponsorEmplyName     string
	CorgEmplyName        string
	SponsorOrgNm         string
	CorgOrgNm            string
	SponsorEmplyTaskNo   string
	CorgEmplyTaskNo      string
	TaskDlwthResultNo    string
	AprsEmplyTaskNo      string
	ReacTotlTms          int
	FinlModfyDt          string
	FinlModfyTm          string
	FinlModfyOrgNo       string
	FinlModfyTelrNo      string

	PageNo       int
	PageRecCount int
}

type T_af_bnk_ln_chk_taskEvent1 struct {
	Form []struct {
		FormData struct {
			PageNo       int
			PageRecCount int
		}
		FormHead struct {
			FormID string
		}
	}
}
