package model

type T_loan_link_acctEvent struct {
	Form []struct {
		FormData T_loan_link_acctFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_link_acctFormData struct {
	RelaAcctId                string
	LoanProdtNo               string
	CtrtNo                    string
	RelaAcctBlngtoRwCategCd   string
	CustNo                    string
	RelaAcctNoTypCd           string
	DfltRepayAcctFlg          string
	LoanDubilNo               string
	RelaAcctNo                string
	IndvCrtfTypCd             string
	IndvCrtfNo                string
	RelaDt                    string
	KeprcdStusCd              string
	RelaAcctOpnAcctBnkBnkNo   string
	RelaAcctOpnAcctBnkBnkName string
	FinlModfyDt               string
	FinlModfyTm               string
	FinlModfyOrgNo            string
	FinlModfyTelrNo           string

	PageNo       int
	PageRecCount int
}
