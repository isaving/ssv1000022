package model

type T_org_orgi_busi_paraEvent struct {
	Form []struct {
		FormData T_org_orgi_busi_paraFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_org_orgi_busi_paraFormData struct {
	KeprcdNo        string
	OrgNo           string
	LoanProdtNo     string
	OprBrchOrgNo    string
	CrtTelrNo       string
	CrtDt           string
	CrtOrgNo        string
	ValidFlg        string
	Remrk           string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
