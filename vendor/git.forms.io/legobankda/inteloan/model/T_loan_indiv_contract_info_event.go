package model

type T_loan_indiv_contract_infoEvent struct {
	Form []struct {
		FormData T_loan_indiv_contract_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_indiv_contract_infoFormData struct {
	CtrtNo                   string
	LoanDubilNo              string
	CustNo                   string
	CustName                 string
	LoanProdtNo              string
	IndvCrtfTypCd            string
	IndvCrtfNo               string
	ExecIntrt                float64
	IntrtFlotManrCd          string
	BpFlotVal                float64
	IntrtFlotRatio           float64
	CtrtTypCd                string
	LoanProdtNm              string
	LoanGuarManrCd           string
	CurCd                    string
	AprvAmt                  float64
	CtrtAmt                  float64
	AprvDeadlMons            int
	CtrtDeadlMons            int
	LoanUsageCd              string
	BrwmnyUsageComnt         string
	SpmeCntLoanRiskClsfCd    string
	SpmeCntLoanRiskClsfComnt string
	SpdmnyManrCd             string
	AutonPaymtLmt            float64
	PayWayCd                 string
	AutonPaymtAcctNo         string
	AutonPaymtAcctNm         string
	PaymtRptsCycCd           string
	PaymtRptsCycQty          int
	RepayManrCd              string
	IntStlManrCd             string
	IntacrManrCd             string
	TranOvdueGraceDays       int
	ChrgManrCd               string
	EntrstChrgAcctNo         string
	AcctNm                   string
	RepaySorcComnt           string
	IntSubsidyRatio          float64
	IntSubsidyExpiryDt       string
	BilsAddr                 string
	PostCd                   string
	EMail                    string
	CotactTelNo              string
	MobileNo                 string
	DsptSltnManrCd           string
	ArbitCommNm              string
	ArbitCommAddr            string
	MakelnPmseCondComnt      string
	OthApntMtsComnt          string
	KepacctOrgNo             string
	OprOrgNo                 string
	OprorEmpnbr              string
	FinlModfyDt              string
	FinlModfyTm              string
	FinlModfyOrgNo           string
	FinlModfyTelrNo          string
	PageNo       int
	PageRecCount int
}
