package model

type Ts_sys_role_funcEvent struct {
	Form []struct {
		FormData Ts_sys_role_funcFormData
		FormHead struct {
			FormID string
		}
	}
}

type Ts_sys_role_funcFormData struct {
	RoleNo          string
	FuncNo          string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
