package model

type T_ae_itmEvent struct {
	Form []struct {
		FormData T_ae_itmFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_itmFormData struct {
	SubjNo           string
	AcctBookCategCd  string
	SubjEngNm        string
	SubjNm           string
	SubjCategCd      string
	KepacctManrCd    string
	AtmtcGnrtGlFlg   string
	DtlSubjFlg       string
	SubjBalZeroFlgCd string
	BalDrctCd        string
	SubjStusCd       string
	SubjOpenAcctDt   string
	EfftDt           string
	SubjHeavyOpenDt  string
	DeregisDt        string
	InvalidDt        string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string
	PageNo       int
	PageRecCount int
}
