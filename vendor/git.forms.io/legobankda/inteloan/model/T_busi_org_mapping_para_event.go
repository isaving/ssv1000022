package model

type T_busi_org_mapping_paraEvent struct {
	Form []struct {
		FormData T_busi_org_mapping_paraFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_busi_org_mapping_paraFormData struct {
	KeprcdNo        string
	SceneTypCd      string
	OrginlOrgNo     string
	OprOrgNo        string
	LoanProdtNo     string
	KeprcdStusCd    string
	CrtTelrNo       string
	CrtDt           string
	CrtOrgNo        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
