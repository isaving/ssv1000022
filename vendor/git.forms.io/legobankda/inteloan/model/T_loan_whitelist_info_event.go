package model

type T_loan_whitelist_infoEvent struct {
	Form []struct {
		FormData T_loan_whitelist_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_whitelist_infoFormData struct {
	TxReqeCd            string
	ChnlNoCd            string
	BizSysNo            string
	RcrdacctFlgCd       string
	DsmtJrnlzTypCd      string
	InterTranCd         string
	LiqdBizTypCd        string
	Bak1                string
	Bak2                string
	Bak3                string
	DsmtJrnlzExecAgngCd string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string

	PageNo       int
	PageRecCount int
}
