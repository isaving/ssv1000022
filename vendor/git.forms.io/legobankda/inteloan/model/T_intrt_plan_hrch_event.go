package model

type T_intrt_plan_hrchEvent struct {
	Form []struct {
		FormData T_intrt_plan_hrchFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intrt_plan_hrchFormData struct {
	IntacrRuleNo    string
	HrchQty         int
	DpstDeadlCd     string
	HrchAmt         float64
	UseIntrtSorcCd  string
	IntrtNo         string
	FixdIntrt       float64
	FixdIntrtEfftDt string
	IntacrPrtyTypCd string
	LowestIntrt     float64
	IntrtFlotManrCd string
	FlotDrctCd      string
	FlotRate        float64
	Flag1           string
	Flag2           string
	Bak1            float64
	Bak2            float64
	Bak3            string
	Bak4            string
	Bak5            string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	TccKeprcdVisFlg string
	PageNo       int
	PageRecCount int
}
