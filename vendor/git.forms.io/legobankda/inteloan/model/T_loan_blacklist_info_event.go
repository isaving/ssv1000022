package model

type T_loan_blacklist_infoEvent struct {
	Form []struct {
		FormData T_loan_blacklist_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_blacklist_infoFormData struct {
	OrgNo                string
	SeqNo                int
	BlklistNo            string
	BizSn                string
	SorcDescr            string
	BlklistTypCd         string
	CustNmlstNewaddTypCd string
	RecmndEmpnbr         string
	RecmndNm	           string
	MobileNo             string
	CustNo               string
	CustName             string
	IndvCrtfTypCd        string
	IndvCrtfNo           string
	BlklistCustClsfCd    string
	BckltRsnCd           string
	BckltRsnComnt        string
	KeprcdStusCd         string
	DeregisRsn           string
	CrtDt                string
	CrtEmpnbr            string
	CrtOrgNo             string
	FinlModfyDt          string
	FinlModfyTm          string
	FinlModfyOrgNo       string
	FinlModfyTelrNo      string

	PageNo       int
	PageRecCount int
}
