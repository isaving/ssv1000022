package model

type T_loan_repayment_billEvent struct {
	Form []struct {
		FormData T_loan_repayment_billFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_repayment_billFormData struct {
	LoanDubilNo            string
	Pridnum                int
	CustNo                 string
	KeprcdStusCd           string
	OperSorcCd             string
	PrinOvdueBgnDt         string
	IntrOvdueBgnDt         string
	CurrPeriodUseGraceDays int
	ActlstTotlAmt          float64
	ActlRepayPrin          float64
	ActlRepayIntr          float64
	FinlRepayDt            string
	ActlPyfDt              string
	RepayDt                string
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string

	PageNo       int
	PageRecCount int
}
