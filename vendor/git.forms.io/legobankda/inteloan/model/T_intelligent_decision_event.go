package model

type T_intelligent_decisionEvent struct {
	Form []struct {
		FormData T_intelligent_decisionFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intelligent_decisionFormData struct {
	CrdtAplySn            string
	BizTriggerTypCd       string
	LoanCharcCd           string
	LoanProdtNo           string
	LoanProdtVersNo       string
	AplyCrdtQta           float64
	TerminalTypCd         string
	OprorEmpnbr           string
	ChnlTypCd             string
	IndvCrtfNo            string
	IndvCrtfTypCd         string
	CustName              string
	CustNo                string
	RsdnceDtlAddr         string
	Age                   int
	GenderCd              string
	MarrgSituationCd      string
	EdctbkgCd             string
	EntrInsdbnkBlklistFlg string
	EntrInsdbnkWhtlFlg    string
	PreCrdtQta            float64
	MobileNo              string
	TxResultStusCd        string
	HitExcludRuleComnt    string
	BlklistTypCd          string
	DcmkTypCd             string
	DcmkDescr             string
	ScoreCardGrpgInfo     string
	EvaltScr              float64
	CreditRatCd           string
	NegtRsn               string
	MonIncomeAmt          float64
	SysSggestCrdtQta      float64
	Bp                    int
	BpFlotDrctCd          string
	AplyDt		          string
	PageNo       int
	PageRecCount int
}
