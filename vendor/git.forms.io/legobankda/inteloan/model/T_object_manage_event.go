package model

type T_object_manageEvent struct {
	Form []struct {
		FormData T_object_manageFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_object_manageFormData struct {
	ObjectType      string
	TargetObject    string
	ObjectKeyType   string
	ObjectKey       string
	SerialNo        int
	ObjectContent   string
	ChannelType     string
	ContactName     string
	TopicId         string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
