package model

type T_chan_lend_expend_infoEvent struct {
	Form []struct {
		FormData T_chan_lend_expend_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_chan_lend_expend_infoFormData struct {
	KeprcdNo        string
	CustNo          string
	LoanProdtNo     string
	CtrtNo          string
	ForsprtDt       string
	ForsprtEquipNo  string
	KeprcdStusCd    string
	CrtDt           string
	ForsprtAmt      float64
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
