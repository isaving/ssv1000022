package model

type T_loan_provident_fund_real_estate_infoEvent struct {
	Form []struct {
		FormData T_loan_provident_fund_real_estate_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_provident_fund_real_estate_infoFormData struct {
	KeprcdNo           string
	ArtgclInvstgTaskNo string
	SeqNo              int
	RecntSavedMon      string
	PermonSavedAmt     float64
	HuprpNm            string
	HsCharcCd          string
	PurhDt             string
	PurhAmt            float64
	CurrWorth          float64
	Remrk              string
	PageNo       int
	PageRecCount int
}
