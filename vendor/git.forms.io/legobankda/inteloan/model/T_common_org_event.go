package model

type T_common_orgEvent struct {
	Form []struct {
		FormData T_common_orgFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_common_orgFormData struct {
	PageNo       int
	PageRecCount int
}
