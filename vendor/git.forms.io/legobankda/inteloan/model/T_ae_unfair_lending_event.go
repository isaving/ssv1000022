package model

type T_ae_unfair_lendingEvent struct {
	Form []struct {
		FormData T_ae_unfair_lendingFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_unfair_lendingFormData struct {
	AcctnDt         string
	BizFolnNo       string
	SysFolnNo       string
	MstkAmt         float64
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
