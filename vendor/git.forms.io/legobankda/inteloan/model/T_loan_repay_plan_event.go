package model

type T_loan_repay_planEvent struct {
	Form []struct {
		FormData T_loan_repay_planFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_repay_planFormData struct {
	LoanDubilNo          string
	SeqNo                int
	RprincPridnum        int
	CustNo               string
	SpDay                string
	IntStlDay            string
	RepayPlanValidFlg    string
	PlanStartDt          string
	PlanMatrDt           string
	RepayManrCd          string
	IntStlCycCd          string
	IntStlCycQty         int
	RpyintPridnum        int
	EqamtEvrpridRepayAmt float64
	SpCycCd              string
	SpCycQty             int
	CycRatioRprincRatio  float64
	CycFixFrhdRprincAmt  float64
	FinlModfyDt          string
	FinlModfyTm          string
	FinlModfyOrgNo       string
	FinlModfyTelrNo      string
	PageNo       int
	PageRecCount int
}
