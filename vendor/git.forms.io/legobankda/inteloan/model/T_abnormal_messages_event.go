package model

type T_abnormal_messagesEvent struct {
	Form []struct {
		FormData T_abnormal_messagesFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_abnormal_messagesFormData struct {
	MessageId          string
	MessageType        int
	SerialNo           int
	Status             int
	TextTmplTypCd      string
	Reason             string
	TemplateId         string
	CallbackId         string
	ChannelId          string
	ProjectId          string
	OrgId              string
	TopicId            string
	SendTime           string
	SourceEmailAddress string
	Title              string
	TargetList         string
	TargetCcList       string
	AttachList         string
	Body               string
	DecodeBody         string
	SendType           string
	ServeType          string
	ErrorMsgInfo       string
	FileId             string
	GlobalBizSeqno     string
	ReturnCode         string
	ReturnMsg          string
	DeliveryTime       string
	ResponseTime       string
	Data               string
	FinlModfyDt        string
	FinlModfyTm        string
	FinlModfyOrgNo     string
	FinlModfyTelrNo    string

	PageNo       int
	PageRecCount int
}
