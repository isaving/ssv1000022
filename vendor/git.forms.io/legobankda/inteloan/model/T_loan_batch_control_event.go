package model

type T_loan_batch_controlEvent struct {
	Form []struct {
		FormData T_loan_batch_controlFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_batch_controlFormData struct {
	JobNo              string
	JobNm              string
	JobBgnExecDt       string
	JobBgnExecTm       string
	JobEndExecDt       string
	JobEndExecTm       string
	CurtyJobStusCd     string
	JobCanRepetExecFlg string
	PgmCdnmAssmblg     string
	BizDt              string
	PageNo       int
	PageRecCount int
}
