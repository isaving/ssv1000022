package model

type T_file_upd_jnlEvent struct {
	Form []struct {
		FormData T_file_upd_jnlFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_file_upd_jnlFormData struct {
	TxSn             string
	CustNo           string
	DocMgmtNo        string
	OperTypCd        string
	OperPersonEmpnbr string
	OperDt           string
	OperTm           string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string

	PageNo       int
	PageRecCount int
}
