package model

type T_loan_cn_listEvent struct {
	Form []struct {
		FormData T_loan_cn_listFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_cn_listFormData struct {
	AcctiAcctNo          string
	BizFolnNo            string
	SysFolnNo            string
	SeqNo                int
	Pridnum              int
	CurCd                string
	TxAcctnDt            string
	MgmtOrgNo            string
	AcctiOrgNo           string
	ChnlCd               string
	BgnDt                string
	EndDt                string
	Days                 int
	PrinIntr             float64
	IntrStusCd           string
	CalcIntrt            float64
	IntacrCnDwDlwthTypCd string
	DlwthStusCd          string
	IntrPlanNo           string
	CalcModeCd           string
	FinlModfyDt          string
	FinlModfyTm          string
	FinlModfyOrgNo       string
	FinlModfyTelrNo      string
	PageNo       int
	PageRecCount int
}
