package model

type T_bal_typeEvent struct {
	Form []struct {
		FormData T_bal_typeFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_bal_typeFormData struct {
	AcctiAcctNo                string
	IntrStusCd                 string
	Pridnum                    int
	BegintDt                   string
	ExpiryDt                   string
	PlanRpyintAmt              float64
	ActlRpyintAmt              float64
	AccmCnDwAmt                float64
	IntStlAmt                  float64
	RecvblPrinPnltintCmpdAmt   float64
	ActlRecvPrinPnltintCmpdAmt float64
	AlrdyTranOffshetIntr       float64
	OnshetIntr                 float64
	DataValidFlgCd             string
	AlrdyDvalIntr              float64
	AlrdyWrtoffIntr            float64
	AcruUnstlIntr              float64
	AccmPrinPnltintCmpdAmt     float64
	ActlRpyintPrerdcAmt        float64
	ActlRpyintPreincAmt        float64
	ActlRpyintTccStusCd        string
	ActlRecvCmpdPrerdcAmt      float64
	ActlRecvCmpdPreincAmt      float64
	ActlRecvCmpdTccStusCd      string
	FrzAmt                     float64
	ActlstPnltintAmt           float64
	FinlCalcDt                 string
	FinlModfyDt                string
	FinlModfyTm                string
	FinlModfyOrgNo             string
	FinlModfyTelrNo            string

	PageNo       int
	PageRecCount int
}
