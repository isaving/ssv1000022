package model

type Ts_sys_constantEvent struct {
	Form []struct {
		FormData Ts_sys_constantFormData
		FormHead struct {
			FormID string
		}
	}
}

type Ts_sys_constantFormData struct {
	PageNo       int
	PageRecCount int
}
