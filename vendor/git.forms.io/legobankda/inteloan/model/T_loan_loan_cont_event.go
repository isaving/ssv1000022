package model

type T_loan_loan_contEvent struct {
	Form []struct {
		FormData T_loan_loan_contFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_loan_contFormData struct {
	LoanDubilNo                    string
	AcctiAcctNo                    string
	ContrtStusCd                   string
	CustNo                         string
	LoanProdtNo                    string
	LoanProdtVersNo                string
	MakelnOrgNo                    string
	IndvCrtfTypCd                  string
	IndvCrtfNo                     string
	RevneCmpdCd                    string
	MakelnManrCd                   string
	RelaAcctDtrmnManrCd            string
	RepayDayDtrmnManrCd            string
	LoanDeadl	               	   int
	LoanDeadlCycCd                 string
	PmitAdvRepayTms                int
	AdvRepayLimitBgnDt             string
	AdvRepayLimitDays              int
	LimitTrmInsidPmitPartlRepayFlg string
	LimitTrmInsidPmitPayOffFlg     string
	OpenAcctDt                     string
	FsttmForsprtDt                 string
	BegintDt                       string
	OrgnlMatrDt                    string
	CurCd                          string
	LoanAmt                        float64
	EmbFlg                         string
	DecdEmbDt                      string
	MansbjTypCd                    string
	CtrtNo                         string
	LoanGuarManrCd                 string
	OthConsmTypCd                  string
	RepayManrCd                    string
	IntStlDayDtrmnManrCd           string
	AdvRepayTms                    int
	TranNormlLoanDt                string
	BldInstltRepayFlg              string
	IntStlPrtyTypCd                string
	LoanIntrtAdjCycCd              string
	LoanIntrtAdjCycQty             int
	StpDeductFlg                   string
	StpDeductRsnTypCd              string
	StpDeductCnfrmDt               string
	LoanIntrtAdjManrCd             string
	ExpdayPayOffManrCd             string
	GraceTrmIntacrFlg              string
	EvrpridMaxGraceTrmDays         int
	ContrtPrdGraceTrmTotlDays      int
	AdvRepayColtfeFlg              string
	ColtfeManrCd                   string
	SglColtfeAmt                   float64
	ColtfeAmtCrdnlnbrCd            string
	ColtfeRatio                    float64
	AcrdgRatioColtfeCeilAmt        float64
	AcrdgRatioColtfeFloorAmt       float64
	CurrExecTmprd                  int
	RepayPlanAdjFlg                string
	RestFlg                        string
	RestDt                         string
	TranDvalDt                     string
	TranDvalFlg                    string
	ExtsnTms                       int
	WaitExtsnFlg                   string
	CurrLoanRiskClsfCd             string
	MatrDt                         string
	PayOffDt                       string
	FinlModfyDt                    string
	FinlModfyTm                    string
	FinlModfyOrgNo                 string
	FinlModfyTelrNo                string

	PageNo       int
	PageRecCount int
}
