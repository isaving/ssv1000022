package model

type T_loan_consu_mgmtEvent struct {
	Form []struct {
		FormData T_loan_consu_mgmtFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_consu_mgmtFormData struct {
	LoanAcctNo      string
	LoanContNo      string
	LoanConcNo      string
	LoanAcctgAcctNo string
	OverdueDays     int
	CustId          string
	CustName        string
	IdType          string
	IdNo            string
	ProdId          string
	ProdVersion     string
	DepAcctNo       string
	AcctOpenDate    string
	ExpDate         string
	RepayType       string
	Status          string
	PeriodBeginDate string
	PeriodEndDate   string
	LastMaintDate   string
	LastMaintTime   string
	LastMaintBrno   string
	LastMaintTell   string

	PageNo       int
	PageRecCount int
}
