package model

type T_artif_interset_rate_flowEvent struct {
	Form []struct {
		FormData T_artif_interset_rate_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_artif_interset_rate_flowFormData struct {
	IntrtAdjSn         string
	CustNo            string
	CustName          string
	CrdtAplySn        string
	IndvCrtfTypCd     string
	IndvCrtfNo        string
	BaseIntrtTypCd    string
	CurCd             string
	IntrtNo           string
	IntrtFlotDrctCd   string
	BpFlotVal         float64
	KeprcdStusCd      string
	CrtTelrNo         string
	CrtTm             string
	CrtOrgNo          string
	ApprvSugstnCd     string
	ApprvSugstnComnt  string
	ApprvTelrNo       string
	ApprvTm           string
	ApprvOrgNo        string
	IntrtFlotManrCd   string
	IntrtFlotRatio    float64
	AdjRsn    		  string
	CrtTelrName 	  string
	ApprvTelrName     string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string

	PageNo       int
	PageRecCount int
}
