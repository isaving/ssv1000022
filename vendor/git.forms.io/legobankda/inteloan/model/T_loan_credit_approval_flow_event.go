package model

type T_loan_credit_approval_flowEvent struct {
	Form []struct {
		FormData T_loan_credit_approval_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_credit_approval_flowFormData struct {
	CrdtAplySn       string
	SeqNo       	 int
	CustNo           string
	OperPersonName   string
	LnrwModeCd       string
	ApprvManrCd      string
	OperEndTm        string
	NodeTypCd        string
	OperPersonEmpnbr string
	OperOrgNo        string
	//OperBgnDt        string
	OperBgnTm        string
	ApprvSugstnCd    string
	RplshInfo        string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string

	PageNo       int
	PageRecCount int
}
