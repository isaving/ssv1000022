package model

type T_loan_info_tmpl_cmEvent struct {
	Form []struct {
		FormData T_loan_info_tmpl_cmFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_info_tmpl_cmFormData struct {
	TmplNo            string
	TmplNm            string
	TmplComnt         string
	EfftDt            string
	LoanProdtNo       string
	ApplyScena        string
	TextTmplTypCd     string
	EventId           string
	TextTmplDetailCd  string
	OrgId             string
	Area              string
	ChannelNumber     string
	SendObjectType    string
	SuitHavegProdtFlg string
	TmplContent       string
	PmitSndFlg        string
	KeprcdStusCd      string
	Priority          string
	ProjectId         string
	Title             string
	MessageType       string
	Type              string
	TmplType          string
	State             string
	Subject           string
	PubType           string
	TmplId            string
	SendStrategyId    string
	AttachFlag        string
	CreateUser        string
	CreateTime        string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string

	PageNo       int
	PageRecCount int
}

