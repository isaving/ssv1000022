package model

type T_af_bnk_coll_task_refeEvent struct {
	Form []struct {
		FormData T_af_bnk_coll_task_refeFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_af_bnk_coll_task_refeFormData struct {
	CollTaskNo       string
	SeqNo            int
	GnrtDt           string
	CollMnrpEmpnbr   string
	CollMnrpOrgNo    string
	TrnsfDt          string
	TrnsfManrCd      string
	TsfinChnlNoCd    string
	TsfinEmpnbr      string
	TsfinOrgNo       string
	OperPersonEmpnbr string
	OperOrgNo        string
	TrnsfStusCd      string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string

	PageNo       int
	PageRecCount int
}
