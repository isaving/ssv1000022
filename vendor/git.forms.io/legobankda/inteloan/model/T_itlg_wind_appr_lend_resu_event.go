package model

type T_itlg_wind_appr_lend_resuEvent struct {
	Form []struct {
		FormData T_itlg_wind_appr_lend_resuFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_itlg_wind_appr_lend_resuFormData struct {
	MakelnAplySn       string
	SeqNo              int
	CustNo             string
	DcmkApprvResultCd  string
	DcmkApprvDescr     string
	HitExcludRuleComnt string
	BlklistTypCd       string
	FinlModfyDt        string
	FinlModfyTm        string
	FinlModfyOrgNo     string
	FinlModifrEmpnbr   string
	PageNo       int
	PageRecCount int
}
