package model

type T_sim_intel_decision_systemEvent struct {
	Form []struct {
		FormData T_sim_intel_decision_systemFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sim_intel_decision_systemFormData struct {
	CrdtAplySn		string
	SeqNo           int
	LoanDeadl       int
	RepayManrCd     string

	PageNo       int
	PageRecCount int
}
