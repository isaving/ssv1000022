package model

type T_loan_gignature_position_paramEvent struct {
	Form []struct {
		FormData T_loan_gignature_position_paramFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_gignature_position_paramFormData struct {
	PageNo       int
	PageRecCount int
}
