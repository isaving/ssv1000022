package model

type T_intelligent_loan_approvalEvent struct {
	Form []struct {
		FormData T_intelligent_loan_approvalFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intelligent_loan_approvalFormData struct {
	BusinessTriggerType           string
	BusinessType                  string
	LoanProdtNo                   string
	LoanProdtVersNo               string
	ApplyForTheLimitOfExpenditure float32
	ChannelType                   string
	HandlerCode                   string
	AgentOrganizationCode         string
	IndvCrtfNo                    string
	IndvCrtfTypCd                 string
	CustName                      string
	CustNo                        string
	ResidentialAddress            string
	Age                           int
	BlacklistFlag                 string
	Id                            int
	DecisionType                  string
	DecisionDescription           string
	ExclusionRules                string
	BlacklistTypeInvolved         string
	PageNo       int
	PageRecCount int
}
