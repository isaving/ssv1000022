package model

type T_loan_inspection_task_resultEvent struct {
	Form []struct {
		FormData T_loan_inspection_task_resultFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_inspection_task_resultFormData struct {
	ChkTaskNo                    string
	SeqNo                        int
	CustNo                       string
	TaskDlwthResultNo            string
	HndlrEmpnbr                  string
	ResultCnfrmRemrkInfo         string
	ReacCustManrCd               string
	KeprcdStusCd                 string
	CorgDlwthResultCd            string
	CollTms                      int
	EmbFlgCd                     string
	AcrdgWhenRprincPayIntFlgCd   string
	FamilyWorkMangNormlFlgCd     string
	BrwerAndFamlMebrCprtChkFlgCd string
	ChkDlwthRemrk                string
	CotactTelNo                  string
	CurrAddr                     string
	Lgtd                         float64
	LgtdDrctCd                   string
	Lttd                         float64
	LttdDrctCd                   string
	UpdAftRsdnceScAdcmCd         string
	UpdAftRsdnceDtlAddr          string
	UpdAftRsdnceAddr             string
	UpdAftCmunicAddrScAdcmCd     string
	UpdAftCmunicDtlAddr          string
	UpdAftCmunicAddr             string
	UpdGrndPostCd                string
	MrgncyConterName             string
	AndBrwerRelCd                string
	MrgncyConterTelNo            string
	SponsorEmpnbr                string
	SponsorOrgNo                 string
	CorgEmpnbr                   string
	CorgOrgNo                    string
	RiskAnlyzInfo                string
	OneslfCmtdInfo               string
	DlwthSugstnCd                string
	DlwthSugstnComnt             string
	DlwthEmpnbr                  string
	DlwthOrgNo                   string
	DlwthDt                      string
	FinlModfyDt                  string
	FinlModfyTm                  string
	FinlModfyOrgNo               string
	FinlModfyTelrNo              string

	PageNo       int
	PageRecCount int
}
