package model

type T_loan_int_settleEvent struct {
	Form []struct {
		FormData T_loan_int_settleFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_int_settleFormData struct {
	AcctiAcctNo     string
	BizFolnNo       string
	SysFolnNo       string
	Pridnum         int
	IntrStusCd      string
	TxAcctnDt       string
	TxReqeCd        string
	DlwthStusCd     string
	IntacrBgnDt     string
	IntacrEndDt     string
	IntacrDays      int
	IntacrAmt       float64
	IntacrIntrt     float64
	IntrAmt         float64
	PmitRvrsFlg     string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
