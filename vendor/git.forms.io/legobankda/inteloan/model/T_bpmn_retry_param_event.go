package model

type T_bpmn_retry_paramEvent struct {
	Form []struct {
		FormData T_bpmn_retry_paramFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_bpmn_retry_paramFormData struct {
	TaskExampId   string
	ProcesParaVal string
	PageNo       int
	PageRecCount int
}
