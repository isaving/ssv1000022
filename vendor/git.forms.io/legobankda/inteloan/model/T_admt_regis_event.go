package model

type T_admt_regisEvent struct {
	Form []struct {
		FormData T_admt_regisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_admt_regisFormData struct {
	LoanDubilNo     string
	SeqNo           int
	DvalBizAplyNo   string
	DvalDlwthStusCd string
	CtrtNo          string
	CustNo          string
	CustName        string
	LoanProdtNo     string
	LoanProdtVersNo string
	LoanProdtNm     string
	GrantDt         string
	MatrDt          string
	VersNo          int
	AdvMatrDt       string
	AdvMatrReason   string
	AuthOrgNo       string
	AuthEmpnbr      string
	AplyOrgNo       string
	AplyEmpnbr      string
	FinlModfyDt     string
	FinlModfyTm      string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
