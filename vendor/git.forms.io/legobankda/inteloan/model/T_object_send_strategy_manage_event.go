package model

type T_object_send_strategy_manageEvent struct {
	Form []struct {
		FormData T_object_send_strategy_manageFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_object_send_strategy_manageFormData struct {
	SendStrategyId      string
	MessageType         string
	MessageObjectNumber int
	PriorityFlag        string
	AdvSndDays          int
	SndBgnTm            string
	SndEndTm            string
	SendSequence        string
	RepeatGapTime       string
	ResponseFlag        string
	ResponseMaxDay      string
	Channel             string
	InfoType            string
	EntrustFlag         string
	EntrustInfo         string
	RepeatFlag          string
	RepeatWaitTime      string
	RepeatMaxFrequency  string
	SendTypeCd          string
	SendType            string
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string

	PageNo       int
	PageRecCount int
}
