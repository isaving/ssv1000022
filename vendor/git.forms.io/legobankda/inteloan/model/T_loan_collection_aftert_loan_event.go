package model

type T_loan_collection_aftert_loanEvent struct {
	Form []struct {
		FormData T_loan_collection_aftert_loanFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_collection_aftert_loanFormData struct {
	CollTaskNo         string
	LoanDubilNo        string
	CustNo             string
	CustName           string
	IndvCrtfTypCd      string
	IndvCrtfNo         string
	OrgNo              string
	OrgNm              string
	CrtDt              string
	TaskDlwthStusCd    string
	AlrdyExecFlg       string
	AtExecWindowFlg    string
	ExecTms            int
	ExecRsn            string
	ExecConcusComnt    string
	CollSponsorEmpnbr  string
	CollSponsorOrgNo   string
	CollCorgEmpnbr     string
	CollCorgOrgNo      string
	CtrtNo             string
	ReacTotlTms        int
	DlwthDt            string
	SponsorEmplyTaskNo string
	CorgEmplyTaskNo    string
	TaskDlwthResultNo  string
	SponsorEmplyName   string
	CorgEmplyName      string
	SponsorOrgNm       string
	CorgOrgNm          string
	ReacCustTms        int
	FinlModfyDt        string
	FinlModfyTm        string
	FinlModfyOrgNo     string
	FinlModfyTelrNo    string

	PageNo       int
	PageRecCount int
}
