package model

type T_loan_manager_customerEvent struct {
	Form []struct {
		FormData T_loan_manager_customerFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_manager_customerFormData struct {
	CustMgrEmpnbr     string
	CorgCustMgrEmpnbr string
	CorgCustMgrName   string
	CustMgrName       string
	LprOrgNo          string
	KeprcdStusCd      string
	CustNo            string
	ExcutvPrvlgTypCd  string
	BlngtoOrgNo       string
	RelBlngTypCd      string
	CustName          string
	IndvCrtfTypCd     string
	IndvCrtfNo        string
	CustCotactTelNo   string
	CrtDt             string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
