package model

type T_loan_collection_dec_aftert_loanEvent struct {
	Form []struct {
		FormData T_loan_collection_dec_aftert_loanFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_collection_dec_aftert_loanFormData struct {
	CollTaskNo               string
	CurrLoanRiskClsfCd       string
	DlqcyProb                float64
	RiskRsn                  string
	SggestCollMgmtManrCd     string
	AfBnkLnModelExecDt       string
	CustName                 string
	CustNo                   string
	IndvCrtfNo               string
	IndvCrtfTypCd            string
	LoanDubilNo              string
	SggestCollExecWindowDays int
	FinlModfyDt              string
	FinlModfyTm              string
	FinlModfyOrgNo           string
	FinlModfyTelrNo          string

	PageNo       int
	PageRecCount int
}
