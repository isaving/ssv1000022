package model

type T_sys_biz_date_planEvent struct {
	Form []struct {
		FormData T_sys_biz_date_planFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sys_biz_date_planFormData struct {
	PlanNo          string
	BizDt           string
	NxtonePlanNo    string
	AvalFlg         string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
