package model

type T_sms_templateEvent struct {
	Form []struct {
		FormData T_sms_templateFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sms_templateFormData struct {
	TmplNo           string
	TmplNm           string
	TmplContent      string
	TmplComnt        string
	TmplTypCd        string
	PmitSndFlg       string
	EfftDt           string
	AdvSndDays       int
	SndBgnTm         string
	SndEndTm         string
	RiskClsfValidFlg string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string

	PageNo       int
	PageRecCount int
}
