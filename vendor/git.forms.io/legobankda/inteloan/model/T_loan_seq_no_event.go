package model

type T_loan_seq_noEvent struct {
	Form []struct {
		FormData T_loan_seq_noFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_seq_noFormData struct {
	IdenCd      string
	CurrSeqNo   int
	FinlModfyDt string
	FinlModfyTm string
	PageNo       int
	PageRecCount int
}
