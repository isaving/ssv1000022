package model

type T_loan_contactEvent struct {
	Form []struct {
		FormData T_loan_contactFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_contactFormData struct {
	MakelnAplySn    string
	SeqNo           int
	CustNo    		string
	ConterCustNo    string
	ConterNm        string
	ConterTelNo     string
	KinRelCd        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
