package model

type T_loan_quota_changeEvent struct {
	Form []struct {
		FormData T_loan_quota_changeFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_quota_changeFormData struct {
	ChgSn            string
	CustNo           string
	CrdtAplySn       string
	OperSorcCd       string
	OperBefQtaStusCd string
	OperAftQtaStusCd string
	QtaOperTypCd     string
	CurCd            string
	ChgBefAvalQta    float64
	ChgAftQta           float64
	ChgAftAvalQta    float64
	CrtDt            string
	CrtTelrNo        string
	CrtOrgNo         string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string
	PageNo       int
	PageRecCount int
}
