package model

type T_intrt_chg_hisEvent struct {
	Form []struct {
		FormData T_intrt_chg_hisFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_intrt_chg_hisFormData struct {
	IntacrRuleNo      string
	EfftDt            string
	ChgDt             string
	UseIntrtSorcCd    string
	HrchQty           int
	OrgNo             string
	IntrtChgCycCd     string
	DpstDeadlCd       string
	HrchAmt           float64
	IntrtNo           string
	RelsIntrtOrgNo    string
	Intrt             float64
	IntrtFlotManrCd   string
	FlotDrctCd        string
	IntrtFlotCeilVal  float64
	IntrtFlotFloorVal float64
	DfltFlotVal       float64
	LowestIntrt       float64
	ChgTelrNo         string
	AuthTelrNo        string
	Flag1             string
	Flag2             string
	Bak1              float64
	Bak2              float64
	Bak3              string
	Bak4              string
	FinlModfyDt       string
	FinlModfyTm       string
	FinlModfyOrgNo    string
	FinlModfyTelrNo   string
	PageNo       int
	PageRecCount int
}
