package model

type T_sys_status_controlEvent struct {
	Form []struct {
		FormData T_sys_status_controlFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_sys_status_controlFormData struct {
	KeprcdNo        string
	SysStusCd       string
	SysCtofModeCd   string
	OnlineBizDt     string
	BatBizDt        string
	NxtoneBizDt     string
	LstoneBizDt     string
	CtofTm          string
	TranOnlineTm    string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
