package model

type T_impa_writeoff_appr_flowEvent struct {
	Form []struct {
		FormData T_impa_writeoff_appr_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_impa_writeoff_appr_flowFormData struct {
	LoanAcctNo       string
	TxSn             string
	BizAplyNo        string
	BizTypCd         string
	OperPersonEmpnbr string
	OperOrgNo        string
	OperTm           string
	ApprvSugstnCd    string
	SugstnDescr      string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModfyTelrNo  string

	PageNo       int
	PageRecCount int
}
