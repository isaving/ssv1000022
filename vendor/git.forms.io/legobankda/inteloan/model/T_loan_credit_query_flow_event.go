package model

type T_loan_credit_query_flowEvent struct {
	Form []struct {
		FormData T_loan_credit_query_flowFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_credit_query_flowFormData struct {
	CrdQurySn       string
	CustNo          string
	IndvCrtfTypCd   string
	IndvCrtfNo      string
	CrdQuryDt       string
	CrdQurySuccFlg  string
	CrdQuryResultId string
	LoanProdtNo     string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
