package model

type Credit_approval_tempEvent struct {
	Form []struct {
		FormData Credit_approval_tempFormData
		FormHead struct {
			FormID string
		}
	}
}

type Credit_approval_tempFormData struct {
	PageNo       int
	PageRecCount int
}
