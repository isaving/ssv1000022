package model

type T_risk_cls_ovrday_compEvent struct {
	Form []struct {
		FormData T_risk_cls_ovrday_compFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_risk_cls_ovrday_compFormData struct {
	RiskClsfNo              string
	EfftDt                  string
	LoanProdtClsfCd         string
	LoanGuarManrCd          string
	LoanRiskClsfCd          string
	LoanRiskClsfDescr       string
	OvdueAmtTypCd           string
	OvdueDaysTypCd          string
	ExecCycCd               string
	BatClsfDtCd             string
	OvdueFloorDays          int
	OvdueCeilDays           int
	RiskClsfOvdueDecdManrCd string
	CrtTelrNo               string
	CrtDt                   string
	CrtTm                   string
	CrtOrgNo                string
	RiskClsfValidFlg        string
	FinlModfyDt             string
	FinlModfyTm             string
	FinlModfyOrgNo          string
	FinlModfyTelrNo         string

	PageNo       int
	PageRecCount int
}
