package model

type T_ae_bkpmEvent struct {
	Form []struct {
		FormData T_ae_bkpmFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_bkpmFormData struct {
	AcctBookCategCd        string
	AcctBookStusCd         string
	AcctBookSeqNo          int
	SubjLength             int
	OnshetPngdgacctSubjNo  string
	OffshetPngdgacctSubjNo string
	UnDistrMarginSubjNo    string
	EngineVaritCd          string
	EngineVaritNm          string
	InterLiqdSubjNo        string
	EfftDt                 string
	InvalidDt              string
	FinlModfyDt            string
	FinlModfyTm            string
	FinlModfyOrgNo         string
	FinlModfyTelrNo        string
	PageNo       int
	PageRecCount int
}
