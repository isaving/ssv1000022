package model

type T_loan_orgaz_balanceEvent struct {
	Form []struct {
		FormData T_loan_orgaz_balanceFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_orgaz_balanceFormData struct {
	AcctnDt         string
	OrgNo           string
	LoanProdtNo     string
	CurCd           string
	TodayLoanBal    float64
	LstdyLoanBal    float64
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string
	PageNo       int
	PageRecCount int
}
