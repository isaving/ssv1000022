package model

type T_early_warn_decision_infoEvent struct {
	Form []struct {
		FormData T_early_warn_decision_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_early_warn_decision_infoFormData struct {
	WarnTaskNo            string
	LoanDubilNo           string
	CustNo                string
	CustName              string
	IndvCrtfTypCd         string
	IndvCrtfNo            string
	RiskRsn               string
	SggestWarnSignalTypCd string
	SggestExecWindowDays  int
	AdjAftCrdtQta         float64
	AdjDrctCd             string
	InlnModelExecDt       string
	DlqcyProb             float64
	CurrRiskGradeCd       string
	FinlModfyDt           string
	FinlModfyTm           string
	FinlModfyOrgNo        string
	FinlModfyTelrNo       string
	PageNo                    int
	PageRecCount              int
}
