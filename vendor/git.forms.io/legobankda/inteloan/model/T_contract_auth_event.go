package model

type T_contract_authEvent struct {
	Form []struct {
		FormData T_contract_authFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_contract_authFormData struct {
	MobileNo            string
	SeqNo               int
	CustNo              string
	CustName            string
	SgnOrgNo            string
	FileSgnNo           string
	TmplNo              string
	AuthTypCd           string
	AuthQuryStusCd      string
	AuthDt              string
	AuthMatrDt          string
	DocMgmtMainTablPkNo int
	FinlModfyDt         string
	FinlModfyTm         string
	FinlModfyOrgNo      string
	FinlModfyTelrNo     string

	PageNo       int
	PageRecCount int
}
