package model

type T_loan_order_business_infoEvent struct {
	Form []struct {
		FormData T_loan_order_business_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_order_business_infoFormData struct {
	OrginlBizSn      string
	IntdTypCd        string
	BizFolnNo        string
	SysFolnNo        string
	CustNo           string
	ExecStepNo       int
	TxTelrNo         string
	TxOrgNo          string
	TxDt             string
	TxTm             string
	FinlModfyDt      string
	FinlModfyTm      string
	FinlModfyOrgNo   string
	FinlModifrEmpnbr string
	PageNo       int
	PageRecCount int
}
