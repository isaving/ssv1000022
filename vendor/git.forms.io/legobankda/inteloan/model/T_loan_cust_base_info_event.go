package model

type T_loan_cust_base_infoEvent struct {
	Form []struct {
		FormData T_loan_cust_base_infoFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_loan_cust_base_infoFormData struct {
	PageNo       int
	PageRecCount int
}
