package model

type T_ae_syslistEvent struct {
	Form []struct {
		FormData T_ae_syslistFormData
		FormHead struct {
			FormID string
		}
	}
}

type T_ae_syslistFormData struct {
	SoftProdtCd     string
	SoftProdtNm     string
	ValidFlg        string
	FinlModfyDt     string
	FinlModfyTm     string
	FinlModfyOrgNo  string
	FinlModfyTelrNo string

	PageNo       int
	PageRecCount int
}
