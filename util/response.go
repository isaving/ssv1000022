package util

import (
	"fmt"
	"git.forms.io/legobankda/inteloan/const"
	"git.forms.io/legobankda/inteloan/model"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego/orm"
	"github.com/json-iterator/go"
	"runtime/debug"
	"strings"
	"time"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary //修改1

func BuildErrResponses(errMsg, errCode string) *client.UserMessage {
	response := &client.UserMessage{}
	/*lang, ok := request.AppProps[model.USERLANG]
	if !ok {
		lang = model.ENUS
	}*/
	//errMsg := i18n.Tr(lang, "errors."+errCode, msg)
	//errMsg := i18n.Tr(lang, "errors."+errCode)
	props := make(map[string]string)
	props[constant.RET_STATUS] = "F"
	props[constant.TRG_TIMESTAMP] = time.Now().Format(constant.TIMESTAMP_FORMAT)

	response.AppProps = props

	rspBodyMsg := &model.ErrorResponse{}
	msg := model.ErrorMsg{
		FormHead: struct{ FormId string }{FormId: constant.ERROR_FORMID},
		FormData: struct {
			RetMsgCode string
			RetAuthLvl string
			RetMessage string
		}{RetMsgCode: errCode, RetAuthLvl: "", RetMessage: errMsg},
	}
	rspBodyMsg.Form = append(rspBodyMsg.Form, msg)
	rspBody, _ := json.Marshal(rspBodyMsg)
	response.Body = rspBody

	return response
}

func BuildErrResponse(errCode, errMsg string) *client.UserMessage {
	// 错误码
	if strings.Contains(errMsg, "bad connection") || strings.Contains(errMsg, "invalid connection") {
		errCode = constant.DB_ERR
	} else if strings.Contains(errMsg, "Error 1062") {
		errCode = constant.PK_CONFLICT
	}
	//修改2
	log.Error(errMsg)
	log.Error(fmt.Sprintf("%s", debug.Stack()))

	response := &client.UserMessage{}

	props := make(map[string]string)
	props[constant.RET_STATUS] = "F"
	props[constant.TRG_TIMESTAMP] = time.Now().Format(constant.TIMESTAMP_FORMAT)
	response.AppProps = props

	rspBodyMsg := &model.ErrorResponse{}
	msg := model.ErrorMsg{
		FormHead: struct{ FormId string }{FormId: constant.ERROR_FORMID},
		FormData: struct {
			RetMsgCode string
			RetAuthLvl string
			RetMessage string
		}{RetMsgCode: errCode, RetAuthLvl: "", RetMessage: errMsg},
	}
	rspBodyMsg.Form = append(rspBodyMsg.Form, msg)
	rspBody, _ := json.Marshal(rspBodyMsg)
	response.Body = rspBody

	return response
}

func BuildSucResponse(formId string, data map[string]interface{}) *client.UserMessage {
	response := &client.UserMessage{}

	props := make(map[string]string)
	props[constant.RET_STATUS] = "N"
	props[constant.TRG_TIMESTAMP] = time.Now().Format(constant.TIMESTAMP_FORMAT)
	response.AppProps = props

	rspBodyMsg := &model.Response{}
	msg := model.Data{
		FormHead: struct{ FormId string }{FormId: formId},
		FormData: data,
	}
	rspBodyMsg.Form = append(rspBodyMsg.Form, msg)
	rspBody, _ := json.Marshal(rspBodyMsg)
	response.Body = rspBody

	return response
}

func BuildArrResponse(formId string, datas []orm.Params) *client.UserMessage {
	response := &client.UserMessage{}

	props := make(map[string]string)
	props[constant.RET_STATUS] = "N"
	props[constant.TRG_TIMESTAMP] = time.Now().Format(constant.TIMESTAMP_FORMAT)
	response.AppProps = props

	rspBodyMsg := &model.ArrayResponse{}
	slice := make([]map[string]interface{}, 0)
	for _, m := range datas {
		slice = append(slice, m)
	}
	msg := model.Datas{
		FormHead: struct{ FormId string }{FormId: formId},
		FormData: slice,
	}
	rspBodyMsg.Form = append(rspBodyMsg.Form, msg)
	rspBody, _ := json.Marshal(rspBodyMsg)
	response.Body = rspBody

	return response
}

func BuildPageResponse(formId string, total int, pageNo int, pageDatas []orm.Params) *client.UserMessage {
	response := &client.UserMessage{}

	props := make(map[string]string)
	props[constant.RET_STATUS] = "N"
	props[constant.TRG_TIMESTAMP] = time.Now().Format(constant.TIMESTAMP_FORMAT)
	response.AppProps = props

	rspBodyMsg := &model.PageResponse{}
	var slice []map[string]interface{}
	for _, m := range pageDatas {
		slice = append(slice, m)
	}
	msg := model.PageDatas{
		FormHead: struct{ FormId string }{FormId: formId},
		FormData: struct {
			PageTotCount int
			PageNo       int
			Records      []map[string]interface{}
		}{PageTotCount: total, PageNo: pageNo, Records: slice},
	}
	rspBodyMsg.Form = append(rspBodyMsg.Form, msg)
	rspBody, _ := json.Marshal(rspBodyMsg)
	response.Body = rspBody

	return response
}
