//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000022/constant"
	"git.forms.io/isaving/sv/ssv1000022/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000022Controller struct {
    controllers.CommController
}

func (*Ssv1000022Controller) ControllerName() string {
	return "Ssv1000022Controller"
}

// @Desc ssv1000022 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000022Controller) Ssv1000022() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000022Controller.Ssv1000022 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000022I := &models.SSV1000022I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000022I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  if err := ssv1000022I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000022 := &services.Ssv1000022Impl{} 
    ssv1000022.New(c.CommController)
	ssv1000022.SSV1000022I = ssv1000022I

	ssv1000022O, err := ssv1000022.Ssv1000022(ssv1000022I)

	if err != nil {
		log.Errorf("Ssv1000022Controller.Ssv1000022 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000022O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}
// @Title Ssv1000022 Controller
// @Description ssv1000022 controller
// @Param Ssv1000022 body models.SSV1000022I true body for SSV1000022 content
// @Success 200 {object} models.SSV1000022O
// @router /create [post]
func (c *Ssv1000022Controller) SWSsv1000022() {
	//Here is to generate API documentation, no need to implement methods
}
