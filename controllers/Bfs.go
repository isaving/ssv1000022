package controllers

import (
	"encoding/base64"
	"fmt"
	"git.forms.io/isaving/sv/ssv1000022/util"
	commRsa "git.forms.io/universe/common/crypto/rsa"
	"git.forms.io/universe/common/json"
	"github.com/astaxie/beego"
)

type BfsController struct {
	beego.Controller
}

func (c *Ssv1000022Controller) Bfs() {

	requestDate :=c.CommController.BaseController.Ctx.Input.RequestBody
	fmt.Println(string(requestDate))
	maps := make(map[string]interface{})
	bfs := &Bfs{}
	if err := json.Unmarshal(requestDate, bfs); err != nil {
		maps["ErrMessage"] = "解包失败"
		maps["Err"] = err
		c.Data["json"] = maps
		c.ServeJSON()
		return
	}
	Pub_Key, _ := base64.StdEncoding.DecodeString(bfs.K2_PubKey)
	rsaInstanc := commRsa.NewRSA(nil, Pub_Key)
	AesPsw, _ := rsaInstanc.Encrypt([]byte(bfs.PassWord))
	BasePws:= base64.StdEncoding.EncodeToString(AesPsw)

	//模拟私钥解密
	Pri_Key, _ := base64.StdEncoding.DecodeString(bfs.K1_PriKey)
	rsaInstanc1 := commRsa.NewRSA(Pri_Key, nil)
	DecPsw, _ := rsaInstanc1.Decrypt(AesPsw)
	fmt.Println(string(DecPsw))
	if bfs.PassWord != string(DecPsw){
		maps["ErrMesssage"] = "私钥解密后的密码和公钥加密的密码不一致"
		c.Data["json"] = maps
		c.ServeJSON()
	}
	byteRsq := util.GenerateKey(string(DecPsw))
	SaltPsw := base64.StdEncoding.EncodeToString(byteRsq)

	maps["SaltPsw"] = SaltPsw
	maps["BasePws"] = BasePws
	c.Data["json"] = maps
	c.ServeJSON()

}

type Bfs struct {
	KeyVersion	string	//公钥版本号
	K1_PriKey	string	//存款私钥
	K2_PubKey	string	//存款公钥
	PassWord	string	//密码
}
